// Importar Sequelize directamente
const { Sequelize } = require('sequelize')
// Importar tu configuración de sequelize
const { sequelize } = require('../../database/mysql')
// Importar el modelo de usuarios
const AuthModel = require('./auth')

const TokenModel = sequelize.define(
    'tokens',
    {
        id: {
            type: Sequelize.INTEGER,
            primaryKey: true,
            primaryKey: true,
            autoIncrement: true,
        },
        guid: {
            type: Sequelize.UUID,
            defaultValue: Sequelize.UUIDV4,
            unique: true,
        },
        user_guid: {
            type: Sequelize.UUID,
        },
        refresh_token: {
            type: Sequelize.TEXT,
        },
        expiry_date: {
            type: Sequelize.DATE,
        },
        created_at: {
            type: Sequelize.DATE,
        },
        revoket: {
            type: Sequelize.BOOLEAN,
        },
        revoked_at: {
            type: Sequelize.DATE,
        },
        device_info: {
            type: Sequelize.TEXT,
        },
    },
    {
        tableName: 'tokens',
        timestamps: false,
    }
)

// Relación uno a uno con el modelo de usuarios
TokenModel.belongsTo(AuthModel, {
    foreignKey: 'user_guid',
    targetKey: 'guid',
    as: 'user',
    allowNull: false,
    where: { is_active: 1 },
    exclude: [
        'password',
        'is_active',
        'created_at',
        'created_by',
        'updated_at',
        'updated_by',
        'deleted_at',
        'deleted_by',
    ], // Excluir estos campos de la relación
})

module.exports = TokenModel
