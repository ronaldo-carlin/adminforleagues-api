const { matchedData } = require('express-validator')
const TenantCredentialsModel = require('../../models/general/tenant_credentials')
const { handleHttpError } = require('../../utils/handleError')
const { handleHttpSuccess } = require('../../utils/handleSuccess')
const { handleHttpCount } = require('../../utils/handleCount')
const { runSeedersMockup } = require('../../services/tenantService')

/**
 * Este controlador es el encargado de crear credenciales de tenant
 * @param {*} req
 * @param {*} res
 */
const CreateTenantCredential = async (req, res) => {
    try {
        const data = matchedData(req)
        const created_by = req.user.guid
        const created_at = new Date()
        data.created_by = created_by
        data.created_at = created_at
        const tenantCredential = await TenantCredentialsModel.create(data)
        return handleHttpSuccess(
            res,
            'Se ha creado la credencial del tenant.',
            201,
            tenantCredential
        )
    } catch (error) {
        console.log(error)
        return handleHttpError(res, '', 403, error)
    }
}

/**
 * Este controlador es el encargado de obtener todas las credenciales activas de tenant
 * @param {*} req
 * @param {*} res
 */
const TenantCredentials = async (req, res) => {
    try {
        const tenantCredentials = await TenantCredentialsModel.findAll({
            where: { is_active: 1 },
        })
        if (!tenantCredentials) {
            return handleHttpError(
                res,
                'No existen credenciales de tenant activas.',
                404
            )
        }
        const message = tenantCredentials.length
            ? 'Se han encontrado las credenciales.'
            : 'No se encontraron credenciales.'
        return handleHttpSuccess(res, message, 201, tenantCredentials)
    } catch (error) {
        console.log(error)
        return handleHttpError(res, '', 403, error)
    }
}

/**
 * Este controlador es el encargado de obtener una credencial de tenant por guid
 * @param {*} req
 * @param {*} res
 */
const TenantCredential = async (req, res) => {
    try {
        const { guid } = req.params
        const dataTenantCredential = await TenantCredentialsModel.findOne({
            where: { guid },
        })
        if (!dataTenantCredential) {
            return handleHttpError(
                res,
                'La credencial de tenant no existe.',
                404
            )
        }
        return handleHttpSuccess(
            res,
            'Se han encontrado los datos.',
            201,
            dataTenantCredential
        )
    } catch (error) {
        console.log(error)
        return handleHttpError(res, '', 403, error)
    }
}

/**
 * Este controlador es el encargado de actualizar credenciales de tenant
 * @param {*} req
 * @param {*} res
 */
const UpdateTenantCredential = async (req, res) => {
    try {
        const { guid } = req.params
        const updated_by = req.user.guid
        const updated_at = new Date()
        const data = matchedData(req)
        data.updated_by = updated_by
        data.updated_at = updated_at
        const [updateCount] = await TenantCredentialsModel.update(data, {
            where: { guid },
        })
        if (updateCount === 0) {
            return handleHttpError(
                res,
                'La credencial de tenant no existe.',
                404
            )
        }
        const updatedTenantCredential = await TenantCredentialsModel.findOne({
            where: { guid },
        })
        if (!updatedTenantCredential) {
            return handleHttpError(
                res,
                'Error al obtener la credencial actualizada.',
                500
            )
        }
        return handleHttpSuccess(
            res,
            'Se ha actualizado la credencial de tenant.',
            201,
            updatedTenantCredential
        )
    } catch (error) {
        console.log(error)
        return handleHttpError(res, '', 403, error)
    }
}

/**
 * Este controlador es el encargado de eliminar credenciales de tenant
 * @param {*} req
 * @param {*} res
 */
const DeleteTenantCredential = async (req, res) => {
    try {
        const { guid } = req.params
        const deleted_by = req.user.guid
        const deleted_at = new Date()
        const [updateCount] = await TenantCredentialsModel.update(
            {
                is_active: 0,
                deleted_by,
                deleted_at,
            },
            {
                where: { guid },
            }
        )
        if (updateCount === 0) {
            return handleHttpError(
                res,
                'La credencial de tenant no existe.',
                404
            )
        }
        handleHttpSuccess(res, 'Se ha eliminado la credencial de tenant.', 201)
    } catch (error) {
        console.log(error)
        handleHttpError(res, '', 403, error)
    }
}

/**
 * Este controlador es el encargado de desactivar credenciales de tenant
 * @param {*} req
 * @param {*} res
 */
const DisableTenantCredential = async (req, res) => {
    try {
        const { guid } = req.params
        const updated_by = req.user.guid
        const updated_at = new Date()
        const [updateCount] = await TenantCredentialsModel.update(
            {
                is_active: 0,
                updated_by,
                updated_at,
            },
            {
                where: { guid },
            }
        )
        if (updateCount === 0) {
            return handleHttpError(
                res,
                'La credencial de tenant no existe.',
                404
            )
        }
        handleHttpSuccess(
            res,
            'Se ha desactivado la credencial de tenant.',
            201
        )
    } catch (error) {
        console.log(error)
        handleHttpError(res, '', 403, error)
    }
}

/**
 * Este controlador es el encargado de activar credenciales de tenant
 * @param {*} req
 * @param {*} res
 */
const EnableTenantCredential = async (req, res) => {
    try {
        const { guid } = req.params
        const updated_by = req.user.guid
        const updated_at = new Date()
        const [updateCount] = await TenantCredentialsModel.update(
            {
                is_active: 1,
                updated_by,
                updated_at,
            },
            {
                where: { guid },
            }
        )
        if (updateCount === 0) {
            return handleHttpError(
                res,
                'La credencial de tenant no existe.',
                404
            )
        }
        handleHttpSuccess(res, 'Se ha activado la credencial de tenant.', 201)
    } catch (error) {
        console.log(error)
        handleHttpError(res, '', 403, error)
    }
}

/**
 * Este controlador es el encargado de obtener todas las credenciales de tenant
 * @param {*} req
 * @param {*} res
 */
const TenantCredentialsAll = async (req, res) => {
    try {
        const dataTenantCredentials = await TenantCredentialsModel.findAll()
        if (!dataTenantCredentials) {
            handleHttpError(res, 'No se encontraron datos.', 404)
            return
        }
        const message = dataTenantCredentials.length
            ? 'Se han encontrado las credenciales.'
            : 'No se encontraron credenciales.'
        handleHttpSuccess(res, message, 201, dataTenantCredentials)
    } catch (error) {
        console.log(error)
        handleHttpError(res, '', 403, error)
    }
}

/**
 * Este controlador es el encargado de contar todas las credenciales de tenant
 * @param {*} req
 * @param {*} res
 */
const CountTenantCredentials = async (req, res) => {
    try {
        const dataTenantCredentials = await TenantCredentialsModel.count()
        if (!dataTenantCredentials) {
            handleHttpError(res, 'No se encontraron datos.', 404)
            return
        }
        handleHttpCount(
            res,
            'Se han encontrado los datos.',
            201,
            dataTenantCredentials
        )
    } catch (error) {
        console.log(error)
        handleHttpError(res, '', 403, error)
    }
}

/**
 * Este controlador es el encargado de ejecutar los seeders mockup para un tenant
 * @param {*} req
 * @param {*} res
 */
const RunSeedersTenant = async (req, res) => {
    try {
        const data = matchedData(req) // Obtener los datos validados del request.

        // obtenemos las credenciales del tenant
        const tenant = await TenantCredentialsModel.findOne({
            where: { tenant_guid: data.tenant_guid },
        })

        // Si no existe el tenant, retornamos un error
        if (!tenant) {
            return handleHttpError(res, 'El tenant no existe.', 404)
        }

        // Si el tenant no está activo, retornamos un error
        if (!tenant.is_active) {
            return handleHttpError(res, 'El tenant no está activo.', 404)
        }

        // ejecutamos los seeders pasando el nombre de la base de datos del tenant
        await runSeedersMockup(tenant.database)

        handleHttpSuccess(res, 'Seeders ejecutados correctamente.', 201)
    } catch (error) {
        console.log(error)
        handleHttpError(res, '', 403, error)
    }
}

module.exports = {
    CreateTenantCredential,
    TenantCredentials,
    TenantCredential,
    UpdateTenantCredential,
    DeleteTenantCredential,
    DisableTenantCredential,
    EnableTenantCredential,
    TenantCredentialsAll,
    CountTenantCredentials,
    RunSeedersTenant,
}
