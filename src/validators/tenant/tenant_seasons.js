const { check, validationResult } = require('express-validator')

// Validación para crear una nueva temporada
const validatorCreateSeason = () => {
    return [
        async (req, res, next) => {
            await check('name')
                .notEmpty()
                .withMessage('El nombre de la temporada es requerido.')
                .run(req)
            await check('description')
                .optional()
                .isString()
                .withMessage('La descripción debe ser una cadena de texto.')
                .run(req)
            await check('start_date')
                .notEmpty()
                .withMessage('La fecha de inicio es requerida.')
                .isISO8601()
                .withMessage('La fecha de inicio debe ser válida.')
                .run(req)
            await check('end_date')
                .notEmpty()
                .withMessage('La fecha de finalización es requerida.')
                .isISO8601()
                .withMessage('La fecha de finalización debe ser válida.')
                .custom((value, { req }) => {
                    if (new Date(value) <= new Date(req.body.start_date)) {
                        throw new Error(
                            'La fecha de finalización debe ser posterior a la fecha de inicio.'
                        )
                    }
                    return true
                })
                .run(req)

            const errors = validationResult(req)
            if (!errors.isEmpty()) {
                return res.status(400).json({ errors: errors.array() })
            }

            next()
        },
    ]
}

// Validación para obtener una temporada por GUID
const validatorSeason = () => {
    return [
        async (req, res, next) => {
            await check('guid')
                .isUUID()
                .withMessage('El guid de la temporada es requerido.')
                .run(req)

            const errors = validationResult(req)
            if (!errors.isEmpty()) {
                return res.status(400).json({ errors: errors.array() })
            }

            next()
        },
    ]
}

// Validación para actualizar una temporada
const validatorUpdateSeason = () => {
    return [
        async (req, res, next) => {
            await check('guid')
                .isUUID()
                .withMessage('El guid de la temporada es requerido.')
                .run(req)
            await check('name')
                .optional()
                .notEmpty()
                .withMessage('El nombre de la temporada es requerido.')
                .run(req)
            await check('description')
                .optional()
                .isString()
                .withMessage('La descripción debe ser una cadena de texto.')
                .run(req)
            await check('start_date')
                .optional()
                .isISO8601()
                .withMessage('La fecha de inicio debe ser válida.')
                .run(req)
            await check('end_date')
                .optional()
                .isISO8601()
                .withMessage('La fecha de finalización debe ser válida.')
                .custom((value, { req }) => {
                    if (new Date(value) <= new Date(req.body.start_date)) {
                        throw new Error(
                            'La fecha de finalización debe ser posterior a la fecha de inicio.'
                        )
                    }
                    return true
                })
                .run(req)

            const errors = validationResult(req)
            if (!errors.isEmpty()) {
                return res.status(400).json({ errors: errors.array() })
            }

            next()
        },
    ]
}

// Validación para eliminar una temporada
const validatorDeleteSeason = () => {
    return [
        async (req, res, next) => {
            await check('guid')
                .isUUID()
                .withMessage('El guid de la temporada es requerido.')
                .run(req)

            const errors = validationResult(req)
            if (!errors.isEmpty()) {
                return res.status(400).json({ errors: errors.array() })
            }

            next()
        },
    ]
}

// Validación para deshabilitar una temporada
const validatorDisableSeason = () => {
    return [
        async (req, res, next) => {
            await check('guid')
                .isUUID()
                .withMessage('El guid de la temporada es requerido.')
                .run(req)

            const errors = validationResult(req)
            if (!errors.isEmpty()) {
                return res.status(400).json({ errors: errors.array() })
            }

            next()
        },
    ]
}

// Validación para habilitar una temporada
const validatorEnableSeason = () => {
    return [
        async (req, res, next) => {
            await check('guid')
                .isUUID()
                .withMessage('El guid de la temporada es requerido.')
                .run(req)

            const errors = validationResult(req)
            if (!errors.isEmpty()) {
                return res.status(400).json({ errors: errors.array() })
            }

            next()
        },
    ]
}

module.exports = {
    validatorCreateSeason,
    validatorSeason,
    validatorUpdateSeason,
    validatorDeleteSeason,
    validatorDisableSeason,
    validatorEnableSeason,
}
