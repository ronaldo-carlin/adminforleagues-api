// Importar Sequelize
const { DataTypes } = require('sequelize')
const { format } = require('sequelize/lib/utils')

// Definir la función factory para el modelo SponsorsTenantModel
const SponsorsTenantModelFactory = (sequelize) => {
    const SponsorsTenantModel = sequelize.define(
        'sponsors',
        {
            id: {
                type: DataTypes.INTEGER,
                primaryKey: true,
                autoIncrement: true,
            },
            guid: {
                type: DataTypes.UUID,
                primaryKey: true,
                defaultValue: DataTypes.UUIDV4,
                unique: true,
            },
            competition_guid: {
                type: DataTypes.TEXT,
            },
            team_guid: {
                type: DataTypes.TEXT,
            },
            name: {
                type: DataTypes.TEXT,
            },
            logo_path: {
                type: DataTypes.TEXT,
            },
            start_date: {
                type: DataTypes.DATE,
            },
            end_date: {
                type: DataTypes.DATE,
            },
            format: {
                type: DataTypes.TEXT,
            },
            site_web: {
                type: DataTypes.TEXT,
            },
            phone: {
                type: DataTypes.TEXT,
            },
            description: {
                type: DataTypes.TEXT,
            },
            is_active: {
                type: DataTypes.INTEGER,
            },
            created_by: {
                type: DataTypes.UUID,
            },
            updated_by: {
                type: DataTypes.UUID,
            },
            deleted_by: {
                type: DataTypes.UUID,
            },
            created_at: {
                type: DataTypes.DATE,
            },
            updated_at: {
                type: DataTypes.DATE,
            },
            deleted_at: {
                type: DataTypes.DATE,
            },
        },
        {
            tableName: 'sponsors',
            timestamps: false,
            defaultScope: {
                attributes: {
                    exclude: [
                        'created_by',
                        'updated_by',
                        'deleted_by',
                        'created_at',
                        'updated_at',
                        'deleted_at',
                    ],
                },
            },
        }
    )

    return SponsorsTenantModel
}

module.exports = SponsorsTenantModelFactory
