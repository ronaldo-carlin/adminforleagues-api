// Importar Sequelize
const { DataTypes } = require('sequelize')

// Definir la función factory para el modelo RefereeMatchesTenantModel
const RefereeMatchesTenantModelFactory = (sequelize) => {
    const RefereeMatchesTenantModel = sequelize.define(
        'referee_matches',
        {
            id: {
                type: DataTypes.INTEGER,
                primaryKey: true,
                autoIncrement: true,
            },
            guid: {
                type: DataTypes.UUID,
                primaryKey: true,
                defaultValue: DataTypes.UUIDV4,
                unique: true,
            },
            user_guid: {
                type: DataTypes.TEXT,
            },
            match_guid: {
                type: DataTypes.TEXT,
            },
            is_active: {
                type: DataTypes.INTEGER,
                defaultValue: 1,
            },
            created_by: {
                type: DataTypes.INTEGER,
            },
            updated_by: {
                type: DataTypes.INTEGER,
            },
            deleted_by: {
                type: DataTypes.INTEGER,
            },
            created_at: {
                type: DataTypes.DATE,
            },
            updated_at: {
                type: DataTypes.DATE,
            },
            deleted_at: {
                type: DataTypes.DATE,
            },
        },
        {
            tableName: 'referee_matches',
            timestamps: false,
        }
    )

    return RefereeMatchesTenantModel
}

module.exports = RefereeMatchesTenantModelFactory
