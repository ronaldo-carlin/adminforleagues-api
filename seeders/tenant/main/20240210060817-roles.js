const GUIDsTenant = require('../../guids_tenant')

module.exports = {
    async up(queryInterface, Sequelize) {
        // Inserta un usuario por defecto
        await queryInterface.bulkInsert(
            'roles',
            [
                {
                    guid: GUIDsTenant.roleSuperAdminGuid,
                    name: 'Super Administrador',
                    is_active: 1, // 1: Activo, 0: Inactivo
                    created_at: new Date(),
                },
                {
                    guid: GUIDsTenant.roleAdminLigaGuid,
                    name: 'Administrador de liga',
                    is_active: 1, // 1: Activo, 0: Inactivo
                    created_at: new Date(),
                },
                {
                    guid: GUIDsTenant.roleEntrenadorGuid,
                    name: 'Entrenador/Dueño del equipo',
                    is_active: 1, // 1: Activo, 0: Inactivo
                    created_at: new Date(),
                },
                {
                    guid: GUIDsTenant.roleArbitroGuid,
                    name: 'Arbitro',
                    is_active: 1, // 1: Activo, 0: Inactivo
                    created_at: new Date(),
                },
            ],
            {}
        )
    },

    async down(queryInterface, Sequelize) {
        await queryInterface.bulkDelete('roles', null, {})
    },
}
