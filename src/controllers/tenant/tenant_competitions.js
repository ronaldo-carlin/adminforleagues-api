const { Op } = require('sequelize')
const { v4: uuidv4 } = require('uuid')
const { matchedData } = require('express-validator')
const { handleHttpSuccess } = require('../../utils/handleSuccess')
const { handleHttpError } = require('../../utils/handleError')
const { handleHttpCount } = require('../../utils/handleCount')
const {
    getSequelize,
    getCompetitionsModel,
} = require('../../helpers/sequelizeHelper')
const PUBLIC_URL = process.env.PUBLIC_URL + '/tenant_competitions'

/**
 * Este controlador es el encargado de registrar una competencia
 * @param {*} req
 * @param {*} res
 */
const CreateCompetition = async (req, res) => {
    try {
        const sequelize = getSequelize(req)
        const CompetitionModel = getCompetitionsModel(sequelize)
        const data = matchedData(req)
        const created_by = req.user.guid
        const created_at = new Date()
        const guid = await uuidv4()
        data.created_by = created_by
        data.created_at = created_at
        data.guid = guid

        const competition = await CompetitionModel.create(data)
        return handleHttpSuccess(res, 'Competencia creada.', 201, competition)
    } catch (error) {
        console.log('Error al crear la competencia:', error)
        handleHttpError(res, 'Error al crear la competencia.', 403, error)
    }
}

/**
 * Este controlador es el encargado de obtener las competencias activas
 * @param {*} req
 * @param {*} res
 */
const Competitions = async (req, res) => {
    try {
        const sequelize = getSequelize(req)
        const CompetitionModel = getCompetitionsModel(sequelize)

        const competitions = await CompetitionModel.findAll({
            where: { is_active: 1 },
        })

        if (!competitions.length) {
            handleHttpError(res, 'No se encontraron competencias.', 404)
            return
        }

        handleHttpSuccess(res, 'Competencias encontradas.', 201, competitions)
    } catch (error) {
        console.log('Error obteniendo competencias:', error)
        handleHttpError(res, 'Error obteniendo competencias.', 403, error)
    }
}

/**
 * Este controlador es el encargado de obtener una competencia por GUID
 * @param {*} req
 * @param {*} res
 */
const Competition = async (req, res) => {
    try {
        const sequelize = getSequelize(req)
        const CompetitionModel = getCompetitionsModel(sequelize)
        const { guid } = req.params

        const competition = await CompetitionModel.findOne({
            where: { guid },
        })

        if (!competition) {
            handleHttpError(res, 'La competencia no existe.', 404)
            return
        }

        if (competition.is_active == 0) {
            handleHttpError(res, 'La competencia ha sido deshabilitada.', 401)
            return
        }

        handleHttpSuccess(res, 'Competencia encontrada.', 201, competition)
    } catch (error) {
        console.log('Error obteniendo competencia:', error)
        handleHttpError(res, 'Error obteniendo competencia.', 403, error)
    }
}

/**
 * Este controlador es el encargado de actualizar una competencia
 * @param {*} req
 * @param {*} res
 */
const UpdateCompetition = async (req, res) => {
    try {
        const sequelize = getSequelize(req)
        const CompetitionModel = getCompetitionsModel(sequelize)
        const { guid } = req.params
        const data = matchedData(req)
        const updated_by = req.user.guid
        const updated_at = new Date()
        data.updated_by = updated_by
        data.updated_at = updated_at

        const [updateCount] = await CompetitionModel.update(data, {
            where: { guid },
        })

        if (updateCount === 0) {
            return handleHttpError(res, 'La competencia no existe.', 404)
        }

        const competition = await CompetitionModel.findOne({
            where: { guid },
        })

        if (!competition) {
            handleHttpError(res, 'La competencia no existe.', 404)
            return
        }

        if (competition.is_active == 0) {
            handleHttpError(res, 'La competencia ha sido deshabilitada.', 401)
            return
        }

        handleHttpSuccess(res, 'Competencia actualizada.', 201, competition)
    } catch (error) {
        console.log('Error actualizando competencia:', error)
        handleHttpError(res, 'Error actualizando competencia.', 403, error)
    }
}

/**
 * Este controlador es el encargado de eliminar una competencia
 * @param {*} req
 * @param {*} res
 */
const DeleteCompetition = async (req, res) => {
    try {
        const sequelize = getSequelize(req)
        const CompetitionModel = getCompetitionsModel(sequelize)
        const { guid } = req.params
        const deleted_by = req.user.guid
        const deleted_at = new Date()
        const [updateCount] = await CompetitionModel.update(
            {
                is_active: 0,
                deleted_by,
                deleted_at,
            },
            {
                where: { guid },
            }
        )

        if (updateCount === 0) {
            return handleHttpError(res, 'La competencia no existe.', 404)
        }

        handleHttpSuccess(res, 'Competencia eliminada.', 201)
    } catch (error) {
        console.log('Error eliminando competencia:', error)
        handleHttpError(res, 'Error eliminando competencia.', 403, error)
    }
}

/**
 * Este controlador es el encargado de desactivar una competencia
 * @param {*} req
 * @param {*} res
 */
const DisableCompetition = async (req, res) => {
    try {
        const sequelize = getSequelize(req)
        const CompetitionModel = getCompetitionsModel(sequelize)
        const { guid } = req.params
        const updated_by = req.user.guid
        const updated_at = new Date()
        const [updateCount] = await CompetitionModel.update(
            {
                is_active: 0,
                updated_by,
                updated_at,
            },
            {
                where: { guid },
            }
        )

        if (updateCount === 0) {
            return handleHttpError(res, 'La competencia no existe.', 404)
        }

        handleHttpSuccess(res, 'Competencia deshabilitada.', 201)
    } catch (error) {
        console.log('Error deshabilitando competencia:', error)
        handleHttpError(res, 'Error deshabilitando competencia.', 403, error)
    }
}

/**
 * Este controlador es el encargado de activar una competencia
 * @param {*} req
 * @param {*} res
 */
const EnableCompetition = async (req, res) => {
    try {
        const sequelize = getSequelize(req)
        const CompetitionModel = getCompetitionsModel(sequelize)
        const { guid } = req.params
        const updated_by = req.user.guid
        const updated_at = new Date()
        const [updateCount] = await CompetitionModel.update(
            {
                is_active: 1,
                updated_by,
                updated_at,
            },
            {
                where: { guid },
            }
        )

        if (updateCount === 0) {
            return handleHttpError(res, 'La competencia no existe.', 404)
        }

        handleHttpSuccess(res, 'Competencia habilitada.', 201)
    } catch (error) {
        console.log('Error habilitando competencia:', error)
        handleHttpError(res, 'Error habilitando competencia.', 403, error)
    }
}

/**
 * Este controlador es el encargado de obtener todas las competencias
 * @param {*} req
 * @param {*} res
 */
const CompetitionsAll = async (req, res) => {
    try {
        const sequelize = getSequelize(req)
        const CompetitionModel = getCompetitionsModel(sequelize)

        const competitions = await CompetitionModel.findAll()

        if (!competitions.length) {
            handleHttpError(res, 'No se encontraron competencias.', 404)
            return
        }

        handleHttpSuccess(res, 'Competencias encontradas.', 201, competitions)
    } catch (error) {
        console.log('Error obteniendo competencias:', error)
        handleHttpError(res, 'Error obteniendo competencias.', 403, error)
    }
}

/**
 * Este controlador es el encargado de contar todas las competencias
 * @param {*} req
 * @param {*} res
 */
const CompetitionsCount = async (req, res) => {
    try {
        const sequelize = getSequelize(req)
        const CompetitionModel = getCompetitionsModel(sequelize)

        const count = await CompetitionModel.count()

        handleHttpCount(res, 'Competencias encontradas.', 201, count)
    } catch (error) {
        console.log('Error contando competencias:', error)
        handleHttpError(res, 'Error contando competencias.', 403, error)
    }
}

/**
 * Este controlador es el encargado de subir una imagen de perfil de la competicion
 * @param {*} req
 * @param {*} res
 */
const UploadProfilePicture = async (req, res) => {
    try {
        // obtenemos la instancia de sequelize del middleware
        const sequelize = getSequelize(req)
        // obtenemos el modelo de usuario
        const CompetitionModel = getCompetitionsModel(sequelize)

        // Verificar si se recibió la imagen en la solicitud
        if (!req.file) {
            return handleHttpError(
                res,
                'No se ha recibido ninguna imagen.',
                400
            )
        }

        // Ruta donde se guardará la imagen en el almacenamiento
        const logoPath = req.file.path

        // Obtener el ID del usuario del token de autenticación
        const guid = req.params.guid

        // Actualizar el campo logo_path del usuario en la base de datos
        /* await CompetitionModel.update(
            {
                logo_path: `${PUBLIC_URL}/images/${req.file.filename}`,
            },
            {
                where: { guid: guid },
                returning: true,
            }
        ) */

        // Preparar la respuesta con los datos de la imagen
        const responseData = {
            filename: req.file.filename,
            logo_path: logoPath,
            url: `${PUBLIC_URL}/images/${req.file.filename}`,
        }

        handleHttpSuccess(res, 'Imagen subida.', 201, responseData)
    } catch (error) {
        console.log('🚀 ~ uploadProfilePicture ~ error:', error)
        //Obtener y devolver el error
        handleHttpError(res, 'Error al guardar la imagen.', 403)
    }
}

module.exports = {
    CreateCompetition,
    Competitions,
    Competition,
    UpdateCompetition,
    DeleteCompetition,
    DisableCompetition,
    EnableCompetition,
    CompetitionsAll,
    CompetitionsCount,
    UploadProfilePicture,
}
