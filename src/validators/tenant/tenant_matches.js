const { check, validationResult } = require('express-validator')
const {
    getSequelize,
    getUserModel,
    getRolesModel,
} = require('../../helpers/sequelizeHelper')

// Validación para crear un nuevo partido (match)
const validatorCreateMatch = () => {
    return [
        async (req, res, next) => {
            const sequelize = getSequelize(req)
            const AuthTenantModel = getUserModel(sequelize)
            const RolesModel = getRolesModel(sequelize)

            await check('Date')
                .optional()
                .isISO8601()
                .withMessage(
                    'La fecha debe ser una fecha válida en formato ISO8601.'
                )
                .custom((value) => {
                    if (new Date(value) < new Date()) {
                        throw new Error(
                            'La fecha debe ser mayor a la fecha actual.'
                        )
                    }
                    return true
                })
                .run(req)
            await check('Time')
                .optional()
                .matches(/^([0-1]\d|2[0-3]):([0-5]\d):([0-5]\d)$/)
                .withMessage(
                    'El tiempo debe ser un valor válido en formato HH:MM:SS.'
                )
                .run(req)
            await check('home_team_guid')
                .optional()
                .isUUID()
                .withMessage(
                    'El GUID del equipo local debe ser un UUID válido.'
                )
                .run(req)
            await check('away_team_guid')
                .optional()
                .isUUID()
                .withMessage(
                    'El GUID del equipo visitante debe ser un UUID válido.'
                )
                .custom((value, { req }) => {
                    if (value === req.body.home_team_guid) {
                        throw new Error(
                            'El equipo visitante no puede ser igual al equipo local.'
                        )
                    }
                    return true
                })
                .run(req)
            await check('competition_guid')
                .optional()
                .isUUID()
                .withMessage(
                    'El GUID de la competencia debe ser un UUID válido.'
                )
                .run(req)
            await check('group')
                .optional()
                .isString()
                .withMessage('El grupo debe ser una cadena de texto.')
                .run(req)
            await check('sede_guid')
                .optional()
                .isUUID()
                .withMessage('El GUID de la sede debe ser un UUID válido.')
                .run(req)
            await check('referee_guid')
                .optional()
                .isUUID()
                .withMessage('El GUID del árbitro debe ser un UUID válido.')
                .custom(async (value, { req }) => {
                    const user = await AuthTenantModel.findOne({
                        where: { guid: value },
                    })
                    if (!user) {
                        throw new Error('El árbitro no existe.')
                    }
                    const role = await RolesModel.findOne({
                        where: { guid: user.role_guid },
                    })
                    if (role.name !== 'Arbitro') {
                        throw new Error('El usuario no es un árbitro.')
                    }
                    return true
                })
                .run(req)

            const errors = validationResult(req)
            if (!errors.isEmpty()) {
                return res.status(400).json({ errors: errors.array() })
            }

            next()
        },
    ]
}

// Validación para obtener un partido (match) por GUID
const validatorMatch = () => {
    return [
        async (req, res, next) => {
            await check('guid')
                .isUUID()
                .withMessage(
                    'El GUID del partido es requerido y debe ser un UUID válido.'
                )
                .run(req)

            const errors = validationResult(req)
            if (!errors.isEmpty()) {
                return res.status(400).json({ errors: errors.array() })
            }

            next()
        },
    ]
}

// Validación para actualizar un partido (match)
const validatorUpdateMatch = () => {
    return [
        async (req, res, next) => {
            const sequelize = getSequelize(req)
            const AuthTenantModel = getUserModel(sequelize)
            const RolesModel = getRolesModel(sequelize)

            await check('guid')
                .isUUID()
                .withMessage(
                    'El GUID del partido es requerido y debe ser un UUID válido.'
                )
                .custom((value) => {
                    if (new Date(value) < new Date()) {
                        throw new Error(
                            'La fecha debe ser mayor a la fecha actual.'
                        )
                    }
                    return true
                })
                .run(req)
            await check('Date')
                .optional()
                .isISO8601()
                .withMessage(
                    'La fecha debe ser una fecha válida en formato ISO8601.'
                )
                .run(req)
            await check('Time')
                .optional()
                .matches(/^([0-1]\d|2[0-3]):([0-5]\d):([0-5]\d)$/)
                .withMessage(
                    'El tiempo debe ser un valor válido en formato HH:MM:SS.'
                )
                .run(req)
            await check('home_team_guid')
                .optional()
                .isUUID()
                .withMessage(
                    'El GUID del equipo local debe ser un UUID válido.'
                )
                .run(req)
            await check('away_team_guid')
                .optional()
                .isUUID()
                .withMessage(
                    'El GUID del equipo visitante debe ser un UUID válido.'
                )
                .custom((value, { req }) => {
                    if (value === req.body.home_team_guid) {
                        throw new Error(
                            'El equipo visitante no puede ser igual al equipo local.'
                        )
                    }
                    return true
                })
                .run(req)
            await check('competition_guid')
                .optional()
                .isUUID()
                .withMessage(
                    'El GUID de la competencia debe ser un UUID válido.'
                )
                .run(req)
            await check('group')
                .optional()
                .isString()
                .withMessage('El grupo debe ser una cadena de texto.')
                .run(req)
            await check('sede_guid')
                .optional()
                .isUUID()
                .withMessage('El GUID de la sede debe ser un UUID válido.')
                .run(req)
            await check('referee_guid')
                .optional()
                .isUUID()
                .withMessage('El GUID del árbitro debe ser un UUID válido.')
                .custom(async (value, { req }) => {
                    const user = await AuthTenantModel.findOne({
                        where: { guid: value },
                    })
                    if (!user) {
                        throw new Error('El árbitro no existe.')
                    }
                    const role = await RolesModel.findOne({
                        where: { guid: user.role_guid },
                    })
                    if (role.name !== 'Arbitro') {
                        throw new Error('El usuario no es un árbitro.')
                    }
                    return true
                })
                .run(req)

            const errors = validationResult(req)
            if (!errors.isEmpty()) {
                return res.status(400).json({ errors: errors.array() })
            }

            next()
        },
    ]
}

// Validación para eliminar un partido (match)
const validatorDeleteMatch = () => {
    return [
        async (req, res, next) => {
            await check('guid')
                .isUUID()
                .withMessage(
                    'El GUID del partido es requerido y debe ser un UUID válido.'
                )
                .run(req)

            const errors = validationResult(req)
            if (!errors.isEmpty()) {
                return res.status(400).json({ errors: errors.array() })
            }

            next()
        },
    ]
}

// Validación para deshabilitar un partido (match)
const validatorDisableMatch = () => {
    return [
        async (req, res, next) => {
            await check('guid')
                .isUUID()
                .withMessage(
                    'El GUID del partido es requerido y debe ser un UUID válido.'
                )
                .run(req)

            const errors = validationResult(req)
            if (!errors.isEmpty()) {
                return res.status(400).json({ errors: errors.array() })
            }

            next()
        },
    ]
}

// Validación para habilitar un partido (match)
const validatorEnableMatch = () => {
    return [
        async (req, res, next) => {
            await check('guid')
                .isUUID()
                .withMessage(
                    'El GUID del partido es requerido y debe ser un UUID válido.'
                )
                .run(req)

            const errors = validationResult(req)
            if (!errors.isEmpty()) {
                return res.status(400).json({ errors: errors.array() })
            }

            next()
        },
    ]
}

module.exports = {
    validatorCreateMatch,
    validatorMatch,
    validatorUpdateMatch,
    validatorDeleteMatch,
    validatorDisableMatch,
    validatorEnableMatch,
}
