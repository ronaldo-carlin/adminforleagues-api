// Importar Sequelize directamente
const { Sequelize } = require('sequelize')
// Importar tu configuración de sequelize
const { sequelize } = require('../../database/mysql')

const ModulesModel = sequelize.define(
    'modules',
    {
        id: {
            type: Sequelize.INTEGER,
            primaryKey: true,
            autoIncrement: true,
        },
        guid: {
            type: Sequelize.UUID,
            defaultValue: Sequelize.UUIDV4,
            unique: true,
        },
        name: {
            type: Sequelize.STRING,
        },
        description: {
            type: Sequelize.STRING,
        },
        to: {
            type: Sequelize.STRING,
        },
        icon: {
            type: Sequelize.STRING,
        },
        is_active: {
            type: Sequelize.INTEGER,
        },
        is_parent: {
            type: Sequelize.INTEGER,
        },
        parent_guid: {
            type: Sequelize.UUID,
        },
        is_active: {
            type: Sequelize.BOOLEAN,
        },
        created_by: {
            type: Sequelize.UUID,
        },
        updated_by: {
            type: Sequelize.UUID,
        },
        deleted_by: {
            type: Sequelize.UUID,
        },
        created_at: {
            type: Sequelize.DATE,
        },
        updated_at: {
            type: Sequelize.DATE,
        },
        deleted_at: {
            type: Sequelize.DATE,
        },
    },
    {
        tableName: 'modules',
        timestamps: false,
        defaultScope: {
            attributes: {
                exclude: [
                    'created_by',
                    'updated_by',
                    'deleted_by',
                    'created_at',
                    'updated_at',
                    'deleted_at',
                ],
            },
        },
    }
)

// Configurar las asociaciones después de definir el modelo
ModulesModel.belongsTo(ModulesModel, {
    foreignKey: 'parent_guid',
    targetKey: 'guid',
    as: 'parent',
})

module.exports = ModulesModel
