const { Op } = require('sequelize')
const { matchedData } = require('express-validator')
const fs = require('fs')
const ejs = require('ejs')
const EMAIL_TEMPLATE = __dirname + '../../html/resetPsw.html'
const { sendEmail } = require('../../helpers/mailer')
const { OAuth2Client } = require('google-auth-library')
const client = new OAuth2Client(process.env.GOOGLE_CLIENT_ID)
const { v4: uuidv4 } = require('uuid')
const AuthModel = require('../../models/general/auth')
const Token = require('../../models/general/token')
const { handleHttpSuccess } = require('../../utils/handleSuccess')
const { handleHttpError } = require('../../utils/handleError')
const { encrypt, compare } = require('../../utils/handlePassword')
const {
    tokenSign,
    verifyAccessToken,
    verifyRefreshToken,
    resetPasswordToken,
} = require('../../utils/handleJwt')
const RolesModel = require('../../models/general/roles')

/**
 * Este controlador es el encargado de registrar un cliente
 * @param {*} req
 * @param {*} res
 */
const Register = async (req, res) => {
    try {
        //Obtenemos y limpiamos el body del request
        const body = matchedData(req)
        //Verificamos si el cliente ya existe en la base de datos
        const user = await AuthModel.findOne({
            where: {
                [Op.or]: [
                    {
                        username: body.username,
                    },
                    {
                        email: body.email,
                    },
                    {
                        phone: body.phone,
                    },
                ],
            },
        })
        //Si el cliente ya existe enviamos una respuesta de error
        if (user) {
            handleHttpError(res, 'El cliente ya existe.', 403)
            return
        }
        //Encriptamos la contraseña
        const encryptedPassword = await encrypt(body.password)
        //Creamos un guid para el cliente
        const guid = await uuidv4()
        //Creamos el cliente en la base de datos
        await AuthModel.create({
            guid: guid,
            role_guid: body.role_guid,
            first_name: body.first_name,
            last_name: body.last_name,
            username: body.username,
            email: body.email,
            phone: body.phone,
            country: body.country,
            state: body.state,
            city: body.city,
            password: encryptedPassword,
            isVerified: 0,
            is_active: 1,
            created_at: new Date(),
        })
        //Enviamos una respuesta de éxito
        handleHttpSuccess(res, 'Cliente creado.', 201)
    } catch (error) {
        console.log('error register:', error)
        //Obtener y devolver el error
        handleHttpError(res, 'Error al crear el cliente.', 403, error)
    }
}

/**
 * Este controlador es el encargado de realizar el login del cliente
 * @param {*} req
 * @param {*} res
 */
const LoginUser = async (req, res) => {
    try {
        //Obtenemos y limpiamos el body del request
        const body = matchedData(req)
        //Buscamos el cliente en la base de datos
        const user = await AuthModel.findOne({
            where: {
                [Op.or]: [
                    {
                        username: body.user.toLowerCase(),
                    },
                    {
                        email: body.user.toLowerCase(),
                    },
                    {
                        phone: body.user.toLowerCase(),
                    },
                ],
            },
        })
        //Si el cliente no existe enviamos una respuesta de error
        if (!user) {
            console.log('🚀 ~ loginUser ~ user:', user)
            handleHttpError(res, 'El cliente no existe.', 404, '')
            return
        }
        //Si el cliente esta inactivo enviamos una respuesta de error
        if (user.active == false) {
            handleHttpError(res, 'El cliente ha sido deshabilitado.', 401, '')
            return
        }
        //Ciframos la contraseña para compararla con la de la base de datos
        const hashPassword = user.get('password')
        //Comparamos la contraseña
        const check = await compare(body.password, hashPassword)
        //Si la contraseña es incorrecta enviamos una respuesta de error
        if (!check) {
            handleHttpSuccess(res, 'Datos incorrectos.', 401)
            return
        }
        //Si se logra logear se envía un objeto con el token y el refreshtoken y los datos del cliente
        const accessToken = await tokenSign(user, true)
        const refreshToken = await tokenSign(user, false)
        const expirationDate = new Date()
        expirationDate.setDate(expirationDate.getDate() + 2) // Token válido por 2 días

        await SaveRefreshToken(
            user.guid,
            refreshToken,
            expirationDate,
            req.headers['User-Agent']
        )

        // quitamos la contraseña del objeto user
        user.password = undefined

        const data = {
            token: accessToken,
            refreshToken: refreshToken,
            user,
        }

        //Si el cliente se puede logear actualizamos la ultima conexión
        const fechaActual = new Date()
        /* const opciones = { year: "numeric", month: "long", day: "numeric" };
    const fechaFormateada = fechaActual.toLocaleDateString("es-MX", opciones); */

        await AuthModel.update(
            { last_login: fechaActual },
            {
                where: {
                    guid: user.guid,
                },
            }
        )
        //Enviamos una respuesta de éxito con los datos
        handleHttpSuccess(res, '¡Bienvenido!', 201, data)
        // Enviamos el correo de inicio de sesión
        await sendEmail({
            to: user.email,
            subject: 'Inicio de sesión exitoso',
            html: `<p>Hola ${user.username}, se ha iniciado sesión en tu cuenta desde el siguiente dispositivo: ${req.headers['user-agent']}</p>`,
        })
    } catch (error) {
        //Obtener y devolver el error
        console.log('error login:', error)

        handleHttpError(res, 'Error login de cliente.', 403, error)
    }
}

/**
 * Este controlador es el encargado de recuperar la contraseña
 * @param {*} req
 * @param {*} res
 */
const ResetPassword = async (req, res) => {
    try {
        //Obtenemos el body del request
        req = req.body
        //Buscamos si el cliente existe
        const user = await AuthModel.findOne({
            where: {
                [Op.or]: [
                    {
                        email: req.user,
                    },
                ],
            },
        })
        //En caso de que no se haya encontrado un cliente
        if (!user) {
            return handleHttpError(res, 'El correo no existe.', 404)
        }
        // Genera un token de restablecimiento de contraseña
        const data = {
            token: await resetPasswordToken(user),
            user,
        }
        // Leemos el fichero de HTML que usaremos para el cuerpo del mensaje
        let html = fs.readFileSync(EMAIL_TEMPLATE, 'utf-8')
        console.log('user: ', user.dataValues)
        const context = {
            name: `${user.first_name} ${user.last_name}`,
            front_url: `${process.env.PUBLIC_FRONT_URL}`,
            action_url:
                `${process.env.PUBLIC_FRONT_URL}/ResetPassword/` + data.token,
        }
        // Renderizar el HTML con los datos
        const contenidoHTML = ejs.render(html, context)

        // Creamos las opciones del correo
        let mailOptions = {
            from: '"Admin For Leagues"' + process.env.MAIL_USER + '', // sender address
            to: req.user,
            subject: 'Solicitud de recuperación de contraseña',
            html: contenidoHTML,
        }

        // Configuramos el transported de nodemailer
        let transporter = nodemailer.createTransport({
            host: process.env.MAIL_HOST,
            port: process.env.MAIL_PORT,
            secure: false, // Cambiar a false para usar TLS en lugar de SSL
            auth: {
                user: process.env.MAIL_USER,
                pass: process.env.MAIL_PASSWORD,
            },
        })

        // Enviar correo electrónico
        transporter.sendMail(mailOptions, (error) => {
            if (error) {
                handleHttpError(
                    res,
                    'Ha ocurrido un error enviando el correo.',
                    403,
                    error
                )
                return console.log(error)
            }
            handleHttpSuccess(
                res,
                'Se enviará un correo con el link de recuperación de contraseña.',
                201
            )
        })
    } catch (error) {
        //Obtener y devolver el error
        console.log('Error ResetPassword', error)
        handleHttpError(res, 'Ha ocurrido un error', 403, error)
    }
}

/**
 * Este controlador es el encargado de revisar si el access token que usa el cliente es valido
 * @param {*} req
 * @param {*} res
 */
const CheckToken = async (req, res) => {
    try {
        let token = req.headers.authorization?.split(' ').pop() // Intenta obtener el token de la cabecera de autorización
        if (!token) {
            // Si no se encuentra en la cabecera de autorización, intenta obtenerlo de un parámetro de consulta llamado "token"
            token = req.body.token
        }

        if (!token) {
            // Si no se proporciona un token en la cabecera de autorización ni en la consulta, responde con un error.
            return handleHttpError(res, 'Token no proporcionado.', 403)
        }

        // Verifica el token
        const dataToken = await verifyAccessToken(token)

        if (!dataToken) {
            // Si el token no es válido, responde con un error.
            return handleHttpError(res, 'Token inválido.', 403)
        }

        // Obtiene el guid del cliente que viene en el token para consultar en la base de datos los datos del cliente
        // Busca los datos del cliente por su guid
        const user = await AuthModel.findOne({
            where: { guid: dataToken.guid },
        })

        const data = {
            user,
        }

        // Envía los datos del cliente
        handleHttpSuccess(res, 'Token válido', 200, data)
    } catch (error) {
        console.log(error)
        // Maneja los errores adecuadamente
        handleHttpError(res, 'Error al verificar el token.', 403, error)
    }
}

/**
 * Este controlador es el encargado de renovar el access token y el refresh token
 * @param {*} req
 * @param {*} res
 * @returns
 */
const RenewAccessToken = async (req, res) => {
    try {
        //Obtenemos el refresh token del body del request
        const refreshToken = req.body.refreshToken

        //Verificamos si el refresh token viene en el body y no esta vacio
        if (!refreshToken || refreshToken === '') {
            handleHttpError(res, 'Refresh token no proporcionado.', 403)
            return
        }

        //Verificamos si el refresh token es valido
        const decoded = await verifyRefreshToken(refreshToken)
        if (!decoded) {
            handleHttpError(res, 'Refresh token inválido.', 401)
            return
        }

        //Buscamos el refresh token en la base de datos
        const validToken = await FindValidRefreshToken(refreshToken)
        if (!validToken) {
            handleHttpError(
                res,
                'Refresh token no encontrado o ha expirado.',
                401
            )
            return
        }

        //Buscamos el cliente en la base de datos
        const user = await AuthModel.findOne({
            where: { guid: validToken.user_guid },
        })

        //Revocamos el refresh token actual
        await RevokeRefreshToken(refreshToken)

        //Generamos un nuevo access token
        const newAccessToken = await tokenSign(user, true)

        //Generamos un nuevo refresh token
        const newRefreshToken = await tokenSign(user, false)

        //Creamos una nueva fecha de expiración
        const expirationDate = new Date()
        expirationDate.setDate(expirationDate.getDate() + 2) // Token válido por 2 días

        //Guardamos el nuevo refresh token en la base de datos
        await SaveRefreshToken(
            user.guid,
            newRefreshToken,
            expirationDate,
            req.headers['User-Agent']
        )

        //Enviamos una respuesta de éxito con los datos
        handleHttpSuccess(res, 'Token renovado', 200, {
            token: newAccessToken,
            refreshToken: newRefreshToken,
            user,
        })
    } catch (error) {
        //Obtener y devolver el error
        handleHttpError(res, false, 'Error al verificar el token.', 403, error)
    }
}

// funcion para guardar el refresh token en la base de datos
const SaveRefreshToken = async (
    userGuid,
    refreshToken,
    expirationDate,
    diviceInfo = null
) => {
    await Token.create({
        user_guid: userGuid,
        refresh_token: refreshToken,
        expiry_date: expirationDate,
        created_at: new Date(),
        device_ifo: diviceInfo,
    })
}

// funcion para revocar el refresh token
const RevokeRefreshToken = async (token) => {
    await Token.update(
        { revoket: 1, revoked_at: new Date() },
        { where: { refresh_token: token } }
    )
}

// funcion para buscar el refresh token en la base de datos
const FindValidRefreshToken = async (token) => {
    const refreshToken = await Token.findOne({
        where: {
            refresh_token: token,
            revoket: 0,
            expiry_date: {
                // Verifica si la fecha de expiración es mayor a la fecha actual
                [Op.gt]: new Date(),
            },
        },
    })
    return refreshToken
}

// Endpoint para encriptar todas las contraseñas de la tabla de clientes
const EncryptPasswords = async (req, res) => {
    try {
        // Obtén todos los clientes de la tabla
        const user = await UserModel.find({
            created_by: 1,
            created_at: '2023-07-05',
        })

        // Encripta las contraseñas de cada cliente
        /* for (let i = 0; i < users.length; i++) {
        const user = users[i];
        const encryptedPassword = await encrypt(user.password);
  
        // Actualiza la contraseña en el cliente
        user.password = encryptedPassword;
        await user.save();
      } */
        const message = 'Se encontraron datos.'
        handleHttpSuccess(res, message, 201, user)
    } catch (error) {
        handleHttpError(res, 'Error al encriptar las contraseñas.', 403, error)
    }
}

// Función para verificar el token de Google
const VerifyGoogleToken = async (token) => {
    try {
        const ticket = await client.verifyIdToken({
            idToken: token,
            audience: process.env.GOOGLE_CLIENT_ID,
        })
        const payload = ticket.getPayload()
        // aqui validamos si el cliente existe en la base de datos y si no existe lo creamos
        console.log('payload:', payload)
        // Verificar si el cliente existe en la base de datos que corresponde al google_id o el email
        let user = await AuthModel.findOne({
            where: {
                [Op.or]: [
                    { google_id: payload.sub }, // payload.sub contiene el ID de Google
                    { email: payload.email }, // payload.email contiene el email del cliente
                ],
            },
        })

        //console.log("user:", user);

        if (!user) {
            console.log('El cliente no existe en la base de datos.')
            // creamos un guid para el cliente
            const guid = await uuidv4()
            // Si el cliente no existe, crearlo en la base de datos
            // obetenemos primero el guid del rol cliente
            const rol = await RolesModel.findOne({
                where: { name: 'cliente' },
            })
            await AuthModel.create({
                guid: guid,
                role_guid: rol.guid,
                google_id: payload.sub,
                email: payload.email,
                first_name: payload.given_name,
                last_name: payload.family_name,
                username: payload.name,
                picture: payload.picture,
                isVerified: 1,
                is_active: 1,
                last_login: new Date(),
                created_at: new Date(),
            })
            // Devolver el cliente creado pero con todos los datos del cliente
            user = await AuthModel.findOne({
                where: {
                    [Op.or]: [
                        { google_id: payload.sub }, // payload.sub contiene el ID de Google
                        { email: payload.email }, // payload.email contiene el email del cliente
                    ],
                },
            })
            console.log('Usuario creado en la base de datos:', user)
        } else {
            //validamos si ya tiene un google_id
            if (user.google_id == null || user.google_id == '') {
                // si el cliente existe, actualizamos su información de google
                user.google_id = payload.sub
                console.log('el cliente existe en la base de datos.', user)
                console.log('Usuario actualizado en la base de datos')
            } else {
                console.log('El cliente inicio sesión con Google.')
            }
        }
        // Devolver el cliente encontrado, creado o actualizado
        return user
    } catch (error) {
        console.error('Error al verificar el token de Google:', error)
        throw error
    }
}

// Función para autenticar con el token de Google
const AuthenticateWithGoogleToken = async (req, res) => {
    const { token } = req.body // Asegúrate de enviar el token con la clave 'token' en el cuerpo de la solicitud
    try {
        const payload = await verifyGoogleToken(token)
        console.log(
            '🚀 ~ authenticateWithGoogleToken ~ payload:',
            payload.dataValues
        )

        const accessToken = await tokenSign(payload, true)
        const refreshToken = await tokenSign(payload, false)
        const expirationDate = new Date()
        expirationDate.setDate(expirationDate.getDate() + 2) // Token válido por 2 días

        await SaveRefreshToken(
            payload.guid,
            refreshToken,
            expirationDate,
            req.headers['User-Agent']
        )

        // quitamos la contraseña del objeto user
        payload.password = undefined

        //Si se logra logear se envía un objeto con el token y los datos del cliente
        const data = {
            token: accessToken,
            refreshToken: refreshToken,
            user: payload,
        }

        //Si el cliente se puede logear actualizamos la ultima conexión
        const fechaActual = new Date()
        /* const opciones = { year: 'numeric', month: 'long', day: 'numeric' }
        const fechaFormateada = fechaActual.toLocaleDateString(
            'es-MX',
            opciones
        ) */

        await AuthModel.update(
            { last_login: fechaActual },
            {
                where: {
                    guid: payload.guid,
                },
            }
        )
        //Enviamos una respuesta de éxito con los datos
        handleHttpSuccess(res, '¡Bienvenido!', 200, data)
        //res.status(200).send({ success: payload });
    } catch (error) {
        console.error(error)
        handleHttpError(res, 'Error al autenticar con Google.', 403, error)
        res.status(400).send({
            success: false,
            message: 'Verificación fallida',
        })
    }
}

module.exports = {
    Register,
    LoginUser,
    ResetPassword,
    CheckToken,
    EncryptPasswords,
    VerifyGoogleToken,
    AuthenticateWithGoogleToken,
    RenewAccessToken,
}
