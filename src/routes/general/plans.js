const express = require('express')
const { matchedData } = require('express-validator')
const router = express.Router()

// validaciones
const {
    ValidatorCreatePlan,
    ValidatorUpdatePlan,
    ValidatorDeletePlan,
    ValidatorDisablePlan,
    ValidatorEnablePlan,
} = require('../../validators/general/plans')

// Importa los controladores
const {
    CreatePlan,
    Plans,
    Plan,
    UpdatePlan,
    DeletePlan,
    DisablePlan,
    EnablePlan,
    PlansAll,
    CountAllPlans,
} = require('../../controllers/general/plans')

// Middleware de autenticación
const authMiddleware = require('../../middleware/session')

// Rutas protegidas que requieren autenticación
/**
 * @swagger
 * tags:
 *   name: Plans
 *   description: API para la gestión de planes
 */

// Ruta para crear un nuevo plan
/**
 * @swagger
 * /plans/CreatePlan:
 *   post:
 *     summary: Crear un nuevo plan
 *     description: Esta ruta permite crear un nuevo plan con nombre y descripción.
 *     tags: [Plans]
 *     requestBody:
 *       required: true
 *       content:
 *         application/json:
 *           schema:
 *             type: object
 *             properties:
 *               name:
 *                 type: string
 *                 example: Básico
 *               description:
 *                  type: string
 *                  example: Plan básico
 *               price:
 *                  type: number
 *                  example: 10.00
 *               trial_days:
 *                  type: number
 *                  example: 7
 *               duration_days:
 *                  type: number
 *                  example: 30
 *               url:
 *                  type: string
 *                  example: https://www.example.com
 *     responses:
 *       201:
 *         description: Plan creado exitosamente
 *         content:
 *           application/json:
 *             schema:
 *               $ref: '#/components/responses/success'
 *       400:
 *         description: Error en los datos enviados
 *         content:
 *           application/json:
 *             schema:
 *               $ref: '#/components/responses/BadRequest'
 *       403:
 *         description: Error de autenticación
 *         content:
 *           application/json:
 *             schema:
 *               $ref: '#/components/responses/UnauthorizedError'
 *       409:
 *         description: Plan ya existe
 *         content:
 *           application/json:
 *             schema:
 *               $ref: '#/components/responses/ConflictError'
 *       500:
 *         description: Error en el servidor
 *         content:
 *           application/json:
 *             schema:
 *               $ref: '#/components/responses/InternalServerError'
 *     security:
 *        - bearerAuth: []
 */
router.post('/CreatePlan', authMiddleware, ValidatorCreatePlan, CreatePlan)

// Ruta para obtener todos los planes activos
/**
 * @swagger
 *  /plans/Plans:
 *   get:
 *     summary: Obtener todos los planes activos
 *     tags: [Plans]
 *     responses:
 *       201:
 *         description: Planes obtenidos exitosamente
 *         content:
 *           application/json:
 *             schema:
 *               $ref: '#/components/responses/success'
 *       403:
 *         description: Error de autenticación
 *         content:
 *           application/json:
 *             schema:
 *               $ref: '#/components/responses/UnauthorizedError'
 *       500:
 *         description: Error en el servidor
 *         content:
 *           application/json:
 *             schema:
 *               $ref: '#/components/responses/InternalServerError'
 *     security:
 *        - bearerAuth: []
 */
router.get('/Plans', authMiddleware, Plans)

// Ruta para obtener un plan
/**
 * @swagger
 * /plans/Plan:
 *   get:
 *     summary: Obtiene un plan por ID
 *     tags: [Plans]
 *     parameters:
 *       - in: path
 *         name: guid
 *         schema:
 *           type: uuid
 *           format: uuid
 *         required: true
 *         description: El GUID del plan
 *     responses:
 *       201:
 *         description: Plan obtenido exitosamente
 *         content:
 *           application/json:
 *             schema:
 *               $ref: '#/components/responses/success'
 *       403:
 *         description: Error de autenticación
 *         content:
 *           application/json:
 *             schema:
 *               $ref: '#/components/responses/UnauthorizedError'
 *       404:
 *        description: Plan no encontrado
 *        content:
 *          application/json:
 *            schema:
 *              $ref: '#/components/responses/NotFoundError'
 *       500:
 *         description: Error en el servidor
 *         content:
 *           application/json:
 *             schema:
 *               $ref: '#/components/responses/InternalServerError'
 *     security:
 *        - bearerAuth: []
 */
router.get('/Plan/:guid', authMiddleware, Plan)

// Ruta para actualizar un plan
/**
 * @swagger
 * /plans/UpdatePlan:
 *   patch:
 *     summary: Actualizar un plan
 *     tags: [Plans]
 *     requestBody:
 *       required: true
 *       content:
 *         application/json:
 *           schema:
 *             type: object
 *             properties:
 *               guid:
 *                 type: string
 *                 example: 495db854-849b-41cd-b6b9-9c84824c92ba
 *               name:
 *                 type: string
 *                 example: Básico
 *               description:
 *                  type: string
 *                  example: Plan básico
 *               price:
 *                  type: number
 *                  example: 10.00
 *               trial_days:
 *                  type: number
 *                  example: 7
 *               duration_days:
 *                  type: number
 *                  example: 30
 *               url:
 *                  type: string
 *                  example: https://www.example.com
 *     responses:
 *       201:
 *         description: Plan actualizado exitosamente
 *         content:
 *           application/json:
 *             schema:
 *               $ref: '#/components/responses/success'
 *       400:
 *         description: Error en los datos enviados
 *         content:
 *           application/json:
 *             schema:
 *               $ref: '#/components/responses/BadRequest'
 *       403:
 *         description: Error de autenticación
 *         content:
 *           application/json:
 *             schema:
 *               $ref: '#/components/responses/UnauthorizedError'
 *       404:
 *        description: Plan no encontrado
 *        content:
 *          application/json:
 *            schema:
 *              $ref: '#/components/responses/NotFoundError'
 *       500:
 *         description: Error en el servidor
 *         content:
 *           application/json:
 *             schema:
 *               $ref: '#/components/responses/InternalServerError'
 *     security:
 *        - bearerAuth: []
 */
router.patch(
    '/UpdatePlan/:guid',
    authMiddleware,
    ValidatorUpdatePlan,
    UpdatePlan
)

// Ruta para eliminar un plan
/**
 * @swagger
 * /plans/DeletePlan:
 *   delete:
 *     summary: Eliminar un plan
 *     tags: [Plans]
 *     parameters:
 *       - in: path
 *         name: guid
 *         schema:
 *           type: uuid
 *           format: uuid
 *         required: true
 *         description: El GUID del plan
 *     responses:
 *       201:
 *         description: Plan eliminado exitosamente
 *         content:
 *           application/json:
 *             schema:
 *               $ref: '#/components/responses/success'
 *       400:
 *         description: Error en los datos enviados
 *         content:
 *           application/json:
 *             schema:
 *               $ref: '#/components/responses/BadRequest'
 *       403:
 *         description: Error de autenticación
 *         content:
 *           application/json:
 *             schema:
 *               $ref: '#/components/responses/UnauthorizedError'
 *       404:
 *        description: Plan no encontrado
 *        content:
 *          application/json:
 *            schema:
 *              $ref: '#/components/responses/NotFoundError'
 *       500:
 *         description: Error en el servidor
 *         content:
 *           application/json:
 *             schema:
 *               $ref: '#/components/responses/InternalServerError'
 *     security:
 *        - bearerAuth: []
 */
router.delete(
    '/DeletePlan/:guid',
    authMiddleware,
    ValidatorDeletePlan,
    DeletePlan
)

// Ruta para desactivar un plan
/**
 * @swagger
 * /plans/DisablePlan:
 *   patch:
 *     summary: Desactivar un plan
 *     tags: [Plans]
 *     requestBody:
 *       required: true
 *       content:
 *         application/json:
 *           schema:
 *             type: object
 *             properties:
 *               guid:
 *                 type: uuid
 *                 example: 495db854-849b-41cd-b6b9-9c84824c92ba
 *     responses:
 *       201:
 *         description: Plan desactivado exitosamente
 *         content:
 *           application/json:
 *             schema:
 *               $ref: '#/components/responses/success'
 *       400:
 *         description: Error en los datos enviados
 *         content:
 *           application/json:
 *             schema:
 *               $ref: '#/components/responses/BadRequest'
 *       403:
 *         description: Error de autenticación
 *         content:
 *           application/json:
 *             schema:
 *               $ref: '#/components/responses/UnauthorizedError'
 *       404:
 *        description: Plan no encontrado
 *        content:
 *          application/json:
 *            schema:
 *              $ref: '#/components/responses/NotFoundError'
 *       500:
 *         description: Error en el servidor
 *         content:
 *           application/json:
 *             schema:
 *               $ref: '#/components/responses/InternalServerError'
 *     security:
 *        - bearerAuth: []
 */
router.patch(
    '/DisablePlan/:guid',
    authMiddleware,
    ValidatorDisablePlan,
    DisablePlan
)

// Ruta para activar un plan
/**
 * @swagger
 * /plans/EnablePlan:
 *   patch:
 *     summary: Activar un plan
 *     tags: [Plans]
 *     requestBody:
 *       required: true
 *       content:
 *         application/json:
 *           schema:
 *             type: object
 *             properties:
 *               guid:
 *                 type: uuid
 *                 example: 495db854-849b-41cd-b6b9-9c84824c92ba
 *     responses:
 *       201:
 *         description: Plan activado exitosamente
 *         content:
 *           application/json:
 *             schema:
 *               $ref: '#/components/responses/success'
 *       400:
 *         description: Error en los datos enviados
 *         content:
 *           application/json:
 *             schema:
 *               $ref: '#/components/responses/BadRequest'
 *       403:
 *         description: Error de autenticación
 *         content:
 *           application/json:
 *             schema:
 *               $ref: '#/components/responses/UnauthorizedError'
 *       404:
 *        description: Plan no encontrado
 *        content:
 *          application/json:
 *            schema:
 *              $ref: '#/components/responses/NotFoundError'
 *       500:
 *         description: Error en el servidor
 *         content:
 *           application/json:
 *             schema:
 *               $ref: '#/components/responses/InternalServerError'
 *     security:
 *        - bearerAuth: []
 */
router.patch(
    '/EnablePlan/:guid',
    authMiddleware,
    ValidatorEnablePlan,
    EnablePlan
)

// Ruta para obtener todos los planes
/**
 * @swagger
 * /plans/PlansAll:
 *   get:
 *     summary: Obtener todos los planes
 *     tags: [Plans]
 *     responses:
 *       201:
 *         description: Planes obtenidos exitosamente
 *         content:
 *           application/json:
 *             schema:
 *               $ref: '#/components/responses/success'
 *       400:
 *         description: Error en los datos enviados
 *         content:
 *           application/json:
 *             schema:
 *               $ref: '#/components/responses/BadRequest'
 *       403:
 *         description: Error de autenticación
 *         content:
 *           application/json:
 *             schema:
 *               $ref: '#/components/responses/UnauthorizedError'
 *       500:
 *         description: Error en el servidor
 *         content:
 *           application/json:
 *             schema:
 *               $ref: '#/components/responses/InternalServerError'
 *     security:
 *        - bearerAuth: []
 */
router.get('/PlansAll', authMiddleware, PlansAll)

// Ruta para contar todos los planes
/**
 * @swagger
 * /plans/PlansCount:
 *   get:
 *     summary: Contar todos los planes
 *     tags: [Plans]
 *     responses:
 *       201:
 *         description: Planes contados exitosamente
 *         content:
 *           application/json:
 *             schema:
 *               $ref: '#/components/responses/success'
 *       400:
 *         description: Error en los datos enviados
 *         content:
 *           application/json:
 *             schema:
 *               $ref: '#/components/responses/BadRequest'
 *       403:
 *         description: Error de autenticación
 *         content:
 *           application/json:
 *             schema:
 *               $ref: '#/components/responses/UnauthorizedError'
 *       500:
 *         description: Error en el servidor
 *         content:
 *           application/json:
 *             schema:
 *               $ref: '#/components/responses/InternalServerError'
 *     security:
 *        - bearerAuth: []
 */
router.get('/PlansCount', authMiddleware, CountAllPlans)

module.exports = router
