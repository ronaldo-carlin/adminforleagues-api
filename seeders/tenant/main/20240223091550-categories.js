const GUIDsTenant = require('../../guids_tenant')

module.exports = {
    async up(queryInterface, Sequelize) {
        // insertar las categorias de los torneos
        await queryInterface.bulkInsert(
            'categories',
            [
                {
                    guid: GUIDsTenant.categoriesLibreGuid,
                    name: 'Libre',
                    description: 'Descripción de la categoría libre',
                    logo_path: 'logo_path',
                    color: '#000000',
                    age_min: 1,
                    age_max: 2,
                    gender: 'masculino',
                    level: 'nivel 1',
                    is_active: 1,
                    created_by: '',
                    created_at: new Date(),
                },
                {
                    guid: GUIDsTenant.categoriesFemenilGuid,
                    name: 'Femenil',
                    description: 'Descripción de la categoría femenil',
                    logo_path: 'logo_path',
                    color: '#000000',
                    age_min: 1,
                    age_max: 2,
                    gender: 'femenino',
                    level: 'nivel 1',
                    is_active: 1,
                    created_by: '',
                    created_at: new Date(),
                },
                {
                    guid: GUIDsTenant.categoriesInfantilGuid,
                    name: 'Infantil',
                    description: 'Descripción de la categoría infantil',
                    logo_path: 'logo_path',
                    color: '#000000',
                    age_min: 1,
                    age_max: 2,
                    gender: 'masculino',
                    level: 'nivel 1',
                    is_active: 1,
                    created_by: '',
                    created_at: new Date(),
                },
            ],
            {}
        )
    },

    async down(queryInterface, Sequelize) {
        await queryInterface.bulkDelete('categories', null, {})
    },
}
