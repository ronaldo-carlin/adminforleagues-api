const express = require('express')
const { matchedData } = require('express-validator')
const router = express.Router()

// Validaciones
const {
    ValidatorCreateCoupon,
    ValidatorUpdateCoupon,
    ValidatorDeleteCoupon,
    ValidatorDisableCoupon,
    ValidatorEnableCoupon,
} = require('../../validators/general/coupons')

// Importar controladores
const {
    CreateCoupon,
    Coupons,
    Coupon,
    UpdateCoupon,
    DeleteCoupon,
    DisableCoupon,
    EnableCoupon,
    CouponsAll,
    CouponsCount,
} = require('../../controllers/general/coupons')

// Middleware de autenticación
const authMiddleware = require('../../middleware/session')

// Rutas protegidas que requieren autenticación
/**
 * @swagger
 * tags:
 *   name: Coupons
 *   description: API para la gestión de cupones
 */

// Ruta para crear un nuevo cupón
/**
 * @swagger
 * /coupons/CreateCoupon:
 *   post:
 *     summary: Crear un nuevo cupón
 *     description: Esta ruta permite crear un nuevo cupón con los detalles especificados.
 *     tags: [Coupons]
 *     requestBody:
 *       required: true
 *       content:
 *         application/json:
 *           schema:
 *             type: object
 *             properties:
 *               code:
 *                 type: string
 *                 example: SAVE20
 *               discount:
 *                 type: number
 *                 example: 20.00
 *               expiration_date:
 *                 type: string
 *                 format: date
 *                 example: 2024-12-31
 *               is_active:
 *                 type: boolean
 *                 example: true
 *     responses:
 *       201:
 *         description: Cupón creado exitosamente
 *         content:
 *           application/json:
 *             schema:
 *               $ref: '#/components/responses/success'
 *       400:
 *         description: Error en los datos enviados
 *         content:
 *           application/json:
 *             schema:
 *               $ref: '#/components/responses/BadRequest'
 *       403:
 *         description: Error de autenticación
 *         content:
 *           application/json:
 *             schema:
 *               $ref: '#/components/responses/UnauthorizedError'
 *       409:
 *         description: El cupón ya existe
 *         content:
 *           application/json:
 *             schema:
 *               $ref: '#/components/responses/ConflictError'
 *       500:
 *         description: Error en el servidor
 *         content:
 *           application/json:
 *             schema:
 *               $ref: '#/components/responses/InternalServerError'
 *     security:
 *        - bearerAuth: []
 */
router.post(
    '/CreateCoupon',
    authMiddleware,
    ValidatorCreateCoupon,
    CreateCoupon
)

// Ruta para obtener todos los cupones
/**
 * @swagger
 * /coupons/Coupons:
 *   get:
 *     summary: Obtener todos los cupones
 *     tags: [Coupons]
 *     responses:
 *       200:
 *         description: Cupones obtenidos exitosamente
 *         content:
 *           application/json:
 *             schema:
 *               $ref: '#/components/responses/success'
 *       403:
 *         description: Error de autenticación
 *         content:
 *           application/json:
 *             schema:
 *               $ref: '#/components/responses/UnauthorizedError'
 *       500:
 *         description: Error en el servidor
 *         content:
 *           application/json:
 *             schema:
 *               $ref: '#/components/responses/InternalServerError'
 *     security:
 *        - bearerAuth: []
 */
router.get('/Coupons', authMiddleware, Coupons)

// Ruta para obtener un cupón por ID
/**
 * @swagger
 * /coupons/Coupon/{guid}:
 *   get:
 *     summary: Obtener un cupón por ID
 *     tags: [Coupons]
 *     parameters:
 *       - in: path
 *         name: guid
 *         schema:
 *           type: string
 *           format: uuid
 *         required: true
 *         description: El GUID del cupón
 *     responses:
 *       200:
 *         description: Cupón obtenido exitosamente
 *         content:
 *           application/json:
 *             schema:
 *               $ref: '#/components/responses/success'
 *       403:
 *         description: Error de autenticación
 *         content:
 *           application/json:
 *             schema:
 *               $ref: '#/components/responses/UnauthorizedError'
 *       404:
 *        description: Cupón no encontrado
 *        content:
 *          application/json:
 *            schema:
 *              $ref: '#/components/responses/NotFoundError'
 *       500:
 *         description: Error en el servidor
 *         content:
 *           application/json:
 *             schema:
 *               $ref: '#/components/responses/InternalServerError'
 *     security:
 *        - bearerAuth: []
 */
router.get('/Coupon/:guid', authMiddleware, Coupon)

// Ruta para actualizar un cupón
/**
 * @swagger
 * /coupons/UpdateCoupon/{guid}:
 *   patch:
 *     summary: Actualizar un cupón
 *     tags: [Coupons]
 *     requestBody:
 *       required: true
 *       content:
 *         application/json:
 *           schema:
 *             type: object
 *             properties:
 *               code:
 *                 type: string
 *                 example: SAVE30
 *               discount:
 *                 type: number
 *                 example: 30.00
 *               expiration_date:
 *                 type: string
 *                 format: date
 *                 example: 2024-12-31
 *               is_active:
 *                 type: boolean
 *                 example: true
 *     responses:
 *       200:
 *         description: Cupón actualizado exitosamente
 *         content:
 *           application/json:
 *             schema:
 *               $ref: '#/components/responses/success'
 *       400:
 *         description: Error en los datos enviados
 *         content:
 *           application/json:
 *             schema:
 *               $ref: '#/components/responses/BadRequest'
 *       403:
 *         description: Error de autenticación
 *         content:
 *           application/json:
 *             schema:
 *               $ref: '#/components/responses/UnauthorizedError'
 *       404:
 *        description: Cupón no encontrado
 *        content:
 *          application/json:
 *            schema:
 *              $ref: '#/components/responses/NotFoundError'
 *       500:
 *         description: Error en el servidor
 *         content:
 *           application/json:
 *             schema:
 *               $ref: '#/components/responses/InternalServerError'
 *     security:
 *        - bearerAuth: []
 */
router.patch(
    '/UpdateCoupon/:guid',
    authMiddleware,
    ValidatorUpdateCoupon,
    UpdateCoupon
)

// Ruta para eliminar un cupón
/**
 * @swagger
 * /coupons/DeleteCoupon/{guid}:
 *   delete:
 *     summary: Eliminar un cupón
 *     tags: [Coupons]
 *     parameters:
 *       - in: path
 *         name: guid
 *         schema:
 *           type: string
 *           format: uuid
 *         required: true
 *         description: El GUID del cupón
 *     responses:
 *       200:
 *         description: Cupón eliminado exitosamente
 *         content:
 *           application/json:
 *             schema:
 *               $ref: '#/components/responses/success'
 *       400:
 *         description: Error en los datos enviados
 *         content:
 *           application/json:
 *             schema:
 *               $ref: '#/components/responses/BadRequest'
 *       403:
 *         description: Error de autenticación
 *         content:
 *           application/json:
 *             schema:
 *               $ref: '#/components/responses/UnauthorizedError'
 *       404:
 *        description: Cupón no encontrado
 *        content:
 *          application/json:
 *            schema:
 *              $ref: '#/components/responses/NotFoundError'
 *       500:
 *         description: Error en el servidor
 *         content:
 *           application/json:
 *             schema:
 *               $ref: '#/components/responses/InternalServerError'
 *     security:
 *        - bearerAuth: []
 */
router.delete(
    '/DeleteCoupon/:guid',
    authMiddleware,
    ValidatorDeleteCoupon,
    DeleteCoupon
)

// Ruta para deshabilitar un cupón
/**
 * @swagger
 * /coupons/DisableCoupon:
 *   patch:
 *     summary: Deshabilitar un cupón
 *     tags: [Coupons]
 *     requestBody:
 *       required: true
 *       content:
 *         application/json:
 *           schema:
 *             type: object
 *             properties:
 *               guid:
 *                 type: uuid
 *                 example: 495db854-849b-41cd-b6b9-9c84824c92ba
 *     responses:
 *       200:
 *         description: Cupón deshabilitado exitosamente
 *         content:
 *           application/json:
 *             schema:
 *               $ref: '#/components/responses/success'
 *       400:
 *         description: Error en los datos enviados
 *         content:
 *           application/json:
 *             schema:
 *               $ref: '#/components/responses/BadRequest'
 *       403:
 *         description: Error de autenticación
 *         content:
 *           application/json:
 *             schema:
 *               $ref: '#/components/responses/UnauthorizedError'
 *       404:
 *        description: Cupón no encontrado
 *        content:
 *          application/json:
 *            schema:
 *              $ref: '#/components/responses/NotFoundError'
 *       500:
 *         description: Error en el servidor
 *         content:
 *           application/json:
 *             schema:
 *               $ref: '#/components/responses/InternalServerError'
 *     security:
 *        - bearerAuth: []
 */
router.patch(
    '/DisableCoupon/:guid',
    authMiddleware,
    ValidatorDisableCoupon,
    DisableCoupon
)

// Ruta para habilitar un cupón
/**
 * @swagger
 * /coupons/EnableCoupon:
 *   patch:
 *     summary: Habilitar un cupón
 *     tags: [Coupons]
 *     requestBody:
 *       required: true
 *       content:
 *         application/json:
 *           schema:
 *             type: object
 *             properties:
 *               guid:
 *                 type: uuid
 *                 example: 495db854-849b-41cd-b6b9-9c84824c92ba
 *     responses:
 *       200:
 *         description: Cupón habilitado exitosamente
 *         content:
 *           application/json:
 *             schema:
 *               $ref: '#/components/responses/success'
 *       400:
 *         description: Error en los datos enviados
 *         content:
 *           application/json:
 *             schema:
 *               $ref: '#/components/responses/BadRequest'
 *       403:
 *         description: Error de autenticación
 *         content:
 *           application/json:
 *             schema:
 *               $ref: '#/components/responses/UnauthorizedError'
 *       404:
 *        description: Cupón no encontrado
 *        content:
 *          application/json:
 *            schema:
 *              $ref: '#/components/responses/NotFoundError'
 *       500:
 *         description: Error en el servidor
 *         content:
 *           application/json:
 *             schema:
 *               $ref: '#/components/responses/InternalServerError'
 *     security:
 *        - bearerAuth: []
 */
router.patch(
    '/EnableCoupon/:guid',
    authMiddleware,
    ValidatorEnableCoupon,
    EnableCoupon
)

// Ruta para obtener todos los cupones sin paginación
/**
 * @swagger
 * /coupons/CouponsAll:
 *   get:
 *     summary: Obtener todos los cupones sin paginación
 *     tags: [Coupons]
 *     responses:
 *       200:
 *         description: Cupones obtenidos exitosamente
 *         content:
 *           application/json:
 *             schema:
 *               $ref: '#/components/responses/success'
 *       403:
 *         description: Error de autenticación
 *         content:
 *           application/json:
 *             schema:
 *               $ref: '#/components/responses/UnauthorizedError'
 *       500:
 *         description: Error en el servidor
 *         content:
 *           application/json:
 *             schema:
 *               $ref: '#/components/responses/InternalServerError'
 *     security:
 *        - bearerAuth: []
 */
router.get('/CouponsAll', authMiddleware, CouponsAll)

// Ruta para obtener el conteo total de cupones
/**
 * @swagger
 * /coupons/CouponsCount:
 *   get:
 *     summary: Obtener el conteo total de cupones
 *     tags: [Coupons]
 *     responses:
 *       200:
 *         description: Conteo de cupones obtenido exitosamente
 *         content:
 *           application/json:
 *             schema:
 *               $ref: '#/components/responses/success'
 *       403:
 *         description: Error de autenticación
 *         content:
 *           application/json:
 *             schema:
 *               $ref: '#/components/responses/UnauthorizedError'
 *       500:
 *         description: Error en el servidor
 *         content:
 *           application/json:
 *             schema:
 *               $ref: '#/components/responses/InternalServerError'
 *     security:
 *        - bearerAuth: []
 */
router.get('/CouponsCount', authMiddleware, CouponsCount)

module.exports = router
