const { check } = require('express-validator')
const validateResults = require('../../utils/handleValidators')

// Validaciones para crear una credencial de Tenant
const ValidatorCreateTenantCredential = [
    check('tenant_guid')
        .isUUID()
        .withMessage('El guid del tenant es requerido y debe ser un UUID.'),
    check('domain')
        .isString()
        .withMessage(
            'El dominio es requerido y debe ser una cadena de caracteres.'
        ),
    check('subdomain')
        .isString()
        .withMessage(
            'El subdominio es requerido y debe ser una cadena de caracteres.'
        ),
    check('database')
        .isString()
        .withMessage(
            'La base de datos es requerida y debe ser una cadena de caracteres.'
        ),
    check('user')
        .isString()
        .withMessage(
            'El usuario es requerido y debe ser una cadena de caracteres.'
        ),
    check('password')
        .isString()
        .withMessage(
            'La contraseña es requerida y debe ser una cadena de caracteres.'
        ),
    check('host')
        .isString()
        .withMessage(
            'El host es requerido y debe ser una cadena de caracteres.'
        ),
    check('port')
        .isInt()
        .withMessage('El puerto es requerido y debe ser un número entero.'),
    validateResults,
]

// Validaciones para actualizar una credencial de Tenant
const ValidatorUpdateTenantCredential = [
    check('guid')
        .isUUID()
        .withMessage('El guid es requerido y debe ser un UUID.'),
    check('tenant_guid')
        .isUUID()
        .withMessage('El guid del tenant es requerido y debe ser un UUID.'),
    check('domain')
        .isString()
        .withMessage(
            'El dominio es requerido y debe ser una cadena de caracteres.'
        ),
    check('subdomain')
        .isString()
        .withMessage(
            'El subdominio es requerido y debe ser una cadena de caracteres.'
        ),
    check('database')
        .isString()
        .withMessage(
            'La base de datos es requerida y debe ser una cadena de caracteres.'
        ),
    check('user')
        .isString()
        .withMessage(
            'El usuario es requerido y debe ser una cadena de caracteres.'
        ),
    check('password')
        .isString()
        .withMessage(
            'La contraseña es requerida y debe ser una cadena de caracteres.'
        ),
    check('host')
        .isString()
        .withMessage(
            'El host es requerido y debe ser una cadena de caracteres.'
        ),
    check('port')
        .isInt()
        .withMessage('El puerto es requerido y debe ser un número entero.'),
    validateResults,
]

// Validaciones para eliminar una credencial de Tenant
const ValidatorDeleteTenantCredential = [
    check('guid')
        .isUUID()
        .withMessage('El guid es requerido y debe ser un UUID.'),
    validateResults,
]

// Validaciones para deshabilitar una credencial de Tenant
const ValidatorDisableTenantCredential = [
    check('guid')
        .isUUID()
        .withMessage('El guid es requerido y debe ser un UUID.'),
    validateResults,
]

// Validaciones para habilitar una credencial de Tenant
const ValidatorEnableTenantCredential = [
    check('guid')
        .isUUID()
        .withMessage('El guid es requerido y debe ser un UUID.'),
    validateResults,
]

// Validaciones para ejecutar los seeders
const ValidatorRunSeedersTenant = [
    check('tenant_guid')
        .isUUID()
        .withMessage('El guid del tenant es requerido y debe ser un UUID.'),
    validateResults,
]

module.exports = {
    ValidatorCreateTenantCredential,
    ValidatorUpdateTenantCredential,
    ValidatorDeleteTenantCredential,
    ValidatorDisableTenantCredential,
    ValidatorEnableTenantCredential,
    ValidatorRunSeedersTenant,
}
