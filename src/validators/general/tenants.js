const { check } = require('express-validator')
const validateResults = require('../../utils/handleValidators')
const TenantsModel = require('../../models/general/tenants')

// Validaciones para crear un Tenant
const ValidatorCreateTenant = [
    check('company_name')
        .isString()
        .withMessage('El nombre de la empresa es requerido.')
        .custom(async (value) => {
            const tenant = await TenantsModel.findOne({
                where: { company_name: value },
            })
            if (tenant) {
                return Promise.reject('El nombre de la empresa ya está en uso.')
            }
        }),
    check('email')
        .isEmail()
        .withMessage('El email es requerido.')
        .custom(async (value) => {
            const tenant = await TenantsModel.findOne({
                where: { email: value },
            })
            if (tenant) {
                return Promise.reject('El email ya está en uso.')
            }
        }),
    check('phone').isString().withMessage('El teléfono es requerido.'),
    check('country').isString().withMessage('El país es requerido.'),
    check('state').isString().withMessage('El estado es requerido.'),
    check('city').isString().withMessage('La ciudad es requerida.'),
    check('address')
        .optional()
        .isString()
        .withMessage('La dirección es requerida.'),

    validateResults,
]

// Validaciones para actualizar un Tenant
const ValidatorUpdateTenant = [
    check('guid').isUUID().withMessage('El guid es requerido.'),
    check('company_name')
        .isString()
        .withMessage('El nombre de la empresa es requerido.'),
    check('email').isEmail().withMessage('El email es requerido.'),
    check('phone').isString().withMessage('El teléfono es requerido.'),
    check('country').isString().withMessage('El país es requerido.'),
    check('state').isString().withMessage('El estado es requerido.'),
    check('city').isString().withMessage('La ciudad es requerida.'),
    check('address')
        .optional()
        .isString()
        .withMessage('La dirección es requerida.'),
    validateResults,
]

// Validaciones para eliminar un Tenant
const ValidatorDeleteTenant = [
    check('guid').isUUID().withMessage('El guid es requerido.'),
    validateResults,
]

// Validaciones para deshabilitar un Tenant
const ValidatorDisableTenant = [
    check('guid').isUUID().withMessage('El guid es requerido.'),
    validateResults,
]

// Validaciones para habilitar un Tenant
const ValidatorEnableTenant = [
    check('guid').isUUID().withMessage('El guid es requerido.'),
    validateResults,
]

module.exports = {
    ValidatorCreateTenant,
    ValidatorUpdateTenant,
    ValidatorDeleteTenant,
    ValidatorDisableTenant,
    ValidatorEnableTenant,
}
