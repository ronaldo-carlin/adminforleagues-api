const express = require('express')
const router = express.Router()

// validaciones
const {
    ValidatorCreateRol,
    ValidatorRol,
    ValidatorUpdateRol,
    ValidatorDeleteRol,
    ValidatorDisableRol,
    ValidatorEnableRol,
} = require('../../validators/general/roles')

// controladores
const {
    createRol,
    Roles,
    Rol,
    updateRol,
    deleteRol,
    disableRol,
    enableRol,
    rolesAll,
    countAllRoles,
} = require('../../controllers/general/roles')

// middleware de autenticación
const authMiddleware = require('../../middleware/session')

// Rutas protegidas que requieren autenticación
/**
 * @swagger
 * tags:
 *   name: Roles
 *   description: API para roles de usuario
 */

/**
 * @swagger
 * /roles/createRol:
 *   post:
 *     summary: Crea un nuevo rol
 *     description: Esta ruta permite crear un nuevo rol con nombre y descripción.
 *     tags: [Roles]
 *     security:
 *       - bearerAuth: []
 *     requestBody:
 *       required: true
 *       content:
 *         application/json:
 *           schema:
 *             $ref: '#/components/schemas/CreateRol'
 *     responses:
 *       201:
 *         description: Rol creado exitosamente
 *       400:
 *         description: Error en los datos enviados
 *         content:
 *           application/json:
 *             schema:
 *               $ref: '#/components/responses/BadRequest'
 *       403:
 *         description: Error de autenticación
 *         content:
 *           application/json:
 *             schema:
 *               $ref: '#/components/responses/UnauthorizedError'
 *       404:
 *        description: Usuario no encontrado
 *        content:
 *          application/json:
 *            schema:
 *              $ref: '#/components/responses/NotFoundError'
 *       409:
 *         description: Rol ya existe
 *         content:
 *           application/json:
 *             schema:
 *               $ref: '#/components/responses/ConflictError'
 *       500:
 *         description: Error en el servidor
 *         content:
 *           application/json:
 *             schema:
 *               $ref: '#/components/responses/InternalServerError'
 */
router.post('/CreateRol', authMiddleware, ValidatorCreateRol, createRol)

/**
 * @swagger
 * /roles/Roles:
 *   get:
 *     summary: Obtiene la lista de roles activos
 *     tags: [Roles]
 *     security:
 *       - bearerAuth: []
 *     responses:
 *       200:
 *         description: Lista de roles obtenida exitosamente
 *         content:
 *           application/json:
 *             schema:
 *               type: array
 *               items:
 *                 $ref: '#/components/schemas/Roles'
 *       403:
 *         description: Error obteniendo los roles
 */
router.get('/Roles', authMiddleware, Roles)

/**
 * @swagger
 * /roles/Rol:
 *   get:
 *     summary: Obtiene un rol por ID
 *     tags: [Roles]
 *     security:
 *       - bearerAuth: []
 *     requestBody:
 *       required: true
 *       content:
 *         application/json:
 *           schema:
 *             $ref: '#/components/schemas/Rol'
 *     responses:
 *       200:
 *         description: Rol obtenido exitosamente
 *         content:
 *           application/json:
 *             schema:
 *               $ref: '#/components/schemas/Roles'
 *       403:
 *         description: Error obteniendo el rol
 */
router.get('/Rol/:guid', authMiddleware, ValidatorRol, Rol)

/**
 * @swagger
 * /roles/updateRol:
 *   patch:
 *     summary: Actualiza un rol existente
 *     tags: [Roles]
 *     security:
 *       - bearerAuth: []
 *     requestBody:
 *       required: true
 *       content:
 *         application/json:
 *           schema:
 *             $ref: '#/components/schemas/UpdateRol'
 *     responses:
 *       200:
 *         description: Rol actualizado exitosamente
 *       403:
 *         description: Error actualizando el rol
 */
router.patch('/UpdateRol/:guid', authMiddleware, ValidatorUpdateRol, updateRol)

/**
 * @swagger
 * /roles/deleteRol:
 *   delete:
 *     summary: Elimina un rol
 *     tags: [Roles]
 *     security:
 *       - bearerAuth: []
 *     requestBody:
 *       required: true
 *       content:
 *         application/json:
 *           schema:
 *             $ref: '#/components/schemas/DeleteRol'
 *     responses:
 *       200:
 *         description: Rol eliminado exitosamente
 *       403:
 *         description: Error eliminando el rol
 */
router.delete('/DeleteRol/:guid', authMiddleware, ValidatorDeleteRol, deleteRol)

/**
 * @swagger
 * /roles/disableRol:
 *   patch:
 *     summary: Deshabilita un rol
 *     tags: [Roles]
 *     security:
 *       - bearerAuth: []
 *     requestBody:
 *       required: true
 *       content:
 *         application/json:
 *           schema:
 *             $ref: '#/components/schemas/DisableRol'
 *     responses:
 *       200:
 *         description: Rol deshabilitado exitosamente
 *       403:
 *         description: Error deshabilitando el rol
 */
router.patch(
    '/DisableRol/:guid',
    authMiddleware,
    ValidatorDisableRol,
    disableRol
)

/**
 * @swagger
 * /roles/enableRol:
 *   patch:
 *     summary: Habilita un rol
 *     tags: [Roles]
 *     security:
 *       - bearerAuth: []
 *     requestBody:
 *       required: true
 *       content:
 *         application/json:
 *           schema:
 *             $ref: '#/components/schemas/EnableRol'
 *     responses:
 *       200:
 *         description: Rol habilitado exitosamente
 *       403:
 *         description: Error habilitando el rol
 */
router.patch('/EnableRol/:guid', authMiddleware, ValidatorEnableRol, enableRol)

/**
 * @swagger
 * /roles/rolesAll:
 *   get:
 *     summary: obtiene todos los roles registrados
 *     tags: [Roles]
 *     security:
 *       - bearerAuth: []
 *     responses:
 *       200:
 *         description: Roles encontrados exitosamente
 *       403:
 *         description: Error encontrando los roles
 */
router.get('/RolesAll', authMiddleware, rolesAll)

/**
 * @swagger
 * /roles/countAllRoles:
 *   get:
 *     summary: Cuenta todos los roles registrados
 *     tags: [Roles]
 *     security:
 *       - bearerAuth: []
 *     responses:
 *       200:
 *         description: Conteo de todos los roles exitoso
 *       403:
 *         description: Error contando todos los roles
 */
router.get('/CountAllRoles', authMiddleware, countAllRoles)

module.exports = router
