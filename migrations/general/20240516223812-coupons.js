module.exports = {
    async up(queryInterface, Sequelize) {
        // entidad para los cupones de descuento
        await queryInterface.createTable('coupons', {
            // identificador del registro
            id: {
                type: Sequelize.INTEGER,
                primaryKey: true,
                autoIncrement: true,
                allowNull: false,
            },
            // guid del registro
            guid: {
                type: Sequelize.UUID,
                defaultValue: Sequelize.UUIDV4, // Genera un UUID por defecto
                unique: true,
                allowNull: false,
            },
            // plan al que aplica el cupon
            plan_guid: {
                type: Sequelize.UUID,
            },
            // codigo del cupon
            code: {
                type: Sequelize.STRING(100),
                unique: true,
            },
            // descripcion del cupon
            description: {
                type: Sequelize.TEXT,
            },
            // tipo de descuento
            discount_type: {
                type: Sequelize.STRING(100),
            },
            // valor del descuento
            discount_value: {
                type: Sequelize.DECIMAL,
            },
            // fecha de inicio de validez
            start_date: {
                type: Sequelize.DATE,
            },
            // fecha de fin de validez
            end_date: {
                type: Sequelize.DATE,
            },
            // cantidad de usos permitidos
            uses: {
                type: Sequelize.INTEGER,
            },
            // cantidad de usos realizados
            used: {
                type: Sequelize.INTEGER,
            },
            // link de redireccion
            link_by: {
                type: Sequelize.STRING(100),
            },
            // cantidad minim del precio de adquisicion
            min_amount: {
                type: Sequelize.DECIMAL,
            },
            // cantidad maxima del precio de adquisicion
            max_amount: {
                type: Sequelize.DECIMAL,
            },
            // estado del registro
            is_active: {
                type: Sequelize.BOOLEAN,
                defaultValue: 1, // valor por defecto
            },
            // usuario que creo el registro
            created_by: {
                type: Sequelize.UUID,
            },
            // fecha de creacion
            created_at: {
                type: Sequelize.DATE,
                defaultValue: Sequelize.literal('CURRENT_TIMESTAMP'),
            },
            // usuario que actualizo el registro
            updated_by: {
                type: Sequelize.UUID,
            },
            // fecha de actualizacion
            updated_at: {
                type: Sequelize.DATE,
            },
            // usuario que elimino el registro
            deleted_by: {
                type: Sequelize.UUID,
            },
            // fecha de eliminacion
            deleted_at: {
                type: Sequelize.DATE,
            },
        })

        // Agregar índices después de crear la tabla
        await queryInterface.addIndex('coupons', ['guid'], {
            unique: true,
            name: 'uniq_guid_coupon',
        })

        await queryInterface.addIndex('coupons', ['plan_guid'], {
            name: 'idx_coupon_plan_guid',
        })

        // Agregar constraints después de crear la tabla
        await queryInterface.addConstraint('coupons', {
            fields: ['plan_guid'],
            type: 'foreign key',
            name: 'fk_coupon_plan_guid',
            references: {
                table: 'plans',
                field: 'guid',
            },
            onDelete: 'NO ACTION',
            onUpdate: 'NO ACTION',
        })
    },

    async down(queryInterface, Sequelize) {
        await queryInterface.removeConstraint('coupons', 'fk_coupon_plan_guid')
        await queryInterface.removeIndex('coupons', 'uniq_guid_coupon')
        await queryInterface.removeIndex('coupons', 'idx_coupon_plan_guid')
        await queryInterface.dropTable('coupons')
    },
}
