const GUIDsTenant = require('../../guids_tenant')

module.exports = {
    async up(queryInterface, Sequelize) {
        await queryInterface.bulkInsert(
            'sports',
            [
                {
                    guid: GUIDsTenant.futbolGuid,
                    name: 'Futbol',
                    description: 'Futbol',
                    icon: 'TbPlayFootball',
                    is_active: 1,
                    created_at: new Date(),
                },
                {
                    guid: GUIDsTenant.baloncestoGuid,
                    name: 'Baloncesto',
                    description: 'Baloncesto',
                    icon: 'GiBasketballBasket',
                    is_active: 1,
                    created_at: new Date(),
                },
                {
                    guid: GUIDsTenant.voleibolGuid,
                    name: 'Voleibol',
                    description: 'Voleibol',
                    icon: 'TbPlayVolleyball',
                    is_active: 1,
                    created_at: new Date(),
                },
                {
                    guid: GUIDsTenant.beisbolGuid,
                    name: 'Beisbol',
                    description: 'Beisbol',
                    icon: 'CiBaseball',
                    is_active: 1,
                    created_at: new Date(),
                },
                {
                    guid: GUIDsTenant.softbolGuid,
                    name: 'Rugby',
                    description: 'Rugby',
                    icon: 'MdOutlineSportsRugby',
                    is_active: 1,
                    created_at: new Date(),
                },
                {
                    guid: GUIDsTenant.futbolSalaGuid,
                    name: 'Ajedrez',
                    description: 'Ajedrez',
                    icon: 'FaChess',
                    is_active: 1,
                    created_at: new Date(),
                },
                {
                    guid: GUIDsTenant.futbolPlayaGuid,
                    name: 'Atletismo',
                    description: 'Atletismo',
                    icon: 'FaRunning',
                    is_active: 1,
                    created_at: new Date(),
                },
                {
                    guid: GUIDsTenant.futbolAmericanoGuid,
                    name: 'Futbol Americano',
                    description: 'Futbol Americano',
                    icon: 'GiAmericanFootballPlayer',
                    is_active: 1,
                    created_at: new Date(),
                },
                {
                    guid: GUIDsTenant.futbolRapidoGuid,
                    name: 'Padel',
                    description: 'Padel',
                    icon: 'IoTennisballOutline',
                    is_active: 1,
                    created_at: new Date(),
                },
                {
                    guid: GUIDsTenant.futbol7Guid,
                    name: 'Tae Kwon Do',
                    description: 'Tae Kwon Do',
                    icon: 'TbKarate',
                    is_active: 1,
                    created_at: new Date(),
                },
                {
                    guid: GUIDsTenant.tenisGuid,
                    name: 'Tenis',
                    description: 'Tenis',
                    icon: 'GiTennisRacket',
                    is_active: 1,
                    created_at: new Date(),
                },
            ],
            {}
        )
    },

    async down(queryInterface, Sequelize) {
        await queryInterface.bulkDelete('sports', null, {})
    },
}
