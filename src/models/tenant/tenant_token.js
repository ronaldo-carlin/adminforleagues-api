// Importar DataTypes de Sequelize
const { DataTypes } = require('sequelize')

// Definir la función factory para el modelo TokenTenantModel
const TokenTenantModelFactory = (sequelize) => {
    const TokenTenantModel = sequelize.define(
        'tokens',
        {
            id: {
                type: DataTypes.INTEGER,
                primaryKey: true,
                primaryKey: true,
                autoIncrement: true,
            },
            guid: {
                type: DataTypes.UUID,
                defaultValue: DataTypes.UUIDV4,
                unique: true,
            },
            user_guid: {
                type: DataTypes.INTEGER,
            },
            refresh_token: {
                type: DataTypes.TEXT,
            },
            expiry_date: {
                type: DataTypes.DATE,
            },
            created_at: {
                type: DataTypes.DATE,
            },
            revoket: {
                type: DataTypes.BOOLEAN,
            },
            revoked_at: {
                type: DataTypes.DATE,
            },
            device_info: {
                type: DataTypes.TEXT,
            },
        },
        {
            tableName: 'tokens',
            timestamps: false,
        }
    )

    return TokenTenantModel
}

module.exports = TokenTenantModelFactory
