module.exports = {
    async up(queryInterface, Sequelize) {
        // entidad para las categorias de los torneos
        await queryInterface.createTable('categories', {
            // id del registro
            id: {
                type: Sequelize.INTEGER,
                primaryKey: true,
                autoIncrement: true,
                allowNull: false,
            },
            // guid del registro
            guid: {
                type: Sequelize.UUID,
                defaultValue: Sequelize.UUIDV4,
                unique: true,
                allowNull: false,
            },
            // guid del deporte que pertenece esta categoria
            sport_guid: {
                type: Sequelize.UUID,
            },
            // nombre de la categoria
            name: {
                type: Sequelize.STRING,
            },
            // descripcion de la categoria
            description: {
                type: Sequelize.TEXT,
            },
            // logo de la categoria
            logo_path: {
                type: Sequelize.TEXT,
            },
            // color de la categoria
            color: {
                type: Sequelize.STRING,
            },
            // edad minimima para participar en la categoria
            age_min: {
                type: Sequelize.INTEGER,
            },
            // edad maxima para participar en la categoria
            age_max: {
                type: Sequelize.INTEGER,
            },
            // genero de la categoria
            gender: {
                type: Sequelize.STRING,
            },
            // nivel de la categoria
            level: {
                type: Sequelize.STRING,
            },
            // estado del registro
            is_active: {
                type: Sequelize.BOOLEAN,
                defaultValue: 1, // valor por defecto
            },
            // usuario que creo el registro
            created_by: {
                type: Sequelize.UUID,
            },
            // fecha de creacion
            created_at: {
                type: Sequelize.DATE,
                defaultValue: Sequelize.literal('CURRENT_TIMESTAMP'),
            },
            // usuario que actualizo el registro
            updated_by: {
                type: Sequelize.UUID,
            },
            // fecha de actualizacion
            updated_at: {
                type: Sequelize.DATE,
            },
            // usuario que elimino el registro
            deleted_by: {
                type: Sequelize.UUID,
            },
            // fecha de eliminacion
            deleted_at: {
                type: Sequelize.DATE,
            },
        })

        await queryInterface.addIndex('categories', ['guid'], {
            unique: true,
            name: 'uniq_categories_guid',
        })
    },

    async down(queryInterface, Sequelize) {
        await queryInterface.removeIndex('categories', 'uniq_categories_guid')
        await queryInterface.dropTable('categories')
    },
}
