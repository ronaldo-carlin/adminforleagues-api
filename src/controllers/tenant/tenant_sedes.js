const { Op } = require('sequelize')
const { v4: uuidv4 } = require('uuid')
const { matchedData } = require('express-validator')
const { handleHttpSuccess } = require('../../utils/handleSuccess')
const { handleHttpError } = require('../../utils/handleError')
const { handleHttpCount } = require('../../utils/handleCount')
const { getSequelize, getSedesModel } = require('../../helpers/sequelizeHelper')

/**
 * Este controlador es el encargado de registrar una sede
 * @param {*} req
 * @param {*} res
 */
const CreateSede = async (req, res) => {
    try {
        const sequelize = getSequelize(req)
        const SedeModel = getSedesModel(sequelize)
        const data = matchedData(req)
        const created_by = req.user.guid
        const created_at = new Date()
        const guid = await uuidv4()
        data.created_by = created_by
        data.created_at = created_at
        data.guid = guid

        const sede = await SedeModel.create(data)
        return handleHttpSuccess(res, 'Sede creada.', 201, sede)
    } catch (error) {
        console.log('Error al crear la sede:', error)
        handleHttpError(res, 'Error al crear la sede.', 403, error)
    }
}

/**
 * Este controlador es el encargado de obtener las sedes activas
 * @param {*} req
 * @param {*} res
 */
const Sedes = async (req, res) => {
    try {
        const sequelize = getSequelize(req)
        const SedeModel = getSedesModel(sequelize)

        const sedes = await SedeModel.findAll({
            where: { is_active: 1 },
        })

        if (!sedes.length) {
            handleHttpError(res, 'No se encontraron sedes.', 404)
            return
        }

        handleHttpSuccess(res, 'Sedes encontradas.', 201, sedes)
    } catch (error) {
        console.log('Error obteniendo sedes:', error)
        handleHttpError(res, 'Error obteniendo sedes.', 403, error)
    }
}

/**
 * Este controlador es el encargado de obtener una sede por GUID
 * @param {*} req
 * @param {*} res
 */
const Sede = async (req, res) => {
    try {
        const sequelize = getSequelize(req)
        const SedeModel = getSedesModel(sequelize)
        const { guid } = req.params

        const sede = await SedeModel.findOne({
            where: { guid },
        })

        if (!sede) {
            handleHttpError(res, 'La sede no existe.', 404)
            return
        }

        if (sede.is_active == 0) {
            handleHttpError(res, 'La sede ha sido deshabilitada.', 401)
            return
        }

        handleHttpSuccess(res, 'Sede encontrada.', 201, sede)
    } catch (error) {
        console.log('Error obteniendo sede:', error)
        handleHttpError(res, 'Error obteniendo sede.', 403, error)
    }
}

/**
 * Este controlador es el encargado de actualizar una sede
 * @param {*} req
 * @param {*} res
 */
const UpdateSede = async (req, res) => {
    try {
        const sequelize = getSequelize(req)
        const SedeModel = getSedesModel(sequelize)
        const { guid } = req.params
        const data = matchedData(req)
        const updated_by = req.user.guid
        const updated_at = new Date()
        data.updated_by = updated_by
        data.updated_at = updated_at

        const [updateCount] = await SedeModel.update(data, {
            where: { guid },
        })

        if (updateCount === 0) {
            return handleHttpError(res, 'La sede no existe.', 404)
        }

        const sede = await SedeModel.findOne({
            where: { guid },
        })

        if (!sede) {
            handleHttpError(res, 'La sede no existe.', 404)
            return
        }

        if (sede.is_active == 0) {
            handleHttpError(res, 'La sede ha sido deshabilitada.', 401)
            return
        }

        handleHttpSuccess(res, 'Sede actualizada.', 201, sede)
    } catch (error) {
        console.log('Error actualizando sede:', error)
        handleHttpError(res, 'Error actualizando sede.', 403, error)
    }
}

/**
 * Este controlador es el encargado de eliminar una sede
 * @param {*} req
 * @param {*} res
 */
const DeleteSede = async (req, res) => {
    try {
        const sequelize = getSequelize(req)
        const SedeModel = getSedesModel(sequelize)
        const { guid } = req.params
        const deleted_by = req.user.guid
        const deleted_at = new Date()
        const [updateCount] = await SedeModel.update(
            {
                is_active: 0,
                deleted_by,
                deleted_at,
            },
            {
                where: { guid },
            }
        )

        if (updateCount === 0) {
            return handleHttpError(res, 'La sede no existe.', 404)
        }

        handleHttpSuccess(res, 'Sede eliminada.', 201)
    } catch (error) {
        console.log('Error eliminando sede:', error)
        handleHttpError(res, 'Error eliminando sede.', 403, error)
    }
}

/**
 * Este controlador es el encargado de desactivar una sede
 * @param {*} req
 * @param {*} res
 */
const DisableSede = async (req, res) => {
    try {
        const sequelize = getSequelize(req)
        const SedeModel = getSedesModel(sequelize)
        const { guid } = req.params
        const updated_by = req.user.guid
        const updated_at = new Date()
        const [updateCount] = await SedeModel.update(
            {
                is_active: 0,
                updated_by,
                updated_at,
            },
            {
                where: { guid },
            }
        )

        if (updateCount === 0) {
            return handleHttpError(res, 'La sede no existe.', 404)
        }

        handleHttpSuccess(res, 'Sede deshabilitada.', 201)
    } catch (error) {
        console.log('Error deshabilitando sede:', error)
        handleHttpError(res, 'Error deshabilitando sede.', 403, error)
    }
}

/**
 * Este controlador es el encargado de activar una sede
 * @param {*} req
 * @param {*} res
 */
const EnableSede = async (req, res) => {
    try {
        const sequelize = getSequelize(req)
        const SedeModel = getSedesModel(sequelize)
        const { guid } = req.params
        const updated_by = req.user.guid
        const updated_at = new Date()
        const [updateCount] = await SedeModel.update(
            {
                is_active: 1,
                updated_by,
                updated_at,
            },
            {
                where: { guid },
            }
        )

        if (updateCount === 0) {
            return handleHttpError(res, 'La sede no existe.', 404)
        }

        handleHttpSuccess(res, 'Sede habilitada.', 201)
    } catch (error) {
        console.log('Error habilitando sede:', error)
        handleHttpError(res, 'Error habilitando sede.', 403, error)
    }
}

/**
 * Este controlador es el encargado de obtener todas las sedes
 * @param {*} req
 * @param {*} res
 */
const SedesAll = async (req, res) => {
    try {
        const sequelize = getSequelize(req)
        const SedeModel = getSedesModel(sequelize)

        const sedes = await SedeModel.findAll()

        if (!sedes.length) {
            handleHttpError(res, 'No se encontraron sedes.', 404)
            return
        }

        handleHttpSuccess(res, 'Sedes encontradas.', 201, sedes)
    } catch (error) {
        console.log('Error obteniendo sedes:', error)
        handleHttpError(res, 'Error obteniendo sedes.', 403, error)
    }
}

/**
 * Este controlador es el encargado de contar todas las sedes
 * @param {*} req
 * @param {*} res
 */
const SedesCount = async (req, res) => {
    try {
        const sequelize = getSequelize(req)
        const SedeModel = getSedesModel(sequelize)

        const count = await SedeModel.count()

        handleHttpCount(res, 'Sedes encontradas.', 201, count)
    } catch (error) {
        console.log('Error contando sedes:', error)
        handleHttpError(res, 'Error contando sedes.', 403, error)
    }
}

module.exports = {
    CreateSede,
    Sedes,
    Sede,
    UpdateSede,
    DeleteSede,
    DisableSede,
    EnableSede,
    SedesAll,
    SedesCount,
}
