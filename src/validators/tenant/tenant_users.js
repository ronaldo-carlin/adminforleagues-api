const { check, validationResult } = require('express-validator')
const validateResults = require('../../utils/handleValidators')
const {
    getSequelize,
    getUserModel,
    getRolesModel,
} = require('../../helpers/sequelizeHelper')

const validatorCreateUser = () => {
    return [
        async (req, res, next) => {
            const sequelize = getSequelize(req)
            const AuthTenantModel = getUserModel(sequelize)

            await check('first_name')
                .exists()
                .notEmpty()
                .isLength({ min: 1, max: 100 })
                .run(req)
            await check('last_name')
                .exists()
                .isLength({ min: 1, max: 100 })
                .run(req)
            await check('username')
                .optional()
                .notEmpty()
                .isLength({ min: 3, max: 50 })
                .custom(async (value) => {
                    const user = await AuthTenantModel.findOne({
                        where: { username: value },
                    })
                    if (user) {
                        throw new Error('El usuario ya está en uso')
                    }
                })
                .run(req)
            await check('email')
                .exists()
                .notEmpty()
                .custom(async (value) => {
                    const user = await AuthTenantModel.findOne({
                        where: { email: value },
                    })
                    if (user) {
                        throw new Error('El correo ya está en uso')
                    }
                })
                .run(req)
            await check('password')
                .optional()
                .notEmpty()
                .isLength({ min: 6, max: 100 })
                .run(req)
            await check('phone')
                .exists()
                .notEmpty()
                .isLength({ min: 10, max: 12 })
                .custom(async (value) => {
                    const user = await AuthTenantModel.findOne({
                        where: { phone: value },
                    })
                    if (user) {
                        throw new Error('El numero ya está en uso')
                    }
                })
                .run(req)
            await check('role_guid').exists().run(req)
            await check('city').optional().exists().run(req)
            await check('state').optional().exists().run(req)

            const errors = validationResult(req)
            if (!errors.isEmpty()) {
                return res.status(400).json({ errors: errors.array() })
            }

            next()
        },
    ]
}

const validatorUpdateUser = () => {
    return [
        async (req, res, next) => {
            const sequelize = getSequelize(req)
            const AuthTenantModel = getUserModel(sequelize)
            const RoleModel = getRolesModel(sequelize)

            await check('first_name')
                .isString()
                .withMessage('El nombre debe ser un string')
                .exists()
                .withMessage('El nombre es requerido')
                .notEmpty()
                .withMessage('El nombre no puede estar vacio')
                .isLength({ min: 1, max: 100 })
                .withMessage('El nombre debe tener menos de 100 caracteres')
                .run(req)

            await check('last_name')
                .isString()
                .withMessage('El apellido debe ser un string')
                .optional()
                .isLength({ min: 1, max: 100 })
                .withMessage('El apellido debe tener menos de 100 caracteres')
                .run(req)

            await check('username')
                .isString()
                .withMessage('El username debe ser un string')
                .optional()
                .notEmpty()
                .withMessage('El username no puede estar vacio')
                .isLength({ min: 3, max: 50 })
                .withMessage('El username debe tener entre 3 y 50 caracteres')
                .custom(async (value) => {
                    const user = await AuthTenantModel.findOne({
                        where: { username: value },
                    })
                    if (user && user.username !== req.body.username) {
                        throw new Error('El usuario ya está en uso')
                    }
                })
                .matches(/^[a-zA-Z0-9]*$/)
                .withMessage('El username solo puede contener letras y números')
                .run(req)

            await check('phone')
                .isString()
                .withMessage('El telefono debe ser un string')
                .optional()
                .notEmpty()
                .withMessage('El telefono no puede estar vacio')
                .isLength({ min: 10, max: 10 })
                .withMessage('El telefono debe tener 10 digitos')
                .custom(async (value) => {
                    const user = await AuthTenantModel.findOne({
                        where: { phone: value },
                    })
                    if (user && user.phone !== req.body.phone) {
                        throw new Error('El numero ya está en uso')
                    }
                })
                .run(req)

            await check('email')
                .isEmail()
                .withMessage('El correo no es valido')
                .exists()
                .withMessage('El correo es requerido')
                .notEmpty()
                .withMessage('El correo no puede estar vacio')
                .isLength({ min: 1, max: 256 })
                .withMessage('El correo debe tener menos de 256 caracteres')
                .custom(async (value) => {
                    const user = await AuthTenantModel.findOne({
                        where: { email: value },
                    })
                    if (user && user.email !== req.body.email) {
                        throw new Error('El correo ya está en uso')
                    }
                })
                .run(req)

            await check('role_guid')
                .isUUID()
                .withMessage('El rol debe ser un GUID')
                .exists()
                .withMessage('El rol es requerido')
                .notEmpty()
                .withMessage('El rol no puede estar vacio')
                .custom(async (value) => {
                    const role = await RoleModel.findOne({
                        where: { guid: value },
                    })
                    if (!role) {
                        throw new Error('El rol no existe')
                    }
                })
                .run(req)

            await check('country').optional().run(req)
            await check('city').optional().isString().run(req)
            await check('state').optional().isString().run(req)
            await check('facebook').optional().isString().run(req)
            await check('instagram').optional().isString().run(req)
            await check('two_factor_auth').optional().isBoolean().run(req)
            await check('weeklyNewsletter').optional().isBoolean().run(req)
            await check('lifecycleEmails').optional().isBoolean().run(req)
            await check('promotionalEmails').optional().isBoolean().run(req)
            await check('productUpdates').optional().isBoolean().run(req)

            const errors = validationResult(req)
            if (!errors.isEmpty()) {
                return res.status(400).json({ errors: errors.array() })
            }

            next()
        },
    ]
}

const ValidatorDeleteUser = () => {
    return [
        check('guid')
            .isUUID()
            .withMessage('El GUID es requerido para deshabilitar el pago.'),
        (req, res, next) => {
            return validateResults(req, res, next)
        },
    ]
}

const ValidatorDisableUser = () => {
    return [
        check('guid')
            .isUUID()
            .withMessage('El GUID es requerido para deshabilitar el pago.'),
        (req, res, next) => {
            return validateResults(req, res, next)
        },
    ]
}

const ValidatorEnableUser = () => {
    return [
        check('guid')
            .isUUID()
            .withMessage('El GUID es requerido para deshabilitar el pago.'),
        (req, res, next) => {
            return validateResults(req, res, next)
        },
    ]
}

const ValidatorPasswordResetUser = () => {
    return [
        check('guid')
            .isUUID()
            .withMessage('El GUID es requerido para deshabilitar el pago.'),
        (req, res, next) => {
            return validateResults(req, res, next)
        },
    ]
}

module.exports = {
    validatorCreateUser,
    validatorUpdateUser,
    ValidatorDeleteUser,
    ValidatorDisableUser,
    ValidatorEnableUser,
    ValidatorPasswordResetUser,
}
