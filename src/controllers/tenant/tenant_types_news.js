const { Op } = require('sequelize')
const { v4: uuidv4 } = require('uuid')
const { matchedData } = require('express-validator')
const { handleHttpSuccess } = require('../../utils/handleSuccess')
const { handleHttpError } = require('../../utils/handleError')
const { handleHttpCount } = require('../../utils/handleCount')
const {
    getSequelize,
    getTypesNewsModel,
} = require('../../helpers/sequelizeHelper')

/**
 * Este controlador es el encargado de registrar un tipo de noticia
 * @param {*} req
 * @param {*} res
 */
const CreateTypeNew = async (req, res) => {
    try {
        const sequelize = getSequelize(req)
        const TypesNewsModel = getTypesNewsModel(sequelize)
        const data = matchedData(req)
        const created_by = req.user.guid
        const created_at = new Date()
        const guid = await uuidv4()
        data.created_by = created_by
        data.created_at = created_at
        data.guid = guid

        const typeNews = await TypesNewsModel.create(data)
        return handleHttpSuccess(res, 'Tipo de noticia creado.', 201, typeNews)
    } catch (error) {
        console.log('Error al crear el tipo de noticia:', error)
        handleHttpError(res, 'Error al crear el tipo de noticia.', 403, error)
    }
}

/**
 * Este controlador es el encargado de obtener los tipos de noticias activos
 * @param {*} req
 * @param {*} res
 */
const TypesNews = async (req, res) => {
    try {
        const sequelize = getSequelize(req)
        const TypesNewsModel = getTypesNewsModel(sequelize)

        const typeNews = await TypesNewsModel.findAll({
            where: { is_active: 1 },
        })

        if (!typeNews.length) {
            handleHttpError(res, 'No se encontraron tipos de noticias.', 404)
            return
        }

        handleHttpSuccess(res, 'Tipos de noticias encontrados.', 201, typeNews)
    } catch (error) {
        console.log('Error obteniendo tipos de noticias:', error)
        handleHttpError(res, 'Error obteniendo tipos de noticias.', 403, error)
    }
}

/**
 * Este controlador es el encargado de obtener un tipo de noticia por GUID
 * @param {*} req
 * @param {*} res
 */
const TypeNew = async (req, res) => {
    try {
        const sequelize = getSequelize(req)
        const TypesNewsModel = getTypesNewsModel(sequelize)
        const { guid } = req.params

        const typeNews = await TypesNewsModel.findOne({
            where: { guid },
        })

        if (!typeNews) {
            handleHttpError(res, 'El tipo de noticia no existe.', 404)
            return
        }

        if (typeNews.is_active == 0) {
            handleHttpError(
                res,
                'El tipo de noticia ha sido deshabilitado.',
                401
            )
            return
        }

        handleHttpSuccess(res, 'Tipo de noticia encontrado.', 201, typeNews)
    } catch (error) {
        console.log('Error obteniendo tipo de noticia:', error)
        handleHttpError(res, 'Error obteniendo tipo de noticia.', 403, error)
    }
}

/**
 * Este controlador es el encargado de actualizar un tipo de noticia
 * @param {*} req
 * @param {*} res
 */
const UpdateTypeNew = async (req, res) => {
    try {
        const sequelize = getSequelize(req)
        const TypesNewsModel = getTypesNewsModel(sequelize)
        const { guid } = req.params
        const data = matchedData(req)
        const updated_by = req.user.guid
        const updated_at = new Date()
        data.updated_by = updated_by
        data.updated_at = updated_at

        const [updateCount] = await TypesNewsModel.update(data, {
            where: { guid },
        })

        if (updateCount === 0) {
            return handleHttpError(res, 'El tipo de noticia no existe.', 404)
        }

        const typeNews = await TypesNewsModel.findOne({
            where: { guid },
        })

        if (!typeNews) {
            handleHttpError(res, 'El tipo de noticia no existe.', 404)
            return
        }

        if (typeNews.is_active == 0) {
            handleHttpError(
                res,
                'El tipo de noticia ha sido deshabilitado.',
                401
            )
            return
        }

        handleHttpSuccess(res, 'Tipo de noticia actualizado.', 201, typeNews)
    } catch (error) {
        console.log('Error actualizando tipo de noticia:', error)
        handleHttpError(res, 'Error actualizando tipo de noticia.', 403, error)
    }
}

/**
 * Este controlador es el encargado de eliminar un tipo de noticia
 * @param {*} req
 * @param {*} res
 */
const DeleteTypeNew = async (req, res) => {
    try {
        const sequelize = getSequelize(req)
        const TypesNewsModel = getTypesNewsModel(sequelize)
        const { guid } = req.params
        const deleted_by = req.user.guid
        const deleted_at = new Date()
        const [updateCount] = await TypesNewsModel.update(
            {
                is_active: 0,
                deleted_by,
                deleted_at,
            },
            {
                where: { guid },
            }
        )

        if (updateCount === 0) {
            return handleHttpError(res, 'El tipo de noticia no existe.', 404)
        }

        handleHttpSuccess(res, 'Tipo de noticia eliminado.', 201)
    } catch (error) {
        console.log('Error eliminando tipo de noticia:', error)
        handleHttpError(res, 'Error eliminando tipo de noticia.', 403, error)
    }
}

/**
 * Este controlador es el encargado de desactivar un tipo de noticia
 * @param {*} req
 * @param {*} res
 */
const DisableTypeNew = async (req, res) => {
    try {
        const sequelize = getSequelize(req)
        const TypesNewsModel = getTypesNewsModel(sequelize)
        const { guid } = req.params
        const updated_by = req.user.guid
        const updated_at = new Date()
        const [updateCount] = await TypesNewsModel.update(
            {
                is_active: 0,
                updated_by,
                updated_at,
            },
            {
                where: { guid },
            }
        )

        if (updateCount === 0) {
            return handleHttpError(res, 'El tipo de noticia no existe.', 404)
        }

        handleHttpSuccess(res, 'Tipo de noticia deshabilitado.', 201)
    } catch (error) {
        console.log('Error deshabilitando tipo de noticia:', error)
        handleHttpError(
            res,
            'Error deshabilitando tipo de noticia.',
            403,
            error
        )
    }
}

/**
 * Este controlador es el encargado de activar un tipo de noticia
 * @param {*} req
 * @param {*} res
 */
const EnableTypeNew = async (req, res) => {
    try {
        const sequelize = getSequelize(req)
        const TypesNewsModel = getTypesNewsModel(sequelize)
        const { guid } = req.params
        const updated_by = req.user.guid
        const updated_at = new Date()
        const [updateCount] = await TypesNewsModel.update(
            {
                is_active: 1,
                updated_by,
                updated_at,
            },
            {
                where: { guid },
            }
        )

        if (updateCount === 0) {
            return handleHttpError(res, 'El tipo de noticia no existe.', 404)
        }

        handleHttpSuccess(res, 'Tipo de noticia habilitado.', 201)
    } catch (error) {
        console.log('Error habilitando tipo de noticia:', error)
        handleHttpError(res, 'Error habilitando tipo de noticia.', 403, error)
    }
}

/**
 * Este controlador es el encargado de obtener todos los tipos de noticias
 * @param {*} req
 * @param {*} res
 */
const TypesNewsAll = async (req, res) => {
    try {
        const sequelize = getSequelize(req)
        const TypesNewsModel = getTypesNewsModel(sequelize)

        const typeNews = await TypesNewsModel.findAll()

        if (!typeNews.length) {
            handleHttpError(res, 'No se encontraron tipos de noticias.', 404)
            return
        }

        handleHttpSuccess(res, 'Tipos de noticias encontrados.', 201, typeNews)
    } catch (error) {
        console.log('Error obteniendo tipos de noticias:', error)
        handleHttpError(res, 'Error obteniendo tipos de noticias.', 403, error)
    }
}

/**
 * Este controlador es el encargado de contar todos los tipos de noticias
 * @param {*} req
 * @param {*} res
 */
const TypesNewsCount = async (req, res) => {
    try {
        const sequelize = getSequelize(req)
        const TypesNewsModel = getTypesNewsModel(sequelize)

        const count = await TypesNewsModel.count()

        handleHttpCount(res, 'Tipos de noticias encontrados.', 201, count)
    } catch (error) {
        console.log('Error contando tipos de noticias:', error)
        handleHttpError(res, 'Error contando tipos de noticias.', 403, error)
    }
}

module.exports = {
    CreateTypeNew,
    TypesNews,
    TypeNew,
    UpdateTypeNew,
    DeleteTypeNew,
    DisableTypeNew,
    EnableTypeNew,
    TypesNewsAll,
    TypesNewsCount,
}
