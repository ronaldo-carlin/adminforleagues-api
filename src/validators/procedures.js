const { check } = require('express-validator')
const validateResults = require('../utils/handleValidators')

const validatorCreateProcedures = [
    check('Nombre').exists().notEmpty(),
    check('CreadoPor').exists().notEmpty(),
    (req, res, next) => {
        validateResults(req, res, next)
    },
]

const validatorUpdateProcedures = [
    check('Nombre').exists().notEmpty(),
    check('Id').exists().notEmpty(),
    check('ActualizadoPor').exists().notEmpty(),
    (req, res, next) => {
        validateResults(req, res, next)
    },
]

const validatorDeleteProcedures = [
    check('Id').exists().notEmpty(),
    (req, res, next) => {
        validateResults(req, res, next)
    },
]

module.exports = {
    validatorCreateProcedures,
    validatorUpdateProcedures,
    validatorDeleteProcedures,
}
