module.exports = {
    async up(queryInterface, Sequelize) {
        // entidad para almacenar los tipos de competencias que se pueden usar ya con sus reglas relacionadas
        // por ejemplo: torneo de eliminacion directa, torneo de grupos, torneo de grupos y eliminacion directa
        await queryInterface.createTable('types_competitions', {
            // id del registro
            id: {
                type: Sequelize.INTEGER,
                primaryKey: true,
                autoIncrement: true,
                allowNull: false,
            },
            // guid del registro
            guid: {
                type: Sequelize.UUID,
                defaultValue: Sequelize.UUIDV4,
                unique: true,
                allowNull: false,
            },
            // deporte al que pertenece el tipo de competencia
            sport_guid: {
                type: Sequelize.UUID,
            },
            // nombre del tipo de competencia
            name: {
                type: Sequelize.TEXT,
            },
            // descripcion del tipo de competencia
            description: {
                type: Sequelize.TEXT,
            },
            // icono de la competencia
            icon: {
                type: Sequelize.TEXT,
            },
            // estado del registro
            is_active: {
                type: Sequelize.BOOLEAN,
                defaultValue: 1, // valor por defecto
            },
            // usuario que creo el registro
            created_by: {
                type: Sequelize.Sequelize.UUID,
            },
            // fecha de creacion
            created_at: {
                type: Sequelize.DATE,
                defaultValue: Sequelize.literal('CURRENT_TIMESTAMP'),
            },
            // usuario que actualizo el registro
            updated_by: {
                type: Sequelize.Sequelize.UUID,
            },
            // fecha de actualizacion
            updated_at: {
                type: Sequelize.DATE,
            },
            // usuario que elimino el registro
            deleted_by: {
                type: Sequelize.Sequelize.UUID,
            },
            // fecha de eliminacion
            deleted_at: {
                type: Sequelize.DATE,
            },
        })

        // indices
        await queryInterface.addIndex('types_competitions', ['guid'], {
            unique: true,
            name: 'uniq_types_competitions_guid',
        })

        await queryInterface.addIndex('types_competitions', ['sport_guid'], {
            name: 'idx_sport_guid',
        })

        // llaves foraneas
        await queryInterface.addConstraint('types_competitions', {
            fields: ['sport_guid'],
            type: 'foreign key',
            name: 'fk_sport_guid',
            references: {
                table: 'sports',
                field: 'guid',
            },
            onDelete: 'restrict',
            onUpdate: 'restrict',
        })
    },

    async down(queryInterface, Sequelize) {
        await queryInterface.removeConstraint(
            'types_competitions',
            'fk_sport_guid'
        )
        await queryInterface.removeIndex(
            'types_competitions',
            'uniq_types_competitions_guid'
        )
        await queryInterface.removeIndex('types_competitions', 'idx_sport_guid')
        await queryInterface.dropTable('types_competitions')
    },
}
