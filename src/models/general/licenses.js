// Importar Sequelize directamente
const { Sequelize } = require('sequelize')
// Importar tu configuración de sequelize
const { sequelize } = require('../../database/mysql')
// Importar el modelo de planes
const PlansModel = require('./plans')

const LicensesModel = sequelize.define(
    'licenses',
    {
        id: {
            type: Sequelize.INTEGER,
            primaryKey: true,
            autoIncrement: true,
        },
        guid: {
            type: Sequelize.UUID,
            defaultValue: Sequelize.UUIDV4,
            unique: true,
        },
        plan_guid: {
            type: Sequelize.UUID,
        },
        name: {
            type: Sequelize.TEXT,
        },
        description: {
            type: Sequelize.TEXT,
        },
        price: {
            type: Sequelize.DECIMAL,
        },
        users: {
            type: Sequelize.INTEGER,
        },
        teams: {
            type: Sequelize.INTEGER,
        },
        matches: {
            type: Sequelize.INTEGER,
        },
        competitions: {
            type: Sequelize.INTEGER,
        },
        sedes: {
            type: Sequelize.INTEGER,
        },
        sponsors: {
            type: Sequelize.INTEGER,
        },
        publications: {
            type: Sequelize.INTEGER,
        },
        players: {
            type: Sequelize.INTEGER,
        },
        coaches: {
            type: Sequelize.INTEGER,
        },
        referees: {
            type: Sequelize.INTEGER,
        },
        reports: {
            type: Sequelize.INTEGER,
        },
        duration_days: {
            type: Sequelize.INTEGER,
        },
        matches_summary: {
            type: Sequelize.STRING,
        },
        matches_reports: {
            type: Sequelize.STRING,
        },
        ads: {
            type: Sequelize.INTEGER,
        },
        is_active: {
            type: Sequelize.INTEGER,
        },
        created_by: {
            type: Sequelize.UUID,
        },
        created_at: {
            type: Sequelize.DATE,
        },
        updated_by: {
            type: Sequelize.UUID,
        },
        updated_at: {
            type: Sequelize.DATE,
        },
        deleted_by: {
            type: Sequelize.UUID,
        },
        deleted_at: {
            type: Sequelize.DATE,
        },
    },
    {
        tableName: 'licenses',
        timestamps: false,
        defaultScope: {
            attributes: {
                exclude: [
                    'created_by',
                    'updated_by',
                    'deleted_by',
                    'created_at',
                    'updated_at',
                    'deleted_at',
                ],
            },
            include: [
                {
                    model: PlansModel,
                    as: 'plan',
                    where: { is_active: 1 },
                    exclude: [
                        'is_active',
                        'created_at',
                        'created_by',
                        'updated_at',
                        'updated_by',
                        'deleted_at',
                        'deleted_by',
                    ],
                },
            ],
        },
    }
)

// Establecer la relación entre planes y licencias
LicensesModel.belongsTo(PlansModel, {
    foreignKey: 'plan_guid', // Llave foránea
    targetKey: 'guid', // Llave primaria
    as: 'plan', // Alias
})

module.exports = LicensesModel
