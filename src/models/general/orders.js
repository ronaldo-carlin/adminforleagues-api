// Importar Sequelize directamente
const { Sequelize } = require('sequelize')
// Importar tu configuración de sequelize
const { sequelize } = require('../../database/mysql')
// Importar el modelo de usuarios
const AuthModel = require('./auth')
// Importar el modelo de planes
const PlansModel = require('./plans')
// Importar el modelo de cupones
const CouponsModel = require('./coupons')

const OrdersModel = sequelize.define(
    'orders',
    {
        id: {
            type: Sequelize.INTEGER,
            primaryKey: true,
            autoIncrement: true,
        },
        guid: {
            type: Sequelize.UUID,
            defaultValue: Sequelize.UUIDV4,
            unique: true,
        },
        user_guid: {
            type: Sequelize.UUID,
        },
        plan_guid: {
            type: Sequelize.UUID,
        },
        coupon_guid: {
            type: Sequelize.UUID,
        },
        transaction: {
            type: Sequelize.TEXT,
        },
        payment_method: {
            type: Sequelize.TEXT,
        },
        amount: {
            type: Sequelize.DECIMAL,
        },
        currency: {
            type: Sequelize.TEXT,
        },
        currency_icon: {
            type: Sequelize.TEXT,
        },
        enroll_start: {
            type: Sequelize.DATE,
        },
        enroll_end: {
            type: Sequelize.DATE,
        },
        expiration_date: {
            type: Sequelize.DATE,
        },
        renewal_date: {
            type: Sequelize.DATE,
        },
        cancel_date: {
            type: Sequelize.DATE,
        },
        cancel_type: {
            type: Sequelize.TEXT,
        },
        is_active: {
            type: Sequelize.INTEGER,
        },
        created_by: {
            type: Sequelize.UUID,
        },
        updated_by: {
            type: Sequelize.UUID,
        },
        deleted_by: {
            type: Sequelize.UUID,
        },
        created_at: {
            type: Sequelize.DATE,
        },
        updated_at: {
            type: Sequelize.DATE,
        },
        deleted_at: {
            type: Sequelize.DATE,
        },
    },
    {
        tableName: 'orders',
        timestamps: false,
        defaultScope: {
            attributes: {
                exclude: [
                    'created_by',
                    'updated_by',
                    'deleted_by',
                    'created_at',
                    'updated_at',
                    'deleted_at',
                ],
            },
            include: [
                {
                    model: AuthModel,
                    as: 'user',
                    exclude: [
                        'password',
                        'is_active',
                        'created_at',
                        'created_by',
                        'updated_at',
                        'updated_by',
                        'deleted_at',
                        'deleted_by',
                    ],
                },
                {
                    model: PlansModel,
                    as: 'plan',
                    exclude: [
                        'is_active',
                        'created_at',
                        'created_by',
                        'updated_at',
                        'updated_by',
                        'deleted_at',
                        'deleted_by',
                    ],
                },
            ],
        },
    }
)

// Relacion entre ordenes y usuarios
OrdersModel.belongsTo(AuthModel, {
    foreignKey: 'user_guid', // El campo de la tabla orders que se usará para la relación
    targetKey: 'guid', // El campo de la tabla auth que se usará para la relación
    as: 'user', // El alias de la relación
})

// Relacion entre ordenes y planes
OrdersModel.belongsTo(PlansModel, {
    foreignKey: 'plan_guid', // El campo de la tabla orders que se usará para la relación
    targetKey: 'guid', // El campo de la tabla plans que se usará para la relación
    as: 'plan', // El alias de la relación
})

// Relacion entre ordenes y cupones
OrdersModel.belongsTo(CouponsModel, {
    foreignKey: 'coupon_guid', // El campo de la tabla orders que se usará para la relación
    targetKey: 'guid', // El campo de la tabla coupons que se usará para la relación
    as: 'coupon', // El alias de la relación
    allowNull: true, // Permitir valores nulos
    required: false, // No requerir la relación
})

module.exports = OrdersModel
