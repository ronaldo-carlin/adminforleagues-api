const { check } = require('express-validator')
const validateResults = require('../../utils/handleValidators')
const User = require('../../models/general/auth')

const validatorCreateUser = [
    check('first_name').exists().notEmpty().isLength({ min: 1, max: 100 }),
    check('last_name').exists().isLength({ min: 1, max: 100 }),
    check('city').exists(),
    check('state').exists(),
    check('phone').exists().notEmpty().isLength({ min: 6, max: 20 }),
    check('email')
        .exists()
        .notEmpty()
        .custom(async (value) => {
            // Verificar si el correo ya existe en la base de datos
            const user = await User.findOne({
                where: { email: value },
            })
            if (user) {
                throw new Error('El correo ya está en uso')
            }
        }),
    check('password').exists().notEmpty().isLength({ min: 6, max: 100 }),
    check('role_guid').exists(),
    (req, res, next) => {
        return validateResults(req, res, next)
    },
]

const validatorUpdateUser = [
    check('guid')
        .isUUID()
        .withMessage('El GUID es requerido para deshabilitar el pago.'),
    check('first_name').exists().notEmpty().isLength({ min: 1, max: 100 }),
    check('last_name').exists().isLength({ min: 1, max: 100 }),
    check('city').exists(),
    check('state').exists(),
    check('phone').exists().notEmpty().isLength({ min: 6, max: 20 }),
    check('username').exists().notEmpty().isLength({ min: 1, max: 100 }),
    check('email').exists().notEmpty().isLength({ min: 1, max: 100 }),
    check('password').exists(),
    check('role_guid').exists(),

    (req, res, next) => {
        return validateResults(req, res, next)
    },
]

const ValidatorDeleteUser = [
    check('guid')
        .isUUID()
        .withMessage('El GUID es requerido para deshabilitar el pago.'),
    validateResults,
]

const ValidatorDisableUser = [
    check('guid')
        .isUUID()
        .withMessage('El GUID es requerido para deshabilitar el pago.'),
    validateResults,
]

const ValidatorEnableUser = [
    check('guid')
        .isUUID()
        .withMessage('El GUID es requerido para deshabilitar el pago.'),
    validateResults,
]

const ValidatorPasswordResetUser = [
    check('guid')
        .isUUID()
        .withMessage('El GUID es requerido para deshabilitar el pago.'),
    validateResults,
]

module.exports = {
    validatorCreateUser,
    validatorUpdateUser,
    ValidatorDeleteUser,
    ValidatorDisableUser,
    ValidatorEnableUser,
    ValidatorPasswordResetUser,
}
