const bcryptjs = require('bcryptjs')

/**
 * Contraseña sin encriptar
 */
const encrypt = async (passwordPlain) => {
    console.log(passwordPlain)
    const hash = await bcryptjs.hash(passwordPlain, 10)

    return hash
}

/**
 * Comparar contraseñas
 * @param {*} passwordPlain
 * @param {*} passwordCrypted
 * @returns
 */
const compare = async (passwordPlain, passwordCrypted) => {
    return (hash = await bcryptjs.compare(passwordPlain, passwordCrypted))
}

module.exports = { encrypt, compare }
