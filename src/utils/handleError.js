const handleHttpError = (
    res,
    msg = 'Algo ha ocurrido, intentelo nuevamente',
    code = 403,
    error
) => {
    res.status(code)

    res.send({
        code: code,
        success: false,
        message: msg,
        error: error,
    })
}

module.exports = { handleHttpError }
