const { check } = require('express-validator')
const validateResults = require('../../utils/handleValidators')
const Roles = require('../../models/general/roles')

// Validaciones para crear un rol
const ValidatorCreateRol = [
    check('name').isString().withMessage('El nombre es requerido.'),
    check('description').isString().withMessage('La descripción es requerida.'),
    validateResults,
]

// Validaciones para obtener un rol
const ValidatorRol = [
    // checamos el guid que sea tipo guid
    check('guid').isUUID().withMessage('El guid es requerido.'),
    validateResults,
]

// Validaciones para actualizar un rol
const ValidatorUpdateRol = [
    check('guid').isUUID().withMessage('El guid es requerido.'),
    check('name').isString().withMessage('El nombre es requerido.'),
    check('description').isString().withMessage('La descripción es requerida.'),
    validateResults,
]

// Validaciones para eliminar un rol
const ValidatorDeleteRol = [
    check('guid').isUUID().withMessage('El guid es requerido.'),
    validateResults,
]

// Validaciones para deshabilitar un rol
const ValidatorDisableRol = [
    check('guid').isUUID().withMessage('El guid es requerido.'),
    validateResults,
]

// Validaciones para habilitar un rol
const ValidatorEnableRol = [
    check('guid').isUUID().withMessage('El guid es requerido.'),
    validateResults,
]

module.exports = {
    ValidatorCreateRol,
    ValidatorRol,
    ValidatorUpdateRol,
    ValidatorDeleteRol,
    ValidatorDisableRol,
    ValidatorEnableRol,
}
