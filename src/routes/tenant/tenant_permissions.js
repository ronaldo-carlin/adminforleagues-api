const express = require('express')
const router = express.Router()

// Controladores de permisos y middleware
const {
    ValidatorCreatePermission,
} = require('../../validators/tenant/tenant_permissions')
const {
    Permissions,
    CreatePermission,
} = require('../../controllers/tenant/tenant_permissions')
const authMiddleware = require('../../middleware/session_tenant')

/* Rutas protegidas que requieren autenticación */
/**
 * @swagger
 * tags:
 *   name: Tenant_Permissions
 *   description: API para la gestión de permisos de los usuarios
 */

/**
 * @swagger
 * /tenant_permissions/CreatePermission:
 *  post:
 *    summary: Obtener los permisos de un usuario
 *    tags: [Tenant_Permissions]
 *    description: Obtiene los permisos activos de un usuario
 *    requestBody:
 *       required: true
 *       content:
 *         application/json:
 *           schema:
 *             type: object
 *             properties:
 *               name:
 *                 type: string
 *                 example: create
 *               description:
 *                 type: string
 *                 example: permiso para crear
 *    responses:
 *       201:
 *         description: Permiso creado exitosamente
 *         content:
 *           application/json:
 *             schema:
 *               $ref: '#/components/responses/success'
 *       400:
 *         description: Error en los datos enviados
 *         content:
 *           application/json:
 *             schema:
 *               $ref: '#/components/responses/BadRequest'
 *       403:
 *         description: Error de autenticación
 *         content:
 *           application/json:
 *             schema:
 *               $ref: '#/components/responses/UnauthorizedError'
 *       409:
 *         description: El permiso ya existe
 *         content:
 *           application/json:
 *             schema:
 *               $ref: '#/components/responses/ConflictError'
 *       500:
 *         description: Error en el servidor
 *         content:
 *           application/json:
 *             schema:
 *               $ref: '#/components/responses/InternalServerError'
 *    security:
 *      - bearerAuth: []
 */
router.post(
    '/CreatePermission',
    authMiddleware,
    ValidatorCreatePermission,
    CreatePermission
)

/**
 * @swagger
 * /tenant_permissions/permissions:
 *  get:
 *    summary: Obtener los permisos de un usuario
 *    tags: [Tenant_Permissions]
 *    description: Obtiene los permisos activos de un usuario
 *    responses:
 *      200:
 *        description: Permisos obtenidos exitosamente
 *        content:
 *          application/json:
 *            schema:
 *              $ref: '#/components/responses/success'
 *      403:
 *        description: Error de autenticación
 *        content:
 *          application/json:
 *            schema:
 *              $ref: '#/components/responses/UnauthorizedError'
 *      500:
 *        description: Error en el servidor
 *        content:
 *          application/json:
 *            schema:
 *              $ref: '#/components/responses/InternalServerError'
 *    security:
 *      - bearerAuth: []
 */
router.get('/Permissions', authMiddleware, Permissions)

module.exports = router
