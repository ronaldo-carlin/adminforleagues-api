const { check, validationResult } = require('express-validator')

// Validación para crear un nuevo tipo de competición
const validatorCreateTypesCompetition = () => {
    return [
        async (req, res, next) => {
            await check('name')
                .notEmpty()
                .withMessage('El nombre del tipo de competición es requerido.')
                .run(req)
            await check('description')
                .optional()
                .isString()
                .withMessage('La descripción debe ser una cadena de texto.')
                .run(req)

            const errors = validationResult(req)
            if (!errors.isEmpty()) {
                return res.status(400).json({ errors: errors.array() })
            }

            next()
        },
    ]
}

// Validación para obtener un tipo de competición por GUID
const validatorTypesCompetition = () => {
    return [
        async (req, res, next) => {
            await check('guid')
                .isUUID()
                .withMessage('El GUID del tipo de competición es requerido.')
                .run(req)

            const errors = validationResult(req)
            if (!errors.isEmpty()) {
                return res.status(400).json({ errors: errors.array() })
            }

            next()
        },
    ]
}

// Validación para actualizar un tipo de competición
const validatorUpdateTypesCompetition = () => {
    return [
        async (req, res, next) => {
            await check('guid')
                .isUUID()
                .withMessage('El GUID del tipo de competición es requerido.')
                .run(req)
            await check('name')
                .optional()
                .notEmpty()
                .withMessage('El nombre del tipo de competición es requerido.')
                .run(req)
            await check('description')
                .optional()
                .isString()
                .withMessage('La descripción debe ser una cadena de texto.')
                .run(req)

            const errors = validationResult(req)
            if (!errors.isEmpty()) {
                return res.status(400).json({ errors: errors.array() })
            }

            next()
        },
    ]
}

// Validación para eliminar un tipo de competición
const validatorDeleteTypesCompetition = () => {
    return [
        async (req, res, next) => {
            await check('guid')
                .isUUID()
                .withMessage('El GUID del tipo de competición es requerido.')
                .run(req)

            const errors = validationResult(req)
            if (!errors.isEmpty()) {
                return res.status(400).json({ errors: errors.array() })
            }

            next()
        },
    ]
}

// Validación para deshabilitar un tipo de competición
const validatorDisableTypesCompetition = () => {
    return [
        async (req, res, next) => {
            await check('guid')
                .isUUID()
                .withMessage('El GUID del tipo de competición es requerido.')
                .run(req)

            const errors = validationResult(req)
            if (!errors.isEmpty()) {
                return res.status(400).json({ errors: errors.array() })
            }

            next()
        },
    ]
}

// Validación para habilitar un tipo de competición
const validatorEnableTypesCompetition = () => {
    return [
        async (req, res, next) => {
            await check('guid')
                .isUUID()
                .withMessage('El GUID del tipo de competición es requerido.')
                .run(req)

            const errors = validationResult(req)
            if (!errors.isEmpty()) {
                return res.status(400).json({ errors: errors.array() })
            }

            next()
        },
    ]
}

module.exports = {
    validatorCreateTypesCompetition,
    validatorTypesCompetition,
    validatorUpdateTypesCompetition,
    validatorDeleteTypesCompetition,
    validatorDisableTypesCompetition,
    validatorEnableTypesCompetition,
}
