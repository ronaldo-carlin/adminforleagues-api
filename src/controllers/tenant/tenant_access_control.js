const { Op } = require('sequelize')
const { v4: uuidv4 } = require('uuid')
const { matchedData } = require('express-validator')
const { handleHttpSuccess } = require('../../utils/handleSuccess')
const { handleHttpError } = require('../../utils/handleError')
const { handleHttpCount } = require('../../utils/handleCount')
const {
    getSequelize,
    getAccessControlModel,
} = require('../../helpers/sequelizeHelper')

/**
 * Este controlador es el encargado de registrar un control de acceso
 * @param {*} req
 * @param {*} res
 */
const CreateAccessControl = async (req, res) => {
    try {
        const sequelize = getSequelize(req)
        const AccessControlModel = getAccessControlModel(sequelize)
        const data = matchedData(req)
        const created_by = req.user.guid
        const created_at = new Date()
        const guid = await uuidv4()
        data.created_by = created_by
        data.created_at = created_at
        data.guid = guid

        const accessControl = await AccessControlModel.create(data)
        return handleHttpSuccess(
            res,
            'Control de acceso creado.',
            201,
            accessControl
        )
    } catch (error) {
        console.log('Error al crear el control de acceso:', error)
        handleHttpError(res, 'Error al crear el control de acceso.', 403, error)
    }
}

/**
 * Este controlador es el encargado de obtener los controles de acceso activos
 * @param {*} req
 * @param {*} res
 */
const AccessControls = async (req, res) => {
    try {
        const sequelize = getSequelize(req)
        const AccessControlModel = getAccessControlModel(sequelize)

        const accessControls = await AccessControlModel.findAll({
            where: { is_active: 1 },
            include: [
                {
                    association: 'permission',
                    attributes: ['id'],
                },
            ],
        })

        if (!accessControls.length) {
            handleHttpError(res, 'No se encontraron controles de acceso.', 404)
            return
        }

        /// Agrupar los roles, módulos y sus permisos
        const groupedData = accessControls.reduce((acc, control) => {
            // Buscar si el rol ya existe en el array acumulado
            const existingRoleIndex = acc.findIndex(
                (item) => item.role.guid === control.role.guid
            )

            const permission = {
                id: control.permission.id,
                guid: control.permission.guid,
                name: control.permission.name,
            }

            // Si el rol ya existe
            if (existingRoleIndex > -1) {
                // Buscar si el módulo ya existe dentro del rol
                const existingModuleIndex = acc[
                    existingRoleIndex
                ].modules.findIndex((mod) => mod.guid === control.module.guid)

                // Si el módulo ya existe, agregar el permiso
                if (existingModuleIndex > -1) {
                    acc[existingRoleIndex].modules[
                        existingModuleIndex
                    ].permissions.push(permission)
                } else {
                    // Si el módulo no existe, agregar un nuevo módulo con el primer permiso
                    acc[existingRoleIndex].modules.push({
                        guid: control.module.guid,
                        name: control.module.name,
                        permissions: [permission],
                    })
                }
            } else {
                // Si el rol no existe, agregar un nuevo rol con el primer módulo y permiso
                acc.push({
                    id: control.id,
                    guid: control.guid,
                    is_active: control.is_active,
                    role: {
                        guid: control.role.guid,
                        name: control.role.name,
                    },
                    modules: [
                        {
                            guid: control.module.guid,
                            name: control.module.name,
                            permissions: [permission],
                        },
                    ],
                })
            }

            return acc
        }, [])

        handleHttpSuccess(
            res,
            'Controles de acceso encontrados.',
            201,
            groupedData
        )
    } catch (error) {
        console.log('Error obteniendo controles de acceso:', error)
        handleHttpError(
            res,
            'Error obteniendo controles de acceso.',
            403,
            error
        )
    }
}

/**
 * Este controlador es el encargado de obtener un control de acceso por GUID
 * @param {*} req
 * @param {*} res
 */
const AccessControl = async (req, res) => {
    try {
        const sequelize = getSequelize(req)
        const AccessControlModel = getAccessControlModel(sequelize)
        const { guid } = req.params

        const accessControl = await AccessControlModel.findOne({
            where: { guid },
        })

        if (!accessControl) {
            handleHttpError(res, 'El control de acceso no existe.', 404)
            return
        }

        if (accessControl.is_active == 0) {
            handleHttpError(
                res,
                'El control de acceso ha sido deshabilitado.',
                401
            )
            return
        }

        handleHttpSuccess(
            res,
            'Control de acceso encontrado.',
            201,
            accessControl
        )
    } catch (error) {
        console.log('Error obteniendo control de acceso:', error)
        handleHttpError(res, 'Error obteniendo control de acceso.', 403, error)
    }
}

/**
 * Este controlador es el encargado de actualizar un control de acceso
 * @param {*} req
 * @param {*} res
 */
const UpdateAccessControl = async (req, res) => {
    try {
        const sequelize = getSequelize(req)
        const AccessControlModel = getAccessControlModel(sequelize)
        const { guid } = req.params
        const data = matchedData(req)
        const updated_by = req.user.guid
        const updated_at = new Date()
        data.updated_by = updated_by
        data.updated_at = updated_at

        const [updateCount] = await AccessControlModel.update(data, {
            where: { guid },
        })

        if (updateCount === 0) {
            return handleHttpError(res, 'El control de acceso no existe.', 404)
        }

        const accessControl = await AccessControlModel.findOne({
            where: { guid },
        })

        if (!accessControl) {
            handleHttpError(res, 'El control de acceso no existe.', 404)
            return
        }

        if (accessControl.is_active == 0) {
            handleHttpError(
                res,
                'El control de acceso ha sido deshabilitado.',
                401
            )
            return
        }

        handleHttpSuccess(
            res,
            'Control de acceso actualizado.',
            201,
            accessControl
        )
    } catch (error) {
        console.log('Error actualizando control de acceso:', error)
        handleHttpError(
            res,
            'Error actualizando control de acceso.',
            403,
            error
        )
    }
}

/**
 * Este controlador es el encargado de eliminar un control de acceso
 * @param {*} req
 * @param {*} res
 */
const DeleteAccessControl = async (req, res) => {
    try {
        const sequelize = getSequelize(req)
        const AccessControlModel = getAccessControlModel(sequelize)
        const { guid } = req.params
        const deleted_by = req.user.guid
        const deleted_at = new Date()
        const [updateCount] = await AccessControlModel.update(
            {
                is_active: 0,
                deleted_by,
                deleted_at,
            },
            {
                where: { guid },
            }
        )

        if (updateCount === 0) {
            return handleHttpError(res, 'El control de acceso no existe.', 404)
        }

        handleHttpSuccess(res, 'Control de acceso eliminado.', 201)
    } catch (error) {
        console.log('Error eliminando control de acceso:', error)
        handleHttpError(res, 'Error eliminando control de acceso.', 403, error)
    }
}

/**
 * Este controlador es el encargado de desactivar un control de acceso
 * @param {*} req
 * @param {*} res
 */
const DisableAccessControl = async (req, res) => {
    try {
        const sequelize = getSequelize(req)
        const AccessControlModel = getAccessControlModel(sequelize)
        const { guid } = req.params
        const updated_by = req.user.guid
        const updated_at = new Date()
        const [updateCount] = await AccessControlModel.update(
            {
                is_active: 0,
                updated_by,
                updated_at,
            },
            {
                where: { guid },
            }
        )

        if (updateCount === 0) {
            return handleHttpError(res, 'El control de acceso no existe.', 404)
        }

        handleHttpSuccess(res, 'Control de acceso deshabilitado.', 201)
    } catch (error) {
        console.log('Error deshabilitando control de acceso:', error)
        handleHttpError(
            res,
            'Error deshabilitando control de acceso.',
            403,
            error
        )
    }
}

/**
 * Este controlador es el encargado de activar un control de acceso
 * @param {*} req
 * @param {*} res
 */
const EnableAccessControl = async (req, res) => {
    try {
        const sequelize = getSequelize(req)
        const AccessControlModel = getAccessControlModel(sequelize)
        const { guid } = req.params
        const updated_by = req.user.guid
        const updated_at = new Date()
        const [updateCount] = await AccessControlModel.update(
            {
                is_active: 1,
                updated_by,
                updated_at,
            },
            {
                where: { guid },
            }
        )

        if (updateCount === 0) {
            return handleHttpError(res, 'El control de acceso no existe.', 404)
        }

        handleHttpSuccess(res, 'Control de acceso habilitado.', 201)
    } catch (error) {
        console.log('Error habilitando control de acceso:', error)
        handleHttpError(res, 'Error habilitando control de acceso.', 403, error)
    }
}

/**
 * Este controlador es el encargado de obtener todos los controles de acceso
 * @param {*} req
 * @param {*} res
 */
const AccessControlsAll = async (req, res) => {
    try {
        const sequelize = getSequelize(req)
        const AccessControlModel = getAccessControlModel(sequelize)

        const accessControls = await AccessControlModel.findAll()

        if (!accessControls.length) {
            handleHttpError(res, 'No se encontraron controles de acceso.', 404)
            return
        }

        handleHttpSuccess(
            res,
            'Controles de acceso encontrados.',
            201,
            accessControls
        )
    } catch (error) {
        console.log('Error obteniendo controles de acceso:', error)
        handleHttpError(
            res,
            'Error obteniendo controles de acceso.',
            403,
            error
        )
    }
}

/**
 * Este controlador es el encargado de contar todos los controles de acceso
 * @param {*} req
 * @param {*} res
 */
const AccessControlsCount = async (req, res) => {
    try {
        const sequelize = getSequelize(req)
        const AccessControlModel = getAccessControlModel(sequelize)

        const count = await AccessControlModel.count()

        handleHttpCount(
            res,
            'Número de controles de acceso encontrados.',
            201,
            count
        )
    } catch (error) {
        console.log('Error contando controles de acceso:', error)
        handleHttpError(res, 'Error contando controles de acceso.', 403, error)
    }
}

/**
 * Este controlador es el encargado de obtener los controles de acceso según el rol
 * @param {*} req
 * @param {*} res
 */
const AccessControlsByRol = async (req, res) => {
    try {
        const sequelize = getSequelize(req)
        const AccessControlModel = getAccessControlModel(sequelize)
        const { guid } = req.params
        console.log('role:', req.params)

        const accessControls = await AccessControlModel.findAll({
            where: { role_guid: guid, is_active: 1 },
        })

        if (!accessControls.length) {
            handleHttpError(res, 'No se encontraron controles de acceso.', 404)
            return
        }

        // Agrupar los roles, módulos y sus permisos
        const groupedData = accessControls.reduce((acc, control) => {
            // Buscar si el rol ya existe en el array acumulado
            const existingRoleIndex = acc.findIndex(
                (item) => item.role.guid === control.role.guid
            )

            const permission = {
                id: control.permission.id,
                guid: control.permission.guid,
                name: control.permission.name,
            }

            // Si el rol ya existe
            if (existingRoleIndex > -1) {
                // Buscar si el módulo ya existe dentro del rol
                const existingModuleIndex = acc[
                    existingRoleIndex
                ].modules.findIndex((mod) => mod.guid === control.module.guid)

                // Si el módulo ya existe, agregar el permiso
                if (existingModuleIndex > -1) {
                    acc[existingRoleIndex].modules[
                        existingModuleIndex
                    ].permissions.push(permission)
                } else {
                    // Si el módulo no existe, agregar un nuevo módulo con el primer permiso
                    acc[existingRoleIndex].modules.push({
                        guid: control.module.guid,
                        name: control.module.name,
                        permissions: [permission],
                    })
                }
            } else {
                // Si el rol no existe, agregar un nuevo rol con el primer módulo y permiso
                acc.push({
                    id: control.id,
                    guid: control.guid,
                    is_active: control.is_active,
                    role: {
                        guid: control.role.guid,
                        name: control.role.name,
                    },
                    modules: [
                        {
                            guid: control.module.guid,
                            name: control.module.name,
                            permissions: [permission],
                        },
                    ],
                })
            }

            return acc
        }, [])

        console.log(groupedData)

        handleHttpSuccess(
            res,
            'Controles de acceso encontrados.',
            201,
            groupedData
        )
    } catch (error) {
        console.log('Error obteniendo controles de acceso:', error)
        handleHttpError(
            res,
            'Error obteniendo controles de acceso.',
            403,
            error
        )
    }
}

module.exports = {
    CreateAccessControl,
    AccessControls,
    AccessControl,
    UpdateAccessControl,
    DeleteAccessControl,
    DisableAccessControl,
    EnableAccessControl,
    AccessControlsAll,
    AccessControlsCount,
    AccessControlsByRol,
}
