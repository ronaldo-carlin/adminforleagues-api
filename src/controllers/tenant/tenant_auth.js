const { Op } = require('sequelize')
const { matchedData } = require('express-validator')
const fs = require('fs')
const ejs = require('ejs')
const EMAIL_TEMPLATE = __dirname + '/../html/resetPsw.html'
const { sendEmail } = require('../../helpers/mailer')
const { OAuth2Client } = require('google-auth-library')
const client = new OAuth2Client(process.env.GOOGLE_CLIENT_ID)
const { v4: uuidv4 } = require('uuid')
const {
    tokenSign,
    verifyAccessToken,
    verifyRefreshToken,
    resetPasswordToken,
} = require('../../utils/handleJwtTenant')
const { handleHttpSuccess } = require('../../utils/handleSuccess')
const { handleHttpError } = require('../../utils/handleError')
const { encrypt, compare } = require('../../utils/handlePassword')
const {
    getSequelize,
    getUserModel,
    getRolesModel,
    getTokenModel,
} = require('../../helpers/sequelizeHelper')

/**
 * Este controlador es el encargado de registrar un usuario
 * @param {*} req
 * @param {*} res
 */
const Register = async (req, res) => {
    try {
        //Obtenemos el body del request
        const body = matchedData(req)
        // obtenemos la instancia de sequelize del middleware
        const sequelize = getSequelize(req)
        // obtenemos el modelo de usuario
        const AuthTenantModel = getUserModel(sequelize)
        // obtenemos el modelo de roles
        const RolesTenantModel = getRolesModel(sequelize)

        //Verificamos si el cliente ya existe en la base de datos
        const user = await AuthTenantModel.findOne({
            where: {
                [Op.or]: [
                    {
                        username: body.username,
                    },
                    {
                        email: body.email,
                    },
                    {
                        phone: body.phone,
                    },
                ],
            },
        })
        //Si el cliente ya existe enviamos una respuesta de error
        if (user) {
            handleHttpError(res, 'El cliente ya existe.', 403)
            return
        }
        //Encriptamos la contraseña
        const encryptedPassword = await encrypt(body.password)
        //Creamos el guid
        const guid = await uuidv4()
        //Obtenemos el guid del Super Administrador
        const role = await RolesTenantModel.findOne({
            where: { name: 'Super Administrador' },
        })
        //Comparamos el guid que manda el cliente con el rol Super Administrador
        //Para evitar que puedan crear un user con el rol Super Administrador
        if (role.guid === body.role_guid) {
            handleHttpError(
                res,
                'No tienes permisos de crear un usuario administrador.',
                403
            )
            return
        }
        //Creamos el cliente en la base de datos
        await AuthModel.create({
            guid: guid,
            role_guid: body.role_guid,
            first_name: body.first_name,
            last_name: body.last_name,
            username: body.username,
            email: body.email,
            password: encryptedPassword,
            phone: body.phone,
            country: body.country,
            state: body.state,
            city: body.city,
            isVerified: 0,
            is_active: 1,
            created_at: new Date(),
        })
        //Enviamos una respuesta de éxito
        handleHttpSuccess(res, 'Cliente creado.', 201)
    } catch (error) {
        //Obtener y devolver el error
        handleHttpError(res, 'Error al crear el usuario.', 403, error)
    }
}

/**
 * Este controlador es el encargado de realizar el login del usuario
 * @param {*} req
 * @param {*} res
 */
const LoginUser = async (req, res) => {
    try {
        //Obtenemos y limpiamos el body del request
        const body = matchedData(req)
        // Obtenemos la instancia de sequelize del middleware
        const sequelize = getSequelize(req)
        // Obtenemos el modelo de usuario
        const AuthTenantModel = getUserModel(sequelize)
        //Buscamos el usuario en la base de datos
        const user = await AuthTenantModel.findOne({
            where: {
                [Op.or]: [
                    {
                        username: body.user.toLowerCase(),
                    },
                    {
                        email: body.user.toLowerCase(),
                    },
                    {
                        phone: body.user.toLowerCase(),
                    },
                ],
            },
        })
        //Si el usuario no existe enviamos una respuesta de error
        if (!user) {
            handleHttpError(res, 'El usuario no existe.', 404, '')
            return
        }
        //Si el usuario esta inactivo enviamos una respuesta de error
        if (user.active == false) {
            handleHttpError(res, 'El usuario ha sido deshabilitado.', 401, '')
            return
        }
        //Ciframos la contraseña para compararla con la de la base de datos
        const hashPassword = user.get('password')
        //Comparamos la contraseña
        const check = await compare(body.password, hashPassword)
        //Si la contraseña es incorrecta enviamos una respuesta de error
        if (!check) {
            handleHttpSuccess(res, 'Datos incorrectos.', 401)
            return
        }
        //Si se logra logear se envía un objeto con el token y el refreshtoken y los datos del usuario
        const accessToken = await tokenSign(user, true)
        const refreshToken = await tokenSign(user, false)
        const expirationDate = new Date()
        expirationDate.setDate(expirationDate.getDate() + 2) // Token válido por 2 días

        await SaveRefreshToken(
            req,
            user.guid,
            refreshToken,
            expirationDate,
            req.headers['User-Agent']
        )

        // quitamos la contraseña del objeto user
        user.password = undefined

        const data = {
            token: accessToken,
            refreshToken: refreshToken,
            user,
        }

        //Si el usuario se puede logear actualizamos la ultima conexión
        const fechaActual = new Date()

        await AuthTenantModel.update(
            { last_login: fechaActual },
            {
                where: {
                    guid: user.guid,
                },
            }
        )
        //Enviamos una respuesta de éxito con los datos
        handleHttpSuccess(res, '¡Bienvenido!', 201, data)
        // Enviamos el correo de inicio de sesión
        await sendEmail({
            to: user.email,
            subject: 'Inicio de sesión exitoso',
            html: `<p>Hola ${user.username}, se ha iniciado sesión en tu cuenta desde el siguiente dispositivo: ${req.headers['user-agent']}</p>`,
        })
    } catch (error) {
        //Obtener y devolver el error
        console.log('error login:', error)

        handleHttpError(res, 'Error login de usuario.', 403, error)
    }
}

/**
 * Este controlador es el encargado de recuperar la contraseña
 * @param {*} req
 * @param {*} res
 */
const ResetPassword = async (req, res) => {
    try {
        //Obtenemos el body del request
        req = req.body
        // obtenemos la instancia de sequelize del middleware
        const sequelize = getSequelize(req)
        // obtenemos el modelo de usuario
        const AuthTenantModel = getUserModel(sequelize)
        // obtenemos el modelo de roles

        //Buscamos si el usuario existe
        const user = await AuthTenantModel.findOne({
            where: {
                [Op.or]: [
                    {
                        email: req.user,
                    },
                ],
            },
        })
        //En caso de que no se haya encontrado un usuario
        if (!user) {
            //Enviamos un mensaje de success falso
            handleHttpSuccess(
                res,
                'Si su usuario o E-Mail existen en la base de datos, se enviará un correo con el link de recuperación de contraseña.',
                201
            )
            return
        }
        // Genera un token de restablecimiento de contraseña
        const data = {
            token: await resetPasswordToken(user),
            user,
        }
        // Leemos el fichero de HTML que usaremos para el cuerpo del mensaje
        let html = fs.readFileSync(EMAIL_TEMPLATE, 'utf-8')
        console.log('user: ', user.dataValues)
        const context = {
            name: `${user.first_name} ${user.last_name}`,
            front_url: `${process.env.PUBLIC_FRONT_URL}`,
            action_url:
                `${process.env.PUBLIC_FRONT_URL}/ResetPassword/` + data.token,
        }
        // Renderizar el HTML con los datos
        const contenidoHTML = ejs.render(html, context)

        // Creamos las opciones del correo
        let mailOptions = {
            from: '"Admin For Leagues"' + process.env.MAIL_USER + '', // sender address
            to: req.user,
            subject: 'Solicitud de recuperación de contraseña',
            html: contenidoHTML,
        }

        // Configuramos el transported de nodemailer
        let transporter = nodemailer.createTransport({
            host: process.env.MAIL_HOST,
            port: process.env.MAIL_PORT,
            secure: false, // Cambiar a false para usar TLS en lugar de SSL
            auth: {
                user: process.env.MAIL_USER,
                pass: process.env.MAIL_PASSWORD,
            },
        })

        // Enviar correo electrónico
        transporter.sendMail(mailOptions, (error) => {
            if (error) {
                handleHttpError(
                    res,
                    'Ha ocurrido un error enviando el correo.',
                    403,
                    error
                )
                return console.log(error)
            }
            handleHttpSuccess(
                res,
                'Se enviará un correo con el link de recuperación de contraseña.',
                201
            )
        })
    } catch (error) {
        //Obtener y devolver el error
        console.log('error resetPassword', error)
        handleHttpError(res, 'Ha ocurrido un error en el try.', 403, error)
    }
}

/**
 * Este controlador es el encargado de revisar si el access token que usa el usuario es valido
 * @param {*} req
 * @param {*} res
 */
const CheckToken = async (req, res) => {
    try {
        // obtenemos la instancia de sequelize del middleware
        const sequelize = getSequelize(req)
        // obtenemos el modelo de usuario
        const AuthTenantModel = getUserModel(sequelize)
        // Obtiene el token de la cabecera de autorización
        let token = req.headers.authorization?.split(' ').pop() // Intenta obtener el token de la cabecera de autorización
        if (!token) {
            // Si no se encuentra en la cabecera de autorización, intenta obtenerlo de un parámetro de consulta llamado "token"
            token = req.body.token
        }

        if (!token) {
            // Si no se proporciona un token en la cabecera de autorización ni en la consulta, responde con un error.
            return handleHttpError(res, 'Token no proporcionado.', 403)
        }

        // Verifica el token
        const dataToken = await verifyAccessToken(token)

        if (!dataToken) {
            // Si el token no es válido, responde con un error.
            return handleHttpError(res, 'Token inválido.', 403)
        }

        // Obtiene el guid del usuario que viene en el token para consultar en la base de datos los datos del usuario
        const query = {
            guid: dataToken.guid,
        }

        // Busca los datos del usuario por su guid
        const user = await AuthTenantModel.findOne({
            where: query,
            attributes: {
                exclude: [
                    'password',
                    'is_active',
                    'last_login',
                    'two_factor_auth',
                    'isVerified',
                    'created_at',
                    'created_by',
                    'deleted_at',
                    'deleted_by',
                    'updated_at',
                    'updated_by',
                ],
            },
        })

        const data = {
            user,
        }

        // Envía los datos del usuario
        handleHttpSuccess(res, 'Token válido', 200, data)
    } catch (error) {
        console.log(error)
        // Maneja los errores adecuadamente
        handleHttpError(res, 'Error al verificar el token.', 403, error)
    }
}

/**
 * Este controlador es el encargado de renovar el access token y el refresh token
 * @param {*} req
 * @param {*} res
 * @returns
 */
const RenewAccessToken = async (req, res) => {
    try {
        // obtenemos la instancia de sequelize del middleware
        const sequelize = getSequelize(req)
        // obtenemos el modelo de usuario
        const AuthTenantModel = getUserModel(sequelize)
        // obtenemos el modelo de roles
        const RolesTenantModel = getRolesModel(sequelize)

        //Obtenemos el refresh token del body del request
        const refreshToken = req.body.refreshToken

        //Verificamos si el refresh token viene en el body y no esta vacio
        if (!refreshToken || refreshToken === '') {
            handleHttpError(res, 'Refresh token no proporcionado.', 403)
            return
        }

        //Verificamos si el refresh token es valido
        const decoded = await verifyRefreshToken(refreshToken)
        if (!decoded) {
            handleHttpError(res, 'Refresh token inválido.', 401)
            return
        }

        //Buscamos el refresh token en la base de datos
        const validToken = await FindValidRefreshToken(req, refreshToken)
        if (!validToken) {
            handleHttpError(
                res,
                'Refresh token no encontrado o ha expirado.',
                401
            )
            return
        }

        //Buscamos el usuario en la base de datos
        const user = await AuthTenantModel.findOne({
            where: { guid: validToken.user_guid },
            attributes: {
                //Excluimos la contraseña
                exclude: ['password'],
            },
            include: [
                {
                    model: RolesTenantModel, // Modelo de roles
                    as: 'role', // Alias definido en la asociación
                    attributes: ['guid', 'name'], // Incluir solo los atributos necesarios del rol
                },
            ],
        })

        //Revocamos el refresh token actual
        await RevokeRefreshToken(req, refreshToken)

        //Generamos un nuevo access token
        const newAccessToken = await tokenSign(user, true)

        //Generamos un nuevo refresh token
        const newRefreshToken = await tokenSign(user, false)

        //Creamos una nueva fecha de expiración
        const expirationDate = new Date()
        expirationDate.setDate(expirationDate.getDate() + 2) // Token válido por 2 días

        //Guardamos el nuevo refresh token en la base de datos
        await SaveRefreshToken(
            req,
            user.guid,
            newRefreshToken,
            expirationDate,
            req.headers['User-Agent']
        )

        //Enviamos una respuesta de éxito con los datos
        handleHttpSuccess(res, 'Token renovado', 200, {
            token: newAccessToken,
            refreshToken: newRefreshToken,
            user,
        })
    } catch (error) {
        //Obtener y devolver el error
        handleHttpError(res, false, 'Error al verificar el token.', 403, error)
    }
}

// funcion para guardar el refresh token en la base de datos
const SaveRefreshToken = async (
    req,
    userGuid,
    refreshToken,
    expirationDate,
    diviceInfo = null
) => {
    //obtenemos la instancia de sequelize del middleware
    const sequelize = getSequelize(req)
    // obtenemos el modelo de token
    const TokenTenantModel = getTokenModel(sequelize)
    // creamos el refresh token en la base de datos
    await TokenTenantModel.create({
        user_guid: userGuid,
        refresh_token: refreshToken,
        expiry_date: expirationDate,
        created_at: new Date(),
        device_ifo: diviceInfo,
    })
}

// funcion para revocar el refresh token
const RevokeRefreshToken = async (req, token) => {
    //obtenemos la instancia de sequelize del middleware
    const sequelize = getSequelize(req)
    // obtenemos el modelo de token
    const TokenTenantModel = getTokenModel(sequelize)
    // revocamos el refresh token en la base de datos
    await TokenTenantModel.update(
        { revoket: 1, revoked_at: new Date() },
        { where: { refresh_token: token } }
    )
}

// funcion para buscar el refresh token en la base de datos
const FindValidRefreshToken = async (req, token) => {
    //obtenemos la instancia de sequelize del middleware
    const sequelize = getSequelize(req)
    // obtenemos el modelo de token
    const TokenTenantModel = getTokenModel(sequelize)
    // buscamos el refresh token en la base de datos
    const refreshToken = await TokenTenantModel.findOne({
        where: {
            refresh_token: token,
            revoket: 0,
            expiry_date: {
                // Verifica si la fecha de expiración es mayor a la fecha actual
                [Op.gt]: new Date(),
            },
        },
    })
    return refreshToken
}

// Función para verificar el token de Google
const VerifyGoogleToken = async (req, token) => {
    try {
        // obtenemos la instancia de sequelize del middleware
        const sequelize = getSequelize(req)
        // obtenemos el modelo de usuario
        const AuthTenantModel = getUserModel(sequelize)
        const ticket = await client.verifyIdToken({
            idToken: token,
            audience: process.env.GOOGLE_CLIENT_ID,
        })
        const payload = ticket.getPayload()
        // aqui validamos si el usuario existe en la base de datos y si no existe lo creamos
        console.log('payload:', payload)
        // Verificar si el usuario existe en la base de datos que corresponde al google_id o el email
        let user = await AuthTenantModel.findOne({
            where: {
                [Op.or]: [
                    { google_id: payload.sub }, // payload.sub contiene el ID de Google
                    { email: payload.email }, // payload.email contiene el email del usuario
                ],
            },
        })

        //console.log("user:", user);

        if (!user) {
            console.log('El usuario no existe en la base de datos.')
            // creamos un guid para el usuario
            const guid = await uuidv4()
            // Si el usuario no existe, crearlo en la base de datos
            await AuthTenantModel.create({
                guid: guid,
                role_guid: '1881e61d-4be8-4e3b-9482-9643d5c05326',
                google_id: payload.sub,
                email: payload.email,
                first_name: payload.given_name,
                last_name: payload.family_name,
                username: payload.name,
                picture: payload.picture,
                isVerified: 1,
                is_active: 1,
                last_login: new Date(),
                created_at: new Date(),
            })
            // Devolver el usuario creado pero con todos los datos del usuario
            user = await AuthTenantModel.findOne({
                where: {
                    [Op.or]: [
                        { google_id: payload.sub }, // payload.sub contiene el ID de Google
                        { email: payload.email }, // payload.email contiene el email del usuario
                    ],
                },
            })
            console.log('Usuario creado en la base de datos:', user)
        } else {
            //validamos si ya tiene un google_id
            if (user.google_id == null || user.google_id == '') {
                // si el usuario existe, actualizamos su información de google
                user.google_id = payload.sub
                console.log('el usuario existe en la base de datos.', user)
                console.log('Usuario actualizado en la base de datos')
            } else {
                console.log('El usuario inicio sesión con Google.')
            }
        }
        // Devolver el usuario encontrado, creado o actualizado
        return user
    } catch (error) {
        console.error('Error al verificar el token de Google:', error)
        throw error
    }
}

// Función para autenticar con el token de Google
const AuthenticateWithGoogleToken = async (req, res) => {
    try {
        // obtenemos la instancia de sequelize del middleware
        const sequelize = getSequelize(req)
        // obtenemos el modelo de usuario
        const AuthTenantModel = getUserModel(sequelize)
        const { token } = req.body // Asegúrate de enviar el token con la clave 'token' en el cuerpo de la solicitud
        const payload = await VerifyGoogleToken(req, token)
        console.log(
            '🚀 ~ AuthenticateWithGoogleToken ~ payload:',
            payload.dataValues
        )

        const accessToken = await tokenSign(payload, true)
        const refreshToken = await tokenSign(payload, false)
        const expirationDate = new Date()
        expirationDate.setDate(expirationDate.getDate() + 2) // Token válido por 2 días

        await SaveRefreshToken(
            req,
            payload.guid,
            refreshToken,
            expirationDate,
            req.headers['User-Agent']
        )

        // quitamos la contraseña del objeto user
        payload.password = undefined

        //Si se logra logear se envía un objeto con el token y los datos del usuario
        const data = {
            token: accessToken,
            refreshToken: refreshToken,
            user: payload,
        }

        //Si el usuario se puede logear actualizamos la ultima conexión
        const fechaActual = new Date()
        /* const opciones = { year: "numeric", month: "long", day: "numeric" };
      const fechaFormateada = fechaActual.toLocaleDateString("es-MX", opciones); */

        await AuthTenantModel.update(
            { last_login: fechaActual },
            {
                where: {
                    guid: payload.guid,
                },
            }
        )
        //Enviamos una respuesta de éxito con los datos
        handleHttpSuccess(res, '¡Bienvenido!', 200, data)
        //res.status(200).send({ success: payload });
    } catch (error) {
        console.error(error)
        handleHttpError(res, 'Error al autenticar con Google.', 403, error)
    }
}

module.exports = {
    Register,
    LoginUser,
    ResetPassword,
    CheckToken,
    VerifyGoogleToken,
    AuthenticateWithGoogleToken,
    RenewAccessToken,
}
