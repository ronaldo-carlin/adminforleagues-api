// index.js
const app = require('./app')
const { io, server } = require('./socket') // Importa 'io' y 'server'
// Importa el archivo de configuración de passport
const passport = require('./config/passport')

// Ahora utiliza passport.initialize() y passport.session() aquí si estás usando sesiones
app.use(passport.initialize())
app.use(passport.session()) // Descomenta si usas sesiones

if (process.env.NODE_ENV === 'production') {
    require('dotenv').config({ path: 'production.env' })
} else {
    require('dotenv').config({ path: '.env.dev' })
}

const PORT = process.env.PORT || 3013
const SOCKET_PORT = process.env.SOCKET_PORT || 3012

async function main() {
    await app.listen(PORT)
    console.log(
        '<<< Server is running on port http://localhost:' + PORT + '>>>'
    )

    server.listen(SOCKET_PORT, () => {
        console.log(
            '<<< Socket.io Server is running on port http://localhost:' +
                SOCKET_PORT +
                '>>>'
        )
    })
}

main()
