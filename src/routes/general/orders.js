const express = require('express')
const { matchedData } = require('express-validator')
const router = express.Router()

// validaciones
const {
    ValidatorCreateOrder,
    ValidatorUpdateOrder,
    ValidatorDeleteOrder,
    ValidatorDisableOrder,
    ValidatorEnableOrder,
} = require('../../validators/general/orders')

// Importa los controladores
const {
    CreateOrder,
    Orders,
    Order,
    UpdateOrder,
    DeleteOrder,
    DisableOrder,
    EnableOrder,
    OrdersAll,
    CountAllOrders,
} = require('../../controllers/general/orders')

// Middleware de autenticación
const authMiddleware = require('../../middleware/session')

// Rutas protegidas que requieren autenticación
/**
 * @swagger
 * tags:
 *   name: Orders
 *   description: API para la gestión de órdenes
 */

// Ruta para crear una nueva orden
/**
 * @swagger
 * /orders/CreateOrder:
 *   post:
 *     summary: Crear una nueva orden
 *     description: Esta ruta permite crear una nueva orden.
 *     tags: [Orders]
 *     requestBody:
 *       required: true
 *       content:
 *         application/json:
 *           schema:
 *             type: object
 *             properties:
 *               user_guid:
 *                 type: string
 *                 example: 123e4567-e89b-12d3-a456-426614174000
 *               plan_guid:
 *                 type: string
 *                 example: 123e4567-e89b-12d3-a456-426614174001
 *               coupon_guid:
 *                 type: string
 *                 example: 123e4567-e89b-12d3-a456-426614174002
 *               transaction:
 *                 type: string
 *                 example: "transaction_12345"
 *               payment_method:
 *                 type: string
 *                 example: "credit_card"
 *               amount:
 *                 type: number
 *                 example: 100.00
 *               currency:
 *                 type: string
 *                 example: "USD"
 *               currency_icon:
 *                 type: string
 *                 example: "$"
 *     responses:
 *       201:
 *         description: Orden creada exitosamente
 *         content:
 *           application/json:
 *             schema:
 *               $ref: '#/components/responses/success'
 *       400:
 *         description: Error en los datos enviados
 *         content:
 *           application/json:
 *             schema:
 *               $ref: '#/components/responses/BadRequest'
 *       403:
 *         description: Error de autenticación
 *         content:
 *           application/json:
 *             schema:
 *               $ref: '#/components/responses/UnauthorizedError'
 *       409:
 *         description: Orden ya existe
 *         content:
 *           application/json:
 *             schema:
 *               $ref: '#/components/responses/ConflictError'
 *       500:
 *         description: Error en el servidor
 *         content:
 *           application/json:
 *             schema:
 *               $ref: '#/components/responses/InternalServerError'
 *     security:
 *        - bearerAuth: []
 */
router.post('/CreateOrder', authMiddleware, ValidatorCreateOrder, CreateOrder)

// Ruta para obtener todas las órdenes activas
/**
 * @swagger
 *  /orders/Orders:
 *   get:
 *     summary: Obtener todas las órdenes activas
 *     tags: [Orders]
 *     responses:
 *       201:
 *         description: Órdenes obtenidas exitosamente
 *         content:
 *           application/json:
 *             schema:
 *               $ref: '#/components/responses/success'
 *       403:
 *         description: Error de autenticación
 *         content:
 *           application/json:
 *             schema:
 *               $ref: '#/components/responses/UnauthorizedError'
 *       500:
 *         description: Error en el servidor
 *         content:
 *           application/json:
 *             schema:
 *               $ref: '#/components/responses/InternalServerError'
 *     security:
 *        - bearerAuth: []
 */
router.get('/Orders', authMiddleware, Orders)

// Ruta para obtener una orden
/**
 * @swagger
 * /orders/Order:
 *   get:
 *     summary: Obtiene una orden por GUID
 *     tags: [Orders]
 *     parameters:
 *       - in: path
 *         name: guid
 *         schema:
 *           type: uuid
 *           format: uuid
 *         required: true
 *         description: El GUID de la orden
 *     responses:
 *       201:
 *         description: Orden obtenida exitosamente
 *         content:
 *           application/json:
 *             schema:
 *               $ref: '#/components/responses/success'
 *       403:
 *         description: Error de autenticación
 *         content:
 *           application/json:
 *             schema:
 *               $ref: '#/components/responses/UnauthorizedError'
 *       404:
 *        description: Orden no encontrada
 *        content:
 *          application/json:
 *            schema:
 *              $ref: '#/components/responses/NotFoundError'
 *       500:
 *         description: Error en el servidor
 *         content:
 *           application/json:
 *             schema:
 *               $ref: '#/components/responses/InternalServerError'
 *     security:
 *        - bearerAuth: []
 */
router.get('/Order/:guid', authMiddleware, Order)

// Ruta para actualizar una orden
/**
 * @swagger
 * /orders/UpdateOrder:
 *   patch:
 *     summary: Actualizar una orden
 *     tags: [Orders]
 *     requestBody:
 *       required: true
 *       content:
 *         application/json:
 *           schema:
 *             type: object
 *             properties:
 *               guid:
 *                 type: string
 *                 example: 495db854-849b-41cd-b6b9-9c84824c92ba
 *               user_guid:
 *                 type: string
 *                 example: 123e4567-e89b-12d3-a456-426614174000
 *               plan_guid:
 *                 type: string
 *                 example: 123e4567-e89b-12d3-a456-426614174001
 *               coupon_guid:
 *                 type: string
 *                 example: 123e4567-e89b-12d3-a456-426614174002
 *               transaction:
 *                 type: string
 *                 example: "transaction_12345"
 *               payment_method:
 *                 type: string
 *                 example: "credit_card"
 *               amount:
 *                 type: number
 *                 example: 100.00
 *               currency:
 *                 type: string
 *                 example: "USD"
 *               currency_icon:
 *                 type: string
 *                 example: "$"
 *               enroll_start:
 *                 type: string
 *                 format: date
 *                 example: "2024-01-01"
 *               enroll_end:
 *                 type: string
 *                 format: date
 *                 example: "2024-01-30"
 *               expiration_date:
 *                 type: string
 *                 format: date
 *                 example: "2024-12-31"
 *               renewal_date:
 *                 type: string
 *                 format: date
 *                 example: "2024-12-01"
 *               cancel_date:
 *                 type: string
 *                 format: date
 *                 example: "2024-06-01"
 *               cancel_type:
 *                 type: string
 *                 example: "user_cancelled"
 *     responses:
 *       201:
 *         description: Orden actualizada exitosamente
 *         content:
 *           application/json:
 *             schema:
 *               $ref: '#/components/responses/success'
 *       400:
 *         description: Error en los datos enviados
 *         content:
 *           application/json:
 *             schema:
 *               $ref: '#/components/responses/BadRequest'
 *       403:
 *         description: Error de autenticación
 *         content:
 *           application/json:
 *             schema:
 *               $ref: '#/components/responses/UnauthorizedError'
 *       404:
 *        description: Orden no encontrada
 *        content:
 *          application/json:
 *            schema:
 *              $ref: '#/components/responses/NotFoundError'
 *       500:
 *         description: Error en el servidor
 *         content:
 *           application/json:
 *             schema:
 *               $ref: '#/components/responses/InternalServerError'
 *     security:
 *        - bearerAuth: []
 */
router.patch(
    '/UpdateOrder/:guid',
    authMiddleware,
    ValidatorUpdateOrder,
    UpdateOrder
)

// Ruta para eliminar una orden
/**
 * @swagger
 * /orders/DeleteOrder:
 *   delete:
 *     summary: Eliminar una orden
 *     tags: [Orders]
 *     parameters:
 *       - in: path
 *         name: guid
 *         schema:
 *           type: uuid
 *           format: uuid
 *         required: true
 *         description: El GUID de la orden
 *     responses:
 *       201:
 *         description: Orden eliminada exitosamente
 *         content:
 *           application/json:
 *             schema:
 *               $ref: '#/components/responses/success'
 *       400:
 *         description: Error en los datos enviados
 *         content:
 *           application/json:
 *             schema:
 *               $ref: '#/components/responses/BadRequest'
 *       403:
 *         description: Error de autenticación
 *         content:
 *           application/json:
 *             schema:
 *               $ref: '#/components/responses/UnauthorizedError'
 *       404:
 *        description: Orden no encontrada
 *        content:
 *          application/json:
 *            schema:
 *              $ref: '#/components/responses/NotFoundError'
 *       500:
 *         description: Error en el servidor
 *         content:
 *           application/json:
 *             schema:
 *               $ref: '#/components/responses/InternalServerError'
 *     security:
 *        - bearerAuth: []
 */
router.delete(
    '/DeleteOrder/:guid',
    authMiddleware,
    ValidatorDeleteOrder,
    DeleteOrder
)

// Ruta para desactivar una orden
/**
 * @swagger
 * /orders/DisableOrder:
 *   patch:
 *     summary: Desactivar una orden
 *     tags: [Orders]
 *     requestBody:
 *       required: true
 *       content:
 *         application/json:
 *           schema:
 *             type: object
 *             properties:
 *               guid:
 *                 type: uuid
 *                 example: 495db854-849b-41cd-b6b9-9c84824c92ba
 *     responses:
 *       201:
 *         description: Orden desactivada exitosamente
 *         content:
 *           application/json:
 *             schema:
 *               $ref: '#/components/responses/success'
 *       400:
 *         description: Error en los datos enviados
 *         content:
 *           application/json:
 *             schema:
 *               $ref: '#/components/responses/BadRequest'
 *       403:
 *         description: Error de autenticación
 *         content:
 *           application/json:
 *             schema:
 *               $ref: '#/components/responses/UnauthorizedError'
 *       404:
 *        description: Orden no encontrada
 *        content:
 *          application/json:
 *            schema:
 *              $ref: '#/components/responses/NotFoundError'
 *       500:
 *         description: Error en el servidor
 *         content:
 *           application/json:
 *             schema:
 *               $ref: '#/components/responses/InternalServerError'
 *     security:
 *        - bearerAuth: []
 */
router.patch(
    '/DisableOrder/:guid',
    authMiddleware,
    ValidatorDisableOrder,
    DisableOrder
)

// Ruta para activar una orden
/**
 * @swagger
 * /orders/EnableOrder:
 *   patch:
 *     summary: Activar una orden
 *     tags: [Orders]
 *     requestBody:
 *       required: true
 *       content:
 *         application/json:
 *           schema:
 *             type: object
 *             properties:
 *               guid:
 *                 type: uuid
 *                 example: 495db854-849b-41cd-b6b9-9c84824c92ba
 *     responses:
 *       201:
 *         description: Orden activada exitosamente
 *         content:
 *           application/json:
 *             schema:
 *               $ref: '#/components/responses/success'
 *       400:
 *         description: Error en los datos enviados
 *         content:
 *           application/json:
 *             schema:
 *               $ref: '#/components/responses/BadRequest'
 *       403:
 *         description: Error de autenticación
 *         content:
 *           application/json:
 *             schema:
 *               $ref: '#/components/responses/UnauthorizedError'
 *       404:
 *        description: Orden no encontrada
 *        content:
 *          application/json:
 *            schema:
 *              $ref: '#/components/responses/NotFoundError'
 *       500:
 *         description: Error en el servidor
 *         content:
 *           application/json:
 *             schema:
 *               $ref: '#/components/responses/InternalServerError'
 *     security:
 *        - bearerAuth: []
 */
router.patch(
    '/EnableOrder/:guid',
    authMiddleware,
    ValidatorEnableOrder,
    EnableOrder
)

// Ruta para obtener todas las órdenes
/**
 * @swagger
 * /orders/OrdersAll:
 *   get:
 *     summary: Obtener todas las órdenes
 *     tags: [Orders]
 *     responses:
 *       201:
 *         description: Órdenes obtenidas exitosamente
 *         content:
 *           application/json:
 *             schema:
 *               $ref: '#/components/responses/success'
 *       400:
 *         description: Error en los datos enviados
 *         content:
 *           application/json:
 *             schema:
 *               $ref: '#/components/responses/BadRequest'
 *       403:
 *         description: Error de autenticación
 *         content:
 *           application/json:
 *             schema:
 *               $ref: '#/components/responses/UnauthorizedError'
 *       500:
 *         description: Error en el servidor
 *         content:
 *           application/json:
 *             schema:
 *               $ref: '#/components/responses/InternalServerError'
 *     security:
 *        - bearerAuth: []
 */
router.get('/OrdersAll', authMiddleware, OrdersAll)

// Ruta para contar todas las órdenes
/**
 * @swagger
 * /orders/OrdersCount:
 *   get:
 *     summary: Contar todas las órdenes
 *     tags: [Orders]
 *     responses:
 *       201:
 *         description: Órdenes contadas exitosamente
 *         content:
 *           application/json:
 *             schema:
 *               $ref: '#/components/responses/success'
 *       400:
 *         description: Error en los datos enviados
 *         content:
 *           application/json:
 *             schema:
 *               $ref: '#/components/responses/BadRequest'
 *       403:
 *         description: Error de autenticación
 *         content:
 *           application/json:
 *             schema:
 *               $ref: '#/components/responses/UnauthorizedError'
 *       500:
 *         description: Error en el servidor
 *         content:
 *           application/json:
 *             schema:
 *               $ref: '#/components/responses/InternalServerError'
 *     security:
 *        - bearerAuth: []
 */
router.get('/OrdersCount', authMiddleware, CountAllOrders)

module.exports = router
