// Importar Sequelize
const { DataTypes } = require('sequelize')

// Definir la función factory para el modelo MatchesTenantModel
const MatchesTenantModelFactory = (sequelize) => {
    const MatchesTenantModel = sequelize.define(
        'matches',
        {
            id: {
                type: DataTypes.INTEGER,
                primaryKey: true,
                autoIncrement: true,
            },
            guid: {
                type: DataTypes.UUID,
                primaryKey: true,
                defaultValue: DataTypes.UUIDV4,
                unique: true,
            },
            home_team_guid: {
                type: DataTypes.TEXT,
            },
            away_team_guid: {
                type: DataTypes.TEXT,
            },
            competition_guid: {
                type: DataTypes.TEXT,
            },
            sede_guid: {
                type: DataTypes.TEXT,
            },
            referee_guid: {
                type: DataTypes.TEXT,
            },
            Date: {
                type: DataTypes.DATE,
            },
            Time: {
                type: DataTypes.TIME,
            },
            group: {
                type: DataTypes.TEXT,
            },
            is_active: {
                type: DataTypes.INTEGER,
            },
            created_by: {
                type: DataTypes.UUID,
            },
            updated_by: {
                type: DataTypes.UUID,
            },
            deleted_by: {
                type: DataTypes.UUID,
            },
            created_at: {
                type: DataTypes.DATE,
            },
            updated_at: {
                type: DataTypes.DATE,
            },
            deleted_at: {
                type: DataTypes.DATE,
            },
        },
        {
            tableName: 'matches',
            timestamps: false,
            defaultScope: {
                attributes: {
                    exclude: [
                        'created_by',
                        'updated_by',
                        'deleted_by',
                        'created_at',
                        'updated_at',
                        'deleted_at',
                    ],
                },
            },
        }
    )

    return MatchesTenantModel
}

module.exports = MatchesTenantModelFactory
