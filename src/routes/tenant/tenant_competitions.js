const path = require('path')
const express = require('express')
const router = express.Router()

// Controladores y validadores para competitions
const {
    validatorCreateCompetition,
    validatorCompetition,
    validatorUpdateCompetition,
    validatorDeleteCompetition,
    validatorDisableCompetition,
    validatorEnableCompetition,
} = require('../../validators/tenant/tenant_competitions')

const {
    CreateCompetition,
    Competitions,
    Competition,
    UpdateCompetition,
    DeleteCompetition,
    DisableCompetition,
    EnableCompetition,
    CompetitionsAll,
    CompetitionsCount,
    UploadProfilePicture,
} = require('../../controllers/tenant/tenant_competitions')

const authMiddleware = require('../../middleware/session_tenant')
const uploadMiddleware = require('../../utils/handleStorage')

/* Rutas protegidas que requieren autenticación */

/**
 * @swagger
 * tags:
 *   name: Tenant_Competitions
 *   description: API para la gestión de competiciones
 */

// Ruta para crear una nueva competición
/**
 * @swagger
 * /tenant_competitions/CreateCompetition:
 *   post:
 *     summary: Crear una nueva competición
 *     description: Esta ruta permite crear una nueva competición con los detalles especificados.
 *     tags: [Tenant_Competitions]
 *     requestBody:
 *       required: true
 *       content:
 *         application/json:
 *           schema:
 *             type: object
 *             properties:
 *               name:
 *                 type: string
 *                 example: "Liga de Fútbol"
 *               description:
 *                 type: string
 *                 example: "Una competición de fútbol entre varios equipos."
 *     responses:
 *       201:
 *         description: Competición creada exitosamente
 *         content:
 *           application/json:
 *             schema:
 *               $ref: '#/components/responses/success'
 *       400:
 *         description: Error en los datos enviados
 *         content:
 *           application/json:
 *             schema:
 *               $ref: '#/components/responses/BadRequest'
 *       403:
 *         description: Error de autenticación
 *         content:
 *           application/json:
 *             schema:
 *               $ref: '#/components/responses/UnauthorizedError'
 *       409:
 *         description: La competición ya existe
 *         content:
 *           application/json:
 *             schema:
 *               $ref: '#/components/responses/ConflictError'
 *       500:
 *         description: Error en el servidor
 *         content:
 *           application/json:
 *             schema:
 *               $ref: '#/components/responses/InternalServerError'
 *     security:
 *       - bearerAuth: []
 */
router.post(
    '/CreateCompetition',
    authMiddleware,
    validatorCreateCompetition(),
    CreateCompetition
)

// Ruta para obtener todas las competiciones activas
/**
 * @swagger
 * /tenant_competitions/Competitions:
 *   get:
 *     summary: Obtener todas las competiciones activas
 *     tags: [Tenant_Competitions]
 *     responses:
 *       200:
 *         description: Competiciones obtenidas exitosamente
 *         content:
 *           application/json:
 *             schema:
 *               $ref: '#/components/responses/success'
 *       403:
 *         description: Error de autenticación
 *         content:
 *           application/json:
 *             schema:
 *               $ref: '#/components/responses/UnauthorizedError'
 *       500:
 *         description: Error en el servidor
 *         content:
 *           application/json:
 *             schema:
 *               $ref: '#/components/responses/InternalServerError'
 *     security:
 *       - bearerAuth: []
 */
router.get('/Competitions', authMiddleware, Competitions)

// Ruta para obtener una competición por GUID
/**
 * @swagger
 * /tenant_competitions/Competition/{guid}:
 *   get:
 *     summary: Obtener una competición por GUID
 *     tags: [Tenant_Competitions]
 *     parameters:
 *       - in: path
 *         name: guid
 *         schema:
 *           type: string
 *           format: uuid
 *         required: true
 *         description: El GUID de la competición
 *     responses:
 *       200:
 *         description: Competición obtenida exitosamente
 *         content:
 *           application/json:
 *             schema:
 *               $ref: '#/components/responses/success'
 *       403:
 *         description: Error de autenticación
 *         content:
 *           application/json:
 *             schema:
 *               $ref: '#/components/responses/UnauthorizedError'
 *       404:
 *         description: Competición no encontrada
 *         content:
 *           application/json:
 *             schema:
 *               $ref: '#/components/responses/NotFoundError'
 *       500:
 *         description: Error en el servidor
 *         content:
 *           application/json:
 *             schema:
 *               $ref: '#/components/responses/InternalServerError'
 *     security:
 *       - bearerAuth: []
 */
router.get(
    '/Competition/:guid',
    authMiddleware,
    validatorCompetition(),
    Competition
)

// Ruta para actualizar una competición
/**
 * @swagger
 * /tenant_competitions/UpdateCompetition/{guid}:
 *   patch:
 *     summary: Actualizar una competición
 *     tags: [Tenant_Competitions]
 *     requestBody:
 *       required: true
 *       content:
 *         application/json:
 *           schema:
 *             type: object
 *             properties:
 *               name:
 *                 type: string
 *                 example: "Liga de Fútbol"
 *               description:
 *                 type: string
 *                 example: "Una competición de fútbol entre varios equipos."
 *     responses:
 *       200:
 *         description: Competición actualizada exitosamente
 *         content:
 *           application/json:
 *             schema:
 *               $ref: '#/components/responses/success'
 *       400:
 *         description: Error en los datos enviados
 *         content:
 *           application/json:
 *             schema:
 *               $ref: '#/components/responses/BadRequest'
 *       403:
 *         description: Error de autenticación
 *         content:
 *           application/json:
 *             schema:
 *               $ref: '#/components/responses/UnauthorizedError'
 *       404:
 *         description: Competición no encontrada
 *         content:
 *           application/json:
 *             schema:
 *               $ref: '#/components/responses/NotFoundError'
 *       500:
 *         description: Error en el servidor
 *         content:
 *           application/json:
 *             schema:
 *               $ref: '#/components/responses/InternalServerError'
 *     security:
 *       - bearerAuth: []
 */
router.patch(
    '/UpdateCompetition/:guid',
    authMiddleware,
    validatorUpdateCompetition(),
    UpdateCompetition
)

// Ruta para eliminar una competición
/**
 * @swagger
 * /tenant_competitions/DeleteCompetition/{guid}:
 *   delete:
 *     summary: Eliminar una competición
 *     tags: [Tenant_Competitions]
 *     parameters:
 *       - in: path
 *         name: guid
 *         schema:
 *           type: string
 *           format: uuid
 *         required: true
 *         description: El GUID de la competición
 *     responses:
 *       200:
 *         description: Competición eliminada exitosamente
 *         content:
 *           application/json:
 *             schema:
 *               $ref: '#/components/responses/success'
 *       400:
 *         description: Error en los datos enviados
 *         content:
 *           application/json:
 *             schema:
 *               $ref: '#/components/responses/BadRequest'
 *       403:
 *         description: Error de autenticación
 *         content:
 *           application/json:
 *             schema:
 *               $ref: '#/components/responses/UnauthorizedError'
 *       404:
 *         description: Competición no encontrada
 *         content:
 *           application/json:
 *             schema:
 *               $ref: '#/components/responses/NotFoundError'
 *       500:
 *         description: Error en el servidor
 *         content:
 *           application/json:
 *             schema:
 *               $ref: '#/components/responses/InternalServerError'
 *     security:
 *       - bearerAuth: []
 */
router.delete(
    '/DeleteCompetition/:guid',
    authMiddleware,
    validatorDeleteCompetition(),
    DeleteCompetition
)

// Ruta para deshabilitar una competición
/**
 * @swagger
 * /tenant_competitions/DisableCompetition/{guid}:
 *   patch:
 *     summary: Deshabilitar una competición
 *     tags: [Tenant_Competitions]
 *     requestBody:
 *       required: true
 *       content:
 *         application/json:
 *           schema:
 *             type: object
 *             properties:
 *               guid:
 *                 type: uuid
 *                 example: 495db854-849b-41cd-b6b9-9c84824c92ba
 *     responses:
 *       200:
 *         description: Competición deshabilitada exitosamente
 *         content:
 *           application/json:
 *             schema:
 *               $ref: '#/components/responses/success'
 *       400:
 *         description: Error en los datos enviados
 *         content:
 *           application/json:
 *             schema:
 *               $ref: '#/components/responses/BadRequest'
 *       403:
 *         description: Error de autenticación
 *         content:
 *           application/json:
 *             schema:
 *               $ref: '#/components/responses/UnauthorizedError'
 *       404:
 *         description: Competición no encontrada
 *         content:
 *           application/json:
 *             schema:
 *               $ref: '#/components/responses/NotFoundError'
 *       500:
 *         description: Error en el servidor
 *         content:
 *           application/json:
 *             schema:
 *               $ref: '#/components/responses/InternalServerError'
 *     security:
 *       - bearerAuth: []
 */
router.patch(
    '/DisableCompetition/:guid',
    authMiddleware,
    validatorDisableCompetition(),
    DisableCompetition
)

// Ruta para habilitar una competición
/**
 * @swagger
 * /tenant_competitions/EnableCompetition/{guid}:
 *   patch:
 *     summary: Habilitar una competición
 *     tags: [Tenant_Competitions]
 *     requestBody:
 *       required: true
 *       content:
 *         application/json:
 *           schema:
 *             type: object
 *             properties:
 *               guid:
 *                 type: uuid
 *                 example: 495db854-849b-41cd-b6b9-9c84824c92ba
 *     responses:
 *       200:
 *         description: Competición habilitada exitosamente
 *         content:
 *           application/json:
 *             schema:
 *               $ref: '#/components/responses/success'
 *       400:
 *         description: Error en los datos enviados
 *         content:
 *           application/json:
 *             schema:
 *               $ref: '#/components/responses/BadRequest'
 *       403:
 *         description: Error de autenticación
 *         content:
 *           application/json:
 *             schema:
 *               $ref: '#/components/responses/UnauthorizedError'
 *       404:
 *         description: Competición no encontrada
 *         content:
 *           application/json:
 *             schema:
 *               $ref: '#/components/responses/NotFoundError'
 *       500:
 *         description: Error en el servidor
 *         content:
 *           application/json:
 *             schema:
 *               $ref: '#/components/responses/InternalServerError'
 *     security:
 *       - bearerAuth: []
 */
router.patch(
    '/EnableCompetition/:guid',
    authMiddleware,
    validatorEnableCompetition(),
    EnableCompetition
)

// Ruta para obtener todas las competiciones (activos/inactivos)
/**
 * @swagger
 * /tenant_competitions/CompetitionsAll:
 *   get:
 *     summary: Obtener todas las competiciones (activos/inactivos)
 *     tags: [Tenant_Competitions]
 *     responses:
 *       200:
 *         description: Todas las competiciones obtenidas exitosamente
 *         content:
 *           application/json:
 *             schema:
 *               $ref: '#/components/responses/success'
 *       403:
 *         description: Error de autenticación
 *         content:
 *           application/json:
 *             schema:
 *               $ref: '#/components/responses/UnauthorizedError'
 *       500:
 *         description: Error en el servidor
 *         content:
 *           application/json:
 *             schema:
 *               $ref: '#/components/responses/InternalServerError'
 *     security:
 *       - bearerAuth: []
 */
router.get('/CompetitionsAll', authMiddleware, CompetitionsAll)

// Ruta para obtener el número total de competiciones
/**
 * @swagger
 * /tenant_competitions/CompetitionsCount:
 *   get:
 *     summary: Obtener el número total de competiciones
 *     tags: [Tenant_Competitions]
 *     responses:
 *       200:
 *         description: Total de competiciones obtenido exitosamente
 *         content:
 *           application/json:
 *             schema:
 *               $ref: '#/components/responses/success'
 *       403:
 *         description: Error de autenticación
 *         content:
 *           application/json:
 *             schema:
 *               $ref: '#/components/responses/UnauthorizedError'
 *       500:
 *         description: Error en el servidor
 *         content:
 *           application/json:
 *             schema:
 *               $ref: '#/components/responses/InternalServerError'
 *     security:
 *       - bearerAuth: []
 */
router.get('/CompetitionsCount', authMiddleware, CompetitionsCount)

// Ruta para agregar la foto de perfil de una competición
/**
 * @swagger
 * /tenant_competitions/AddProfilePicture/{guid}:
 *   post:
 *     summary: Agregar la foto de perfil de una competición
 *     tags: [Tenant_Competitions]
 *     parameters:
 *       - in: path
 *         name: guid
 *         schema:
 *           type: string
 *           format: uuid
 *         required: true
 *         description: El GUID de la competición
 *     requestBody:
 *       required: true
 *       content:
 *         multipart/form-data:
 *           schema:
 *             type: object
 *             properties:
 *               profile_picture:
 *                 type: string
 *                 format: binary
 *     responses:
 *       200:
 *         description: Foto de perfil de la competición agregada exitosamente
 *         content:
 *           application/json:
 *             schema:
 *               $ref: '#/components/responses/success'
 *       400:
 *         description: Error en los datos enviados
 *         content:
 *           application/json:
 *             schema:
 *               $ref: '#/components/responses/BadRequest'
 *       404:
 *         description: Competición no encontrada
 *         content:
 *           application/json:
 *             schema:
 *               $ref: '#/components/responses/NotFoundError'
 *       409:
 *         description: Error al subir la foto de perfil de la competición
 *         content:
 *           application/json:
 *             schema:
 *               $ref: '#/components/responses/ConflictError'
 *       500:
 *         description: Error en el servidor
 *         content:
 *           application/json:
 *             schema:
 *               $ref: '#/components/responses/InternalServerError'
 *     security:
 *       - bearerAuth: []
 */
router.post(
    '/UploadProfile/:guid',
    uploadMiddleware('profile_picture_competition').single('logo_path'),
    UploadProfilePicture
)

module.exports = router
