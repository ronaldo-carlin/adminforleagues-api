const express = require('express')
const router = express.Router()

// Controladores y validadores para sports
const {
    validatorCreateSport,
    validatorSport,
    validatorUpdateSport,
    validatorDeleteSport,
    validatorDisableSport,
    validatorEnableSport,
} = require('../../validators/tenant/tenant_sports')

const {
    CreateSport,
    Sports,
    Sport,
    UpdateSport,
    DeleteSport,
    DisableSport,
    EnableSport,
    SportsAll,
    SportsCount,
} = require('../../controllers/tenant/tenant_sports')

const authMiddleware = require('../../middleware/session_tenant')

/* Rutas protegidas que requieren autenticación */
/**
 * @swagger
 * tags:
 *   name: Tenant_Sports
 *   description: API para la gestión de deportes
 */

// Ruta para crear un nuevo deporte
/**
 * @swagger
 * /tenant_sports/CreateSport:
 *   post:
 *     summary: Crear un nuevo deporte
 *     description: Esta ruta permite crear un nuevo deporte con los detalles especificados.
 *     tags: [Tenant_Sports]
 *     requestBody:
 *       required: true
 *       content:
 *         application/json:
 *           schema:
 *             type: object
 *             properties:
 *               name:
 *                 type: string
 *                 example: "Fútbol"
 *               description:
 *                 type: string
 *                 example: "Un deporte de equipo jugado entre dos equipos de once jugadores con una pelota esférica."
 *     responses:
 *       201:
 *         description: Deporte creado exitosamente
 *         content:
 *           application/json:
 *             schema:
 *               $ref: '#/components/responses/success'
 *       400:
 *         description: Error en los datos enviados
 *         content:
 *           application/json:
 *             schema:
 *               $ref: '#/components/responses/BadRequest'
 *       403:
 *         description: Error de autenticación
 *         content:
 *           application/json:
 *             schema:
 *               $ref: '#/components/responses/UnauthorizedError'
 *       409:
 *         description: El deporte ya existe
 *         content:
 *           application/json:
 *             schema:
 *               $ref: '#/components/responses/ConflictError'
 *       500:
 *         description: Error en el servidor
 *         content:
 *           application/json:
 *             schema:
 *               $ref: '#/components/responses/InternalServerError'
 *     security:
 *        - bearerAuth: []
 */
router.post('/CreateSport', authMiddleware, validatorCreateSport(), CreateSport)

// Ruta para obtener todos los deportes activos
/**
 * @swagger
 * /tenant_sports/Sports:
 *   get:
 *     summary: Obtener todos los deportes activos
 *     tags: [Tenant_Sports]
 *     responses:
 *       200:
 *         description: Deportes obtenidos exitosamente
 *         content:
 *           application/json:
 *             schema:
 *               $ref: '#/components/responses/success'
 *       403:
 *         description: Error de autenticación
 *         content:
 *           application/json:
 *             schema:
 *               $ref: '#/components/responses/UnauthorizedError'
 *       500:
 *         description: Error en el servidor
 *         content:
 *           application/json:
 *             schema:
 *               $ref: '#/components/responses/InternalServerError'
 *     security:
 *        - bearerAuth: []
 */
router.get('/Sports', authMiddleware, Sports)

// Ruta para obtener un deporte por GUID
/**
 * @swagger
 * /tenant_sports/Sport/{guid}:
 *   get:
 *     summary: Obtener un deporte por GUID
 *     tags: [Tenant_Sports]
 *     parameters:
 *       - in: path
 *         name: guid
 *         schema:
 *           type: string
 *           format: uuid
 *         required: true
 *         description: El GUID del deporte
 *     responses:
 *       200:
 *         description: Deporte obtenido exitosamente
 *         content:
 *           application/json:
 *             schema:
 *               $ref: '#/components/responses/success'
 *       403:
 *         description: Error de autenticación
 *         content:
 *           application/json:
 *             schema:
 *               $ref: '#/components/responses/UnauthorizedError'
 *       404:
 *         description: Deporte no encontrado
 *         content:
 *           application/json:
 *             schema:
 *               $ref: '#/components/responses/NotFoundError'
 *       500:
 *         description: Error en el servidor
 *         content:
 *           application/json:
 *             schema:
 *               $ref: '#/components/responses/InternalServerError'
 *     security:
 *        - bearerAuth: []
 */
router.get('/Sport/:guid', authMiddleware, validatorSport(), Sport)

// Ruta para actualizar un deporte
/**
 * @swagger
 * /tenant_sports/UpdateSport/{guid}:
 *   patch:
 *     summary: Actualizar un deporte
 *     tags: [Tenant_Sports]
 *     requestBody:
 *       required: true
 *       content:
 *         application/json:
 *           schema:
 *             type: object
 *             properties:
 *               name:
 *                 type: string
 *                 example: "Fútbol"
 *               description:
 *                 type: string
 *                 example: "Un deporte de equipo jugado entre dos equipos de once jugadores con una pelota esférica."
 *     responses:
 *       200:
 *         description: Deporte actualizado exitosamente
 *         content:
 *           application/json:
 *             schema:
 *               $ref: '#/components/responses/success'
 *       400:
 *         description: Error en los datos enviados
 *         content:
 *           application/json:
 *             schema:
 *               $ref: '#/components/responses/BadRequest'
 *       403:
 *         description: Error de autenticación
 *         content:
 *           application/json:
 *             schema:
 *               $ref: '#/components/responses/UnauthorizedError'
 *       404:
 *         description: Deporte no encontrado
 *         content:
 *           application/json:
 *             schema:
 *               $ref: '#/components/responses/NotFoundError'
 *       500:
 *         description: Error en el servidor
 *         content:
 *           application/json:
 *             schema:
 *               $ref: '#/components/responses/InternalServerError'
 *     security:
 *        - bearerAuth: []
 */
router.patch(
    '/UpdateSport/:guid',
    authMiddleware,
    validatorUpdateSport(),
    UpdateSport
)

// Ruta para eliminar un deporte
/**
 * @swagger
 * /tenant_sports/DeleteSport/{guid}:
 *   delete:
 *     summary: Eliminar un deporte
 *     tags: [Tenant_Sports]
 *     parameters:
 *       - in: path
 *         name: guid
 *         schema:
 *           type: string
 *           format: uuid
 *         required: true
 *         description: El GUID del deporte
 *     responses:
 *       200:
 *         description: Deporte eliminado exitosamente
 *         content:
 *           application/json:
 *             schema:
 *               $ref: '#/components/responses/success'
 *       400:
 *         description: Error en los datos enviados
 *         content:
 *           application/json:
 *             schema:
 *               $ref: '#/components/responses/BadRequest'
 *       403:
 *         description: Error de autenticación
 *         content:
 *           application/json:
 *             schema:
 *               $ref: '#/components/responses/UnauthorizedError'
 *       404:
 *         description: Deporte no encontrado
 *         content:
 *           application/json:
 *             schema:
 *               $ref: '#/components/responses/NotFoundError'
 *       500:
 *         description: Error en el servidor
 *         content:
 *           application/json:
 *             schema:
 *               $ref: '#/components/responses/InternalServerError'
 *     security:
 *        - bearerAuth: []
 */
router.delete(
    '/DeleteSport/:guid',
    authMiddleware,
    validatorDeleteSport(),
    DeleteSport
)

// Ruta para deshabilitar un deporte
/**
 * @swagger
 * /tenant_sports/DisableSport/{guid}:
 *   patch:
 *     summary: Deshabilitar un deporte
 *     tags: [Tenant_Sports]
 *     requestBody:
 *       required: true
 *       content:
 *         application/json:
 *           schema:
 *             type: object
 *             properties:
 *               guid:
 *                 type: uuid
 *                 example: 495db854-849b-41cd-b6b9-9c84824c92ba
 *     responses:
 *       200:
 *         description: Deporte deshabilitado exitosamente
 *         content:
 *           application/json:
 *             schema:
 *               $ref: '#/components/responses/success'
 *       400:
 *         description: Error en los datos enviados
 *         content:
 *           application/json:
 *             schema:
 *               $ref: '#/components/responses/BadRequest'
 *       403:
 *         description: Error de autenticación
 *         content:
 *           application/json:
 *             schema:
 *               $ref: '#/components/responses/UnauthorizedError'
 *       404:
 *         description: Deporte no encontrado
 *         content:
 *           application/json:
 *             schema:
 *               $ref: '#/components/responses/NotFoundError'
 *       500:
 *         description: Error en el servidor
 *         content:
 *           application/json:
 *             schema:
 *               $ref: '#/components/responses/InternalServerError'
 *     security:
 *       - bearerAuth: []
 */
router.patch(
    '/DisableSport/:guid',
    authMiddleware,
    validatorDisableSport(),
    DisableSport
)

// Ruta para habilitar un deporte
/**
 * @swagger
 * /tenant_sports/EnableSport/{guid}:
 *   patch:
 *     summary: Habilitar un deporte
 *     tags: [Tenant_Sports]
 *     requestBody:
 *       required: true
 *       content:
 *         application/json:
 *           schema:
 *             type: object
 *             properties:
 *               guid:
 *                 type: uuid
 *                 example: 495db854-849b-41cd-b6b9-9c84824c92ba
 *     responses:
 *       200:
 *         description: Deporte habilitado exitosamente
 *         content:
 *           application/json:
 *             schema:
 *               $ref: '#/components/responses/success'
 *       400:
 *         description: Error en los datos enviados
 *         content:
 *           application/json:
 *             schema:
 *               $ref: '#/components/responses/BadRequest'
 *       403:
 *         description: Error de autenticación
 *         content:
 *           application/json:
 *             schema:
 *               $ref: '#/components/responses/UnauthorizedError'
 *       404:
 *         description: Deporte no encontrado
 *         content:
 *           application/json:
 *             schema:
 *               $ref: '#/components/responses/NotFoundError'
 *       500:
 *         description: Error en el servidor
 *         content:
 *           application/json:
 *             schema:
 *               $ref: '#/components/responses/InternalServerError'
 *     security:
 *        - bearerAuth: []
 */
router.patch(
    '/EnableSport/:guid',
    authMiddleware,
    validatorEnableSport(),
    EnableSport
)

// Ruta para obtener todos los deportes con estado de activación
/**
 * @swagger
 * /tenant_sports/SportsAll:
 *   get:
 *     summary: Obtener todos los deportes con estado de activación
 *     tags: [Tenant_Sports]
 *     responses:
 *       200:
 *         description: Deportes obtenidos exitosamente
 *         content:
 *           application/json:
 *             schema:
 *               $ref: '#/components/responses/success'
 *       403:
 *         description: Error de autenticación
 *         content:
 *           application/json:
 *             schema:
 *               $ref: '#/components/responses/UnauthorizedError'
 *       500:
 *         description: Error en el servidor
 *         content:
 *           application/json:
 *             schema:
 *               $ref: '#/components/responses/InternalServerError'
 *     security:
 *        - bearerAuth: []
 */
router.get('/SportsAll', authMiddleware, SportsAll)

// Ruta para obtener el conteo de deportes
/**
 * @swagger
 * /tenant_sports/SportsCount:
 *   get:
 *     summary: Obtener el conteo de deportes
 *     tags: [Tenant_Sports]
 *     responses:
 *       200:
 *         description: Conteo de deportes obtenido exitosamente
 *         content:
 *           application/json:
 *             schema:
 *               $ref: '#/components/responses/success'
 *       403:
 *         description: Error de autenticación
 *         content:
 *           application/json:
 *             schema:
 *               $ref: '#/components/responses/UnauthorizedError'
 *       500:
 *         description: Error en el servidor
 *         content:
 *           application/json:
 *             schema:
 *               $ref: '#/components/responses/InternalServerError'
 *     security:
 *        - bearerAuth: []
 */
router.get('/SportsCount', authMiddleware, SportsCount)

module.exports = router
