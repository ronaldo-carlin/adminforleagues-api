// Importar Sequelize
const { DataTypes } = require('sequelize')

// Definir la función factory para el modelo CoachTeamsTenantModel
const CoachTeamsTenantModelFactory = (sequelize) => {
    const CoachTeamsTenantModel = sequelize.define(
        'coach_teams',
        {
            id: {
                type: DataTypes.INTEGER,
                primaryKey: true,
                autoIncrement: true,
            },
            guid: {
                type: DataTypes.UUID,
                primaryKey: true,
                defaultValue: DataTypes.UUIDV4,
                unique: true,
            },
            user_guid: {
                type: DataTypes.TEXT,
            },
            team_guid: {
                type: DataTypes.TEXT,
            },
            is_owner: {
                type: DataTypes.INTEGER,
            },
            is_active: {
                type: DataTypes.INTEGER,
                defaultValue: 1,
            },
            created_by: {
                type: DataTypes.UUID,
            },
            updated_by: {
                type: DataTypes.UUID,
            },
            deleted_by: {
                type: DataTypes.UUID,
            },
            created_at: {
                type: DataTypes.DATE,
            },
            updated_at: {
                type: DataTypes.DATE,
            },
            deleted_at: {
                type: DataTypes.DATE,
            },
        },
        {
            tableName: 'coach_teams',
            timestamps: false,
        }
    )

    return CoachTeamsTenantModel
}

module.exports = CoachTeamsTenantModelFactory
