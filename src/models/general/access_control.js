// Importar Sequelize directamente
const { Sequelize } = require('sequelize')
// Importar tu configuración de sequelize
const { sequelize } = require('../../database/mysql')
// Importar el modelo de roles
const RolesModel = require('./roles')
// Importar el modelo de modulos
const ModulesModel = require('./modules')
// Importar el modelo de permisos
const PermissionsModel = require('./permissions')
// Importar el modelo de planes
const PlansModel = require('./plans')
// Importar el modelo de licencias
const LicensesModel = require('./licenses')

const AccessControlModel = sequelize.define(
    'access_control',
    {
        id: {
            type: Sequelize.INTEGER,
            primaryKey: true,
            autoIncrement: true,
        },
        guid: {
            type: Sequelize.UUID,
            defaultValue: Sequelize.UUIDV4,
            unique: true,
        },
        role_guid: {
            type: Sequelize.UUID,
        },
        module_guid: {
            type: Sequelize.UUID,
        },
        permissions_guid: {
            type: Sequelize.UUID,
        },
        plan_guid: {
            type: Sequelize.UUID,
        },
        license_guid: {
            type: Sequelize.UUID,
        },
        is_active: {
            type: Sequelize.BOOLEAN,
        },
        created_by: {
            type: Sequelize.UUID,
        },
        updated_by: {
            type: Sequelize.UUID,
        },
        deleted_by: {
            type: Sequelize.UUID,
        },
        created_at: {
            type: Sequelize.DATE,
        },
        updated_at: {
            type: Sequelize.DATE,
        },
        deleted_at: {
            type: Sequelize.DATE,
        },
    },
    {
        tableName: 'access_control',
        timestamps: false,
        defaultScope: {
            attributes: {
                exclude: [
                    'role_guid',
                    'module_guid',
                    'permissions_guid',
                    'plan_guid',
                    'license_guid',
                    'is_active',
                    'created_by',
                    'updated_by',
                    'deleted_by',
                    'created_at',
                    'updated_at',
                    'deleted_at',
                ],
            },
            include: [
                {
                    model: RolesModel,
                    as: 'role',
                    attributes: ['guid', 'name'],
                    required: false,
                    where: { is_active: 1 },
                    exclude: [
                        'is_active',
                        'created_at',
                        'created_by',
                        'updated_at',
                        'updated_by',
                        'deleted_at',
                        'deleted_by',
                    ],
                },
                {
                    model: ModulesModel,
                    as: 'module',
                    attributes: ['guid', 'name'],
                    required: false,
                    where: { is_active: 1 },
                    exclude: [
                        'is_active',
                        'created_at',
                        'created_by',
                        'updated_at',
                        'updated_by',
                        'deleted_at',
                        'deleted_by',
                    ],
                },
                {
                    model: PermissionsModel,
                    as: 'permissions',
                    attributes: ['guid', 'name'],
                    required: false,
                    where: { is_active: 1 },
                    exclude: [
                        'is_active',
                        'created_at',
                        'created_by',
                        'updated_at',
                        'updated_by',
                        'deleted_at',
                        'deleted_by',
                    ],
                },
                {
                    model: PlansModel,
                    as: 'plan',
                    attributes: ['guid', 'name'],
                    required: false,
                    where: { is_active: 1 },
                    exclude: [
                        'is_active',
                        'created_at',
                        'created_by',
                        'updated_at',
                        'updated_by',
                        'deleted_at',
                        'deleted_by',
                    ],
                },
            ],
        },
    }
)

// Asociación con el modelo de roles
AccessControlModel.belongsTo(RolesModel, {
    foreignKey: 'role_guid', // El campo de la tabla access_control que se usará para la relación
    targetKey: 'guid', // El campo de la tabla roles que se usará para la relación
    as: 'role', // El alias de la relación
})

// Asociación con el modelo de modulos
AccessControlModel.belongsTo(ModulesModel, {
    foreignKey: 'module_guid', // El campo de la tabla access_control que se usará para la relación
    targetKey: 'guid', // El campo de la tabla modules que se usará para la relación
    as: 'module', // El alias de la relación
})

// Asociación con el modelo de permisos
AccessControlModel.belongsTo(PermissionsModel, {
    foreignKey: 'permissions_guid', // El campo de la tabla access_control que se usará para la relación
    targetKey: 'guid', // El campo de la tabla permissions que se usará para la relación
    as: 'permissions', // El alias de la relación
})

// Asociación con el modelo de planes
AccessControlModel.belongsTo(PlansModel, {
    foreignKey: 'plan_guid', // El campo de la tabla access_control que se usará para la relación
    targetKey: 'guid', // El campo de la tabla plans que se usará para la relación
    as: 'plan', // El alias de la relación
})

// Asociación con el modelo de licencias
AccessControlModel.belongsTo(LicensesModel, {
    foreignKey: 'license_guid', // El campo de la tabla access_control que se usará para la relación
    targetKey: 'guid', // El campo de la tabla licenses que se usará para la relación
    as: 'license', // El alias de la relación
})

module.exports = AccessControlModel
