const { v4: uuidv4 } = require('uuid')
const GUIDsGeneral = require('../guids_general')
module.exports = {
    async up(queryInterface, Sequelize) {
        // Inserta un usuario por defecto
        await queryInterface.bulkInsert(
            'tenant_credentials',
            [
                {
                    guid: uuidv4(),
                    tenant_guid: GUIDsGeneral.tenantSuperAdminGuid,
                    domain: 'adminforleagues.com',
                    subdomain: 'localhost:3013',
                    database: 'tenant_superadmin_2024',
                    user: 'root',
                    password: '',
                    host: 'localhost',
                    port: '3306',
                    is_active: 1, // 1: Activo, 0: Inactivo
                    created_at: new Date(),
                },
            ],
            {}
        )
    },

    async down(queryInterface, Sequelize) {
        await queryInterface.bulkDelete('tenant_credentials', null, {})
    },
}
