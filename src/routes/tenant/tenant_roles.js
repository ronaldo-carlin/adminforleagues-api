const express = require('express')
const router = express.Router()

// controladores de roles y middleware
const {
    validatorCreateRole,
    validatorUpdateRole,
    validatorDeleteRole,
    validatorDisableRole,
    validatorEnableRole,
} = require('../../validators/tenant/tenant_roles')

const {
    CreateRole,
    Roles,
    Role,
    UpdateRole,
    DeleteRole,
    DisableRole,
    EnableRole,
    RolesAll,
    RolesCount,
} = require('../../controllers/tenant/tenant_roles')

const authMiddleware = require('../../middleware/session_tenant')

/* Rutas protegidas que requieren autenticación */
/**
 * @swagger
 * tags:
 *   name: Tenant_Roles
 *   description: API para la gestión de roles
 */

// Ruta para crear un nuevo rol
/**
 * @swagger
 * /tenant_roles/CreateRole:
 *   post:
 *     summary: Crear un nuevo rol
 *     description: Esta ruta permite crear un nuevo rol con los detalles especificados.
 *     tags: [Tenant_Roles]
 *     requestBody:
 *       required: true
 *       content:
 *         application/json:
 *           schema:
 *             type: object
 *             properties:
 *               name:
 *                 type: string
 *                 example: admin
 *               description:
 *                 type: string
 *                 example: Administrador del sistema
 *     responses:
 *       201:
 *         description: Rol creado exitosamente
 *         content:
 *           application/json:
 *             schema:
 *               $ref: '#/components/responses/success'
 *       400:
 *         description: Error en los datos enviados
 *         content:
 *           application/json:
 *             schema:
 *               $ref: '#/components/responses/BadRequest'
 *       403:
 *         description: Error de autenticación
 *         content:
 *           application/json:
 *             schema:
 *               $ref: '#/components/responses/UnauthorizedError'
 *       409:
 *         description: El rol ya existe
 *         content:
 *           application/json:
 *             schema:
 *               $ref: '#/components/responses/ConflictError'
 *       500:
 *         description: Error en el servidor
 *         content:
 *           application/json:
 *             schema:
 *               $ref: '#/components/responses/InternalServerError'
 *     security:
 *        - bearerAuth: []
 */
router.post('/CreateRol', authMiddleware, validatorCreateRole(), CreateRole)

// Ruta para obtener todos los roles activos
/**
 * @swagger
 * /tenant_roles/Roles:
 *   get:
 *     summary: Obtener todos los roles activos
 *     tags: [Tenant_Roles]
 *     responses:
 *       200:
 *         description: Roles obtenidos exitosamente
 *         content:
 *           application/json:
 *             schema:
 *               $ref: '#/components/responses/success'
 *       403:
 *         description: Error de autenticación
 *         content:
 *           application/json:
 *             schema:
 *               $ref: '#/components/responses/UnauthorizedError'
 *       500:
 *         description: Error en el servidor
 *         content:
 *           application/json:
 *             schema:
 *               $ref: '#/components/responses/InternalServerError'
 *     security:
 *        - bearerAuth: []
 */
router.get('/Roles', authMiddleware, Roles)

// Ruta para obtener un rol por Guid
/**
 * @swagger
 * /tenant_roles/Role:
 *   get:
 *     summary: Obtener un rol por ID
 *     tags: [Tenant_Roles]
 *     parameters:
 *       - in: path
 *         name: guid
 *         schema:
 *           type: string
 *           format: uuid
 *         required: true
 *         description: El GUID del rol
 *     responses:
 *       200:
 *         description: Rol obtenido exitosamente
 *         content:
 *           application/json:
 *             schema:
 *               $ref: '#/components/responses/success'
 *       403:
 *         description: Error de autenticación
 *         content:
 *           application/json:
 *             schema:
 *               $ref: '#/components/responses/UnauthorizedError'
 *       404:
 *         description: Rol no encontrado
 *         content:
 *           application/json:
 *             schema:
 *               $ref: '#/components/responses/NotFoundError'
 *       500:
 *         description: Error en el servidor
 *         content:
 *           application/json:
 *             schema:
 *               $ref: '#/components/responses/InternalServerError'
 *     security:
 *        - bearerAuth: []
 */
router.get('/Rol/:guid', authMiddleware, Role)

// Ruta para actualizar un rol
/**
 * @swagger
 * /tenant_roles/UpdateRol:
 *   patch:
 *     summary: Actualizar un rol
 *     tags: [Tenant_Roles]
 *     requestBody:
 *       required: true
 *       content:
 *         application/json:
 *           schema:
 *             type: object
 *             properties:
 *               guid:
 *                 type: uuid
 *                 example: 495db854-849b-41cd-b6b9-9c84824c92ba
 *               name:
 *                 type: string
 *                 example: admin
 *               description:
 *                 type: string
 *                 example: Administrador del sistema
 *     responses:
 *       200:
 *         description: Rol actualizado exitosamente
 *         content:
 *           application/json:
 *             schema:
 *               $ref: '#/components/responses/success'
 *       400:
 *         description: Error en los datos enviados
 *         content:
 *           application/json:
 *             schema:
 *               $ref: '#/components/responses/BadRequest'
 *       403:
 *         description: Error de autenticación
 *         content:
 *           application/json:
 *             schema:
 *               $ref: '#/components/responses/UnauthorizedError'
 *       404:
 *         description: Rol no encontrado
 *         content:
 *           application/json:
 *             schema:
 *               $ref: '#/components/responses/NotFoundError'
 *       500:
 *         description: Error en el servidor
 *         content:
 *           application/json:
 *             schema:
 *               $ref: '#/components/responses/InternalServerError'
 *     security:
 *        - bearerAuth: []
 */
router.patch(
    '/UpdateRol/:guid',
    authMiddleware,
    validatorUpdateRole(),
    UpdateRole
)

// Ruta para eliminar un rol
/**
 * @swagger
 * /tenant_roles/DeleteRol:
 *   delete:
 *     summary: Eliminar un rol
 *     tags: [Tenant_Roles]
 *     parameters:
 *       - in: path
 *         name: guid
 *         schema:
 *           type: string
 *           format: uuid
 *         required: true
 *         description: El GUID del rol
 *     responses:
 *       200:
 *         description: Rol eliminado exitosamente
 *         content:
 *           application/json:
 *             schema:
 *               $ref: '#/components/responses/success'
 *       400:
 *         description: Error en los datos enviados
 *         content:
 *           application/json:
 *             schema:
 *               $ref: '#/components/responses/BadRequest'
 *       403:
 *         description: Error de autenticación
 *         content:
 *           application/json:
 *             schema:
 *               $ref: '#/components/responses/UnauthorizedError'
 *       404:
 *         description: Rol no encontrado
 *         content:
 *           application/json:
 *             schema:
 *               $ref: '#/components/responses/NotFoundError'
 *       500:
 *         description: Error en el servidor
 *         content:
 *           application/json:
 *             schema:
 *               $ref: '#/components/responses/InternalServerError'
 *     security:
 *        - bearerAuth: []
 */
router.delete(
    '/DeleteRol/:guid',
    authMiddleware,
    validatorDeleteRole(),
    DeleteRole
)

// Ruta para deshabilitar un rol
/**
 * @swagger
 * /tenant_roles/DisableRol:
 *   patch:
 *     summary: Deshabilitar un rol
 *     tags: [Tenant_Roles]
 *     requestBody:
 *       required: true
 *       content:
 *         application/json:
 *           schema:
 *             type: object
 *             properties:
 *               guid:
 *                 type: uuid
 *                 example: 495db854-849b-41cd-b6b9-9c84824c92ba
 *     responses:
 *       200:
 *         description: Rol deshabilitado exitosamente
 *         content:
 *           application/json:
 *             schema:
 *               $ref: '#/components/responses/success'
 *       400:
 *         description: Error en los datos enviados
 *         content:
 *           application/json:
 *             schema:
 *               $ref: '#/components/responses/BadRequest'
 *       403:
 *         description: Error de autenticación
 *         content:
 *           application/json:
 *             schema:
 *               $ref: '#/components/responses/UnauthorizedError'
 *       404:
 *         description: Rol no encontrado
 *         content:
 *           application/json:
 *             schema:
 *               $ref: '#/components/responses/NotFoundError'
 *       500:
 *         description: Error en el servidor
 *         content:
 *           application/json:
 *             schema:
 *               $ref: '#/components/responses/InternalServerError'
 *     security:
 *        - bearerAuth: []
 */
router.patch(
    '/DisableRol/:guid',
    authMiddleware,
    validatorDisableRole(),
    DisableRole
)

// Ruta para habilitar un rol
/**
 * @swagger
 * /tenant_roles/EnableRol:
 *   patch:
 *     summary: Habilitar un rol
 *     tags: [Tenant_Roles]
 *     requestBody:
 *       required: true
 *       content:
 *         application/json:
 *           schema:
 *             type: object
 *             properties:
 *               guid:
 *                 type: uuid
 *                 example: 495db854-849b-41cd-b6b9-9c84824c92ba
 *     responses:
 *       200:
 *         description: Rol habilitado exitosamente
 *         content:
 *           application/json:
 *             schema:
 *               $ref: '#/components/responses/success'
 *       400:
 *         description: Error en los datos enviados
 *         content:
 *           application/json:
 *             schema:
 *               $ref: '#/components/responses/BadRequest'
 *       403:
 *         description: Error de autenticación
 *         content:
 *           application/json:
 *             schema:
 *               $ref: '#/components/responses/UnauthorizedError'
 *       404:
 *         description: Rol no encontrado
 *         content:
 *           application/json:
 *             schema:
 *               $ref: '#/components/responses/NotFoundError'
 *       500:
 *         description: Error en el servidor
 *         content:
 *           application/json:
 *             schema:
 *               $ref: '#/components/responses/InternalServerError'
 *     security:
 *        - bearerAuth: []
 */
router.patch(
    '/EnableRol/:guid',
    authMiddleware,
    validatorEnableRole(),
    EnableRole
)

// Ruta para obtener todos los roles (activos e inactivos)
/**
 * @swagger
 * /tenant_roles/RolesAll:
 *   get:
 *     summary: Obtener todos los roles
 *     tags: [Tenant_Roles]
 *     responses:
 *       200:
 *         description: Roles obtenidos exitosamente
 *         content:
 *           application/json:
 *             schema:
 *               $ref: '#/components/responses/success'
 *       403:
 *         description: Error de autenticación
 *         content:
 *           application/json:
 *             schema:
 *               $ref: '#/components/responses/UnauthorizedError'
 *       500:
 *         description: Error en el servidor
 *         content:
 *           application/json:
 *             schema:
 *               $ref: '#/components/responses/InternalServerError'
 *     security:
 *        - bearerAuth: []
 */
router.get('/RolesAll', authMiddleware, RolesAll)

// Ruta para obtener el conteo de roles
/**
 * @swagger
 * /tenant_roles/RolesCount:
 *   get:
 *     summary: Obtener el conteo de roles
 *     tags: [Tenant_Roles]
 *     responses:
 *       200:
 *         description: Conteo de roles obtenido exitosamente
 *         content:
 *           application/json:
 *             schema:
 *               $ref: '#/components/responses/success'
 *       403:
 *         description: Error de autenticación
 *         content:
 *           application/json:
 *             schema:
 *               $ref: '#/components/responses/UnauthorizedError'
 *       500:
 *         description: Error en el servidor
 *         content:
 *           application/json:
 *             schema:
 *               $ref: '#/components/responses/InternalServerError'
 *     security:
 *        - bearerAuth: []
 */
router.get('/RolesCount', authMiddleware, RolesCount)

module.exports = router
