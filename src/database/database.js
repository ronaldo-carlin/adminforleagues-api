const { Sequelize } = require('sequelize')
const TenantCredentials = require('../models/general/tenant_credentials')

const database = process.env.MYSQL_DATABASE
const username = process.env.MYSQL_USER
const password = process.env.MYSQL_PASSWORD
const host = process.env.MYSQL_HOST

const tenantDatabaseMap = new Map()

const getSequelizeInstance = async (subdomain) => {
    if (!subdomain) {
        return new Sequelize(database, username, password, {
            host,
            dialect: 'mysql',
            logging: false,
        })
    }

    if (tenantDatabaseMap.has(subdomain)) {
        return tenantDatabaseMap.get(subdomain)
    }

    const credentials = await TenantCredentials.findOne({
        where: { subdomain: subdomain, is_active: 1 },
    })

    if (!credentials) {
        throw new Error('Tenant not found')
    }

    const sequelize = new Sequelize(
        credentials.database,
        credentials.user,
        credentials.password,
        {
            host: credentials.host,
            dialect: 'mysql',
            logging: false,
        }
    )

    await sequelize.authenticate()
    tenantDatabaseMap.set(subdomain, sequelize)

    return sequelize
}

module.exports = {
    getSequelizeInstance,
}
