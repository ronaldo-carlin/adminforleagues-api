const express = require('express')
const router = express.Router()

// Controladores y validadores para Types competitions
const {
    validatorCreateTypesCompetition,
    validatorTypesCompetition,
    validatorUpdateTypesCompetition,
    validatorDeleteTypesCompetition,
    validatorDisableTypesCompetition,
    validatorEnableTypesCompetition,
} = require('../../validators/tenant/tenant_types_competitions')

const {
    CreateTypesCompetition,
    TypesCompetitions,
    TypesCompetition,
    UpdateTypesCompetition,
    DeleteTypesCompetition,
    DisableTypesCompetition,
    EnableTypesCompetition,
    TypesCompetitionsAll,
    TypesCompetitionsCount,
    TypesCompetitionsSport,
} = require('../../controllers/tenant/tenant_types_competitions')

const authMiddleware = require('../../middleware/session_tenant')

/* Rutas protegidas que requieren autenticación */
/**
 * @swagger
 * tags:
 *   name: Tenant_Types_Competitions
 *   description: API para la gestión de tipos de competencias
 */

// Ruta para crear un nuevo tipo de competencia
/**
 * @swagger
 * /tenant_type_competitions/CreateTypesCompetition:
 *   post:
 *     summary: Crear un nuevo tipo de competencia
 *     description: Esta ruta permite crear un nuevo tipo de competencia con los detalles especificados.
 *     tags: [Tenant_Types_Competitions]
 *     requestBody:
 *       required: true
 *       content:
 *         application/json:
 *           schema:
 *             type: object
 *             properties:
 *               name:
 *                 type: string
 *                 example: "Competencia Internacional"
 *               description:
 *                 type: string
 *                 example: "Tipo de competencia que incluye participantes internacionales."
 *     responses:
 *       201:
 *         description: Tipo de competencia creado exitosamente
 *         content:
 *           application/json:
 *             schema:
 *               $ref: '#/components/responses/success'
 *       400:
 *         description: Error en los datos enviados
 *         content:
 *           application/json:
 *             schema:
 *               $ref: '#/components/responses/BadRequest'
 *       403:
 *         description: Error de autenticación
 *         content:
 *           application/json:
 *             schema:
 *               $ref: '#/components/responses/UnauthorizedError'
 *       409:
 *         description: El tipo de competencia ya existe
 *         content:
 *           application/json:
 *             schema:
 *               $ref: '#/components/responses/ConflictError'
 *       500:
 *         description: Error en el servidor
 *         content:
 *           application/json:
 *             schema:
 *               $ref: '#/components/responses/InternalServerError'
 *     security:
 *        - bearerAuth: []
 */
router.post(
    '/CreateTypesCompetition',
    authMiddleware,
    validatorCreateTypesCompetition(),
    CreateTypesCompetition
)

// Ruta para obtener todos los tipos de competencias activas
/**
 * @swagger
 * /tenant_type_competitions/TypesCompetitions:
 *   get:
 *     summary: Obtener todos los tipos de competencias activas
 *     tags: [Tenant_Types_Competitions]
 *     responses:
 *       200:
 *         description: Tipos de competencias obtenidas exitosamente
 *         content:
 *           application/json:
 *             schema:
 *               $ref: '#/components/responses/success'
 *       403:
 *         description: Error de autenticación
 *         content:
 *           application/json:
 *             schema:
 *               $ref: '#/components/responses/UnauthorizedError'
 *       500:
 *         description: Error en el servidor
 *         content:
 *           application/json:
 *             schema:
 *               $ref: '#/components/responses/InternalServerError'
 *     security:
 *        - bearerAuth: []
 */
router.get('/TypesCompetitions', authMiddleware, TypesCompetitions)

// Ruta para obtener todas las categorías activas dependiendo el deporte seleccionado
/**
 * @swagger
 * /tenant_type_competitions/TypesCompetitionsSport/{guid}:
 *   get:
 *     summary: Obtener un tipo de competencia por GUID
 *     tags: [Tenant_Types_Competitions]
 *     parameters:
 *       - in: path
 *         name: guid
 *         schema:
 *           type: string
 *           format: uuid
 *         required: true
 *         description: El GUID del tipo de competencia
 *     responses:
 *       200:
 *         description: Tipo de competencia obtenido exitosamente
 *         content:
 *           application/json:
 *             schema:
 *               $ref: '#/components/responses/success'
 *       403:
 *         description: Error de autenticación
 *         content:
 *           application/json:
 *             schema:
 *               $ref: '#/components/responses/UnauthorizedError'
 *       404:
 *         description: Tipo de competencia no encontrado
 *         content:
 *           application/json:
 *             schema:
 *               $ref: '#/components/responses/NotFoundError'
 *       500:
 *         description: Error en el servidor
 *         content:
 *           application/json:
 *             schema:
 *               $ref: '#/components/responses/InternalServerError'
 *     security:
 *        - bearerAuth: []
 */
router.get(
    '/TypesCompetitionsSport/:guid',
    authMiddleware,
    validatorTypesCompetition(),
    TypesCompetitionsSport
)

// Ruta para obtener un tipo de competencia por GUID
/**
 * @swagger
 * /tenant_type_competitions/TypesCompetition/{guid}:
 *   get:
 *     summary: Obtener un tipo de competencia por GUID
 *     tags: [Tenant_Types_Competitions]
 *     parameters:
 *       - in: path
 *         name: guid
 *         schema:
 *           type: string
 *           format: uuid
 *         required: true
 *         description: El GUID del tipo de competencia
 *     responses:
 *       200:
 *         description: Tipo de competencia obtenido exitosamente
 *         content:
 *           application/json:
 *             schema:
 *               $ref: '#/components/responses/success'
 *       403:
 *         description: Error de autenticación
 *         content:
 *           application/json:
 *             schema:
 *               $ref: '#/components/responses/UnauthorizedError'
 *       404:
 *         description: Tipo de competencia no encontrado
 *         content:
 *           application/json:
 *             schema:
 *               $ref: '#/components/responses/NotFoundError'
 *       500:
 *         description: Error en el servidor
 *         content:
 *           application/json:
 *             schema:
 *               $ref: '#/components/responses/InternalServerError'
 *     security:
 *        - bearerAuth: []
 */
router.get(
    '/TypesCompetition/:guid',
    authMiddleware,
    validatorTypesCompetition(),
    TypesCompetition
)

// Ruta para actualizar un tipo de competencia
/**
 * @swagger
 * /tenant_type_competitions/UpdateTypesCompetition/{guid}:
 *   patch:
 *     summary: Actualizar un tipo de competencia
 *     tags: [Tenant_Types_Competitions]
 *     requestBody:
 *       required: true
 *       content:
 *         application/json:
 *           schema:
 *             type: object
 *             properties:
 *               name:
 *                 type: string
 *                 example: "Competencia Internacional"
 *               description:
 *                 type: string
 *                 example: "Tipo de competencia que incluye participantes internacionales."
 *     responses:
 *       200:
 *         description: Tipo de competencia actualizado exitosamente
 *         content:
 *           application/json:
 *             schema:
 *               $ref: '#/components/responses/success'
 *       400:
 *         description: Error en los datos enviados
 *         content:
 *           application/json:
 *             schema:
 *               $ref: '#/components/responses/BadRequest'
 *       403:
 *         description: Error de autenticación
 *         content:
 *           application/json:
 *             schema:
 *               $ref: '#/components/responses/UnauthorizedError'
 *       404:
 *         description: Tipo de competencia no encontrado
 *         content:
 *           application/json:
 *             schema:
 *               $ref: '#/components/responses/NotFoundError'
 *       500:
 *         description: Error en el servidor
 *         content:
 *           application/json:
 *             schema:
 *               $ref: '#/components/responses/InternalServerError'
 *     security:
 *        - bearerAuth: []
 */
router.patch(
    '/UpdateTypesCompetition/:guid',
    authMiddleware,
    validatorUpdateTypesCompetition(),
    UpdateTypesCompetition
)

// Ruta para eliminar un tipo de competencia
/**
 * @swagger
 * /tenant_type_competitions/DeleteTypesCompetition/{guid}:
 *   delete:
 *     summary: Eliminar un tipo de competencia
 *     tags: [Tenant_Types_Competitions]
 *     parameters:
 *       - in: path
 *         name: guid
 *         schema:
 *           type: string
 *           format: uuid
 *         required: true
 *         description: El GUID del tipo de competencia
 *     responses:
 *       200:
 *         description: Tipo de competencia eliminado exitosamente
 *         content:
 *           application/json:
 *             schema:
 *               $ref: '#/components/responses/success'
 *       400:
 *         description: Error en los datos enviados
 *         content:
 *           application/json:
 *             schema:
 *               $ref: '#/components/responses/BadRequest'
 *       403:
 *         description: Error de autenticación
 *         content:
 *           application/json:
 *             schema:
 *               $ref: '#/components/responses/UnauthorizedError'
 *       404:
 *         description: Tipo de competencia no encontrado
 *         content:
 *           application/json:
 *             schema:
 *               $ref: '#/components/responses/NotFoundError'
 *       500:
 *         description: Error en el servidor
 *         content:
 *           application/json:
 *             schema:
 *               $ref: '#/components/responses/InternalServerError'
 *     security:
 *        - bearerAuth: []
 */
router.delete(
    '/DeleteTypesCompetition/:guid',
    authMiddleware,
    validatorDeleteTypesCompetition(),
    DeleteTypesCompetition
)

// Ruta para deshabilitar un tipo de competencia
/**
 * @swagger
 * /tenant_type_competitions/DisableTypesCompetition/{guid}:
 *   patch:
 *     summary: Deshabilitar un tipo de competencia
 *     tags: [Tenant_Types_Competitions]
 *     requestBody:
 *       required: true
 *       content:
 *         application/json:
 *           schema:
 *             type: object
 *             properties:
 *               guid:
 *                 type: uuid
 *                 example: 495db854-849b-41cd-b6b9-9c84824c92ba
 *     responses:
 *       200:
 *         description: Tipo de competencia deshabilitado exitosamente
 *         content:
 *           application/json:
 *             schema:
 *               $ref: '#/components/responses/success'
 *       400:
 *         description: Error en los datos enviados
 *         content:
 *           application/json:
 *             schema:
 *               $ref: '#/components/responses/BadRequest'
 *       403:
 *         description: Error de autenticación
 *         content:
 *           application/json:
 *             schema:
 *               $ref: '#/components/responses/UnauthorizedError'
 *       404:
 *         description: Tipo de competencia no encontrado
 *         content:
 *           application/json:
 *             schema:
 *               $ref: '#/components/responses/NotFoundError'
 *       500:
 *         description: Error en el servidor
 *         content:
 *           application/json:
 *             schema:
 *               $ref: '#/components/responses/InternalServerError'
 *     security:
 *        - bearerAuth: []
 */
router.patch(
    '/DisableTypesCompetition/:guid',
    authMiddleware,
    validatorDisableTypesCompetition(),
    DisableTypesCompetition
)

// Ruta para habilitar un tipo de competencia
/**
 * @swagger
 * /tenant_type_competitions/EnableTypesCompetition/{guid}:
 *   patch:
 *     summary: Habilitar un tipo de competencia
 *     tags: [Tenant_Types_Competitions]
 *     requestBody:
 *       required: true
 *       content:
 *         application/json:
 *           schema:
 *             type: object
 *             properties:
 *               guid:
 *                 type: uuid
 *                 example: 495db854-849b-41cd-b6b9-9c84824c92ba
 *     responses:
 *       200:
 *         description: Tipo de competencia habilitado exitosamente
 *         content:
 *           application/json:
 *             schema:
 *               $ref: '#/components/responses/success'
 *       400:
 *         description: Error en los datos enviados
 *         content:
 *           application/json:
 *             schema:
 *               $ref: '#/components/responses/BadRequest'
 *       403:
 *         description: Error de autenticación
 *         content:
 *           application/json:
 *             schema:
 *               $ref: '#/components/responses/UnauthorizedError'
 *       404:
 *         description: Tipo de competencia no encontrado
 *         content:
 *           application/json:
 *             schema:
 *               $ref: '#/components/responses/NotFoundError'
 *       500:
 *         description: Error en el servidor
 *         content:
 *           application/json:
 *             schema:
 *               $ref: '#/components/responses/InternalServerError'
 *     security:
 *        - bearerAuth: []
 */
router.patch(
    '/EnableTypesCompetition/:guid',
    authMiddleware,
    validatorEnableTypesCompetition(),
    EnableTypesCompetition
)

// Ruta para obtener todos los tipos de competencias
/**
 * @swagger
 * /tenant_type_competitions/TypesCompetitionsAll:
 *   get:
 *     summary: Obtener todos los tipos de competencias
 *     tags: [Tenant_Types_Competitions]
 *     responses:
 *       200:
 *         description: Todos los tipos de competencias obtenidos exitosamente
 *         content:
 *           application/json:
 *             schema:
 *               $ref: '#/components/responses/success'
 *       403:
 *         description: Error de autenticación
 *         content:
 *           application/json:
 *             schema:
 *               $ref: '#/components/responses/UnauthorizedError'
 *       500:
 *         description: Error en el servidor
 *         content:
 *           application/json:
 *             schema:
 *               $ref: '#/components/responses/InternalServerError'
 *     security:
 *        - bearerAuth: []
 */
router.get('/TypesCompetitionsAll', authMiddleware, TypesCompetitionsAll)

// Ruta para obtener el conteo de tipos de competencias
/**
 * @swagger
 * /tenant_type_competitions/TypesCompetitionsCount:
 *   get:
 *     summary: Obtener el conteo de tipos de competencias
 *     tags: [Tenant_Types_Competitions]
 *     responses:
 *       200:
 *         description: Conteo de tipos de competencias obtenido exitosamente
 *         content:
 *           application/json:
 *             schema:
 *               $ref: '#/components/responses/success'
 *       403:
 *         description: Error de autenticación
 *         content:
 *           application/json:
 *             schema:
 *               $ref: '#/components/responses/UnauthorizedError'
 *       500:
 *         description: Error en el servidor
 *         content:
 *           application/json:
 *             schema:
 *               $ref: '#/components/responses/InternalServerError'
 *     security:
 *        - bearerAuth: []
 */
router.get('/TypesCompetitionsCount', authMiddleware, TypesCompetitionsCount)

module.exports = router
