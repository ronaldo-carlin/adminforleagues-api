const { Sequelize } = require('sequelize')
const { exec } = require('child_process')
const util = require('util')
const execPromise = util.promisify(exec)
const { sequelize } = require('../database/mysql')
const TenantCredentialsModel = require('../models/general/tenant_credentials')
const AuthTenantModelFactory = require('../models/tenant/tenant_auth')
const RolesTenantModelFactory = require('../models/tenant/tenant_roles')

const createDatabase = async (dbName) => {
    try {
        await sequelize.query(`CREATE DATABASE IF NOT EXISTS ${dbName}`)
        console.log(`Base de datos ${dbName} creada correctamente.`)
    } catch (error) {
        console.error(`Error al crear la base de datos ${dbName}:`, error)
        throw error
    }
}

const runMigrations = async (dbName) => {
    try {
        process.env.TENANT_DATABASE_NAME = dbName
        process.env.TENANT_DATABASE_USER = process.env.MYSQL_USER
        process.env.TENANT_DATABASE_PASSWORD = process.env.MYSQL_PASSWORD
        process.env.TENANT_DATABASE_HOST = process.env.MYSQL_HOST

        // Ejecutar las migraciones usando el CLI de Sequelize
        const command = `npm run migrate:tenant`
        const { stdout, stderr } = await execPromise(command)

        console.log('Migration stdout:', stdout)
        console.error('Migration stderr:', stderr)

        console.log('Migrations executed successfully.')

        // Insertar los seeders en la base de datos del tenant
        await runSeeders(dbName)
    } catch (error) {
        console.error('Error al ejecutar las migraciones:', error)
        throw error
    } finally {
        // Limpiar las variables de entorno después de las migraciones
        delete process.env.TENANT_DATABASE_NAME
        delete process.env.TENANT_DATABASE_USER
        delete process.env.TENANT_DATABASE_PASSWORD
        delete process.env.TENANT_DATABASE_HOST
    }
}

const createUserWithPermissions = async (dbName, dbUser, dbPassword) => {
    try {
        await sequelize.query(
            `CREATE USER '${dbUser}'@'%' IDENTIFIED BY '${dbPassword}'`
        )

        // Grant specific CRUD permissions
        await sequelize.query(
            `GRANT SELECT, INSERT, UPDATE, DELETE ON ${dbName}.* TO '${dbUser}'@'%'`
        )

        console.log(
            `User ${dbUser} created with CRUD permissions on ${dbName}.`
        )
    } catch (error) {
        console.error(
            `Error creating user ${dbUser} with CRUD permissions on ${dbName}:`,
            error
        )
        throw error
    }
}

const insertTenantCredentials = async (credentials, options) => {
    await TenantCredentialsModel.create(credentials, options)
}

const insertClientData = async (dbName, data, options) => {
    try {
        process.env.TENANT_DATABASE_NAME = dbName
        process.env.TENANT_DATABASE_USER = process.env.MYSQL_USER
        process.env.TENANT_DATABASE_PASSWORD = process.env.MYSQL_PASSWORD
        process.env.TENANT_DATABASE_HOST = process.env.MYSQL_HOST

        // Crear una nueva instancia de Sequelize para la base de datos del tenant
        const sequelizeTenant = new Sequelize(
            dbName,
            process.env.MYSQL_USER,
            process.env.MYSQL_PASSWORD,
            {
                host: process.env.MYSQL_HOST,
                dialect: 'mysql',
            }
        )

        // Autenticar la conexión a la base de datos
        await sequelizeTenant.authenticate()
        console.log(
            `Conexión a la base de datos ${dbName} establecida correctamente.`
        )

        // obtenemos el guid del rol administador para asignarle los permisos antes de insertar el usuario
        const RolesTenantModel = RolesTenantModelFactory(sequelizeTenant)

        const role = await RolesTenantModel.findOne({
            where: { name: 'Super Administrador' },
        })

        // Definir el guid del rol en los datos del usuario
        data.role_guid = role.guid

        // Definir el modelo usando la nueva instancia de Sequelize
        const AuthTenantModel = AuthTenantModelFactory(sequelizeTenant)

        // Insertar los datos en la tabla 'users'
        await AuthTenantModel.create(data)
    } catch (error) {
        console.error('Error al insertar el usuario:', error)
        throw error
    } finally {
        // Limpiar las variables de entorno después de las migraciones
        delete process.env.TENANT_DATABASE_NAME
        delete process.env.TENANT_DATABASE_USER
        delete process.env.TENANT_DATABASE_PASSWORD
        delete process.env.TENANT_DATABASE_HOST
    }
}

const runSeeders = async (dbName) => {
    try {
        process.env.TENANT_DATABASE_NAME = dbName
        process.env.TENANT_DATABASE_USER = process.env.MYSQL_USER
        process.env.TENANT_DATABASE_PASSWORD = process.env.MYSQL_PASSWORD
        process.env.TENANT_DATABASE_HOST = process.env.MYSQL_HOST

        // Ejecutar las migraciones usando el CLI de Sequelize
        const command = `npm run seed:tenant`
        const { stdout, stderr } = await execPromise(command)

        console.log('Seeder stdout:', stdout)
        console.error('Seeder stderr:', stderr)

        console.log('Seeders executed successfully.')
    } catch (error) {
        console.error('Error al ejecutar lo seeders:', error)
        throw error
    } finally {
        // Limpiar las variables de entorno después de las migraciones
        delete process.env.TENANT_DATABASE_NAME
        delete process.env.TENANT_DATABASE_USER
        delete process.env.TENANT_DATABASE_PASSWORD
        delete process.env.TENANT_DATABASE_HOST
    }
}

const runSeedersMockup = async (dbName) => {
    try {
        process.env.TENANT_DATABASE_NAME = dbName
        process.env.TENANT_DATABASE_USER = process.env.MYSQL_USER
        process.env.TENANT_DATABASE_PASSWORD = process.env.MYSQL_PASSWORD
        process.env.TENANT_DATABASE_HOST = process.env.MYSQL_HOST

        // Ejecutar las migraciones usando el CLI de Sequelize
        const command = `npm run seed:tenant:mockup`
        const { stdout, stderr } = await execPromise(command)

        console.log('Seeder stdout:', stdout)
        console.error('Seeder stderr:', stderr)

        console.log('Seeders executed successfully.')
    } catch (error) {
        console.error('Error al ejecutar lo seeders:', error)
        throw error
    } finally {
        // Limpiar las variables de entorno después de las migraciones
        delete process.env.TENANT_DATABASE_NAME
        delete process.env.TENANT_DATABASE_USER
        delete process.env.TENANT_DATABASE_PASSWORD
        delete process.env.TENANT_DATABASE_HOST
    }
}

module.exports = {
    createDatabase,
    runMigrations,
    createUserWithPermissions,
    insertTenantCredentials,
    insertClientData,
    runSeedersMockup,
}
