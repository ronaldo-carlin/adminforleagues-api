const GUIDsGeneral = require('../guids_general')

module.exports = {
    async up(queryInterface, Sequelize) {
        // Inserta los módulos por defecto
        await queryInterface.bulkInsert(
            'modules',
            [
                {
                    guid: GUIDsGeneral.moduleHomeGuid,
                    name: 'Módulo principal',
                    description: 'Módulo principal de la aplicación',
                    to: '/',
                    icon: 'home',
                    is_parent: 1, // 1: Es un módulo padre, 0: No es un módulo padre
                    is_active: 1, // 1: Activo, 0: Inactivo
                    created_at: new Date(),
                },
                {
                    guid: GUIDsGeneral.moduleFinanzasGuid,
                    name: 'Finanzas',
                    description: 'Módulo de finanzas de la aplicación',
                    to: '/finanzas',
                    icon: 'finanzas',
                    is_parent: 1, // 1: Es un módulo padre, 0: No es un módulo padre
                    is_active: 1, // 1: Activo, 0: Inactivo
                    created_at: new Date(),
                },
                {
                    guid: GUIDsGeneral.moduleSoporteGuid,
                    name: 'Soporte',
                    description: 'Módulo de soporte',
                    to: '/informes',
                    icon: 'informes',
                    is_parent: 1, // 1: Es un módulo padre, 0: No es un módulo padre
                    is_active: 1, // 1: Activo, 0: Inactivo
                    created_at: new Date(),
                },
            ],
            {}
        )
    },

    async down(queryInterface, Sequelize) {
        await queryInterface.bulkDelete('modules', null, {})
    },
}
