const RolesModel = require('../../models/general/roles')
const { handleHttpError } = require('../../utils/handleError')
const { handleHttpSuccess } = require('../../utils/handleSuccess')
const { handleHttpCount } = require('../../utils/handleCount')
const { matchedData } = require('express-validator')

/**
 * Este controlador es el encargado de crear los roles
 * @param {*} req
 * @param {*} res
 */
const createRol = async (req, res) => {
    try {
        //Obtenemos los datos que vienen en el body
        const { name, description } = req.body
        //Obtenemos el guid del usuario que esta creando el rol
        const created_by = req.user.guid
        //Creamos el rol
        const role = await RolesModel.create({
            name,
            description,
            created_by,
        })
        //Enviamos los datos
        handleHttpSuccess(res, 'Se ha creado el rol.', 201, role)
    } catch (error) {
        //Obtener y devolver el error
        handleHttpError(res, '', 403, error)
    }
}

/**
 * Este controlador es el encargado de obtener todos los roles activos
 * @param {*} req
 * @param {*} res
 */
const Roles = async (req, res) => {
    try {
        //Buscamos todos los roles
        const role = await RolesModel.findAll({
            where: { is_active: 1 },
            attributes: ['guid', 'name', 'description'],
        })
        //Si no existen roles enviamos error
        if (!role) {
            handleHttpError(res, 'El rol no existe.', 404)
            return
        }
        const message = role.length
            ? 'Se han encontrado los datos.'
            : 'No se encontraron datos.'
        //Enviamos los datos
        handleHttpSuccess(res, message, 201, role)
    } catch (error) {
        //Obtener y devolver el error
        handleHttpError(res, '', 403, error)
    }
}

/**
 * Este controlador es el encargado de obtener los roles por guid
 * @param {*} req
 * @param {*} res
 */
const Rol = async (req, res) => {
    try {
        //Obtenemos el guid que viene en los parametros
        const { guid } = req.params
        //Buscamos el rol
        const role = await RolesModel.findOne({
            where: {
                guid,
            },
        })
        //Si no existe el rol enviamos error
        if (!role) {
            handleHttpError(res, 'El rol no existe.', 404)
            return
        }
        //Enviamos los datos
        handleHttpSuccess(res, 'Se han encontrado los datos.', 201, role)
    } catch (error) {
        //Obtener y devolver el error
        handleHttpError(res, '', 403, error)
    }
}

/**
 * Este controlador es el encargado de actualizar los roles
 * @param {*} req
 * @param {*} res
 */
const updateRol = async (req, res) => {
    try {
        const { guid } = req.params
        //Obtenemos el guid del usuario que esta actualizando el rol
        const updated_by = req.user.guid
        //Obtenemos la fecha actual
        const updated_at = new Date()
        const data = matchedData(req)
        data.updated_by = updated_by
        data.updated_at = updated_at
        //Actualizamos el rol
        const [updateCount] = await RolesModel.update(data, {
            where: { guid },
        })
        if (updateCount === 0) {
            handleHttpError(res, 'El rol no existe.', 404)
            return
        }
        //Buscamos el rol actualizado
        const role = await RolesModel.findOne({
            where: { guid },
        })
        if (!role) {
            handleHttpError(res, 'Error al obtener el rol actualizado.', 500)
            return
        }
        //Enviamos los datos
        handleHttpSuccess(res, 'Se ha actualizado el rol.', 201, role)
    } catch (error) {
        //Obtener y devolver el error
        handleHttpError(res, '', 403, error)
    }
}

/**
 * Este controlador es el encargado de eliminar los roles
 * @param {*} req
 * @param {*} res
 */
const deleteRol = async (req, res) => {
    try {
        //Obtenemos el guid que viene en los parametros
        const { guid } = req.params
        //Obtenemos el guid del usuario que esta eliminando el rol
        const deleted_by = req.user.guid
        //Obtenemos la fecha actual
        const deleted_at = new Date()
        //Eliminamos el rol
        const role = await RolesModel.update(
            {
                is_active: 0,
                deleted_by,
                deleted_at,
            },
            {
                where: {
                    guid,
                },
            }
        )
        //Enviamos los datos
        handleHttpSuccess(res, 'Se ha eliminado el rol.', 201, role)
    } catch (error) {
        //Obtener y devolver el error
        handleHttpError(res, '', 403, error)
    }
}

/**
 * Este controlador es el encargado de desactivar los roles
 * @param {*} req
 * @param {*} res
 */
const disableRol = async (req, res) => {
    try {
        //Obtenemos el guid que viene en los parametros
        const { guid } = req.params
        console.log(req)
        //Obtenemos el guid del usuario que esta desactivando el rol
        const updated_by = req.user.guid
        //Obtenemos la fecha actual
        const updated_at = new Date()
        //Desactivamos el rol
        const [updateCount] = await RolesModel.update(
            {
                is_active: 0,
                updated_by,
                updated_at,
            },
            {
                where: { guid },
            }
        )
        //Si no existe el rol enviamos error
        if (updateCount === 0) {
            handleHttpError(res, 'El rol no existe.', 404)
            return
        }
        //Enviamos los datos
        handleHttpSuccess(res, 'Se ha desactivado el rol.', 201)
    } catch (error) {
        console.log(error)
        //Obtener y devolver el error
        handleHttpError(res, '', 403, error)
    }
}

/**
 * Este controlador es el encargado de activar los roles
 * @param {*} req
 * @param {*} res
 */
const enableRol = async (req, res) => {
    try {
        //Obtenemos el guid que viene en los parametros
        const { guid } = req.params
        //Obtenemos el guid del usuario que esta activando el rol
        const updated_by = req.user.guid
        //Obtenemos la fecha actual
        const updated_at = new Date()
        //Activamos el rol
        const [updateCount] = await RolesModel.update(
            {
                is_active: 1,
                updated_by,
                updated_at,
            },
            {
                where: { guid },
            }
        )
        //Si no existe el rol enviamos error
        if (updateCount === 0) {
            handleHttpError(res, 'El rol no existe.', 404)
            return
        }
        //Enviamos los datos
        handleHttpSuccess(res, 'Se ha activado el rol.', 201)
    } catch (error) {
        console.log(error)
        //Obtener y devolver el error
        handleHttpError(res, '', 403, error)
    }
}

/**
 * Este controlador es el encargado de obtener todos los registros de control de acceso
 * @param {*} req
 * @param {*} res
 */
const rolesAll = async (req, res) => {
    try {
        //Buscamos todos los roles
        const role = await RolesModel.findAll({
            where: { is_active: 1 },
            attributes: ['guid', 'name', 'description'],
        })
        //Si no existen roles enviamos error
        if (!role) {
            handleHttpError(res, 'No se encontraron datos.', 404)
            return
        }
        //Enviamos los datos
        handleHttpSuccess(res, 'Se han encontrado los datos.', 201, role)
    } catch (error) {
        //Obtener y devolver el error
        handleHttpError(res, '', 403, error)
    }
}

/**
 * Este controlador es el encargado de contar todos los roles
 * @param {*} req
 * @param {*} res
 */
const countAllRoles = async (req, res) => {
    try {
        //Contamos todos los roles
        const role = await RolesModel.count()
        //Enviamos los datos
        handleHttpCount(res, 'Se han encontrado los datos.', 201, role)
    } catch (error) {
        //Obtener y devolver el error
        handleHttpError(res, '', 403, error)
    }
}

module.exports = {
    createRol,
    Roles,
    Rol,
    updateRol,
    deleteRol,
    disableRol,
    enableRol,
    rolesAll,
    countAllRoles,
}
