const GUIDsTenant = require('../../guids_tenant')

module.exports = {
    async up(queryInterface, Sequelize) {
        await queryInterface.bulkInsert(
            'sports',
            [
                {
                    guid: GUIDsTenant.futbolGuid,
                    name: 'Futbol',
                    description: 'Futbol',
                    icon: 'futbol',
                    is_active: 1,
                    created_at: new Date(),
                },
                {
                    guid: GUIDsTenant.baloncestoGuid,
                    name: 'Baloncesto',
                    description: 'Baloncesto',
                    icon: 'baloncesto',
                    is_active: 1,
                    created_at: new Date(),
                },
                {
                    guid: GUIDsTenant.voleibolGuid,
                    name: 'Voleibol',
                    description: 'Voleibol',
                    icon: 'voleibol',
                    is_active: 1,
                    created_at: new Date(),
                },
                {
                    guid: GUIDsTenant.beisbolGuid,
                    name: 'Beisbol',
                    description: 'Beisbol',
                    icon: 'beisbol',
                    is_active: 1,
                    created_at: new Date(),
                },
                {
                    guid: GUIDsTenant.softbolGuid,
                    name: 'Softbol',
                    description: 'Softbol',
                    icon: 'softbol',
                    is_active: 1,
                    created_at: new Date(),
                },
                {
                    guid: GUIDsTenant.futbolSalaGuid,
                    name: 'Futbol Sala',
                    description: 'Futbol Sala',
                    icon: 'futbol_sala',
                    is_active: 1,
                    created_at: new Date(),
                },
                {
                    guid: GUIDsTenant.futbolPlayaGuid,
                    name: 'Futbol Playa',
                    description: 'Futbol Playa',
                    icon: 'futbol_playa',
                    is_active: 1,
                    created_at: new Date(),
                },
                {
                    guid: GUIDsTenant.futbolAmericanoGuid,
                    name: 'Futbol Americano',
                    description: 'Futbol Americano',
                    icon: 'futbol_americano',
                    is_active: 1,
                    created_at: new Date(),
                },
                {
                    guid: GUIDsTenant.futbolRapidoGuid,
                    name: 'Futbol Rapido',
                    description: 'Futbol Rapido',
                    icon: 'futbol_rapido',
                    is_active: 1,
                    created_at: new Date(),
                },
                {
                    guid: GUIDsTenant.futbol7Guid,
                    name: 'Futbol 7',
                    description: 'Futbol 7',
                    icon: 'futbol_7',
                    is_active: 1,
                    created_at: new Date(),
                },
                {
                    guid: GUIDsTenant.tenisGuid,
                    name: 'Tenis',
                    description: 'Tenis',
                    icon: 'tenis',
                    is_active: 1,
                    created_at: new Date(),
                },
            ],
            {}
        )
    },

    async down(queryInterface, Sequelize) {
        await queryInterface.bulkDelete('sports', null, {})
    },
}
