module.exports = {
    async up(queryInterface, Sequelize) {
        // entidad para los cupones de descuento
        await queryInterface.createTable('types_coupons', {
            // identificador del registro
            id: {
                type: Sequelize.INTEGER,
                primaryKey: true,
                autoIncrement: true,
                allowNull: false,
            },
            // guid del registro
            guid: {
                type: Sequelize.UUID,
                defaultValue: Sequelize.UUIDV4, // Genera un UUID por defecto
                unique: true,
                allowNull: false,
            },
            // nombre del tipo de cupon
            name: {
                type: Sequelize.STRING(255),
                unique: true,
            },
            // descripcion del tipo de cupon
            description: {
                type: Sequelize.TEXT,
            },
            // estado del registro
            is_active: {
                type: Sequelize.BOOLEAN,
                defaultValue: 1, // valor por defecto
            },
            // usuario que creo el registro
            created_by: {
                type: Sequelize.UUID,
            },
            // fecha de creacion
            created_at: {
                type: Sequelize.DATE,
                defaultValue: Sequelize.literal('CURRENT_TIMESTAMP'),
            },
            // usuario que actualizo el registro
            updated_by: {
                type: Sequelize.UUID,
            },
            // fecha de actualizacion
            updated_at: {
                type: Sequelize.DATE,
            },
            // usuario que elimino el registro
            deleted_by: {
                type: Sequelize.UUID,
            },
            // fecha de eliminacion
            deleted_at: {
                type: Sequelize.DATE,
            },
        })

        // Agregar índices después de crear la tabla
        await queryInterface.addIndex('types_coupons', ['guid'], {
            unique: true,
            name: 'uniq_types_coupons_guid',
        })
    },

    async down(queryInterface, Sequelize) {
        await queryInterface.removeIndex(
            'types_coupons',
            'uniq_types_coupons_guid'
        )
        await queryInterface.dropTable('types_coupons')
    },
}
