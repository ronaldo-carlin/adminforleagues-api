// Importar Sequelize
const { DataTypes } = require('sequelize')

// Definir la función factory para el modelo PlayersTenantModel
const PlayersTenantModelFactory = (sequelize) => {
    const PlayersTenantModel = sequelize.define(
        'players',
        {
            id: {
                type: DataTypes.INTEGER,
                primaryKey: true,
                autoIncrement: true,
            },
            guid: {
                type: DataTypes.UUID,
                primaryKey: true,
                defaultValue: DataTypes.UUIDV4,
                unique: true,
            },
            team_guid: {
                type: DataTypes.TEXT,
            },
            first_name: {
                type: DataTypes.TEXT,
            },
            last_name: {
                type: DataTypes.TEXT,
            },
            age: {
                type: DataTypes.INTEGER,
            },
            birthdate: {
                type: DataTypes.DATE,
            },
            dorsal_number: {
                type: DataTypes.INTEGER,
            },
            position: {
                type: DataTypes.TEXT,
            },
            height: {
                type: DataTypes.TEXT,
            },
            weight: {
                type: DataTypes.TEXT,
            },
            skilled_foot: {
                type: DataTypes.TEXT,
            },
            personality_traits: {
                type: DataTypes.TEXT,
            },
            image_path: {
                type: DataTypes.TEXT,
            },
            phone_number: {
                type: DataTypes.TEXT,
            },
            country: {
                type: DataTypes.TEXT,
            },
            city: {
                type: DataTypes.TEXT,
            },
            state: {
                type: DataTypes.TEXT,
            },
            nacionality: {
                type: DataTypes.TEXT,
            },
            facebook: {
                type: DataTypes.TEXT,
            },
            instagram: {
                type: DataTypes.TEXT,
            },
            is_active: {
                type: DataTypes.INTEGER,
                defaultValue: 1,
            },
            created_by: {
                type: DataTypes.UUID,
            },
            updated_by: {
                type: DataTypes.UUID,
            },
            deleted_by: {
                type: DataTypes.UUID,
            },
            created_at: {
                type: DataTypes.DATE,
            },
            updated_at: {
                type: DataTypes.DATE,
            },
            deleted_at: {
                type: DataTypes.DATE,
            },
        },
        {
            tableName: 'players',
            timestamps: false,
            defaultScope: {
                attributes: {
                    exclude: [
                        'created_by',
                        'updated_by',
                        'deleted_by',
                        'created_at',
                        'updated_at',
                        'deleted_at',
                    ],
                },
            },
        }
    )

    return PlayersTenantModel
}

module.exports = PlayersTenantModelFactory
