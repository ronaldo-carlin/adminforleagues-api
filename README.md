# Administrador de Ligas Multi Deportes - Backend

Bienvenido al proyecto del backend del Administrador de Ligas Multi Deportes, una API multitenant diseñada para gestionar ligas y torneos de diferentes deportes.

## Descripción del Proyecto

Este proyecto está diseñado para proporcionar una API que permita a los administradores de ligas gestionar equipos, jugadores, partidos y resultados en múltiples deportes. La API ofrece funcionalidades avanzadas para la administración de ligas, incluyendo la creación y configuración de ligas, el seguimiento de estadísticas y la generación de informes.

## Características Principales

-   Gestión de múltiples ligas y deportes en una sola plataforma.
-   Creación y configuración de equipos, jugadores y partidos.
-   Seguimiento y visualización de estadísticas en tiempo real.
-   Generación de informes detallados de rendimiento.
-   Seguridad multitenant para separar y proteger los datos de diferentes ligas.
-   Autenticación y autorización robustas con JSON Web Tokens (JWT).

## Tecnologías Utilizadas

-   Node.js
-   Express.js
-   MySQL
-   Sequelize
-   Swagger (swagger-jsdoc, swagger-ui-express)
-   Redis
-   Socket.IO para actualizaciones en tiempo real
-   Entre otras bibliotecas y paquetes detallados en el archivo package.json.

## Requisitos Previos

Antes de comenzar, asegúrate de tener instalado lo siguiente:

-   Node.js y npm
-   Dependencias del proyecto (se pueden instalar ejecutando `npm install`)

## Configuración

1. Clona este repositorio en tu máquina local.
2. Ejecuta `npm install` para instalar las dependencias del proyecto.
3. Configura las variables de entorno necesarias, como la conexión a la base de datos MySQL y las claves JWT, en un archivo `.env` en la raíz del proyecto.
4. Realiza las migraciones de la base de datos:
    - Migraciones generales: `npm run migrate:general`
    - Migraciones de tenants: `npm run migrate:tenant`
5. Realiza Población de datos de ejemplo:
    - Datos generales: `npm run bd:demo_general`
    - Datos de tenants: `npm run bd:demo_tenant`

## Uso

1. Ejecuta la aplicación utilizando `npm start` o `npm run dev` en modo de desarrollo.
2. Accede a la API a través de la URL proporcionada en tu navegador o aplicación cliente.
3. Utiliza la interfaz de Swagger para explorar y probar los endpoints de la API en `http://localhost:3013/docs`.

## Documentación

Para obtener más información sobre cómo utilizar la API y las rutas disponibles, consulta la documentación en [http://localhost:3013/docs].

## Contribución

Si deseas contribuir a este proyecto, ¡te damos la bienvenida! Por favor, sigue estas pautas para contribuir:

1. Haz un fork del repositorio.
2. Crea una nueva rama (`git checkout -b feature/nueva-caracteristica`).
3. Realiza tus cambios y commitea (`git commit -am 'Añadir nueva característica'`).
4. Envía tus cambios (`git push origin feature/nueva-caracteristica`).
5. Crea un nuevo Pull Request.

## Autor

-   Alex Ronaldo San German Carlin

## Licencia

Este proyecto está licenciado bajo la Licencia ISC. Consulta el archivo LICENSE para obtener más información.

---

Si tienes alguna pregunta o necesitas ayuda, no dudes en ponerte en contacto con nosotros.

¡Disfruta de tu proyecto de Administrador de Ligas Multi Deportes!
