module.exports = {
    async up(queryInterface, Sequelize) {
        // entidad para los patrocinadores
        await queryInterface.createTable('sponsors', {
            // id del registro
            id: {
                type: Sequelize.INTEGER,
                primaryKey: true,
                autoIncrement: true,
                allowNull: false,
            },
            guid: {
                type: Sequelize.UUID,
                primaryKey: true,
                defaultValue: Sequelize.UUIDV4,
                unique: true,
            },
            // torneo al que pertenece el patrocinador
            competition_guid: {
                type: Sequelize.UUID,
            },
            // equipo al que pertenece el patrocinador
            team_guid: {
                type: Sequelize.UUID,
            },
            // nombre del patrocinador
            name: {
                type: Sequelize.TEXT,
            },
            // logo del patrocinador
            logo_path: {
                type: Sequelize.TEXT,
            },
            // fecha de inicio del patrocinio
            start_date: {
                type: Sequelize.DATE,
            },
            // fecha de finalizacion del patrocinio
            end_date: {
                type: Sequelize.DATE,
            },
            // tipo de patrocinio
            format: {
                type: Sequelize.TEXT,
            },
            // pagina web del patrocinador
            site_web: {
                type: Sequelize.TEXT,
            },
            // correo electronico del patrocinador
            phone: {
                type: Sequelize.TEXT,
            },
            // descripcion del patrocinador
            description: {
                type: Sequelize.TEXT,
            },
            // estado del registro
            is_active: {
                type: Sequelize.BOOLEAN,
                defaultValue: 1, // valor por defecto
            },
            // usuario que creo el registro
            created_by: {
                type: Sequelize.UUID,
            },
            // fecha de creacion
            created_at: {
                type: Sequelize.DATE,
                defaultValue: Sequelize.literal('CURRENT_TIMESTAMP'),
            },
            // usuario que actualizo el registro
            updated_by: {
                type: Sequelize.UUID,
            },
            // fecha de actualizacion
            updated_at: {
                type: Sequelize.DATE,
            },
            // usuario que elimino el registro
            deleted_by: {
                type: Sequelize.UUID,
            },
            // fecha de eliminacion
            deleted_at: {
                type: Sequelize.DATE,
            },
        })

        // indices
        await queryInterface.addIndex('sponsors', ['guid'], {
            unique: true,
            name: 'unique_sponsors_guid',
        })

        await queryInterface.addIndex('sponsors', ['competition_guid'], {
            name: 'idx_competition_guid',
        })

        await queryInterface.addIndex('sponsors', ['team_guid'], {
            name: 'idx_team_guid',
        })

        // llaves foraneas
        await queryInterface.addConstraint('sponsors', {
            fields: ['competition_guid'],
            type: 'foreign key',
            name: 'fk_sponsors_competition',
            references: {
                table: 'competitions',
                field: 'guid',
            },
            onDelete: 'CASCADE',
            onUpdate: 'CASCADE',
        })

        await queryInterface.addConstraint('sponsors', {
            fields: ['team_guid'],
            type: 'foreign key',
            name: 'fk_sponsors_team',
            references: {
                table: 'teams',
                field: 'guid',
            },
            onDelete: 'CASCADE',
            onUpdate: 'CASCADE',
        })
    },

    async down(queryInterface, Sequelize) {
        await queryInterface.removeConstraint('sponsors', 'fk_sponsors_team')
        await queryInterface.removeConstraint(
            'sponsors',
            'fk_sponsors_competition'
        )
        await queryInterface.removeIndex('sponsors', 'unique_sponsors_guid')
        await queryInterface.removeIndex('sponsors', 'idx_competition_guid')
        await queryInterface.removeIndex('sponsors', 'idx_team_guid')
        await queryInterface.dropTable('sponsors')
    },
}
