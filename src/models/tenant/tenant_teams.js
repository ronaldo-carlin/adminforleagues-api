// Importar Sequelize
const { DataTypes } = require('sequelize')
const CompetitionsTenantModelFactory = require('./tenant_competitions')
const AuthTenantModelFactory = require('./tenant_auth')

// Definir la función factory para el modelo TeamsTenantModel
const TeamsTenantModelFactory = (sequelize) => {
    const CompetitionsTenantModel = CompetitionsTenantModelFactory(sequelize)
    const AuthTenantModel = AuthTenantModelFactory(sequelize)

    const TeamsTenantModel = sequelize.define(
        'teams',
        {
            id: {
                type: DataTypes.INTEGER,
                primaryKey: true,
                autoIncrement: true,
            },
            guid: {
                type: DataTypes.TEXT,
                primaryKey: true,
                defaultValue: DataTypes.UUIDV4,
                unique: true,
            },
            competition_guid: {
                type: DataTypes.TEXT,
            },
            name: {
                type: DataTypes.TEXT,
            },
            group: {
                type: DataTypes.TEXT,
            },
            image_path: {
                type: DataTypes.TEXT,
            },
            description: {
                type: DataTypes.TEXT,
            },
            is_active: {
                type: DataTypes.INTEGER,
            },
            created_by: {
                type: DataTypes.UUID,
            },
            updated_by: {
                type: DataTypes.UUID,
            },
            deleted_by: {
                type: DataTypes.UUID,
            },
            created_at: {
                type: DataTypes.DATE,
            },
            updated_at: {
                type: DataTypes.DATE,
            },
            deleted_at: {
                type: DataTypes.DATE,
            },
        },
        {
            tableName: 'teams',
            timestamps: false,
            defaultScope: {
                attributes: {
                    exclude: [
                        'created_by',
                        'updated_by',
                        'deleted_by',
                        'created_at',
                        'updated_at',
                        'deleted_at',
                    ],
                },
            },
        }
    )

    TeamsTenantModel.belongsTo(CompetitionsTenantModel, {
        foreignKey: 'competition_guid',
        targetKey: 'guid',
        as: 'competition',
    })

    // Definir la relación entre el usuario con el rol de entrenador y el equipo
    TeamsTenantModel.belongsTo(AuthTenantModel, {
        foreignKey: 'created_by',
        targetKey: 'guid',
        as: 'created_by_user',
    })

    return TeamsTenantModel
}

module.exports = TeamsTenantModelFactory
