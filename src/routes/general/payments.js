const express = require('express')
const { matchedData } = require('express-validator')
const router = express.Router()

// Validaciones
const {
    ValidatorCreatePayment,
    ValidatorUpdatePayment,
    ValidatorDeletePayment,
    ValidatorDisablePayment,
    ValidatorEnablePayment,
} = require('../../validators/general/payments')

// Importar controladores
const {
    CreatePayment,
    Payments,
    Payment,
    UpdatePayment,
    DeletePayment,
    DisablePayment,
    EnablePayment,
    PaymentsAll,
    PaymentsCount,
} = require('../../controllers/general/payments')

// Middleware de autenticación
const authMiddleware = require('../../middleware/session')

// Rutas protegidas que requieren autenticación
/**
 * @swagger
 * tags:
 *   name: Payments
 *   description: API para la gestión de pagos
 */

// Ruta para crear un nuevo pago
/**
 * @swagger
 * /payments/CreatePayment:
 *   post:
 *     summary: Crear un nuevo pago
 *     description: Esta ruta permite crear un nuevo pago con los detalles especificados.
 *     tags: [Payments]
 *     requestBody:
 *       required: true
 *       content:
 *         application/json:
 *           schema:
 *             type: object
 *             properties:
 *               user_guid:
 *                 type: string
 *                 example: 495db854-849b-41cd-b6b9-9c84824c92ba
 *               email:
 *                 type: string
 *                 example: user@example.com
 *               plan_guid:
 *                 type: string
 *                 example: 495db854-849b-41cd-b6b9-9c84824c92ba
 *               amount:
 *                 type: number
 *                 example: 10.00
 *               method:
 *                 type: string
 *                 example: credit_card
 *               transaction:
 *                 type: string
 *                 example: txn_1234567890
 *               reference:
 *                 type: string
 *                 example: ref_1234567890
 *               concept:
 *                 type: string
 *                 example: Subscription payment
 *               card:
 *                 type: string
 *                 example: Visa ending in 1234
 *               bank:
 *                 type: string
 *                 example: Bank of Example
 *               message:
 *                 type: string
 *                 example: Payment successful
 *     responses:
 *       201:
 *         description: Pago creado exitosamente
 *         content:
 *           application/json:
 *             schema:
 *               $ref: '#/components/responses/success'
 *       400:
 *         description: Error en los datos enviados
 *         content:
 *           application/json:
 *             schema:
 *               $ref: '#/components/responses/BadRequest'
 *       403:
 *         description: Error de autenticación
 *         content:
 *           application/json:
 *             schema:
 *               $ref: '#/components/responses/UnauthorizedError'
 *       409:
 *         description: El pago ya existe
 *         content:
 *           application/json:
 *             schema:
 *               $ref: '#/components/responses/ConflictError'
 *       500:
 *         description: Error en el servidor
 *         content:
 *           application/json:
 *             schema:
 *               $ref: '#/components/responses/InternalServerError'
 *     security:
 *        - bearerAuth: []
 */
router.post(
    '/CreatePayment',
    authMiddleware,
    ValidatorCreatePayment,
    CreatePayment
)

// Ruta para obtener todos los pagos
/**
 * @swagger
 * /payments/Payments:
 *   get:
 *     summary: Obtener todos los pagos
 *     tags: [Payments]
 *     responses:
 *       200:
 *         description: Pagos obtenidos exitosamente
 *         content:
 *           application/json:
 *             schema:
 *               $ref: '#/components/responses/success'
 *       403:
 *         description: Error de autenticación
 *         content:
 *           application/json:
 *             schema:
 *               $ref: '#/components/responses/UnauthorizedError'
 *       500:
 *         description: Error en el servidor
 *         content:
 *           application/json:
 *             schema:
 *               $ref: '#/components/responses/InternalServerError'
 *     security:
 *        - bearerAuth: []
 */
router.get('/Payments', authMiddleware, Payments)

// Ruta para obtener un pago por ID
/**
 * @swagger
 * /payments/Payment:
 *   get:
 *     summary: Obtener un pago por ID
 *     tags: [Payments]
 *     parameters:
 *       - in: path
 *         name: guid
 *         schema:
 *           type: string
 *           format: uuid
 *         required: true
 *         description: El GUID del pago
 *     responses:
 *       200:
 *         description: Pago obtenido exitosamente
 *         content:
 *           application/json:
 *             schema:
 *               $ref: '#/components/responses/success'
 *       403:
 *         description: Error de autenticación
 *         content:
 *           application/json:
 *             schema:
 *               $ref: '#/components/responses/UnauthorizedError'
 *       404:
 *        description: Pago no encontrado
 *        content:
 *          application/json:
 *            schema:
 *              $ref: '#/components/responses/NotFoundError'
 *       500:
 *         description: Error en el servidor
 *         content:
 *           application/json:
 *             schema:
 *               $ref: '#/components/responses/InternalServerError'
 *     security:
 *        - bearerAuth: []
 */
router.get('/Payment/:guid', authMiddleware, Payment)

// Ruta para actualizar un pago
/**
 * @swagger
 * /payments/UpdatePayment:
 *   patch:
 *     summary: Actualizar un pago
 *     tags: [Payments]
 *     requestBody:
 *       required: true
 *       content:
 *         application/json:
 *           schema:
 *             type: object
 *             properties:
 *               guid:
 *                 type: string
 *                 example: 495db854-849b-41cd-b6b9-9c84824c92ba
 *               amount:
 *                 type: number
 *                 example: 20.00
 *               method:
 *                 type: string
 *                 example: credit_card
 *               transaction:
 *                 type: string
 *                 example: txn_updated_1234567890
 *               reference:
 *                 type: string
 *                 example: ref_updated_1234567890
 *               status:
 *                 type: string
 *                 example: completed
 *     responses:
 *       200:
 *         description: Pago actualizado exitosamente
 *         content:
 *           application/json:
 *             schema:
 *               $ref: '#/components/responses/success'
 *       400:
 *         description: Error en los datos enviados
 *         content:
 *           application/json:
 *             schema:
 *               $ref: '#/components/responses/BadRequest'
 *       403:
 *         description: Error de autenticación
 *         content:
 *           application/json:
 *             schema:
 *               $ref: '#/components/responses/UnauthorizedError'
 *       404:
 *        description: Pago no encontrado
 *        content:
 *          application/json:
 *            schema:
 *              $ref: '#/components/responses/NotFoundError'
 *       500:
 *         description: Error en el servidor
 *         content:
 *           application/json:
 *             schema:
 *               $ref: '#/components/responses/InternalServerError'
 *     security:
 *        - bearerAuth: []
 */
router.patch(
    '/UpdatePayment/:guid',
    authMiddleware,
    ValidatorUpdatePayment,
    UpdatePayment
)

// Ruta para eliminar un pago
/**
 * @swagger
 * /payments/DeletePayment:
 *   delete:
 *     summary: Eliminar un pago
 *     tags: [Payments]
 *     parameters:
 *       - in: path
 *         name: guid
 *         schema:
 *           type: string
 *           format: uuid
 *         required: true
 *         description: El GUID del pago
 *     responses:
 *       200:
 *         description: Pago eliminado exitosamente
 *         content:
 *           application/json:
 *             schema:
 *               $ref: '#/components/responses/success'
 *       400:
 *         description: Error en los datos enviados
 *         content:
 *           application/json:
 *             schema:
 *               $ref: '#/components/responses/BadRequest'
 *       403:
 *         description: Error de autenticación
 *         content:
 *           application/json:
 *             schema:
 *               $ref: '#/components/responses/UnauthorizedError'
 *       404:
 *        description: Pago no encontrado
 *        content:
 *          application/json:
 *            schema:
 *              $ref: '#/components/responses/NotFoundError'
 *       500:
 *         description: Error en el servidor
 *         content:
 *           application/json:
 *             schema:
 *               $ref: '#/components/responses/InternalServerError'
 *     security:
 *        - bearerAuth: []
 */
router.delete(
    '/DeletePayment/:guid',
    authMiddleware,
    ValidatorDeletePayment,
    DeletePayment
)

// Ruta para deshabilitar un pago
/**
 * @swagger
 * /payments/DisablePayment:
 *   patch:
 *     summary: Deshabilitar un pago
 *     tags: [Payments]
 *     requestBody:
 *       required: true
 *       content:
 *         application/json:
 *           schema:
 *             type: object
 *             properties:
 *               guid:
 *                 type: uuid
 *                 example: 495db854-849b-41cd-b6b9-9c84824c92ba
 *     responses:
 *       200:
 *         description: Pago deshabilitado exitosamente
 *         content:
 *           application/json:
 *             schema:
 *               $ref: '#/components/responses/success'
 *       400:
 *         description: Error en los datos enviados
 *         content:
 *           application/json:
 *             schema:
 *               $ref: '#/components/responses/BadRequest'
 *       403:
 *         description: Error de autenticación
 *         content:
 *           application/json:
 *             schema:
 *               $ref: '#/components/responses/UnauthorizedError'
 *       404:
 *        description: Pago no encontrado
 *        content:
 *          application/json:
 *            schema:
 *              $ref: '#/components/responses/NotFoundError'
 *       500:
 *         description: Error en el servidor
 *         content:
 *           application/json:
 *             schema:
 *               $ref: '#/components/responses/InternalServerError'
 *     security:
 *        - bearerAuth: []
 */
router.patch(
    '/DisablePayment/:guid',
    authMiddleware,
    ValidatorDisablePayment,
    DisablePayment
)

// Ruta para habilitar un pago
/**
 * @swagger
 * /payments/EnablePayment:
 *   patch:
 *     summary: Habilitar un pago
 *     tags: [Payments]
 *     parameters:
 *       - in: path
 *         name: guid
 *         schema:
 *           type: string
 *           format: uuid
 *         required: true
 *         description: El GUID del pago
 *     responses:
 *       200:
 *         description: Pago habilitado exitosamente
 *         content:
 *           application/json:
 *             schema:
 *               $ref: '#/components/responses/success'
 *       400:
 *         description: Error en los datos enviados
 *         content:
 *           application/json:
 *             schema:
 *               $ref: '#/components/responses/BadRequest'
 *       403:
 *         description: Error de autenticación
 *         content:
 *           application/json:
 *             schema:
 *               $ref: '#/components/responses/UnauthorizedError'
 *       404:
 *        description: Pago no encontrado
 *        content:
 *          application/json:
 *            schema:
 *              $ref: '#/components/responses/NotFoundError'
 *       500:
 *         description: Error en el servidor
 *         content:
 *           application/json:
 *             schema:
 *               $ref: '#/components/responses/InternalServerError'
 *     security:
 *        - bearerAuth: []
 */
router.patch(
    '/EnablePayment/:guid',
    authMiddleware,
    ValidatorEnablePayment,
    EnablePayment
)

// Ruta para obtener todos los pagos
/**
 * @swagger
 * /payments/PaymentsAll:
 *   get:
 *     summary: Obtener todos los pagos
 *     tags: [Payments]
 *     responses:
 *       200:
 *         description: Pagos obtenidos exitosamente
 *         content:
 *           application/json:
 *             schema:
 *               $ref: '#/components/responses/success'
 *       403:
 *         description: Error de autenticación
 *         content:
 *           application/json:
 *             schema:
 *               $ref: '#/components/responses/UnauthorizedError'
 *       500:
 *         description: Error en el servidor
 *         content:
 *           application/json:
 *             schema:
 *               $ref: '#/components/responses/InternalServerError'
 *     security:
 *        - bearerAuth: []
 */
router.get('/PaymentsAll', authMiddleware, PaymentsAll)

// Ruta para obtener la cantidad de pagos
/**
 * @swagger
 * /payments/PaymentsCount:
 *   get:
 *     summary: Obtener la cantidad de pagos
 *     tags: [Payments]
 *     responses:
 *       200:
 *         description: Cantidad de pagos obtenida exitosamente
 *         content:
 *           application/json:
 *             schema:
 *               $ref: '#/components/responses/success'
 *       403:
 *         description: Error de autenticación
 *         content:
 *           application/json:
 *             schema:
 *               $ref: '#/components/responses/UnauthorizedError'
 *       500:
 *         description: Error en el servidor
 *         content:
 *           application/json:
 *             schema:
 *               $ref: '#/components/responses/InternalServerError'
 *     security:
 *        - bearerAuth: []
 */
router.get('/PaymentsCount', authMiddleware, PaymentsCount)

module.exports = router
