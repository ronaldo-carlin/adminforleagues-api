const { validationResult } = require('express-validator')
const { handleHttpError } = require('./handleError')

const validateResults = (req, res, next) => {
    const errors = validationResult(req)
    if (!errors.isEmpty()) {
        console.log('Validation errors:', errors.array()) // Log de depuración
        // Utiliza handleHttpError para enviar los errores de validación
        return handleHttpError(res, 'Validation failed', 400, errors.array())
    }
    next()
}

module.exports = validateResults
