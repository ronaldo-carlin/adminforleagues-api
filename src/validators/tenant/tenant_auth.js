const { check, validationResult } = require('express-validator')
const validateResults = require('../../utils/handleValidators')
const { getSequelize, getUserModel } = require('../../helpers/sequelizeHelper')
const User = require('../../models/general/auth')

const validatorLogin = () => {
    return [
        check('user').exists().notEmpty(),
        check('password').exists().notEmpty().isLength({ min: 6, max: 20 }),
        (req, res, next) => validateResults(req, res, next),
    ]
}

const validatorRegisterUser = () => {
    return [
        async (req, res, next) => {
            const sequelize = getSequelize(req)
            const AuthTenantModel = getUserModel(sequelize)

            await check('role_guid')
                .isUUID()
                .exists()
                .notEmpty()
                .withMessage('El GUID del rol es requerido.')
                .run(req)
            await check('first_name')
                .isString()
                .exists()
                .notEmpty()
                .isLength({ min: 1, max: 255 })
                .withMessage('El nombre es requerido')
                .run(req)
            await check('last_name')
                .isString()
                .exists()
                .isLength({ min: 1, max: 255 })
                .withMessage('El apellido es requerido')
                .run(req)
            await check('username')
                .isString()
                .exists()
                .notEmpty()
                .isLength({ min: 1, max: 255 })
                .custom(async (value) => {
                    const user = await AuthTenantModel.findOne({
                        where: { username: value },
                    })
                    if (user) {
                        throw new Error('El nombre de usuario ya está en uso')
                    }
                })
                .run(req)
            await check('email')
                .isEmail()
                .exists()
                .notEmpty()
                .custom(async (value) => {
                    const user = await AuthTenantModel.findOne({
                        where: { email: value },
                    })
                    if (user) {
                        throw new Error('El correo ya está en uso')
                    }
                })
                .run(req)
            await check('password')
                .exists()
                .notEmpty()
                .isLength({ min: 6, max: 100 })
                .run(req)
            await check('phone')
                .isNumeric()
                .exists()
                .notEmpty()
                .isLength({ min: 6, max: 20 })
                .custom(async (value) => {
                    const user = await AuthTenantModel.findOne({
                        where: { phone: value },
                    })
                    if (user) {
                        throw new Error('El teléfono ya está en uso')
                    }
                })
                .run(req)
            await check('country')
                .isString()
                .exists()
                .withMessage('El país es requerido')
                .run(req)
            await check('city')
                .isString()
                .exists()
                .withMessage('La ciudad es requerida')
                .run(req)
            await check('state')
                .isString()
                .exists()
                .withMessage('El estado es requerido')
                .run(req)

            const errors = validationResult(req)
            if (!errors.isEmpty()) {
                return res.status(400).json({ errors: errors.array() })
            }

            next()
        },
    ]
}

const validatorGoogle = () => {
    return [
        async (req, res, next) => {
            await check('token')
                .exists()
                .notEmpty()
                .withMessage('El token es requerido.')
                .run(req)

            const errors = validationResult(req)
            if (!errors.isEmpty()) {
                return res.status(400).json({ errors: errors.array() })
            }

            console.log('validatorGoogle middleware ejecutado.')
            next()
        },
    ]
}

module.exports = {
    validatorLogin,
    validatorRegisterUser,
    validatorGoogle,
}
