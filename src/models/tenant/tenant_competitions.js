// Importar Sequelize
const { DataTypes } = require('sequelize')

// Definir la función factory para el modelo CompetitionsTenantModel
const CompetitionsTenantModelFactory = (sequelize) => {
    const CompetitionsTenantModel = sequelize.define(
        'competitions',
        {
            id: {
                type: DataTypes.INTEGER,
                primaryKey: true,
                autoIncrement: true,
            },
            guid: {
                type: DataTypes.UUID,
                primaryKey: true,
                defaultValue: DataTypes.UUIDV4,
                unique: true,
            },
            category_guid: {
                type: DataTypes.UUID,
            },
            season_guid: {
                type: DataTypes.UUID,
            },
            sport_guid: {
                type: DataTypes.UUID,
            },
            type_competition_guid: {
                type: DataTypes.UUID,
            },
            name: {
                type: DataTypes.TEXT,
            },
            description: {
                type: DataTypes.TEXT,
            },
            logo_path: {
                type: DataTypes.TEXT,
            },
            teams: {
                type: DataTypes.INTEGER,
            },
            players: {
                type: DataTypes.INTEGER,
            },
            players_field: {
                type: DataTypes.INTEGER,
            },
            start_date: {
                type: DataTypes.DATE,
            },
            end_date: {
                type: DataTypes.DATE,
            },
            site_web: {
                type: DataTypes.TEXT,
            },
            is_active: {
                type: DataTypes.INTEGER,
            },
            created_by: {
                type: DataTypes.UUID,
            },
            updated_by: {
                type: DataTypes.UUID,
            },
            deleted_by: {
                type: DataTypes.UUID,
            },
            created_at: {
                type: DataTypes.DATE,
            },
            updated_at: {
                type: DataTypes.DATE,
            },
            deleted_at: {
                type: DataTypes.DATE,
            },
        },
        {
            tableName: 'competitions',
            timestamps: false,
            defaultScope: {
                attributes: {
                    exclude: [
                        'created_by',
                        'updated_by',
                        'deleted_by',
                        'created_at',
                        'updated_at',
                        'deleted_at',
                    ],
                },
            },
        }
    )

    return CompetitionsTenantModel
}

module.exports = CompetitionsTenantModelFactory
