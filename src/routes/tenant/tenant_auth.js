const express = require('express')
const router = express.Router()

// Controladores de autenticación y middleware
const {
    validatorLogin,
    validatorRegisterUser,
    validatorGoogle,
} = require('../../validators/tenant/tenant_auth')

const {
    Register,
    LoginUser,
    ResetPassword,
    CheckToken,
    AuthenticateWithGoogleToken,
    RenewAccessToken,
} = require('../../controllers/tenant/tenant_auth')

const authMiddleware = require('../../middleware/session')

/**
 * @swagger
 * tags:
 *   name: Tenant_Auth
 *   description: API para autenticación de usuarios
 */

/* Rutas para la autenticación */

/**
 * @swagger
 * /tenant_auth/Register:
 *   post:
 *     summary: Crear un nuevo usuario
 *     tags: [Tenant_Auth]
 *     requestBody:
 *       required: true
 *       content:
 *         application/json:
 *           schema:
 *             type: object
 *             properties:
 *               first_name:
 *                type: string
 *                example: prueba
 *               last_name:
 *                type: string
 *                example: prueba
 *               role_guid:
 *                type: uuid
 *                example: 1881e61d-4be8-4e3b-9482-9643d5c05327
 *               username:
 *                type: string
 *                example: prueba
 *               email:
 *                type: string
 *                example: user@example.com
 *               password:
 *                type: string
 *                example: Prueba321+.
 *               phone:
 *                type: string
 *                example: 1234567890
 *               country:
 *                type: string
 *                example:  mexico
 *               state:
 *                type: string
 *                example: veracruz
 *               city:
 *                type: string
 *                example: veracruz
 *     responses:
 *       201:
 *         description: Usuario creado exitosamente
 *         content:
 *           application/json:
 *             schema:
 *               $ref: '#/components/responses/success'
 *       400:
 *         description: Error en los datos enviados
 *         content:
 *           application/json:
 *             schema:
 *               $ref: '#/components/responses/BadRequest'
 *       403:
 *         description: Error de autenticación
 *         content:
 *           application/json:
 *             schema:
 *               $ref: '#/components/responses/UnauthorizedError'
 *       409:
 *         description: El usuario ya existe
 *         content:
 *           application/json:
 *             schema:
 *               $ref: '#/components/responses/ConflictError'
 *       500:
 *         description: Error en el servidor
 *         content:
 *           application/json:
 *             schema:
 *               $ref: '#/components/responses/InternalServerError'
 */
router.post('/Register', validatorRegisterUser(), Register)

/* Rutas para el inicio de sesión */
/**
 * @swagger
 * /tenant_auth/login:
 *   post:
 *     summary: Iniciar sesión
 *     tags: [Tenant_Auth]
 *     requestBody:
 *       required: true
 *       content:
 *         application/json:
 *           schema:
 *             type: object
 *             properties:
 *               user:
 *                type: string
 *                example: admin@adminforleagues.com
 *               password:
 *                type: string
 *                example: Admin+.
 *     responses:
 *       200:
 *         description: Inicio de sesión exitoso
 *       401:
 *         description: Autenticación fallida
 */
router.post('/Login', validatorLogin(), LoginUser)

/* Rutas para la autenticación con Google */
/**
 * @swagger
 * /tenant_auth/google/token:
 *   post:
 *     summary: Autenticar usando el token de Google
 *     tags: [Tenant_Auth]
 *     requestBody:
 *       content:
 *         application/json:
 *           schema:
 *             type: object
 *             properties:
 *               token:
 *                 type: string
 *                 description: Token de Google
 *                 example: "your-google-token"
 *     responses:
 *       200:
 *         description: Autenticación exitosa
 *       401:
 *         description: Autenticación fallida
 */
router.post('/Google', validatorGoogle(), AuthenticateWithGoogleToken)

/* Rutas para el restablecimiento de contraseña */
/**
 * @swagger
 * /tenant_auth/reset-password:
 *   post:
 *     summary: Resetear contraseña
 *     tags: [Tenant_Auth]
 *     requestBody:
 *       required: true
 *       content:
 *         application/json:
 *           schema:
 *             $ref: '#/components/schemas/ResetPassword'
 *     responses:
 *       200:
 *         description: Contraseña reseteada exitosamente
 *       400:
 *         description: Error en los datos enviados
 */
router.post('/ResetPassword', ResetPassword)

/* Rutas para la renovación de tokens */
/**
 * @swagger
 * /tenant_auth/renew-access-token:
 *   post:
 *     summary: Renovar el token de acceso
 *     tags: [Tenant_Auth]
 *     requestBody:
 *       required: true
 *       content:
 *         application/json:
 *           schema:
 *             $ref: '#/components/schemas/RenewAccessToken'
 *     responses:
 *       200:
 *         description: Token renovado exitosamente
 *       401:
 *         description: Autenticación fallida
 */
router.post('/RenewAccessToken', RenewAccessToken)

/* Rutas para la verificación de tokens */
/**
 * @swagger
 * /tenant_auth/CheckToken:
 *   post:
 *     summary: Verificar un token
 *     tags: [Tenant_Auth]
 *     security:
 *       - bearerAuth: []
 *     responses:
 *       200:
 *         description: Token verificado exitosamente
 *       401:
 *         description: Autenticación fallida
 */
router.post('/CheckToken', authMiddleware, CheckToken)

module.exports = router
