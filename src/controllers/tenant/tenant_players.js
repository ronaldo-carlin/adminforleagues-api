const { Op } = require('sequelize')
const { v4: uuidv4 } = require('uuid')
const { matchedData } = require('express-validator')
const { handleHttpSuccess } = require('../../utils/handleSuccess')
const { handleHttpError } = require('../../utils/handleError')
const { handleHttpCount } = require('../../utils/handleCount')
const {
    getSequelize,
    getPlayersModel,
} = require('../../helpers/sequelizeHelper')

/**
 * Este controlador es el encargado de registrar un jugador
 * @param {*} req
 * @param {*} res
 */
const CreatePlayer = async (req, res) => {
    try {
        const sequelize = getSequelize(req)
        const PlayerModel = getPlayersModel(sequelize)
        const data = matchedData(req)
        const created_by = req.user.guid
        const created_at = new Date()
        const guid = await uuidv4()
        data.created_by = created_by
        data.created_at = created_at
        data.guid = guid

        const player = await PlayerModel.create(data)
        return handleHttpSuccess(res, 'Jugador creado.', 201, player)
    } catch (error) {
        console.log('Error al crear el jugador:', error)
        handleHttpError(res, 'Error al crear el jugador.', 403, error)
    }
}

/**
 * Este controlador es el encargado de obtener los jugadores activos
 * @param {*} req
 * @param {*} res
 */
const Players = async (req, res) => {
    try {
        const sequelize = getSequelize(req)
        const PlayerModel = getPlayersModel(sequelize)

        const players = await PlayerModel.findAll({
            where: { is_active: 1 },
        })

        if (!players.length) {
            handleHttpError(res, 'No se encontraron jugadores.', 404)
            return
        }

        handleHttpSuccess(res, 'Jugadores encontrados.', 201, players)
    } catch (error) {
        console.log('Error obteniendo jugadores:', error)
        handleHttpError(res, 'Error obteniendo jugadores.', 403, error)
    }
}

/**
 * Este controlador es el encargado de obtener un jugador por GUID
 * @param {*} req
 * @param {*} res
 */
const Player = async (req, res) => {
    try {
        const sequelize = getSequelize(req)
        const PlayerModel = getPlayersModel(sequelize)
        const { guid } = req.params

        const player = await PlayerModel.findOne({
            where: { guid },
        })

        if (!player) {
            handleHttpError(res, 'El jugador no existe.', 404)
            return
        }

        if (player.is_active == 0) {
            handleHttpError(res, 'El jugador ha sido deshabilitado.', 401)
            return
        }

        handleHttpSuccess(res, 'Jugador encontrado.', 201, player)
    } catch (error) {
        console.log('Error obteniendo jugador:', error)
        handleHttpError(res, 'Error obteniendo jugador.', 403, error)
    }
}

/**
 * Este controlador es el encargado de actualizar un jugador
 * @param {*} req
 * @param {*} res
 */
const UpdatePlayer = async (req, res) => {
    try {
        const sequelize = getSequelize(req)
        const PlayerModel = getPlayersModel(sequelize)
        const { guid } = req.params
        const data = matchedData(req)
        const updated_by = req.user.guid
        const updated_at = new Date()
        data.updated_by = updated_by
        data.updated_at = updated_at

        const [updateCount] = await PlayerModel.update(data, {
            where: { guid },
        })

        if (updateCount === 0) {
            return handleHttpError(res, 'El jugador no existe.', 404)
        }

        const player = await PlayerModel.findOne({
            where: { guid },
        })

        if (!player) {
            handleHttpError(res, 'El jugador no existe.', 404)
            return
        }

        if (player.is_active == 0) {
            handleHttpError(res, 'El jugador ha sido deshabilitado.', 401)
            return
        }

        handleHttpSuccess(res, 'Jugador actualizado.', 201, player)
    } catch (error) {
        console.log('Error actualizando jugador:', error)
        handleHttpError(res, 'Error actualizando jugador.', 403, error)
    }
}

/**
 * Este controlador es el encargado de eliminar un jugador
 * @param {*} req
 * @param {*} res
 */
const DeletePlayer = async (req, res) => {
    try {
        const sequelize = getSequelize(req)
        const PlayerModel = getPlayersModel(sequelize)
        const { guid } = req.params
        const deleted_by = req.user.guid
        const deleted_at = new Date()
        const [updateCount] = await PlayerModel.update(
            {
                is_active: 0,
                deleted_by,
                deleted_at,
            },
            {
                where: { guid },
            }
        )

        if (updateCount === 0) {
            return handleHttpError(res, 'El jugador no existe.', 404)
        }

        handleHttpSuccess(res, 'Jugador eliminado.', 201)
    } catch (error) {
        console.log('Error eliminando jugador:', error)
        handleHttpError(res, 'Error eliminando jugador.', 403, error)
    }
}

/**
 * Este controlador es el encargado de desactivar un jugador
 * @param {*} req
 * @param {*} res
 */
const DisablePlayer = async (req, res) => {
    try {
        const sequelize = getSequelize(req)
        const PlayerModel = getPlayersModel(sequelize)
        const { guid } = req.params
        const updated_by = req.user.guid
        const updated_at = new Date()
        const [updateCount] = await PlayerModel.update(
            {
                is_active: 0,
                updated_by,
                updated_at,
            },
            {
                where: { guid },
            }
        )

        if (updateCount === 0) {
            return handleHttpError(res, 'El jugador no existe.', 404)
        }

        handleHttpSuccess(res, 'Jugador deshabilitado.', 201)
    } catch (error) {
        console.log('Error deshabilitando jugador:', error)
        handleHttpError(res, 'Error deshabilitando jugador.', 403, error)
    }
}

/**
 * Este controlador es el encargado de activar un jugador
 * @param {*} req
 * @param {*} res
 */
const EnablePlayer = async (req, res) => {
    try {
        const sequelize = getSequelize(req)
        const PlayerModel = getPlayersModel(sequelize)
        const { guid } = req.params
        const updated_by = req.user.guid
        const updated_at = new Date()
        const [updateCount] = await PlayerModel.update(
            {
                is_active: 1,
                updated_by,
                updated_at,
            },
            {
                where: { guid },
            }
        )

        if (updateCount === 0) {
            return handleHttpError(res, 'El jugador no existe.', 404)
        }

        handleHttpSuccess(res, 'Jugador habilitado.', 201)
    } catch (error) {
        console.log('Error habilitando jugador:', error)
        handleHttpError(res, 'Error habilitando jugador.', 403, error)
    }
}

/**
 * Este controlador es el encargado de obtener todos los jugadores
 * @param {*} req
 * @param {*} res
 */
const PlayersAll = async (req, res) => {
    try {
        const sequelize = getSequelize(req)
        const PlayerModel = getPlayersModel(sequelize)

        const players = await PlayerModel.findAll()

        if (!players.length) {
            handleHttpError(res, 'No se encontraron jugadores.', 404)
            return
        }

        handleHttpSuccess(res, 'Jugadores encontrados.', 201, players)
    } catch (error) {
        console.log('Error obteniendo jugadores:', error)
        handleHttpError(res, 'Error obteniendo jugadores.', 403, error)
    }
}

/**
 * Este controlador es el encargado de contar todos los jugadores
 * @param {*} req
 * @param {*} res
 */
const PlayersCount = async (req, res) => {
    try {
        const sequelize = getSequelize(req)
        const PlayerModel = getPlayersModel(sequelize)

        const count = await PlayerModel.count()

        handleHttpCount(res, 'Jugadores encontrados.', 201, count)
    } catch (error) {
        console.log('Error contando jugadores:', error)
        handleHttpError(res, 'Error contando jugadores.', 403, error)
    }
}

module.exports = {
    CreatePlayer,
    Players,
    Player,
    UpdatePlayer,
    DeletePlayer,
    DisablePlayer,
    EnablePlayer,
    PlayersAll,
    PlayersCount,
}
