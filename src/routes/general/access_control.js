const express = require('express')
const router = express.Router()

// Validaciones
const {
    ValidatorCreateAccessControl,
    ValidatorAccessControl,
    ValidatorUpdateAccessControl,
    ValidatorDeleteAccessControl,
    ValidatorDisableAccessControl,
    ValidatorEnableAccessControl,
} = require('../../validators/general/access_control')

// Controladores
const {
    CreateAccessControl,
    AccessControls,
    AccessControl,
    UpdateAccessControl,
    DeleteAccessControl,
    DisableAccessControl,
    EnableAccessControl,
    AccessControlsAll,
    CountAccessControls,
} = require('../../controllers/general/access_control')

// Middleware de autenticación
const authMiddleware = require('../../middleware/session')

// Documentación de Swagger
/**
 * @swagger
 * tags:
 *   name: Access_Control
 *   description: API para el Control de acceso
 */

/**
 * @swagger
 * /access_control/CreateAccessControl:
 *   post:
 *     summary: Crea un nuevo control de acceso
 *     description: Esta ruta permite crear un nuevo control de acceso.
 *     tags: [Access_Control]
 *     security:
 *       - bearerAuth: []
 *     requestBody:
 *       required: true
 *       content:
 *         application/json:
 *           schema:
 *             $ref: '#/components/schemas/CreateAccessControl'
 *     responses:
 *       201:
 *         description: Control de acceso creado exitosamente
 *       400:
 *         description: Error en los datos enviados
 *         content:
 *           application/json:
 *             schema:
 *               $ref: '#/components/responses/BadRequest'
 *       403:
 *         description: Error de autenticación
 *         content:
 *           application/json:
 *             schema:
 *               $ref: '#/components/responses/UnauthorizedError'
 *       500:
 *         description: Error en el servidor
 *         content:
 *           application/json:
 *             schema:
 *               $ref: '#/components/responses/InternalServerError'
 */
router.post(
    '/CreateAccessControl',
    authMiddleware,
    ValidatorCreateAccessControl,
    CreateAccessControl
)

/**
 * @swagger
 * /access_control/AccessControls:
 *   get:
 *     summary: Obtiene la lista de controles de acceso
 *     tags: [Access_Control]
 *     security:
 *       - bearerAuth: []
 *     responses:
 *       200:
 *         description: Lista de controles de acceso obtenida exitosamente
 *         content:
 *           application/json:
 *             schema:
 *               type: array
 *               items:
 *                 $ref: '#/components/schemas/AccessControl'
 *       403:
 *         description: Error obteniendo los controles de acceso
 */
router.get('/AccessControls', authMiddleware, AccessControls)

/**
 * @swagger
 * /access_control/AccessControl:
 *   get:
 *     summary: Obtiene un control de acceso por ID
 *     tags: [Access_Control]
 *     security:
 *       - bearerAuth: []
 *     requestBody:
 *       required: true
 *       content:
 *         application/json:
 *           schema:
 *             $ref: '#/components/schemas/AccessControl'
 *     responses:
 *       200:
 *         description: Control de acceso obtenido exitosamente
 *         content:
 *           application/json:
 *             schema:
 *               $ref: '#/components/schemas/AccessControl'
 *       403:
 *         description: Error obteniendo el control de acceso
 */
router.get(
    '/AccessControl/:guid',
    authMiddleware,
    ValidatorAccessControl,
    AccessControl
)

/**
 * @swagger
 * /access_control/UpdateAccessControl:
 *   patch:
 *     summary: Actualiza un control de acceso existente
 *     tags: [Access_Control]
 *     security:
 *       - bearerAuth: []
 *     requestBody:
 *       required: true
 *       content:
 *         application/json:
 *           schema:
 *             $ref: '#/components/schemas/UpdateAccessControl'
 *     responses:
 *       200:
 *         description: Control de acceso actualizado exitosamente
 *       403:
 *         description: Error actualizando el control de acceso
 */
router.patch(
    '/UpdateAccessControl/:guid',
    authMiddleware,
    ValidatorUpdateAccessControl,
    UpdateAccessControl
)

/**
 * @swagger
 * /access_control/DeleteAccessControl:
 *   delete:
 *     summary: Elimina un control de acceso
 *     tags: [Access_Control]
 *     security:
 *       - bearerAuth: []
 *     requestBody:
 *       required: true
 *       content:
 *         application/json:
 *           schema:
 *             $ref: '#/components/schemas/DeleteAccessControl'
 *     responses:
 *       200:
 *         description: Control de acceso eliminado exitosamente
 *       403:
 *         description: Error eliminando el control de acceso
 */
router.delete(
    '/DeleteAccessControl/:guid',
    authMiddleware,
    ValidatorDeleteAccessControl,
    DeleteAccessControl
)

/**
 * @swagger
 * /access_control/DisableAccessControl:
 *   patch:
 *     summary: Deshabilita un control de acceso
 *     tags: [Access_Control]
 *     security:
 *       - bearerAuth: []
 *     requestBody:
 *       required: true
 *       content:
 *         application/json:
 *           schema:
 *             $ref: '#/components/schemas/DisableAccessControl'
 *     responses:
 *       200:
 *         description: Control de acceso deshabilitado exitosamente
 *       403:
 *         description: Error deshabilitando el control de acceso
 */
router.patch(
    '/DisableAccessControl/:guid',
    authMiddleware,
    ValidatorDisableAccessControl,
    DisableAccessControl
)

/**
 * @swagger
 * /access_control/EnableAccessControl:
 *   patch:
 *     summary: Habilita un control de acceso
 *     tags: [Access_Control]
 *     security:
 *       - bearerAuth: []
 *     requestBody:
 *       required: true
 *       content:
 *         application/json:
 *           schema:
 *             $ref: '#/components/schemas/EnableAccessControl'
 *     responses:
 *       200:
 *         description: Control de acceso habilitado exitosamente
 *       403:
 *         description: Error habilitando el control de acceso
 */
router.patch(
    '/EnableAccessControl/:guid',
    authMiddleware,
    ValidatorEnableAccessControl,
    EnableAccessControl
)

/**
 * @swagger
 * /access_control/AccessControlsAll:
 *   get:
 *     summary: obtiene todos los controles de acceso registrados
 *     tags: [Access_Control]
 *     security:
 *       - bearerAuth: []
 *     responses:
 *       200:
 *         description: Lista de controles de acceso obtenida exitosamente
 *       403:
 *         description: Error encontrando los controles de acceso
 */
router.get('/AccessControlsAll', authMiddleware, AccessControlsAll)

/**
 * @swagger
 * /access_control/CountAccessControls:
 *   get:
 *     summary: Cuenta todos los controles de acceso
 *     tags: [Access_Control]
 *     security:
 *       - bearerAuth: []
 *     responses:
 *       200:
 *         description: Conteo de todos los controles de acceso exitoso
 *       403:
 *         description: Error contando todos los controles de acceso
 */
router.get('/CountAccessControls', authMiddleware, CountAccessControls)

module.exports = router
