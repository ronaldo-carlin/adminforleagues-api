const { check, validationResult } = require('express-validator')

// Validación para crear una nueva competición
const validatorCreateCompetition = () => {
    return [
        async (req, res, next) => {
            await check('name')
                .notEmpty()
                .withMessage('El nombre de la competición es requerido.')
                .run(req)
            await check('category_guid')
                .isUUID()
                .withMessage('El guid de la categoría es requerido.')
                .run(req)
            await check('season_guid')
                .optional()
                .isUUID()
                .withMessage('El guid de la temporada debe ser un UUID válido.')
                .run(req)
            await check('sport_guid')
                .isUUID()
                .withMessage('El guid del deporte es requerido.')
                .run(req)
            await check('type_competition_guid')
                .isUUID()
                .withMessage('El guid del tipo de competición es requerido.')
                .run(req)
            await check('description')
                .optional()
                .isString()
                .withMessage('La descripción debe ser una cadena de texto.')
                .run(req)
            await check('logo_path')
                .optional()
                .isString()
                .withMessage('La ruta del logo debe ser una cadena de texto.')
                .run(req)
            await check('teams')
                .optional()
                .isInt({ min: 0 })
                .withMessage(
                    'El número de equipos debe ser un entero no negativo.'
                )
                .run(req)
            await check('players')
                .optional()
                .isInt({ min: 0 })
                .withMessage(
                    'El número de jugadores debe ser un entero no negativo.'
                )
                .run(req)
            await check('players_field')
                .optional()
                .isInt({ min: 0 })
                .withMessage(
                    'El número de jugadores en campo debe ser un entero no negativo.'
                )
                .run(req)
            await check('start_date')
                .optional()
                .isISO8601()
                .withMessage('La fecha de inicio debe ser una fecha válida.')
                .run(req)
            await check('end_date')
                .optional()
                .isISO8601()
                .withMessage('La fecha de fin debe ser una fecha válida.')
                .run(req)
            await check('site_web')
                .optional()
                .isString()
                .withMessage('El sitio web debe ser una cadena de texto.')
                .run(req)
            await check('registration')
                .isNumeric()
                .withMessage('La inscripcion debe de ser un numero')
                .run(req)
            await check('cost_arbitration')
                .isNumeric()
                .withMessage('El costo de arbitraje debe de ser un numero')
                .run(req)
            await check('payment_arbitration')
                .isNumeric()
                .withMessage('El pago del arbitro debe de ser un numero')
                .run(req)
            await check('payment_arbitration_default')
                .isNumeric()
                .withMessage(
                    'El pago del arbitraje por defecto debe de ser un numero'
                )
                .run(req)

            const errors = validationResult(req)
            if (!errors.isEmpty()) {
                return res.status(400).json({ errors: errors.array() })
            }

            next()
        },
    ]
}

// Validación para obtener una competición por GUID
const validatorCompetition = () => {
    return [
        async (req, res, next) => {
            await check('guid')
                .isUUID()
                .withMessage('El guid de la competición es requerido.')
                .run(req)

            const errors = validationResult(req)
            if (!errors.isEmpty()) {
                return res.status(400).json({ errors: errors.array() })
            }

            next()
        },
    ]
}

// Validación para actualizar una competición
const validatorUpdateCompetition = () => {
    return [
        async (req, res, next) => {
            await check('guid')
                .isUUID()
                .withMessage('El guid de la competición es requerido.')
                .run(req)
            await check('name')
                .optional()
                .notEmpty()
                .withMessage(
                    'El nombre de la competición no puede estar vacío.'
                )
                .run(req)
            await check('description')
                .optional()
                .isString()
                .withMessage('La descripción debe ser una cadena de texto.')
                .run(req)
            await check('logo_path')
                .optional()
                .isString()
                .withMessage('La ruta del logo debe ser una cadena de texto.')
                .run(req)
            await check('teams')
                .optional()
                .isInt({ min: 0 })
                .withMessage(
                    'El número de equipos debe ser un entero no negativo.'
                )
                .run(req)
            await check('players')
                .optional()
                .isInt({ min: 0 })
                .withMessage(
                    'El número de jugadores debe ser un entero no negativo.'
                )
                .run(req)
            await check('players_field')
                .optional()
                .isInt({ min: 0 })
                .withMessage(
                    'El número de jugadores en campo debe ser un entero no negativo.'
                )
                .run(req)
            await check('start_date')
                .optional()
                .isISO8601()
                .withMessage('La fecha de inicio debe ser una fecha válida.')
                .run(req)
            await check('end_date')
                .optional()
                .isISO8601()
                .withMessage('La fecha de fin debe ser una fecha válida.')
                .run(req)
            await check('site_web')
                .optional()
                .isString()
                .withMessage('El sitio web debe ser una cadena de texto.')
                .run(req)
            await check('registration')
                .isNumeric()
                .withMessage('La inscripcion debe de ser un numero')
                .run(req)
            await check('cost_arbitration')
                .isNumeric()
                .withMessage('El costo de arbitraje debe de ser un numero')
                .run(req)
            await check('payment_arbitration')
                .isNumeric()
                .withMessage('El pago del arbitro debe de ser un numero')
                .run(req)
            await check('payment_arbitration_default')
                .isNumeric()
                .withMessage(
                    'El pago del arbitraje por defecto debe de ser un numero'
                )
                .run(req)

            const errors = validationResult(req)
            if (!errors.isEmpty()) {
                return res.status(400).json({ errors: errors.array() })
            }

            next()
        },
    ]
}

// Validación para eliminar una competición
const validatorDeleteCompetition = () => {
    return [
        async (req, res, next) => {
            await check('guid')
                .isUUID()
                .withMessage('El guid de la competición es requerido.')
                .run(req)

            const errors = validationResult(req)
            if (!errors.isEmpty()) {
                return res.status(400).json({ errors: errors.array() })
            }

            next()
        },
    ]
}

// Validación para deshabilitar una competición
const validatorDisableCompetition = () => {
    return [
        async (req, res, next) => {
            await check('guid')
                .isUUID()
                .withMessage('El guid de la competición es requerido.')
                .run(req)

            const errors = validationResult(req)
            if (!errors.isEmpty()) {
                return res.status(400).json({ errors: errors.array() })
            }

            next()
        },
    ]
}

// Validación para habilitar una competición
const validatorEnableCompetition = () => {
    return [
        async (req, res, next) => {
            await check('guid')
                .isUUID()
                .withMessage('El guid de la competición es requerido.')
                .run(req)

            const errors = validationResult(req)
            if (!errors.isEmpty()) {
                return res.status(400).json({ errors: errors.array() })
            }

            next()
        },
    ]
}

module.exports = {
    validatorCreateCompetition,
    validatorCompetition,
    validatorUpdateCompetition,
    validatorDeleteCompetition,
    validatorDisableCompetition,
    validatorEnableCompetition,
}
