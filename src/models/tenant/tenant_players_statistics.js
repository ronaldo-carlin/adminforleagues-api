// Importar Sequelize
const { DataTypes } = require('sequelize')

// Definir la función factory para el modelo PlayersStatisticsTenantModel
const PlayersStatisticsTenantModelFactory = (sequelize) => {
    const PlayersStatisticsTenantModel = sequelize.define(
        'players_statistics',
        {
            id: {
                type: DataTypes.INTEGER,
                primaryKey: true,
                autoIncrement: true,
            },
            guid: {
                type: DataTypes.UUID,
                primaryKey: true,
                defaultValue: DataTypes.UUIDV4,
                unique: true,
            },
            match_guid: {
                type: DataTypes.TEXT,
            },
            player_guid: {
                type: DataTypes.TEXT,
            },
            team_guid: {
                type: DataTypes.TEXT,
            },
            competition_guid: {
                type: DataTypes.TEXT,
            },
            goals: {
                type: DataTypes.INTEGER,
            },
            assists: {
                type: DataTypes.INTEGER,
            },
            yellow_cards: {
                type: DataTypes.INTEGER,
            },
            red_cards: {
                type: DataTypes.INTEGER,
            },
            is_active: {
                type: DataTypes.INTEGER,
                defaultValue: 1,
            },
            created_by: {
                type: DataTypes.UUID,
            },
            updated_by: {
                type: DataTypes.UUID,
            },
            deleted_by: {
                type: DataTypes.UUID,
            },
            created_at: {
                type: DataTypes.DATE,
            },
            updated_at: {
                type: DataTypes.DATE,
            },
            deleted_at: {
                type: DataTypes.DATE,
            },
        },
        {
            tableName: 'players_statistics',
            timestamps: false,
        }
    )

    return PlayersStatisticsTenantModel
}

module.exports = PlayersStatisticsTenantModelFactory
