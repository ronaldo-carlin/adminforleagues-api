const { check } = require('express-validator')
const validateResults = require('../../utils/handleValidators')

// Validaciones para crear un Permission
const ValidatorCreatePermission = [
    check('name').isString().withMessage('El nombre es requerido.'),
    check('description').isString().withMessage('La descripción es requerida.'),
    validateResults,
]

// Validaciones para actualizar un Permission
const ValidatorUpdatePermission = [
    check('guid').isUUID().withMessage('El guid es requerido.'),
    check('name').isString().withMessage('El nombre es requerido.'),
    validateResults,
]

// Validaciones para eliminar un Permission
const ValidatorDeletePermission = [
    check('guid').isUUID().withMessage('El guid es requerido.'),
    validateResults,
]

// Validaciones para deshabilitar un Permission
const ValidatorDisablePermission = [
    check('guid').isUUID().withMessage('El guid es requerido.'),
    validateResults,
]

// Validaciones para habilitar un Permission
const ValidatorEnablePermission = [
    check('guid').isUUID().withMessage('El guid es requerido.'),
    validateResults,
]

module.exports = {
    ValidatorCreatePermission,
    ValidatorUpdatePermission,
    ValidatorDeletePermission,
    ValidatorDisablePermission,
    ValidatorEnablePermission,
}
