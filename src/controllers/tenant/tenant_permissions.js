const { Op } = require('sequelize')
const { v4: uuidv4 } = require('uuid')
const { matchedData } = require('express-validator')
const { handleHttpSuccess } = require('../../utils/handleSuccess')
const { handleHttpError } = require('../../utils/handleError')
const { handleHttpCount } = require('../../utils/handleCount')
const {
    getSequelize,
    getPermissionsModel,
} = require('../../helpers/sequelizeHelper')

/**
 * Este controlador es el encargado de registrar un permiso
 * @param {*} req
 * @param {*} res
 */
const CreatePermission = async (req, res) => {
    try {
        const sequelize = getSequelize(req)
        const PermissionModel = getPermissionsModel(sequelize)
        const data = matchedData(req)
        const created_by = req.user.guid
        const created_at = new Date()
        const guid = await uuidv4()
        data.created_by = created_by
        data.created_at = created_at
        data.guid = guid

        const permision = await PermissionModel.create(data)
        return handleHttpSuccess(res, 'Permiso creado.', 201, permision)
    } catch (error) {
        console.log('Error al crear el permiso:', error)
        handleHttpError(res, 'Error al crear el permiso.', 403, error)
    }
}

/**
 * Este controlador es el encargado de obtener los permisos activos
 * @param {*} req
 * @param {*} res
 */
const Permissions = async (req, res) => {
    try {
        const sequelize = getSequelize(req)
        const PermissionModel = getPermissionsModel(sequelize)

        const permissions = await PermissionModel.findAll({
            where: { is_active: 1 },
        })

        if (!permissions.length) {
            handleHttpError(res, 'No se encontraron permisos.', 404)
            return
        }

        handleHttpSuccess(res, 'Permisos encontrados.', 201, permissions)
    } catch (error) {
        console.log('Error obteniendo permisos:', error)
        handleHttpError(res, 'Error obteniendo permisos.', 403, error)
    }
}

/**
 * Este controlador es el encargado de obtener un permiso por GUID
 * @param {*} req
 * @param {*} res
 */
const Permission = async (req, res) => {
    try {
        const sequelize = getSequelize(req)
        const PermissionModel = getPermissionsModel(sequelize)
        const { guid } = req.params

        const permission = await PermissionModel.findOne({
            where: { guid },
        })

        if (!permission) {
            handleHttpError(res, 'El permiso no existe.', 404)
            return
        }

        if (permission.is_active == 0) {
            handleHttpError(res, 'El permiso ha sido deshabilitado.', 401)
            return
        }

        handleHttpSuccess(res, 'Permiso encontrado.', 201, permission)
    } catch (error) {
        console.log('Error obteniendo permiso:', error)
        handleHttpError(res, 'Error obteniendo permiso.', 403, error)
    }
}

/**
 * Este controlador es el encargado de actualizar un permiso
 * @param {*} req
 * @param {*} res
 */
const UpdatePermission = async (req, res) => {
    try {
        const sequelize = getSequelize(req)
        const PermissionModel = getPermissionsModel(sequelize)
        const { guid } = req.params
        const data = matchedData(req)
        const updated_by = req.user.guid
        const updated_at = new Date()
        data.updated_by = updated_by
        data.updated_at = updated_at

        const [updateCount] = await PermissionModel.update(data, {
            where: { guid },
        })

        if (updateCount === 0) {
            return handleHttpError(res, 'El pago no existe.', 404)
        }

        const permission = await PermissionModel.findOne({
            where: { guid },
        })

        if (!permission) {
            handleHttpError(res, 'El permiso no existe.', 404)
            return
        }

        if (permission.is_active == 0) {
            handleHttpError(res, 'El permiso ha sido deshabilitado.', 401)
            return
        }

        handleHttpSuccess(res, 'Permiso actualizado.', 201, permission)
    } catch (error) {
        console.log('Error actualizando permiso:', error)
        handleHttpError(res, 'Error actualizando permiso.', 403, error)
    }
}

/**
 * Este controlador es el encargado de eliminar un permiso
 * @param {*} req
 * @param {*} res
 */
const DeletePermission = async (req, res) => {
    try {
        const sequelize = getSequelize(req)
        const PermissionModel = getPermissionsModel(sequelize)
        const { guid } = req.params
        const deleted_by = req.user.guid
        const deleted_at = new Date()
        const [updateCount] = await PermissionModel.update(
            {
                is_active: 0,
                deleted_by,
                deleted_at,
            },
            {
                where: { guid },
            }
        )

        if (updateCount === 0) {
            return handleHttpError(res, 'El permiso no existe.', 404)
        }

        handleHttpSuccess(res, 'Permiso eliminado.', 201)
    } catch (error) {
        console.log('Error eliminando permiso:', error)
        handleHttpError(res, 'Error eliminando permiso.', 403, error)
    }
}

/**
 * Este controlador es el encargado de desactivar un permiso
 * @param {*} req
 * @param {*} res
 */
const DisablePermission = async (req, res) => {
    try {
        const sequelize = getSequelize(req)
        const PermissionModel = getPermissionsModel(sequelize)
        const { guid } = req.params
        const updated_by = req.user.guid
        const updated_at = new Date()
        const [updateCount] = await PermissionModel.update(
            {
                is_active: 0,
                updated_by,
                updated_at,
            },
            {
                where: { guid },
            }
        )

        if (updateCount === 0) {
            return handleHttpError(res, 'El permiso no existe.', 404)
        }

        handleHttpSuccess(res, 'Permiso deshabilitado.', 201)
    } catch (error) {
        console.log('Error deshabilitando permiso:', error)
        handleHttpError(res, 'Error deshabilitando permiso.', 403, error)
    }
}

/**
 * Este controlador es el encargado de activar un permiso
 * @param {*} req
 * @param {*} res
 */
const EnablePermission = async (req, res) => {
    try {
        const sequelize = getSequelize(req)
        const PermissionModel = getPermissionsModel(sequelize)
        const { guid } = req.params
        const updated_by = req.user.guid
        const updated_at = new Date()
        const [updateCount] = await PermissionModel.update(
            {
                is_active: 1,
                updated_by,
                updated_at,
            },
            {
                where: { guid },
            }
        )

        if (updateCount === 0) {
            return handleHttpError(res, 'El permiso no existe.', 404)
        }

        handleHttpSuccess(res, 'Permiso habilitado.', 201)
    } catch (error) {
        console.log('Error habilitando permiso:', error)
        handleHttpError(res, 'Error habilitando permiso.', 403, error)
    }
}

/**
 * Este controlador es el encargado de obtener todos los permisos
 * @param {*} req
 * @param {*} res
 */
const PermissionsAll = async (req, res) => {
    try {
        const sequelize = getSequelize(req)
        const PermissionModel = getPermissionsModel(sequelize)

        const permissions = await PermissionModel.findAll()

        if (!permissions.length) {
            handleHttpError(res, 'No se encontraron permisos.', 404)
            return
        }

        handleHttpSuccess(res, 'Permisos encontrados.', 201, permissions)
    } catch (error) {
        console.log('Error obteniendo permisos:', error)
        handleHttpError(res, 'Error obteniendo permisos.', 403, error)
    }
}

/**
 * Este controlador es el encargado de obtener todos los permisos de un usuario
 * @param {*} req
 * @param {*} res
 */
const PermissionsCount = async (req, res) => {
    try {
        const sequelize = getSequelize(req)
        const PermissionModel = getPermissionsModel(sequelize)

        const permissions = await PermissionModel.count()

        if (!permissions.length) {
            handleHttpError(res, 'No se encontraron permisos.', 404)
            return
        }

        handleHttpCount(res, 'Permisos encontrados.', 201, permissions)
    } catch (error) {
        console.log('Error obteniendo permisos:', error)
        handleHttpError(res, 'Error obteniendo permisos.', 403, error)
    }
}

module.exports = {
    CreatePermission,
    Permissions,
    Permission,
    UpdatePermission,
    DeletePermission,
    DisablePermission,
    EnablePermission,
    PermissionsAll,
    PermissionsCount,
}
