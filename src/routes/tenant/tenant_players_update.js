const express = require('express')
const router = express.Router()

// Controladores y validadores para players
const {
    validatorCreatePlayerUpdate,
    validatorPlayerUpdate,
    validatorUpdatePlayerUpdate,
    validatorDeletePlayerUpdate,
    validatorDisablePlayerUpdate,
    validatorEnablePlayerUpdate,
} = require('../../validators/tenant/tenant_players_update')

const {
    CreatePlayerUpdate,
    PlayersUpdate,
    PlayerUpdate,
    UpdatePlayerUpdate,
    DeletePlayerUpdate,
    DisablePlayerUpdate,
    EnablePlayerUpdate,
    PlayersUpdateAll,
    PlayersUpdateCount,
} = require('../../controllers/tenant/tenant_players_update')

const authMiddleware = require('../../middleware/session_tenant')

/* Rutas protegidas que requieren autenticación */
/**
 * @swagger
 * tags:
 *   name: Tenant_Players_Update
 *   description: API para la gestión de jugadores actualizados por los mismos jugadores
 */

// Ruta para crear un nuevo jugador
/**
 * @swagger
 * /tenant_players_update/CreatePlayer:
 *   post:
 *     summary: Crear un nuevo jugador
 *     description: Esta ruta permite crear un nuevo jugador con los detalles especificados.
 *     tags: [Tenant_Players_Update]
 *     requestBody:
 *       required: true
 *       content:
 *         application/json:
 *           schema:
 *             type: object
 *             properties:
 *               name:
 *                 type: string
 *                 example: "Juan Pérez"
 *               team_guid:
 *                 type: string
 *                 format: uuid
 *                 example: "495db854-849b-41cd-b6b9-9c84824c92ba"
 *     responses:
 *       201:
 *         description: Jugador creado exitosamente
 *         content:
 *           application/json:
 *             schema:
 *               $ref: '#/components/responses/success'
 *       400:
 *         description: Error en los datos enviados
 *         content:
 *           application/json:
 *             schema:
 *               $ref: '#/components/responses/BadRequest'
 *       403:
 *         description: Error de autenticación
 *         content:
 *           application/json:
 *             schema:
 *               $ref: '#/components/responses/UnauthorizedError'
 *       409:
 *         description: El jugador ya existe
 *         content:
 *           application/json:
 *             schema:
 *               $ref: '#/components/responses/ConflictError'
 *       500:
 *         description: Error en el servidor
 *         content:
 *           application/json:
 *             schema:
 *               $ref: '#/components/responses/InternalServerError'
 *     security:
 *        - bearerAuth: []
 */
router.post(
    '/CreatePlayerUpdate',
    authMiddleware,
    validatorCreatePlayerUpdate(),
    CreatePlayerUpdate
)

// Ruta para obtener todos los jugadores activos
/**
 * @swagger
 * /tenant_players_update/PlayersUpdate:
 *   get:
 *     summary: Obtener todos los jugadores activos
 *     tags: [Tenant_Players_Update]
 *     responses:
 *       200:
 *         description: Jugadores obtenidos exitosamente
 *         content:
 *           application/json:
 *             schema:
 *               $ref: '#/components/responses/success'
 *       403:
 *         description: Error de autenticación
 *         content:
 *           application/json:
 *             schema:
 *               $ref: '#/components/responses/UnauthorizedError'
 *       500:
 *         description: Error en el servidor
 *         content:
 *           application/json:
 *             schema:
 *               $ref: '#/components/responses/InternalServerError'
 *     security:
 *        - bearerAuth: []
 */
router.get('/PlayersUpdate', authMiddleware, PlayersUpdate)

// Ruta para obtener un jugador por GUID
/**
 * @swagger
 * /tenant_players_update/Player/{guid}:
 *   get:
 *     summary: Obtener un jugador por GUID
 *     tags: [Tenant_Players_Update]
 *     parameters:
 *       - in: path
 *         name: guid
 *         schema:
 *           type: string
 *           format: uuid
 *         required: true
 *         description: El GUID del jugador
 *     responses:
 *       200:
 *         description: Jugador obtenido exitosamente
 *         content:
 *           application/json:
 *             schema:
 *               $ref: '#/components/responses/success'
 *       403:
 *         description: Error de autenticación
 *         content:
 *           application/json:
 *             schema:
 *               $ref: '#/components/responses/UnauthorizedError'
 *       404:
 *         description: Jugador no encontrado
 *         content:
 *           application/json:
 *             schema:
 *               $ref: '#/components/responses/NotFoundError'
 *       500:
 *         description: Error en el servidor
 *         content:
 *           application/json:
 *             schema:
 *               $ref: '#/components/responses/InternalServerError'
 *     security:
 *        - bearerAuth: []
 */
router.get(
    '/PlayerUpdate/:guid',
    authMiddleware,
    validatorPlayerUpdate(),
    PlayerUpdate
)

// Ruta para actualizar un jugador
/**
 * @swagger
 * /tenant_players_update/UpdatePlayer/{guid}:
 *   patch:
 *     summary: Actualizar un jugador
 *     tags: [Tenant_Players_Update]
 *     requestBody:
 *       required: true
 *       content:
 *         application/json:
 *           schema:
 *             type: object
 *             properties:
 *               name:
 *                 type: string
 *                 example: "Juan Pérez"
 *               team_guid:
 *                 type: string
 *                 format: uuid
 *                 example: "495db854-849b-41cd-b6b9-9c84824c92ba"
 *     responses:
 *       200:
 *         description: Jugador actualizado exitosamente
 *         content:
 *           application/json:
 *             schema:
 *               $ref: '#/components/responses/success'
 *       400:
 *         description: Error en los datos enviados
 *         content:
 *           application/json:
 *             schema:
 *               $ref: '#/components/responses/BadRequest'
 *       403:
 *         description: Error de autenticación
 *         content:
 *           application/json:
 *             schema:
 *               $ref: '#/components/responses/UnauthorizedError'
 *       404:
 *         description: Jugador no encontrado
 *         content:
 *           application/json:
 *             schema:
 *               $ref: '#/components/responses/NotFoundError'
 *       500:
 *         description: Error en el servidor
 *         content:
 *           application/json:
 *             schema:
 *               $ref: '#/components/responses/InternalServerError'
 *     security:
 *        - bearerAuth: []
 */
router.patch(
    '/UpdatePlayerUpdate/:guid',
    authMiddleware,
    validatorUpdatePlayerUpdate(),
    UpdatePlayerUpdate
)

// Ruta para eliminar un jugador
/**
 * @swagger
 * /tenant_players_update/DeletePlayer/{guid}:
 *   delete:
 *     summary: Eliminar un jugador
 *     tags: [Tenant_Players_Update]
 *     parameters:
 *       - in: path
 *         name: guid
 *         schema:
 *           type: string
 *           format: uuid
 *         required: true
 *         description: El GUID del jugador
 *     responses:
 *       200:
 *         description: Jugador eliminado exitosamente
 *         content:
 *           application/json:
 *             schema:
 *               $ref: '#/components/responses/success'
 *       400:
 *         description: Error en los datos enviados
 *         content:
 *           application/json:
 *             schema:
 *               $ref: '#/components/responses/BadRequest'
 *       403:
 *         description: Error de autenticación
 *         content:
 *           application/json:
 *             schema:
 *               $ref: '#/components/responses/UnauthorizedError'
 *       404:
 *         description: Jugador no encontrado
 *         content:
 *           application/json:
 *             schema:
 *               $ref: '#/components/responses/NotFoundError'
 *       500:
 *         description: Error en el servidor
 *         content:
 *           application/json:
 *             schema:
 *               $ref: '#/components/responses/InternalServerError'
 *     security:
 *        - bearerAuth: []
 */
router.delete(
    '/DeletePlayerUpdate/:guid',
    authMiddleware,
    validatorDeletePlayerUpdate(),
    DeletePlayerUpdate
)

// Ruta para deshabilitar un jugador
/**
 * @swagger
 * /tenant_players_update/DisablePlayer/{guid}:
 *   patch:
 *     summary: Deshabilitar un jugador
 *     tags: [Tenant_Players_Update]
 *     requestBody:
 *       required: true
 *       content:
 *         application/json:
 *           schema:
 *             type: object
 *             properties:
 *               guid:
 *                 type: uuid
 *                 example: 495db854-849b-41cd-b6b9-9c84824c92ba
 *     responses:
 *       200:
 *         description: Jugador deshabilitado exitosamente
 *         content:
 *           application/json:
 *             schema:
 *               $ref: '#/components/responses/success'
 *       400:
 *         description: Error en los datos enviados
 *         content:
 *           application/json:
 *             schema:
 *               $ref: '#/components/responses/BadRequest'
 *       403:
 *         description: Error de autenticación
 *         content:
 *           application/json:
 *             schema:
 *               $ref: '#/components/responses/UnauthorizedError'
 *       404:
 *         description: Jugador no encontrado
 *         content:
 *           application/json:
 *             schema:
 *               $ref: '#/components/responses/NotFoundError'
 *       500:
 *         description: Error en el servidor
 *         content:
 *           application/json:
 *             schema:
 *               $ref: '#/components/responses/InternalServerError'
 *     security:
 *        - bearerAuth: []
 */
router.patch(
    '/DisablePlayerUpdate/:guid',
    authMiddleware,
    validatorDisablePlayerUpdate(),
    DisablePlayerUpdate
)

// Ruta para habilitar un jugador
/**
 * @swagger
 * /tenant_players_update/EnablePlayer/{guid}:
 *   patch:
 *     summary: Habilitar un jugador
 *     tags: [Tenant_Players_Update]
 *     requestBody:
 *       required: true
 *       content:
 *         application/json:
 *           schema:
 *             type: object
 *             properties:
 *               guid:
 *                 type: uuid
 *                 example: 495db854-849b-41cd-b6b9-9c84824c92ba
 *     responses:
 *       200:
 *         description: Jugador habilitado exitosamente
 *         content:
 *           application/json:
 *             schema:
 *               $ref: '#/components/responses/success'
 *       400:
 *         description: Error en los datos enviados
 *         content:
 *           application/json:
 *             schema:
 *               $ref: '#/components/responses/BadRequest'
 *       403:
 *         description: Error de autenticación
 *         content:
 *           application/json:
 *             schema:
 *               $ref: '#/components/responses/UnauthorizedError'
 *       404:
 *         description: Jugador no encontrado
 *         content:
 *           application/json:
 *             schema:
 *               $ref: '#/components/responses/NotFoundError'
 *       500:
 *         description: Error en el servidor
 *         content:
 *           application/json:
 *             schema:
 *               $ref: '#/components/responses/InternalServerError'
 *     security:
 *        - bearerAuth: []
 */
router.patch(
    '/EnablePlayerUpdate/:guid',
    authMiddleware,
    validatorEnablePlayerUpdate(),
    EnablePlayerUpdate
)

// Ruta para obtener todos los jugadores
/**
 * @swagger
 * /tenant_players_update/PlayersAll:
 *   get:
 *     summary: Obtener todos los jugadores actualizados
 *     tags: [Tenant_Players_Update]
 *     responses:
 *       200:
 *         description: players obtenidos exitosamente
 *         content:
 *           application/json:
 *             schema:
 *               $ref: '#/components/responses/success'
 *       403:
 *         description: Error de autenticación
 *         content:
 *           application/json:
 *             schema:
 *               $ref: '#/components/responses/UnauthorizedError'
 *       500:
 *         description: Error en el servidor
 *         content:
 *           application/json:
 *             schema:
 *               $ref: '#/components/responses/InternalServerError'
 *     security:
 *        - bearerAuth: []
 */
router.get('/PlayersUpdateAll', authMiddleware, PlayersUpdateAll)

// Ruta para contar jugadores
/**
 * @swagger
 * /tenant_players_update/PlayersCount:
 *   get:
 *     summary: Contar jugadores actualizados
 *     tags: [Tenant_Players_Update]
 *     responses:
 *       200:
 *         description: Contador de jugadores obtenido exitosamente
 *         content:
 *           application/json:
 *             schema:
 *               $ref: '#/components/responses/success'
 *       403:
 *         description: Error de autenticación
 *         content:
 *           application/json:
 *             schema:
 *               $ref: '#/components/responses/UnauthorizedError'
 *       500:
 *         description: Error en el servidor
 *         content:
 *           application/json:
 *             schema:
 *               $ref: '#/components/responses/InternalServerError'
 *     security:
 *        - bearerAuth: []
 */
router.get('/PlayersUpdateCount', authMiddleware, PlayersUpdateCount)

module.exports = router
