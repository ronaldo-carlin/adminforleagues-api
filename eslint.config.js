module.exports = {
    // Configuración general de ESLint aquí
    ignores: [
        '**/.commitlintrc.json',
        '**/ecosystem.config.js',
        '**/.husky/*',
        '**/.prettierignore',
        '**/CHANGELOG.md',
        '**/README.md',
        '**/package-lock.json',
        '**/package.json',
        '**/yarn.lock',
        '**/dist/*',
        '**/node_modules/*',
        '**/coverage/*',
        '**/public/*',
        '**/src/assets/*',
        '**/src/environments/*',
        '**/src/index.html',
        // Otros patrones a ignorar según sea necesario
    ],
    // Reglas y configuraciones adicionales de ESLint
}
