const handleHttpCount = (res, msg = 'Todo Ok!', code = 201, count) => {
    res.status(code)

    const responseObj = {
        code: code,
        success: true,
        message: msg,
        count: count,
    }

    res.send(responseObj)
}

module.exports = { handleHttpCount }
