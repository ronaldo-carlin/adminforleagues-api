module.exports = {
    async up(queryInterface, Sequelize) {
        // entidad para las estadisticas de los jugadores
        await queryInterface.createTable('players_statistics', {
            // identificador de la tabla
            id: {
                type: Sequelize.INTEGER,
                primaryKey: true,
                autoIncrement: true,
                allowNull: false,
            },
            // identificador unico
            guid: {
                type: Sequelize.UUID,
                defaultValue: Sequelize.UUIDV4,
                unique: true,
                allowNull: false,
            },
            // identificador del partido
            match_guid: {
                type: Sequelize.UUID,
            },
            // identificador del jugador
            player_guid: {
                type: Sequelize.UUID,
            },
            // identificador del equipo
            team_guid: {
                type: Sequelize.UUID,
            },
            // identificador del torneo
            competition_guid: {
                type: Sequelize.UUID,
            },
            // cantidad de goles del jugador
            goals: {
                type: Sequelize.INTEGER,
            },
            // cantidad de asistencias del jugador
            assists: {
                type: Sequelize.INTEGER,
            },
            // cantidad de tarjetas amarillas del jugador
            yellow_cards: {
                type: Sequelize.INTEGER,
            },
            // cantidad de tarjetas rojas del jugador
            red_cards: {
                type: Sequelize.INTEGER,
            },
            // estado del registro
            is_active: {
                type: Sequelize.BOOLEAN,
                defaultValue: 1, // valor por defecto
            },
            // usuario que creo el registro
            created_by: {
                type: Sequelize.UUID,
            },
            // fecha de creacion
            created_at: {
                type: Sequelize.DATE,
                defaultValue: Sequelize.literal('CURRENT_TIMESTAMP'),
            },
            // usuario que actualizo el registro
            updated_by: {
                type: Sequelize.UUID,
            },
            // fecha de actualizacion
            updated_at: {
                type: Sequelize.DATE,
            },
            // usuario que elimino el registro
            deleted_by: {
                type: Sequelize.UUID,
            },
            // fecha de eliminacion
            deleted_at: {
                type: Sequelize.DATE,
            },
        })

        // indices
        await queryInterface.addIndex('players_statistics', ['guid'], {
            unique: true,
            name: 'players_statistics_guid',
        })

        await queryInterface.addIndex('players_statistics', ['match_guid'], {
            name: 'idx_players_statistics_match_guid',
        })

        await queryInterface.addIndex('players_statistics', ['player_guid'], {
            name: 'idx_players_statistics_player_guid',
        })

        await queryInterface.addIndex('players_statistics', ['team_guid'], {
            name: 'idx_players_statistics_team_guid',
        })

        await queryInterface.addIndex(
            'players_statistics',
            ['competition_guid'],
            {
                name: 'idx_players_statistics_competition_guid',
            }
        )

        // llaves foraneas
        await queryInterface.addConstraint('players_statistics', {
            fields: ['match_guid'],
            type: 'foreign key',
            name: 'fk_players_statistics_match_guid',
            references: {
                table: 'matches',
                field: 'guid',
            },
            onUpdate: 'CASCADE',
            onDelete: 'NO ACTION',
        })

        await queryInterface.addConstraint('players_statistics', {
            fields: ['player_guid'],
            type: 'foreign key',
            name: 'fk_players_statistics_player_guid',
            references: {
                table: 'players',
                field: 'guid',
            },
            onUpdate: 'CASCADE',
            onDelete: 'NO ACTION',
        })

        await queryInterface.addConstraint('players_statistics', {
            fields: ['team_guid'],
            type: 'foreign key',
            name: 'fk_players_statistics_team_guid',
            references: {
                table: 'teams',
                field: 'guid',
            },
            onUpdate: 'CASCADE',
            onDelete: 'NO ACTION',
        })

        await queryInterface.addConstraint('players_statistics', {
            fields: ['competition_guid'],
            type: 'foreign key',
            name: 'fk_players_statistics_competition_guid',
            references: {
                table: 'competitions',
                field: 'guid',
            },
            onUpdate: 'CASCADE',
            onDelete: 'NO ACTION',
        })
    },

    async down(queryInterface, Sequelize) {
        await queryInterface.removeConstraint(
            'players_statistics',
            'fk_players_statistics_competition_guid'
        )
        await queryInterface.removeConstraint(
            'players_statistics',
            'fk_players_statistics_team_guid'
        )
        await queryInterface.removeConstraint(
            'players_statistics',
            'fk_players_statistics_player_guid'
        )
        await queryInterface.removeConstraint(
            'players_statistics',
            'fk_players_statistics_match_guid'
        )
        await queryInterface.removeIndex(
            'players_statistics',
            'idx_players_statistics_competition_guid'
        )
        await queryInterface.removeIndex(
            'players_statistics',
            'idx_players_statistics_team_guid'
        )
        await queryInterface.removeIndex(
            'players_statistics',
            'idx_players_statistics_player_guid'
        )
        await queryInterface.removeIndex(
            'players_statistics',
            'idx_players_statistics_match_guid'
        )
        await queryInterface.removeIndex(
            'players_statistics',
            'players_statistics_guid'
        )
        await queryInterface.dropTable('players_statistics')
    },
}
