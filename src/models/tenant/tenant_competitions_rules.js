// Importar Sequelize
const { DataTypes } = require('sequelize')

// Definir la función factory para el modelo CompetitionRulesTenantModel
const CompetitionRulesTenantModelFactory = (sequelize) => {
    const CompetitionRulesTenantModel = sequelize.define(
        'competitions_rules',
        {
            id: {
                type: DataTypes.INTEGER,
                primaryKey: true,
                autoIncrement: true,
            },
            guid: {
                type: DataTypes.UUID,
                primaryKey: true,
                defaultValue: DataTypes.UUIDV4,
                unique: true,
            },
            type_competition_guid: {
                type: DataTypes.UUID,
            },
            title: {
                type: DataTypes.TEXT,
            },
            description: {
                type: DataTypes.TEXT,
            },
            is_active: {
                type: DataTypes.INTEGER,
                defaultValue: 1,
            },
            created_by: {
                type: DataTypes.UUID,
            },
            updated_by: {
                type: DataTypes.UUID,
            },
            deleted_by: {
                type: DataTypes.UUID,
            },
            created_at: {
                type: DataTypes.DATE,
            },
            updated_at: {
                type: DataTypes.DATE,
            },
            deleted_at: {
                type: DataTypes.DATE,
            },
        },
        {
            tableName: 'competitions_rules',
            timestamps: false,
        }
    )

    return CompetitionRulesTenantModel
}

module.exports = CompetitionRulesTenantModelFactory
