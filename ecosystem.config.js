module.exports = {
    apps: [
        {
            name: 'AFL-App',
            script: 'src/index.js', // Ruta del archivo de entrada de tu aplicación
            instances: 1, // Número de instancias a ejecutar
            exec_mode: 'fork', // Modo de ejecución (puede ser 'fork' o 'cluster')
            watch: false, // Vigilar cambios en archivos (puede ser true o false)
            env: {
                NODE_ENV: 'production', // Variables de entorno para la aplicación
            },
        },
    ],
    // Comandos a ejecutar después de la implementación
    deploy: {
        production: {
            user: 'SSH_USERNAME',
            host: 'SSH_HOSTMACHINE',
            ref: 'origin/master',
            repo: 'GIT_REPOSITORY',
            path: 'DESTINATION_PATH',
            'pre-deploy-local': '',
            'post-deploy':
                'npm install && pm2 reload ecosystem.config.js --env production',
            'pre-setup': '',
        },
    },
}
