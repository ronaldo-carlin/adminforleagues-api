// Importar Sequelize
const { DataTypes } = require('sequelize')

// Definir la función factory para el modelo PermissionsTenantModel
const PermissionsTenantModelFactory = (sequelize) => {
    const PermissionsTenantModel = sequelize.define(
        'permissions',
        {
            id: {
                type: DataTypes.INTEGER,
                primaryKey: true,
                autoIncrement: true,
            },
            guid: {
                type: DataTypes.UUID,
                primaryKey: true,
                defaultValue: DataTypes.UUIDV4,
                unique: true,
            },
            name: {
                type: DataTypes.TEXT,
            },
            description: {
                type: DataTypes.TEXT,
            },
            is_active: {
                type: DataTypes.INTEGER,
                defaultValue: 1,
            },
            created_by: {
                type: DataTypes.UUID,
            },
            updated_by: {
                type: DataTypes.UUID,
            },
            deleted_by: {
                type: DataTypes.UUID,
            },
            created_at: {
                type: DataTypes.DATE,
            },
            updated_at: {
                type: DataTypes.DATE,
            },
            deleted_at: {
                type: DataTypes.DATE,
            },
        },
        {
            tableName: 'permissions',
            timestamps: false,
            defaultScope: {
                attributes: {
                    exclude: [
                        'created_by',
                        'created_at',
                        'updated_by',
                        'updated_at',
                        'deleted_by',
                        'deleted_at',
                    ],
                },
            },
        }
    )

    return PermissionsTenantModel
}

module.exports = PermissionsTenantModelFactory
