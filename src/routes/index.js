const express = require('express')
const path = require('path')

const router = express.Router()

const detectSubdomain = require('../middleware/detectSubdomain')

router.use(detectSubdomain)

// Cargar rutas de 'general'
const generalRoutes = require('./general')
router.use('/api', generalRoutes)

// Cargar rutas de 'tenant'
const tenantRoutes = require('./tenant')
router.use('/api', tenantRoutes)

module.exports = router
