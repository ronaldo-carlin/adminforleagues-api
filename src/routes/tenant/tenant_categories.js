const express = require('express')
const router = express.Router()

// Controladores y validadores para categories
const {
    validatorCreateCategory,
    validatorCategory,
    validatorUpdateCategory,
    validatorDeleteCategory,
    validatorDisableCategory,
    validatorEnableCategory,
    validatorSport,
} = require('../../validators/tenant/tenant_categories')

const {
    CreateCategory,
    Categories,
    CategoriesSport,
    Category,
    UpdateCategory,
    DeleteCategory,
    DisableCategory,
    EnableCategory,
    CategoriesAll,
    CategoriesCount,
} = require('../../controllers/tenant/tenant_categories')

const authMiddleware = require('../../middleware/session_tenant')

/* Rutas protegidas que requieren autenticación */
/**
 * @swagger
 * tags:
 *   name: Tenant_Categories
 *   description: API para la gestión de categorías
 */

// Ruta para crear una nueva categoría
/**
 * @swagger
 * /tenant_categories/CreateCategory:
 *   post:
 *     summary: Crear una nueva categoría
 *     description: Esta ruta permite crear una nueva categoría con los detalles especificados.
 *     tags: [Tenant_Categories]
 *     requestBody:
 *       required: true
 *       content:
 *         application/json:
 *           schema:
 *             type: object
 *             properties:
 *               name:
 *                 type: string
 *                 example: "Tecnología"
 *               description:
 *                 type: string
 *                 example: "Categoría que abarca productos tecnológicos."
 *     responses:
 *       201:
 *         description: Categoría creada exitosamente
 *         content:
 *           application/json:
 *             schema:
 *               $ref: '#/components/responses/success'
 *       400:
 *         description: Error en los datos enviados
 *         content:
 *           application/json:
 *             schema:
 *               $ref: '#/components/responses/BadRequest'
 *       403:
 *         description: Error de autenticación
 *         content:
 *           application/json:
 *             schema:
 *               $ref: '#/components/responses/UnauthorizedError'
 *       409:
 *         description: La categoría ya existe
 *         content:
 *           application/json:
 *             schema:
 *               $ref: '#/components/responses/ConflictError'
 *       500:
 *         description: Error en el servidor
 *         content:
 *           application/json:
 *             schema:
 *               $ref: '#/components/responses/InternalServerError'
 *     security:
 *       - bearerAuth: []
 */
router.post(
    '/CreateCategory',
    authMiddleware,
    validatorCreateCategory(),
    CreateCategory
)

// Ruta para obtener todas las categorías activas
/**
 * @swagger
 * /tenant_categories/Categories:
 *   get:
 *     summary: Obtener todas las categorías activas
 *     tags: [Tenant_Categories]
 *     responses:
 *       200:
 *         description: Categorías obtenidas exitosamente
 *         content:
 *           application/json:
 *             schema:
 *               $ref: '#/components/responses/success'
 *       403:
 *         description: Error de autenticación
 *         content:
 *           application/json:
 *             schema:
 *               $ref: '#/components/responses/UnauthorizedError'
 *       500:
 *         description: Error en el servidor
 *         content:
 *           application/json:
 *             schema:
 *               $ref: '#/components/responses/InternalServerError'
 *     security:
 *       - bearerAuth: []
 */
router.get('/Categories', authMiddleware, Categories)

// Ruta para obtener todas las categorías activas dependiendo el deporte seleccionado
/**
 * @swagger
 * /tenant_categories/Category/{guid}:
 *   get:
 *     summary: Obtener las categorías activas dependiendo el deporte seleccionado
 *     tags: [Tenant_Categories]
 *     parameters:
 *       - in: path
 *         name: guid
 *         schema:
 *           type: string
 *           format: uuid
 *         required: true
 *         description: El GUID de la categoría
 *     responses:
 *       200:
 *         description: Categorías obtenida exitosamente
 *         content:
 *           application/json:
 *             schema:
 *               $ref: '#/components/responses/success'
 *       403:
 *         description: Error de autenticación
 *         content:
 *           application/json:
 *             schema:
 *               $ref: '#/components/responses/UnauthorizedError'
 *       404:
 *         description: Categorías no encontradas
 *         content:
 *           application/json:
 *             schema:
 *               $ref: '#/components/responses/NotFoundError'
 *       500:
 *         description: Error en el servidor
 *         content:
 *           application/json:
 *             schema:
 *               $ref: '#/components/responses/InternalServerError'
 *     security:
 *       - bearerAuth: []
 */
router.get(
    '/Categories/:guid',
    authMiddleware,
    validatorSport(),
    CategoriesSport
)

// Ruta para obtener una categoría por GUID
/**
 * @swagger
 * /tenant_categories/Category/{guid}:
 *   get:
 *     summary: Obtener una categoría por GUID
 *     tags: [Tenant_Categories]
 *     parameters:
 *       - in: path
 *         name: guid
 *         schema:
 *           type: string
 *           format: uuid
 *         required: true
 *         description: El GUID de la categoría
 *     responses:
 *       200:
 *         description: Categoría obtenida exitosamente
 *         content:
 *           application/json:
 *             schema:
 *               $ref: '#/components/responses/success'
 *       403:
 *         description: Error de autenticación
 *         content:
 *           application/json:
 *             schema:
 *               $ref: '#/components/responses/UnauthorizedError'
 *       404:
 *         description: Categoría no encontrada
 *         content:
 *           application/json:
 *             schema:
 *               $ref: '#/components/responses/NotFoundError'
 *       500:
 *         description: Error en el servidor
 *         content:
 *           application/json:
 *             schema:
 *               $ref: '#/components/responses/InternalServerError'
 *     security:
 *       - bearerAuth: []
 */
router.get('/Category/:guid', authMiddleware, validatorCategory(), Category)

// Ruta para actualizar una categoría
/**
 * @swagger
 * /tenant_categories/UpdateCategory/{guid}:
 *   patch:
 *     summary: Actualizar una categoría
 *     tags: [Tenant_Categories]
 *     requestBody:
 *       required: true
 *       content:
 *         application/json:
 *           schema:
 *             type: object
 *             properties:
 *               name:
 *                 type: string
 *                 example: "Tecnología"
 *               description:
 *                 type: string
 *                 example: "Categoría que abarca productos tecnológicos."
 *     responses:
 *       200:
 *         description: Categoría actualizada exitosamente
 *         content:
 *           application/json:
 *             schema:
 *               $ref: '#/components/responses/success'
 *       400:
 *         description: Error en los datos enviados
 *         content:
 *           application/json:
 *             schema:
 *               $ref: '#/components/responses/BadRequest'
 *       403:
 *         description: Error de autenticación
 *         content:
 *           application/json:
 *             schema:
 *               $ref: '#/components/responses/UnauthorizedError'
 *       404:
 *         description: Categoría no encontrada
 *         content:
 *           application/json:
 *             schema:
 *               $ref: '#/components/responses/NotFoundError'
 *       500:
 *         description: Error en el servidor
 *         content:
 *           application/json:
 *             schema:
 *               $ref: '#/components/responses/InternalServerError'
 *     security:
 *       - bearerAuth: []
 */
router.patch(
    '/UpdateCategory/:guid',
    authMiddleware,
    validatorUpdateCategory(),
    UpdateCategory
)

// Ruta para eliminar una categoría
/**
 * @swagger
 * /tenant_categories/DeleteCategory/{guid}:
 *   delete:
 *     summary: Eliminar una categoría
 *     tags: [Tenant_Categories]
 *     parameters:
 *       - in: path
 *         name: guid
 *         schema:
 *           type: string
 *           format: uuid
 *         required: true
 *         description: El GUID de la categoría
 *     responses:
 *       200:
 *         description: Categoría eliminada exitosamente
 *         content:
 *           application/json:
 *             schema:
 *               $ref: '#/components/responses/success'
 *       400:
 *         description: Error en los datos enviados
 *         content:
 *           application/json:
 *             schema:
 *               $ref: '#/components/responses/BadRequest'
 *       403:
 *         description: Error de autenticación
 *         content:
 *           application/json:
 *             schema:
 *               $ref: '#/components/responses/UnauthorizedError'
 *       404:
 *         description: Categoría no encontrada
 *         content:
 *           application/json:
 *             schema:
 *               $ref: '#/components/responses/NotFoundError'
 *       500:
 *         description: Error en el servidor
 *         content:
 *           application/json:
 *             schema:
 *               $ref: '#/components/responses/InternalServerError'
 *     security:
 *       - bearerAuth: []
 */
router.delete(
    '/DeleteCategory/:guid',
    authMiddleware,
    validatorDeleteCategory(),
    DeleteCategory
)

// Ruta para deshabilitar una categoría
/**
 * @swagger
 * /tenant_categories/DisableCategory/{guid}:
 *   patch:
 *     summary: Deshabilitar una categoría
 *     tags: [Tenant_Categories]
 *     requestBody:
 *       required: true
 *       content:
 *         application/json:
 *           schema:
 *             type: object
 *             properties:
 *               guid:
 *                 type: uuid
 *                 example: 495db854-849b-41cd-b6b9-9c84824c92ba
 *     responses:
 *       200:
 *         description: Categoría deshabilitada exitosamente
 *         content:
 *           application/json:
 *             schema:
 *               $ref: '#/components/responses/success'
 *       400:
 *         description: Error en los datos enviados
 *         content:
 *           application/json:
 *             schema:
 *               $ref: '#/components/responses/BadRequest'
 *       403:
 *         description: Error de autenticación
 *         content:
 *           application/json:
 *             schema:
 *               $ref: '#/components/responses/UnauthorizedError'
 *       404:
 *         description: Categoría no encontrada
 *         content:
 *           application/json:
 *             schema:
 *               $ref: '#/components/responses/NotFoundError'
 *       500:
 *         description: Error en el servidor
 *         content:
 *           application/json:
 *             schema:
 *               $ref: '#/components/responses/InternalServerError'
 *     security:
 *       - bearerAuth: []
 */
router.patch(
    '/DisableCategory/:guid',
    authMiddleware,
    validatorDisableCategory(),
    DisableCategory
)

// Ruta para habilitar una categoría
/**
 * @swagger
 * /tenant_categories/EnableCategory/{guid}:
 *   patch:
 *     summary: Habilitar una categoría
 *     tags: [Tenant_Categories]
 *     requestBody:
 *       required: true
 *       content:
 *         application/json:
 *           schema:
 *             type: object
 *             properties:
 *               guid:
 *                 type: uuid
 *                 example: 495db854-849b-41cd-b6b9-9c84824c92ba
 *     responses:
 *       200:
 *         description: Categoría habilitada exitosamente
 *         content:
 *           application/json:
 *             schema:
 *               $ref: '#/components/responses/success'
 *       400:
 *         description: Error en los datos enviados
 *         content:
 *           application/json:
 *             schema:
 *               $ref: '#/components/responses/BadRequest'
 *       403:
 *         description: Error de autenticación
 *         content:
 *           application/json:
 *             schema:
 *               $ref: '#/components/responses/UnauthorizedError'
 *       404:
 *         description: Categoría no encontrada
 *         content:
 *           application/json:
 *             schema:
 *               $ref: '#/components/responses/NotFoundError'
 *       500:
 *         description: Error en el servidor
 *         content:
 *           application/json:
 *             schema:
 *               $ref: '#/components/responses/InternalServerError'
 *     security:
 *       - bearerAuth: []
 */
router.patch(
    '/EnableCategory/:guid',
    authMiddleware,
    validatorEnableCategory(),
    EnableCategory
)

// Ruta para obtener todas las categorías
/**
 * @swagger
 * /tenant_categories/CategoriesAll:
 *   get:
 *     summary: Obtener todas las categorías
 *     tags: [Tenant_Categories]
 *     responses:
 *       200:
 *         description: Todas las categorías obtenidas exitosamente
 *         content:
 *           application/json:
 *             schema:
 *               $ref: '#/components/responses/success'
 *       403:
 *         description: Error de autenticación
 *         content:
 *           application/json:
 *             schema:
 *               $ref: '#/components/responses/UnauthorizedError'
 *       500:
 *         description: Error en el servidor
 *         content:
 *           application/json:
 *             schema:
 *               $ref: '#/components/responses/InternalServerError'
 *     security:
 *       - bearerAuth: []
 */
router.get('/CategoriesAll', authMiddleware, CategoriesAll)

// Ruta para obtener el conteo de categorías
/**
 * @swagger
 * /tenant_categories/CategoriesCount:
 *   get:
 *     summary: Obtener el conteo de categorías
 *     tags: [Tenant_Categories]
 *     responses:
 *       200:
 *         description: Conteo de categorías obtenido exitosamente
 *         content:
 *           application/json:
 *             schema:
 *               $ref: '#/components/responses/success'
 *       403:
 *         description: Error de autenticación
 *         content:
 *           application/json:
 *             schema:
 *               $ref: '#/components/responses/UnauthorizedError'
 *       500:
 *         description: Error en el servidor
 *         content:
 *           application/json:
 *             schema:
 *               $ref: '#/components/responses/InternalServerError'
 *     security:
 *       - bearerAuth: []
 */
router.get('/CategoriesCount', authMiddleware, CategoriesCount)

module.exports = router
