const { check } = require('express-validator')
const validateResults = require('../../utils/handleValidators')
const User = require('../../models/general/auth')
const { UUID } = require('sequelize')

const validatorLogin = [
    check('user').exists().notEmpty(),
    check('password').exists().notEmpty().isLength({ min: 6, max: 20 }),
    (req, res, next) => {
        return validateResults(req, res, next)
    },
]

const validatorRegisterUser = [
    check('role_guid')
        .isUUID()
        .exists()
        .notEmpty()
        .withMessage('El GUID del rol es requerido.'),
    check('first_name')
        .isString()
        .exists()
        .notEmpty()
        .isLength({ min: 1, max: 255 })
        .withMessage('El nombre es requerido'),
    check('last_name')
        .isString()
        .exists()
        .isLength({ min: 1, max: 255 })
        .withMessage('El apellido es requerido'),
    check('username')
        .isString()
        .exists()
        .notEmpty()
        .isLength({ min: 1, max: 255 })
        .custom(async (value) => {
            // Verificar si el correo ya existe en la base de datos
            const user = await User.findOne({
                where: { username: value },
            })
            if (user) {
                throw new Error('El nombre de usuario ya está en uso')
            }
        }),
    check('email')
        .isEmail()
        .exists()
        .notEmpty()
        .custom(async (value) => {
            // Verificar si el correo ya existe en la base de datos
            const user = await User.findOne({
                where: { email: value },
            })
            if (user) {
                throw new Error('El correo ya está en uso')
            }
        }),
    check('password').exists().notEmpty().isLength({ min: 6, max: 100 }),
    check('phone')
        .isNumeric()
        .exists()
        .notEmpty()
        .isLength({ min: 6, max: 20 })
        .custom(async (value) => {
            // Verificar si el correo ya existe en la base de datos
            const user = await User.findOne({
                where: { phone: value },
            })
            if (user) {
                throw new Error('El teléfono ya está en uso')
            }
        }),
    check('country').isString().exists().withMessage('El país es requerido'),
    check('city').isString().exists().withMessage('La ciudad es requerida'),
    check('state').isString().exists().withMessage('El estado es requerido'),
    (req, res, next) => {
        return validateResults(req, res, next)
    },
]

const validatorGoogle = [
    check('token').exists().notEmpty().withMessage('El token es requerido.'),
]

module.exports = {
    validatorLogin,
    validatorRegisterUser,
    validatorGoogle,
}
