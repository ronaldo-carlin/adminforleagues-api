module.exports = {
    async up(queryInterface, Sequelize) {
        // entidad para los planes
        await queryInterface.createTable('plans', {
            // identificador del registro
            id: {
                type: Sequelize.INTEGER,
                primaryKey: true,
                autoIncrement: true,
                allowNull: false,
            },
            // identificador del plan
            guid: {
                type: Sequelize.UUID,
                defaultValue: Sequelize.UUIDV4, // Genera un UUID por defecto
                unique: true,
                allowNull: false,
            },
            // nombre del plan
            name: {
                type: Sequelize.TEXT,
            },
            // descripcion del plan
            description: {
                type: Sequelize.TEXT,
            },
            // precio del plan
            price: {
                type: Sequelize.DECIMAL,
            },
            // cantidad de dias de prueba del plan
            trial_days: {
                type: Sequelize.INTEGER,
            },
            // cantidad de dias de duracion del plan
            duration_days: {
                type: Sequelize.INTEGER,
            },
            // Booleano que indica si el plan incluye una URL personalizada.
            url: {
                type: Sequelize.TEXT,
            },
            // estado del registro
            is_active: {
                type: Sequelize.BOOLEAN,
                defaultValue: 1, // valor por defecto
            },
            // usuario que creo el registro
            created_by: {
                type: Sequelize.UUID,
            },
            // fecha de creacion
            created_at: {
                type: Sequelize.DATE,
                defaultValue: Sequelize.literal('CURRENT_TIMESTAMP'),
            },
            // usuario que actualizo el registro
            updated_by: {
                type: Sequelize.UUID,
            },
            // fecha de actualizacion
            updated_at: {
                type: Sequelize.DATE,
            },
            // usuario que elimino el registro
            deleted_by: {
                type: Sequelize.UUID,
            },
            // fecha de eliminacion
            deleted_at: {
                type: Sequelize.DATE,
            },
        })

        // Agregar índice único después de crear la tabla
        await queryInterface.addIndex('plans', ['guid'], {
            unique: true,
            name: 'uniq_plans_guid',
        })
    },

    async down(queryInterface, Sequelize) {
        await queryInterface.removeIndex('plans', 'uniq_plans_guid')
        await queryInterface.dropTable('plans')
    },
}
