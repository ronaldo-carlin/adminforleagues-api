const { check, validationResult } = require('express-validator')

// Validación para crear una nueva categoría
const validatorCreateCategory = () => {
    return [
        async (req, res, next) => {
            await check('sport_guid')
                .isUUID()
                .withMessage('El GUID del deporte es requerido.')
                .run(req)
            await check('name')
                .notEmpty()
                .withMessage('El nombre de la categoría es requerido.')
                .run(req)
            await check('description')
                .optional()
                .isString()
                .withMessage('La descripción debe ser una cadena de texto.')
                .run(req)
            await check('logo_path')
                .optional()
                .isString()
                .withMessage('La ruta del logo debe ser una cadena de texto.')
                .run(req)
            await check('color')
                .optional()
                .isString()
                .withMessage('El color debe ser una cadena de texto.')
                .run(req)
            await check('age_min')
                .optional()
                .isInt({ min: 0 })
                .withMessage('La edad mínima debe ser un número entero.')
                .run(req)
            await check('age_max')
                .optional()
                .isInt({ min: 0 })
                .withMessage('La edad máxima debe ser un número entero.')
                .run(req)
            await check('gander')
                .optional()
                .isString()
                .withMessage('El género debe ser una cadena de texto.')
                .run(req)
            await check('level')
                .optional()
                .isString()
                .withMessage('El nivel debe ser una cadena de texto.')
                .run(req)

            const errors = validationResult(req)
            if (!errors.isEmpty()) {
                return res.status(400).json({ errors: errors.array() })
            }

            next()
        },
    ]
}

// Validación para obtener una categoría por GUID
const validatorCategory = () => {
    return [
        async (req, res, next) => {
            await check('guid')
                .isUUID()
                .withMessage('El GUID de la categoría es requerido.')
                .run(req)

            const errors = validationResult(req)
            if (!errors.isEmpty()) {
                return res.status(400).json({ errors: errors.array() })
            }

            next()
        },
    ]
}

// Validación para obtener una categoría por GUID
const validatorSport = () => {
    return [
        async (req, res, next) => {
            await check('guid')
                .isUUID()
                .withMessage('El GUID del deporte es requerido.')
                .run(req)

            const errors = validationResult(req)
            if (!errors.isEmpty()) {
                return res.status(400).json({ errors: errors.array() })
            }

            next()
        },
    ]
}

// Validación para actualizar una categoría
const validatorUpdateCategory = () => {
    return [
        async (req, res, next) => {
            await check('guid')
                .isUUID()
                .withMessage('El GUID de la categoría es requerido.')
                .run(req)
            await check('sport_guid')
                .optional()
                .isUUID()
                .withMessage('El GUID del deporte debe ser válido.')
                .run(req)
            await check('name')
                .optional()
                .notEmpty()
                .withMessage('El nombre de la categoría es requerido.')
                .run(req)
            await check('description')
                .optional()
                .isString()
                .withMessage('La descripción debe ser una cadena de texto.')
                .run(req)
            await check('logo_path')
                .optional()
                .isString()
                .withMessage('La ruta del logo debe ser una cadena de texto.')
                .run(req)
            await check('color')
                .optional()
                .isString()
                .withMessage('El color debe ser una cadena de texto.')
                .run(req)
            await check('age_min')
                .optional()
                .isInt({ min: 0 })
                .withMessage('La edad mínima debe ser un número entero.')
                .run(req)
            await check('age_max')
                .optional()
                .isInt({ min: 0 })
                .withMessage('La edad máxima debe ser un número entero.')
                .run(req)
            await check('gander')
                .optional()
                .isString()
                .withMessage('El género debe ser una cadena de texto.')
                .run(req)
            await check('level')
                .optional()
                .isString()
                .withMessage('El nivel debe ser una cadena de texto.')
                .run(req)

            const errors = validationResult(req)
            if (!errors.isEmpty()) {
                return res.status(400).json({ errors: errors.array() })
            }

            next()
        },
    ]
}

// Validación para eliminar una categoría
const validatorDeleteCategory = () => {
    return [
        async (req, res, next) => {
            await check('guid')
                .isUUID()
                .withMessage('El GUID de la categoría es requerido.')
                .run(req)

            const errors = validationResult(req)
            if (!errors.isEmpty()) {
                return res.status(400).json({ errors: errors.array() })
            }

            next()
        },
    ]
}

// Validación para deshabilitar una categoría
const validatorDisableCategory = () => {
    return [
        async (req, res, next) => {
            await check('guid')
                .isUUID()
                .withMessage('El GUID de la categoría es requerido.')
                .run(req)

            const errors = validationResult(req)
            if (!errors.isEmpty()) {
                return res.status(400).json({ errors: errors.array() })
            }

            next()
        },
    ]
}

// Validación para habilitar una categoría
const validatorEnableCategory = () => {
    return [
        async (req, res, next) => {
            await check('guid')
                .isUUID()
                .withMessage('El GUID de la categoría es requerido.')
                .run(req)

            const errors = validationResult(req)
            if (!errors.isEmpty()) {
                return res.status(400).json({ errors: errors.array() })
            }

            next()
        },
    ]
}

module.exports = {
    validatorCreateCategory,
    validatorCategory,
    validatorUpdateCategory,
    validatorDeleteCategory,
    validatorDisableCategory,
    validatorEnableCategory,
    validatorSport,
}
