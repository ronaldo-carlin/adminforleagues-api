// Importar Sequelize
const { DataTypes } = require('sequelize')

// Definir la función factory para el modelo PaymentsTenantModel
const PaymentsTenantModelFactory = (sequelize) => {
    const PaymentsTenantModel = sequelize.define(
        'payments',
        {
            id: {
                type: DataTypes.INTEGER,
                primaryKey: true,
                autoIncrement: true,
            },
            guid: {
                type: DataTypes.UUID,
                primaryKey: true,
                defaultValue: DataTypes.UUIDV4,
                unique: true,
            },
            user_guid: {
                type: DataTypes.TEXT,
            },
            plan_guid: {
                type: DataTypes.TEXT,
            },
            license_guid: {
                type: DataTypes.TEXT,
            },
            email: {
                type: DataTypes.TEXT,
            },
            amount: {
                type: DataTypes.DOUBLE,
            },
            currency: {
                type: DataTypes.TEXT,
            },
            currency_icon: {
                type: DataTypes.TEXT,
            },
            currency_code: {
                type: DataTypes.TEXT,
            },
            method: {
                type: DataTypes.TEXT,
            },
            transaction: {
                type: DataTypes.TEXT,
            },
            reference: {
                type: DataTypes.TEXT,
            },
            concept: {
                type: DataTypes.TEXT,
            },
            card: {
                type: DataTypes.TEXT,
            },
            bank: {
                type: DataTypes.TEXT,
            },
            message: {
                type: DataTypes.TEXT,
            },
            status: {
                type: DataTypes.TEXT,
            },
            is_active: {
                type: DataTypes.INTEGER,
                defaultValue: 1,
            },
            created_by: {
                type: DataTypes.UUID,
            },
            updated_by: {
                type: DataTypes.UUID,
            },
            deleted_by: {
                type: DataTypes.UUID,
            },
            created_at: {
                type: DataTypes.DATE,
            },
            updated_at: {
                type: DataTypes.DATE,
            },
            deleted_at: {
                type: DataTypes.DATE,
            },
        },
        {
            tableName: 'payments',
            timestamps: false,
        }
    )

    return PaymentsTenantModel
}

module.exports = PaymentsTenantModelFactory
