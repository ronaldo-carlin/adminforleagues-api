const { check, validationResult } = require('express-validator')

// Validación para crear un nuevo jugador
const validatorCreatePlayer = () => {
    return [
        async (req, res, next) => {
            await check('team_guid')
                .optional()
                .isUUID()
                .withMessage('El GUID del equipo debe ser un UUID válido.')
                .run(req)
            await check('first_name')
                .notEmpty()
                .withMessage('El nombre es requerido.')
                .run(req)
            await check('last_name')
                .notEmpty()
                .withMessage('El apellido es requerido.')
                .run(req)
            await check('age')
                .optional()
                .isInt({ min: 0 })
                .withMessage('La edad debe ser un número entero positivo.')
                .run(req)
            await check('birthdate')
                .optional()
                .isISO8601()
                .withMessage(
                    'La fecha de nacimiento debe ser una fecha válida.'
                )
                .run(req)
            await check('dorsal_number')
                .optional()
                .isInt({ min: 0 })
                .withMessage(
                    'El número dorsal debe ser un número entero positivo.'
                )
                .run(req)
            await check('position')
                .optional()
                .isString()
                .withMessage('La posición debe ser una cadena de texto.')
                .run(req)
            await check('height')
                .optional()
                .isInt({ min: 0 })
                .withMessage('La altura debe ser una entero de texto.')
                .run(req)
            await check('weight')
                .optional()
                .isInt({ min: 0 })
                .withMessage('El peso debe ser una entero de texto.')
                .run(req)
            await check('skilled_foot')
                .optional()
                .isString()
                .withMessage('El pie hábil debe ser una cadena de texto.')
                .run(req)
            await check('personality_traits')
                .optional()
                .isString()
                .withMessage(
                    'Los rasgos de personalidad deben ser una cadena de texto.'
                )
                .run(req)
            await check('image_path')
                .optional()
                .isString()
                .withMessage(
                    'La ruta de la imagen debe ser una cadena de texto.'
                )
                .run(req)
            await check('phone_number')
                .optional()
                .isString()
                .withMessage(
                    'El número de teléfono debe ser una cadena de texto.'
                )
                .run(req)
            await check('country')
                .optional()
                .isString()
                .withMessage('El país debe ser una cadena de texto.')
                .run(req)
            await check('city')
                .optional()
                .isString()
                .withMessage('La ciudad debe ser una cadena de texto.')
                .run(req)
            await check('state')
                .optional()
                .isString()
                .withMessage('El estado debe ser una cadena de texto.')
                .run(req)
            await check('nacionality')
                .optional()
                .isString()
                .withMessage('La nacionalidad debe ser una cadena de texto.')
                .run(req)
            await check('facebook')
                .optional()
                .isURL()
                .withMessage('El enlace de Facebook debe ser una URL válida.')
                .run(req)
            await check('instagram')
                .optional()
                .isURL()
                .withMessage('El enlace de Instagram debe ser una URL válida.')
                .run(req)

            const errors = validationResult(req)
            if (!errors.isEmpty()) {
                return res.status(400).json({ errors: errors.array() })
            }

            next()
        },
    ]
}

// Validación para obtener un jugador por GUID
const validatorPlayer = () => {
    return [
        async (req, res, next) => {
            await check('guid')
                .isUUID()
                .withMessage(
                    'El GUID del jugador es requerido y debe ser un UUID válido.'
                )
                .run(req)

            const errors = validationResult(req)
            if (!errors.isEmpty()) {
                return res.status(400).json({ errors: errors.array() })
            }

            next()
        },
    ]
}

// Validación para actualizar un jugador
const validatorUpdatePlayer = () => {
    return [
        async (req, res, next) => {
            await check('guid')
                .isUUID()
                .withMessage(
                    'El GUID del jugador es requerido y debe ser un UUID válido.'
                )
                .run(req)
            await check('team_guid')
                .optional()
                .isUUID()
                .withMessage('El GUID del equipo debe ser un UUID válido.')
                .run(req)
            await check('first_name')
                .optional()
                .notEmpty()
                .withMessage('El nombre es requerido.')
                .run(req)
            await check('last_name')
                .optional()
                .notEmpty()
                .withMessage('El apellido es requerido.')
                .run(req)
            await check('age')
                .optional()
                .isInt({ min: 0 })
                .withMessage('La edad debe ser un número entero positivo.')
                .run(req)
            await check('birthdate')
                .optional()
                .isISO8601()
                .withMessage(
                    'La fecha de nacimiento debe ser una fecha válida.'
                )
                .run(req)
            await check('dorsal_number')
                .optional()
                .isInt({ min: 0 })
                .withMessage(
                    'El número dorsal debe ser un número entero positivo.'
                )
                .run(req)
            await check('position')
                .optional()
                .isString()
                .withMessage('La posición debe ser una cadena de texto.')
                .run(req)
            await check('height')
                .optional()
                .isInt({ min: 0 })
                .withMessage('La altura debe ser una entero de texto.')
                .run(req)
            await check('weight')
                .optional()
                .isInt({ min: 0 })
                .withMessage('El peso debe ser una entero de texto.')
                .run(req)
            await check('skilled_foot')
                .optional()
                .isString()
                .withMessage('El pie hábil debe ser una cadena de texto.')
                .run(req)
            await check('personality_traits')
                .optional()
                .isString()
                .withMessage(
                    'Los rasgos de personalidad deben ser una cadena de texto.'
                )
                .run(req)
            await check('image_path')
                .optional()
                .isString()
                .withMessage(
                    'La ruta de la imagen debe ser una cadena de texto.'
                )
                .run(req)
            await check('phone_number')
                .optional()
                .isString()
                .withMessage(
                    'El número de teléfono debe ser una cadena de texto.'
                )
                .run(req)
            await check('country')
                .optional()
                .isString()
                .withMessage('El país debe ser una cadena de texto.')
                .run(req)
            await check('city')
                .optional()
                .isString()
                .withMessage('La ciudad debe ser una cadena de texto.')
                .run(req)
            await check('state')
                .optional()
                .isString()
                .withMessage('El estado debe ser una cadena de texto.')
                .run(req)
            await check('nacionality')
                .optional()
                .isString()
                .withMessage('La nacionalidad debe ser una cadena de texto.')
                .run(req)
            await check('facebook')
                .optional()
                .isURL()
                .withMessage('El enlace de Facebook debe ser una URL válida.')
                .run(req)
            await check('instagram')
                .optional()
                .isURL()
                .withMessage('El enlace de Instagram debe ser una URL válida.')
                .run(req)

            const errors = validationResult(req)
            if (!errors.isEmpty()) {
                return res.status(400).json({ errors: errors.array() })
            }

            next()
        },
    ]
}

// Validación para eliminar un jugador
const validatorDeletePlayer = () => {
    return [
        async (req, res, next) => {
            await check('guid')
                .isUUID()
                .withMessage(
                    'El GUID del jugador es requerido y debe ser un UUID válido.'
                )
                .run(req)

            const errors = validationResult(req)
            if (!errors.isEmpty()) {
                return res.status(400).json({ errors: errors.array() })
            }

            next()
        },
    ]
}

// Validación para deshabilitar un jugador
const validatorDisablePlayer = () => {
    return [
        async (req, res, next) => {
            await check('guid')
                .isUUID()
                .withMessage(
                    'El GUID del jugador es requerido y debe ser un UUID válido.'
                )
                .run(req)

            const errors = validationResult(req)
            if (!errors.isEmpty()) {
                return res.status(400).json({ errors: errors.array() })
            }

            next()
        },
    ]
}

// Validación para habilitar un jugador
const validatorEnablePlayer = () => {
    return [
        async (req, res, next) => {
            await check('guid')
                .isUUID()
                .withMessage(
                    'El GUID del jugador es requerido y debe ser un UUID válido.'
                )
                .run(req)

            const errors = validationResult(req)
            if (!errors.isEmpty()) {
                return res.status(400).json({ errors: errors.array() })
            }

            next()
        },
    ]
}

module.exports = {
    validatorCreatePlayer,
    validatorPlayer,
    validatorUpdatePlayer,
    validatorDeletePlayer,
    validatorDisablePlayer,
    validatorEnablePlayer,
}
