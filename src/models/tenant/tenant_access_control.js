// Importar Sequelize
const { DataTypes } = require('sequelize')
const RolesTenantModelFactory = require('./tenant_roles')
const ModulesTenantModelFactory = require('./tenant_modules')
const PermissionsTenantModelFactory = require('./tenant_Permissions')

const AccessControlTenantModelFactory = (sequelize) => {
    // Importar el modelo de roles
    const RolesTenantModel = RolesTenantModelFactory(sequelize)
    // Importar el modelo de modulos
    const ModulesTenantModel = ModulesTenantModelFactory(sequelize)
    // Importar el modelo de permisos
    const PermissionsTenantModel = PermissionsTenantModelFactory(sequelize)

    const AccessControlTenantModel = sequelize.define(
        'access_control',
        {
            id: {
                type: DataTypes.INTEGER,
                primaryKey: true,
                autoIncrement: true,
            },
            guid: {
                type: DataTypes.UUID,
                primaryKey: true,
                defaultValue: DataTypes.UUIDV4,
                unique: true,
            },
            role_guid: {
                type: DataTypes.UUID,
            },
            module_guid: {
                type: DataTypes.UUID,
            },
            permissions_guid: {
                type: DataTypes.UUID,
            },
            is_active: {
                type: DataTypes.BOOLEAN,
                defaultValue: true,
            },
            created_by: {
                type: DataTypes.UUID,
            },
            updated_by: {
                type: DataTypes.UUID,
            },
            deleted_by: {
                type: DataTypes.UUID,
            },
            created_at: {
                type: DataTypes.DATE,
            },
            updated_at: {
                type: DataTypes.DATE,
            },
            deleted_at: {
                type: DataTypes.DATE,
            },
        },
        {
            tableName: 'access_control',
            timestamps: false,
            defaultScope: {
                attributes: {
                    exclude: [
                        'created_by',
                        'created_at',
                        'updated_by',
                        'updated_at',
                        'deleted_by',
                        'deleted_at',
                    ],
                },
                include: [
                    {
                        model: RolesTenantModel,
                        as: 'role',
                        where: { is_active: 1 },
                        attributes: ['guid', 'name'],
                    },
                    {
                        model: ModulesTenantModel,
                        as: 'module',
                        where: { is_active: 1 },
                        attributes: ['guid', 'name'],
                    },
                    {
                        model: PermissionsTenantModel,
                        as: 'permission',
                        where: { is_active: 1 },
                        attributes: ['guid', 'name'],
                    },
                ],
            },
        }
    )

    AccessControlTenantModel.belongsTo(RolesTenantModel, {
        foreignKey: 'role_guid',
        targetKey: 'guid',
        as: 'role',
    })

    AccessControlTenantModel.belongsTo(ModulesTenantModel, {
        foreignKey: 'module_guid',
        targetKey: 'guid',
        as: 'module',
    })

    AccessControlTenantModel.belongsTo(PermissionsTenantModel, {
        foreignKey: 'permissions_guid',
        targetKey: 'guid',
        as: 'permission',
    })

    return AccessControlTenantModel
}
module.exports = AccessControlTenantModelFactory
