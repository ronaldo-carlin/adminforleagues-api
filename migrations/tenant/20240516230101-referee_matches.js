'use strict'

/** @type {import('sequelize-cli').Migration} */
module.exports = {
    async up(queryInterface, Sequelize) {
        // entidad para los arbitros de los partidos
        await queryInterface.createTable('referee_matches', {
            // id del registro
            id: {
                type: Sequelize.INTEGER,
                primaryKey: true,
                autoIncrement: true,
                allowNull: false,
            },
            // guid del registro
            guid: {
                type: Sequelize.UUID,
                defaultValue: Sequelize.UUIDV4,
                unique: true,
                allowNull: false,
            },
            // id del usuario con el rol de arbitro
            user_guid: {
                type: Sequelize.UUID,
            },
            // id del partido al que pertenece el arbitro
            match_guid: {
                type: Sequelize.UUID,
            },
            // estado del registro
            is_active: {
                type: Sequelize.BOOLEAN,
                defaultValue: 1, // valor por defecto
            },
            // usuario que creo el registro
            created_by: {
                type: Sequelize.UUID,
            },
            // fecha de creacion
            created_at: {
                type: Sequelize.DATE,
                defaultValue: Sequelize.literal('CURRENT_TIMESTAMP'),
            },
            // usuario que actualizo el registro
            updated_by: {
                type: Sequelize.UUID,
            },
            // fecha de actualizacion
            updated_at: {
                type: Sequelize.DATE,
            },
            // usuario que elimino el registro
            deleted_by: {
                type: Sequelize.UUID,
            },
            // fecha de eliminacion
            deleted_at: {
                type: Sequelize.DATE,
            },
        })

        // indeces
        await queryInterface.addIndex('referee_matches', ['guid'], {
            unique: true,
            name: 'unique_referee_matches_guid',
        })

        await queryInterface.addIndex('referee_matches', ['user_guid'], {
            name: 'idx_user_guid',
        })

        await queryInterface.addIndex('referee_matches', ['match_guid'], {
            name: 'idx_match_guid',
        })

        // llaves foraneas
        await queryInterface.addConstraint('referee_matches', {
            fields: ['user_guid'],
            type: 'foreign key',
            name: 'fk_referee_user',
            references: {
                table: 'users',
                field: 'guid',
            },
            onDelete: 'NO ACTION',
            onUpdate: 'CASCADE',
        })

        await queryInterface.addConstraint('referee_matches', {
            fields: ['match_guid'],
            type: 'foreign key',
            name: 'fk_referee_match',
            references: {
                table: 'matches',
                field: 'guid',
            },
            onDelete: 'NO ACTION',
            onUpdate: 'CASCADE',
        })
    },

    async down(queryInterface, Sequelize) {
        await queryInterface.removeConstraint(
            'referee_matches',
            'fk_referee_user'
        )
        await queryInterface.removeConstraint(
            'referee_matches',
            'fk_referee_match'
        )
        await queryInterface.removeIndex(
            'referee_matches',
            'unique_referee_matches_guid'
        )
        await queryInterface.removeIndex('referee_matches', 'idx_user_guid')
        await queryInterface.removeIndex('referee_matches', 'idx_match_guid')
        await queryInterface.dropTable('referee_matches')
    },
}
