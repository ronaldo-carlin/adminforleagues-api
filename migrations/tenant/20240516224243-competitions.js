module.exports = {
    async up(queryInterface, Sequelize) {
        // entidad para torneos
        await queryInterface.createTable('competitions', {
            // id del registro
            id: {
                type: Sequelize.INTEGER,
                primaryKey: true,
                autoIncrement: true,
                allowNull: false,
            },
            // identificador del torneo
            guid: {
                type: Sequelize.UUID,
                defaultValue: Sequelize.UUIDV4, // Genera un UUID por defecto
                unique: true,
                allowNull: false,
            },
            // identificador de la categoria del torneo
            category_guid: {
                type: Sequelize.UUID,
            },
            // identificador de la temporada del torneo
            season_guid: {
                type: Sequelize.UUID,
            },
            // tipo de deporte
            sport_guid: {
                type: Sequelize.UUID,
            },
            // tipo de torneo (liguilla, copa, playoff, eliminacion directa, etc.)
            type_competition_guid: {
                type: Sequelize.UUID,
            },
            // nombre del torneo
            name: {
                type: Sequelize.TEXT,
            },
            // descripcion del torneo
            description: {
                type: Sequelize.TEXT,
            },
            // logo del torneo
            logo_path: {
                type: Sequelize.TEXT,
            },
            // cantidad de equipos participantes
            teams: {
                type: Sequelize.INTEGER,
            },
            // cantidad de jugadores por equipo
            players: {
                type: Sequelize.INTEGER,
            },
            // cantidad de jugadores en cancha por equipo
            players_field: {
                type: Sequelize.INTEGER,
            },
            // fecha de inicio del torneo
            start_date: {
                type: Sequelize.DATE,
            },
            // fecha de finalizacion del torneo
            end_date: {
                type: Sequelize.DATE,
            },
            // pagina web del torneo
            site_web: {
                type: Sequelize.TEXT,
            },
            // descripcion del torneo
            description: {
                type: Sequelize.TEXT,
            },
            // Inscripcion
            registration: {
                type: Sequelize.INTEGER,
            },
            // costo de arbitraje
            cost_arbitration: {
                type: Sequelize.INTEGER,
            },
            // pago de arbitraje
            payment_arbitration: {
                type: Sequelize.INTEGER,
            },
            // pago por defecto
            payment_arbitration_default: {
                type: Sequelize.INTEGER,
            },
            // estado del registro
            is_active: {
                type: Sequelize.BOOLEAN,
                defaultValue: 1, // valor por defecto
            },
            // usuario que creo el registro
            created_by: {
                type: Sequelize.UUID,
            },
            // fecha de creacion
            created_at: {
                type: Sequelize.DATE,
                defaultValue: Sequelize.literal('CURRENT_TIMESTAMP'),
            },
            // usuario que actualizo el registro
            updated_by: {
                type: Sequelize.UUID,
            },
            // fecha de actualizacion
            updated_at: {
                type: Sequelize.DATE,
            },
            // usuario que elimino el registro
            deleted_by: {
                type: Sequelize.UUID,
            },
            // fecha de eliminacion
            deleted_at: {
                type: Sequelize.DATE,
            },
        })

        // indices
        await queryInterface.addIndex('competitions', ['guid'], {
            unique: true,
            name: 'uniq_competitions_guid',
        })

        await queryInterface.addIndex('competitions', ['category_guid'], {
            name: 'idx_category_guid',
        })

        await queryInterface.addIndex('competitions', ['season_guid'], {
            name: 'idx_season_guid',
        })

        await queryInterface.addIndex('competitions', ['sport_guid'], {
            name: 'idx_sport_guid',
        })

        await queryInterface.addIndex(
            'competitions',
            ['type_competition_guid'],
            {
                name: 'idx_type_competition_guid',
            }
        )

        // llaves foraneas
        await queryInterface.addConstraint('competitions', {
            fields: ['category_guid'],
            type: 'foreign key',
            name: 'fk_category_competition',
            references: {
                table: 'categories',
                field: 'guid',
            },
            onDelete: 'NO ACTION',
            onUpdate: 'NO ACTION',
        })

        await queryInterface.addConstraint('competitions', {
            fields: ['season_guid'],
            type: 'foreign key',
            name: 'fk_season_competition',
            references: {
                table: 'seasons',
                field: 'guid',
            },
            onDelete: 'NO ACTION',
            onUpdate: 'NO ACTION',
        })

        await queryInterface.addConstraint('competitions', {
            fields: ['sport_guid'],
            type: 'foreign key',
            name: 'fk_sport_competition',
            references: {
                table: 'sports',
                field: 'guid',
            },
            onDelete: 'NO ACTION',
            onUpdate: 'NO ACTION',
        })

        await queryInterface.addConstraint('competitions', {
            fields: ['type_competition_guid'],
            type: 'foreign key',
            name: 'fk_type_competition_competition',
            references: {
                table: 'types_competitions',
                field: 'guid',
            },
            onDelete: 'NO ACTION',
            onUpdate: 'NO ACTION',
        })
    },

    async down(queryInterface, Sequelize) {
        await queryInterface.removeConstraint(
            'competitions',
            'fk_category_competition'
        )
        await queryInterface.removeConstraint(
            'competitions',
            'fk_season_competition'
        )
        await queryInterface.removeConstraint(
            'competitions',
            'fk_sport_competition'
        )
        await queryInterface.removeConstraint(
            'competitions',
            'fk_type_competition_competition'
        )
        await queryInterface.removeIndex(
            'competitions',
            'idx_type_competition_guid'
        )
        await queryInterface.removeIndex('competitions', 'idx_sport_guid')
        await queryInterface.removeIndex('competitions', 'idx_season_guid')
        await queryInterface.removeIndex('competitions', 'idx_category_guid')
        await queryInterface.removeIndex(
            'competitions',
            'uniq_competitions_guid'
        )
        await queryInterface.dropTable('competitions')
    },
}
