const express = require('express')
const router = express.Router()

// Controladores y validadores para access_control
const {
    validatorCreateAccessControl,
    validatorAccessControl,
    validatorUpdateAccessControl,
    validatorDeleteAccessControl,
    validatorDisableAccessControl,
    validatorEnableAccessControl,
    validatorAccessControlsByRol,
} = require('../../validators/tenant/tenant_access_control')

const {
    CreateAccessControl,
    AccessControls,
    AccessControl,
    UpdateAccessControl,
    DeleteAccessControl,
    DisableAccessControl,
    EnableAccessControl,
    AccessControlsAll,
    AccessControlsCount,
    AccessControlsByRol,
} = require('../../controllers/tenant/tenant_access_control')

const authMiddleware = require('../../middleware/session_tenant')

/* Rutas protegidas que requieren autenticación */
/**
 * @swagger
 * tags:
 *   name: Tenant_Access_Controls
 *   description: API para la gestión de controles de acceso
 */

// Ruta para crear un nuevo control de acceso
/**
 * @swagger
 * /tenant_access_controls/CreateAccessControl:
 *   post:
 *     summary: Crear un nuevo control de acceso
 *     description: Esta ruta permite crear un nuevo control de acceso con los detalles especificados.
 *     tags: [Tenant_Access_Controls]
 *     requestBody:
 *       required: true
 *       content:
 *         application/json:
 *           schema:
 *             type: object
 *             properties:
 *               role_guid:
 *                 type: uuid
 *                 example: 495db854-849b-41cd-b6b9-9c84824c92ba
 *               module_guid:
 *                 type: uuid
 *                 example: 495db854-849b-41cd-b6b9-9c84824c92ba
 *               Permissions_guid:
 *                 type: uuid
 *                 example: 495db854-849b-41cd-b6b9-9c84824c92ba
 *     responses:
 *       201:
 *         description: Control de acceso creado exitosamente
 *         content:
 *           application/json:
 *             schema:
 *               $ref: '#/components/responses/success'
 *       400:
 *         description: Error en los datos enviados
 *         content:
 *           application/json:
 *             schema:
 *               $ref: '#/components/responses/BadRequest'
 *       403:
 *         description: Error de autenticación
 *         content:
 *           application/json:
 *             schema:
 *               $ref: '#/components/responses/UnauthorizedError'
 *       409:
 *         description: El control de acceso ya existe
 *         content:
 *           application/json:
 *             schema:
 *               $ref: '#/components/responses/ConflictError'
 *       500:
 *         description: Error en el servidor
 *         content:
 *           application/json:
 *             schema:
 *               $ref: '#/components/responses/InternalServerError'
 *     security:
 *        - bearerAuth: []
 */
router.post(
    '/CreateAccessControl',
    authMiddleware,
    validatorCreateAccessControl(),
    CreateAccessControl
)

// Ruta para obtener todos los controles de acceso activos
/**
 * @swagger
 * /tenant_access_controls/AccessControls:
 *   get:
 *     summary: Obtener todos los controles de acceso activos
 *     tags: [Tenant_Access_Controls]
 *     responses:
 *       200:
 *         description: Controles de acceso obtenidos exitosamente
 *         content:
 *           application/json:
 *             schema:
 *               $ref: '#/components/responses/success'
 *       403:
 *         description: Error de autenticación
 *         content:
 *           application/json:
 *             schema:
 *               $ref: '#/components/responses/UnauthorizedError'
 *       500:
 *         description: Error en el servidor
 *         content:
 *           application/json:
 *             schema:
 *               $ref: '#/components/responses/InternalServerError'
 *     security:
 *        - bearerAuth: []
 */
router.get('/AccessControls', authMiddleware, AccessControls)

// Ruta para obtener un control de acceso por GUID
/**
 * @swagger
 * /tenant_access_controls/AccessControl/{guid}:
 *   get:
 *     summary: Obtener un control de acceso por GUID
 *     tags: [Tenant_Access_Controls]
 *     parameters:
 *       - in: path
 *         name: guid
 *         schema:
 *           type: string
 *           format: uuid
 *         required: true
 *         description: El GUID del control de acceso
 *     responses:
 *       200:
 *         description: Control de acceso obtenido exitosamente
 *         content:
 *           application/json:
 *             schema:
 *               $ref: '#/components/responses/success'
 *       403:
 *         description: Error de autenticación
 *         content:
 *           application/json:
 *             schema:
 *               $ref: '#/components/responses/UnauthorizedError'
 *       404:
 *         description: Control de acceso no encontrado
 *         content:
 *           application/json:
 *             schema:
 *               $ref: '#/components/responses/NotFoundError'
 *       500:
 *         description: Error en el servidor
 *         content:
 *           application/json:
 *             schema:
 *               $ref: '#/components/responses/InternalServerError'
 *     security:
 *        - bearerAuth: []
 */
router.get(
    '/AccessControl/:guid',
    authMiddleware,
    validatorAccessControl(),
    AccessControl
)

// Ruta para actualizar un control de acceso
/**
 * @swagger
 * /tenant_access_controls/UpdateAccessControl/{guid}:
 *   patch:
 *     summary: Actualizar un control de acceso
 *     tags: [Tenant_Access_Controls]
 *     requestBody:
 *       required: true
 *       content:
 *         application/json:
 *           schema:
 *             type: object
 *             properties:
 *               name:
 *                 type: string
 *                 example: acceso_admin
 *               description:
 *                 type: string
 *                 example: Control de acceso para administradores
 *     responses:
 *       200:
 *         description: Control de acceso actualizado exitosamente
 *         content:
 *           application/json:
 *             schema:
 *               $ref: '#/components/responses/success'
 *       400:
 *         description: Error en los datos enviados
 *         content:
 *           application/json:
 *             schema:
 *               $ref: '#/components/responses/BadRequest'
 *       403:
 *         description: Error de autenticación
 *         content:
 *           application/json:
 *             schema:
 *               $ref: '#/components/responses/UnauthorizedError'
 *       404:
 *         description: Control de acceso no encontrado
 *         content:
 *           application/json:
 *             schema:
 *               $ref: '#/components/responses/NotFoundError'
 *       500:
 *         description: Error en el servidor
 *         content:
 *           application/json:
 *             schema:
 *               $ref: '#/components/responses/InternalServerError'
 *     security:
 *        - bearerAuth: []
 */
router.patch(
    '/UpdateAccessControl/:guid',
    authMiddleware,
    validatorUpdateAccessControl(),
    UpdateAccessControl
)

// Ruta para eliminar un control de acceso
/**
 * @swagger
 * /tenant_access_controls/DeleteAccessControl/{guid}:
 *   delete:
 *     summary: Eliminar un control de acceso
 *     tags: [Tenant_Access_Controls]
 *     parameters:
 *       - in: path
 *         name: guid
 *         schema:
 *           type: string
 *           format: uuid
 *         required: true
 *         description: El GUID del control de acceso
 *     responses:
 *       200:
 *         description: Control de acceso eliminado exitosamente
 *         content:
 *           application/json:
 *             schema:
 *               $ref: '#/components/responses/success'
 *       400:
 *         description: Error en los datos enviados
 *         content:
 *           application/json:
 *             schema:
 *               $ref: '#/components/responses/BadRequest'
 *       403:
 *         description: Error de autenticación
 *         content:
 *           application/json:
 *             schema:
 *               $ref: '#/components/responses/UnauthorizedError'
 *       404:
 *         description: Control de acceso no encontrado
 *         content:
 *           application/json:
 *             schema:
 *               $ref: '#/components/responses/NotFoundError'
 *       500:
 *         description: Error en el servidor
 *         content:
 *           application/json:
 *             schema:
 *               $ref: '#/components/responses/InternalServerError'
 *     security:
 *        - bearerAuth: []
 */
router.delete(
    '/DeleteAccessControl/:guid',
    authMiddleware,
    validatorDeleteAccessControl(),
    DeleteAccessControl
)

// Ruta para deshabilitar un control de acceso
/**
 * @swagger
 * /tenant_access_controls/DisableAccessControl/{guid}:
 *   patch:
 *     summary: Deshabilitar un control de acceso
 *     tags: [Tenant_Access_Controls]
 *     requestBody:
 *       required: true
 *       content:
 *         application/json:
 *           schema:
 *             type: object
 *             properties:
 *               guid:
 *                 type: uuid
 *                 example: 495db854-849b-41cd-b6b9-9c84824c92ba
 *     responses:
 *       200:
 *         description: Control de acceso deshabilitado exitosamente
 *         content:
 *           application/json:
 *             schema:
 *               $ref: '#/components/responses/success'
 *       400:
 *         description: Error en los datos enviados
 *         content:
 *           application/json:
 *             schema:
 *               $ref: '#/components/responses/BadRequest'
 *       403:
 *         description: Error de autenticación
 *         content:
 *           application/json:
 *             schema:
 *               $ref: '#/components/responses/UnauthorizedError'
 *       404:
 *         description: Control de acceso no encontrado
 *         content:
 *           application/json:
 *             schema:
 *               $ref: '#/components/responses/NotFoundError'
 *       500:
 *         description: Error en el servidor
 *         content:
 *           application/json:
 *             schema:
 *               $ref: '#/components/responses/InternalServerError'
 *     security:
 *        - bearerAuth: []
 */
router.patch(
    '/DisableAccessControl/:guid',
    authMiddleware,
    validatorDisableAccessControl(),
    DisableAccessControl
)

// Ruta para habilitar un control de acceso
/**
 * @swagger
 * /tenant_access_controls/EnableAccessControl/{guid}:
 *   patch:
 *     summary: Habilitar un control de acceso
 *     tags: [Tenant_Access_Controls]
 *     requestBody:
 *       required: true
 *       content:
 *         application/json:
 *           schema:
 *             type: object
 *             properties:
 *               guid:
 *                 type: uuid
 *                 example: 495db854-849b-41cd-b6b9-9c84824c92ba
 *     responses:
 *       200:
 *         description: Control de acceso habilitado exitosamente
 *         content:
 *           application/json:
 *             schema:
 *               $ref: '#/components/responses/success'
 *       400:
 *         description: Error en los datos enviados
 *         content:
 *           application/json:
 *             schema:
 *               $ref: '#/components/responses/BadRequest'
 *       403:
 *         description: Error de autenticación
 *         content:
 *           application/json:
 *             schema:
 *               $ref: '#/components/responses/UnauthorizedError'
 *       404:
 *         description: Control de acceso no encontrado
 *         content:
 *           application/json:
 *             schema:
 *               $ref: '#/components/responses/NotFoundError'
 *       500:
 *         description: Error en el servidor
 *         content:
 *           application/json:
 *             schema:
 *               $ref: '#/components/responses/InternalServerError'
 *     security:
 *        - bearerAuth: []
 */
router.patch(
    '/EnableAccessControl/:guid',
    authMiddleware,
    validatorEnableAccessControl(),
    EnableAccessControl
)

// Ruta para obtener todos los controles de acceso, incluidos los inactivos
/**
 * @swagger
 * /tenant_access_controls/AccessControlsAll:
 *   get:
 *     summary: Obtener todos los controles de acceso, incluidos los inactivos
 *     tags: [Tenant_Access_Controls]
 *     responses:
 *       200:
 *         description: Controles de acceso obtenidos exitosamente
 *         content:
 *           application/json:
 *             schema:
 *               $ref: '#/components/responses/success'
 *       403:
 *         description: Error de autenticación
 *         content:
 *           application/json:
 *             schema:
 *               $ref: '#/components/responses/UnauthorizedError'
 *       500:
 *         description: Error en el servidor
 *         content:
 *           application/json:
 *             schema:
 *               $ref: '#/components/responses/InternalServerError'
 *     security:
 *        - bearerAuth: []
 */
router.get('/AccessControlsAll', authMiddleware, AccessControlsAll)

// Ruta para contar todos los controles de acceso
/**
 * @swagger
 * /tenant_access_controls/AccessControlsCount:
 *   get:
 *     summary: Contar todos los controles de acceso
 *     tags: [Tenant_Access_Controls]
 *     responses:
 *       200:
 *         description: Número de controles de acceso contado exitosamente
 *         content:
 *           application/json:
 *             schema:
 *               $ref: '#/components/responses/success'
 *       403:
 *         description: Error de autenticación
 *         content:
 *           application/json:
 *             schema:
 *               $ref: '#/components/responses/UnauthorizedError'
 *       500:
 *         description: Error en el servidor
 *         content:
 *           application/json:
 *             schema:
 *               $ref: '#/components/responses/InternalServerError'
 *     security:
 *        - bearerAuth: []
 */
router.get('/AccessControlsCount', authMiddleware, AccessControlsCount)

// Ruta para obtener todos los controles de acceso por rol
/**
 * @swagger
 * /tenant_access_controls/AccessControlsByRol/{guid}:
 *   get:
 *     summary: Obtener todos los controles de acceso por rol
 *     tags: [Tenant_Access_Controls]
 *     parameters:
 *       - in: path
 *         name: guid
 *         schema:
 *           type: string
 *           format: uuid
 *         required: true
 *         description: El GUID del rol
 *     responses:
 *       200:
 *         description: Controles de acceso por rol obtenidos exitosamente
 *         content:
 *           application/json:
 *             schema:
 *               $ref: '#/components/responses/success'
 *       403:
 *         description: Error de autenticación
 *         content:
 *           application/json:
 *             schema:
 *               $ref: '#/components/responses/UnauthorizedError'
 *       404:
 *         description: Rol no encontrado
 *         content:
 *           application/json:
 *             schema:
 *               $ref: '#/components/responses/NotFoundError'
 *       500:
 *         description: Error en el servidor
 *         content:
 *           application/json:
 *             schema:
 *               $ref: '#/components/responses/InternalServerError'
 *     security:
 *        - bearerAuth: []
 */
router.get(
    '/AccessControlsByRol/:guid',
    authMiddleware,
    /* validatorAccessControlsByRol, */
    AccessControlsByRol
)

module.exports = router
