// Importar Sequelize directamente
const { Sequelize } = require('sequelize')
// Importar tu configuración de sequelize
const { sequelize } = require('../../database/mysql')
// Importar el modelo de Tenant
const TenantModel = require('./tenants')
// Importar el modelo de roles
const RolesModel = require('./roles')

const AuthModel = sequelize.define(
    'users',
    {
        id: {
            type: Sequelize.INTEGER,
            primaryKey: true,
            autoIncrement: true,
        },
        guid: {
            type: Sequelize.UUID,
            defaultValue: Sequelize.UUIDV4,
            unique: true,
        },
        tenant_guid: {
            type: Sequelize.UUID,
        },
        role_guid: {
            type: Sequelize.UUID,
        },
        username: {
            type: Sequelize.STRING(256),
        },
        first_name: {
            type: Sequelize.STRING(256),
        },
        last_name: {
            type: Sequelize.STRING(256),
        },
        email: {
            type: Sequelize.TEXT,
            allowNull: false,
            unique: true,
        },
        password: {
            type: Sequelize.TEXT,
        },
        phone: {
            type: Sequelize.STRING(15),
        },
        isVerified: {
            type: Sequelize.BOOLEAN,
        },
        image_path: {
            type: Sequelize.TEXT,
        },
        picture: {
            type: Sequelize.TEXT,
        },
        country: {
            type: Sequelize.STRING(256),
        },
        state: {
            type: Sequelize.STRING(256),
        },
        city: {
            type: Sequelize.STRING(256),
        },
        google_id: {
            type: Sequelize.TEXT,
        },
        facebook_id: {
            type: Sequelize.TEXT,
        },
        two_factor_auth: {
            type: Sequelize.INTEGER,
        },
        weeklyNewsletter: {
            type: Sequelize.INTEGER,
        },
        lifecycleEmails: {
            type: Sequelize.INTEGER,
        },
        promotionalEmails: {
            type: Sequelize.INTEGER,
        },
        productUpdates: {
            type: Sequelize.INTEGER,
        },
        last_login: {
            type: Sequelize.DATE,
        },
        is_active: {
            type: Sequelize.INTEGER,
            defaultValue: 1,
        },
        created_by: {
            type: Sequelize.UUID,
        },
        updated_by: {
            type: Sequelize.UUID,
        },
        deleted_by: {
            type: Sequelize.UUID,
        },
        created_at: {
            type: Sequelize.DATE,
        },
        updated_at: {
            type: Sequelize.DATE,
        },
        deleted_at: {
            type: Sequelize.DATE,
        },
    },
    {
        tableName: 'users',
        timestamps: false,
        defaultScope: {
            attributes: {
                exclude: [
                    'created_by',
                    'created_at',
                    'updated_by',
                    'updated_at',
                    'deleted_by',
                    'deleted_at',
                ],
            },
            include: [
                {
                    model: RolesModel,
                    as: 'role',
                    where: { is_active: 1 },
                    attributes: ['guid', 'name'],
                },
                {
                    model: TenantModel,
                    as: 'tenant',
                    where: { is_active: 1 },
                    required: false,
                    attributes: {
                        exclude: [
                            'id',
                            'is_active',
                            'created_at',
                            'created_by',
                            'updated_at',
                            'updated_by',
                            'deleted_at',
                            'deleted_by',
                        ],
                    },
                },
            ],
        },
    }
)

AuthModel.belongsTo(RolesModel, {
    foreignKey: 'role_guid',
    targetKey: 'guid',
    as: 'role',
})

AuthModel.belongsTo(TenantModel, {
    foreignKey: 'tenant_guid',
    targetKey: 'guid',
    as: 'tenant',
})

module.exports = AuthModel
