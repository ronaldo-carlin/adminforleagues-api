'use strict'

/** @type {import('sequelize-cli').Migration} */
module.exports = {
    async up(queryInterface, Sequelize) {
        // entidad para almacenar las reglas de las categorias
        await queryInterface.createTable('categorie_rules', {
            // id del registro
            id: {
                type: Sequelize.INTEGER,
                primaryKey: true,
                autoIncrement: true,
                allowNull: false,
            },
            // guid del registro
            guid: {
                type: Sequelize.UUID,
                defaultValue: Sequelize.UUIDV4,
                unique: true,
                allowNull: false,
            },
            // identificador de la categoria
            category_guid: {
                type: Sequelize.UUID,
            },
            // titulo de la regla
            title: {
                type: Sequelize.TEXT,
            },
            // descripcion de la regla
            description: {
                type: Sequelize.TEXT,
            },
            // estado del registro
            is_active: {
                type: Sequelize.BOOLEAN,
                defaultValue: 1, // valor por defecto
            },
            // usuario que creo el registro
            created_by: {
                type: Sequelize.UUID,
            },
            // fecha de creacion
            created_at: {
                type: Sequelize.DATE,
                defaultValue: Sequelize.literal('CURRENT_TIMESTAMP'),
            },
            // usuario que actualizo el registro
            updated_by: {
                type: Sequelize.UUID,
            },
            // fecha de actualizacion
            updated_at: {
                type: Sequelize.DATE,
            },
            // usuario que elimino el registro
            deleted_by: {
                type: Sequelize.UUID,
            },
            // fecha de eliminacion
            deleted_at: {
                type: Sequelize.DATE,
            },
        })

        // indices
        await queryInterface.addIndex('categorie_rules', ['guid'], {
            unique: true,
            name: 'unique_categorie_rules_guid',
        })

        await queryInterface.addIndex('categorie_rules', ['category_guid'], {
            name: 'idx_categorie_rules_category_guid',
        })

        // llaves foraneas
        await queryInterface.addConstraint('categorie_rules', {
            fields: ['category_guid'],
            type: 'foreign key',
            name: 'fk_categorie_rules_category_guid',
            references: {
                table: 'categories',
                field: 'guid',
            },
            onUpdate: 'CASCADE',
            onDelete: 'NO ACTION',
        })
    },

    async down(queryInterface, Sequelize) {
        await queryInterface.removeConstraint(
            'categorie_rules',
            'fk_categorie_rules_category_guid'
        )
        await queryInterface.removeIndex(
            'categorie_rules',
            'idx_categorie_rules_category_guid'
        )
        await queryInterface.removeIndex(
            'categorie_rules',
            'unique_categorie_rules_guid'
        )
        await queryInterface.dropTable('categorie_rules')
    },
}
