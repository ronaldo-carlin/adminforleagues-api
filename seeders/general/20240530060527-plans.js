const GUIDsGeneral = require('../guids_general')
module.exports = {
    async up(queryInterface, Sequelize) {
        // Inserta los 5 planes por defecto
        await queryInterface.bulkInsert(
            'plans',
            [
                {
                    guid: GUIDsGeneral.planUnicoGuid,
                    name: 'PLAN UNICO',
                    description:
                        '1 torneo. Temporadas ilimitadas en torneo. Todas las características de la app. Herramientas: administrador web, página web, aplicación móvil. Servicios: soporte técnico, siempre actualizado, aliados comerciales, comunidad sport',
                    price: 150,
                    trial_days: 30,
                    duration_days: 30,
                    url: 'personalizada',
                    is_active: 1, // 1: Activo, 0: Inactivo
                    created_at: new Date(),
                },
                {
                    guid: GUIDsGeneral.planBasicoGuid,
                    name: 'PLAN BASICO',
                    description:
                        '3 torneo. Temporadas ilimitadas en torneo. Todas las características de la app. Herramientas: administrador web, página web, aplicación móvil. Servicios: soporte técnico, siempre actualizado, aliados comerciales, comunidad sport',
                    price: 300,
                    trial_days: 30,
                    duration_days: 30,
                    url: 'personalizada',
                    is_active: 1, // 1: Activo, 0: Inactivo
                    created_at: new Date(),
                },
                {
                    guid: GUIDsGeneral.planIntermedioGuid,
                    name: 'PLAN INTERMEDIO',
                    description:
                        '7 torneo. Temporadas ilimitadas en torneo. Todas las características de la app. Herramientas: administrador web, página web, aplicación móvil. Servicios: soporte técnico, siempre actualizado, aliados comerciales, comunidad sport',
                    price: 500,
                    trial_days: 30,
                    duration_days: 30,
                    url: 'personalizada',
                    is_active: 1, // 1: Activo, 0: Inactivo
                    created_at: new Date(),
                },
                {
                    guid: GUIDsGeneral.planAvanzadoGuid,
                    name: 'PLAN AVANZADO',
                    description:
                        '15 torneo. Temporadas ilimitadas en torneo. Todas las características de la app. Herramientas: administrador web, página web, aplicación móvil. Servicios: soporte técnico, siempre actualizado, aliados comerciales, comunidad sport',
                    price: 900,
                    trial_days: 30,
                    duration_days: 30,
                    url: 'personalizada',
                    is_active: 1, // 1: Activo, 0: Inactivo
                    created_at: new Date(),
                },
                {
                    guid: GUIDsGeneral.planPremiumGuid,
                    name: 'PLAN PREMIUM',
                    description:
                        '30 torneo. Temporadas ilimitadas en torneo. Todas las características de la app. Herramientas: administrador web, página web, aplicación móvil. Servicios: soporte técnico, siempre actualizado, aliados comerciales, comunidad sport',
                    price: 1500,
                    trial_days: 30,
                    duration_days: 30,
                    url: 'personalizada',
                    is_active: 1, // 1: Activo, 0: Inactivo
                    created_at: new Date(),
                },
            ],
            {}
        )
    },

    async down(queryInterface, Sequelize) {
        await queryInterface.bulkDelete('plans', null, {})
    },
}
