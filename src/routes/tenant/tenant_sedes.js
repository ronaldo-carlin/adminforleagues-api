const express = require('express')
const router = express.Router()

// Controladores y validadores para sedes
const {
    validatorCreateSede,
    validatorSede,
    validatorUpdateSede,
    validatorDeleteSede,
    validatorDisableSede,
    validatorEnableSede,
} = require('../../validators/tenant/tenant_sedes')

const {
    CreateSede,
    Sedes,
    Sede,
    UpdateSede,
    DeleteSede,
    DisableSede,
    EnableSede,
    SedesAll,
    SedesCount,
} = require('../../controllers/tenant/tenant_sedes')

const authMiddleware = require('../../middleware/session_tenant')

/* Rutas protegidas que requieren autenticación */
/**
 * @swagger
 * tags:
 *   name: Tenant_Sedes
 *   description: API para la gestión de sedes
 */

// Ruta para crear una nueva sede
/**
 * @swagger
 * /tenant_sedes/CreateSede:
 *   post:
 *     summary: Crear una nueva sede
 *     description: Esta ruta permite crear una nueva sede con los detalles especificados.
 *     tags: [Tenant_Sedes]
 *     requestBody:
 *       required: true
 *       content:
 *         application/json:
 *           schema:
 *             type: object
 *             properties:
 *               name:
 *                 type: string
 *                 example: "Sede Central"
 *               location:
 *                 type: string
 *                 example: "Ciudad de México"
 *     responses:
 *       201:
 *         description: Sede creada exitosamente
 *         content:
 *           application/json:
 *             schema:
 *               $ref: '#/components/responses/success'
 *       400:
 *         description: Error en los datos enviados
 *         content:
 *           application/json:
 *             schema:
 *               $ref: '#/components/responses/BadRequest'
 *       403:
 *         description: Error de autenticación
 *         content:
 *           application/json:
 *             schema:
 *               $ref: '#/components/responses/UnauthorizedError'
 *       409:
 *         description: La sede ya existe
 *         content:
 *           application/json:
 *             schema:
 *               $ref: '#/components/responses/ConflictError'
 *       500:
 *         description: Error en el servidor
 *         content:
 *           application/json:
 *             schema:
 *               $ref: '#/components/responses/InternalServerError'
 *     security:
 *        - bearerAuth: []
 */
router.post('/CreateSede', authMiddleware, validatorCreateSede(), CreateSede)

// Ruta para obtener todas las sedes activas
/**
 * @swagger
 * /tenant_sedes/Sedes:
 *   get:
 *     summary: Obtener todas las sedes activas
 *     tags: [Tenant_Sedes]
 *     responses:
 *       200:
 *         description: Sedes obtenidas exitosamente
 *         content:
 *           application/json:
 *             schema:
 *               $ref: '#/components/responses/success'
 *       403:
 *         description: Error de autenticación
 *         content:
 *           application/json:
 *             schema:
 *               $ref: '#/components/responses/UnauthorizedError'
 *       500:
 *         description: Error en el servidor
 *         content:
 *           application/json:
 *             schema:
 *               $ref: '#/components/responses/InternalServerError'
 *     security:
 *        - bearerAuth: []
 */
router.get('/Sedes', authMiddleware, Sedes)

// Ruta para obtener una sede por GUID
/**
 * @swagger
 * /tenant_sedes/Sede/{guid}:
 *   get:
 *     summary: Obtener una sede por GUID
 *     tags: [Tenant_Sedes]
 *     parameters:
 *       - in: path
 *         name: guid
 *         schema:
 *           type: string
 *           format: uuid
 *         required: true
 *         description: El GUID de la sede
 *     responses:
 *       200:
 *         description: Sede obtenida exitosamente
 *         content:
 *           application/json:
 *             schema:
 *               $ref: '#/components/responses/success'
 *       403:
 *         description: Error de autenticación
 *         content:
 *           application/json:
 *             schema:
 *               $ref: '#/components/responses/UnauthorizedError'
 *       404:
 *         description: Sede no encontrada
 *         content:
 *           application/json:
 *             schema:
 *               $ref: '#/components/responses/NotFoundError'
 *       500:
 *         description: Error en el servidor
 *         content:
 *           application/json:
 *             schema:
 *               $ref: '#/components/responses/InternalServerError'
 *     security:
 *        - bearerAuth: []
 */
router.get('/Sede/:guid', authMiddleware, validatorSede(), Sede)

// Ruta para actualizar una sede
/**
 * @swagger
 * /tenant_sedes/UpdateSede/{guid}:
 *   patch:
 *     summary: Actualizar una sede
 *     tags: [Tenant_Sedes]
 *     requestBody:
 *       required: true
 *       content:
 *         application/json:
 *           schema:
 *             type: object
 *             properties:
 *               name:
 *                 type: string
 *                 example: "Sede Central"
 *               location:
 *                 type: string
 *                 example: "Ciudad de México"
 *     responses:
 *       200:
 *         description: Sede actualizada exitosamente
 *         content:
 *           application/json:
 *             schema:
 *               $ref: '#/components/responses/success'
 *       400:
 *         description: Error en los datos enviados
 *         content:
 *           application/json:
 *             schema:
 *               $ref: '#/components/responses/BadRequest'
 *       403:
 *         description: Error de autenticación
 *         content:
 *           application/json:
 *             schema:
 *               $ref: '#/components/responses/UnauthorizedError'
 *       404:
 *         description: Sede no encontrada
 *         content:
 *           application/json:
 *             schema:
 *               $ref: '#/components/responses/NotFoundError'
 *       500:
 *         description: Error en el servidor
 *         content:
 *           application/json:
 *             schema:
 *               $ref: '#/components/responses/InternalServerError'
 *     security:
 *        - bearerAuth: []
 */
router.patch(
    '/UpdateSede/:guid',
    authMiddleware,
    validatorUpdateSede(),
    UpdateSede
)

// Ruta para eliminar una sede
/**
 * @swagger
 * /tenant_sedes/DeleteSede/{guid}:
 *   delete:
 *     summary: Eliminar una sede
 *     tags: [Tenant_Sedes]
 *     parameters:
 *       - in: path
 *         name: guid
 *         schema:
 *           type: string
 *           format: uuid
 *         required: true
 *         description: El GUID de la sede
 *     responses:
 *       200:
 *         description: Sede eliminada exitosamente
 *         content:
 *           application/json:
 *             schema:
 *               $ref: '#/components/responses/success'
 *       400:
 *         description: Error en los datos enviados
 *         content:
 *           application/json:
 *             schema:
 *               $ref: '#/components/responses/BadRequest'
 *       403:
 *         description: Error de autenticación
 *         content:
 *           application/json:
 *             schema:
 *               $ref: '#/components/responses/UnauthorizedError'
 *       404:
 *         description: Sede no encontrada
 *         content:
 *           application/json:
 *             schema:
 *               $ref: '#/components/responses/NotFoundError'
 *       500:
 *         description: Error en el servidor
 *         content:
 *           application/json:
 *             schema:
 *               $ref: '#/components/responses/InternalServerError'
 *     security:
 *        - bearerAuth: []
 */
router.delete(
    '/DeleteSede/:guid',
    authMiddleware,
    validatorDeleteSede(),
    DeleteSede
)

// Ruta para deshabilitar una sede
/**
 * @swagger
 * /tenant_sedes/DisableSede/{guid}:
 *   patch:
 *     summary: Deshabilitar una sede
 *     tags: [Tenant_Sedes]
 *     requestBody:
 *       required: true
 *       content:
 *         application/json:
 *           schema:
 *             type: object
 *             properties:
 *               guid:
 *                 type: uuid
 *                 example: 495db854-849b-41cd-b6b9-9c84824c92ba
 *     responses:
 *       200:
 *         description: Sede deshabilitada exitosamente
 *         content:
 *           application/json:
 *             schema:
 *               $ref: '#/components/responses/success'
 *       400:
 *         description: Error en los datos enviados
 *         content:
 *           application/json:
 *             schema:
 *               $ref: '#/components/responses/BadRequest'
 *       403:
 *         description: Error de autenticación
 *         content:
 *           application/json:
 *             schema:
 *               $ref: '#/components/responses/UnauthorizedError'
 *       404:
 *         description: Sede no encontrada
 *         content:
 *           application/json:
 *             schema:
 *               $ref: '#/components/responses/NotFoundError'
 *       500:
 *         description: Error en el servidor
 *         content:
 *           application/json:
 *             schema:
 *               $ref: '#/components/responses/InternalServerError'
 *     security:
 *        - bearerAuth: []
 */
router.patch(
    '/DisableSede/:guid',
    authMiddleware,
    validatorDisableSede(),
    DisableSede
)

// Ruta para habilitar una sede
/**
 * @swagger
 * /tenant_sedes/EnableSede/{guid}:
 *   patch:
 *     summary: Habilitar una sede
 *     tags: [Tenant_Sedes]
 *     requestBody:
 *       required: true
 *       content:
 *         application/json:
 *           schema:
 *             type: object
 *             properties:
 *               guid:
 *                 type: uuid
 *                 example: 495db854-849b-41cd-b6b9-9c84824c92ba
 *     responses:
 *       200:
 *         description: Sede habilitada exitosamente
 *         content:
 *           application/json:
 *             schema:
 *               $ref: '#/components/responses/success'
 *       400:
 *         description: Error en los datos enviados
 *         content:
 *           application/json:
 *             schema:
 *               $ref: '#/components/responses/BadRequest'
 *       403:
 *         description: Error de autenticación
 *         content:
 *           application/json:
 *             schema:
 *               $ref: '#/components/responses/UnauthorizedError'
 *       404:
 *         description: Sede no encontrada
 *         content:
 *           application/json:
 *             schema:
 *               $ref: '#/components/responses/NotFoundError'
 *       500:
 *         description: Error en el servidor
 *         content:
 *           application/json:
 *             schema:
 *               $ref: '#/components/responses/InternalServerError'
 *     security:
 *        - bearerAuth: []
 */
router.patch(
    '/EnableSede/:guid',
    authMiddleware,
    validatorEnableSede(),
    EnableSede
)

// Ruta para obtener todas las sedes, incluyendo las deshabilitadas
/**
 * @swagger
 * /tenant_sedes/SedesAll:
 *   get:
 *     summary: Obtener todas las sedes, incluyendo las deshabilitadas
 *     tags: [Tenant_Sedes]
 *     responses:
 *       200:
 *         description: Sedes obtenidas exitosamente
 *         content:
 *           application/json:
 *             schema:
 *               $ref: '#/components/responses/success'
 *       403:
 *         description: Error de autenticación
 *         content:
 *           application/json:
 *             schema:
 *               $ref: '#/components/responses/UnauthorizedError'
 *       500:
 *         description: Error en el servidor
 *         content:
 *           application/json:
 *             schema:
 *               $ref: '#/components/responses/InternalServerError'
 *     security:
 *        - bearerAuth: []
 */
router.get('/SedesAll', authMiddleware, SedesAll)

// Ruta para contar todas las sedes
/**
 * @swagger
 * /tenant_sedes/SedesCount:
 *   get:
 *     summary: Contar todas las sedes
 *     tags: [Tenant_Sedes]
 *     responses:
 *       200:
 *         description: Conteo de sedes obtenido exitosamente
 *         content:
 *           application/json:
 *             schema:
 *               $ref: '#/components/responses/success'
 *       403:
 *         description: Error de autenticación
 *         content:
 *           application/json:
 *             schema:
 *               $ref: '#/components/responses/UnauthorizedError'
 *       500:
 *         description: Error en el servidor
 *         content:
 *           application/json:
 *             schema:
 *               $ref: '#/components/responses/InternalServerError'
 *     security:
 *        - bearerAuth: []
 */
router.get('/SedesCount', authMiddleware, SedesCount)

module.exports = router
