const express = require('express')
const { matchedData } = require('express-validator')
const router = express.Router()
const ModulesModel = require('../../models/general/modules')

// validaciones
const {
    ValidatorCreateModule,
    ValidatorUpdateModule,
    ValidatorDeleteModule,
    ValidatorDisableModule,
    ValidatorEnableModule,
} = require('../../validators/general/modules')

// Importa los controladores
const {
    createModule,
    Modules,
    Module,
    updateModule,
    deleteModule,
    disableModule,
    enableModule,
    modulesAll,
    countAllModules,
} = require('../../controllers/general/modules')

// Middleware de autenticación
const authMiddleware = require('../../middleware/session')

// Rutas protegidas que requieren autenticación
/**
 * @swagger
 * tags:
 *   name: Modules
 *   description: API para la gestión de módulos
 */

// Ruta para crear un nuevo módulo
/**
 * @swagger
 * /modules/CreateModule:
 *   post:
 *     summary: Crear un nuevo módulo
 *     description: Esta ruta permite crear un nuevo módulo con nombre y descripción.
 *     tags: [Modules]
 *     requestBody:
 *       required: true
 *       content:
 *         application/json:
 *           schema:
 *             type: object
 *             properties:
 *               name:
 *                 type: string
 *                 example: Administración
 *               description:
 *                 type: string
 *                 example: Módulo de administración
 *               to:
 *                 type: string
 *                 example: /admin
 *               icon:
 *                  type: string
 *                  example: fa fa-cogs
 *               is_parent:
 *                  type: boolean
 *                  example: true
 *               parent_guid:
 *                  type: uuid
 *                  example: 123e4567-e89b-12d3-a456-426614174000
 *     responses:
 *       201:
 *         description: Module creado exitosamente
 *         content:
 *           application/json:
 *             schema:
 *               $ref: '#/components/responses/success'
 *       400:
 *         description: Error en los datos enviados
 *         content:
 *           application/json:
 *             schema:
 *               $ref: '#/components/responses/BadRequest'
 *       403:
 *         description: Error de autenticación
 *         content:
 *           application/json:
 *             schema:
 *               $ref: '#/components/responses/UnauthorizedError'
 *       409:
 *         description: Module ya existe
 *         content:
 *           application/json:
 *             schema:
 *               $ref: '#/components/responses/ConflictError'
 *       500:
 *         description: Error en el servidor
 *         content:
 *           application/json:
 *             schema:
 *               $ref: '#/components/responses/InternalServerError'
 *     security:
 *        - bearerAuth: []
 */
router.post(
    '/CreateModule',
    authMiddleware,
    ValidatorCreateModule,
    createModule
)

// Ruta para obtener todos los módulos activos
/**
 * @swagger
 *  /modules/modules:
 *   get:
 *     summary: Obtener todos los módulos activos
 *     tags: [Modules]
 *     responses:
 *       201:
 *         description: Modulos obtenidos exitosamente
 *         content:
 *           application/json:
 *             schema:
 *               $ref: '#/components/responses/success'
 *       403:
 *         description: Error de autenticación
 *         content:
 *           application/json:
 *             schema:
 *               $ref: '#/components/responses/UnauthorizedError'
 *       500:
 *         description: Error en el servidor
 *         content:
 *           application/json:
 *             schema:
 *               $ref: '#/components/responses/InternalServerError'
 *     security:
 *        - bearerAuth: []
 */
router.get('/Modules', authMiddleware, Modules)

// Ruta para obtener un módulo
/**
 * @swagger
 * /modules/module:
 *   get:
 *     summary: Obtiene un modulo por ID
 *     tags: [Modules]
 *     requestBody:
 *       required: true
 *       content:
 *         application/json:
 *           schema:
 *             type: object
 *             properties:
 *               guid:
 *                 type: uuid
 *     responses:
 *       201:
 *         description: Modulo obtenido exitosamente
 *         content:
 *           application/json:
 *             schema:
 *               $ref: '#/components/responses/success'
 *       403:
 *         description: Error de autenticación
 *         content:
 *           application/json:
 *             schema:
 *               $ref: '#/components/responses/UnauthorizedError'
 *       404:
 *        description: Modulo no encontrado
 *        content:
 *          application/json:
 *            schema:
 *              $ref: '#/components/responses/NotFoundError'
 *       500:
 *         description: Error en el servidor
 *         content:
 *           application/json:
 *             schema:
 *               $ref: '#/components/responses/InternalServerError'
 *     security:
 *        - bearerAuth: []
 */
router.get('/Module/:guid', authMiddleware, Module)

// Ruta para actualizar un módulo
/**
 * @swagger
 * /modules/updateModule:
 *   patch:
 *     summary: Actualizar un módulo
 *     tags: [Modules]
 *     requestBody:
 *       required: true
 *       content:
 *         application/json:
 *           schema:
 *             type: object
 *             properties:
 *               guid:
 *                 type: string
 *                 example: 6555ee6c-c713-46bc-8f0a-9ee7fde4ae89
 *               name:
 *                 type: string
 *                 example: Administración
 *               description:
 *                 type: string
 *                 example: Módulo de administración
 *               to:
 *                 type: string
 *                 example: /admin
 *               icon:
 *                  type: string
 *                  example: fa fa-cogs
 *               is_parent:
 *                  type: boolean
 *                  example: true
 *               parent_guid:
 *                  type: uuid
 *                  example: 123e4567-e89b-12d3-a456-426614174000
 *     responses:
 *       201:
 *         description: Modulo actualizado exitosamente
 *         content:
 *           application/json:
 *             schema:
 *               $ref: '#/components/responses/success'
 *       400:
 *         description: Error en los datos enviados
 *         content:
 *           application/json:
 *             schema:
 *               $ref: '#/components/responses/BadRequest'
 *       403:
 *         description: Error de autenticación
 *         content:
 *           application/json:
 *             schema:
 *               $ref: '#/components/responses/UnauthorizedError'
 *       404:
 *        description: Modulo no encontrado
 *        content:
 *          application/json:
 *            schema:
 *              $ref: '#/components/responses/NotFoundError'
 *       500:
 *         description: Error en el servidor
 *         content:
 *           application/json:
 *             schema:
 *               $ref: '#/components/responses/InternalServerError'
 *     security:
 *        - bearerAuth: []
 */
router.patch(
    '/UpdateModule/:guid',
    authMiddleware,
    ValidatorUpdateModule,
    updateModule
)

// Ruta para eliminar un módulo
/**
 * @swagger
 * /modules/DeleteModule:
 *   delete:
 *     summary: Eliminar un módulo
 *     tags: [Modules]
 *     requestBody:
 *       required: true
 *       content:
 *         application/json:
 *           schema:
 *             type: object
 *             properties:
 *               guid:
 *                 type: uuid
 *     responses:
 *       201:
 *         description: Modulo eliminado exitosamente
 *         content:
 *           application/json:
 *             schema:
 *               $ref: '#/components/responses/success'
 *       400:
 *         description: Error en los datos enviados
 *         content:
 *           application/json:
 *             schema:
 *               $ref: '#/components/responses/BadRequest'
 *       403:
 *         description: Error de autenticación
 *         content:
 *           application/json:
 *             schema:
 *               $ref: '#/components/responses/UnauthorizedError'
 *       404:
 *        description: Modulo no encontrado
 *        content:
 *          application/json:
 *            schema:
 *              $ref: '#/components/responses/NotFoundError'
 *       500:
 *         description: Error en el servidor
 *         content:
 *           application/json:
 *             schema:
 *               $ref: '#/components/responses/InternalServerError'
 *     security:
 *        - bearerAuth: []
 */
router.delete(
    '/DeleteModule/:guid',
    authMiddleware,
    ValidatorDeleteModule,
    deleteModule
)

// Ruta para desactivar un módulo
/**
 * @swagger
 * /modules/DisableModule:
 *   patch:
 *     summary: Desactivar un módulo
 *     tags: [Modules]
 *     requestBody:
 *       required: true
 *       content:
 *         application/json:
 *           schema:
 *             type: object
 *             properties:
 *               guid:
 *                 type: uuid
 *     responses:
 *       201:
 *         description: Modulo desactivado exitosamente
 *         content:
 *           application/json:
 *             schema:
 *               $ref: '#/components/responses/success'
 *       400:
 *         description: Error en los datos enviados
 *         content:
 *           application/json:
 *             schema:
 *               $ref: '#/components/responses/BadRequest'
 *       403:
 *         description: Error de autenticación
 *         content:
 *           application/json:
 *             schema:
 *               $ref: '#/components/responses/UnauthorizedError'
 *       404:
 *        description: Modulo no encontrado
 *        content:
 *          application/json:
 *            schema:
 *              $ref: '#/components/responses/NotFoundError'
 *       500:
 *         description: Error en el servidor
 *         content:
 *           application/json:
 *             schema:
 *               $ref: '#/components/responses/InternalServerError'
 *     security:
 *        - bearerAuth: []
 */
router.patch(
    '/DisableModule/:guid',
    authMiddleware,
    ValidatorDisableModule,
    disableModule
)

// Ruta para activar un módulo
/**
 * @swagger
 * /modules/EnableModule:
 *   patch:
 *     summary: Activar un módulo
 *     tags: [Modules]
 *     requestBody:
 *       required: true
 *       content:
 *         application/json:
 *           schema:
 *             type: object
 *             properties:
 *               guid:
 *                 type: uuid
 *     responses:
 *       201:
 *         description: Modulo activado exitosamente
 *         content:
 *           application/json:
 *             schema:
 *               $ref: '#/components/responses/success'
 *       400:
 *         description: Error en los datos enviados
 *         content:
 *           application/json:
 *             schema:
 *               $ref: '#/components/responses/BadRequest'
 *       403:
 *         description: Error de autenticación
 *         content:
 *           application/json:
 *             schema:
 *               $ref: '#/components/responses/UnauthorizedError'
 *       404:
 *        description: Modulo no encontrado
 *        content:
 *          application/json:
 *            schema:
 *              $ref: '#/components/responses/NotFoundError'
 *       500:
 *         description: Error en el servidor
 *         content:
 *           application/json:
 *             schema:
 *               $ref: '#/components/responses/InternalServerError'
 *     security:
 *        - bearerAuth: []
 */
router.patch(
    '/EnableModule/:guid',
    authMiddleware,
    ValidatorEnableModule,
    enableModule
)

// Ruta para obtener todos los módulos
/**
 * @swagger
 * /modules/ModulesAll:
 *   get:
 *     summary: Obtener todos los módulos
 *     tags: [Modules]
 *     responses:
 *       201:
 *         description: Modulos obtenidos exitosamente
 *         content:
 *           application/json:
 *             schema:
 *               $ref: '#/components/responses/success'
 *       400:
 *         description: Error en los datos enviados
 *         content:
 *           application/json:
 *             schema:
 *               $ref: '#/components/responses/BadRequest'
 *       403:
 *         description: Error de autenticación
 *         content:
 *           application/json:
 *             schema:
 *               $ref: '#/components/responses/UnauthorizedError'
 *       500:
 *         description: Error en el servidor
 *         content:
 *           application/json:
 *             schema:
 *               $ref: '#/components/responses/InternalServerError'
 *     security:
 *        - bearerAuth: []
 */
router.get('/ModulesAll', authMiddleware, modulesAll)

// Ruta para contar todos los módulos
/**
 * @swagger
 * /modules/ModulesCount:
 *   get:
 *     summary: Contar todos los módulos
 *     tags: [Modules]
 *     responses:
 *       201:
 *         description: Modulos contados exitosamente
 *         content:
 *           application/json:
 *             schema:
 *               $ref: '#/components/responses/success'
 *       400:
 *         description: Error en los datos enviados
 *         content:
 *           application/json:
 *             schema:
 *               $ref: '#/components/responses/BadRequest'
 *       403:
 *         description: Error de autenticación
 *         content:
 *           application/json:
 *             schema:
 *               $ref: '#/components/responses/UnauthorizedError'
 *       500:
 *         description: Error en el servidor
 *         content:
 *           application/json:
 *             schema:
 *               $ref: '#/components/responses/InternalServerError'
 *     security:
 *        - bearerAuth: []
 */
router.get('/ModulesCount', authMiddleware, countAllModules)

module.exports = router
