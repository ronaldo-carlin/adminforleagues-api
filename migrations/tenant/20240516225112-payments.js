module.exports = {
    async up(queryInterface, Sequelize) {
        // entidad para registrar los pagos
        await queryInterface.createTable('payments', {
            // identificador del registro
            id: {
                type: Sequelize.INTEGER,
                primaryKey: true,
                autoIncrement: true,
                allowNull: false,
            },
            guid: {
                type: Sequelize.UUID,
                defaultValue: Sequelize.UUIDV4,
                unique: true,
                allowNull: false,
            },
            // identificador del usuario que realizo el pago
            user_guid: {
                type: Sequelize.UUID,
            },
            // tipo de pago
            type: {
                type: Sequelize.TEXT,
            },
            // correo electronico del usuario que realizo el pago
            email: {
                type: Sequelize.TEXT,
            },
            // monto del pago
            amount: {
                type: Sequelize.DECIMAL,
            },
            // moneda del pago
            currency: {
                type: Sequelize.TEXT,
            },
            // icono de la moneda
            currency_icon: {
                type: Sequelize.TEXT,
            },
            // codigo de la moneda
            currency_code: {
                type: Sequelize.TEXT,
            },
            // metodo de pago
            method: {
                type: Sequelize.TEXT,
            },
            // identificador de la transaccion
            transaction: {
                type: Sequelize.TEXT,
            },
            // referencia de la transaccion
            reference: {
                type: Sequelize.TEXT,
            },
            // concepto del pago
            concept: {
                type: Sequelize.TEXT,
            },
            // tarjeta de pago
            card: {
                type: Sequelize.TEXT,
            },
            // banco de la tarjeta
            bank: {
                type: Sequelize.TEXT,
            },
            // mensaje de respuesta
            message: {
                type: Sequelize.TEXT,
            },
            // estado de la transaccion
            status: {
                type: Sequelize.TEXT,
            },
            // estado del registro
            is_active: {
                type: Sequelize.BOOLEAN,
                defaultValue: 1, // valor por defecto
            },
            // usuario que creo el registro
            created_by: {
                type: Sequelize.UUID,
            },
            // fecha de creacion
            created_at: {
                type: Sequelize.DATE,
                defaultValue: Sequelize.literal('CURRENT_TIMESTAMP'),
            },
            // usuario que actualizo el registro
            updated_by: {
                type: Sequelize.UUID,
            },
            // fecha de actualizacion
            updated_at: {
                type: Sequelize.DATE,
            },
            // usuario que elimino el registro
            deleted_by: {
                type: Sequelize.UUID,
            },
            // fecha de eliminacion
            deleted_at: {
                type: Sequelize.DATE,
            },
        })

        // indices
        await queryInterface.addIndex('payments', ['guid'], {
            unique: true,
            name: 'unique_payments_guid',
        })

        await queryInterface.addIndex('payments', ['user_guid'], {
            name: 'idx_payments_user_guid',
        })

        // llaves foraneas
        await queryInterface.addConstraint('payments', {
            fields: ['user_guid'],
            type: 'foreign key',
            name: 'fk_payments_user_guid',
            references: {
                table: 'users',
                field: 'guid',
            },
            onUpdate: 'CASCADE',
            onDelete: 'NO ACTION',
        })
    },

    async down(queryInterface, Sequelize) {
        await queryInterface.removeConstraint(
            'payments',
            'fk_payments_user_guid'
        )
        await queryInterface.removeIndex('payments', 'unique_payments_guid')
        await queryInterface.removeIndex('payments', 'idx_payments_user_guid')
        await queryInterface.dropTable('payments')
    },
}
