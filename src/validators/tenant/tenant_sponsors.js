const { check, validationResult } = require('express-validator')
const {
    getSequelize,
    getCompetitionsModel,
    getTeamsModel,
} = require('../../helpers/sequelizeHelper')

// Validación para crear un sponsor
const validatorCreateSponsor = () => {
    return [
        async (req, res, next) => {
            const sequelize = getSequelize(req)
            const CompetitionsModel = getCompetitionsModel(sequelize)
            const TeamsModel = getTeamsModel(sequelize)

            await check('competition_guid')
                .isUUID()
                .withMessage(
                    'El GUID de la competencia debe ser un UUID válido.'
                )
                .custom(async (value) => {
                    const competition = await CompetitionsModel.findOne({
                        where: { guid: value },
                    })
                    if (!competition) {
                        return Promise.reject('La competencia no existe.')
                    }
                })
                .run(req)

            await check('team_guid')
                .optional()
                .isUUID()
                .withMessage('El GUID del equipo debe ser un UUID válido.')
                .custom(async (value) => {
                    if (value) {
                        const team = await TeamsModel.findOne({
                            where: { guid: value },
                        })
                        if (!team) {
                            return Promise.reject('El equipo no existe.')
                        }
                    }
                })
                .run(req)

            await check('name')
                .isString()
                .withMessage('El nombre debe ser una cadena de texto.')
                .run(req)

            await check('logo_path')
                .optional()
                .isString()
                .withMessage('El path del logo debe ser una cadena de texto.')
                .run(req)

            await check('start_date')
                .isISO8601()
                .withMessage(
                    'La fecha de inicio debe ser una fecha válida en formato ISO8601.'
                )
                //validar que la fecha de inicio sea menor a la fecha de fin
                .custom((value, { req }) => {
                    if (req.body.end_date) {
                        if (value > req.body.end_date) {
                            throw new Error(
                                'La fecha de inicio debe ser menor a la fecha de fin.'
                            )
                        }
                        //validar que la fecha de inicio sea mayor a la fecha actual
                        if (value < new Date().toISOString()) {
                            throw new Error(
                                'La fecha de inicio debe ser mayor a la fecha actual.'
                            )
                        }
                    }
                    return true
                })
                .run(req)

            await check('end_date')
                .optional()
                .isISO8601()
                .withMessage(
                    'La fecha de fin debe ser una fecha válida en formato ISO8601.'
                )
                //validar que la fecha de fin sea mayor a la fecha de inicio
                .custom((value, { req }) => {
                    if (req.body.start_date) {
                        if (value < req.body.start_date) {
                            throw new Error(
                                'La fecha de fin debe ser mayor a la fecha de inicio.'
                            )
                        }
                    }
                    return true
                })
                .run(req)

            await check('format')
                .optional()
                .isString()
                .withMessage('El formato debe ser una cadena de texto.')
                .run(req)

            await check('site_web')
                .optional()
                .isURL()
                .withMessage('El sitio web debe ser una URL válida.')
                .run(req)

            await check('phone')
                .optional()
                .isMobilePhone()
                .withMessage('El número de teléfono debe ser válido.')
                .run(req)

            await check('description')
                .optional()
                .isString()
                .withMessage('La descripción debe ser una cadena de texto.')
                .run(req)

            const errors = validationResult(req)
            if (!errors.isEmpty()) {
                return res.status(400).json({ errors: errors.array() })
            }

            next()
        },
    ]
}

// Validación para obtener un sponsor por GUID
const validatorSponsor = () => {
    return [
        async (req, res, next) => {
            await check('guid')
                .isUUID()
                .withMessage(
                    'El GUID del sponsor es requerido y debe ser un UUID válido.'
                )
                .run(req)

            const errors = validationResult(req)
            if (!errors.isEmpty()) {
                return res.status(400).json({ errors: errors.array() })
            }

            next()
        },
    ]
}

// Validación para actualizar un sponsor
const validatorUpdateSponsor = () => {
    return [
        async (req, res, next) => {
            const sequelize = getSequelize(req)
            const CompetitionsModel = getCompetitionsModel(sequelize)
            const TeamsModel = getTeamsModel(sequelize)

            await check('guid')
                .isUUID()
                .withMessage(
                    'El GUID del sponsor es requerido y debe ser un UUID válido.'
                )
                .run(req)

            await check('competition_guid')
                .optional()
                .isUUID()
                .withMessage(
                    'El GUID de la competencia debe ser un UUID válido.'
                )
                .custom(async (value) => {
                    if (value) {
                        const competition = await CompetitionsModel.findOne({
                            where: { guid: value },
                        })
                        if (!competition) {
                            return Promise.reject('La competencia no existe.')
                        }
                    }
                })
                .run(req)

            await check('team_guid')
                .optional()
                .isUUID()
                .withMessage('El GUID del equipo debe ser un UUID válido.')
                .custom(async (value) => {
                    if (value) {
                        const team = await TeamsModel.findOne({
                            where: { guid: value },
                        })
                        if (!team) {
                            return Promise.reject('El equipo no existe.')
                        }
                    }
                })
                .run(req)

            await check('name')
                .optional()
                .isString()
                .withMessage('El nombre debe ser una cadena de texto.')
                .run(req)

            await check('logo_path')
                .optional()
                .isString()
                .withMessage('El path del logo debe ser una cadena de texto.')
                .run(req)

            await check('start_date')
                .optional()
                .isISO8601()
                .withMessage(
                    'La fecha de inicio debe ser una fecha válida en formato ISO8601.'
                )
                .custom((value, { req }) => {
                    if (req.body.end_date) {
                        if (value > req.body.end_date) {
                            throw new Error(
                                'La fecha de inicio debe ser menor a la fecha de fin.'
                            )
                        }
                        //validar que la fecha de inicio sea mayor a la fecha actual
                        if (value < new Date().toISOString()) {
                            throw new Error(
                                'La fecha de inicio debe ser mayor a la fecha actual.'
                            )
                        }
                    }
                    return true
                })
                .run(req)

            await check('end_date')
                .optional()
                .isISO8601()
                .withMessage(
                    'La fecha de fin debe ser una fecha válida en formato ISO8601.'
                )
                .custom((value, { req }) => {
                    if (req.body.start_date) {
                        if (value < req.body.start_date) {
                            throw new Error(
                                'La fecha de fin debe ser mayor a la fecha de inicio.'
                            )
                        }
                    }
                    return true
                })
                .run(req)

            await check('format')
                .optional()
                .isString()
                .withMessage('El formato debe ser una cadena de texto.')
                .run(req)

            await check('site_web')
                .optional()
                .isURL()
                .withMessage('El sitio web debe ser una URL válida.')
                .run(req)

            await check('phone')
                .optional()
                .isMobilePhone()
                .withMessage('El número de teléfono debe ser válido.')
                .run(req)

            await check('description')
                .optional()
                .isString()
                .withMessage('La descripción debe ser una cadena de texto.')
                .run(req)

            await check('is_active')
                .optional()
                .isInt({ min: 0, max: 1 })
                .withMessage(
                    'El estado activo debe ser 0 (inactivo) o 1 (activo).'
                )
                .run(req)

            const errors = validationResult(req)
            if (!errors.isEmpty()) {
                return res.status(400).json({ errors: errors.array() })
            }

            next()
        },
    ]
}

// Validación para eliminar un sponsor
const validatorDeleteSponsor = () => {
    return [
        async (req, res, next) => {
            await check('guid')
                .isUUID()
                .withMessage(
                    'El GUID del sponsor es requerido y debe ser un UUID válido.'
                )
                .run(req)

            const errors = validationResult(req)
            if (!errors.isEmpty()) {
                return res.status(400).json({ errors: errors.array() })
            }

            next()
        },
    ]
}

// validación para deshabilitar un sponsor
const validatorDisableSponsor = () => {
    return [
        async (req, res, next) => {
            await check('guid')
                .isUUID()
                .withMessage(
                    'El GUID del sponsor es requerido y debe ser un UUID válido.'
                )
                .run(req)

            const errors = validationResult(req)
            if (!errors.isEmpty()) {
                return res.status(400).json({ errors: errors.array() })
            }

            next()
        },
    ]
}

// validación para habilitar un sponsor
const validatorEnableSponsor = () => {
    return [
        async (req, res, next) => {
            await check('guid')
                .isUUID()
                .withMessage(
                    'El GUID del sponsor es requerido y debe ser un UUID válido.'
                )
                .run(req)

            const errors = validationResult(req)
            if (!errors.isEmpty()) {
                return res.status(400).json({ errors: errors.array() })
            }

            next()
        },
    ]
}

module.exports = {
    validatorCreateSponsor,
    validatorSponsor,
    validatorUpdateSponsor,
    validatorDeleteSponsor,
    validatorDisableSponsor,
    validatorEnableSponsor,
}
