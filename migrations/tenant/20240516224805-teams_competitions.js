module.exports = {
    async up(queryInterface, Sequelize) {
        // entidad que relaciona los equipos con los torneos
        await queryInterface.createTable('teams_competitions', {
            // id del equipo en el torneo
            id: {
                type: Sequelize.INTEGER,
                primaryKey: true,
                autoIncrement: true,
                allowNull: false,
            },
            guid: {
                type: Sequelize.UUID,
                defaultValue: Sequelize.UUIDV4,
                unique: true,
                allowNull: false,
            },
            // identificador unico del equipo\
            team_guid: {
                type: Sequelize.UUID,
            },
            // identificador unico del torneo
            competition_guid: {
                type: Sequelize.UUID,
            },
            // fase del torneo
            group: {
                type: Sequelize.TEXT,
            },
            // puntos del equipo en el torneo
            points: {
                type: Sequelize.INTEGER,
            },
            // partidos jugados por el equipo en el torneo
            played: {
                type: Sequelize.INTEGER,
            },
            // partidos ganados por el equipo en el torneo
            won: {
                type: Sequelize.INTEGER,
            },
            // partidos empatados por el equipo en el torneo
            drawn: {
                type: Sequelize.INTEGER,
            },
            // partidos perdidos por el equipo en el torneo
            lost: {
                type: Sequelize.INTEGER,
            },
            // goles a favor del equipo en el torneo
            goals_for: {
                type: Sequelize.INTEGER,
            },
            // goles en contra del equipo en el torneo
            goals_against: {
                type: Sequelize.INTEGER,
            },
            // diferencia de goles del equipo en el torneo
            goal_difference: {
                type: Sequelize.INTEGER,
            },
            // estado del registro
            is_active: {
                type: Sequelize.BOOLEAN,
                defaultValue: 1, // valor por defecto
            },
            // usuario que creo el registro
            created_by: {
                type: Sequelize.UUID,
            },
            // fecha de creacion
            created_at: {
                type: Sequelize.DATE,
                defaultValue: Sequelize.literal('CURRENT_TIMESTAMP'),
            },
            // usuario que actualizo el registro
            updated_by: {
                type: Sequelize.UUID,
            },
            // fecha de actualizacion
            updated_at: {
                type: Sequelize.DATE,
            },
            // usuario que elimino el registro
            deleted_by: {
                type: Sequelize.UUID,
            },
            // fecha de eliminacion
            deleted_at: {
                type: Sequelize.DATE,
            },
        })

        // se agrega indice de la tabla
        await queryInterface.addIndex('teams_competitions', ['guid'], {
            unique: true,
            name: 'unique_teams_competitions_guid',
        })

        await queryInterface.addIndex('teams_competitions', ['team_guid'], {
            name: 'idx_team_guid',
        })

        await queryInterface.addIndex(
            'teams_competitions',
            ['competition_guid'],
            {
                name: 'idx_competition_guid',
            }
        )

        // agregamos las llaves foraneas
        await queryInterface.addConstraint('teams_competitions', {
            fields: ['team_guid'],
            type: 'foreign key',
            name: 'fk_team_guid',
            references: {
                table: 'teams',
                field: 'guid',
            },
            onDelete: 'NO ACTION',
            onUpdate: 'CASCADE',
        })

        await queryInterface.addConstraint('teams_competitions', {
            fields: ['competition_guid'],
            type: 'foreign key',
            name: 'fk_competition_guid',
            references: {
                table: 'competitions',
                field: 'guid',
            },
            onDelete: 'NO ACTION',
            onUpdate: 'CASCADE',
        })
    },

    async down(queryInterface, Sequelize) {
        await queryInterface.removeConstraint(
            'teams_competitions',
            'fk_team_guid'
        )
        await queryInterface.removeConstraint(
            'teams_competitions',
            'fk_competition_guid'
        )
        await queryInterface.removeIndex(
            'teams_competitions',
            'unique_teams_competitions_guid'
        )
        await queryInterface.removeIndex('teams_competitions', 'idx_team_guid')
        await queryInterface.removeIndex(
            'teams_competitions',
            'idx_competition_guid'
        )
        await queryInterface.dropTable('teams_competitions')
    },
}
