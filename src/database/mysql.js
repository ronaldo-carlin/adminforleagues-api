const { Sequelize } = require('sequelize')

if (process.env.NODE_ENV === 'production') {
    require('dotenv').config({ path: 'production.env' })
} else {
    require('dotenv').config({ path: '.env.dev' })
}

const database = process.env.MYSQL_DATABASE
const username = process.env.MYSQL_USER
const password = process.env.MYSQL_PASSWORD
const host = process.env.MYSQL_HOST

const sequelize = new Sequelize(database, username, password, {
    host,
    dialect: 'mysql',
    logging: false,
})

const dbConnectMySQL = async () => {
    try {
        await sequelize.authenticate()
        console.log('<<< Conexión a MySQL exitosa >>>')
    } catch (err) {
        console.error('<<< Error en la conexión a MySQL >>>', err)
    }
}

module.exports = { sequelize, dbConnectMySQL }
