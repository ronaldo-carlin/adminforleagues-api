// Importar Sequelize
const { DataTypes } = require('sequelize')

// Definir la función factory para el modelo SedesTenantModel
const SedesTenantModelFactory = (sequelize) => {
    const SedesTenantModel = sequelize.define(
        'sedes',
        {
            id: {
                type: DataTypes.INTEGER,
                primaryKey: true,
                autoIncrement: true,
            },
            guid: {
                type: DataTypes.UUID,
                primaryKey: true,
                defaultValue: DataTypes.UUIDV4,
                unique: true,
            },
            name: {
                type: DataTypes.TEXT,
            },
            description: {
                type: DataTypes.TEXT,
            },
            schedule: {
                type: DataTypes.TEXT,
            },
            address: {
                type: DataTypes.TEXT,
            },
            city: {
                type: DataTypes.TEXT,
            },
            state: {
                type: DataTypes.TEXT,
            },
            country: {
                type: DataTypes.TEXT,
            },
            capacity: {
                type: DataTypes.INTEGER,
            },
            latitude: {
                type: DataTypes.TEXT,
            },
            longitude: {
                type: DataTypes.TEXT,
            },
            image_path: {
                type: DataTypes.TEXT,
            },
            is_active: {
                type: DataTypes.INTEGER,
                defaultValue: 1,
            },
            created_by: {
                type: DataTypes.UUID,
            },
            updated_by: {
                type: DataTypes.UUID,
            },
            deleted_by: {
                type: DataTypes.UUID,
            },
            created_at: {
                type: DataTypes.DATE,
            },
            updated_at: {
                type: DataTypes.DATE,
            },
            deleted_at: {
                type: DataTypes.DATE,
            },
        },
        {
            tableName: 'sedes',
            timestamps: false,
            defaultScope: {
                attributes: {
                    exclude: [
                        'created_by',
                        'updated_by',
                        'deleted_by',
                        'created_at',
                        'updated_at',
                        'deleted_at',
                    ],
                },
            },
        }
    )

    return SedesTenantModel
}

module.exports = SedesTenantModelFactory
