module.exports = {
    async up(queryInterface, Sequelize) {
        // entidad para los tenants que contratan los servicios
        await queryInterface.createTable('tenants', {
            // id
            id: {
                type: Sequelize.INTEGER,
                primaryKey: true,
                autoIncrement: true,
                allowNull: false,
            },
            // guid
            guid: {
                type: Sequelize.UUID,
                defaultValue: Sequelize.UUIDV4,
                unique: true,
                allowNull: false,
            },
            // nombre de la empresa
            company_name: {
                type: Sequelize.TEXT,
                allowNull: false,
                unique: true,
            },
            // correo electronico
            email: {
                type: Sequelize.TEXT,
                allowNull: false,
                unique: true,
            },
            // telefono
            phone: {
                type: Sequelize.STRING(15),
            },
            // pais
            country: {
                type: Sequelize.TEXT,
            },
            // estado
            state: {
                type: Sequelize.TEXT,
            },
            // ciudad
            city: {
                type: Sequelize.TEXT,
            },
            // direccion
            address: {
                type: Sequelize.TEXT,
            },
            // estado del tenant
            is_active: {
                type: Sequelize.BOOLEAN,
                defaultValue: 1, // valor por defecto
            },
            // usuario que creo el registro
            created_by: {
                type: Sequelize.UUID,
            },
            // fecha de creacion
            created_at: {
                type: Sequelize.DATE,
                defaultValue: Sequelize.literal('CURRENT_TIMESTAMP'),
            },
            // usuario que actualizo el registro
            updated_by: {
                type: Sequelize.UUID,
            },
            // fecha de actualizacion
            updated_at: {
                type: Sequelize.DATE,
            },
            // usuario que elimino el registro
            deleted_by: {
                type: Sequelize.UUID,
            },
            // fecha de eliminacion
            deleted_at: {
                type: Sequelize.DATE,
            },
        })
        // Agregar índices únicos después de crear la tabla
        await queryInterface.addIndex('tenants', ['guid'], {
            unique: true,
            name: 'uniq_tenants_guid',
        })

        await queryInterface.addIndex('tenants', ['email'], {
            unique: true,
            using: 'HASH',
            name: 'uniq_tenants_email',
        })
    },

    async down(queryInterface, Sequelize) {
        await queryInterface.removeIndex('tenants', 'uniq_tenants_guid')
        await queryInterface.removeIndex('tenants', 'uniq_tenants_email')
        await queryInterface.dropTable('tenants')
    },
}
