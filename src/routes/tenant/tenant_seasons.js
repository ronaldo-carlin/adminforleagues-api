const express = require('express')
const router = express.Router()

// Controladores y validadores para seasons
const {
    validatorCreateSeason,
    validatorSeason,
    validatorUpdateSeason,
    validatorDeleteSeason,
    validatorDisableSeason,
    validatorEnableSeason,
} = require('../../validators/tenant/tenant_seasons')

const {
    CreateSeason,
    Seasons,
    Season,
    UpdateSeason,
    DeleteSeason,
    DisableSeason,
    EnableSeason,
    SeasonsAll,
    SeasonsCount,
} = require('../../controllers/tenant/tenant_seasons')

const authMiddleware = require('../../middleware/session_tenant')

/* Rutas protegidas que requieren autenticación */
/**
 * @swagger
 * tags:
 *   name: Tenant_Seasons
 *   description: API para la gestión de temporadas
 */

// Ruta para crear una nueva temporada
/**
 * @swagger
 * /tenant_seasons/CreateSeason:
 *   post:
 *     summary: Crear una nueva temporada
 *     description: Esta ruta permite crear una nueva temporada con los detalles especificados.
 *     tags: [Tenant_Seasons]
 *     requestBody:
 *       required: true
 *       content:
 *         application/json:
 *           schema:
 *             type: object
 *             properties:
 *               name:
 *                 type: string
 *                 example: "Temporada 2024"
 *               description:
 *                 type: string
 *                 example: "Temporada de eventos deportivos para el año 2024."
 *     responses:
 *       201:
 *         description: Temporada creada exitosamente
 *         content:
 *           application/json:
 *             schema:
 *               $ref: '#/components/responses/success'
 *       400:
 *         description: Error en los datos enviados
 *         content:
 *           application/json:
 *             schema:
 *               $ref: '#/components/responses/BadRequest'
 *       403:
 *         description: Error de autenticación
 *         content:
 *           application/json:
 *             schema:
 *               $ref: '#/components/responses/UnauthorizedError'
 *       409:
 *         description: La temporada ya existe
 *         content:
 *           application/json:
 *             schema:
 *               $ref: '#/components/responses/ConflictError'
 *       500:
 *         description: Error en el servidor
 *         content:
 *           application/json:
 *             schema:
 *               $ref: '#/components/responses/InternalServerError'
 *     security:
 *        - bearerAuth: []
 */
router.post(
    '/CreateSeason',
    authMiddleware,
    validatorCreateSeason(),
    CreateSeason
)

// Ruta para obtener todas las temporadas activas
/**
 * @swagger
 * /tenant_seasons/Seasons:
 *   get:
 *     summary: Obtener todas las temporadas activas
 *     tags: [Tenant_Seasons]
 *     responses:
 *       200:
 *         description: Temporadas obtenidas exitosamente
 *         content:
 *           application/json:
 *             schema:
 *               $ref: '#/components/responses/success'
 *       403:
 *         description: Error de autenticación
 *         content:
 *           application/json:
 *             schema:
 *               $ref: '#/components/responses/UnauthorizedError'
 *       500:
 *         description: Error en el servidor
 *         content:
 *           application/json:
 *             schema:
 *               $ref: '#/components/responses/InternalServerError'
 *     security:
 *        - bearerAuth: []
 */
router.get('/Seasons', authMiddleware, Seasons)

// Ruta para obtener una temporada por GUID
/**
 * @swagger
 * /tenant_seasons/Season/{guid}:
 *   get:
 *     summary: Obtener una temporada por GUID
 *     tags: [Tenant_Seasons]
 *     parameters:
 *       - in: path
 *         name: guid
 *         schema:
 *           type: string
 *           format: uuid
 *         required: true
 *         description: El GUID de la temporada
 *     responses:
 *       200:
 *         description: Temporada obtenida exitosamente
 *         content:
 *           application/json:
 *             schema:
 *               $ref: '#/components/responses/success'
 *       403:
 *         description: Error de autenticación
 *         content:
 *           application/json:
 *             schema:
 *               $ref: '#/components/responses/UnauthorizedError'
 *       404:
 *         description: Temporada no encontrada
 *         content:
 *           application/json:
 *             schema:
 *               $ref: '#/components/responses/NotFoundError'
 *       500:
 *         description: Error en el servidor
 *         content:
 *           application/json:
 *             schema:
 *               $ref: '#/components/responses/InternalServerError'
 *     security:
 *        - bearerAuth: []
 */
router.get('/Season/:guid', authMiddleware, validatorSeason(), Season)

// Ruta para actualizar una temporada
/**
 * @swagger
 * /tenant_seasons/UpdateSeason/{guid}:
 *   patch:
 *     summary: Actualizar una temporada
 *     tags: [Tenant_Seasons]
 *     requestBody:
 *       required: true
 *       content:
 *         application/json:
 *           schema:
 *             type: object
 *             properties:
 *               name:
 *                 type: string
 *                 example: "Temporada 2024"
 *               description:
 *                 type: string
 *                 example: "Temporada de eventos deportivos para el año 2024."
 *     responses:
 *       200:
 *         description: Temporada actualizada exitosamente
 *         content:
 *           application/json:
 *             schema:
 *               $ref: '#/components/responses/success'
 *       400:
 *         description: Error en los datos enviados
 *         content:
 *           application/json:
 *             schema:
 *               $ref: '#/components/responses/BadRequest'
 *       403:
 *         description: Error de autenticación
 *         content:
 *           application/json:
 *             schema:
 *               $ref: '#/components/responses/UnauthorizedError'
 *       404:
 *         description: Temporada no encontrada
 *         content:
 *           application/json:
 *             schema:
 *               $ref: '#/components/responses/NotFoundError'
 *       500:
 *         description: Error en el servidor
 *         content:
 *           application/json:
 *             schema:
 *               $ref: '#/components/responses/InternalServerError'
 *     security:
 *        - bearerAuth: []
 */
router.patch(
    '/UpdateSeason/:guid',
    authMiddleware,
    validatorUpdateSeason(),
    UpdateSeason
)

// Ruta para eliminar una temporada
/**
 * @swagger
 * /tenant_seasons/DeleteSeason/{guid}:
 *   delete:
 *     summary: Eliminar una temporada
 *     tags: [Tenant_Seasons]
 *     parameters:
 *       - in: path
 *         name: guid
 *         schema:
 *           type: string
 *           format: uuid
 *         required: true
 *         description: El GUID de la temporada
 *     responses:
 *       200:
 *         description: Temporada eliminada exitosamente
 *         content:
 *           application/json:
 *             schema:
 *               $ref: '#/components/responses/success'
 *       400:
 *         description: Error en los datos enviados
 *         content:
 *           application/json:
 *             schema:
 *               $ref: '#/components/responses/BadRequest'
 *       403:
 *         description: Error de autenticación
 *         content:
 *           application/json:
 *             schema:
 *               $ref: '#/components/responses/UnauthorizedError'
 *       404:
 *         description: Temporada no encontrada
 *         content:
 *           application/json:
 *             schema:
 *               $ref: '#/components/responses/NotFoundError'
 *       500:
 *         description: Error en el servidor
 *         content:
 *           application/json:
 *             schema:
 *               $ref: '#/components/responses/InternalServerError'
 *     security:
 *        - bearerAuth: []
 */
router.delete(
    '/DeleteSeason/:guid',
    authMiddleware,
    validatorDeleteSeason(),
    DeleteSeason
)

// Ruta para deshabilitar una temporada
/**
 * @swagger
 * /tenant_seasons/DisableSeason/{guid}:
 *   patch:
 *     summary: Deshabilitar una temporada
 *     tags: [Tenant_Seasons]
 *     requestBody:
 *       required: true
 *       content:
 *         application/json:
 *           schema:
 *             type: object
 *             properties:
 *               guid:
 *                 type: uuid
 *                 example: 495db854-849b-41cd-b6b9-9c84824c92ba
 *     responses:
 *       200:
 *         description: Temporada deshabilitada exitosamente
 *         content:
 *           application/json:
 *             schema:
 *               $ref: '#/components/responses/success'
 *       400:
 *         description: Error en los datos enviados
 *         content:
 *           application/json:
 *             schema:
 *               $ref: '#/components/responses/BadRequest'
 *       403:
 *         description: Error de autenticación
 *         content:
 *           application/json:
 *             schema:
 *               $ref: '#/components/responses/UnauthorizedError'
 *       404:
 *         description: Temporada no encontrada
 *         content:
 *           application/json:
 *             schema:
 *               $ref: '#/components/responses/NotFoundError'
 *       500:
 *         description: Error en el servidor
 *         content:
 *           application/json:
 *             schema:
 *               $ref: '#/components/responses/InternalServerError'
 *     security:
 *        - bearerAuth: []
 */
router.patch(
    '/DisableSeason/:guid',
    authMiddleware,
    validatorDisableSeason(),
    DisableSeason
)

// Ruta para habilitar una temporada
/**
 * @swagger
 * /tenant_seasons/EnableSeason/{guid}:
 *   patch:
 *     summary: Habilitar una temporada
 *     tags: [Tenant_Seasons]
 *     requestBody:
 *       required: true
 *       content:
 *         application/json:
 *           schema:
 *             type: object
 *             properties:
 *               guid:
 *                 type: uuid
 *                 example: 495db854-849b-41cd-b6b9-9c84824c92ba
 *     responses:
 *       200:
 *         description: Temporada habilitada exitosamente
 *         content:
 *           application/json:
 *             schema:
 *               $ref: '#/components/responses/success'
 *       400:
 *         description: Error en los datos enviados
 *         content:
 *           application/json:
 *             schema:
 *               $ref: '#/components/responses/BadRequest'
 *       403:
 *         description: Error de autenticación
 *         content:
 *           application/json:
 *             schema:
 *               $ref: '#/components/responses/UnauthorizedError'
 *       404:
 *         description: Temporada no encontrada
 *         content:
 *           application/json:
 *             schema:
 *               $ref: '#/components/responses/NotFoundError'
 *       500:
 *         description: Error en el servidor
 *         content:
 *           application/json:
 *             schema:
 *               $ref: '#/components/responses/InternalServerError'
 *     security:
 *        - bearerAuth: []
 */
router.patch(
    '/EnableSeason/:guid',
    authMiddleware,
    validatorEnableSeason(),
    EnableSeason
)

// Ruta para obtener todas las temporadas activas e inactivas
/**
 * @swagger
 * /tenant_seasons/SeasonsAll:
 *   get:
 *     summary: Obtener todas las temporadas (activas e inactivas)
 *     tags: [Tenant_Seasons]
 *     responses:
 *       200:
 *         description: Temporadas obtenidas exitosamente
 *         content:
 *           application/json:
 *             schema:
 *               $ref: '#/components/responses/success'
 *       403:
 *         description: Error de autenticación
 *         content:
 *           application/json:
 *             schema:
 *               $ref: '#/components/responses/UnauthorizedError'
 *       500:
 *         description: Error en el servidor
 *         content:
 *           application/json:
 *             schema:
 *               $ref: '#/components/responses/InternalServerError'
 *     security:
 *        - bearerAuth: []
 */
router.get('/SeasonsAll', authMiddleware, SeasonsAll)

// Ruta para obtener el número total de temporadas activas
/**
 * @swagger
 * /tenant_seasons/SeasonsCount:
 *   get:
 *     summary: Obtener el número total de temporadas activas
 *     tags: [Tenant_Seasons]
 *     responses:
 *       200:
 *         description: Número total de temporadas obtenidas exitosamente
 *         content:
 *           application/json:
 *             schema:
 *               $ref: '#/components/responses/success'
 *       403:
 *         description: Error de autenticación
 *         content:
 *           application/json:
 *             schema:
 *               $ref: '#/components/responses/UnauthorizedError'
 *       500:
 *         description: Error en el servidor
 *         content:
 *           application/json:
 *             schema:
 *               $ref: '#/components/responses/InternalServerError'
 *     security:
 *        - bearerAuth: []
 */
router.get('/SeasonsCount', authMiddleware, SeasonsCount)

module.exports = router
