module.exports = {
    async up(queryInterface, Sequelize) {
        // entidad para los jugadores
        await queryInterface.createTable('players', {
            // Identificador único del jugador
            id: {
                type: Sequelize.INTEGER,
                primaryKey: true,
                autoIncrement: true,
                allowNull: false,
            },
            // Identificador único del jugador
            guid: {
                type: Sequelize.UUID,
                defaultValue: Sequelize.UUIDV4,
                unique: true,
                allowNull: false,
            },
            // equipo al que pertenece el jugador
            team_guid: {
                type: Sequelize.UUID,
            },
            // Nombre del jugador
            first_name: {
                type: Sequelize.TEXT,
            },
            // Apellido del jugador
            last_name: {
                type: Sequelize.TEXT,
            },
            // edad del jugador
            age: {
                type: Sequelize.INTEGER,
            },
            // fecha de nacimiento del jugador
            birthdate: {
                type: Sequelize.DATE,
            },
            // numero de dorsla del jugador
            dorsal_number: {
                type: Sequelize.INTEGER,
            },
            // posicion del jugador
            position: {
                type: Sequelize.TEXT,
            },
            // altura del jugador
            height: {
                type: Sequelize.INTEGER,
            },
            // peso del jugador
            weight: {
                type: Sequelize.INTEGER,
            },
            // pie habil del jugador
            skilled_foot: {
                type: Sequelize.TEXT,
            },
            // rasgos de personalidad del jugador
            personality_traits: {
                type: Sequelize.TEXT,
            },
            // imagen del jugador
            image_path: {
                type: Sequelize.TEXT,
            },
            // numero de telefono del jugador
            phone_number: {
                type: Sequelize.BIGINT,
            },
            // pais de origen del jugador
            country: {
                type: Sequelize.TEXT,
            },
            // ciudad de origen del jugador
            city: {
                type: Sequelize.TEXT,
            },
            // estado de origen del jugador
            state: {
                type: Sequelize.TEXT,
            },
            // nacionalidad del jugador
            nacionality: {
                type: Sequelize.TEXT,
            },
            // link de facebook del jugador
            facebook: {
                type: Sequelize.TEXT,
            },
            // link de instagram del jugador
            instagram: {
                type: Sequelize.TEXT,
            },
            // estado del jugador
            is_active: {
                type: Sequelize.BOOLEAN,
                defaultValue: 1, // valor por defecto
            },
            // usuario que creo el registro
            created_by: {
                type: Sequelize.UUID,
            },
            // fecha de creacion
            created_at: {
                type: Sequelize.DATE,
                defaultValue: Sequelize.literal('CURRENT_TIMESTAMP'),
            },
            // usuario que actualizo el registro
            updated_by: {
                type: Sequelize.UUID,
            },
            // fecha de actualizacion
            updated_at: {
                type: Sequelize.DATE,
            },
            // usuario que elimino el registro
            deleted_by: {
                type: Sequelize.UUID,
            },
            // fecha de eliminacion
            deleted_at: {
                type: Sequelize.DATE,
            },
        })

        // indeces
        await queryInterface.addIndex('players', ['guid'], {
            unique: true,
            name: 'uniq_players_guid',
        })

        await queryInterface.addIndex('players', ['team_guid'], {
            name: 'idx_team_guid',
        })

        // llave foranea
        await queryInterface.addConstraint('players', {
            fields: ['team_guid'],
            type: 'foreign key',
            name: 'fk_players_team',
            references: {
                table: 'teams',
                field: 'guid',
            },
            onDelete: 'cascade',
            onUpdate: 'cascade',
        })
    },

    async down(queryInterface, Sequelize) {
        await queryInterface.removeConstraint('players', 'fk_players_team')
        await queryInterface.removeIndex('players', 'uniq_players_guid')
        await queryInterface.removeIndex('players', 'idx_team_guid')
        await queryInterface.dropTable('players')
    },
}
