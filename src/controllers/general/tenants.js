const { matchedData } = require('express-validator')
const TenantsModel = require('../../models/general/tenants')
const { handleHttpError } = require('../../utils/handleError')
const { handleHttpSuccess } = require('../../utils/handleSuccess')
const { handleHttpCount } = require('../../utils/handleCount')
const {
    createDatabase,
    runMigrations,
    createUserWithPermissions,
    insertTenantCredentials,
    insertClientData,
    runSeeders,
} = require('../../services/tenantService')
const { v4: uuidv4 } = require('uuid')
const { encrypt } = require('../../utils/handlePassword')

/**
 * Este controlador es el encargado de crear tenants
 * @param {*} req
 * @param {*} res
 */
const CreateTenant = async (req, res) => {
    const transaction = await TenantsModel.sequelize.transaction() // Iniciar una transacción para asegurar la consistencia de datos.
    try {
        const data = matchedData(req) // Obtener los datos validados del request.
        const created_by = req.user.guid // Agregar el usuario que creó el tenant
        const created_at = new Date() // Agregar la fecha de creación
        const first_name = req.user.first_name // Obtener el nombre del usuario
        const last_name = req.user.last_name // Obtener el apellido del usuario
        const username = req.user.username // Obtener el nombre de usuario del usuario
        const isVerified = 1 // Definir que el usuario está verificado
        data.created_by = created_by // Agregar el usuario que actualizó el tenant
        data.created_at = created_at // Agregar la fecha de actualización
        data.first_name = first_name // Agregar el nombre del usuario
        data.last_name = last_name // Agregar el apellido del usuario
        data.username = username // Agregar el nombre de usuario del usuario
        data.isVerified = isVerified // Agregar que el usuario está verificado

        // Crear nuevo tenant en la tabla principal
        const tenant = await TenantsModel.create(data, { transaction })

        // Generar el nombre de la base de datos
        const dbName = `tenant_${created_at.getFullYear()}${data.company_name}${uuidv4().replace(/-/g, '')}`

        // Crear la nueva base de datos
        await createDatabase(dbName)

        // Correr las migraciones en la nueva base de datos
        await runMigrations(dbName)

        // Crear nuevo usuario de base de datos con permisos específicos
        const dbUser = `user_${data.company_name}${uuidv4()}`
        const dbPassword = await encrypt(uuidv4())
        await createUserWithPermissions(dbName, dbUser, dbPassword)

        // Insertar las credenciales del nuevo tenant en la tabla general
        await insertTenantCredentials(
            {
                tenant_guid: tenant.guid,
                domain: process.env.PUBLIC_URL,
                subdomain: data.company_name,
                database: dbName,
                user: dbUser,
                password: dbPassword,
                host: process.env.MYSQL_HOST,
                port: process.env.MYSQL_PORT,
                created_by: created_by,
                created_at: created_at,
            },
            { transaction }
        )

        // Insertar los datos del nuevo cliente en la nueva base de datos
        await insertClientData(dbName, data, { transaction })

        // Confirmar la transacción
        await transaction.commit()

        return handleHttpSuccess(res, 'Se ha creado el tenant.', 201, tenant) // Retornar el tenant creado
    } catch (error) {
        console.log(error)
        return handleHttpError(res, '', 403, error)
    }
}

/**
 * Este controlador es el encargado de obtener todos los tenants activos
 * @param {*} req
 * @param {*} res
 */
const Tenants = async (req, res) => {
    try {
        const tenants = await TenantsModel.findAll({
            where: { is_active: 1 },
        })
        if (!tenants) {
            return handleHttpError(res, 'El tenant no existe.', 404)
        }
        const message = tenants.length
            ? 'Se han encontrado los datos.'
            : 'No se encontraron datos.'
        return handleHttpSuccess(res, message, 201, tenants)
    } catch (error) {
        console.log(error)
        return handleHttpError(res, '', 403, error)
    }
}

/**
 * Este controlador es el encargado de obtener tenants por guid
 * @param {*} req
 * @param {*} res
 */
const Tenant = async (req, res) => {
    try {
        const { guid } = req.params
        const DataTenant = await TenantsModel.findOne({
            where: { guid },
        })
        if (!DataTenant) {
            return handleHttpError(res, 'El tenant no existe.', 404)
        }
        return handleHttpSuccess(
            res,
            'Se han encontrado los datos.',
            201,
            DataTenant
        )
    } catch (error) {
        console.log(error)
        return handleHttpError(res, '', 403, error)
    }
}

/**
 * Este controlador es el encargado de actualizar tenants
 * @param {*} req
 * @param {*} res
 */
const UpdateTenant = async (req, res) => {
    try {
        const { guid } = req.params
        const updated_by = req.user.guid
        const updated_at = new Date()
        const data = matchedData(req)
        data.updated_by = updated_by
        data.updated_at = updated_at
        const [updateCount] = await TenantsModel.update(data, {
            where: { guid },
        })
        if (updateCount === 0) {
            return handleHttpError(res, 'El tenant no existe.', 404)
        }
        const updatedTenant = await TenantsModel.findOne({
            where: { guid },
        })
        if (!updatedTenant) {
            return handleHttpError(
                res,
                'Error al obtener el tenant actualizado.',
                500
            )
        }
        return handleHttpSuccess(
            res,
            'Se ha actualizado el tenant.',
            201,
            updatedTenant
        )
    } catch (error) {
        console.log(error)
        return handleHttpError(res, '', 403, error)
    }
}

/**
 * Este controlador es el encargado de eliminar tenants
 * @param {*} req
 * @param {*} res
 */
const DeleteTenant = async (req, res) => {
    try {
        const { guid } = req.params
        const deleted_by = req.user.guid
        const deleted_at = new Date()
        const [updateCount] = await TenantsModel.update(
            {
                is_active: 0,
                deleted_by,
                deleted_at,
            },
            {
                where: { guid },
            }
        )
        if (updateCount === 0) {
            return handleHttpError(res, 'El tenant no existe.', 404)
        }
        handleHttpSuccess(res, 'Se ha eliminado el tenant.', 201)
    } catch (error) {
        console.log(error)
        handleHttpError(res, '', 403, error)
    }
}

/**
 * Este controlador es el encargado de desactivar tenants
 * @param {*} req
 * @param {*} res
 */
const DisableTenant = async (req, res) => {
    try {
        const { guid } = req.params
        const updated_by = req.user.guid
        const updated_at = new Date()
        const [updateCount] = await TenantsModel.update(
            {
                is_active: 0,
                updated_by,
                updated_at,
            },
            {
                where: { guid },
            }
        )
        if (updateCount === 0) {
            return handleHttpError(res, 'El tenant no existe.', 404)
        }
        handleHttpSuccess(res, 'Se ha desactivado el tenant.', 201)
    } catch (error) {
        console.log(error)
        handleHttpError(res, '', 403, error)
    }
}

/**
 * Este controlador es el encargado de activar tenants
 * @param {*} req
 * @param {*} res
 */
const EnableTenant = async (req, res) => {
    try {
        const { guid } = req.params
        const updated_by = req.user.guid
        const updated_at = new Date()
        const [updateCount] = await TenantsModel.update(
            {
                is_active: 1,
                updated_by,
                updated_at,
            },
            {
                where: { guid },
            }
        )
        if (updateCount === 0) {
            return handleHttpError(res, 'El tenant no existe.', 404)
        }
        handleHttpSuccess(res, 'Se ha activado el tenant.', 201)
    } catch (error) {
        console.log(error)
        handleHttpError(res, '', 403, error)
    }
}

/**
 * Este controlador es el encargado de obtener todos los tenants
 * @param {*} req
 * @param {*} res
 */
const TenantsAll = async (req, res) => {
    try {
        const DataTenants = await TenantsModel.findAll()
        if (!DataTenants) {
            handleHttpError(res, 'No se encontraron datos.', 404)
            return
        }
        const message = DataTenants.length
            ? 'Se han encontrado los datos.'
            : 'No se encontraron datos.'
        handleHttpSuccess(res, message, 201, DataTenants)
    } catch (error) {
        console.log(error)
        handleHttpError(res, '', 403, error)
    }
}

/**
 * Este controlador es el encargado de contar todos los tenants
 * @param {*} req
 * @param {*} res
 */
const CountAllTenants = async (req, res) => {
    try {
        const DataTenants = await TenantsModel.count()
        if (!DataTenants) {
            handleHttpError(res, 'No se encontraron datos.', 404)
            return
        }
        handleHttpCount(res, 'Se han encontrado los datos.', 201, DataTenants)
    } catch (error) {
        console.log(error)
        handleHttpError(res, '', 403, error)
    }
}

module.exports = {
    CreateTenant,
    Tenants,
    Tenant,
    UpdateTenant,
    DeleteTenant,
    DisableTenant,
    EnableTenant,
    TenantsAll,
    CountAllTenants,
}
