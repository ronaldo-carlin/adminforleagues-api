const jwt = require('jsonwebtoken')

const JWT_SECRET = process.env.JWT_SECRET
const JWT_TIME_EXPIRE = process.env.JWT_TIME_EXPIRE
const JWT_SECRET_REFRESH = process.env.JWT_SECRET_REFRESH
const JWT_TIME_EXPIRE_REFRESH = process.env.JWT_TIME_EXPIRE_REFRESH
const JWT_TIME_EXPIRE_RESET_PASSWORD =
    process.env.JWT_TIME_EXPIRE_RESET_PASSWORD

/**
 * Objeto de usuario
 * @param {*} user
 */

// si isAccessToken es true, se genera un token de acceso, de lo contrario, se genera un token de actualización
const tokenSign = async (user, isAccessToken) => {
    const payload = isAccessToken
        ? {
              id: user.id,
              guid: user.guid,
              role_guid: user.role_guid,
              email: user.email,
              username: user.username,
              firstname: user.firstname,
              lastname: user.lastname,
              phone: user.phone,
              image_path: user.image_path,
              picture: user.picture,
              google_id: user.google_id,
          }
        : { id: user.id, guid: user.guid }

    const secret = isAccessToken ? JWT_SECRET : JWT_SECRET_REFRESH
    const expiresIn = isAccessToken ? JWT_TIME_EXPIRE : JWT_TIME_EXPIRE_REFRESH

    const token = jwt.sign(payload, secret, { expiresIn, algorithm: 'HS256' })

    return token
}

const resetPasswordToken = async (user) => {
    const resetPassword = jwt.sign(
        {
            userId: user.id,
            resetPassword: true,
        },
        JWT_SECRET,
        {
            expiresIn: JWT_TIME_EXPIRE_RESET_PASSWORD, // Token de restablecimiento de contraseña válido por 1 hora
        }
    )
    return resetPassword
}

/**
 * Pasar token de sesion
 * @param {*} tokenJwt
 * @returns
 */
const verifyAccessToken = async (tokenJwt) => {
    try {
        return jwt.verify(tokenJwt, JWT_SECRET)
    } catch (err) {
        return null
    }
}

const verifyRefreshToken = async (tokenJwt) => {
    try {
        return jwt.verify(tokenJwt, JWT_SECRET_REFRESH)
    } catch (err) {
        return null
    }
}

module.exports = {
    tokenSign,
    verifyAccessToken,
    verifyRefreshToken,
    resetPasswordToken,
}
