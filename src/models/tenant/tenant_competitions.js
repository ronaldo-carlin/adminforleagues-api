// Importar Sequelize
const { DataTypes } = require('sequelize')
const CategoriesTenantModelFactory = require('./tenant_categories')
const SeasonsTenantModelFactory = require('./tenant_seasons')
const SportsTenantModelFactory = require('./tenant_sports')
const TypeCompetitionsTenantModelFactory = require('./tenant_types_competitions')

// Definir la función factory para el modelo CompetitionsTenantModel
const CompetitionsTenantModelFactory = (sequelize) => {
    const CategoriesTenantModel = CategoriesTenantModelFactory(sequelize)
    const SeasonsTenantModel = SeasonsTenantModelFactory(sequelize)
    const SportsTenantModel = SportsTenantModelFactory(sequelize)
    const TypeCompetitionsTenantModel =
        TypeCompetitionsTenantModelFactory(sequelize)

    const CompetitionsTenantModel = sequelize.define(
        'competitions',
        {
            id: {
                type: DataTypes.INTEGER,
                primaryKey: true,
                autoIncrement: true,
            },
            guid: {
                type: DataTypes.UUID,
                primaryKey: true,
                defaultValue: DataTypes.UUIDV4,
                unique: true,
            },
            category_guid: {
                type: DataTypes.UUID,
            },
            season_guid: {
                type: DataTypes.UUID,
            },
            sport_guid: {
                type: DataTypes.UUID,
            },
            type_competition_guid: {
                type: DataTypes.UUID,
            },
            name: {
                type: DataTypes.TEXT,
            },
            description: {
                type: DataTypes.TEXT,
            },
            logo_path: {
                type: DataTypes.TEXT,
            },
            teams: {
                type: DataTypes.INTEGER,
            },
            players: {
                type: DataTypes.INTEGER,
            },
            players_field: {
                type: DataTypes.INTEGER,
            },
            start_date: {
                type: DataTypes.DATE,
            },
            end_date: {
                type: DataTypes.DATE,
            },
            site_web: {
                type: DataTypes.TEXT,
            },
            registration: {
                type: DataTypes.INTEGER,
            },
            cost_arbitration: {
                type: DataTypes.INTEGER,
            },
            payment_arbitration: {
                type: DataTypes.INTEGER,
            },
            payment_arbitration_default: {
                type: DataTypes.INTEGER,
            },
            is_active: {
                type: DataTypes.INTEGER,
            },
            created_by: {
                type: DataTypes.UUID,
            },
            updated_by: {
                type: DataTypes.UUID,
            },
            deleted_by: {
                type: DataTypes.UUID,
            },
            created_at: {
                type: DataTypes.DATE,
            },
            updated_at: {
                type: DataTypes.DATE,
            },
            deleted_at: {
                type: DataTypes.DATE,
            },
        },
        {
            tableName: 'competitions',
            timestamps: false,
            defaultScope: {
                attributes: {
                    exclude: [
                        'category_guid',
                        'season_guid',
                        'sport_guid',
                        'type_competition_guid',
                        'created_by',
                        'updated_by',
                        'deleted_by',
                        'created_at',
                        'updated_at',
                        'deleted_at',
                    ],
                },
                include: [
                    {
                        model: CategoriesTenantModel,
                        as: 'category',
                        attributes: ['guid', 'name'],
                    },
                    {
                        model: SeasonsTenantModel,
                        as: 'season',
                        attributes: ['guid', 'name'],
                    },
                    {
                        model: SportsTenantModel,
                        as: 'sport',
                        attributes: ['guid', 'name'],
                    },
                    {
                        model: TypeCompetitionsTenantModel,
                        as: 'type_competition',
                        attributes: ['guid', 'name'],
                    },
                ],
            },
        }
    )

    CompetitionsTenantModel.belongsTo(CategoriesTenantModel, {
        foreignKey: 'category_guid',
        targetKey: 'guid',
        as: 'category',
    })

    CompetitionsTenantModel.belongsTo(SeasonsTenantModel, {
        foreignKey: 'season_guid',
        targetKey: 'guid',
        as: 'season',
    })

    CompetitionsTenantModel.belongsTo(SportsTenantModel, {
        foreignKey: 'sport_guid',
        targetKey: 'guid',
        as: 'sport',
    })

    CompetitionsTenantModel.belongsTo(TypeCompetitionsTenantModel, {
        foreignKey: 'type_competition_guid',
        targetKey: 'guid',
        as: 'type_competition',
    })

    return CompetitionsTenantModel
}

module.exports = CompetitionsTenantModelFactory
