module.exports = {
    async up(queryInterface, Sequelize) {
        // entidad para ver las credenciales de los cliente que contratan los servicios
        await queryInterface.createTable('tenant_credentials', {
            // id
            id: {
                type: Sequelize.INTEGER,
                primaryKey: true,
                autoIncrement: true,
                allowNull: false,
            },
            // guid
            guid: {
                type: Sequelize.UUID,
                defaultValue: Sequelize.UUIDV4, // Genera un UUID por defecto
                unique: true,
                allowNull: false,
            },
            // guid del cliente
            tenant_guid: {
                type: Sequelize.UUID,
            },
            // dominio del cliente
            domain: {
                type: Sequelize.STRING(255),
                defaultValue: 'adminforleagues.com',
            },
            // subdominio del cliente
            subdomain: {
                type: Sequelize.STRING(255),
            },
            // nombre de la base de datos
            database: {
                type: Sequelize.STRING(255),
            },
            // nombre de usuario
            user: {
                type: Sequelize.STRING(255),
            },
            // contraseña
            password: {
                type: Sequelize.STRING(255),
            },
            // host
            host: {
                type: Sequelize.STRING(255),
            },
            // puerto
            port: {
                type: Sequelize.INTEGER,
            },
            // estado del registro
            is_active: {
                type: Sequelize.BOOLEAN,
            },
            // usuario que creo el registro
            created_by: {
                type: Sequelize.UUID,
            },
            // fecha de creacion
            created_at: {
                type: Sequelize.DATE,
                defaultValue: Sequelize.literal('CURRENT_TIMESTAMP'),
            },
            // usuario que actualizo el registro
            updated_by: {
                type: Sequelize.UUID,
            },
            // fecha de actualizacion
            updated_at: {
                type: Sequelize.DATE,
            },
            // usuario que elimino el registro
            deleted_by: {
                type: Sequelize.UUID,
            },
            // fecha de eliminacion
            deleted_at: {
                type: Sequelize.DATE,
            },
        })

        // Agregar índices y claves foráneas después de crear la tabla
        await queryInterface.addIndex('tenant_credentials', ['guid'], {
            unique: true,
            name: 'uniq_tenant_credentials_guid',
        })

        await queryInterface.addIndex('tenant_credentials', ['tenant_guid'], {
            name: 'idx_tenant_credentials_tenant_guid',
        })

        await queryInterface.addConstraint('tenant_credentials', {
            fields: ['tenant_guid'],
            type: 'foreign key',
            name: 'fk_tenant_credentials_tenant_guid',
            references: {
                table: 'tenants',
                field: 'guid',
            },
            onDelete: 'NO ACTION',
            onUpdate: 'NO ACTION',
        })
    },

    async down(queryInterface, Sequelize) {
        await queryInterface.removeConstraint(
            'tenant_credentials',
            'fk_tenant_credentials_tenant_guid'
        )
        await queryInterface.removeIndex(
            'tenant_credentials',
            'uniq_tenant_credentials_guid'
        )
        await queryInterface.removeIndex(
            'tenant_credentials',
            'idx_tenant_credentials_tenant_guid'
        )
        await queryInterface.dropTable('tenant_credentials')
    },
}
