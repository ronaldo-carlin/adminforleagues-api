const multer = require('multer')
const fs = require('fs')
const path = require('path')

const createStorage = (destinationFolder = 'users') => {
    console.log('createStorage -> destinationFolder', destinationFolder)
    return multer.diskStorage({
        destination: async function (req, file, cb) {
            const dir = path.join(__dirname, '..', 'storage', destinationFolder)

            if (!fs.existsSync(dir)) {
                await fs.promises.mkdir(dir, { recursive: true })
            }

            cb(null, dir)
        },
        filename: function (req, file, cb) {
            const ext = file.originalname.split('.').pop()
            const filename = `file-${Date.now()}.${ext}`
            cb(null, filename)
        },
    })
}

const uploadMiddleware = (destinationFolder) => {
    const storage = createStorage(destinationFolder)
    return multer({ storage })
}

module.exports = uploadMiddleware
