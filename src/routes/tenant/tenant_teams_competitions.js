const express = require('express')
const router = express.Router()

// Controladores y validadores para teams_competitions
const {
    validatorCreateTeamsCompetition,
    validatorTeamsCompetition,
    validatorUpdateTeamsCompetition,
    validatorDeleteTeamsCompetition,
    validatorDisableTeamsCompetition,
    validatorEnableTeamsCompetition,
} = require('../../validators/tenant/tenant_teams_competitions')

const {
    CreateTeamsCompetition,
    TeamsCompetitions,
    TeamsCompetition,
    UpdateTeamsCompetition,
    DeleteTeamsCompetition,
    DisableTeamsCompetition,
    EnableTeamsCompetition,
    TeamsCompetitionsAll,
    TeamsCompetitionsCount,
} = require('../../controllers/tenant/tenant_teams_competitions')

const authMiddleware = require('../../middleware/session_tenant')

/* Rutas protegidas que requieren autenticación */
/**
 * @swagger
 * tags:
 *   name: Tenant_Team_Competitions
 *   description: API para la gestión de competencias de equipos
 */

// Ruta para crear una nueva competencia de equipo
/**
 * @swagger
 * /tenant_teams_competitions/CreateTeamCompetition:
 *   post:
 *     summary: Crear una nueva competencia de equipo
 *     description: Esta ruta permite crear una nueva competencia de equipo con los detalles especificados.
 *     tags: [Tenant_Team_Competitions]
 *     requestBody:
 *       required: true
 *       content:
 *         application/json:
 *           schema:
 *             type: object
 *             properties:
 *               team_guid:
 *                 type: string
 *                 format: uuid
 *                 example: "e7b2c6c3-dc78-49f1-905b-9b8d48eeb5c2"
 *               competition_guid:
 *                 type: string
 *                 format: uuid
 *                 example: "76c26d26-53e5-4a05-b5c4-bb68f5c6b574"
 *               role:
 *                 type: string
 *                 example: "Participante"
 *     responses:
 *       201:
 *         description: Competencia de equipo creada exitosamente
 *         content:
 *           application/json:
 *             schema:
 *               $ref: '#/components/responses/success'
 *       400:
 *         description: Error en los datos enviados
 *         content:
 *           application/json:
 *             schema:
 *               $ref: '#/components/responses/BadRequest'
 *       403:
 *         description: Error de autenticación
 *         content:
 *           application/json:
 *             schema:
 *               $ref: '#/components/responses/UnauthorizedError'
 *       409:
 *         description: La competencia de equipo ya existe
 *         content:
 *           application/json:
 *             schema:
 *               $ref: '#/components/responses/ConflictError'
 *       500:
 *         description: Error en el servidor
 *         content:
 *           application/json:
 *             schema:
 *               $ref: '#/components/responses/InternalServerError'
 *     security:
 *        - bearerAuth: []
 */
router.post(
    '/CreateTeamsCompetition',
    authMiddleware,
    validatorCreateTeamsCompetition(),
    CreateTeamsCompetition
)

// Ruta para obtener todas las competencias de equipos activas
/**
 * @swagger
 * /tenant_teams_competitions/TeamCompetitions:
 *   get:
 *     summary: Obtener todas las competencias de equipos activas
 *     tags: [Tenant_Team_Competitions]
 *     responses:
 *       200:
 *         description: Competencias de equipos obtenidas exitosamente
 *         content:
 *           application/json:
 *             schema:
 *               $ref: '#/components/responses/success'
 *       403:
 *         description: Error de autenticación
 *         content:
 *           application/json:
 *             schema:
 *               $ref: '#/components/responses/UnauthorizedError'
 *       500:
 *         description: Error en el servidor
 *         content:
 *           application/json:
 *             schema:
 *               $ref: '#/components/responses/InternalServerError'
 *     security:
 *        - bearerAuth: []
 */
router.get('/TeamsCompetitions', authMiddleware, TeamsCompetitions)

// Ruta para obtener una competencia de equipo por GUID
/**
 * @swagger
 * /tenant_teams_competitions/TeamCompetition/{guid}:
 *   get:
 *     summary: Obtener una competencia de equipo por GUID
 *     tags: [Tenant_Team_Competitions]
 *     parameters:
 *       - in: path
 *         name: guid
 *         schema:
 *           type: string
 *           format: uuid
 *         required: true
 *         description: El GUID de la competencia de equipo
 *     responses:
 *       200:
 *         description: Competencia de equipo obtenida exitosamente
 *         content:
 *           application/json:
 *             schema:
 *               $ref: '#/components/responses/success'
 *       403:
 *         description: Error de autenticación
 *         content:
 *           application/json:
 *             schema:
 *               $ref: '#/components/responses/UnauthorizedError'
 *       404:
 *         description: Competencia de equipo no encontrada
 *         content:
 *           application/json:
 *             schema:
 *               $ref: '#/components/responses/NotFoundError'
 *       500:
 *         description: Error en el servidor
 *         content:
 *           application/json:
 *             schema:
 *               $ref: '#/components/responses/InternalServerError'
 *     security:
 *        - bearerAuth: []
 */
router.get(
    '/TeamsCompetition/:guid',
    authMiddleware,
    validatorTeamsCompetition(),
    TeamsCompetition
)

// Ruta para actualizar una competencia de equipo
/**
 * @swagger
 * /tenant_teams_competitions/UpdateTeamCompetition/{guid}:
 *   patch:
 *     summary: Actualizar una competencia de equipo
 *     tags: [Tenant_Team_Competitions]
 *     requestBody:
 *       required: true
 *       content:
 *         application/json:
 *           schema:
 *             type: object
 *             properties:
 *               team_guid:
 *                 type: string
 *                 format: uuid
 *                 example: "e7b2c6c3-dc78-49f1-905b-9b8d48eeb5c2"
 *               competition_guid:
 *                 type: string
 *                 format: uuid
 *                 example: "76c26d26-53e5-4a05-b5c4-bb68f5c6b574"
 *               role:
 *                 type: string
 *                 example: "Participante"
 *     responses:
 *       200:
 *         description: Competencia de equipo actualizada exitosamente
 *         content:
 *           application/json:
 *             schema:
 *               $ref: '#/components/responses/success'
 *       400:
 *         description: Error en los datos enviados
 *         content:
 *           application/json:
 *             schema:
 *               $ref: '#/components/responses/BadRequest'
 *       403:
 *         description: Error de autenticación
 *         content:
 *           application/json:
 *             schema:
 *               $ref: '#/components/responses/UnauthorizedError'
 *       404:
 *         description: Competencia de equipo no encontrada
 *         content:
 *           application/json:
 *             schema:
 *               $ref: '#/components/responses/NotFoundError'
 *       500:
 *         description: Error en el servidor
 *         content:
 *           application/json:
 *             schema:
 *               $ref: '#/components/responses/InternalServerError'
 *     security:
 *        - bearerAuth: []
 */
router.patch(
    '/UpdateTeamsCompetition/:guid',
    authMiddleware,
    validatorUpdateTeamsCompetition(),
    UpdateTeamsCompetition
)

// Ruta para eliminar una competencia de equipo
/**
 * @swagger
 * /tenant_teams_competitions/DeleteTeamCompetition/{guid}:
 *   delete:
 *     summary: Eliminar una competencia de equipo
 *     tags: [Tenant_Team_Competitions]
 *     parameters:
 *       - in: path
 *         name: guid
 *         schema:
 *           type: string
 *           format: uuid
 *         required: true
 *         description: El GUID de la competencia de equipo
 *     responses:
 *       200:
 *         description: Competencia de equipo eliminada exitosamente
 *         content:
 *           application/json:
 *             schema:
 *               $ref: '#/components/responses/success'
 *       400:
 *         description: Error en los datos enviados
 *         content:
 *           application/json:
 *             schema:
 *               $ref: '#/components/responses/BadRequest'
 *       403:
 *         description: Error de autenticación
 *         content:
 *           application/json:
 *             schema:
 *               $ref: '#/components/responses/UnauthorizedError'
 *       404:
 *         description: Competencia de equipo no encontrada
 *         content:
 *           application/json:
 *             schema:
 *               $ref: '#/components/responses/NotFoundError'
 *       500:
 *         description: Error en el servidor
 *         content:
 *           application/json:
 *             schema:
 *               $ref: '#/components/responses/InternalServerError'
 *     security:
 *        - bearerAuth: []
 */
router.delete(
    '/DeleteTeamsCompetition/:guid',
    authMiddleware,
    validatorDeleteTeamsCompetition(),
    DeleteTeamsCompetition
)

// Ruta para deshabilitar una competencia de equipo
/**
 * @swagger
 * /tenant_teams_competitions/DisableTeamCompetition/{guid}:
 *   patch:
 *     summary: Deshabilitar una competencia de equipo
 *     tags: [Tenant_Team_Competitions]
 *     parameters:
 *       - in: path
 *         name: guid
 *         schema:
 *           type: string
 *           format: uuid
 *         required: true
 *         description: El GUID de la competencia de equipo
 *     responses:
 *       200:
 *         description: Competencia de equipo deshabilitada exitosamente
 *         content:
 *           application/json:
 *             schema:
 *               $ref: '#/components/responses/success'
 *       400:
 *         description: Error en los datos enviados
 *         content:
 *           application/json:
 *             schema:
 *               $ref: '#/components/responses/BadRequest'
 *       403:
 *         description: Error de autenticación
 *         content:
 *           application/json:
 *             schema:
 *               $ref: '#/components/responses/UnauthorizedError'
 *       404:
 *         description: Competencia de equipo no encontrada
 *         content:
 *           application/json:
 *             schema:
 *               $ref: '#/components/responses/NotFoundError'
 *       500:
 *         description: Error en el servidor
 *         content:
 *           application/json:
 *             schema:
 *               $ref: '#/components/responses/InternalServerError'
 *     security:
 *        - bearerAuth: []
 */
router.patch(
    '/DisableTeamsCompetition/:guid',
    authMiddleware,
    validatorDisableTeamsCompetition(),
    DisableTeamsCompetition
)

// Ruta para habilitar una competencia de equipo
/**
 * @swagger
 * /tenant_teams_competitions/EnableTeamCompetition/{guid}:
 *   patch:
 *     summary: Habilitar una competencia de equipo
 *     tags: [Tenant_Team_Competitions]
 *     parameters:
 *       - in: path
 *         name: guid
 *         schema:
 *           type: string
 *           format: uuid
 *         required: true
 *         description: El GUID de la competencia de equipo
 *     responses:
 *       200:
 *         description: Competencia de equipo habilitada exitosamente
 *         content:
 *           application/json:
 *             schema:
 *               $ref: '#/components/responses/success'
 *       400:
 *         description: Error en los datos enviados
 *         content:
 *           application/json:
 *             schema:
 *               $ref: '#/components/responses/BadRequest'
 *       403:
 *         description: Error de autenticación
 *         content:
 *           application/json:
 *             schema:
 *               $ref: '#/components/responses/UnauthorizedError'
 *       404:
 *         description: Competencia de equipo no encontrada
 *         content:
 *           application/json:
 *             schema:
 *               $ref: '#/components/responses/NotFoundError'
 *       500:
 *         description: Error en el servidor
 *         content:
 *           application/json:
 *             schema:
 *               $ref: '#/components/responses/InternalServerError'
 *     security:
 *        - bearerAuth: []
 */
router.patch(
    '/EnableTeamsCompetition/:guid',
    authMiddleware,
    validatorEnableTeamsCompetition(),
    EnableTeamsCompetition
)

// Ruta para obtener el conteo total de competencias de equipos
/**
 * @swagger
 * /tenant_teams_competitions/TeamCompetitionsCount:
 *   get:
 *     summary: Obtener el conteo total de competencias de equipos
 *     tags: [Tenant_Team_Competitions]
 *     responses:
 *       200:
 *         description: Conteo total de competencias de equipos obtenido exitosamente
 *         content:
 *           application/json:
 *             schema:
 *               $ref: '#/components/responses/success'
 *       403:
 *         description: Error de autenticación
 *         content:
 *           application/json:
 *             schema:
 *               $ref: '#/components/responses/UnauthorizedError'
 *       500:
 *         description: Error en el servidor
 *         content:
 *           application/json:
 *             schema:
 *               $ref: '#/components/responses/InternalServerError'
 *     security:
 *        - bearerAuth: []
 */
router.get('/TeamsCompetitionsCount', authMiddleware, TeamsCompetitionsCount)

// Ruta para obtener todas las competencias de equipos (deshabilitadas y activas)
/**
 * @swagger
 * /tenant_teams_competitions/TeamCompetitionsAll:
 *   get:
 *     summary: Obtener todas las competencias de equipos (deshabilitadas y activas)
 *     tags: [Tenant_Team_Competitions]
 *     responses:
 *       200:
 *         description: Competencias de equipos obtenidas exitosamente
 *         content:
 *           application/json:
 *             schema:
 *               $ref: '#/components/responses/success'
 *       403:
 *         description: Error de autenticación
 *         content:
 *           application/json:
 *             schema:
 *               $ref: '#/components/responses/UnauthorizedError'
 *       500:
 *         description: Error en el servidor
 *         content:
 *           application/json:
 *             schema:
 *               $ref: '#/components/responses/InternalServerError'
 *     security:
 *        - bearerAuth: []
 */
router.get('/TeamsCompetitionsAll', authMiddleware, TeamsCompetitionsAll)

module.exports = router
