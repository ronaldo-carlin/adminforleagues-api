const express = require('express')
const router = express.Router()

// Controladores y validadores para matches
const {
    validatorCreateMatch,
    validatorMatch,
    validatorUpdateMatch,
    validatorDeleteMatch,
    validatorDisableMatch,
    validatorEnableMatch,
} = require('../../validators/tenant/tenant_matches')

const {
    CreateMatch,
    Matches,
    Match,
    UpdateMatch,
    DeleteMatch,
    DisableMatch,
    EnableMatch,
    MatchesAll,
    MatchesCount,
} = require('../../controllers/tenant/tenant_matches')

const authMiddleware = require('../../middleware/session_tenant')

/* Rutas protegidas que requieren autenticación */
/**
 * @swagger
 * tags:
 *   name: Tenant_Matches
 *   description: API para la gestión de partidos
 */

// Ruta para crear un nuevo partido
/**
 * @swagger
 * /tenant_matches/CreateMatch:
 *   post:
 *     summary: Crear un nuevo partido
 *     description: Esta ruta permite crear un nuevo partido con los detalles especificados.
 *     tags: [Tenant_Matches]
 *     requestBody:
 *       required: true
 *       content:
 *         application/json:
 *           schema:
 *             type: object
 *             properties:
 *               team1_guid:
 *                 type: string
 *                 example: "495db854-849b-41cd-b6b9-9c84824c92ba"
 *               team2_guid:
 *                 type: string
 *                 example: "d2ebd854-849b-41cd-b6b9-9c84824c92bc"
 *               date:
 *                 type: string
 *                 format: date
 *                 example: "2024-09-15"
 *               location:
 *                 type: string
 *                 example: "Estadio Azteca"
 *     responses:
 *       201:
 *         description: Partido creado exitosamente
 *         content:
 *           application/json:
 *             schema:
 *               $ref: '#/components/responses/success'
 *       400:
 *         description: Error en los datos enviados
 *         content:
 *           application/json:
 *             schema:
 *               $ref: '#/components/responses/BadRequest'
 *       403:
 *         description: Error de autenticación
 *         content:
 *           application/json:
 *             schema:
 *               $ref: '#/components/responses/UnauthorizedError'
 *       409:
 *         description: El partido ya existe
 *         content:
 *           application/json:
 *             schema:
 *               $ref: '#/components/responses/ConflictError'
 *       500:
 *         description: Error en el servidor
 *         content:
 *           application/json:
 *             schema:
 *               $ref: '#/components/responses/InternalServerError'
 *     security:
 *        - bearerAuth: []
 */
router.post('/CreateMatch', authMiddleware, validatorCreateMatch(), CreateMatch)

// Ruta para obtener todos los partidos activos
/**
 * @swagger
 * /tenant_matches/Matches:
 *   get:
 *     summary: Obtener todos los partidos activos
 *     tags: [Tenant_Matches]
 *     responses:
 *       200:
 *         description: Partidos obtenidos exitosamente
 *         content:
 *           application/json:
 *             schema:
 *               $ref: '#/components/responses/success'
 *       403:
 *         description: Error de autenticación
 *         content:
 *           application/json:
 *             schema:
 *               $ref: '#/components/responses/UnauthorizedError'
 *       500:
 *         description: Error en el servidor
 *         content:
 *           application/json:
 *             schema:
 *               $ref: '#/components/responses/InternalServerError'
 *     security:
 *        - bearerAuth: []
 */
router.get('/Matches', authMiddleware, Matches)

// Ruta para obtener un partido por GUID
/**
 * @swagger
 * /tenant_matches/Match/{guid}:
 *   get:
 *     summary: Obtener un partido por GUID
 *     tags: [Tenant_Matches]
 *     parameters:
 *       - in: path
 *         name: guid
 *         schema:
 *           type: string
 *           format: uuid
 *         required: true
 *         description: El GUID del partido
 *     responses:
 *       200:
 *         description: Partido obtenido exitosamente
 *         content:
 *           application/json:
 *             schema:
 *               $ref: '#/components/responses/success'
 *       403:
 *         description: Error de autenticación
 *         content:
 *           application/json:
 *             schema:
 *               $ref: '#/components/responses/UnauthorizedError'
 *       404:
 *         description: Partido no encontrado
 *         content:
 *           application/json:
 *             schema:
 *               $ref: '#/components/responses/NotFoundError'
 *       500:
 *         description: Error en el servidor
 *         content:
 *           application/json:
 *             schema:
 *               $ref: '#/components/responses/InternalServerError'
 *     security:
 *        - bearerAuth: []
 */
router.get('/Match/:guid', authMiddleware, validatorMatch(), Match)

// Ruta para actualizar un partido
/**
 * @swagger
 * /tenant_matches/UpdateMatch/{guid}:
 *   patch:
 *     summary: Actualizar un partido
 *     tags: [Tenant_Matches]
 *     requestBody:
 *       required: true
 *       content:
 *         application/json:
 *           schema:
 *             type: object
 *             properties:
 *               team1_guid:
 *                 type: string
 *                 example: "495db854-849b-41cd-b6b9-9c84824c92ba"
 *               team2_guid:
 *                 type: string
 *                 example: "d2ebd854-849b-41cd-b6b9-9c84824c92bc"
 *               date:
 *                 type: string
 *                 format: date
 *                 example: "2024-09-15"
 *               location:
 *                 type: string
 *                 example: "Estadio Azteca"
 *     responses:
 *       200:
 *         description: Partido actualizado exitosamente
 *         content:
 *           application/json:
 *             schema:
 *               $ref: '#/components/responses/success'
 *       400:
 *         description: Error en los datos enviados
 *         content:
 *           application/json:
 *             schema:
 *               $ref: '#/components/responses/BadRequest'
 *       403:
 *         description: Error de autenticación
 *         content:
 *           application/json:
 *             schema:
 *               $ref: '#/components/responses/UnauthorizedError'
 *       404:
 *         description: Partido no encontrado
 *         content:
 *           application/json:
 *             schema:
 *               $ref: '#/components/responses/NotFoundError'
 *       500:
 *         description: Error en el servidor
 *         content:
 *           application/json:
 *             schema:
 *               $ref: '#/components/responses/InternalServerError'
 *     security:
 *        - bearerAuth: []
 */
router.patch(
    '/UpdateMatch/:guid',
    authMiddleware,
    validatorUpdateMatch(),
    UpdateMatch
)

// Ruta para eliminar un partido
/**
 * @swagger
 * /tenant_matches/DeleteMatch/{guid}:
 *   delete:
 *     summary: Eliminar un partido
 *     tags: [Tenant_Matches]
 *     parameters:
 *       - in: path
 *         name: guid
 *         schema:
 *           type: string
 *           format: uuid
 *         required: true
 *         description: El GUID del partido
 *     responses:
 *       200:
 *         description: Partido eliminado exitosamente
 *         content:
 *           application/json:
 *             schema:
 *               $ref: '#/components/responses/success'
 *       400:
 *         description: Error en los datos enviados
 *         content:
 *           application/json:
 *             schema:
 *               $ref: '#/components/responses/BadRequest'
 *       403:
 *         description: Error de autenticación
 *         content:
 *           application/json:
 *             schema:
 *               $ref: '#/components/responses/UnauthorizedError'
 *       404:
 *         description: Partido no encontrado
 *         content:
 *           application/json:
 *             schema:
 *               $ref: '#/components/responses/NotFoundError'
 *       500:
 *         description: Error en el servidor
 *         content:
 *           application/json:
 *             schema:
 *               $ref: '#/components/responses/InternalServerError'
 *     security:
 *        - bearerAuth: []
 */
router.delete(
    '/DeleteMatch/:guid',
    authMiddleware,
    validatorDeleteMatch(),
    DeleteMatch
)

// Ruta para deshabilitar un partido
/**
 * @swagger
 * /tenant_matches/DisableMatch/{guid}:
 *   patch:
 *     summary: Deshabilitar un partido
 *     tags: [Tenant_Matches]
 *     requestBody:
 *       required: true
 *       content:
 *         application/json:
 *           schema:
 *             type: object
 *             properties:
 *               guid:
 *                 type: uuid
 *                 example: "495db854-849b-41cd-b6b9-9c84824c92ba"
 *     responses:
 *       200:
 *         description: Partido deshabilitado exitosamente
 *         content:
 *           application/json:
 *             schema:
 *               $ref: '#/components/responses/success'
 *       400:
 *         description: Error en los datos enviados
 *         content:
 *           application/json:
 *             schema:
 *               $ref: '#/components/responses/BadRequest'
 *       403:
 *         description: Error de autenticación
 *         content:
 *           application/json:
 *             schema:
 *               $ref: '#/components/responses/UnauthorizedError'
 *       404:
 *         description: Partido no encontrado
 *         content:
 *           application/json:
 *             schema:
 *               $ref: '#/components/responses/NotFoundError'
 *       500:
 *         description: Error en el servidor
 *         content:
 *           application/json:
 *             schema:
 *               $ref: '#/components/responses/InternalServerError'
 *     security:
 *        - bearerAuth: []
 */
router.patch(
    '/DisableMatch/:guid',
    authMiddleware,
    validatorDisableMatch(),
    DisableMatch
)

// Ruta para habilitar un partido
/**
 * @swagger
 * /tenant_matches/EnableMatch/{guid}:
 *   patch:
 *     summary: Habilitar un partido
 *     tags: [Tenant_Matches]
 *     requestBody:
 *       required: true
 *       content:
 *         application/json:
 *           schema:
 *             type: object
 *             properties:
 *               guid:
 *                 type: uuid
 *                 example: "495db854-849b-41cd-b6b9-9c84824c92ba"
 *     responses:
 *       200:
 *         description: Partido habilitado exitosamente
 *         content:
 *           application/json:
 *             schema:
 *               $ref: '#/components/responses/success'
 *       400:
 *         description: Error en los datos enviados
 *         content:
 *           application/json:
 *             schema:
 *               $ref: '#/components/responses/BadRequest'
 *       403:
 *         description: Error de autenticación
 *         content:
 *           application/json:
 *             schema:
 *               $ref: '#/components/responses/UnauthorizedError'
 *       404:
 *         description: Partido no encontrado
 *         content:
 *           application/json:
 *             schema:
 *               $ref: '#/components/responses/NotFoundError'
 *       500:
 *         description: Error en el servidor
 *         content:
 *           application/json:
 *             schema:
 *               $ref: '#/components/responses/InternalServerError'
 *     security:
 *        - bearerAuth: []
 */
router.patch(
    '/EnableMatch/:guid',
    authMiddleware,
    validatorEnableMatch(),
    EnableMatch
)

// Ruta para obtener todos los partidos (activos e inactivos)
/**
 * @swagger
 * /tenant_matches/MatchesAll:
 *   get:
 *     summary: Obtener todos los partidos, tanto activos como inactivos
 *     tags: [Tenant_Matches]
 *     responses:
 *       200:
 *         description: Partidos obtenidos exitosamente
 *         content:
 *           application/json:
 *             schema:
 *               $ref: '#/components/responses/success'
 *       403:
 *         description: Error de autenticación
 *         content:
 *           application/json:
 *             schema:
 *               $ref: '#/components/responses/UnauthorizedError'
 *       500:
 *         description: Error en el servidor
 *         content:
 *           application/json:
 *             schema:
 *               $ref: '#/components/responses/InternalServerError'
 *     security:
 *        - bearerAuth: []
 */
router.get('/MatchesAll', authMiddleware, MatchesAll)

// Ruta para obtener el conteo de partidos
/**
 * @swagger
 * /tenant_matches/MatchesCount:
 *   get:
 *     summary: Obtener el conteo de partidos
 *     tags: [Tenant_Matches]
 *     responses:
 *       200:
 *         description: Conteo de partidos obtenido exitosamente
 *         content:
 *           application/json:
 *             schema:
 *               $ref: '#/components/responses/success'
 *       403:
 *         description: Error de autenticación
 *         content:
 *           application/json:
 *             schema:
 *               $ref: '#/components/responses/UnauthorizedError'
 *       500:
 *         description: Error en el servidor
 *         content:
 *           application/json:
 *             schema:
 *               $ref: '#/components/responses/InternalServerError'
 *     security:
 *        - bearerAuth: []
 */
router.get('/MatchesCount', authMiddleware, MatchesCount)

module.exports = router
