const { Op } = require('sequelize')
const { v4: uuidv4 } = require('uuid')
const { matchedData } = require('express-validator')
const { handleHttpError } = require('../../utils/handleError')
const { handleHttpSuccess } = require('../../utils/handleSuccess')
const { handleHttpCount } = require('../../utils/handleCount')
const { getSequelize, getRolesModel } = require('../../helpers/sequelizeHelper')

/**
 * Este controlador es el encargado de crear un rol
 * @param {*} req
 * @param {*} res
 */
const CreateRole = async (req, res) => {
    try {
        // obtenemos la instancia de sequelize del middleware
        const sequelize = getSequelize(req)
        // obtenemos el modelo de roles
        const RolesTenantModel = getRolesModel(sequelize)

        const data = matchedData(req)
        const guid = uuidv4()
        const created_by = req.user.guid
        const created_at = new Date()
        data.guid = guid
        data.created_by = created_by
        data.created_at = created_at
        const rol = await RolesTenantModel.create(data)
        handleHttpSuccess(res, 'Rol creado.', 201, rol)
    } catch (error) {
        console.log(error)
        handleHttpError(res, 'Error al crear el rol.', 403, error)
    }
}

/**
 * Este controlador es el encargado de obtener todos los roles activos
 * @param {*} req
 * @param {*} res
 */
const Roles = async (req, res) => {
    try {
        // obtenemos la instancia de sequelize del middleware
        const sequelize = getSequelize(req)
        // obtenemos el modelo de roles
        const RolesTenantModel = getRolesModel(sequelize)
        // Buscamos todos los roles activos
        const roles = await RolesTenantModel.findAll({
            where: { is_active: 1 },
        })
        if (!roles) {
            handleHttpError(res, 'No se encontraron roles activos.', 404)
            return
        }
        const message = roles.length
            ? 'Se han encontrado los roles activos.'
            : 'No se encontraron roles activos.'
        handleHttpSuccess(res, message, 201, roles)
    } catch (error) {
        console.log(error)
        handleHttpError(res, 'Error obteniendo roles activos.', 403, error)
    }
}

/**
 * Este controlador es el encargado de obtener un rol por su guid
 * @param {*} req
 * @param {*} res
 */
const Role = async (req, res) => {
    try {
        // obtenemos la instancia de sequelize del middleware
        const sequelize = getSequelize(req)
        // obtenemos el modelo de roles
        const RolesTenantModel = getRolesModel(sequelize)

        const { guid } = req.params
        // Buscamos el rol con el guid proporcionado
        const role = await RolesTenantModel.findOne({
            where: { guid },
        })

        //Si el rol no existe enviamos una respuesta de error
        if (!role) {
            handleHttpError(res, 'El rol no existe.', 404)
            return
        }
        //Si el rol esta inactivo enviamos una respuesta de error
        if (role.is_active == 0) {
            handleHttpError(res, 'El rol ha sido deshabilitado.', 401)
            return
        }

        //Si existe el rol lo enviamos al front
        handleHttpSuccess(res, 'Se ha encontrado el rol.', 201, role)
    } catch (error) {
        console.log(error)
        handleHttpError(res, 'Error obteniendo rol.', 403, error)
    }
}

/**
 * Este controlador es el encargado de actualizar un rol
 * @param {*} req
 * @param {*} res
 */
const UpdateRole = async (req, res) => {
    try {
        // obtenemos la instancia de sequelize del middleware
        const sequelize = getSequelize(req)
        // obtenemos el modelo de roles
        const RolesTenantModel = getRolesModel(sequelize)

        const { guid } = req.params
        const updated_by = req.user.guid
        const updated_at = new Date()
        const data = matchedData(req)
        data.updated_by = updated_by
        data.updated_at = updated_at

        const [updateCount] = await RolesTenantModel.update(data, {
            where: { guid },
        })

        if (updateCount === 0) {
            handleHttpError(res, 'El rol no existe.', 404)
            return
        }

        const updatedRole = await RolesTenantModel.findOne({ where: { guid } })
        if (!UpdateRole) {
            return handleHttpError(res, 'Error al obtener el rol.', 500)
        }
        handleHttpSuccess(res, 'Se ha actualizado el rol.', 201, updatedRole)
    } catch (error) {
        handleHttpError(res, 'Error al actualizar el rol.', 403, error)
    }
}

/**
 * Este controlador es el encargado de eliminar un rol
 * @param {*} req
 * @param {*} res
 */
const DeleteRole = async (req, res) => {
    try {
        // obtenemos la instancia de sequelize del middleware
        const sequelize = getSequelize(req)
        // obtenemos el modelo de roles
        const RolesTenantModel = getRolesModel(sequelize)
        const { guid } = req.params
        const deleted_by = req.user.guid
        const deleted_at = new Date()

        const [updateCount] = await RolesTenantModel.update(
            {
                is_active: 0,
                deleted_by,
                deleted_at,
            },
            {
                where: { guid },
            }
        )

        if (updateCount === 0) {
            handleHttpError(res, 'El rol no existe.', 404)
            return
        }

        handleHttpSuccess(res, 'Se ha eliminado el rol.', 201)
    } catch (error) {
        handleHttpError(res, 'Error al eliminar el rol.', 403, error)
    }
}

/**
 * Este controlador es el encargado de deshabilitar un rol
 * @param {*} req
 * @param {*} res
 */
const DisableRole = async (req, res) => {
    try {
        // obtenemos la instancia de sequelize del middleware
        const sequelize = getSequelize(req)
        // obtenemos el modelo de roles
        const RolesTenantModel = getRolesModel(sequelize)

        const { guid } = req.params
        const updated_by = req.user.guid
        const updated_at = new Date()

        const [updateCount] = await RolesTenantModel.update(
            {
                is_active: 0,
                updated_by,
                updated_at,
            },
            {
                where: { guid },
            }
        )

        if (updateCount === 0) {
            handleHttpError(res, 'El rol no existe.', 404)
            return
        }

        handleHttpSuccess(res, 'Se ha desactivado el rol.', 201)
    } catch (error) {
        handleHttpError(res, 'Error al deshabilitar el rol.', 403, error)
    }
}

/**
 * Este controlador es el encargado de habilitar un rol
 * @param {*} req
 * @param {*} res
 */
const EnableRole = async (req, res) => {
    try {
        // obtenemos la instancia de sequelize del middleware
        const sequelize = getSequelize(req)
        // obtenemos el modelo de roles
        const RolesTenantModel = getRolesModel(sequelize)

        const { guid } = req.params
        const updated_by = req.user.guid
        const updated_at = new Date()

        const [updateCount] = await RolesTenantModel.update(
            {
                is_active: 1,
                updated_by,
                updated_at,
            },
            {
                where: { guid },
            }
        )

        if (updateCount === 0) {
            handleHttpError(res, 'El rol no existe.', 404)
            return
        }

        handleHttpSuccess(res, 'Se ha activado el rol.', 201)
    } catch (error) {
        handleHttpError(res, 'Error al habilitar el rol.', 403, error)
    }
}

/**
 * Este controlador es el encargado de obtener todos los roles
 * @param {*} req
 * @param {*} res
 */
const RolesAll = async (req, res) => {
    try {
        // obtenemos la instancia de sequelize del middleware
        const sequelize = getSequelize(req)
        // obtenemos el modelo de roles
        const RolesTenantModel = getRolesModel(sequelize)
        // Obtenemos todos los roles de la base de datos
        const roles = await RolesTenantModel.findAll()
        if (!roles) {
            handleHttpError(res, 'No se encontraron datos.', 404)
            return
        }
        const message = roles.length
            ? 'Se han encontrado los datos.'
            : 'No se encontraron datos.'
        handleHttpSuccess(res, message, 201, roles)
    } catch (error) {
        console.log(error)
        handleHttpError(res, 'Error obteniendo roles.', 403, error)
    }
}

/**
 * Este controlador es el encargado de contar todos los roles
 * @param {*} req
 * @param {*} res
 */
const RolesCount = async (req, res) => {
    try {
        // obtenemos la instancia de sequelize del middleware
        const sequelize = getSequelize(req)
        // obtenemos el modelo de roles
        const RolesTenantModel = getRolesModel(sequelize)
        // Contamos todos los roles de la base de datos
        const rolesCount = await RolesTenantModel.count()
        if (!rolesCount) {
            handleHttpError(res, 'No se encontraron datos.', 404)
            return
        }
        handleHttpCount(res, 'Se han encontrado los datos.', 201, rolesCount)
    } catch (error) {
        console.log(error)
        handleHttpError(res, 'Error contando roles.', 403, error)
    }
}

module.exports = {
    CreateRole,
    Roles,
    Role,
    UpdateRole,
    DeleteRole,
    DisableRole,
    EnableRole,
    RolesAll,
    RolesCount,
}
