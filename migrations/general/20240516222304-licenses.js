module.exports = {
    async up(queryInterface, Sequelize) {
        // enttidad de licencias
        await queryInterface.createTable('licenses', {
            // identificador de la tabla
            id: {
                type: Sequelize.INTEGER,
                primaryKey: true,
                autoIncrement: true,
                allowNull: false,
            },
            // identificador de la licencia
            guid: {
                type: Sequelize.UUID,
                defaultValue: Sequelize.UUIDV4, // Genera un UUID por defecto
                unique: true,
                allowNull: false,
            },
            // guid del plan
            plan_guid: {
                type: Sequelize.UUID,
            },
            // nombre de la licencia
            name: {
                type: Sequelize.STRING,
            },
            // descripcion de la licencia
            description: {
                type: Sequelize.STRING,
            },
            // precio de la licencia
            price: {
                type: Sequelize.DOUBLE,
            },
            // cantidad máxima de usuarios permitidos
            users: {
                type: Sequelize.INTEGER,
            },
            // cantidad máxima de equipos permitidos por competicion
            teams: {
                type: Sequelize.INTEGER,
            },
            // cantidad máxima de partidos permitidos por competicion
            matches: {
                type: Sequelize.INTEGER,
            },
            // cantidad máxima de competiciones permitidas por licencia
            competitions: {
                type: Sequelize.INTEGER,
            },
            // cantidad máxima de sedes permitidas por competicion
            sedes: {
                type: Sequelize.INTEGER,
            },
            // cantidad máxima de patrocinadores permitidos por competicion
            sponsors: {
                type: Sequelize.INTEGER,
            },
            // cantidad máxima de publicaciones permitidas por competicion
            publications: {
                type: Sequelize.INTEGER,
            },
            // cantidad máxima de jugadores permitidos por competicion
            players: {
                type: Sequelize.INTEGER,
            },
            // cantidad máxima de entrenadores permitidos por competicion
            coaches: {
                type: Sequelize.INTEGER,
            },
            // cantidad máxima de árbitros permitidos por competicion
            referees: {
                type: Sequelize.INTEGER,
            },
            // cantidad máxima de informes permitidos por competicion
            reports: {
                type: Sequelize.INTEGER,
            },
            // duracion de la licencia en dias
            duration_days: {
                type: Sequelize.INTEGER,
            },
            // acceso ver el resumen de los partidos ('básicos', 'detallados')
            matches_summary: {
                type: Sequelize.STRING,
            },
            // acceso a imprimir informes de los partidos ('básicos', 'detallados')
            matches_reports: {
                type: Sequelize.STRING,
            },
            // ads permitidos en la licencia
            ads: {
                type: Sequelize.INTEGER,
            },
            // estado del registro
            is_active: {
                type: Sequelize.BOOLEAN,
                defaultValue: 1, // valor por defecto
            },
            // usuario que creo el registro
            created_by: {
                type: Sequelize.UUID,
            },
            // fecha de creacion
            created_at: {
                type: Sequelize.DATE,
                defaultValue: Sequelize.literal('CURRENT_TIMESTAMP'),
            },
            // usuario que actualizo el registro
            updated_by: {
                type: Sequelize.UUID,
            },
            // fecha de actualizacion
            updated_at: {
                type: Sequelize.DATE,
            },
            // usuario que elimino el registro
            deleted_by: {
                type: Sequelize.UUID,
            },
            // fecha de eliminacion
            deleted_at: {
                type: Sequelize.DATE,
            },
        })

        // Agregar índice único después de crear la tabla
        await queryInterface.addIndex('licenses', ['guid'], {
            unique: true,
            name: 'uniq_licenses_guid',
        })

        await queryInterface.addIndex('licenses', ['plan_guid'], {
            name: 'idx_licenses_plan_guid',
        })

        // Agregar clave foranea
        await queryInterface.addConstraint('licenses', {
            fields: ['plan_guid'],
            type: 'foreign key',
            name: 'fk_licenses_plan_guid',
            references: {
                table: 'plans',
                field: 'guid',
            },
            onDelete: 'cascade',
            onUpdate: 'cascade',
        })
    },

    async down(queryInterface, Sequelize) {
        await queryInterface.removeConstraint(
            'licenses',
            'fk_licenses_plan_guid'
        )
        await queryInterface.removeIndex('licenses', 'uniq_licenses_guid')
        await queryInterface.dropTable('licenses')
    },
}
