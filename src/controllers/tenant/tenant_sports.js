const { Op } = require('sequelize')
const { v4: uuidv4 } = require('uuid')
const { matchedData } = require('express-validator')
const { handleHttpSuccess } = require('../../utils/handleSuccess')
const { handleHttpError } = require('../../utils/handleError')
const { handleHttpCount } = require('../../utils/handleCount')
const {
    getSequelize,
    getSportsModel,
} = require('../../helpers/sequelizeHelper')

/**
 * Este controlador es el encargado de registrar un deporte
 * @param {*} req
 * @param {*} res
 */
const CreateSport = async (req, res) => {
    try {
        const sequelize = getSequelize(req)
        const SportModel = getSportsModel(sequelize)
        const data = matchedData(req)
        const created_by = req.user.guid
        const created_at = new Date()
        const guid = await uuidv4()
        data.created_by = created_by
        data.created_at = created_at
        data.guid = guid

        const sport = await SportModel.create(data)
        return handleHttpSuccess(res, 'Deporte creado.', 201, sport)
    } catch (error) {
        console.log('Error al crear el deporte:', error)
        handleHttpError(res, 'Error al crear el deporte.', 403, error)
    }
}

/**
 * Este controlador es el encargado de obtener los deportes activos
 * @param {*} req
 * @param {*} res
 */
const Sports = async (req, res) => {
    try {
        const sequelize = getSequelize(req)
        const SportModel = getSportsModel(sequelize)

        const sports = await SportModel.findAll({
            where: { is_active: 1 },
        })

        if (!sports.length) {
            handleHttpError(res, 'No se encontraron deportes.', 404)
            return
        }

        handleHttpSuccess(res, 'Deportes encontrados.', 201, sports)
    } catch (error) {
        console.log('Error obteniendo deportes:', error)
        handleHttpError(res, 'Error obteniendo deportes.', 403, error)
    }
}

/**
 * Este controlador es el encargado de obtener un deporte por GUID
 * @param {*} req
 * @param {*} res
 */
const Sport = async (req, res) => {
    try {
        const sequelize = getSequelize(req)
        const SportModel = getSportsModel(sequelize)
        const { guid } = req.params

        const sport = await SportModel.findOne({
            where: { guid },
        })

        if (!sport) {
            handleHttpError(res, 'El deporte no existe.', 404)
            return
        }

        if (sport.is_active == 0) {
            handleHttpError(res, 'El deporte ha sido deshabilitado.', 401)
            return
        }

        handleHttpSuccess(res, 'Deporte encontrado.', 201, sport)
    } catch (error) {
        console.log('Error obteniendo deporte:', error)
        handleHttpError(res, 'Error obteniendo deporte.', 403, error)
    }
}

/**
 * Este controlador es el encargado de actualizar un deporte
 * @param {*} req
 * @param {*} res
 */
const UpdateSport = async (req, res) => {
    try {
        const sequelize = getSequelize(req)
        const SportModel = getSportsModel(sequelize)
        const { guid } = req.params
        const data = matchedData(req)
        const updated_by = req.user.guid
        const updated_at = new Date()
        data.updated_by = updated_by
        data.updated_at = updated_at

        const [updateCount] = await SportModel.update(data, {
            where: { guid },
        })

        if (updateCount === 0) {
            return handleHttpError(res, 'El deporte no existe.', 404)
        }

        const sport = await SportModel.findOne({
            where: { guid },
        })

        if (!sport) {
            handleHttpError(res, 'El deporte no existe.', 404)
            return
        }

        if (sport.is_active == 0) {
            handleHttpError(res, 'El deporte ha sido deshabilitado.', 401)
            return
        }

        handleHttpSuccess(res, 'Deporte actualizado.', 201, sport)
    } catch (error) {
        console.log('Error actualizando deporte:', error)
        handleHttpError(res, 'Error actualizando deporte.', 403, error)
    }
}

/**
 * Este controlador es el encargado de eliminar un deporte
 * @param {*} req
 * @param {*} res
 */
const DeleteSport = async (req, res) => {
    try {
        const sequelize = getSequelize(req)
        const SportModel = getSportsModel(sequelize)
        const { guid } = req.params
        const deleted_by = req.user.guid
        const deleted_at = new Date()
        const [updateCount] = await SportModel.update(
            {
                is_active: 0,
                deleted_by,
                deleted_at,
            },
            {
                where: { guid },
            }
        )

        if (updateCount === 0) {
            return handleHttpError(res, 'El deporte no existe.', 404)
        }

        handleHttpSuccess(res, 'Deporte eliminado.', 201)
    } catch (error) {
        console.log('Error eliminando deporte:', error)
        handleHttpError(res, 'Error eliminando deporte.', 403, error)
    }
}

/**
 * Este controlador es el encargado de desactivar un deporte
 * @param {*} req
 * @param {*} res
 */
const DisableSport = async (req, res) => {
    try {
        const sequelize = getSequelize(req)
        const SportModel = getSportsModel(sequelize)
        const { guid } = req.params
        const updated_by = req.user.guid
        const updated_at = new Date()
        const [updateCount] = await SportModel.update(
            {
                is_active: 0,
                updated_by,
                updated_at,
            },
            {
                where: { guid },
            }
        )

        if (updateCount === 0) {
            return handleHttpError(res, 'El deporte no existe.', 404)
        }

        handleHttpSuccess(res, 'Deporte deshabilitado.', 201)
    } catch (error) {
        console.log('Error deshabilitando deporte:', error)
        handleHttpError(res, 'Error deshabilitando deporte.', 403, error)
    }
}

/**
 * Este controlador es el encargado de activar un deporte
 * @param {*} req
 * @param {*} res
 */
const EnableSport = async (req, res) => {
    try {
        const sequelize = getSequelize(req)
        const SportModel = getSportsModel(sequelize)
        const { guid } = req.params
        const updated_by = req.user.guid
        const updated_at = new Date()
        const [updateCount] = await SportModel.update(
            {
                is_active: 1,
                updated_by,
                updated_at,
            },
            {
                where: { guid },
            }
        )

        if (updateCount === 0) {
            return handleHttpError(res, 'El deporte no existe.', 404)
        }

        handleHttpSuccess(res, 'Deporte habilitado.', 201)
    } catch (error) {
        console.log('Error habilitando deporte:', error)
        handleHttpError(res, 'Error habilitando deporte.', 403, error)
    }
}

/**
 * Este controlador es el encargado de obtener todos los deportes
 * @param {*} req
 * @param {*} res
 */
const SportsAll = async (req, res) => {
    try {
        const sequelize = getSequelize(req)
        const SportModel = getSportsModel(sequelize)

        const sports = await SportModel.findAll()

        if (!sports.length) {
            handleHttpError(res, 'No se encontraron deportes.', 404)
            return
        }

        handleHttpSuccess(res, 'Deportes encontrados.', 201, sports)
    } catch (error) {
        console.log('Error obteniendo deportes:', error)
        handleHttpError(res, 'Error obteniendo deportes.', 403, error)
    }
}

/**
 * Este controlador es el encargado de contar todos los deportes
 * @param {*} req
 * @param {*} res
 */
const SportsCount = async (req, res) => {
    try {
        const sequelize = getSequelize(req)
        const SportModel = getSportsModel(sequelize)

        const count = await SportModel.count()

        handleHttpCount(res, 'Deportes encontrados.', 201, count)
    } catch (error) {
        console.log('Error contando deportes:', error)
        handleHttpError(res, 'Error contando deportes.', 403, error)
    }
}

module.exports = {
    CreateSport,
    Sports,
    Sport,
    UpdateSport,
    DeleteSport,
    DisableSport,
    EnableSport,
    SportsAll,
    SportsCount,
}
