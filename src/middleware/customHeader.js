const { handleHttpError } = require('../utils/handleError')

const customHeader = (req, res, next) => {
    try {
        const apiKey = req.headers['x-api-key']
        if (apiKey === 'd2a362f9c4e3ba891b5e7a95f8b2b3d4') {
            next()
        } else {
            handleHttpError(res, 'API_KEY_INCORRECTA.', 403)
        }
    } catch (error) {
        handleHttpError(res, 'PROBLEMAS_EN_LOS_CUSTOM_HEADER.', 403)
    }
}

module.exports = customHeader
