const { check, validationResult } = require('express-validator')

// Validaciones para crear un rol
const validatorCreateRole = () => {
    return [
        async (req, res, next) => {
            await check('name')
                .exists()
                .notEmpty()
                .isLength({ min: 1, max: 100 })
                .run(req)
            await check('description')
                .exists()
                .isLength({ min: 1, max: 255 })
                .run(req)

            const errors = validationResult(req)
            if (!errors.isEmpty()) {
                return res.status(400).json({ errors: errors.array() })
            }

            next()
        },
    ]
}

// Validaciones para obtener un rol
const validatorRole = () => {
    return [
        async (req, res, next) => {
            await check('guid')
                .isUUID()
                .withMessage('El GUID es requerido.')
                .run(req)

            const errors = validationResult(req)
            if (!errors.isEmpty()) {
                return res.status(400).json({ errors: errors.array() })
            }

            next()
        },
    ]
}

// Validaciones para actualizar un rol
const validatorUpdateRole = () => {
    return [
        async (req, res, next) => {
            await check('guid')
                .isUUID()
                .withMessage('El GUID es requerido.')
                .run(req)
            await check('name')
                .exists()
                .notEmpty()
                .isLength({ min: 1, max: 100 })
                .run(req)
            await check('description')
                .exists()
                .isLength({ min: 1, max: 255 })
                .run(req)

            const errors = validationResult(req)
            if (!errors.isEmpty()) {
                return res.status(400).json({ errors: errors.array() })
            }

            next()
        },
    ]
}

// Validaciones para eliminar un rol
const validatorDeleteRole = () => {
    return [
        async (req, res, next) => {
            await check('guid')
                .isUUID()
                .withMessage('El GUID es requerido.')
                .run(req)

            const errors = validationResult(req)
            if (!errors.isEmpty()) {
                return res.status(400).json({ errors: errors.array() })
            }

            next()
        },
    ]
}

// Validaciones para deshabilitar un rol
const validatorDisableRole = () => {
    return [
        async (req, res, next) => {
            await check('guid')
                .isUUID()
                .withMessage('El GUID es requerido.')
                .run(req)

            const errors = validationResult(req)
            if (!errors.isEmpty()) {
                return res.status(400).json({ errors: errors.array() })
            }

            next()
        },
    ]
}

// Validaciones para habilitar un rol
const validatorEnableRole = () => {
    return [
        async (req, res, next) => {
            await check('guid')
                .isUUID()
                .withMessage('El GUID es requerido.')
                .run(req)

            const errors = validationResult(req)
            if (!errors.isEmpty()) {
                return res.status(400).json({ errors: errors.array() })
            }

            next()
        },
    ]
}

module.exports = {
    validatorCreateRole,
    validatorRole,
    validatorUpdateRole,
    validatorDeleteRole,
    validatorDisableRole,
    validatorEnableRole,
}
