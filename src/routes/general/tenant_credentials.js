const express = require('express')
const { matchedData } = require('express-validator')
const router = express.Router()

// validaciones
const {
    ValidatorCreateTenantCredential,
    ValidatorUpdateTenantCredential,
    ValidatorDeleteTenantCredential,
    ValidatorDisableTenantCredential,
    ValidatorEnableTenantCredential,
    ValidatorRunSeedersTenant,
} = require('../../validators/general/tenant_credentials')

// Importa los controladores
const {
    CreateTenantCredential,
    TenantCredentials,
    TenantCredential,
    UpdateTenantCredential,
    DeleteTenantCredential,
    DisableTenantCredential,
    EnableTenantCredential,
    TenantCredentialsAll,
    CountTenantCredentials,
    RunSeedersTenant,
} = require('../../controllers/general/tenant_credentials')

// Middleware de autenticación
const authMiddleware = require('../../middleware/session')

// Rutas protegidas que requieren autenticación
/**
 * @swagger
 * tags:
 *   name: Tenant_Credentials
 *   description: API para la gestión de credenciales de tenants
 */

// Ruta para crear una nueva credencial de tenant
/**
 * @swagger
 * /tenant_credentials/CreateTenantCredential:
 *   post:
 *     summary: Crear una nueva credencial de tenant
 *     description: Esta ruta permite crear una nueva credencial de tenant.
 *     tags: [Tenant_Credentials]
 *     requestBody:
 *       required: true
 *       content:
 *         application/json:
 *           schema:
 *             type: object
 *             properties:
 *               tenant_id:
 *                 type: uuid
 *                 example: '123e4567-e89b-12d3-a456-426614174000'
 *     responses:
 *       201:
 *         description: Credencial de tenant creada exitosamente
 *         content:
 *           application/json:
 *             schema:
 *               $ref: '#/components/responses/success'
 *       400:
 *         description: Error en los datos enviados
 *         content:
 *           application/json:
 *             schema:
 *               $ref: '#/components/responses/BadRequest'
 *       403:
 *         description: Error de autenticación
 *         content:
 *           application/json:
 *             schema:
 *               $ref: '#/components/responses/UnauthorizedError'
 *       409:
 *         description: Credencial de tenant ya existe
 *         content:
 *           application/json:
 *             schema:
 *               $ref: '#/components/responses/ConflictError'
 *       500:
 *         description: Error en el servidor
 *         content:
 *           application/json:
 *             schema:
 *               $ref: '#/components/responses/InternalServerError'
 *     security:
 *        - bearerAuth: []
 */
router.post(
    '/CreateTenantCredential',
    authMiddleware,
    ValidatorCreateTenantCredential,
    CreateTenantCredential
)

// Ruta para obtener todas las credenciales de tenants activas
/**
 * @swagger
 *  /tenant_credentials/TenantCredentials:
 *   get:
 *     summary: Obtener todas las credenciales de tenants activas
 *     tags: [Tenant_Credentials]
 *     responses:
 *       201:
 *         description: Credenciales obtenidas exitosamente
 *         content:
 *           application/json:
 *             schema:
 *               $ref: '#/components/responses/success'
 *       403:
 *         description: Error de autenticación
 *         content:
 *           application/json:
 *             schema:
 *               $ref: '#/components/responses/UnauthorizedError'
 *       500:
 *         description: Error en el servidor
 *         content:
 *           application/json:
 *             schema:
 *               $ref: '#/components/responses/InternalServerError'
 *     security:
 *        - bearerAuth: []
 */
router.get('/TenantCredentials', authMiddleware, TenantCredentials)

// Ruta para obtener una credencial de tenant
/**
 * @swagger
 * /tenant_credentials/TenantCredential:
 *   get:
 *     summary: Obtener una credencial de tenant por GUID
 *     tags: [Tenant_Credentials]
 *     parameters:
 *       - in: path
 *         name: guid
 *         schema:
 *           type: uuid
 *           format: uuid
 *         required: true
 *         description: El GUID de la credencial de tenant
 *     responses:
 *       201:
 *         description: Credencial de tenant obtenida exitosamente
 *         content:
 *           application/json:
 *             schema:
 *               $ref: '#/components/responses/success'
 *       403:
 *         description: Error de autenticación
 *         content:
 *           application/json:
 *             schema:
 *               $ref: '#/components/responses/UnauthorizedError'
 *       404:
 *        description: Credencial de tenant no encontrada
 *        content:
 *          application/json:
 *            schema:
 *              $ref: '#/components/responses/NotFoundError'
 *       500:
 *         description: Error en el servidor
 *         content:
 *           application/json:
 *             schema:
 *               $ref: '#/components/responses/InternalServerError'
 *     security:
 *        - bearerAuth: []
 */
router.get('/TenantCredential/:guid', authMiddleware, TenantCredential)

// Ruta para actualizar una credencial de tenant
/**
 * @swagger
 * /tenant_credentials/UpdateTenantCredential:
 *   patch:
 *     summary: Actualizar una credencial de tenant
 *     tags: [Tenant_Credentials]
 *     parameters:
 *       - in: path
 *         name: guid
 *         schema:
 *           type: uuid
 *           format: uuid
 *         required: true
 *         description: El GUID de la credencial de tenant
 *     requestBody:
 *       required: true
 *       content:
 *         application/json:
 *           schema:
 *             type: object
 *             properties:
 *               api_key:
 *                 type: string
 *                 example: 'newexampleapikey'
 *               api_secret:
 *                 type: string
 *                 example: 'newexamplesecret'
 *     responses:
 *       201:
 *         description: Credencial de tenant actualizada exitosamente
 *         content:
 *           application/json:
 *             schema:
 *               $ref: '#/components/responses/success'
 *       400:
 *         description: Error en los datos enviados
 *         content:
 *           application/json:
 *             schema:
 *               $ref: '#/components/responses/BadRequest'
 *       403:
 *         description: Error de autenticación
 *         content:
 *           application/json:
 *             schema:
 *               $ref: '#/components/responses/UnauthorizedError'
 *       404:
 *        description: Credencial de tenant no encontrada
 *        content:
 *          application/json:
 *            schema:
 *              $ref: '#/components/responses/NotFoundError'
 *       500:
 *         description: Error en el servidor
 *         content:
 *           application/json:
 *             schema:
 *               $ref: '#/components/responses/InternalServerError'
 *     security:
 *        - bearerAuth: []
 */
router.patch(
    '/UpdateTenantCredential/:guid',
    authMiddleware,
    ValidatorUpdateTenantCredential,
    UpdateTenantCredential
)

// Ruta para eliminar una credencial de tenant
/**
 * @swagger
 * /tenant_credentials/DeleteTenantCredential:
 *   delete:
 *     summary: Eliminar una credencial de tenant
 *     tags: [Tenant_Credentials]
 *     parameters:
 *       - in: path
 *         name: guid
 *         schema:
 *           type: uuid
 *           format: uuid
 *         required: true
 *         description: El GUID de la credencial de tenant
 *     responses:
 *       201:
 *         description: Credencial de tenant eliminada exitosamente
 *         content:
 *           application/json:
 *             schema:
 *               $ref: '#/components/responses/success'
 *       400:
 *         description: Error en los datos enviados
 *         content:
 *           application/json:
 *             schema:
 *               $ref: '#/components/responses/BadRequest'
 *       403:
 *         description: Error de autenticación
 *         content:
 *           application/json:
 *             schema:
 *               $ref: '#/components/responses/UnauthorizedError'
 *       404:
 *        description: Credencial de tenant no encontrada
 *        content:
 *          application/json:
 *            schema:
 *              $ref: '#/components/responses/NotFoundError'
 *       500:
 *         description: Error en el servidor
 *         content:
 *           application/json:
 *             schema:
 *               $ref: '#/components/responses/InternalServerError'
 *     security:
 *        - bearerAuth: []
 */
router.delete(
    '/DeleteTenantCredential/:guid',
    authMiddleware,
    ValidatorDeleteTenantCredential,
    DeleteTenantCredential
)

// Ruta para desactivar una credencial de tenant
/**
 * @swagger
 * /tenant_credentials/DisableTenantCredential:
 *   patch:
 *     summary: Desactivar una credencial de tenant
 *     tags: [Tenant_Credentials]
 *     parameters:
 *       - in: path
 *         name: guid
 *         schema:
 *           type: uuid
 *           format: uuid
 *         required: true
 *         description: El GUID de la credencial de tenant
 *     responses:
 *       201:
 *         description: Credencial de tenant desactivada exitosamente
 *         content:
 *           application/json:
 *             schema:
 *               $ref: '#/components/responses/success'
 *       400:
 *         description: Error en los datos enviados
 *         content:
 *           application/json:
 *             schema:
 *               $ref: '#/components/responses/BadRequest'
 *       403:
 *         description: Error de autenticación
 *         content:
 *           application/json:
 *             schema:
 *               $ref: '#/components/responses/UnauthorizedError'
 *       404:
 *        description: Credencial de tenant no encontrada
 *        content:
 *          application/json:
 *            schema:
 *              $ref: '#/components/responses/NotFoundError'
 *       500:
 *         description: Error en el servidor
 *         content:
 *           application/json:
 *             schema:
 *               $ref: '#/components/responses/InternalServerError'
 *     security:
 *        - bearerAuth: []
 */
router.patch(
    '/DisableTenantCredential/:guid',
    authMiddleware,
    ValidatorDisableTenantCredential,
    DisableTenantCredential
)

// Ruta para activar una credencial de tenant
/**
 * @swagger
 * /tenant_credentials/EnableTenantCredential:
 *   patch:
 *     summary: Activar una credencial de tenant
 *     tags: [Tenant_Credentials]
 *     parameters:
 *       - in: path
 *         name: guid
 *         schema:
 *           type: uuid
 *           format: uuid
 *         required: true
 *         description: El GUID de la credencial de tenant
 *     responses:
 *       201:
 *         description: Credencial de tenant activada exitosamente
 *         content:
 *           application/json:
 *             schema:
 *               $ref: '#/components/responses/success'
 *       400:
 *         description: Error en los datos enviados
 *         content:
 *           application/json:
 *             schema:
 *               $ref: '#/components/responses/BadRequest'
 *       403:
 *         description: Error de autenticación
 *         content:
 *           application/json:
 *             schema:
 *               $ref: '#/components/responses/UnauthorizedError'
 *       404:
 *        description: Credencial de tenant no encontrada
 *        content:
 *          application/json:
 *            schema:
 *              $ref: '#/components/responses/NotFoundError'
 *       500:
 *         description: Error en el servidor
 *         content:
 *           application/json:
 *             schema:
 *               $ref: '#/components/responses/InternalServerError'
 *     security:
 *        - bearerAuth: []
 */
router.patch(
    '/EnableTenantCredential/:guid',
    authMiddleware,
    ValidatorEnableTenantCredential,
    EnableTenantCredential
)

// Ruta para obtener todas las credenciales de tenants
/**
 * @swagger
 * /tenant_credentials/TenantCredentialsAll:
 *   get:
 *     summary: Obtener todas las credenciales de tenants
 *     tags: [Tenant_Credentials]
 *     responses:
 *       201:
 *         description: Credenciales obtenidas exitosamente
 *         content:
 *           application/json:
 *             schema:
 *               $ref: '#/components/responses/success'
 *       400:
 *         description: Error en los datos enviados
 *         content:
 *           application/json:
 *             schema:
 *               $ref: '#/components/responses/BadRequest'
 *       403:
 *         description: Error de autenticación
 *         content:
 *           application/json:
 *             schema:
 *               $ref: '#/components/responses/UnauthorizedError'
 *       500:
 *         description: Error en el servidor
 *         content:
 *           application/json:
 *             schema:
 *               $ref: '#/components/responses/InternalServerError'
 *     security:
 *        - bearerAuth: []
 */
router.get('/TenantCredentialsAll', authMiddleware, TenantCredentialsAll)

// Ruta para contar todas las credenciales de tenants
/**
 * @swagger
 * /tenant_credentials/TenantCredentialsCount:
 *   get:
 *     summary: Contar todas las credenciales de tenants
 *     tags: [Tenant_Credentials]
 *     responses:
 *       201:
 *         description: Credenciales contadas exitosamente
 *         content:
 *           application/json:
 *             schema:
 *               $ref: '#/components/responses/success'
 *       400:
 *         description: Error en los datos enviados
 *         content:
 *           application/json:
 *             schema:
 *               $ref: '#/components/responses/BadRequest'
 *       403:
 *         description: Error de autenticación
 *         content:
 *           application/json:
 *             schema:
 *               $ref: '#/components/responses/UnauthorizedError'
 *       500:
 *         description: Error en el servidor
 *         content:
 *           application/json:
 *             schema:
 *               $ref: '#/components/responses/InternalServerError'
 *     security:
 *        - bearerAuth: []
 */
router.get('/TenantCredentialsCount', authMiddleware, CountTenantCredentials)

// Ruta para ejecutar los seeders de un tenant
/**
 * @swagger
 * /tenant_credentials/RunSeedersTenant:
 *   post:
 *     summary: Ejecutar los seeders de un tenant
 *     tags: [Tenant_Credentials]
 *     requestBody:
 *       required: true
 *       content:
 *         application/json:
 *           schema:
 *             type: object
 *             properties:
 *               guid:
 *                type: guid
 *                example: 12345678-1234-1234-1234-123456789012
 *     responses:
 *       201:
 *         description: Seeders ejecutados exitosamente
 *         content:
 *           application/json:
 *             schema:
 *               $ref: '#/components/responses/success'
 *       400:
 *         description: Error en los datos enviados
 *         content:
 *           application/json:
 *             schema:
 *               $ref: '#/components/responses/BadRequest'
 *       403:
 *         description: Error de autenticación
 *         content:
 *           application/json:
 *             schema:
 *               $ref: '#/components/responses/UnauthorizedError'
 *       404:
 *        description: Tenant no encontrado
 *        content:
 *          application/json:
 *            schema:
 *              $ref: '#/components/responses/NotFoundError'
 *       500:
 *         description: Error en el servidor
 *         content:
 *           application/json:
 *             schema:
 *               $ref: '#/components/responses/InternalServerError'
 *     security:
 *        - bearerAuth: []
 */
router.post(
    '/RunSeedersTenant',
    authMiddleware,
    ValidatorRunSeedersTenant,
    RunSeedersTenant
)

module.exports = router
