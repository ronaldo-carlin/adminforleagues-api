const { check, validationResult } = require('express-validator')
const {
    getSequelize,
    getTeamsModel,
    getCompetitionsModel,
} = require('../../helpers/sequelizeHelper')

// Validación para crear una competencia de equipo
const validatorCreateTeamsCompetition = () => {
    return [
        async (req, res, next) => {
            const sequelize = getSequelize(req)
            const TeamsModel = getTeamsModel(sequelize)
            const CompetitionsModel = getCompetitionsModel(sequelize)

            await check('team_guid')
                .isUUID()
                .withMessage('El GUID del equipo debe ser un UUID válido.')
                .custom(async (value) => {
                    const team = await TeamsModel.findOne({
                        where: { guid: value },
                    })
                    if (!team) {
                        return Promise.reject('El equipo no existe.')
                    }
                })
                .run(req)

            await check('competition_guid')
                .isUUID()
                .withMessage(
                    'El GUID de la competencia debe ser un UUID válido.'
                )
                .custom(async (value) => {
                    const competition = await CompetitionsModel.findOne({
                        where: { guid: value },
                    })
                    if (!competition) {
                        return Promise.reject('La competencia no existe.')
                    }
                })
                .run(req)

            await check('group')
                .optional()
                .isString()
                .withMessage('El grupo debe ser una cadena de texto.')
                .run(req)

            await check('points')
                .optional()
                .isInt({ min: 0 })
                .withMessage('Los puntos deben ser un número entero positivo.')
                .run(req)

            await check('played')
                .optional()
                .isInt({ min: 0 })
                .withMessage(
                    'Los partidos jugados deben ser un número entero positivo.'
                )
                .run(req)

            await check('won')
                .optional()
                .isInt({ min: 0 })
                .withMessage(
                    'Los partidos ganados deben ser un número entero positivo.'
                )
                .run(req)

            await check('drawn')
                .optional()
                .isInt({ min: 0 })
                .withMessage(
                    'Los partidos empatados deben ser un número entero positivo.'
                )
                .run(req)

            await check('lost')
                .optional()
                .isInt({ min: 0 })
                .withMessage(
                    'Los partidos perdidos deben ser un número entero positivo.'
                )
                .run(req)

            await check('goals_for')
                .optional()
                .isInt({ min: 0 })
                .withMessage(
                    'Los goles a favor deben ser un número entero positivo.'
                )
                .run(req)

            await check('goals_against')
                .optional()
                .isInt({ min: 0 })
                .withMessage(
                    'Los goles en contra deben ser un número entero positivo.'
                )
                .run(req)

            await check('goal_difference')
                .optional()
                .isInt()
                .withMessage(
                    'La diferencia de goles debe ser un número entero.'
                )
                .run(req)

            const errors = validationResult(req)
            if (!errors.isEmpty()) {
                return res.status(400).json({ errors: errors.array() })
            }

            next()
        },
    ]
}

// Validación para obtener una competencia de equipo por GUID
const validatorTeamsCompetition = () => {
    return [
        async (req, res, next) => {
            await check('guid')
                .isUUID()
                .withMessage(
                    'El GUID de la competencia de equipo es requerido y debe ser un UUID válido.'
                )
                .run(req)

            const errors = validationResult(req)
            if (!errors.isEmpty()) {
                return res.status(400).json({ errors: errors.array() })
            }

            next()
        },
    ]
}

// Validación para actualizar una competencia de equipo
const validatorUpdateTeamsCompetition = () => {
    return [
        async (req, res, next) => {
            const sequelize = getSequelize(req)
            const TeamsModel = getTeamsModel(sequelize)
            const CompetitionsModel = getCompetitionsModel(sequelize)

            await check('guid')
                .isUUID()
                .withMessage(
                    'El GUID de la competencia de equipo es requerido y debe ser un UUID válido.'
                )
                .run(req)

            await check('team_guid')
                .optional()
                .isUUID()
                .withMessage('El GUID del equipo debe ser un UUID válido.')
                .custom(async (value) => {
                    if (value) {
                        const team = await TeamsModel.findOne({
                            where: { guid: value },
                        })
                        if (!team) {
                            return Promise.reject('El equipo no existe.')
                        }
                    }
                })
                .run(req)

            await check('competition_guid')
                .optional()
                .isUUID()
                .withMessage(
                    'El GUID de la competencia debe ser un UUID válido.'
                )
                .custom(async (value) => {
                    if (value) {
                        const competition = await CompetitionsModel.findOne({
                            where: { guid: value },
                        })
                        if (!competition) {
                            return Promise.reject('La competencia no existe.')
                        }
                    }
                })
                .run(req)

            await check('group')
                .optional()
                .isString()
                .withMessage('El grupo debe ser una cadena de texto.')
                .run(req)

            await check('points')
                .optional()
                .isInt({ min: 0 })
                .withMessage('Los puntos deben ser un número entero positivo.')
                .run(req)

            await check('played')
                .optional()
                .isInt({ min: 0 })
                .withMessage(
                    'Los partidos jugados deben ser un número entero positivo.'
                )
                .run(req)

            await check('won')
                .optional()
                .isInt({ min: 0 })
                .withMessage(
                    'Los partidos ganados deben ser un número entero positivo.'
                )
                .run(req)

            await check('drawn')
                .optional()
                .isInt({ min: 0 })
                .withMessage(
                    'Los partidos empatados deben ser un número entero positivo.'
                )
                .run(req)

            await check('lost')
                .optional()
                .isInt({ min: 0 })
                .withMessage(
                    'Los partidos perdidos deben ser un número entero positivo.'
                )
                .run(req)

            await check('goals_for')
                .optional()
                .isInt({ min: 0 })
                .withMessage(
                    'Los goles a favor deben ser un número entero positivo.'
                )
                .run(req)

            await check('goals_against')
                .optional()
                .isInt({ min: 0 })
                .withMessage(
                    'Los goles en contra deben ser un número entero positivo.'
                )
                .run(req)

            await check('goal_difference')
                .optional()
                .isInt()
                .withMessage(
                    'La diferencia de goles debe ser un número entero.'
                )
                .run(req)

            const errors = validationResult(req)
            if (!errors.isEmpty()) {
                return res.status(400).json({ errors: errors.array() })
            }

            next()
        },
    ]
}

// Validación para eliminar una competencia de equipo
const validatorDeleteTeamsCompetition = () => {
    return [
        async (req, res, next) => {
            await check('guid')
                .isUUID()
                .withMessage(
                    'El GUID de la competencia de equipo es requerido y debe ser un UUID válido.'
                )
                .run(req)

            const errors = validationResult(req)
            if (!errors.isEmpty()) {
                return res.status(400).json({ errors: errors.array() })
            }

            next()
        },
    ]
}

// Validación para deshabilitar una competencia de equipo
const validatorDisableTeamsCompetition = () => {
    return [
        async (req, res, next) => {
            await check('guid')
                .isUUID()
                .withMessage(
                    'El GUID de la competencia de equipo es requerido y debe ser un UUID válido.'
                )
                .run(req)

            const errors = validationResult(req)
            if (!errors.isEmpty()) {
                return res.status(400).json({ errors: errors.array() })
            }

            next()
        },
    ]
}

// Validación para habilitar una competencia de equipo
const validatorEnableTeamsCompetition = () => {
    return [
        async (req, res, next) => {
            await check('guid')
                .isUUID()
                .withMessage(
                    'El GUID de la competencia de equipo es requerido y debe ser un UUID válido.'
                )
                .run(req)

            const errors = validationResult(req)
            if (!errors.isEmpty()) {
                return res.status(400).json({ errors: errors.array() })
            }

            next()
        },
    ]
}

module.exports = {
    validatorCreateTeamsCompetition,
    validatorTeamsCompetition,
    validatorUpdateTeamsCompetition,
    validatorDeleteTeamsCompetition,
    validatorDisableTeamsCompetition,
    validatorEnableTeamsCompetition,
}
