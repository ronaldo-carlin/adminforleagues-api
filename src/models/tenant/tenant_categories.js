// Importar Sequelize
const { DataTypes } = require('sequelize')
const SportTenantModelFactory = require('./tenant_sports')

// Definir la función factory para el modelo CategoriesTenantModel
const CategoriesTenantModelFactory = (sequelize) => {
    // Importar el modelo de licencias
    const SportTenantModel = SportTenantModelFactory(sequelize)

    const CategoriesTenantModel = sequelize.define(
        'categories',
        {
            id: {
                type: DataTypes.INTEGER,
                primaryKey: true,
                autoIncrement: true,
            },
            guid: {
                type: DataTypes.UUID,
                primaryKey: true,
                defaultValue: DataTypes.UUIDV4,
                unique: true,
            },
            sport_guid: {
                type: DataTypes.UUID,
            },
            name: {
                type: DataTypes.TEXT,
            },
            description: {
                type: DataTypes.TEXT,
            },
            logo_path: {
                type: DataTypes.TEXT,
            },
            color: {
                type: DataTypes.TEXT,
            },
            age_min: {
                type: DataTypes.INTEGER,
            },
            age_max: {
                type: DataTypes.INTEGER,
            },
            gender: {
                type: DataTypes.TEXT,
            },
            level: {
                type: DataTypes.TEXT,
            },
            is_active: {
                type: DataTypes.INTEGER,
                defaultValue: 1,
            },
            created_by: {
                type: DataTypes.UUID,
            },
            updated_by: {
                type: DataTypes.UUID,
            },
            deleted_by: {
                type: DataTypes.UUID,
            },
            created_at: {
                type: DataTypes.DATE,
            },
            updated_at: {
                type: DataTypes.DATE,
            },
            deleted_at: {
                type: DataTypes.DATE,
            },
        },
        {
            tableName: 'categories',
            timestamps: false,
            defaultScope: {
                attributes: {
                    exclude: [
                        'created_by',
                        'updated_by',
                        'deleted_by',
                        'created_at',
                        'updated_at',
                        'deleted_at',
                    ],
                },
                include: [
                    {
                        model: SportTenantModel,
                        as: 'sport',
                        attributes: ['guid', 'name'],
                        where: { is_active: 1 },
                        required: true,
                        exclude: [
                            'created_at',
                            'created_by',
                            'updated_at',
                            'updated_by',
                            'deleted_at',
                            'deleted_by',
                        ],
                    },
                ],
            },
        }
    )

    CategoriesTenantModel.belongsTo(SportTenantModel, {
        foreignKey: 'sport_guid', // El campo de la tabla access_control que se usará para la relación
        targetKey: 'guid', // El campo de la tabla sport que se usará para la relación
        as: 'sport', // El alias que se utilizará para la relación
    })

    return CategoriesTenantModel
}

// Asociación con el modelo de roles

module.exports = CategoriesTenantModelFactory
