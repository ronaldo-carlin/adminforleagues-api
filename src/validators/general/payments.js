const { check } = require('express-validator')
const validateResults = require('../../utils/handleValidators')

// Validaciones para crear un Payment
const ValidatorCreatePayment = [
    check('user_guid')
        .isUUID()
        .withMessage('El GUID del usuario es requerido.'),
    check('plan_guid').isUUID().withMessage('El GUID del plan es requerido.'),
    check('email')
        .isEmail()
        .withMessage('El email es requerido y debe tener un formato válido.'),
    check('amount')
        .isNumeric()
        .withMessage('El monto es requerido y debe ser numérico.'),
    check('method').isString().withMessage('El método de pago es requerido.'),
    check('transaction')
        .isString()
        .withMessage('El número de transacción es requerido.'),
    check('reference').isString().withMessage('La referencia es requerida.'),
    check('concept').isString().withMessage('El concepto es requerido.'),
    check('card')
        .optional()
        .isString()
        .withMessage('El tipo de tarjeta debe ser una cadena de caracteres.'),
    check('bank')
        .optional()
        .isString()
        .withMessage('El banco debe ser una cadena de caracteres.'),
    check('message')
        .optional()
        .isString()
        .withMessage('El mensaje debe ser una cadena de caracteres.'),
    check('status')
        .isString()
        .withMessage('El estado debe ser un número entero.'),
    validateResults,
]

// Validaciones para actualizar un Payment
const ValidatorUpdatePayment = [
    check('guid')
        .isUUID()
        .withMessage('El GUID es requerido para actualizar el pago.'),
    check('user_guid')
        .optional()
        .isString()
        .withMessage('El GUID del usuario debe ser un UUID válido.'),
    check('plan_guid')
        .optional()
        .isUUID()
        .withMessage('El GUID del plan debe ser un UUID válido.'),
    check('email')
        .optional()
        .isEmail()
        .withMessage('El email debe tener un formato válido.'),
    check('amount')
        .optional()
        .isNumeric()
        .withMessage('El monto debe ser numérico.'),
    check('method')
        .optional()
        .isString()
        .withMessage('El método de pago debe ser una cadena de caracteres.'),
    check('transaction')
        .optional()
        .isString()
        .withMessage(
            'El número de transacción debe ser una cadena de caracteres.'
        ),
    check('reference')
        .optional()
        .isString()
        .withMessage('La referencia debe ser una cadena de caracteres.'),
    check('concept')
        .optional()
        .isString()
        .withMessage('El concepto debe ser una cadena de caracteres.'),
    check('card')
        .optional()
        .isString()
        .withMessage('El tipo de tarjeta debe ser una cadena de caracteres.'),
    check('bank')
        .optional()
        .isString()
        .withMessage('El banco debe ser una cadena de caracteres.'),
    check('message')
        .optional()
        .isString()
        .withMessage('El mensaje debe ser una cadena de caracteres.'),
    check('status')
        .optional()
        .isString()
        .withMessage('El estado debe ser un número entero.'),
    validateResults,
]

// Validaciones para eliminar un Payment
const ValidatorDeletePayment = [
    check('guid')
        .isUUID()
        .withMessage('El GUID es requerido para eliminar el pago.'),
    validateResults,
]

// Validaciones para deshabilitar un Payment
const ValidatorDisablePayment = [
    check('guid')
        .isUUID()
        .withMessage('El GUID es requerido para deshabilitar el pago.'),
    validateResults,
]

// Validaciones para habilitar un Payment
const ValidatorEnablePayment = [
    check('guid')
        .isUUID()
        .withMessage('El GUID es requerido para habilitar el pago.'),
    validateResults,
]

module.exports = {
    ValidatorCreatePayment,
    ValidatorUpdatePayment,
    ValidatorDeletePayment,
    ValidatorDisablePayment,
    ValidatorEnablePayment,
}
