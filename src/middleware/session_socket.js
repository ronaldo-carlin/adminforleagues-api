const jwt = require('jsonwebtoken')
const config = require('../config/config') // Asegúrate de importar la configuración de tu aplicación
const { verifyAccessToken } = require('../utils/handleJwt')

function validarJWT(client, next) {
    const token = client.handshake.headers['authorization']

    if (!token) {
        console.log('Cliente no proporcionó un token JWT')
        return next(new Error('Cliente no autorizado'))
    }

    // Verifica el token JWT utilizando tu módulo de utilidades
    const decodedToken = verifyAccessToken(token)

    if (!decodedToken) {
        console.log('Token JWT no válido')
        return next(new Error('Token JWT no válido'))
    }

    // Si el token es válido, puedes acceder a los datos decodificados en decodedToken.
    // Por ejemplo, puedes obtener el ID del usuario.
    const userId = decodedToken.id

    // Guarda el userId en el objeto del cliente para su posterior uso
    client.userId = userId

    // Continúa con la conexión del cliente
    next()
}

module.exports = validarJWT
