module.exports = {
    async up(queryInterface, Sequelize) {
        // entidad para guardan los permisos que se pueden asignar a los roles
        await queryInterface.createTable('permissions', {
            // identificador del registro
            id: {
                type: Sequelize.INTEGER,
                primaryKey: true,
                autoIncrement: true,
                allowNull: false,
            },
            guid: {
                type: Sequelize.UUID,
                defaultValue: Sequelize.UUIDV4,
                unique: true,
                allowNull: false,
            },
            // nombre del permiso
            name: {
                type: Sequelize.TEXT,
            },
            // descripcion del permiso
            description: {
                type: Sequelize.TEXT,
            },
            // estado del registro
            is_active: {
                type: Sequelize.BOOLEAN,
                defaultValue: 1, // valor por defecto
            },
            // usuario que creo el registro
            created_by: {
                type: Sequelize.UUID,
            },
            // fecha de creacion
            created_at: {
                type: Sequelize.DATE,
                defaultValue: Sequelize.literal('CURRENT_TIMESTAMP'),
            },
            // usuario que actualizo el registro
            updated_by: {
                type: Sequelize.UUID,
            },
            // fecha de actualizacion
            updated_at: {
                type: Sequelize.DATE,
            },
            // usuario que elimino el registro
            deleted_by: {
                type: Sequelize.UUID,
            },
            // fecha de eliminacion
            deleted_at: {
                type: Sequelize.DATE,
            },
        })

        // Agregar índice único en 'guid'
        await queryInterface.addIndex('permissions', ['guid'], {
            unique: true,
            type: 'unique',
            name: 'unique_permissions_guid',
        })
    },

    async down(queryInterface, Sequelize) {
        await queryInterface.removeIndex(
            'permissions',
            'unique_permissions_guid'
        )
        await queryInterface.dropTable('permissions')
    },
}
