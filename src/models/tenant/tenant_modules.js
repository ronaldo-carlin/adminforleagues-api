// Importar Sequelize
const { DataTypes } = require('sequelize')

// Definir la función factory para el modelo ModulesTenantModel
const ModulesTenantModelFactory = (sequelize) => {
    const ModulesTenantModel = sequelize.define(
        'modules',
        {
            id: {
                type: DataTypes.INTEGER,
                primaryKey: true,
                autoIncrement: true,
            },
            guid: {
                type: DataTypes.UUID,
                primaryKey: true,
                defaultValue: DataTypes.UUIDV4,
                unique: true,
            },
            name: {
                type: DataTypes.TEXT,
            },
            to: {
                type: DataTypes.TEXT,
            },
            icon: {
                type: DataTypes.TEXT,
            },
            is_active: {
                type: DataTypes.INTEGER,
                defaultValue: 1,
            },
            created_by: {
                type: DataTypes.UUID,
            },
            updated_by: {
                type: DataTypes.UUID,
            },
            deleted_by: {
                type: DataTypes.UUID,
            },
            created_at: {
                type: DataTypes.DATE,
            },
            updated_at: {
                type: DataTypes.DATE,
            },
            deleted_at: {
                type: DataTypes.DATE,
            },
        },
        {
            tableName: 'modules',
            timestamps: false,
        }
    )

    return ModulesTenantModel
}

module.exports = ModulesTenantModelFactory
