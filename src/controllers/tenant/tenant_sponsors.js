const { Op } = require('sequelize')
const { v4: uuidv4 } = require('uuid')
const { matchedData } = require('express-validator')
const { handleHttpSuccess } = require('../../utils/handleSuccess')
const { handleHttpError } = require('../../utils/handleError')
const { handleHttpCount } = require('../../utils/handleCount')
const {
    getSequelize,
    getSponsorsModel,
} = require('../../helpers/sequelizeHelper')

/**
 * Este controlador es el encargado de registrar un sponsor
 * @param {*} req
 * @param {*} res
 */
const CreateSponsor = async (req, res) => {
    try {
        const sequelize = getSequelize(req)
        const SponsorsTenantModel = getSponsorsModel(sequelize)
        const data = matchedData(req)
        const created_by = req.user.guid
        const created_at = new Date()
        const guid = await uuidv4()
        data.created_by = created_by
        data.created_at = created_at
        data.guid = guid

        const sponsor = await SponsorsTenantModel.create(data)
        return handleHttpSuccess(res, 'Sponsor creado.', 201, sponsor)
    } catch (error) {
        console.log('Error al crear el sponsor:', error)
        handleHttpError(res, 'Error al crear el sponsor.', 403, error)
    }
}

/**
 * Este controlador es el encargado de obtener los sponsors activos
 * @param {*} req
 * @param {*} res
 */
const Sponsors = async (req, res) => {
    try {
        const sequelize = getSequelize(req)
        const SponsorsTenantModel = getSponsorsModel(sequelize)

        const sponsors = await SponsorsTenantModel.findAll({
            where: { is_active: 1 },
        })

        if (!sponsors.length) {
            handleHttpError(res, 'No se encontraron sponsors.', 404)
            return
        }

        handleHttpSuccess(res, 'Sponsors encontrados.', 201, sponsors)
    } catch (error) {
        console.log('Error obteniendo sponsors:', error)
        handleHttpError(res, 'Error obteniendo sponsors.', 403, error)
    }
}

/**
 * Este controlador es el encargado de obtener un sponsor por GUID
 * @param {*} req
 * @param {*} res
 */
const Sponsor = async (req, res) => {
    try {
        const sequelize = getSequelize(req)
        const SponsorsTenantModel = getSponsorsModel(sequelize)
        const { guid } = req.params

        const sponsor = await SponsorsTenantModel.findOne({
            where: { guid },
        })

        if (!sponsor) {
            handleHttpError(res, 'El sponsor no existe.', 404)
            return
        }

        if (sponsor.is_active == 0) {
            handleHttpError(res, 'El sponsor ha sido deshabilitado.', 401)
            return
        }

        handleHttpSuccess(res, 'Sponsor encontrado.', 201, sponsor)
    } catch (error) {
        console.log('Error obteniendo sponsor:', error)
        handleHttpError(res, 'Error obteniendo sponsor.', 403, error)
    }
}

/**
 * Este controlador es el encargado de actualizar un sponsor
 * @param {*} req
 * @param {*} res
 */
const UpdateSponsor = async (req, res) => {
    try {
        const sequelize = getSequelize(req)
        const SponsorsTenantModel = getSponsorsModel(sequelize)
        const { guid } = req.params
        const data = matchedData(req)
        const updated_by = req.user.guid
        const updated_at = new Date()
        data.updated_by = updated_by
        data.updated_at = updated_at

        const [updateCount] = await SponsorsTenantModel.update(data, {
            where: { guid },
        })

        if (updateCount === 0) {
            return handleHttpError(res, 'El sponsor no existe.', 404)
        }

        const sponsor = await SponsorsTenantModel.findOne({
            where: { guid },
        })

        if (!sponsor) {
            handleHttpError(res, 'El sponsor no existe.', 404)
            return
        }

        if (sponsor.is_active == 0) {
            handleHttpError(res, 'El sponsor ha sido deshabilitado.', 401)
            return
        }

        handleHttpSuccess(res, 'Sponsor actualizado.', 201, sponsor)
    } catch (error) {
        console.log('Error actualizando sponsor:', error)
        handleHttpError(res, 'Error actualizando sponsor.', 403, error)
    }
}

/**
 * Este controlador es el encargado de eliminar un sponsor
 * @param {*} req
 * @param {*} res
 */
const DeleteSponsor = async (req, res) => {
    try {
        const sequelize = getSequelize(req)
        const SponsorsTenantModel = getSponsorsModel(sequelize)
        const { guid } = req.params
        const deleted_by = req.user.guid
        const deleted_at = new Date()
        const [updateCount] = await SponsorsTenantModel.update(
            {
                is_active: 0,
                deleted_by,
                deleted_at,
            },
            {
                where: { guid },
            }
        )

        if (updateCount === 0) {
            return handleHttpError(res, 'El sponsor no existe.', 404)
        }

        handleHttpSuccess(res, 'Sponsor eliminado.', 201)
    } catch (error) {
        console.log('Error eliminando sponsor:', error)
        handleHttpError(res, 'Error eliminando sponsor.', 403, error)
    }
}

/**
 * Este controlador es el encargado de desactivar un sponsor
 * @param {*} req
 * @param {*} res
 */
const DisableSponsor = async (req, res) => {
    try {
        const sequelize = getSequelize(req)
        const SponsorsTenantModel = getSponsorsModel(sequelize)
        const { guid } = req.params
        const updated_by = req.user.guid
        const updated_at = new Date()
        const [updateCount] = await SponsorsTenantModel.update(
            {
                is_active: 0,
                updated_by,
                updated_at,
            },
            {
                where: { guid },
            }
        )

        if (updateCount === 0) {
            return handleHttpError(res, 'El sponsor no existe.', 404)
        }

        handleHttpSuccess(res, 'Sponsor deshabilitado.', 201)
    } catch (error) {
        console.log('Error deshabilitando sponsor:', error)
        handleHttpError(res, 'Error deshabilitando sponsor.', 403, error)
    }
}

/**
 * Este controlador es el encargado de activar un sponsor
 * @param {*} req
 * @param {*} res
 */
const EnableSponsor = async (req, res) => {
    try {
        const sequelize = getSequelize(req)
        const SponsorsTenantModel = getSponsorsModel(sequelize)
        const { guid } = req.params
        const updated_by = req.user.guid
        const updated_at = new Date()
        const [updateCount] = await SponsorsTenantModel.update(
            {
                is_active: 1,
                updated_by,
                updated_at,
            },
            {
                where: { guid },
            }
        )

        if (updateCount === 0) {
            return handleHttpError(res, 'El sponsor no existe.', 404)
        }

        handleHttpSuccess(res, 'Sponsor habilitado.', 201)
    } catch (error) {
        console.log('Error habilitando sponsor:', error)
        handleHttpError(res, 'Error habilitando sponsor.', 403, error)
    }
}

/**
 * Este controlador es el encargado de obtener todos los sponsors
 * @param {*} req
 * @param {*} res
 */
const SponsorsAll = async (req, res) => {
    try {
        const sequelize = getSequelize(req)
        const SponsorsTenantModel = getSponsorsModel(sequelize)

        const sponsors = await SponsorsTenantModel.findAll()

        if (!sponsors.length) {
            handleHttpError(res, 'No se encontraron sponsors.', 404)
            return
        }

        handleHttpSuccess(res, 'Sponsors encontrados.', 201, sponsors)
    } catch (error) {
        console.log('Error obteniendo sponsors:', error)
        handleHttpError(res, 'Error obteniendo sponsors.', 403, error)
    }
}

/**
 * Este controlador es el encargado de contar todos los sponsors
 * @param {*} req
 * @param {*} res
 */
const SponsorsCount = async (req, res) => {
    try {
        const sequelize = getSequelize(req)
        const SponsorsTenantModel = getSponsorsModel(sequelize)

        const count = await SponsorsTenantModel.count()

        handleHttpCount(res, 'Sponsors encontrados.', 201, count)
    } catch (error) {
        console.log('Error contando sponsors:', error)
        handleHttpError(res, 'Error contando sponsors.', 403, error)
    }
}

module.exports = {
    CreateSponsor,
    Sponsors,
    Sponsor,
    UpdateSponsor,
    DeleteSponsor,
    DisableSponsor,
    EnableSponsor,
    SponsorsAll,
    SponsorsCount,
}
