// Importar Sequelize
const { DataTypes } = require('sequelize')

// Definir la función factory para el modelo TeamsCompetitionsTenantModel
const TeamsCompetitionsTenantModelFactory = (sequelize) => {
    const TeamsCompetitionsTenantModel = sequelize.define(
        'teams_competitions',
        {
            id: {
                type: DataTypes.INTEGER,
                primaryKey: true,
                autoIncrement: true,
            },
            guid: {
                type: DataTypes.UUID,
                primaryKey: true,
                defaultValue: DataTypes.UUIDV4,
                unique: true,
            },
            team_guid: {
                type: DataTypes.TEXT,
            },
            competition_guid: {
                type: DataTypes.TEXT,
            },
            group: {
                type: DataTypes.TEXT,
            },
            points: {
                type: DataTypes.INTEGER,
            },
            played: {
                type: DataTypes.INTEGER,
            },
            won: {
                type: DataTypes.INTEGER,
            },
            drawn: {
                type: DataTypes.INTEGER,
            },
            lost: {
                type: DataTypes.INTEGER,
            },
            goals_for: {
                type: DataTypes.INTEGER,
            },
            goals_against: {
                type: DataTypes.INTEGER,
            },
            goal_difference: {
                type: DataTypes.INTEGER,
            },
            is_active: {
                type: DataTypes.INTEGER,
            },
            created_by: {
                type: DataTypes.UUID,
            },
            updated_by: {
                type: DataTypes.UUID,
            },
            deleted_by: {
                type: DataTypes.UUID,
            },
            created_at: {
                type: DataTypes.DATE,
            },
            updated_at: {
                type: DataTypes.DATE,
            },
            deleted_at: {
                type: DataTypes.DATE,
            },
        },
        {
            tableName: 'teams_competitions',
            timestamps: false,
            defaultScope: {
                attributes: {
                    exclude: [
                        'created_by',
                        'updated_by',
                        'deleted_by',
                        'created_at',
                        'updated_at',
                        'deleted_at',
                    ],
                },
            },
        }
    )

    return TeamsCompetitionsTenantModel
}

module.exports = TeamsCompetitionsTenantModelFactory
