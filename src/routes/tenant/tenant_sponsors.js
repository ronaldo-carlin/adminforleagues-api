const express = require('express')
const router = express.Router()

// Controladores y validadores para sponsors
const {
    validatorCreateSponsor,
    validatorSponsor,
    validatorUpdateSponsor,
    validatorDeleteSponsor,
    validatorDisableSponsor,
    validatorEnableSponsor,
} = require('../../validators/tenant/tenant_sponsors')

const {
    CreateSponsor,
    Sponsors,
    Sponsor,
    UpdateSponsor,
    DeleteSponsor,
    DisableSponsor,
    EnableSponsor,
    SponsorsAll,
    SponsorsCount,
} = require('../../controllers/tenant/tenant_sponsors')

const authMiddleware = require('../../middleware/session_tenant')

/* Rutas protegidas que requieren autenticación */
/**
 * @swagger
 * tags:
 *   name: Tenant_Sponsors
 *   description: API para la gestión de patrocinadores
 */

// Ruta para crear un nuevo patrocinador
/**
 * @swagger
 * /tenant_sponsors/CreateSponsor:
 *   post:
 *     summary: Crear un nuevo patrocinador
 *     description: Esta ruta permite crear un nuevo patrocinador con los detalles especificados.
 *     tags: [Tenant_Sponsors]
 *     requestBody:
 *       required: true
 *       content:
 *         application/json:
 *           schema:
 *             type: object
 *             properties:
 *               name:
 *                 type: string
 *                 example: "Patrocinador Ejemplo"
 *               logo:
 *                 type: string
 *                 example: "https://example.com/logo.png"
 *     responses:
 *       201:
 *         description: Patrocinador creado exitosamente
 *         content:
 *           application/json:
 *             schema:
 *               $ref: '#/components/responses/success'
 *       400:
 *         description: Error en los datos enviados
 *         content:
 *           application/json:
 *             schema:
 *               $ref: '#/components/responses/BadRequest'
 *       403:
 *         description: Error de autenticación
 *         content:
 *           application/json:
 *             schema:
 *               $ref: '#/components/responses/UnauthorizedError'
 *       409:
 *         description: El patrocinador ya existe
 *         content:
 *           application/json:
 *             schema:
 *               $ref: '#/components/responses/ConflictError'
 *       500:
 *         description: Error en el servidor
 *         content:
 *           application/json:
 *             schema:
 *               $ref: '#/components/responses/InternalServerError'
 *     security:
 *        - bearerAuth: []
 */
router.post(
    '/CreateSponsor',
    authMiddleware,
    validatorCreateSponsor(),
    CreateSponsor
)

// Ruta para obtener todos los patrocinadores activos
/**
 * @swagger
 * /tenant_sponsors/Sponsors:
 *   get:
 *     summary: Obtener todos los patrocinadores activos
 *     tags: [Tenant_Sponsors]
 *     responses:
 *       200:
 *         description: Patrocinadores obtenidos exitosamente
 *         content:
 *           application/json:
 *             schema:
 *               $ref: '#/components/responses/success'
 *       403:
 *         description: Error de autenticación
 *         content:
 *           application/json:
 *             schema:
 *               $ref: '#/components/responses/UnauthorizedError'
 *       500:
 *         description: Error en el servidor
 *         content:
 *           application/json:
 *             schema:
 *               $ref: '#/components/responses/InternalServerError'
 *     security:
 *        - bearerAuth: []
 */
router.get('/Sponsors', authMiddleware, Sponsors)

// Ruta para obtener un patrocinador por GUID
/**
 * @swagger
 * /tenant_sponsors/Sponsor/{guid}:
 *   get:
 *     summary: Obtener un patrocinador por GUID
 *     tags: [Tenant_Sponsors]
 *     parameters:
 *       - in: path
 *         name: guid
 *         schema:
 *           type: string
 *           format: uuid
 *         required: true
 *         description: El GUID del patrocinador
 *     responses:
 *       200:
 *         description: Patrocinador obtenido exitosamente
 *         content:
 *           application/json:
 *             schema:
 *               $ref: '#/components/responses/success'
 *       403:
 *         description: Error de autenticación
 *         content:
 *           application/json:
 *             schema:
 *               $ref: '#/components/responses/UnauthorizedError'
 *       404:
 *         description: Patrocinador no encontrado
 *         content:
 *           application/json:
 *             schema:
 *               $ref: '#/components/responses/NotFoundError'
 *       500:
 *         description: Error en el servidor
 *         content:
 *           application/json:
 *             schema:
 *               $ref: '#/components/responses/InternalServerError'
 *     security:
 *        - bearerAuth: []
 */
router.get('/Sponsor/:guid', authMiddleware, validatorSponsor(), Sponsor)

// Ruta para actualizar un patrocinador
/**
 * @swagger
 * /tenant_sponsors/UpdateSponsor/{guid}:
 *   patch:
 *     summary: Actualizar un patrocinador
 *     tags: [Tenant_Sponsors]
 *     requestBody:
 *       required: true
 *       content:
 *         application/json:
 *           schema:
 *             type: object
 *             properties:
 *               name:
 *                 type: string
 *                 example: "Patrocinador Ejemplo"
 *               logo:
 *                 type: string
 *                 example: "https://example.com/logo.png"
 *     responses:
 *       200:
 *         description: Patrocinador actualizado exitosamente
 *         content:
 *           application/json:
 *             schema:
 *               $ref: '#/components/responses/success'
 *       400:
 *         description: Error en los datos enviados
 *         content:
 *           application/json:
 *             schema:
 *               $ref: '#/components/responses/BadRequest'
 *       403:
 *         description: Error de autenticación
 *         content:
 *           application/json:
 *             schema:
 *               $ref: '#/components/responses/UnauthorizedError'
 *       404:
 *         description: Patrocinador no encontrado
 *         content:
 *           application/json:
 *             schema:
 *               $ref: '#/components/responses/NotFoundError'
 *       500:
 *         description: Error en el servidor
 *         content:
 *           application/json:
 *             schema:
 *               $ref: '#/components/responses/InternalServerError'
 *     security:
 *        - bearerAuth: []
 */
router.patch(
    '/UpdateSponsor/:guid',
    authMiddleware,
    validatorUpdateSponsor(),
    UpdateSponsor
)

// Ruta para eliminar un patrocinador
/**
 * @swagger
 * /tenant_sponsors/DeleteSponsor/{guid}:
 *   delete:
 *     summary: Eliminar un patrocinador
 *     tags: [Tenant_Sponsors]
 *     parameters:
 *       - in: path
 *         name: guid
 *         schema:
 *           type: string
 *           format: uuid
 *         required: true
 *         description: El GUID del patrocinador
 *     responses:
 *       200:
 *         description: Patrocinador eliminado exitosamente
 *         content:
 *           application/json:
 *             schema:
 *               $ref: '#/components/responses/success'
 *       400:
 *         description: Error en los datos enviados
 *         content:
 *           application/json:
 *             schema:
 *               $ref: '#/components/responses/BadRequest'
 *       403:
 *         description: Error de autenticación
 *         content:
 *           application/json:
 *             schema:
 *               $ref: '#/components/responses/UnauthorizedError'
 *       404:
 *         description: Patrocinador no encontrado
 *         content:
 *           application/json:
 *             schema:
 *               $ref: '#/components/responses/NotFoundError'
 *       500:
 *         description: Error en el servidor
 *         content:
 *           application/json:
 *             schema:
 *               $ref: '#/components/responses/InternalServerError'
 *     security:
 *        - bearerAuth: []
 */
router.delete(
    '/DeleteSponsor/:guid',
    authMiddleware,
    validatorDeleteSponsor(),
    DeleteSponsor
)

// Ruta para deshabilitar un patrocinador
/**
 * @swagger
 * /tenant_sponsors/DisableSponsor/{guid}:
 *   patch:
 *     summary: Deshabilitar un patrocinador
 *     tags: [Tenant_Sponsors]
 *     requestBody:
 *       required: true
 *       content:
 *         application/json:
 *           schema:
 *             type: object
 *             properties:
 *               guid:
 *                 type: uuid
 *                 example: 495db854-849b-41cd-b6b9-9c84824c92ba
 *     responses:
 *       200:
 *         description: Patrocinador deshabilitado exitosamente
 *         content:
 *           application/json:
 *             schema:
 *               $ref: '#/components/responses/success'
 *       400:
 *         description: Error en los datos enviados
 *         content:
 *           application/json:
 *             schema:
 *               $ref: '#/components/responses/BadRequest'
 *       403:
 *         description: Error de autenticación
 *         content:
 *           application/json:
 *             schema:
 *               $ref: '#/components/responses/UnauthorizedError'
 *       404:
 *         description: Jugador no encontrado
 *         content:
 *           application/json:
 *             schema:
 *               $ref: '#/components/responses/NotFoundError'
 *       500:
 *         description: Error en el servidor
 *         content:
 *           application/json:
 *             schema:
 *               $ref: '#/components/responses/InternalServerError'
 *     security:
 *        - bearerAuth: []
 */
router.patch(
    '/DisableSponsor/:guid',
    authMiddleware,
    validatorDisableSponsor(),
    DisableSponsor
)

// Ruta para habilitar un patrocinador
/**
 * @swagger
 * /tenant_sponsors/EnableSponsor/{guid}:
 *   patch:
 *     summary: Habilitar un patrocinador
 *     tags: [Tenant_Sponsors]
 *     requestBody:
 *       required: true
 *       content:
 *         application/json:
 *           schema:
 *             type: object
 *             properties:
 *               guid:
 *                 type: uuid
 *                 example: 495db854-849b-41cd-b6b9-9c84824c92ba
 *     responses:
 *       200:
 *         description: Patrocinador habilitado exitosamente
 *         content:
 *           application/json:
 *             schema:
 *               $ref: '#/components/responses/success'
 *       400:
 *         description: Error en los datos enviados
 *         content:
 *           application/json:
 *             schema:
 *               $ref: '#/components/responses/BadRequest'
 *       403:
 *         description: Error de autenticación
 *         content:
 *           application/json:
 *             schema:
 *               $ref: '#/components/responses/UnauthorizedError'
 *       404:
 *         description: Jugador no encontrado
 *         content:
 *           application/json:
 *             schema:
 *               $ref: '#/components/responses/NotFoundError'
 *       500:
 *         description: Error en el servidor
 *         content:
 *           application/json:
 *             schema:
 *               $ref: '#/components/responses/InternalServerError'
 *     security:
 *        - bearerAuth: []
 */

router.patch(
    '/EnableSponsor/:guid',
    authMiddleware,
    validatorEnableSponsor(),
    EnableSponsor
)

// Ruta para obtener todos los patrocinadores
/**
 * @swagger
 * /tenant_sponsors/SponsorsAll:
 *   get:
 *     summary: Obtener todos los patrocinadores
 *     tags: [Tenant_Sponsors]
 *     responses:
 *       200:
 *         description: Patrocinadores obtenidos exitosamente
 *         content:
 *           application/json:
 *             schema:
 *               $ref: '#/components/responses/success'
 *       403:
 *         description: Error de autenticación
 *         content:
 *           application/json:
 *             schema:
 *               $ref: '#/components/responses/UnauthorizedError'
 *       404:
 *         description: No se encontraron patrocinadores
 *         content:
 *           application/json:
 *             schema:
 *               $ref: '#/components/responses/NotFoundError'
 *       500:
 *         description: Error en el servidor
 *         content:
 *           application/json:
 *             schema:
 *               $ref: '#/components/responses/InternalServerError'
 *     security:
 *        - bearerAuth: []
 */
router.get('/SponsorsAll', authMiddleware, SponsorsAll)

// Ruta para contar todos los patrocinadores
/**
 * @swagger
 * /tenant_sponsors/SponsorsCount:
 *   get:
 *     summary: Contar todos los patrocinadores
 *     tags: [Tenant_Sponsors]
 *     responses:
 *       200:
 *         description: Patrocinadores contados exitosamente
 *         content:
 *           application/json:
 *             schema:
 *               $ref: '#/components/responses/success'
 *       403:
 *         description: Error de autenticación
 *         content:
 *           application/json:
 *             schema:
 *               $ref: '#/components/responses/UnauthorizedError'
 *       404:
 *         description: No se encontraron patrocinadores
 *         content:
 *           application/json:
 *             schema:
 *               $ref: '#/components/responses/NotFoundError'
 *       500:
 *         description: Error en el servidor
 *         content:
 *           application/json:
 *             schema:
 *               $ref: '#/components/responses/InternalServerError'
 *     security:
 *        - bearerAuth: []
 */
router.get('/SponsorsCount', authMiddleware, SponsorsCount)

module.exports = router
