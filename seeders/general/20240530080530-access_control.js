const { v4: uuidv4 } = require('uuid')
const GUIDsGeneral = require('../guids_general')
module.exports = {
    async up(queryInterface, Sequelize) {
        // Inserta los registros de control de acceso por defecto
        await queryInterface.bulkInsert(
            'access_control',
            [
                {
                    guid: uuidv4(),
                    role_guid: GUIDsGeneral.roleSuperAdminGuid,
                    module_guid: GUIDsGeneral.moduleHomeGuid,
                    permissions_guid: GUIDsGeneral.permissionCreateGuid,
                    plan_guid: GUIDsGeneral.planUnicoGuid,
                    license_guid: GUIDsGeneral.licensePlanUnicoGuid,
                },
                {
                    guid: uuidv4(),
                    role_guid: GUIDsGeneral.roleSuperAdminGuid,
                    module_guid: GUIDsGeneral.moduleHomeGuid,
                    permissions_guid: GUIDsGeneral.permissionReadGuid,
                    plan_guid: GUIDsGeneral.planUnicoGuid,
                    license_guid: GUIDsGeneral.licensePlanUnicoGuid,
                },
                {
                    guid: uuidv4(),
                    role_guid: GUIDsGeneral.roleSuperAdminGuid,
                    module_guid: GUIDsGeneral.moduleHomeGuid,
                    permissions_guid: GUIDsGeneral.permissionUpdateGuid,
                    plan_guid: GUIDsGeneral.planUnicoGuid,
                    license_guid: GUIDsGeneral.licensePlanUnicoGuid,
                },
                {
                    guid: uuidv4(),
                    role_guid: GUIDsGeneral.roleSuperAdminGuid,
                    module_guid: GUIDsGeneral.moduleHomeGuid,
                    permissions_guid: GUIDsGeneral.permissionDeleteGuid,
                    plan_guid: GUIDsGeneral.planUnicoGuid,
                    license_guid: GUIDsGeneral.licensePlanUnicoGuid,
                },
                {
                    guid: uuidv4(),
                    role_guid: GUIDsGeneral.roleSuperAdminGuid,
                    module_guid: GUIDsGeneral.moduleHomeGuid,
                    permissions_guid: GUIDsGeneral.PermissionEnableCreateGuid,
                    plan_guid: GUIDsGeneral.planUnicoGuid,
                    license_guid: GUIDsGeneral.licensePlanUnicoGuid,
                },
                {
                    guid: uuidv4(),
                    role_guid: GUIDsGeneral.roleSuperAdminGuid,
                    module_guid: GUIDsGeneral.moduleHomeGuid,
                    permissions_guid: GUIDsGeneral.permissionDisableGuid,
                    plan_guid: GUIDsGeneral.planUnicoGuid,
                    license_guid: GUIDsGeneral.licensePlanUnicoGuid,
                },
                {
                    guid: uuidv4(),
                    role_guid: GUIDsGeneral.roleSuperAdminGuid,
                    module_guid: GUIDsGeneral.moduleFinanzasGuid,
                    permissions_guid: GUIDsGeneral.permissionCreateGuid,
                    plan_guid: GUIDsGeneral.planUnicoGuid,
                    license_guid: GUIDsGeneral.licensePlanUnicoGuid,
                },
                {
                    guid: uuidv4(),
                    role_guid: GUIDsGeneral.roleSuperAdminGuid,
                    module_guid: GUIDsGeneral.moduleFinanzasGuid,
                    permissions_guid: GUIDsGeneral.permissionReadGuid,
                    plan_guid: GUIDsGeneral.planUnicoGuid,
                    license_guid: GUIDsGeneral.licensePlanUnicoGuid,
                },
                {
                    guid: uuidv4(),
                    role_guid: GUIDsGeneral.roleSuperAdminGuid,
                    module_guid: GUIDsGeneral.moduleFinanzasGuid,
                    permissions_guid: GUIDsGeneral.permissionUpdateGuid,
                    plan_guid: GUIDsGeneral.planUnicoGuid,
                    license_guid: GUIDsGeneral.licensePlanUnicoGuid,
                },
                {
                    guid: uuidv4(),
                    role_guid: GUIDsGeneral.roleSuperAdminGuid,
                    module_guid: GUIDsGeneral.moduleFinanzasGuid,
                    permissions_guid: GUIDsGeneral.permissionDeleteGuid,
                    plan_guid: GUIDsGeneral.planUnicoGuid,
                    license_guid: GUIDsGeneral.licensePlanUnicoGuid,
                },
                {
                    guid: uuidv4(),
                    role_guid: GUIDsGeneral.roleSuperAdminGuid,
                    module_guid: GUIDsGeneral.moduleFinanzasGuid,
                    permissions_guid: GUIDsGeneral.PermissionEnableCreateGuid,
                    plan_guid: GUIDsGeneral.planUnicoGuid,
                    license_guid: GUIDsGeneral.licensePlanUnicoGuid,
                },
                {
                    guid: uuidv4(),
                    role_guid: GUIDsGeneral.roleSuperAdminGuid,
                    module_guid: GUIDsGeneral.moduleFinanzasGuid,
                    permissions_guid: GUIDsGeneral.permissionDisableGuid,
                    plan_guid: GUIDsGeneral.planUnicoGuid,
                    license_guid: GUIDsGeneral.licensePlanUnicoGuid,
                },
                {
                    guid: uuidv4(),
                    role_guid: GUIDsGeneral.roleSuperAdminGuid,
                    module_guid: GUIDsGeneral.moduleFinanzasGuid,
                    permissions_guid: GUIDsGeneral.permissionCreateGuid,
                    plan_guid: GUIDsGeneral.planUnicoGuid,
                    license_guid: GUIDsGeneral.licensePlanUnicoGuid,
                },
                {
                    guid: uuidv4(),
                    role_guid: GUIDsGeneral.roleSuperAdminGuid,
                    module_guid: GUIDsGeneral.moduleFinanzasGuid,
                    permissions_guid: GUIDsGeneral.permissionReadGuid,
                    plan_guid: GUIDsGeneral.planUnicoGuid,
                    license_guid: GUIDsGeneral.licensePlanUnicoGuid,
                },
            ],
            {}
        )
    },
    async down(queryInterface, Sequelize) {
        await queryInterface.bulkDelete('access_control', null, {})
    },
}
