const detectSubdomain = (req, res, next) => {
    // Obtén el encabezado Host
    const host = req.headers.host

    // Extrae el subdominio
    const subdomain = host.split('.')[0]

    // Almacena el subdominio en la solicitud
    req.subdomain = subdomain

    // Log para debug
    console.log(`Subdomain: ${subdomain}`)
    console.log(`Host: ${host}`)

    next()
}

module.exports = detectSubdomain
