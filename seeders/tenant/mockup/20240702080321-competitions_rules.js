const { v4: uuidv4 } = require('uuid')
const GUIDsTenant = require('../../guids_tenant')
module.exports = {
    async up(queryInterface, Sequelize) {
        await queryInterface.bulkInsert('competitions_rules', [
            // reglas de una liga de futbol
            {
                guid: uuidv4(),
                type_competition_guid: GUIDsTenant.typesCompetitionsLigaGuid,
                title: 'Reglas de una liga de futbol',
                description: 'Reglas de una liga de futbol',
                is_active: 1,
                created_at: new Date(),
            },
        ])
    },

    async down(queryInterface, Sequelize) {
        await queryInterface.bulkDelete('competitions_rules', null, {})
    },
}
