const { Op } = require('sequelize')
const { v4: uuidv4 } = require('uuid')
const { matchedData } = require('express-validator')
const { handleHttpSuccess } = require('../../utils/handleSuccess')
const { handleHttpError } = require('../../utils/handleError')
const { handleHttpCount } = require('../../utils/handleCount')
const {
    getSequelize,
    getSeasonsModel,
} = require('../../helpers/sequelizeHelper')

/**
 * Este controlador es el encargado de registrar una temporada
 * @param {*} req
 * @param {*} res
 */
const CreateSeason = async (req, res) => {
    try {
        const sequelize = getSequelize(req)
        const SeasonModel = getSeasonsModel(sequelize)
        const data = matchedData(req)
        const created_by = req.user.guid
        const created_at = new Date()
        const guid = await uuidv4()
        data.created_by = created_by
        data.created_at = created_at
        data.guid = guid

        const season = await SeasonModel.create(data)
        return handleHttpSuccess(res, 'Temporada creada.', 201, season)
    } catch (error) {
        console.log('Error al crear la temporada:', error)
        handleHttpError(res, 'Error al crear la temporada.', 403, error)
    }
}

/**
 * Este controlador es el encargado de obtener las temporadas activas
 * @param {*} req
 * @param {*} res
 */
const Seasons = async (req, res) => {
    try {
        const sequelize = getSequelize(req)
        const SeasonModel = getSeasonsModel(sequelize)

        const seasons = await SeasonModel.findAll({
            where: { is_active: 1 },
        })

        if (!seasons.length) {
            handleHttpError(res, 'No se encontraron temporadas.', 404)
            return
        }

        handleHttpSuccess(res, 'Temporadas encontradas.', 201, seasons)
    } catch (error) {
        console.log('Error obteniendo temporadas:', error)
        handleHttpError(res, 'Error obteniendo temporadas.', 403, error)
    }
}

/**
 * Este controlador es el encargado de obtener una temporada por GUID
 * @param {*} req
 * @param {*} res
 */
const Season = async (req, res) => {
    try {
        const sequelize = getSequelize(req)
        const SeasonModel = getSeasonsModel(sequelize)
        const { guid } = req.params

        const season = await SeasonModel.findOne({
            where: { guid },
        })

        if (!season) {
            handleHttpError(res, 'La temporada no existe.', 404)
            return
        }

        if (season.is_active == 0) {
            handleHttpError(res, 'La temporada ha sido deshabilitada.', 401)
            return
        }

        handleHttpSuccess(res, 'Temporada encontrada.', 201, season)
    } catch (error) {
        console.log('Error obteniendo temporada:', error)
        handleHttpError(res, 'Error obteniendo temporada.', 403, error)
    }
}

/**
 * Este controlador es el encargado de actualizar una temporada
 * @param {*} req
 * @param {*} res
 */
const UpdateSeason = async (req, res) => {
    try {
        const sequelize = getSequelize(req)
        const SeasonModel = getSeasonsModel(sequelize)
        const { guid } = req.params
        const data = matchedData(req)
        const updated_by = req.user.guid
        const updated_at = new Date()
        data.updated_by = updated_by
        data.updated_at = updated_at

        const [updateCount] = await SeasonModel.update(data, {
            where: { guid },
        })

        if (updateCount === 0) {
            return handleHttpError(res, 'La temporada no existe.', 404)
        }

        const season = await SeasonModel.findOne({
            where: { guid },
        })

        if (!season) {
            handleHttpError(res, 'La temporada no existe.', 404)
            return
        }

        if (season.is_active == 0) {
            handleHttpError(res, 'La temporada ha sido deshabilitada.', 401)
            return
        }

        handleHttpSuccess(res, 'Temporada actualizada.', 201, season)
    } catch (error) {
        console.log('Error actualizando temporada:', error)
        handleHttpError(res, 'Error actualizando temporada.', 403, error)
    }
}

/**
 * Este controlador es el encargado de eliminar una temporada
 * @param {*} req
 * @param {*} res
 */
const DeleteSeason = async (req, res) => {
    try {
        const sequelize = getSequelize(req)
        const SeasonModel = getSeasonsModel(sequelize)
        const { guid } = req.params
        const deleted_by = req.user.guid
        const deleted_at = new Date()
        const [updateCount] = await SeasonModel.update(
            {
                is_active: 0,
                deleted_by,
                deleted_at,
            },
            {
                where: { guid },
            }
        )

        if (updateCount === 0) {
            return handleHttpError(res, 'La temporada no existe.', 404)
        }

        handleHttpSuccess(res, 'Temporada eliminada.', 201)
    } catch (error) {
        console.log('Error eliminando temporada:', error)
        handleHttpError(res, 'Error eliminando temporada.', 403, error)
    }
}

/**
 * Este controlador es el encargado de desactivar una temporada
 * @param {*} req
 * @param {*} res
 */
const DisableSeason = async (req, res) => {
    try {
        const sequelize = getSequelize(req)
        const SeasonModel = getSeasonsModel(sequelize)
        const { guid } = req.params
        const updated_by = req.user.guid
        const updated_at = new Date()
        const [updateCount] = await SeasonModel.update(
            {
                is_active: 0,
                updated_by,
                updated_at,
            },
            {
                where: { guid },
            }
        )

        if (updateCount === 0) {
            return handleHttpError(res, 'La temporada no existe.', 404)
        }

        handleHttpSuccess(res, 'Temporada deshabilitada.', 201)
    } catch (error) {
        console.log('Error deshabilitando temporada:', error)
        handleHttpError(res, 'Error deshabilitando temporada.', 403, error)
    }
}

/**
 * Este controlador es el encargado de activar una temporada
 * @param {*} req
 * @param {*} res
 */
const EnableSeason = async (req, res) => {
    try {
        const sequelize = getSequelize(req)
        const SeasonModel = getSeasonsModel(sequelize)
        const { guid } = req.params
        const updated_by = req.user.guid
        const updated_at = new Date()
        const [updateCount] = await SeasonModel.update(
            {
                is_active: 1,
                updated_by,
                updated_at,
            },
            {
                where: { guid },
            }
        )

        if (updateCount === 0) {
            return handleHttpError(res, 'La temporada no existe.', 404)
        }

        handleHttpSuccess(res, 'Temporada habilitada.', 201)
    } catch (error) {
        console.log('Error habilitando temporada:', error)
        handleHttpError(res, 'Error habilitando temporada.', 403, error)
    }
}

/**
 * Este controlador es el encargado de obtener todas las temporadas
 * @param {*} req
 * @param {*} res
 */
const SeasonsAll = async (req, res) => {
    try {
        const sequelize = getSequelize(req)
        const SeasonModel = getSeasonsModel(sequelize)

        const seasons = await SeasonModel.findAll()

        if (!seasons.length) {
            handleHttpError(res, 'No se encontraron temporadas.', 404)
            return
        }

        handleHttpSuccess(res, 'Temporadas encontradas.', 201, seasons)
    } catch (error) {
        console.log('Error obteniendo temporadas:', error)
        handleHttpError(res, 'Error obteniendo temporadas.', 403, error)
    }
}

/**
 * Este controlador es el encargado de contar todas las temporadas
 * @param {*} req
 * @param {*} res
 */
const SeasonsCount = async (req, res) => {
    try {
        const sequelize = getSequelize(req)
        const SeasonModel = getSeasonsModel(sequelize)

        const count = await SeasonModel.count()

        handleHttpCount(res, 'Temporadas encontradas.', 201, count)
    } catch (error) {
        console.log('Error contando temporadas:', error)
        handleHttpError(res, 'Error contando temporadas.', 403, error)
    }
}

module.exports = {
    CreateSeason,
    Seasons,
    Season,
    UpdateSeason,
    DeleteSeason,
    DisableSeason,
    EnableSeason,
    SeasonsAll,
    SeasonsCount,
}
