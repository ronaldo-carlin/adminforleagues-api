const dotenv = require('dotenv')
const path = require('path')

require('dotenv').config({ path: path.resolve(__dirname, '../../.env.dev') })

module.exports = {
    development: {
        database: process.env.MYSQL_DATABASE || 'database_name',
        username: process.env.MYSQL_USER || 'root',
        password: process.env.MYSQL_PASSWORD || null,
        host: process.env.MYSQL_HOST || 'localhost',
        dialect: 'mysql',
    },
    // Configuración dinámica para migraciones de tenant
    tenant: {
        database: process.env.TENANT_DATABASE_NAME,
        username:
            process.env.TENANT_DATABASE_USER ||
            process.env.MYSQL_USER ||
            'root',
        password:
            process.env.TENANT_DATABASE_PASSWORD ||
            process.env.MYSQL_PASSWORD ||
            null,
        host:
            process.env.TENANT_DATABASE_HOST ||
            process.env.MYSQL_HOST ||
            'localhost',
        dialect: 'mysql',
    },
}
