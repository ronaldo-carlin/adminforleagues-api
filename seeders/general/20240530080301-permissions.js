const GUIDsGeneral = require('../guids_general')

module.exports = {
    async up(queryInterface, Sequelize) {
        // Inserta los permisos por defecto
        await queryInterface.bulkInsert(
            'permissions',
            [
                {
                    guid: GUIDsGeneral.permissionCreateGuid,
                    name: 'create',
                    is_active: 1, // 1: Activo, 0: Inactivo
                    created_at: new Date(),
                },
                {
                    guid: GUIDsGeneral.permissionReadGuid,
                    name: 'read',
                    is_active: 1, // 1: Activo, 0: Inactivo
                    created_at: new Date(),
                },
                {
                    guid: GUIDsGeneral.permissionUpdateGuid,
                    name: 'update',
                    is_active: 1, // 1: Activo, 0: Inactivo
                    created_at: new Date(),
                },
                {
                    guid: GUIDsGeneral.permissionDeleteGuid,
                    name: 'delete',
                    is_active: 1, // 1: Activo, 0: Inactivo
                    created_at: new Date(),
                },
                {
                    guid: GUIDsGeneral.permissionDisableGuid,
                    name: 'disable',
                    is_active: 1, // 1: Activo, 0: Inactivo
                    created_at: new Date(),
                },
                {
                    guid: GUIDsGeneral.permissionEnableCreateGuid,
                    name: 'enable',
                    is_active: 1, // 1: Activo, 0: Inactivo
                    created_at: new Date(),
                },
            ],
            {}
        )
    },

    async down(queryInterface, Sequelize) {
        await queryInterface.bulkDelete('permissions', null, {})
    },
}
