module.exports = {
    async up(queryInterface, Sequelize) {
        // entidad para las noticias del sistema
        await queryInterface.createTable('news', {
            // id del registro
            id: {
                type: Sequelize.INTEGER,
                primaryKey: true,
                autoIncrement: true,
                allowNull: false,
            },
            guid: {
                type: Sequelize.UUID,
                defaultValue: Sequelize.UUIDV4,
                unique: true,
                allowNull: false,
            },
            // tipo de la noticia
            type_news_guid: {
                type: Sequelize.UUID,
            },
            // titulo de la noticia
            title: {
                type: Sequelize.STRING,
            },
            // descripcion de la noticia
            description: {
                type: Sequelize.TEXT,
            },
            // fecha de publicacion de la noticia
            publication_date: {
                type: Sequelize.DATE,
            },
            // fecha de expiracion de la noticia
            expiration_date: {
                type: Sequelize.DATE,
            },
            // portada de la noticia
            cover: {
                type: Sequelize.TEXT,
            },
            // contenido de imagenes la noticia
            image_path: {
                type: Sequelize.TEXT,
            },
            // video de la noticia
            video_path: {
                type: Sequelize.TEXT,
            },
            // url de la noticia
            link: {
                type: Sequelize.TEXT,
            },
            // tags de la noticia
            tags: {
                type: Sequelize.TEXT,
            },
            // estado del registro
            is_active: {
                type: Sequelize.BOOLEAN,
                defaultValue: 1, // valor por defecto
            },
            // usuario que creo el registro
            created_by: {
                type: Sequelize.Sequelize.UUID,
            },
            // fecha de creacion
            created_at: {
                type: Sequelize.DATE,
                defaultValue: Sequelize.literal('CURRENT_TIMESTAMP'),
            },
            // usuario que actualizo el registro
            updated_by: {
                type: Sequelize.Sequelize.UUID,
            },
            // fecha de actualizacion
            updated_at: {
                type: Sequelize.DATE,
            },
            // usuario que elimino el registro
            deleted_by: {
                type: Sequelize.Sequelize.UUID,
            },
            // fecha de eliminacion
            deleted_at: {
                type: Sequelize.DATE,
            },
        })

        // indices
        await queryInterface.addIndex('news', ['guid'], {
            unique: true,
            name: 'idx_news_guid',
        })

        await queryInterface.addIndex('news', ['type_news_guid'], {
            name: 'idx_news_type_news_guid',
        })

        // llave foranea para el tipo de noticia
        await queryInterface.addConstraint('news', {
            fields: ['type_news_guid'],
            type: 'foreign key',
            name: 'fk_type_news_guid',
            references: {
                table: 'types_news',
                field: 'guid',
            },
            onUpdate: 'cascade',
            onDelete: 'cascade',
        })
    },

    async down(queryInterface, Sequelize) {
        await queryInterface.removeConstraint('news', 'fk_type_news_guid')
        await queryInterface.removeIndex('news', 'idx_news_guid')
        await queryInterface.removeIndex('news', 'idx_news_type_news_guid')
        await queryInterface.dropTable('news')
    },
}
