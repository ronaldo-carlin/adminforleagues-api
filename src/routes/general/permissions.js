const express = require('express')
const { matchedData } = require('express-validator')
const router = express.Router()

// validaciones
const {
    ValidatorCreatePermission,
    ValidatorUpdatePermission,
    ValidatorDeletePermission,
    ValidatorDisablePermission,
    ValidatorEnablePermission,
} = require('../../validators/general/permissions')

// Importa los controladores
const {
    createPermission,
    Permissions,
    Permission,
    updatePermission,
    deletePermission,
    disablePermission,
    enablePermission,
    permissionsAll,
    countAllPermissions,
} = require('../../controllers/general/permissions')

// Middleware de autenticación
const authMiddleware = require('../../middleware/session')

// Rutas protegidas que requieren autenticación
/**
 * @swagger
 * tags:
 *   name: Permissions
 *   description: API para la gestión de permisos
 */

// Ruta para crear un nuevo permiso
/**
 * @swagger
 * /permissions/CreatePermission:
 *   post:
 *     summary: Crear un nuevo permiso
 *     description: Esta ruta permite crear un nuevo permiso con nombre y descripción.
 *     tags: [Permissions]
 *     requestBody:
 *       required: true
 *       content:
 *         application/json:
 *           schema:
 *             type: object
 *             properties:
 *               name:
 *                 type: string
 *                 example: Leer
 *     responses:
 *       201:
 *         description: Permiso creado exitosamente
 *         content:
 *           application/json:
 *             schema:
 *               $ref: '#/components/responses/success'
 *       400:
 *         description: Error en los datos enviados
 *         content:
 *           application/json:
 *             schema:
 *               $ref: '#/components/responses/BadRequest'
 *       403:
 *         description: Error de autenticación
 *         content:
 *           application/json:
 *             schema:
 *               $ref: '#/components/responses/UnauthorizedError'
 *       409:
 *         description: Permiso ya existe
 *         content:
 *           application/json:
 *             schema:
 *               $ref: '#/components/responses/ConflictError'
 *       500:
 *         description: Error en el servidor
 *         content:
 *           application/json:
 *             schema:
 *               $ref: '#/components/responses/InternalServerError'
 *     security:
 *        - bearerAuth: []
 */
router.post(
    '/CreatePermission',
    authMiddleware,
    ValidatorCreatePermission,
    createPermission
)

// Ruta para obtener todos los permisos activos
/**
 * @swagger
 *  /permissions/Permissions:
 *   get:
 *     summary: Obtener todos los permisos activos
 *     tags: [Permissions]
 *     responses:
 *       201:
 *         description: Permisos obtenidos exitosamente
 *         content:
 *           application/json:
 *             schema:
 *               $ref: '#/components/responses/success'
 *       403:
 *         description: Error de autenticación
 *         content:
 *           application/json:
 *             schema:
 *               $ref: '#/components/responses/UnauthorizedError'
 *       500:
 *         description: Error en el servidor
 *         content:
 *           application/json:
 *             schema:
 *               $ref: '#/components/responses/InternalServerError'
 *     security:
 *        - bearerAuth: []
 */
router.get('/Permissions', authMiddleware, Permissions)

// Ruta para obtener un permiso
/**
 * @swagger
 * /permissions/Permission:
 *   get:
 *     summary: Obtiene un permiso por ID
 *     tags: [Permissions]
 *     requestBody:
 *       required: true
 *       content:
 *         application/json:
 *           schema:
 *             type: object
 *             properties:
 *               guid:
 *                 type: uuid
 *     responses:
 *       201:
 *         description: Permiso obtenido exitosamente
 *         content:
 *           application/json:
 *             schema:
 *               $ref: '#/components/responses/success'
 *       403:
 *         description: Error de autenticación
 *         content:
 *           application/json:
 *             schema:
 *               $ref: '#/components/responses/UnauthorizedError'
 *       404:
 *        description: Permiso no encontrado
 *        content:
 *          application/json:
 *            schema:
 *              $ref: '#/components/responses/NotFoundError'
 *       500:
 *         description: Error en el servidor
 *         content:
 *           application/json:
 *             schema:
 *               $ref: '#/components/responses/InternalServerError'
 *     security:
 *        - bearerAuth: []
 */
router.get('/Permission/:guid', authMiddleware, Permission)

// Ruta para actualizar un permiso
/**
 * @swagger
 * /permissions/UpdatePermission:
 *   patch:
 *     summary: Actualizar un permiso
 *     tags: [Permissions]
 *     requestBody:
 *       required: true
 *       content:
 *         application/json:
 *           schema:
 *             type: object
 *             properties:
 *               guid:
 *                 type: string
 *                 example: 6555ee6c-c713-46bc-8f0a-9ee7fde4ae89
 *               name:
 *                 type: string
 *                 example: Leer
 *     responses:
 *       201:
 *         description: Permiso actualizado exitosamente
 *         content:
 *           application/json:
 *             schema:
 *               $ref: '#/components/responses/success'
 *       400:
 *         description: Error en los datos enviados
 *         content:
 *           application/json:
 *             schema:
 *               $ref: '#/components/responses/BadRequest'
 *       403:
 *         description: Error de autenticación
 *         content:
 *           application/json:
 *             schema:
 *               $ref: '#/components/responses/UnauthorizedError'
 *       404:
 *        description: Permiso no encontrado
 *        content:
 *          application/json:
 *            schema:
 *              $ref: '#/components/responses/NotFoundError'
 *       500:
 *         description: Error en el servidor
 *         content:
 *           application/json:
 *             schema:
 *               $ref: '#/components/responses/InternalServerError'
 *     security:
 *        - bearerAuth: []
 */
router.patch(
    '/UpdatePermission/:guid',
    authMiddleware,
    ValidatorUpdatePermission,
    updatePermission
)

// Ruta para eliminar un permiso
/**
 * @swagger
 * /permissions/DeletePermission:
 *   delete:
 *     summary: Eliminar un permiso
 *     tags: [Permissions]
 *     requestBody:
 *       required: true
 *       content:
 *         application/json:
 *           schema:
 *             type: object
 *             properties:
 *               guid:
 *                 type: uuid
 *     responses:
 *       201:
 *         description: Permiso eliminado exitosamente
 *         content:
 *           application/json:
 *             schema:
 *               $ref: '#/components/responses/success'
 *       400:
 *         description: Error en los datos enviados
 *         content:
 *           application/json:
 *             schema:
 *               $ref: '#/components/responses/BadRequest'
 *       403:
 *         description: Error de autenticación
 *         content:
 *           application/json:
 *             schema:
 *               $ref: '#/components/responses/UnauthorizedError'
 *       404:
 *        description: Permiso no encontrado
 *        content:
 *          application/json:
 *            schema:
 *              $ref: '#/components/responses/NotFoundError'
 *       500:
 *         description: Error en el servidor
 *         content:
 *           application/json:
 *             schema:
 *               $ref: '#/components/responses/InternalServerError'
 *     security:
 *        - bearerAuth: []
 */
router.delete(
    '/DeletePermission/:guid',
    authMiddleware,
    ValidatorDeletePermission,
    deletePermission
)

// Ruta para desactivar un permiso
/**
 * @swagger
 * /permissions/DisablePermission:
 *   patch:
 *     summary: Desactivar un permiso
 *     tags: [Permissions]
 *     requestBody:
 *       required: true
 *       content:
 *         application/json:
 *           schema:
 *             type: object
 *             properties:
 *               guid:
 *                 type: uuid
 *     responses:
 *       201:
 *         description: Permiso desactivado exitosamente
 *         content:
 *           application/json:
 *             schema:
 *               $ref: '#/components/responses/success'
 *       400:
 *         description: Error en los datos enviados
 *         content:
 *           application/json:
 *             schema:
 *               $ref: '#/components/responses/BadRequest'
 *       403:
 *         description: Error de autenticación
 *         content:
 *           application/json:
 *             schema:
 *               $ref: '#/components/responses/UnauthorizedError'
 *       404:
 *        description: Permiso no encontrado
 *        content:
 *          application/json:
 *            schema:
 *              $ref: '#/components/responses/NotFoundError'
 *       500:
 *         description: Error en el servidor
 *         content:
 *           application/json:
 *             schema:
 *               $ref: '#/components/responses/InternalServerError'
 *     security:
 *        - bearerAuth: []
 */
router.patch(
    '/DisablePermission/:guid',
    authMiddleware,
    ValidatorDisablePermission,
    disablePermission
)

// Ruta para activar un permiso
/**
 * @swagger
 * /permissions/EnablePermission:
 *   patch:
 *     summary: Activar un permiso
 *     tags: [Permissions]
 *     requestBody:
 *       required: true
 *       content:
 *         application/json:
 *           schema:
 *             type: object
 *             properties:
 *               guid:
 *                 type: uuid
 *     responses:
 *       201:
 *         description: Permiso activado exitosamente
 *         content:
 *           application/json:
 *             schema:
 *               $ref: '#/components/responses/success'
 *       400:
 *         description: Error en los datos enviados
 *         content:
 *           application/json:
 *             schema:
 *               $ref: '#/components/responses/BadRequest'
 *       403:
 *         description: Error de autenticación
 *         content:
 *           application/json:
 *             schema:
 *               $ref: '#/components/responses/UnauthorizedError'
 *       404:
 *        description: Permiso no encontrado
 *        content:
 *          application/json:
 *            schema:
 *              $ref: '#/components/responses/NotFoundError'
 *       500:
 *         description: Error en el servidor
 *         content:
 *           application/json:
 *             schema:
 *               $ref: '#/components/responses/InternalServerError'
 *     security:
 *        - bearerAuth: []
 */
router.patch(
    '/EnablePermission/:guid',
    authMiddleware,
    ValidatorEnablePermission,
    enablePermission
)

// Ruta para obtener todos los permisos
/**
 * @swagger
 * /permissions/PermissionsAll:
 *   get:
 *     summary: Obtener todos los permisos
 *     tags: [Permissions]
 *     responses:
 *       201:
 *         description: Permisos obtenidos exitosamente
 *         content:
 *           application/json:
 *             schema:
 *               $ref: '#/components/responses/success'
 *       400:
 *         description: Error en los datos enviados
 *         content:
 *           application/json:
 *             schema:
 *               $ref: '#/components/responses/BadRequest'
 *       403:
 *         description: Error de autenticación
 *         content:
 *           application/json:
 *             schema:
 *               $ref: '#/components/responses/UnauthorizedError'
 *       500:
 *         description: Error en el servidor
 *         content:
 *           application/json:
 *             schema:
 *               $ref: '#/components/responses/InternalServerError'
 *     security:
 *        - bearerAuth: []
 */
router.get('/PermissionsAll', authMiddleware, permissionsAll)

// Ruta para contar todos los permisos
/**
 * @swagger
 * /permissions/PermissionsCount:
 *   get:
 *     summary: Contar todos los permisos
 *     tags: [Permissions]
 *     responses:
 *       201:
 *         description: Permisos contados exitosamente
 *         content:
 *           application/json:
 *             schema:
 *               $ref: '#/components/responses/success'
 *       400:
 *         description: Error en los datos enviados
 *         content:
 *           application/json:
 *             schema:
 *               $ref: '#/components/responses/BadRequest'
 *       403:
 *         description: Error de autenticación
 *         content:
 *           application/json:
 *             schema:
 *               $ref: '#/components/responses/UnauthorizedError'
 *       500:
 *         description: Error en el servidor
 *         content:
 *           application/json:
 *             schema:
 *               $ref: '#/components/responses/InternalServerError'
 *     security:
 *        - bearerAuth: []
 */
router.get('/PermissionsCount', authMiddleware, countAllPermissions)

module.exports = router
