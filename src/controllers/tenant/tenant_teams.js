const { Op } = require('sequelize')
const { v4: uuidv4 } = require('uuid')
const { matchedData } = require('express-validator')
const { handleHttpSuccess } = require('../../utils/handleSuccess')
const { handleHttpError } = require('../../utils/handleError')
const { handleHttpCount } = require('../../utils/handleCount')
const { getSequelize, getTeamsModel } = require('../../helpers/sequelizeHelper')

/**
 * Este controlador es el encargado de registrar un equipo
 * @param {*} req
 * @param {*} res
 */
const CreateTeam = async (req, res) => {
    try {
        const sequelize = getSequelize(req)
        const TeamModel = getTeamsModel(sequelize)
        const data = matchedData(req)
        const created_by = req.user.guid
        const created_at = new Date()
        const guid = await uuidv4()
        data.created_by = created_by
        data.created_at = created_at
        data.guid = guid

        const team = await TeamModel.create(data)
        return handleHttpSuccess(res, 'Equipo creado.', 201, team)
    } catch (error) {
        console.log('Error al crear el equipo:', error)
        handleHttpError(res, 'Error al crear el equipo.', 403, error)
    }
}

/**
 * Este controlador es el encargado de obtener los equipos activos
 * @param {*} req
 * @param {*} res
 */
const Teams = async (req, res) => {
    try {
        const sequelize = getSequelize(req)
        const TeamModel = getTeamsModel(sequelize)

        const teams = await TeamModel.findAll({
            where: { is_active: 1 },
        })

        if (!teams.length) {
            handleHttpError(res, 'No se encontraron equipos.', 404)
            return
        }

        handleHttpSuccess(res, 'Equipos encontrados.', 201, teams)
    } catch (error) {
        console.log('Error obteniendo equipos:', error)
        handleHttpError(res, 'Error obteniendo equipos.', 403, error)
    }
}

/**
 * Este controlador es el encargado de obtener un equipo por GUID
 * @param {*} req
 * @param {*} res
 */
const Team = async (req, res) => {
    try {
        const sequelize = getSequelize(req)
        const TeamModel = getTeamsModel(sequelize)
        const { guid } = req.params

        const team = await TeamModel.findOne({
            where: { guid },
        })

        if (!team) {
            handleHttpError(res, 'El equipo no existe.', 404)
            return
        }

        if (team.is_active == 0) {
            handleHttpError(res, 'El equipo ha sido deshabilitado.', 401)
            return
        }

        handleHttpSuccess(res, 'Equipo encontrado.', 201, team)
    } catch (error) {
        console.log('Error obteniendo equipo:', error)
        handleHttpError(res, 'Error obteniendo equipo.', 403, error)
    }
}

/**
 * Este controlador es el encargado de actualizar un equipo
 * @param {*} req
 * @param {*} res
 */
const UpdateTeam = async (req, res) => {
    try {
        const sequelize = getSequelize(req)
        const TeamModel = getTeamsModel(sequelize)
        const { guid } = req.params
        const data = matchedData(req)
        const updated_by = req.user.guid
        const updated_at = new Date()
        data.updated_by = updated_by
        data.updated_at = updated_at

        const [updateCount] = await TeamModel.update(data, {
            where: { guid },
        })

        if (updateCount === 0) {
            return handleHttpError(res, 'El equipo no existe.', 404)
        }

        const team = await TeamModel.findOne({
            where: { guid },
        })

        if (!team) {
            handleHttpError(res, 'El equipo no existe.', 404)
            return
        }

        if (team.is_active == 0) {
            handleHttpError(res, 'El equipo ha sido deshabilitado.', 401)
            return
        }

        handleHttpSuccess(res, 'Equipo actualizado.', 201, team)
    } catch (error) {
        console.log('Error actualizando equipo:', error)
        handleHttpError(res, 'Error actualizando equipo.', 403, error)
    }
}

/**
 * Este controlador es el encargado de eliminar un equipo
 * @param {*} req
 * @param {*} res
 */
const DeleteTeam = async (req, res) => {
    try {
        const sequelize = getSequelize(req)
        const TeamModel = getTeamsModel(sequelize)
        const { guid } = req.params
        const deleted_by = req.user.guid
        const deleted_at = new Date()
        const [updateCount] = await TeamModel.update(
            {
                is_active: 0,
                deleted_by,
                deleted_at,
            },
            {
                where: { guid },
            }
        )

        if (updateCount === 0) {
            return handleHttpError(res, 'El equipo no existe.', 404)
        }

        handleHttpSuccess(res, 'Equipo eliminado.', 201)
    } catch (error) {
        console.log('Error eliminando equipo:', error)
        handleHttpError(res, 'Error eliminando equipo.', 403, error)
    }
}

/**
 * Este controlador es el encargado de desactivar un equipo
 * @param {*} req
 * @param {*} res
 */
const DisableTeam = async (req, res) => {
    try {
        const sequelize = getSequelize(req)
        const TeamModel = getTeamsModel(sequelize)
        const { guid } = req.params
        const updated_by = req.user.guid
        const updated_at = new Date()
        const [updateCount] = await TeamModel.update(
            {
                is_active: 0,
                updated_by,
                updated_at,
            },
            {
                where: { guid },
            }
        )

        if (updateCount === 0) {
            return handleHttpError(res, 'El equipo no existe.', 404)
        }

        handleHttpSuccess(res, 'Equipo deshabilitado.', 201)
    } catch (error) {
        console.log('Error deshabilitando equipo:', error)
        handleHttpError(res, 'Error deshabilitando equipo.', 403, error)
    }
}

/**
 * Este controlador es el encargado de activar un equipo
 * @param {*} req
 * @param {*} res
 */
const EnableTeam = async (req, res) => {
    try {
        const sequelize = getSequelize(req)
        const TeamModel = getTeamsModel(sequelize)
        const { guid } = req.params
        const updated_by = req.user.guid
        const updated_at = new Date()
        const [updateCount] = await TeamModel.update(
            {
                is_active: 1,
                updated_by,
                updated_at,
            },
            {
                where: { guid },
            }
        )

        if (updateCount === 0) {
            return handleHttpError(res, 'El equipo no existe.', 404)
        }

        handleHttpSuccess(res, 'Equipo habilitado.', 201)
    } catch (error) {
        console.log('Error habilitando equipo:', error)
        handleHttpError(res, 'Error habilitando equipo.', 403, error)
    }
}

/**
 * Este controlador es el encargado de obtener todos los equipos
 * @param {*} req
 * @param {*} res
 */
const TeamsAll = async (req, res) => {
    try {
        const sequelize = getSequelize(req)
        const TeamModel = getTeamsModel(sequelize)

        const teams = await TeamModel.findAll()

        if (!teams.length) {
            handleHttpError(res, 'No se encontraron equipos.', 404)
            return
        }

        handleHttpSuccess(res, 'Equipos encontrados.', 201, teams)
    } catch (error) {
        console.log('Error obteniendo equipos:', error)
        handleHttpError(res, 'Error obteniendo equipos.', 403, error)
    }
}

/**
 * Este controlador es el encargado de contar todos los equipos
 * @param {*} req
 * @param {*} res
 */
const TeamsCount = async (req, res) => {
    try {
        const sequelize = getSequelize(req)
        const TeamModel = getTeamsModel(sequelize)

        const count = await TeamModel.count()

        handleHttpCount(res, 'Equipos encontrados.', 201, count)
    } catch (error) {
        console.log('Error contando equipos:', error)
        handleHttpError(res, 'Error contando equipos.', 403, error)
    }
}

module.exports = {
    CreateTeam,
    Teams,
    Team,
    UpdateTeam,
    DeleteTeam,
    DisableTeam,
    EnableTeam,
    TeamsAll,
    TeamsCount,
}
