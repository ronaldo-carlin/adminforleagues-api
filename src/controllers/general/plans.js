const { matchedData } = require('express-validator')
const PlansModel = require('../../models/general/plans')
const { handleHttpError } = require('../../utils/handleError')
const { handleHttpSuccess } = require('../../utils/handleSuccess')
const { handleHttpCount } = require('../../utils/handleCount')

/**
 * Este controlador es el encargado de crear planes
 * @param {*} req
 * @param {*} res
 */
const CreatePlan = async (req, res) => {
    try {
        const data = matchedData(req)
        const created_by = req.user.guid
        const created_at = new Date()
        data.created_by = created_by
        data.created_at = created_at
        const plan = await PlansModel.create(data)
        return handleHttpSuccess(res, 'Se ha creado el plan.', 201, plan)
    } catch (error) {
        console.log(error)
        return handleHttpError(res, '', 403, error)
    }
}

/**
 * Este controlador es el encargado de obtener todos los planes activos
 * @param {*} req
 * @param {*} res
 */
const Plans = async (req, res) => {
    try {
        const plans = await PlansModel.findAll({
            where: { is_active: 1 },
        })
        if (!plans) {
            return handleHttpError(res, 'El plan no existe.', 404)
        }
        const message = plans.length
            ? 'Se han encontrado los datos.'
            : 'No se encontraron datos.'
        return handleHttpSuccess(res, message, 201, plans)
    } catch (error) {
        console.log(error)
        return handleHttpError(res, '', 403, error)
    }
}

/**
 * Este controlador es el encargado de obtener planes por guid
 * @param {*} req
 * @param {*} res
 */
const Plan = async (req, res) => {
    try {
        const { guid } = req.params
        const DataPlan = await PlansModel.findOne({
            where: { guid },
        })
        if (!DataPlan) {
            return handleHttpError(res, 'El plan no existe.', 404)
        }
        return handleHttpSuccess(
            res,
            'Se han encontrado los datos.',
            201,
            DataPlan
        )
    } catch (error) {
        console.log(error)
        return handleHttpError(res, '', 403, error)
    }
}

/**
 * Este controlador es el encargado de actualizar planes
 * @param {*} req
 * @param {*} res
 */
const UpdatePlan = async (req, res) => {
    try {
        const { guid } = req.params
        const updated_by = req.user.guid
        const updated_at = new Date()
        const data = matchedData(req)
        data.updated_by = updated_by
        data.updated_at = updated_at
        const [updateCount] = await PlansModel.update(data, {
            where: { guid },
        })
        if (updateCount === 0) {
            return handleHttpError(res, 'El plan no existe.', 404)
        }
        const updatedPlan = await PlansModel.findOne({
            where: { guid },
        })
        if (!updatedPlan) {
            return handleHttpError(
                res,
                'Error al obtener el plan actualizado.',
                500
            )
        }
        return handleHttpSuccess(
            res,
            'Se ha actualizado el plan.',
            201,
            updatedPlan
        )
    } catch (error) {
        console.log(error)
        return handleHttpError(res, '', 403, error)
    }
}

/**
 * Este controlador es el encargado de eliminar planes
 * @param {*} req
 * @param {*} res
 */
const DeletePlan = async (req, res) => {
    try {
        const { guid } = req.params
        const deleted_by = req.user.guid
        const deleted_at = new Date()
        const [updateCount] = await PlansModel.update(
            {
                is_active: 0,
                deleted_by,
                deleted_at,
            },
            {
                where: { guid },
            }
        )
        if (updateCount === 0) {
            return handleHttpError(res, 'El plan no existe.', 404)
        }
        handleHttpSuccess(res, 'Se ha eliminado el plan.', 201)
    } catch (error) {
        console.log(error)
        handleHttpError(res, '', 403, error)
    }
}

/**
 * Este controlador es el encargado de desactivar planes
 * @param {*} req
 * @param {*} res
 */
const DisablePlan = async (req, res) => {
    try {
        const { guid } = req.params
        const updated_by = req.user.guid
        const updated_at = new Date()
        const [updateCount] = await PlansModel.update(
            {
                is_active: 0,
                updated_by,
                updated_at,
            },
            {
                where: { guid },
            }
        )
        if (updateCount === 0) {
            return handleHttpError(res, 'El plan no existe.', 404)
        }
        handleHttpSuccess(res, 'Se ha desactivado el plan.', 201)
    } catch (error) {
        console.log(error)
        handleHttpError(res, '', 403, error)
    }
}

/**
 * Este controlador es el encargado de activar planes
 * @param {*} req
 * @param {*} res
 */
const EnablePlan = async (req, res) => {
    try {
        const { guid } = req.params
        const updated_by = req.user.guid
        const updated_at = new Date()
        const [updateCount] = await PlansModel.update(
            {
                is_active: 1,
                updated_by,
                updated_at,
            },
            {
                where: { guid },
            }
        )
        if (updateCount === 0) {
            return handleHttpError(res, 'El plan no existe.', 404)
        }
        handleHttpSuccess(res, 'Se ha activado el plan.', 201)
    } catch (error) {
        console.log(error)
        handleHttpError(res, '', 403, error)
    }
}

/**
 * Este controlador es el encargado de obtener todos los planes
 * @param {*} req
 * @param {*} res
 */
const PlansAll = async (req, res) => {
    try {
        const DataPlans = await PlansModel.findAll()
        if (!DataPlans) {
            handleHttpError(res, 'No se encontraron datos.', 404)
            return
        }
        const message = DataPlans.length
            ? 'Se han encontrado los datos.'
            : 'No se encontraron datos.'
        handleHttpSuccess(res, message, 201, DataPlans)
    } catch (error) {
        console.log(error)
        handleHttpError(res, '', 403, error)
    }
}

/**
 * Este controlador es el encargado de contar todos los planes
 * @param {*} req
 * @param {*} res
 */
const CountAllPlans = async (req, res) => {
    try {
        const DataPlans = await PlansModel.count()
        if (!DataPlans) {
            handleHttpError(res, 'No se encontraron datos.', 404)
            return
        }
        handleHttpCount(res, 'Se han encontrado los datos.', 201, DataPlans)
    } catch (error) {
        console.log(error)
        handleHttpError(res, '', 403, error)
    }
}

module.exports = {
    CreatePlan,
    Plans,
    Plan,
    UpdatePlan,
    DeletePlan,
    DisablePlan,
    EnablePlan,
    PlansAll,
    CountAllPlans,
}
