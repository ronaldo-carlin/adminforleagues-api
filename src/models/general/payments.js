// Importar Sequelize directamente
const { Sequelize } = require('sequelize')
// Importar tu configuración de sequelize
const { sequelize } = require('../../database/mysql')
// Importar el modelo de usuarios
const AuthModel = require('./auth')
// Importar el modelo de planes
const PlansModel = require('./plans')
// Importar el modelo de licencias
const LicensesModel = require('./licenses')

const PaymentsModel = sequelize.define(
    'payments',
    {
        id: {
            type: Sequelize.INTEGER,
            primaryKey: true,
            autoIncrement: true,
        },
        guid: {
            type: Sequelize.UUID,
            defaultValue: Sequelize.UUIDV4,
            unique: true,
        },
        user_guid: {
            type: Sequelize.TEXT,
        },
        plan_guid: {
            type: Sequelize.TEXT,
        },
        email: {
            type: Sequelize.TEXT,
        },
        amount: {
            type: Sequelize.DECIMAL,
        },
        method: {
            type: Sequelize.TEXT,
        },
        transaction: {
            type: Sequelize.TEXT,
        },
        reference: {
            type: Sequelize.TEXT,
        },
        concept: {
            type: Sequelize.TEXT,
        },
        card: {
            type: Sequelize.TEXT,
        },
        bank: {
            type: Sequelize.TEXT,
        },
        message: {
            type: Sequelize.TEXT,
        },
        status: {
            type: Sequelize.TEXT,
        },
        is_active: {
            type: Sequelize.BOOLEAN,
        },
        created_by: {
            type: Sequelize.UUID,
        },
        updated_by: {
            type: Sequelize.UUID,
        },
        deleted_by: {
            type: Sequelize.UUID,
        },
        created_at: {
            type: Sequelize.DATE,
        },
        updated_at: {
            type: Sequelize.DATE,
        },
        deleted_at: {
            type: Sequelize.DATE,
        },
    },
    {
        tableName: 'payments',
        timestamps: false,
        defaultScope: {
            attributes: {
                exclude: [
                    'created_by',
                    'updated_by',
                    'deleted_by',
                    'created_at',
                    'updated_at',
                    'deleted_at',
                ],
            },
            include: [
                {
                    model: AuthModel,
                    as: 'user',
                    attributes: {
                        exclude: [
                            'password',
                            'is_active',
                            'created_at',
                            'created_by',
                            'updated_at',
                            'updated_by',
                            'deleted_at',
                            'deleted_by',
                        ],
                    },
                    where: { is_active: 1 },
                },
                {
                    model: PlansModel,
                    as: 'plan',
                    attributes: {
                        exclude: [
                            'is_active',
                            'created_at',
                            'created_by',
                            'updated_at',
                            'updated_by',
                            'deleted_at',
                            'deleted_by',
                        ],
                    },
                    where: { is_active: 1 },
                },
            ],
        },
    }
)

// Relación uno a uno con el modelo de usuarios
PaymentsModel.belongsTo(AuthModel, {
    foreignKey: 'user_guid', // El campo de la tabla payments que se usará para la relación
    targetKey: 'guid', // El campo de la tabla auth que se usará para la relación
    as: 'user', // El alias de la relación
    allowNull: false, // No permitir valores nulos
    where: { is_active: 1 }, // Solo traer usuarios activos
    exclude: [
        'password',
        'is_active',
        'created_at',
        'created_by',
        'updated_at',
        'updated_by',
        'deleted_at',
        'deleted_by',
    ], // Excluir estos campos de la relación
})

// Relación uno a uno con el modelo de planes
PaymentsModel.belongsTo(PlansModel, {
    foreignKey: 'plan_guid', // El campo de la tabla payments que se usará para la relación
    targetKey: 'guid', // El campo de la tabla plans que se usará para la relación
    as: 'plan', // El alias de la relación
    allowNull: false, // No permitir valores nulos
    where: { is_active: 1 }, // Solo traer planes activos
    exclude: [
        'is_active',
        'created_at',
        'created_by',
        'updated_at',
        'updated_by',
        'deleted_at',
        'deleted_by',
    ], // Excluir estos campos de la relación
})

module.exports = PaymentsModel
