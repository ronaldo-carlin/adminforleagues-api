const swaggerJsdoc = require('swagger-jsdoc')
const path = require('path')

// Definición de la configuración de Swagger
const swaggerDefinition = {
    openapi: '3.0.3',
    info: {
        title: 'Documentación de API Admin For Leagues',
        version: '1.0.0',
        description:
            'Esta es la documentación de la API de administrador de ligas.',
        license: {
            name: 'MIT',
            url: 'https://opensource.org/licenses/MIT',
        },
        contact: {
            name: 'API',
            url: 'http://localhost:3013/api',
            email: 'info@localhost:3013',
        },
    },
    servers: [
        {
            url: 'http://localhost:3013/api',
            description: 'Servidor de Desarrollo',
        },
    ],
    components: {
        securitySchemes: {
            bearerAuth: {
                type: 'http',
                scheme: 'bearer',
                bearerFormat: 'JWT',
            },
        },
        schemas: {
            CreateRol: {
                type: 'object',
                required: ['name', 'description'],
                properties: {
                    name: {
                        type: 'string',
                        description: 'Nombre del rol',
                        example: 'Administrador',
                    },
                    description: {
                        type: 'string',
                        description: 'Descripción del rol',
                        example: 'Rol con todos los permisos',
                    },
                },
            },
            Roles: {
                type: 'object',
                properties: {
                    guid: {
                        type: 'string',
                        description: 'GUID único del rol',
                        example: 1,
                    },
                    name: {
                        type: 'string',
                        description: 'Nombre del rol',
                        example: 'Administrador',
                    },
                    description: {
                        type: 'string',
                        description: 'Descripción del rol',
                        example: 'Rol con todos los permisos',
                    },
                },
            },
            Rol: {
                type: 'object',
                properties: {
                    id: {
                        type: 'integer',
                        description: 'ID único del rol',
                        example: 1,
                    },
                    name: {
                        type: 'string',
                        description: 'Nombre del rol',
                        example: 'Administrador',
                    },
                    description: {
                        type: 'string',
                        description: 'Descripción del rol',
                        example: 'Rol con todos los permisos',
                    },
                },
            },
            UpdateRol: {
                type: 'object',
                required: ['guid', 'name', 'description'],
                properties: {
                    guid: {
                        type: 'string',
                        description: 'GUID único del rol',
                        example: 1,
                    },
                    name: {
                        type: 'string',
                        description: 'Nombre del rol',
                        example: 'Administrador Actualizado',
                    },
                    description: {
                        type: 'string',
                        description: 'Descripción del rol',
                        example: 'Rol con permisos actualizados',
                    },
                },
            },
            DeleteRol: {
                type: 'object',
                required: ['guid'],
                properties: {
                    id: {
                        type: 'string',
                        description: 'GUID único del rol a eliminar',
                        example: 1,
                    },
                },
            },
            DisableRol: {
                type: 'object',
                required: ['guid'],
                properties: {
                    id: {
                        type: 'string',
                        description: 'GUID único del rol a deshabilitar',
                        example: 1,
                    },
                },
            },
            EnableRol: {
                type: 'object',
                required: ['guid'],
                properties: {
                    id: {
                        type: 'string',
                        description: 'GUID único del rol a habilitar',
                        example: 1,
                    },
                },
            },
            CreateUser: {
                type: 'object',
                required: ['name', 'email', 'password'],
                properties: {
                    name: {
                        type: 'string',
                        description: 'Nombre del usuario',
                        example: 'John Doe',
                    },
                    email: {
                        type: 'string',
                        description: 'Correo electrónico del usuario',
                        example: 'john.doe@example.com',
                    },
                    password: {
                        type: 'string',
                        description: 'Contraseña del usuario',
                        example: 'password123',
                    },
                },
            },
            LoginUser: {
                type: 'object',
                required: ['email', 'password'],
                properties: {
                    email: {
                        type: 'string',
                        description: 'Correo electrónico del usuario',
                        example: 'john.doe@example.com',
                    },
                    password: {
                        type: 'string',
                        description: 'Contraseña del usuario',
                        example: 'password123',
                    },
                },
            },
            ResetPassword: {
                type: 'object',
                required: ['email'],
                properties: {
                    email: {
                        type: 'string',
                        description: 'Correo electrónico del usuario',
                        example: 'john.doe@example.com',
                    },
                },
            },
            RenewAccessToken: {
                type: 'object',
                required: ['refreshToken'],
                properties: {
                    refreshToken: {
                        type: 'string',
                        description: 'Token de refresco',
                        example: 'your-refresh-token',
                    },
                },
            },
            GetUser: {
                type: 'object',
                required: ['id'],
                properties: {
                    id: {
                        type: 'integer',
                        description: 'ID del usuario',
                        example: 1,
                    },
                },
            },
            UpdateUser: {
                type: 'object',
                required: ['name', 'email'],
                properties: {
                    name: {
                        type: 'string',
                        description: 'Nombre del usuario',
                        example: 'John Doe Updated',
                    },
                    email: {
                        type: 'string',
                        description: 'Correo electrónico del usuario',
                        example: 'john.doe.updated@example.com',
                    },
                },
            },
            CreateAccessControl: {
                type: 'object',
                required: ['name', 'description'],
                properties: {
                    role_guid: {
                        type: 'UUID',
                        description: 'GUID del rol',
                    },
                    module_guid: {
                        type: 'UUID',
                        description: 'GUID del módulo',
                    },
                    permissions_guid: {
                        type: 'UUID',
                        description: 'GUID del permiso',
                    },
                    plan_guid: {
                        type: 'UUID',
                        description: 'GUID del plan',
                    },
                },
            },
            AccessControl: {
                type: 'object',
                properties: {
                    id: {
                        type: 'integer',
                        description: 'ID del control de acceso',
                    },
                    name: {
                        type: 'string',
                        description: 'Nombre del control de acceso',
                    },
                    description: {
                        type: 'string',
                        description: 'Descripción del control de acceso',
                    },
                },
            },
            UpdateAccessControl: {
                type: 'object',
                properties: {
                    name: {
                        type: 'string',
                        description: 'Nombre del control de acceso',
                    },
                    description: {
                        type: 'string',
                        description: 'Descripción del control de acceso',
                    },
                },
            },
            DeleteAccessControl: {
                type: 'object',
                properties: {
                    id: {
                        type: 'integer',
                        description: 'ID del control de acceso',
                    },
                },
            },
            DisableAccessControl: {
                type: 'object',
                properties: {
                    id: {
                        type: 'integer',
                        description: 'ID del control de acceso',
                    },
                },
            },
            EnableAccessControl: {
                type: 'object',
                properties: {
                    id: {
                        type: 'integer',
                        description: 'ID del control de acceso',
                    },
                },
            },
        },
        responses: {
            success: {
                description: 'Petición exitosa',
                content: {
                    'application/json': {
                        schema: {
                            type: 'object',
                            properties: {
                                code: {
                                    type: 'integer',
                                    example: 200,
                                },
                                success: {
                                    type: 'boolean',
                                    example: true,
                                },
                                message: {
                                    type: 'string',
                                    example: 'Petición exitosa',
                                },
                                data: {
                                    type: 'object',
                                },
                            },
                        },
                    },
                },
            },
            BadRequest: {
                description: 'Error en los datos proporcionados',
                content: {
                    'application/json': {
                        schema: {
                            type: 'object',
                            properties: {
                                code: {
                                    type: 'integer',
                                    example: 400,
                                },
                                success: {
                                    type: 'boolean',
                                    example: false,
                                },
                                message: {
                                    type: 'string',
                                    example:
                                        'Error en los datos proporcionados',
                                },
                                errors: {
                                    type: 'array',
                                    items: {
                                        type: 'object',
                                        properties: {
                                            param: {
                                                type: 'string',
                                                example: 'name',
                                            },
                                            msg: {
                                                type: 'string',
                                                example:
                                                    'El nombre es requerido.',
                                            },
                                        },
                                    },
                                },
                            },
                        },
                    },
                },
            },
            UnauthorizedError: {
                description: 'JWT token no válido o no proporcionado',
                content: {
                    'application/json': {
                        schema: {
                            type: 'object',
                            properties: {
                                code: {
                                    type: 'integer',
                                    example: 401,
                                },
                                success: {
                                    type: 'boolean',
                                    example: false,
                                },
                                message: {
                                    type: 'string',
                                    example: 'Unauthorized',
                                },
                            },
                        },
                    },
                },
            },
            NotFoundError: {
                description: 'Recurso no encontrado',
                content: {
                    'application/json': {
                        schema: {
                            type: 'object',
                            properties: {
                                code: {
                                    type: 'integer',
                                    example: 404,
                                },
                                success: {
                                    type: 'boolean',
                                    example: false,
                                },
                                message: {
                                    type: 'string',
                                    example: 'Recurso no encontrado',
                                },
                            },
                        },
                    },
                },
            },
            ConflictError: {
                description: 'Conflicto en los datos',
                content: {
                    'application/json': {
                        schema: {
                            type: 'object',
                            properties: {
                                code: {
                                    type: 'integer',
                                    example: 409,
                                },
                                success: {
                                    type: 'boolean',
                                    example: false,
                                },
                                message: {
                                    type: 'string',
                                    example: 'Conflicto en los datos',
                                },
                            },
                        },
                    },
                },
            },
            InternalServerError: {
                description: 'Error en el servidor',
                content: {
                    'application/json': {
                        schema: {
                            type: 'object',
                            properties: {
                                code: {
                                    type: 'integer',
                                    example: 500,
                                },
                                success: {
                                    type: 'boolean',
                                    example: false,
                                },
                                message: {
                                    type: 'string',
                                    example: 'Internal Server Error',
                                },
                                error: {
                                    type: 'object',
                                },
                            },
                        },
                    },
                },
            },
        },
    },
}

// Opciones de Swagger
const options = {
    swaggerDefinition,
    apis: [
        path.resolve(__dirname, '../routes/general/*.js'), // Ajusta esto según sea necesario
        path.resolve(__dirname, '../routes/tenant/*.js'), // Si tienes más rutas, agrégalas aquí
    ],
}

// Configuración de Swagger
const openApiConfiguration = swaggerJsdoc(options)

module.exports = openApiConfiguration
