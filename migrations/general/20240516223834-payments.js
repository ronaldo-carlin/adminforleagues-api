module.exports = {
    async up(queryInterface, Sequelize) {
        // entidad para registrar los pagos
        await queryInterface.createTable('payments', {
            // identificador del registro
            id: {
                type: Sequelize.INTEGER,
                primaryKey: true,
                autoIncrement: true,
                allowNull: false,
            },
            // identificador del pago
            guid: {
                type: Sequelize.UUID,
                defaultValue: Sequelize.UUIDV4, // Genera un UUID por defecto
                unique: true,
                allowNull: false,
            },
            // identificador del usuario que realizo el pago
            user_guid: {
                type: Sequelize.UUID,
            },
            // identificador del plan que se pago
            plan_guid: {
                type: Sequelize.UUID,
            },
            // correo electronico del usuario que realizo el pago
            email: {
                type: Sequelize.TEXT,
            },
            // monto del pago
            amount: {
                type: Sequelize.DECIMAL,
            },
            // metodo de pago
            method: {
                type: Sequelize.TEXT,
            },
            // identificador de la transaccion
            transaction: {
                type: Sequelize.TEXT,
            },
            // referencia de la transaccion
            reference: {
                type: Sequelize.TEXT,
            },
            // concepto del pago
            concept: {
                type: Sequelize.TEXT,
            },
            // tarjeta de pago
            card: {
                type: Sequelize.TEXT,
            },
            // banco de la tarjeta
            bank: {
                type: Sequelize.TEXT,
            },
            // mensaje de respuesta
            message: {
                type: Sequelize.TEXT,
            },
            // estado de la transaccion
            status: {
                type: Sequelize.TEXT,
            },
            // estado del registro
            is_active: {
                type: Sequelize.BOOLEAN,
                defaultValue: 1, // valor por defecto
            },
            // usuario que creo el registro
            created_by: {
                type: Sequelize.UUID,
            },
            // fecha de creacion
            created_at: {
                type: Sequelize.DATE,
                defaultValue: Sequelize.literal('CURRENT_TIMESTAMP'),
            },
            // usuario que actualizo el registro
            updated_by: {
                type: Sequelize.UUID,
            },
            // fecha de actualizacion
            updated_at: {
                type: Sequelize.DATE,
            },
            // usuario que elimino el registro
            deleted_by: {
                type: Sequelize.UUID,
            },
            // fecha de eliminacion
            deleted_at: {
                type: Sequelize.DATE,
            },
        })

        // Agregar índices después de crear la tabla
        await queryInterface.addIndex('payments', ['guid'], {
            unique: true,
            name: 'uniq_payments_guid',
        })

        await queryInterface.addIndex('payments', ['user_guid'], {
            name: 'idx_user_guid',
        })

        await queryInterface.addIndex('payments', ['plan_guid'], {
            name: 'idx_plan_guid',
        })

        // Agregar constraints después de crear la tabla
        await queryInterface.addConstraint('payments', {
            fields: ['user_guid'],
            type: 'foreign key',
            name: 'fk_payment_user',
            references: {
                table: 'users',
                field: 'guid',
            },
            onDelete: 'NO ACTION',
            onUpdate: 'NO ACTION',
        })

        await queryInterface.addConstraint('payments', {
            fields: ['plan_guid'],
            type: 'foreign key',
            name: 'fk_payment_plan',
            references: {
                table: 'plans',
                field: 'guid',
            },
            onDelete: 'NO ACTION',
            onUpdate: 'NO ACTION',
        })
    },

    async down(queryInterface, Sequelize) {
        await queryInterface.removeConstraint('payments', 'fk_payment_user')
        await queryInterface.removeConstraint('payments', 'fk_payment_plan')
        await queryInterface.removeIndex('payments', 'uniq_payments_guid')
        await queryInterface.removeIndex('payments', 'idx_user_guid')
        await queryInterface.removeIndex('payments', 'idx_plan_guid')
        await queryInterface.dropTable('payments')
    },
}
