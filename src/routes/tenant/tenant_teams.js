const express = require('express')
const router = express.Router()

// Controladores y validadores para teams
const {
    validatorCreateTeam,
    validatorTeam,
    validatorUpdateTeam,
    validatorDeleteTeam,
    validatorDisableTeam,
    validatorEnableTeam,
} = require('../../validators/tenant/tenant_teams')

const {
    CreateTeam,
    Teams,
    Team,
    UpdateTeam,
    DeleteTeam,
    DisableTeam,
    EnableTeam,
    TeamsAll,
    TeamsCount,
} = require('../../controllers/tenant/tenant_teams')

const authMiddleware = require('../../middleware/session_tenant')

/* Rutas protegidas que requieren autenticación */
/**
 * @swagger
 * tags:
 *   name: Tenant_Teams
 *   description: API para la gestión de equipos
 */

// Ruta para crear un nuevo equipo
/**
 * @swagger
 * /tenant_teams/CreateTeam:
 *   post:
 *     summary: Crear un nuevo equipo
 *     description: Esta ruta permite crear un nuevo equipo con los detalles especificados.
 *     tags: [Tenant_Teams]
 *     requestBody:
 *       required: true
 *       content:
 *         application/json:
 *           schema:
 *             type: object
 *             properties:
 *               name:
 *                 type: string
 *                 example: "Los Guerreros"
 *               description:
 *                 type: string
 *                 example: "Equipo de fútbol con una larga tradición."
 *     responses:
 *       201:
 *         description: Equipo creado exitosamente
 *         content:
 *           application/json:
 *             schema:
 *               $ref: '#/components/responses/success'
 *       400:
 *         description: Error en los datos enviados
 *         content:
 *           application/json:
 *             schema:
 *               $ref: '#/components/responses/BadRequest'
 *       403:
 *         description: Error de autenticación
 *         content:
 *           application/json:
 *             schema:
 *               $ref: '#/components/responses/UnauthorizedError'
 *       409:
 *         description: El equipo ya existe
 *         content:
 *           application/json:
 *             schema:
 *               $ref: '#/components/responses/ConflictError'
 *       500:
 *         description: Error en el servidor
 *         content:
 *           application/json:
 *             schema:
 *               $ref: '#/components/responses/InternalServerError'
 *     security:
 *        - bearerAuth: []
 */
router.post('/CreateTeam', authMiddleware, validatorCreateTeam(), CreateTeam)

// Ruta para obtener todos los equipos activos
/**
 * @swagger
 * /tenant_teams/Teams:
 *   get:
 *     summary: Obtener todos los equipos activos
 *     tags: [Tenant_Teams]
 *     responses:
 *       200:
 *         description: Equipos obtenidos exitosamente
 *         content:
 *           application/json:
 *             schema:
 *               $ref: '#/components/responses/success'
 *       403:
 *         description: Error de autenticación
 *         content:
 *           application/json:
 *             schema:
 *               $ref: '#/components/responses/UnauthorizedError'
 *       500:
 *         description: Error en el servidor
 *         content:
 *           application/json:
 *             schema:
 *               $ref: '#/components/responses/InternalServerError'
 *     security:
 *        - bearerAuth: []
 */
router.get('/Teams', authMiddleware, Teams)

// Ruta para obtener un equipo por GUID
/**
 * @swagger
 * /tenant_teams/Team/{guid}:
 *   get:
 *     summary: Obtener un equipo por GUID
 *     tags: [Tenant_Teams]
 *     parameters:
 *       - in: path
 *         name: guid
 *         schema:
 *           type: string
 *           format: uuid
 *         required: true
 *         description: El GUID del equipo
 *     responses:
 *       200:
 *         description: Equipo obtenido exitosamente
 *         content:
 *           application/json:
 *             schema:
 *               $ref: '#/components/responses/success'
 *       403:
 *         description: Error de autenticación
 *         content:
 *           application/json:
 *             schema:
 *               $ref: '#/components/responses/UnauthorizedError'
 *       404:
 *         description: Equipo no encontrado
 *         content:
 *           application/json:
 *             schema:
 *               $ref: '#/components/responses/NotFoundError'
 *       500:
 *         description: Error en el servidor
 *         content:
 *           application/json:
 *             schema:
 *               $ref: '#/components/responses/InternalServerError'
 *     security:
 *        - bearerAuth: []
 */
router.get('/Team/:guid', authMiddleware, validatorTeam(), Team)

// Ruta para actualizar un equipo
/**
 * @swagger
 * /tenant_teams/UpdateTeam/{guid}:
 *   patch:
 *     summary: Actualizar un equipo
 *     tags: [Tenant_Teams]
 *     requestBody:
 *       required: true
 *       content:
 *         application/json:
 *           schema:
 *             type: object
 *             properties:
 *               name:
 *                 type: string
 *                 example: "Los Guerreros"
 *               description:
 *                 type: string
 *                 example: "Equipo de fútbol con una larga tradición."
 *     responses:
 *       200:
 *         description: Equipo actualizado exitosamente
 *         content:
 *           application/json:
 *             schema:
 *               $ref: '#/components/responses/success'
 *       400:
 *         description: Error en los datos enviados
 *         content:
 *           application/json:
 *             schema:
 *               $ref: '#/components/responses/BadRequest'
 *       403:
 *         description: Error de autenticación
 *         content:
 *           application/json:
 *             schema:
 *               $ref: '#/components/responses/UnauthorizedError'
 *       404:
 *         description: Equipo no encontrado
 *         content:
 *           application/json:
 *             schema:
 *               $ref: '#/components/responses/NotFoundError'
 *       500:
 *         description: Error en el servidor
 *         content:
 *           application/json:
 *             schema:
 *               $ref: '#/components/responses/InternalServerError'
 *     security:
 *        - bearerAuth: []
 */
router.patch(
    '/UpdateTeam/:guid',
    authMiddleware,
    validatorUpdateTeam(),
    UpdateTeam
)

// Ruta para eliminar un equipo
/**
 * @swagger
 * /tenant_teams/DeleteTeam/{guid}:
 *   delete:
 *     summary: Eliminar un equipo
 *     tags: [Tenant_Teams]
 *     parameters:
 *       - in: path
 *         name: guid
 *         schema:
 *           type: string
 *           format: uuid
 *         required: true
 *         description: El GUID del equipo
 *     responses:
 *       200:
 *         description: Equipo eliminado exitosamente
 *         content:
 *           application/json:
 *             schema:
 *               $ref: '#/components/responses/success'
 *       400:
 *         description: Error en los datos enviados
 *         content:
 *           application/json:
 *             schema:
 *               $ref: '#/components/responses/BadRequest'
 *       403:
 *         description: Error de autenticación
 *         content:
 *           application/json:
 *             schema:
 *               $ref: '#/components/responses/UnauthorizedError'
 *       404:
 *         description: Equipo no encontrado
 *         content:
 *           application/json:
 *             schema:
 *               $ref: '#/components/responses/NotFoundError'
 *       500:
 *         description: Error en el servidor
 *         content:
 *           application/json:
 *             schema:
 *               $ref: '#/components/responses/InternalServerError'
 *     security:
 *        - bearerAuth: []
 */
router.delete(
    '/DeleteTeam/:guid',
    authMiddleware,
    validatorDeleteTeam(),
    DeleteTeam
)

// Ruta para deshabilitar un equipo
/**
 * @swagger
 * /tenant_teams/DisableTeam/{guid}:
 *   patch:
 *     summary: Deshabilitar un equipo
 *     tags: [Tenant_Teams]
 *     requestBody:
 *       required: true
 *       content:
 *         application/json:
 *           schema:
 *             type: object
 *             properties:
 *               guid:
 *                 type: uuid
 *                 example: "495db854-849b-41cd-b6b9-9c84824c92ba"
 *     responses:
 *       200:
 *         description: Equipo deshabilitado exitosamente
 *         content:
 *           application/json:
 *             schema:
 *               $ref: '#/components/responses/success'
 *       400:
 *         description: Error en los datos enviados
 *         content:
 *           application/json:
 *             schema:
 *               $ref: '#/components/responses/BadRequest'
 *       403:
 *         description: Error de autenticación
 *         content:
 *           application/json:
 *             schema:
 *               $ref: '#/components/responses/UnauthorizedError'
 *       404:
 *         description: Equipo no encontrado
 *         content:
 *           application/json:
 *             schema:
 *               $ref: '#/components/responses/NotFoundError'
 *       500:
 *         description: Error en el servidor
 *         content:
 *           application/json:
 *             schema:
 *               $ref: '#/components/responses/InternalServerError'
 *     security:
 *        - bearerAuth: []
 */
router.patch(
    '/DisableTeam/:guid',
    authMiddleware,
    validatorDisableTeam(),
    DisableTeam
)

// Ruta para habilitar un equipo
/**
 * @swagger
 * /tenant_teams/EnableTeam/{guid}:
 *   patch:
 *     summary: Habilitar un equipo
 *     tags: [Tenant_Teams]
 *     requestBody:
 *       required: true
 *       content:
 *         application/json:
 *           schema:
 *             type: object
 *             properties:
 *               guid:
 *                 type: uuid
 *                 example: "495db854-849b-41cd-b6b9-9c84824c92ba"
 *     responses:
 *       200:
 *         description: Equipo habilitado exitosamente
 *         content:
 *           application/json:
 *             schema:
 *               $ref: '#/components/responses/success'
 *       400:
 *         description: Error en los datos enviados
 *         content:
 *           application/json:
 *             schema:
 *               $ref: '#/components/responses/BadRequest'
 *       403:
 *         description: Error de autenticación
 *         content:
 *           application/json:
 *             schema:
 *               $ref: '#/components/responses/UnauthorizedError'
 *       404:
 *         description: Equipo no encontrado
 *         content:
 *           application/json:
 *             schema:
 *               $ref: '#/components/responses/NotFoundError'
 *       500:
 *         description: Error en el servidor
 *         content:
 *           application/json:
 *             schema:
 *               $ref: '#/components/responses/InternalServerError'
 *     security:
 *        - bearerAuth: []
 */
router.patch(
    '/EnableTeam/:guid',
    authMiddleware,
    validatorEnableTeam(),
    EnableTeam
)

// Ruta para obtener todos los equipos (activos e inactivos)
/**
 * @swagger
 * /tenant_teams/TeamsAll:
 *   get:
 *     summary: Obtener todos los equipos (activos e inactivos)
 *     tags: [Tenant_Teams]
 *     responses:
 *       200:
 *         description: Equipos obtenidos exitosamente
 *         content:
 *           application/json:
 *             schema:
 *               $ref: '#/components/responses/success'
 *       403:
 *         description: Error de autenticación
 *         content:
 *           application/json:
 *             schema:
 *               $ref: '#/components/responses/UnauthorizedError'
 *       500:
 *         description: Error en el servidor
 *         content:
 *           application/json:
 *             schema:
 *               $ref: '#/components/responses/InternalServerError'
 *     security:
 *        - bearerAuth: []
 */
router.get('/TeamsAll', authMiddleware, TeamsAll)

// Ruta para contar todos los equipos (activos e inactivos)
/**
 * @swagger
 * /tenant_teams/TeamsCount:
 *   get:
 *     summary: Contar todos los equipos (activos e inactivos)
 *     tags: [Tenant_Teams]
 *     responses:
 *       200:
 *         description: Conteo realizado exitosamente
 *         content:
 *           application/json:
 *             schema:
 *               $ref: '#/components/responses/success'
 *       403:
 *         description: Error de autenticación
 *         content:
 *           application/json:
 *             schema:
 *               $ref: '#/components/responses/UnauthorizedError'
 *       500:
 *         description: Error en el servidor
 *         content:
 *           application/json:
 *             schema:
 *               $ref: '#/components/responses/InternalServerError'
 *     security:
 *        - bearerAuth: []
 */
router.get('/TeamsCount', authMiddleware, TeamsCount)

module.exports = router
