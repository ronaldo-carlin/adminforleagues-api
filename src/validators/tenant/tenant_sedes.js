const { check, validationResult } = require('express-validator')

// Validación para crear una nueva sede
const validatorCreateSede = () => {
    return [
        async (req, res, next) => {
            await check('name')
                .notEmpty()
                .withMessage('El nombre de la sede es requerido.')
                .run(req)
            await check('description')
                .optional()
                .isString()
                .withMessage('La descripción debe ser una cadena de texto.')
                .run(req)
            await check('schedule')
                .optional()
                .isString()
                .withMessage('El horario debe ser una cadena de texto.')
                .run(req)
            await check('address')
                .optional()
                .isString()
                .withMessage('La dirección debe ser una cadena de texto.')
                .run(req)
            await check('city')
                .optional()
                .isString()
                .withMessage('La ciudad debe ser una cadena de texto.')
                .run(req)
            await check('state')
                .optional()
                .isString()
                .withMessage('El estado debe ser una cadena de texto.')
                .run(req)
            await check('country')
                .optional()
                .isString()
                .withMessage('El país debe ser una cadena de texto.')
                .run(req)
            await check('capacity')
                .optional()
                .isInt()
                .withMessage('La capacidad debe ser un número entero.')
                .run(req)
            await check('latitude')
                .optional()
                .isString()
                .withMessage('La latitud debe ser una cadena de texto.')
                .run(req)
            await check('longitude')
                .optional()
                .isString()
                .withMessage('La longitud debe ser una cadena de texto.')
                .run(req)
            await check('image_path')
                .optional()
                .isString()
                .withMessage(
                    'La ruta de la imagen debe ser una cadena de texto.'
                )
                .run(req)

            const errors = validationResult(req)
            if (!errors.isEmpty()) {
                return res.status(400).json({ errors: errors.array() })
            }

            next()
        },
    ]
}

// Validación para obtener una sede por GUID
const validatorSede = () => {
    return [
        async (req, res, next) => {
            await check('guid')
                .isUUID()
                .withMessage('El GUID de la sede es requerido.')
                .run(req)

            const errors = validationResult(req)
            if (!errors.isEmpty()) {
                return res.status(400).json({ errors: errors.array() })
            }

            next()
        },
    ]
}

// Validación para actualizar una sede
const validatorUpdateSede = () => {
    return [
        async (req, res, next) => {
            await check('guid')
                .isUUID()
                .withMessage('El GUID de la sede es requerido.')
                .run(req)
            await check('name')
                .optional()
                .notEmpty()
                .withMessage('El nombre de la sede es requerido.')
                .run(req)
            await check('description')
                .optional()
                .isString()
                .withMessage('La descripción debe ser una cadena de texto.')
                .run(req)
            await check('schedule')
                .optional()
                .isString()
                .withMessage('El horario debe ser una cadena de texto.')
                .run(req)
            await check('address')
                .optional()
                .isString()
                .withMessage('La dirección debe ser una cadena de texto.')
                .run(req)
            await check('city')
                .optional()
                .isString()
                .withMessage('La ciudad debe ser una cadena de texto.')
                .run(req)
            await check('state')
                .optional()
                .isString()
                .withMessage('El estado debe ser una cadena de texto.')
                .run(req)
            await check('country')
                .optional()
                .isString()
                .withMessage('El país debe ser una cadena de texto.')
                .run(req)
            await check('capacity')
                .optional()
                .isInt()
                .withMessage('La capacidad debe ser un número entero.')
                .run(req)
            await check('latitude')
                .optional()
                .isString()
                .withMessage('La latitud debe ser una cadena de texto.')
                .run(req)
            await check('longitude')
                .optional()
                .isString()
                .withMessage('La longitud debe ser una cadena de texto.')
                .run(req)
            await check('image_path')
                .optional()
                .isString()
                .withMessage(
                    'La ruta de la imagen debe ser una cadena de texto.'
                )
                .run(req)

            const errors = validationResult(req)
            if (!errors.isEmpty()) {
                return res.status(400).json({ errors: errors.array() })
            }

            next()
        },
    ]
}

// Validación para eliminar una sede
const validatorDeleteSede = () => {
    return [
        async (req, res, next) => {
            await check('guid')
                .isUUID()
                .withMessage('El GUID de la sede es requerido.')
                .run(req)

            const errors = validationResult(req)
            if (!errors.isEmpty()) {
                return res.status(400).json({ errors: errors.array() })
            }

            next()
        },
    ]
}

// Validación para deshabilitar una sede
const validatorDisableSede = () => {
    return [
        async (req, res, next) => {
            await check('guid')
                .isUUID()
                .withMessage('El GUID de la sede es requerido.')
                .run(req)

            const errors = validationResult(req)
            if (!errors.isEmpty()) {
                return res.status(400).json({ errors: errors.array() })
            }

            next()
        },
    ]
}

// Validación para habilitar una sede
const validatorEnableSede = () => {
    return [
        async (req, res, next) => {
            await check('guid')
                .isUUID()
                .withMessage('El GUID de la sede es requerido.')
                .run(req)

            const errors = validationResult(req)
            if (!errors.isEmpty()) {
                return res.status(400).json({ errors: errors.array() })
            }

            next()
        },
    ]
}

module.exports = {
    validatorCreateSede,
    validatorSede,
    validatorUpdateSede,
    validatorDeleteSede,
    validatorDisableSede,
    validatorEnableSede,
}
