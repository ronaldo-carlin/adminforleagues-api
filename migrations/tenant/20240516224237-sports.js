module.exports = {
    async up(queryInterface, Sequelize) {
        await queryInterface.createTable('sports', {
            id: {
                type: Sequelize.INTEGER,
                autoIncrement: true,
                primaryKey: true,
                allowNull: false,
            },
            guid: {
                type: Sequelize.UUID,
                unique: true,
                allowNull: true,
            },
            name: {
                type: Sequelize.STRING(100),
                allowNull: true,
            },
            description: {
                type: Sequelize.TEXT,
                allowNull: true,
            },
            icon: {
                type: Sequelize.STRING(255),
                allowNull: true,
            },
            is_active: {
                type: Sequelize.BOOLEAN,
                defaultValue: 1, // valor por defecto
            },
            created_by: {
                type: Sequelize.UUID,
                allowNull: true,
            },
            created_at: {
                type: Sequelize.DATE,
                allowNull: true,
                defaultValue: Sequelize.literal('CURRENT_TIMESTAMP'),
            },
            updated_by: {
                type: Sequelize.UUID,
                allowNull: true,
            },
            updated_at: {
                type: Sequelize.DATE,
                allowNull: true,
                defaultValue: Sequelize.literal('CURRENT_TIMESTAMP'),
            },
            deleted_by: {
                type: Sequelize.UUID,
                allowNull: true,
            },
            deleted_at: {
                type: Sequelize.DATE,
                allowNull: true,
                defaultValue: Sequelize.literal('CURRENT_TIMESTAMP'),
            },
        })

        await queryInterface.addIndex('sports', ['guid'], {
            unique: true,
            name: 'uniq_sports_guid',
        })
    },

    async down(queryInterface, Sequelize) {
        await queryInterface.removeIndex('sports', 'uniq_sports_guid')
        await queryInterface.dropTable('sports')
    },
}
