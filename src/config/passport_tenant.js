const passport = require('passport')
const GoogleStrategy = require('passport-google-oauth20').Strategy
const User = require('../models/tenant/tenant_auth') // Asegúrate de que la ruta a tu modelo de usuario es correcta

passport.use(
    new GoogleStrategy(
        {
            clientID: process.env.GOOGLE_CLIENT_ID,
            clientSecret: process.env.GOOGLE_CLIENT_SECRET,
            callbackURL: '/auth/google/callback',
        },
        function (accessToken, refreshToken, profile, cb) {
            // Tu lógica para encontrar o crear el usuario va aquí
            // Por ejemplo, si estás usando MongoDB, podrías buscar o crear el usuario así:
            User.findOne({ googleId: profile.id }, function (err, user) {
                if (!user) {
                    // Crear un nuevo usuario si no existe
                    user = new User({ googleId: profile.id })
                    user.save(function (err) {
                        if (err) console.log(err)
                        return cb(err, user)
                    })
                } else {
                    // Usuario encontrado
                    return cb(err, user)
                }
            })
        }
    )
)

passport.serializeUser(function (user, done) {
    done(null, user.id)
})

passport.deserializeUser(function (id, done) {
    User.findById(id, function (err, user) {
        done(err, user)
    })
})

module.exports = passport
