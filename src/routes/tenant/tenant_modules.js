const express = require('express')
const router = express.Router()

// Controladores de módulos y middleware
const { Modules } = require('../../controllers/tenant/tenant_modules')
const authMiddleware = require('../../middleware/session_tenant')

/* Rutas protegidas que requieren autenticación */
/**
 * @swagger
 * tags:
 *   name: Tenant_Modules
 *   description: API para la gestión de módulos de los usuarios
 */

/**
 * @swagger
 * /tenant_modules/Modules:
 *  get:
 *    summary: Obtener los módulos de un usuario
 *    tags: [Tenant_Modules]
 *    description: Obtiene los módulos activos de un usuario
 *    responses:
 *      200:
 *        description: Módulos obtenidos exitosamente
 *        content:
 *          application/json:
 *            schema:
 *              $ref: '#/components/responses/success'
 *      403:
 *        description: Error de autenticación
 *        content:
 *          application/json:
 *            schema:
 *              $ref: '#/components/responses/UnauthorizedError'
 *      500:
 *        description: Error en el servidor
 *        content:
 *          application/json:
 *            schema:
 *              $ref: '#/components/responses/InternalServerError'
 *    security:
 *      - bearerAuth: []
 */
router.get('/Modules', authMiddleware, Modules)

module.exports = router
