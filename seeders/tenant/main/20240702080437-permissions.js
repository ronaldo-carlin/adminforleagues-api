const GUIDsTenant = require('../../guids_tenant')

module.exports = {
    async up(queryInterface, Sequelize) {
        // Inserta los permisos por defecto
        await queryInterface.bulkInsert(
            'permissions',
            [
                {
                    guid: GUIDsTenant.PermissionCreateGuid,
                    name: 'create',
                    description: 'Permiso para crear registros',
                    is_active: 1, // 1: Activo, 0: Inactivo
                    created_at: new Date(),
                },
                {
                    guid: GUIDsTenant.PermissionReadGuid,
                    name: 'read',
                    description: 'Permiso para leer registros',
                    is_active: 1, // 1: Activo, 0: Inactivo
                    created_at: new Date(),
                },
                {
                    guid: GUIDsTenant.PermissionUpdateGuid,
                    name: 'update',
                    description: 'Permiso para actualizar registros',
                    is_active: 1, // 1: Activo, 0: Inactivo
                    created_at: new Date(),
                },
                {
                    guid: GUIDsTenant.PermissionDeleteGuid,
                    name: 'delete',
                    description: 'Permiso para eliminar registros',
                    is_active: 1, // 1: Activo, 0: Inactivo
                    created_at: new Date(),
                },
                {
                    guid: GUIDsTenant.PermissionDisableGuid,
                    name: 'disable',
                    description: 'Permiso para deshabilitar registros',
                    is_active: 1, // 1: Activo, 0: Inactivo
                    created_at: new Date(),
                },
                {
                    guid: GUIDsTenant.PermissionEnableCreateGuid,
                    name: 'enable',
                    description:
                        'Permiso para habilitar la creación de registros',
                    is_active: 1, // 1: Activo, 0: Inactivo
                    created_at: new Date(),
                },
                {
                    guid: GUIDsTenant.PermissionExportGuid,
                    name: 'export',
                    description: 'Permiso para exportar registros',
                    is_active: 1, // 1: Activo, 0: Inactivo
                    created_at: new Date(),
                },
                {
                    guid: GUIDsTenant.PermissionImportGuid,
                    name: 'import',
                    description: 'Permiso para importar registros',
                    is_active: 1, // 1: Activo, 0: Inactivo
                    created_at: new Date(),
                },
                {
                    guid: GUIDsTenant.PermissionFullAccessGuid,
                    name: 'Full Access',
                    description:
                        'Acceso completo a todos los módulos y permisos',
                    is_active: 1, // 1: Activo, 0: Inactivo
                    created_at: new Date(),
                },
                {
                    guid: GUIDsTenant.PermissionManageContentGuid,
                    name: 'Manage Content',
                    description:
                        'Permiso para crear, actualizar y eliminar contenido',
                    is_active: 1, // 1: Activo, 0: Inactivo
                    created_at: new Date(),
                },
                {
                    guid: GUIDsTenant.PermissionStatusManagementGuid,
                    name: 'Status Management',
                    description:
                        'Permiso para habilitar y deshabilitar registros',
                    is_active: 1, // 1: Activo, 0: Inactivo
                    created_at: new Date(),
                },
            ],
            {}
        )
    },

    async down(queryInterface, Sequelize) {
        await queryInterface.bulkDelete('permissions', null, {})
    },
}
