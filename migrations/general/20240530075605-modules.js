module.exports = {
    async up(queryInterface, Sequelize) {
        // entidad de modulos
        await queryInterface.createTable('modules', {
            // identificador del registro
            id: {
                type: Sequelize.INTEGER,
                primaryKey: true,
                autoIncrement: true,
                allowNull: false,
            },
            // identificador guia del registro
            guid: {
                type: Sequelize.UUID,
                defaultValue: Sequelize.UUIDV4, // Genera un UUID por defecto
                unique: true,
                allowNull: false,
            },
            // nombre del modulo
            name: {
                type: Sequelize.TEXT,
            },
            // descripcion del modulo
            description: {
                type: Sequelize.TEXT,
            },
            // ubicacion del modulo
            to: {
                type: Sequelize.TEXT,
            },
            // icono del modulo
            icon: {
                type: Sequelize.TEXT,
            },
            // opcion para saber si es un modulo padre o no
            is_parent: {
                type: Sequelize.BOOLEAN,
                defaultValue: 0, // valor por defecto
            },
            // identificador del modulo padre
            parent_guid: {
                type: Sequelize.UUID,
            },
            // estado del registro
            is_active: {
                type: Sequelize.BOOLEAN,
                defaultValue: 1, // valor por defecto
            },
            // usuario que creo el registro
            created_by: {
                type: Sequelize.UUID,
            },
            // fecha de creacion
            created_at: {
                type: Sequelize.DATE,
                defaultValue: Sequelize.literal('CURRENT_TIMESTAMP'),
            },
            // usuario que actualizo el registro
            updated_by: {
                type: Sequelize.UUID,
            },
            // fecha de actualizacion
            updated_at: {
                type: Sequelize.DATE,
            },
            // usuario que elimino el registro
            deleted_by: {
                type: Sequelize.UUID,
            },
            // fecha de eliminacion
            deleted_at: {
                type: Sequelize.DATE,
            },
        })

        // Agregar índices después de crear la tabla
        await queryInterface.addIndex('modules', ['guid'], {
            unique: true,
            name: 'uniq_modules_guid',
        })
    },

    async down(queryInterface, Sequelize) {
        await queryInterface.dropTable('modules')
    },
}
