module.exports = {
    async up(queryInterface, Sequelize) {
        await queryInterface.createTable('tokens', {
            id: {
                type: Sequelize.INTEGER,
                primaryKey: true,
                autoIncrement: true,
                allowNull: false,
            },
            // Identificador unico del registro
            guid: {
                type: Sequelize.UUID,
                defaultValue: Sequelize.UUIDV4,
                unique: true,
                allowNull: false,
            },
            // el id del usuario que genero el token
            user_guid: {
                type: Sequelize.UUID,
            },
            // el refresh token que se envia al cliente para que lo guarde en el local storage
            refresh_token: {
                type: Sequelize.STRING,
            },
            // la fecha de expiracion del token
            expiry_date: {
                type: Sequelize.DATE,
            },
            // la fecha de creacion del refresh token
            created_at: {
                type: Sequelize.DATE,
                defaultValue: Sequelize.literal('CURRENT_TIMESTAMP'),
            },
            // indicador booleano que indica si el token fue revocado o no
            revoket: {
                type: Sequelize.BOOLEAN,
                defaultValue: false,
            },
            // la fecha en que se revoco el token
            revoked_at: {
                type: Sequelize.DATE,
                defaultValue: Sequelize.literal('CURRENT_TIMESTAMP'),
            },
            // el tipo de dispositivo que solicito el token
            device_info: {
                type: Sequelize.TEXT,
            },
        })

        // Add indexes
        await queryInterface.addIndex('tokens', ['guid'], {
            unique: true,
            name: 'uniq_tokens_guid',
        })

        await queryInterface.addIndex('tokens', ['user_guid'], {
            name: 'idx_tokens_user_guid',
        })

        await queryInterface.addConstraint('tokens', {
            fields: ['user_guid'],
            type: 'foreign key',
            name: 'fk_tokens_user_guid',
            references: {
                table: 'users',
                field: 'guid',
            },
            onDelete: 'NO ACTION',
            onUpdate: 'NO ACTION',
        })
    },

    async down(queryInterface, Sequelize) {
        await queryInterface.removeConstraint('tokens', 'fk_tokens_user_guid')
        await queryInterface.removeIndex('tokens', 'uniq_tokens_guid')
        await queryInterface.removeIndex('tokens', 'idx_tokens_user_guid')
        await queryInterface.dropTable('tokens')
    },
}
