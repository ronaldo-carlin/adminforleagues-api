// Importar Sequelize
const { DataTypes } = require('sequelize')

// Definir la función factory para el modelo NewsTenantModel
const NewsTenantModelFactory = (sequelize) => {
    const NewsTenantModel = sequelize.define(
        'news',
        {
            id: {
                type: DataTypes.INTEGER,
                primaryKey: true,
                autoIncrement: true,
            },
            guid: {
                type: DataTypes.UUID,
                primaryKey: true,
                defaultValue: DataTypes.UUIDV4,
                unique: true,
            },
            title: {
                type: DataTypes.TEXT,
            },
            description: {
                type: DataTypes.TEXT,
            },
            type_new_guid: {
                type: DataTypes.TEXT,
            },
            publication_date: {
                type: DataTypes.DATE,
            },
            expiration_date: {
                type: DataTypes.DATE,
            },
            cover: {
                type: DataTypes.TEXT,
            },
            image_path: {
                type: DataTypes.TEXT,
            },
            video_path: {
                type: DataTypes.TEXT,
            },
            link: {
                type: DataTypes.TEXT,
            },
            is_active: {
                type: DataTypes.INTEGER,
                defaultValue: 1,
            },
            created_by: {
                type: DataTypes.UUID,
            },
            updated_by: {
                type: DataTypes.UUID,
            },
            deleted_by: {
                type: DataTypes.UUID,
            },
            created_at: {
                type: DataTypes.DATE,
            },
            updated_at: {
                type: DataTypes.DATE,
            },
            deleted_at: {
                type: DataTypes.DATE,
            },
        },
        {
            tableName: 'news',
            timestamps: false,
        }
    )

    return NewsTenantModel
}

module.exports = NewsTenantModelFactory
