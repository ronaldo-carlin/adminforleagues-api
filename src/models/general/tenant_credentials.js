// Importar Sequelize directamente
const { Sequelize } = require('sequelize')
// Importar tu configuración de sequelize
const { sequelize } = require('../../database/mysql')
// Importar el modelo de tenant
const TenantModel = require('./tenants')

const TenantCredentialsModel = sequelize.define(
    'tenant_credentials',
    {
        id: {
            type: Sequelize.INTEGER,
            primaryKey: true,
            autoIncrement: true,
        },
        guid: {
            type: Sequelize.UUID,
            defaultValue: Sequelize.UUIDV4,
            unique: true,
        },
        tenant_guid: {
            type: Sequelize.UUID,
        },
        domain: {
            type: Sequelize.TEXT,
        },
        subdomain: {
            type: Sequelize.TEXT,
        },
        database: {
            type: Sequelize.TEXT,
        },
        user: {
            type: Sequelize.TEXT,
        },
        password: {
            type: Sequelize.TEXT,
        },
        host: {
            type: Sequelize.TEXT,
        },
        port: {
            type: Sequelize.INTEGER,
        },
        is_active: {
            type: Sequelize.INTEGER,
        },
        created_by: {
            type: Sequelize.UUID,
        },
        updated_by: {
            type: Sequelize.UUID,
        },
        deleted_by: {
            type: Sequelize.UUID,
        },
        created_at: {
            type: Sequelize.DATE,
        },
        updated_at: {
            type: Sequelize.DATE,
        },
        deleted_at: {
            type: Sequelize.DATE,
        },
    },
    {
        tableName: 'tenant_credentials',
        timestamps: false,
        defaultScope: {
            attributes: {
                exclude: [
                    'created_by',
                    'updated_by',
                    'deleted_by',
                    'created_at',
                    'updated_at',
                    'deleted_at',
                ],
            },
            include: [
                {
                    model: TenantModel,
                    as: 'tenant',
                    required: true,
                    where: {
                        is_active: true,
                    },
                    exclude: [
                        'created_by',
                        'updated_by',
                        'deleted_by',
                        'created_at',
                        'updated_at',
                        'deleted_at',
                    ],
                },
            ],
        },
    }
)

TenantCredentialsModel.belongsTo(TenantModel, {
    foreignKey: 'tenant_guid',
    targetKey: 'guid',
    as: 'tenant',
})

module.exports = TenantCredentialsModel
