const GUIDsGeneral = require('../guids_general')

module.exports = {
    async up(queryInterface, Sequelize) {
        // Inserta un usuario por defecto
        await queryInterface.bulkInsert(
            'tenants',
            [
                {
                    guid: GUIDsGeneral.tenantSuperAdminGuid,
                    company_name: 'Super Tenant',
                    email: 'tenant@adminforleagues.com',
                    phone: '1234567890',
                    country: 'Colombia',
                    city: 'Bogotá',
                    address: 'Calle 123 # 45-67',
                    is_active: 1, // 1: Activo, 0: Inactivo
                    created_at: new Date(),
                },
            ],
            {}
        )
    },

    async down(queryInterface, Sequelize) {
        await queryInterface.bulkDelete('tenants', null, {})
    },
}
