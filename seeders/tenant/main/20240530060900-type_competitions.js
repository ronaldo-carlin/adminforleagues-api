const GUIDsTenant = require('../../guids_tenant')

module.exports = {
    async up(queryInterface, Sequelize) {
        await queryInterface.bulkInsert('types_competitions', [
            {
                guid: GUIDsTenant.typesCompetitionsLigaGuid,
                sport_guid: GUIDsTenant.futbolGuid,
                name: 'Liga',
                description:
                    'Los equipos compiten entre sí en varias rondas, acumulando puntos para determinar el ganador final. Todos los equipos juegan contra todos',
                icon: 'MdFormatListNumbered',
                is_active: 1,
                created_at: new Date(),
            },
            {
                guid: GUIDsTenant.typesCompetitionsCopaGuid,
                sport_guid: GUIDsTenant.futbolGuid,
                name: 'Eliminacion Directa',
                description:
                    'Los equipos se enfrentan en rondas eliminatorias. El perdedor de cada enfrentamiento queda fuera del torneo, y el ganador avanza hasta la final.',
                icon: 'TbTournament',
                is_active: 1,
                created_at: new Date(),
            },
            {
                guid: GUIDsTenant.typesCompetitionsTorneoGuid,
                sport_guid: GUIDsTenant.futbolGuid,
                name: 'Fase de Grupos',
                description:
                    'Los equipos se dividen en grupos y juegan entre sí. Los mejores equipos de cada grupo avanzan a la siguiente fase, generalmente de eliminación directa.',
                icon: 'TfiLayoutGrid2',
                is_active: 1,
                created_at: new Date(),
            },
            {
                guid: GUIDsTenant.typesCompetitionsEliminacionDirectaGuid,
                sport_guid: GUIDsTenant.futbolGuid,
                name: 'Torneo Relampago',
                description:
                    'Un torneo acelerado donde los equipos se eliminan en rondas rápidas, con partidos que se juegan en un corto período de tiempo.',
                icon: 'BsFillLightningChargeFill',
                is_active: 1,
                created_at: new Date(),
            },
        ])
    },

    async down(queryInterface, Sequelize) {
        await queryInterface.bulkDelete('types_competitions', null, {})
    },
}
