const { check, validationResult } = require('express-validator')

// Validación para crear un nuevo deporte
const validatorCreateSport = () => {
    return [
        async (req, res, next) => {
            await check('name')
                .notEmpty()
                .withMessage('El nombre del deporte es requerido.')
                .run(req)
            await check('description')
                .optional()
                .isString()
                .withMessage('La descripción debe ser una cadena de texto.')
                .run(req)
            await check('icon')
                .optional()
                .isString()
                .withMessage('El icono debe ser una cadena de texto.')
                .run(req)

            const errors = validationResult(req)
            if (!errors.isEmpty()) {
                return res.status(400).json({ errors: errors.array() })
            }

            next()
        },
    ]
}

// Validación para obtener un deporte por GUID
const validatorSport = () => {
    return [
        async (req, res, next) => {
            await check('guid')
                .isUUID()
                .withMessage('El guid del deporte es requerido.')
                .run(req)

            const errors = validationResult(req)
            if (!errors.isEmpty()) {
                return res.status(400).json({ errors: errors.array() })
            }

            next()
        },
    ]
}

// Validación para actualizar un deporte
const validatorUpdateSport = () => {
    return [
        async (req, res, next) => {
            await check('guid')
                .isUUID()
                .withMessage('El guid del deporte es requerido.')
                .run(req)
            await check('name')
                .optional()
                .notEmpty()
                .withMessage('El nombre del deporte es requerido.')
                .run(req)
            await check('description')
                .optional()
                .isString()
                .withMessage('La descripción debe ser una cadena de texto.')
                .run(req)
            await check('icon')
                .optional()
                .isString()
                .withMessage('El icono debe ser una cadena de texto.')
                .run(req)

            const errors = validationResult(req)
            if (!errors.isEmpty()) {
                return res.status(400).json({ errors: errors.array() })
            }

            next()
        },
    ]
}

// Validación para eliminar un deporte
const validatorDeleteSport = () => {
    return [
        async (req, res, next) => {
            await check('guid')
                .isUUID()
                .withMessage('El guid del deporte es requerido.')
                .run(req)

            const errors = validationResult(req)
            if (!errors.isEmpty()) {
                return res.status(400).json({ errors: errors.array() })
            }

            next()
        },
    ]
}

// Validación para deshabilitar un deporte
const validatorDisableSport = () => {
    return [
        async (req, res, next) => {
            await check('guid')
                .isUUID()
                .withMessage('El guid del deporte es requerido.')
                .run(req)

            const errors = validationResult(req)
            if (!errors.isEmpty()) {
                return res.status(400).json({ errors: errors.array() })
            }

            next()
        },
    ]
}

// Validación para habilitar un deporte
const validatorEnableSport = () => {
    return [
        async (req, res, next) => {
            await check('guid')
                .isUUID()
                .withMessage('El guid del deporte es requerido.')
                .run(req)

            const errors = validationResult(req)
            if (!errors.isEmpty()) {
                return res.status(400).json({ errors: errors.array() })
            }

            next()
        },
    ]
}

module.exports = {
    validatorCreateSport,
    validatorSport,
    validatorUpdateSport,
    validatorDeleteSport,
    validatorDisableSport,
    validatorEnableSport,
}
