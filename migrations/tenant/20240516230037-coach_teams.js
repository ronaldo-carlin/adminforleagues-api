module.exports = {
    async up(queryInterface, Sequelize) {
        // entidad para los equipos de los entrenadores
        await queryInterface.createTable('coach_teams', {
            // identificador de la tabla
            id: {
                type: Sequelize.INTEGER,
                primaryKey: true,
                autoIncrement: true,
                allowNull: false,
            },
            // identificador unico de la tabla
            guid: {
                type: Sequelize.UUID,
                defaultValue: Sequelize.UUIDV4,
                unique: true,
                allowNull: false,
            },
            // identificador del entrenador
            user_guid: {
                type: Sequelize.UUID,
            },
            // identificador del equipo
            team_guid: {
                type: Sequelize.UUID,
            },
            // si el entrenador es el dueño del equipo
            is_owner: {
                type: Sequelize.INTEGER,
            },
            // estado del registro
            is_active: {
                type: Sequelize.BOOLEAN,
                defaultValue: 1, // valor por defecto
            },
            // usuario que creo el registro
            created_by: {
                type: Sequelize.UUID,
            },
            // fecha de creacion
            created_at: {
                type: Sequelize.DATE,
                defaultValue: Sequelize.literal('CURRENT_TIMESTAMP'),
            },
            // usuario que actualizo el registro
            updated_by: {
                type: Sequelize.UUID,
            },
            // fecha de actualizacion
            updated_at: {
                type: Sequelize.DATE,
            },
            // usuario que elimino el registro
            deleted_by: {
                type: Sequelize.UUID,
            },
            // fecha de eliminacion
            deleted_at: {
                type: Sequelize.DATE,
            },
        })

        // indeces
        await queryInterface.addIndex('coach_teams', ['guid'], {
            unique: true,
            name: 'unique_coach_teams_guid',
        })

        await queryInterface.addIndex('coach_teams', ['user_guid'], {
            name: 'idx_coach_teams_user_guid',
        })

        await queryInterface.addIndex('coach_teams', ['team_guid'], {
            name: 'idx_coach_teams_team_guid',
        })

        // llaves foraneas
        await queryInterface.addConstraint('coach_teams', {
            fields: ['user_guid'],
            type: 'foreign key',
            name: 'fk_coach_teams_user_guid',
            references: {
                table: 'users',
                field: 'guid',
            },
            onDelete: 'NO ACTION',
            onUpdate: 'CASCADE',
        })

        await queryInterface.addConstraint('coach_teams', {
            fields: ['team_guid'],
            type: 'foreign key',
            name: 'fk_coach_teams_team_guid',
            references: {
                table: 'teams',
                field: 'guid',
            },
            onDelete: 'NO ACTION',
            onUpdate: 'CASCADE',
        })
    },

    async down(queryInterface, Sequelize) {
        await queryInterface.removeConstraint(
            'coach_teams',
            'fk_coach_teams_user_guid'
        )
        await queryInterface.removeConstraint(
            'coach_teams',
            'fk_coach_teams_team_guid'
        )
        await queryInterface.removeIndex(
            'coach_teams',
            'idx_coach_teams_team_guid'
        )
        await queryInterface.removeIndex(
            'coach_teams',
            'idx_coach_teams_user_guid'
        )
        await queryInterface.removeIndex(
            'coach_teams',
            'unique_coach_teams_guid'
        )
        await queryInterface.dropTable('coach_teams')
    },
}
