module.exports = {
    async up(queryInterface, Sequelize) {
        // entidad para almacenar las ordenes de compra
        await queryInterface.createTable('orders', {
            // id del registro
            id: {
                type: Sequelize.INTEGER,
                primaryKey: true,
                autoIncrement: true,
                allowNull: false,
            },
            guid: {
                type: Sequelize.UUID,
                defaultValue: Sequelize.UUIDV4,
                unique: true,
                allowNull: false,
            },
            // identificador del usuario que realizo la compra
            user_guid: {
                type: Sequelize.UUID,
            },
            // identificador del payment_types comprado
            payment_type_guid: {
                type: Sequelize.UUID,
            },
            // cupon de descuento
            coupon_guid: {
                type: Sequelize.UUID,
            },
            // transaccion de pago
            transaction: {
                type: Sequelize.TEXT,
            },
            // metodo de pago
            payment_method: {
                type: Sequelize.TEXT,
            },
            // monto de la transaccion
            amount: {
                type: Sequelize.DECIMAL,
            },
            // moneda de la transaccion
            currency: {
                type: Sequelize.TEXT,
            },
            // icono de la moneda
            currency_icon: {
                type: Sequelize.TEXT,
            },
            // fecha de inicio de la orden
            enroll_start: {
                type: Sequelize.DATE,
            },
            // fecha de finalizacion de la orden
            enroll_end: {
                type: Sequelize.DATE,
            },
            // fecha de expiracion de la orden
            expiration_date: {
                type: Sequelize.DATE,
            },
            // fecha de renovacion de la orden
            renewal_date: {
                type: Sequelize.DATE,
            },
            // fecha de cancelacion de la orden
            cancel_date: {
                type: Sequelize.DATE,
            },
            // tipo de cancelacion
            cancel_type: {
                type: Sequelize.TEXT,
            },
            // estado de la orden
            status: {
                type: Sequelize.TEXT,
            },
            // estado del registro
            is_active: {
                type: Sequelize.BOOLEAN,
                defaultValue: 1, // valor por defecto
            },
            // usuario que creo el registro
            created_by: {
                type: Sequelize.UUID,
            },
            // fecha de creacion
            created_at: {
                type: Sequelize.DATE,
                defaultValue: Sequelize.literal('CURRENT_TIMESTAMP'),
            },
            // usuario que actualizo el registro
            updated_by: {
                type: Sequelize.UUID,
            },
            // fecha de actualizacion
            updated_at: {
                type: Sequelize.DATE,
            },
            // usuario que elimino el registro
            deleted_by: {
                type: Sequelize.UUID,
            },
            // fecha de eliminacion
            deleted_at: {
                type: Sequelize.DATE,
            },
        })

        // indices
        await queryInterface.addIndex('orders', ['guid'], {
            unique: true,
            name: 'unique_orders_guid',
        })

        await queryInterface.addIndex('orders', ['user_guid'], {
            name: 'idx_orders_user_guid',
        })

        await queryInterface.addIndex('orders', ['payment_type_guid'], {
            name: 'idx_orders_payment_type_guid',
        })

        await queryInterface.addIndex('orders', ['coupon_guid'], {
            name: 'idx_orders_coupon_guid',
        })

        // llave foraneas
        await queryInterface.addConstraint('orders', {
            fields: ['user_guid'],
            type: 'foreign key',
            name: 'fk_orders_user_guid',
            references: {
                table: 'users',
                field: 'guid',
            },
            onUpdate: 'CASCADE',
            onDelete: 'NO ACTION',
        })

        await queryInterface.addConstraint('orders', {
            fields: ['payment_type_guid'],
            type: 'foreign key',
            name: 'fk_orders_payment_type_guid',
            references: {
                table: 'payment_types',
                field: 'guid',
            },
            onUpdate: 'CASCADE',
            onDelete: 'NO ACTION',
        })

        await queryInterface.addConstraint('orders', {
            fields: ['coupon_guid'],
            type: 'foreign key',
            name: 'fk_orders_coupon_guid',
            references: {
                table: 'coupons',
                field: 'guid',
            },
            onUpdate: 'CASCADE',
            onDelete: 'NO ACTION',
        })
    },

    async down(queryInterface, Sequelize) {
        await queryInterface.removeConstraint('orders', 'fk_orders_user_guid')
        await queryInterface.removeConstraint(
            'orders',
            'fk_orders_payment_type_guid'
        )
        await queryInterface.removeConstraint('orders', 'fk_orders_coupon_guid')
        await queryInterface.removeIndex('orders', 'unique_orders_guid')
        await queryInterface.removeIndex('orders', 'idx_orders_user_guid')
        await queryInterface.removeIndex(
            'orders',
            'idx_orders_payment_type_guid'
        )
        await queryInterface.removeIndex('orders', 'idx_orders_coupon_guid')
        await queryInterface.dropTable('orders')
    },
}
