const express = require('express')
const { matchedData } = require('express-validator')
const router = express.Router()

// validaciones
const {
    ValidatorCreateLicense,
    ValidatorUpdateLicense,
    ValidatorDeleteLicense,
    ValidatorDisableLicense,
    ValidatorEnableLicense,
} = require('../../validators/general/licenses')

// Importa los controladores
const {
    CreateLicense,
    Licenses,
    License,
    UpdateLicense,
    DeleteLicense,
    DisableLicense,
    EnableLicense,
    LicensesAll,
    CountAllLicenses,
} = require('../../controllers/general/licenses')

// Middleware de autenticación
const authMiddleware = require('../../middleware/session')

// Rutas protegidas que requieren autenticación
/**
 * @swagger
 * tags:
 *   name: Licenses
 *   description: API para la gestión de licencias
 */

// Ruta para crear una nueva licencia
/**
 * @swagger
 * /licenses/CreateLicense:
 *   post:
 *     summary: Crear una nueva licencia
 *     description: Esta ruta permite crear una nueva licencia con nombre y descripción.
 *     tags: [Licenses]
 *     requestBody:
 *       required: true
 *       content:
 *         application/json:
 *           schema:
 *             type: object
 *             properties:
 *               name:
 *                 type: string
 *                 example: Licencia Básica
 *               description:
 *                  type: string
 *                  example: Licencia básica
 *               price:
 *                  type: number
 *                  example: 10.00
 *               trial_days:
 *                  type: number
 *                  example: 7
 *               duration_days:
 *                  type: number
 *                  example: 30
 *               url:
 *                  type: string
 *                  example: https://www.example.com
 *     responses:
 *       201:
 *         description: Licencia creada exitosamente
 *         content:
 *           application/json:
 *             schema:
 *               $ref: '#/components/responses/success'
 *       400:
 *         description: Error en los datos enviados
 *         content:
 *           application/json:
 *             schema:
 *               $ref: '#/components/responses/BadRequest'
 *       403:
 *         description: Error de autenticación
 *         content:
 *           application/json:
 *             schema:
 *               $ref: '#/components/responses/UnauthorizedError'
 *       409:
 *         description: Licencia ya existe
 *         content:
 *           application/json:
 *             schema:
 *               $ref: '#/components/responses/ConflictError'
 *       500:
 *         description: Error en el servidor
 *         content:
 *           application/json:
 *             schema:
 *               $ref: '#/components/responses/InternalServerError'
 *     security:
 *        - bearerAuth: []
 */
router.post(
    '/CreateLicense',
    authMiddleware,
    ValidatorCreateLicense,
    CreateLicense
)

// Ruta para obtener todas las licencias activas
/**
 * @swagger
 *  /licenses/Licenses:
 *   get:
 *     summary: Obtener todas las licencias activas
 *     tags: [Licenses]
 *     responses:
 *       201:
 *         description: Licencias obtenidas exitosamente
 *         content:
 *           application/json:
 *             schema:
 *               $ref: '#/components/responses/success'
 *       403:
 *         description: Error de autenticación
 *         content:
 *           application/json:
 *             schema:
 *               $ref: '#/components/responses/UnauthorizedError'
 *       500:
 *         description: Error en el servidor
 *         content:
 *           application/json:
 *             schema:
 *               $ref: '#/components/responses/InternalServerError'
 *     security:
 *        - bearerAuth: []
 */
router.get('/Licenses', authMiddleware, Licenses)

// Ruta para obtener una licencia
/**
 * @swagger
 * /licenses/License:
 *   get:
 *     summary: Obtiene una licencia por ID
 *     tags: [Licenses]
 *     parameters:
 *       - in: path
 *         name: guid
 *         schema:
 *           type: uuid
 *           format: uuid
 *         required: true
 *         description: El GUID de la licencia
 *     responses:
 *       201:
 *         description: Licencia obtenida exitosamente
 *         content:
 *           application/json:
 *             schema:
 *               $ref: '#/components/responses/success'
 *       403:
 *         description: Error de autenticación
 *         content:
 *           application/json:
 *             schema:
 *               $ref: '#/components/responses/UnauthorizedError'
 *       404:
 *        description: Licencia no encontrada
 *        content:
 *          application/json:
 *            schema:
 *              $ref: '#/components/responses/NotFoundError'
 *       500:
 *         description: Error en el servidor
 *         content:
 *           application/json:
 *             schema:
 *               $ref: '#/components/responses/InternalServerError'
 *     security:
 *        - bearerAuth: []
 */
router.get('/License/:guid', authMiddleware, License)

// Ruta para actualizar una licencia
/**
 * @swagger
 * /licenses/UpdateLicense:
 *   patch:
 *     summary: Actualizar una licencia
 *     tags: [Licenses]
 *     requestBody:
 *       required: true
 *       content:
 *         application/json:
 *           schema:
 *             type: object
 *             properties:
 *               guid:
 *                 type: string
 *                 example: 495db854-849b-41cd-b6b9-9c84824c92ba
 *               name:
 *                 type: string
 *                 example: Licencia Básica
 *               description:
 *                  type: string
 *                  example: Licencia básica
 *               price:
 *                  type: number
 *                  example: 10.00
 *               trial_days:
 *                  type: number
 *                  example: 7
 *               duration_days:
 *                  type: number
 *                  example: 30
 *               url:
 *                  type: string
 *                  example: https://www.example.com
 *     responses:
 *       201:
 *         description: Licencia actualizada exitosamente
 *         content:
 *           application/json:
 *             schema:
 *               $ref: '#/components/responses/success'
 *       400:
 *         description: Error en los datos enviados
 *         content:
 *           application/json:
 *             schema:
 *               $ref: '#/components/responses/BadRequest'
 *       403:
 *         description: Error de autenticación
 *         content:
 *           application/json:
 *             schema:
 *               $ref: '#/components/responses/UnauthorizedError'
 *       404:
 *        description: Licencia no encontrada
 *        content:
 *          application/json:
 *            schema:
 *              $ref: '#/components/responses/NotFoundError'
 *       500:
 *         description: Error en el servidor
 *         content:
 *           application/json:
 *             schema:
 *               $ref: '#/components/responses/InternalServerError'
 *     security:
 *        - bearerAuth: []
 */
router.patch(
    '/UpdateLicense/:guid',
    authMiddleware,
    ValidatorUpdateLicense,
    UpdateLicense
)

// Ruta para eliminar una licencia
/**
 * @swagger
 * /licenses/DeleteLicense:
 *   delete:
 *     summary: Eliminar una licencia
 *     tags: [Licenses]
 *     parameters:
 *       - in: path
 *         name: guid
 *         schema:
 *           type: uuid
 *           format: uuid
 *         required: true
 *         description: El GUID de la licencia
 *     responses:
 *       201:
 *         description: Licencia eliminada exitosamente
 *         content:
 *           application/json:
 *             schema:
 *               $ref: '#/components/responses/success'
 *       400:
 *         description: Error en los datos enviados
 *         content:
 *           application/json:
 *             schema:
 *               $ref: '#/components/responses/BadRequest'
 *       403:
 *         description: Error de autenticación
 *         content:
 *           application/json:
 *             schema:
 *               $ref: '#/components/responses/UnauthorizedError'
 *       404:
 *        description: Licencia no encontrada
 *        content:
 *          application/json:
 *            schema:
 *              $ref: '#/components/responses/NotFoundError'
 *       500:
 *         description: Error en el servidor
 *         content:
 *           application/json:
 *             schema:
 *               $ref: '#/components/responses/InternalServerError'
 *     security:
 *        - bearerAuth: []
 */
router.delete(
    '/DeleteLicense/:guid',
    authMiddleware,
    ValidatorDeleteLicense,
    DeleteLicense
)

// Ruta para desactivar una licencia
/**
 * @swagger
 * /licenses/DisableLicense:
 *   patch:
 *     summary: Desactivar una licencia
 *     tags: [Licenses]
 *     requestBody:
 *       required: true
 *       content:
 *         application/json:
 *           schema:
 *             type: object
 *             properties:
 *               guid:
 *                 type: uuid
 *                 example: 495db854-849b-41cd-b6b9-9c84824c92ba
 *     responses:
 *       201:
 *         description: Licencia desactivada exitosamente
 *         content:
 *           application/json:
 *             schema:
 *               $ref: '#/components/responses/success'
 *       400:
 *         description: Error en los datos enviados
 *         content:
 *           application/json:
 *             schema:
 *               $ref: '#/components/responses/BadRequest'
 *       403:
 *         description: Error de autenticación
 *         content:
 *           application/json:
 *             schema:
 *               $ref: '#/components/responses/UnauthorizedError'
 *       404:
 *        description: Licencia no encontrada
 *        content:
 *          application/json:
 *            schema:
 *              $ref: '#/components/responses/NotFoundError'
 *       500:
 *         description: Error en el servidor
 *         content:
 *           application/json:
 *             schema:
 *               $ref: '#/components/responses/InternalServerError'
 *     security:
 *        - bearerAuth: []
 */
router.patch(
    '/DisableLicense/:guid',
    authMiddleware,
    ValidatorDisableLicense,
    DisableLicense
)

// Ruta para activar una licencia
/**
 * @swagger
 * /licenses/EnableLicense:
 *   patch:
 *     summary: Activar una licencia
 *     tags: [Licenses]
 *     requestBody:
 *       required: true
 *       content:
 *         application/json:
 *           schema:
 *             type: object
 *             properties:
 *               guid:
 *                 type: uuid
 *                 example: 495db854-849b-41cd-b6b9-9c84824c92ba
 *     responses:
 *       201:
 *         description: Licencia activada exitosamente
 *         content:
 *           application/json:
 *             schema:
 *               $ref: '#/components/responses/success'
 *       400:
 *         description: Error en los datos enviados
 *         content:
 *           application/json:
 *             schema:
 *               $ref: '#/components/responses/BadRequest'
 *       403:
 *         description: Error de autenticación
 *         content:
 *           application/json:
 *             schema:
 *               $ref: '#/components/responses/UnauthorizedError'
 *       404:
 *        description: Licencia no encontrada
 *        content:
 *          application/json:
 *            schema:
 *              $ref: '#/components/responses/NotFoundError'
 *       500:
 *         description: Error en el servidor
 *         content:
 *           application/json:
 *             schema:
 *               $ref: '#/components/responses/InternalServerError'
 *     security:
 *        - bearerAuth: []
 */
router.patch(
    '/EnableLicense/:guid',
    authMiddleware,
    ValidatorEnableLicense,
    EnableLicense
)

// Ruta para obtener todas las licencias
/**
 * @swagger
 * /licenses/LicensesAll:
 *   get:
 *     summary: Obtener todas las licencias
 *     tags: [Licenses]
 *     responses:
 *       201:
 *         description: Licencias obtenidas exitosamente
 *         content:
 *           application/json:
 *             schema:
 *               $ref: '#/components/responses/success'
 *       400:
 *         description: Error en los datos enviados
 *         content:
 *           application/json:
 *             schema:
 *               $ref: '#/components/responses/BadRequest'
 *       403:
 *         description: Error de autenticación
 *         content:
 *           application/json:
 *             schema:
 *               $ref: '#/components/responses/UnauthorizedError'
 *       500:
 *         description: Error en el servidor
 *         content:
 *           application/json:
 *             schema:
 *               $ref: '#/components/responses/InternalServerError'
 *     security:
 *        - bearerAuth: []
 */
router.get('/LicensesAll', authMiddleware, LicensesAll)

// Ruta para contar todas las licencias
/**
 * @swagger
 * /licenses/LicensesCount:
 *   get:
 *     summary: Contar todas las licencias
 *     tags: [Licenses]
 *     responses:
 *       201:
 *         description: Licencias contadas exitosamente
 *         content:
 *           application/json:
 *             schema:
 *               $ref: '#/components/responses/success'
 *       400:
 *         description: Error en los datos enviados
 *         content:
 *           application/json:
 *             schema:
 *               $ref: '#/components/responses/BadRequest'
 *       403:
 *         description: Error de autenticación
 *         content:
 *           application/json:
 *             schema:
 *               $ref: '#/components/responses/UnauthorizedError'
 *       500:
 *         description: Error en el servidor
 *         content:
 *           application/json:
 *             schema:
 *               $ref: '#/components/responses/InternalServerError'
 *     security:
 *        - bearerAuth: []
 */
router.get('/LicensesCount', authMiddleware, CountAllLicenses)

module.exports = router
