// Importar Sequelize directamente
const { Sequelize } = require('sequelize')
// Importar tu configuración de sequelize
const { sequelize } = require('../../database/mysql')
// Importar el modelo de planes
const PlansModel = require('./plans')

const CouponsModel = sequelize.define(
    'coupons',
    {
        id: {
            type: Sequelize.INTEGER,
            primaryKey: true,
            autoIncrement: true,
        },
        guid: {
            type: Sequelize.UUID,
            defaultValue: Sequelize.UUIDV4,
            unique: true,
        },
        plan_guid: {
            type: Sequelize.UUID,
        },
        code: {
            type: Sequelize.TEXT,
        },
        description: {
            type: Sequelize.TEXT,
        },
        discount_type: {
            type: Sequelize.DECIMAL,
        },
        discount_value: {
            type: Sequelize.DECIMAL,
        },
        start_date: {
            type: Sequelize.DATE,
        },
        end_date: {
            type: Sequelize.DATE,
        },
        uses: {
            type: Sequelize.INTEGER,
        },
        used: {
            type: Sequelize.INTEGER,
        },
        link_by: {
            type: Sequelize.TEXT,
        },
        min_amount: {
            type: Sequelize.DECIMAL,
        },
        max_amount: {
            type: Sequelize.DECIMAL,
        },
        is_active: {
            type: Sequelize.INTEGER,
        },
        created_by: {
            type: Sequelize.UUID,
        },
        updated_by: {
            type: Sequelize.UUID,
        },
        deleted_by: {
            type: Sequelize.UUID,
        },
        created_at: {
            type: Sequelize.DATE,
        },
        updated_at: {
            type: Sequelize.DATE,
        },
        deleted_at: {
            type: Sequelize.DATE,
        },
    },
    {
        tableName: 'coupons',
        timestamps: false,
        defaultScope: {
            attributes: {
                exclude: [
                    'created_by',
                    'updated_by',
                    'deleted_by',
                    'created_at',
                    'updated_at',
                    'deleted_at',
                ],
            },
            include: [
                {
                    model: PlansModel,
                    as: 'plan',
                    where: {
                        is_active: 1,
                    },
                    exclude: [
                        'created_by',
                        'updated_by',
                        'deleted_by',
                        'created_at',
                        'updated_at',
                        'deleted_at',
                    ],
                },
            ],
        },
    }
)

CouponsModel.belongsTo(PlansModel, {
    foreignKey: 'plan_guid',
    targetKey: 'guid',
    as: 'plan',
})

module.exports = CouponsModel
