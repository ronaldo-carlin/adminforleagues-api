const { Op } = require('sequelize')
const { v4: uuidv4 } = require('uuid')
const { matchedData } = require('express-validator')
const { handleHttpSuccess } = require('../../utils/handleSuccess')
const { handleHttpError } = require('../../utils/handleError')
const { handleHttpCount } = require('../../utils/handleCount')
const {
    getSequelize,
    getCategoriesModel,
} = require('../../helpers/sequelizeHelper')

/**
 * Este controlador es el encargado de registrar una categoría
 * @param {*} req
 * @param {*} res
 */
const CreateCategory = async (req, res) => {
    try {
        const sequelize = getSequelize(req)
        const CategoryModel = getCategoriesModel(sequelize)
        const data = matchedData(req)
        const created_by = req.user.guid
        const created_at = new Date()
        const guid = await uuidv4()
        data.created_by = created_by
        data.created_at = created_at
        data.guid = guid

        const category = await CategoryModel.create(data)
        return handleHttpSuccess(res, 'Categoría creada.', 201, category)
    } catch (error) {
        console.log('Error al crear la categoría:', error)
        handleHttpError(res, 'Error al crear la categoría.', 403, error)
    }
}

/**
 * Este controlador es el encargado de obtener las categorías activas
 * @param {*} req
 * @param {*} res
 */
const Categories = async (req, res) => {
    try {
        const sequelize = getSequelize(req)
        const CategoryModel = getCategoriesModel(sequelize)

        const categories = await CategoryModel.findAll({
            where: { is_active: 1 },
        })

        if (!categories.length) {
            handleHttpError(res, 'No se encontraron categorías.', 404)
            return
        }

        handleHttpSuccess(res, 'Categorías encontradas.', 201, categories)
    } catch (error) {
        console.log('Error obteniendo categorías:', error)
        handleHttpError(res, 'Error obteniendo categorías.', 403, error)
    }
}

/**
 * Este controlador es el encargado de obtener las categorías activas
 * @param {*} req
 * @param {*} res
 */
const CategoriesSport = async (req, res) => {
    try {
        const sequelize = getSequelize(req)
        const CategoryModel = getCategoriesModel(sequelize)
        const { guid } = req.params

        const categories = await CategoryModel.findAll({
            where: { is_active: 1, sport_guid: guid },
        })

        if (!categories.length) {
            handleHttpError(res, 'No se encontraron categorías.', 200)
            return
        }

        handleHttpSuccess(res, 'Categorías encontradas.', 201, categories)
    } catch (error) {
        console.log('Error obteniendo categorías:', error)
        handleHttpError(res, 'Error obteniendo categorías.', 403, error)
    }
}

/**
 * Este controlador es el encargado de obtener una categoría por GUID
 * @param {*} req
 * @param {*} res
 */
const Category = async (req, res) => {
    try {
        const sequelize = getSequelize(req)
        const CategoryModel = getCategoriesModel(sequelize)
        const { guid } = req.params

        const category = await CategoryModel.findOne({
            where: { guid },
        })

        if (!category) {
            handleHttpError(res, 'La categoría no existe.', 204)
            return
        }

        if (category.is_active == 0) {
            handleHttpError(res, 'La categoría ha sido deshabilitada.', 410)
            return
        }

        handleHttpSuccess(res, 'Categoría encontrada.', 201, category)
    } catch (error) {
        console.log('Error obteniendo categoría:', error)
        handleHttpError(res, 'Error obteniendo categoría.', 403, error)
    }
}

/**
 * Este controlador es el encargado de actualizar una categoría
 * @param {*} req
 * @param {*} res
 */
const UpdateCategory = async (req, res) => {
    try {
        const sequelize = getSequelize(req)
        const CategoryModel = getCategoriesModel(sequelize)
        const { guid } = req.params
        const data = matchedData(req)
        const updated_by = req.user.guid
        const updated_at = new Date()
        data.updated_by = updated_by
        data.updated_at = updated_at

        const [updateCount] = await CategoryModel.update(data, {
            where: { guid },
        })

        if (updateCount === 0) {
            return handleHttpError(res, 'La categoría no existe.', 404)
        }

        const category = await CategoryModel.findOne({
            where: { guid },
        })

        if (!category) {
            handleHttpError(res, 'La categoría no existe.', 404)
            return
        }

        if (category.is_active == 0) {
            handleHttpError(res, 'La categoría ha sido deshabilitada.', 401)
            return
        }

        handleHttpSuccess(res, 'Categoría actualizada.', 201, category)
    } catch (error) {
        console.log('Error actualizando categoría:', error)
        handleHttpError(res, 'Error actualizando categoría.', 403, error)
    }
}

/**
 * Este controlador es el encargado de eliminar una categoría
 * @param {*} req
 * @param {*} res
 */
const DeleteCategory = async (req, res) => {
    try {
        const sequelize = getSequelize(req)
        const CategoryModel = getCategoriesModel(sequelize)
        const { guid } = req.params
        const deleted_by = req.user.guid
        const deleted_at = new Date()
        const [updateCount] = await CategoryModel.update(
            {
                is_active: 0,
                deleted_by,
                deleted_at,
            },
            {
                where: { guid },
            }
        )

        if (updateCount === 0) {
            return handleHttpError(res, 'La categoría no existe.', 404)
        }

        handleHttpSuccess(res, 'Categoría eliminada.', 201)
    } catch (error) {
        console.log('Error eliminando categoría:', error)
        handleHttpError(res, 'Error eliminando categoría.', 403, error)
    }
}

/**
 * Este controlador es el encargado de desactivar una categoría
 * @param {*} req
 * @param {*} res
 */
const DisableCategory = async (req, res) => {
    try {
        const sequelize = getSequelize(req)
        const CategoryModel = getCategoriesModel(sequelize)
        const { guid } = req.params
        const updated_by = req.user.guid
        const updated_at = new Date()
        const [updateCount] = await CategoryModel.update(
            {
                is_active: 0,
                updated_by,
                updated_at,
            },
            {
                where: { guid },
            }
        )

        if (updateCount === 0) {
            return handleHttpError(res, 'La categoría no existe.', 404)
        }

        handleHttpSuccess(res, 'Categoría deshabilitada.', 201)
    } catch (error) {
        console.log('Error deshabilitando categoría:', error)
        handleHttpError(res, 'Error deshabilitando categoría.', 403, error)
    }
}

/**
 * Este controlador es el encargado de activar una categoría
 * @param {*} req
 * @param {*} res
 */
const EnableCategory = async (req, res) => {
    try {
        const sequelize = getSequelize(req)
        const CategoryModel = getCategoriesModel(sequelize)
        const { guid } = req.params
        const updated_by = req.user.guid
        const updated_at = new Date()
        const [updateCount] = await CategoryModel.update(
            {
                is_active: 1,
                updated_by,
                updated_at,
            },
            {
                where: { guid },
            }
        )

        if (updateCount === 0) {
            return handleHttpError(res, 'La categoría no existe.', 404)
        }

        handleHttpSuccess(res, 'Categoría habilitada.', 201)
    } catch (error) {
        console.log('Error habilitando categoría:', error)
        handleHttpError(res, 'Error habilitando categoría.', 403, error)
    }
}

/**
 * Este controlador es el encargado de obtener todas las categorías
 * @param {*} req
 * @param {*} res
 */
const CategoriesAll = async (req, res) => {
    try {
        const sequelize = getSequelize(req)
        const CategoryModel = getCategoriesModel(sequelize)

        const categories = await CategoryModel.findAll()

        if (!categories.length) {
            handleHttpError(res, 'No se encontraron categorías.', 404)
            return
        }

        handleHttpSuccess(res, 'Categorías encontradas.', 201, categories)
    } catch (error) {
        console.log('Error obteniendo categorías:', error)
        handleHttpError(res, 'Error obteniendo categorías.', 403, error)
    }
}

/**
 * Este controlador es el encargado de contar todas las categorías
 * @param {*} req
 * @param {*} res
 */
const CategoriesCount = async (req, res) => {
    try {
        const sequelize = getSequelize(req)
        const CategoryModel = getCategoriesModel(sequelize)

        const count = await CategoryModel.count()

        handleHttpCount(res, 'Categorías encontradas.', 201, count)
    } catch (error) {
        console.log('Error contando categorías:', error)
        handleHttpError(res, 'Error contando categorías.', 403, error)
    }
}

module.exports = {
    CreateCategory,
    Categories,
    CategoriesSport,
    Category,
    UpdateCategory,
    DeleteCategory,
    DisableCategory,
    EnableCategory,
    CategoriesAll,
    CategoriesCount,
}
