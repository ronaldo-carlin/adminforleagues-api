const { matchedData } = require('express-validator')
const AccessControlModel = require('../../models/general/access_control')
const RolesModel = require('../../models/general/roles')
const ModulesModel = require('../../models/general/modules')
const PermissionsModel = require('../../models/general/permissions')
const PlansModel = require('../../models/general/plans')
const LicensesModel = require('../../models/general/licenses')
const { handleHttpError } = require('../../utils/handleError')
const { handleHttpSuccess } = require('../../utils/handleSuccess')
const { handleHttpCount } = require('../../utils/handleCount')

/**
 * @description Función para crear un nuevo registro de control de acceso
 * @param {Object} req Request object
 * @param {Object} res Response object
 */
const CreateAccessControl = async (req, res) => {
    try {
        // Obtenemos los datos que vienen en el body
        const data = matchedData(req)
        // Obtenemos el guid del usuario que esta creando el control de acceso
        const created_by = req.user.guid
        // Obtenemos la fecha actual
        const created_at = new Date()
        // Le agregamos el usuario y la fecha de creación
        data.created_by = created_by
        data.created_at = created_at
        // Creamos el control de acceso
        const accessControl = await AccessControlModel.create(data)
        return handleHttpSuccess(
            res,
            'Access control created successfully',
            201,
            accessControl
        )
    } catch (error) {
        return handleHttpError(
            res,
            'Error al crear el control de acceso.',
            403,
            error
        )
    }
}

/**
 * @description Función para obtener todos los registros de control de acceso activos
 * @param {Object} req Request object
 * @param {Object} res Response object
 */
const AccessControls = async (req, res) => {
    try {
        // Buscamos todos los controles de acceso activos
        const accessControls = await AccessControlModel.findAll({
            where: { is_active: 1 },
        })
        return handleHttpSuccess(
            res,
            'Access controls retrieved',
            201,
            accessControls
        )
    } catch (error) {
        return handleHttpError(res, 'Error al obtener los datos', 403, error)
    }
}

/**
 * @description Función para obtener un registro de control de acceso por guid
 * @param {Object} req Request object
 * @param {Object} res Response object
 */
const AccessControl = async (req, res) => {
    try {
        const { guid } = req.params
        const accessControl = await AccessControlModel.findOne({
            where: { guid },
        })
        // Si no se encuentra el control de acceso
        if (!accessControl) {
            return handleHttpError(
                res,
                'Access control not found',
                404,
                'Access control not found'
            )
        }
        // Si se encuentra el control de acceso y el is_active es 0
        if (accessControl.is_active === 0) {
            return handleHttpError(
                res,
                'Access control is not active',
                404,
                'Access control is not active'
            )
        }
        return handleHttpSuccess(
            res,
            'Access control retrieved',
            201,
            accessControl
        )
    } catch (error) {
        console.error('Error: ', error) // Log the error
        handleHttpError(res, '', 403, error)
    }
}

/**
 * @description Función para actualizar un registro de control de acceso por guid
 * @param {Object} req Request object
 * @param {Object} res Response object
 */
const UpdateAccessControl = async (req, res) => {
    try {
        // Obtenemos el guid del control de acceso
        const { guid } = req.params
        //Obtenemos el guid del usuario que esta actualizando el rol
        const updated_by = req.user.guid
        //Obtenemos la fecha actual
        const updated_at = new Date()
        // Obtenemos los datos a actualizar
        const data = matchedData(req)
        // le agregamos el usuario y la fecha de actualización
        data.updated_by = updated_by
        data.updated_at = updated_at
        // Actualizamos el registro
        const [updateCount] = await AccessControlModel.update(data, {
            where: { guid },
        })
        if (updateCount === 0) {
            return handleHttpError(res, 'Access control not found', 404)
        }
        // Obtenemos el registro actualizado
        const accessControl = await AccessControlModel.findOne({
            where: { guid },
        })
        // Si no se encuentra el control de acceso
        if (!accessControl) {
            return handleHttpError(res, 'Access control not found', 404)
        }
        // Si se actualiza el registro
        return handleHttpSuccess(
            res,
            'Access control updated successfully',
            201,
            accessControl
        )
    } catch (error) {
        console.error('Error: ', error) // Log the error
        return handleHttpError(res, '', 403, error)
    }
}

/**
 * @description Función para eliminar un registro de control de acceso por guid
 * @param {Object} req Request object
 * @param {Object} res Response object
 */
const DeleteAccessControl = async (req, res) => {
    try {
        // Obtenemos el guid del control de acceso
        const { guid } = req.params
        // Obtenemos el guid del usuario que esta eliminando el rol
        const deleted_by = req.user.guid
        // Obtenemos la fecha actual
        const deleted_at = new Date()
        // Eliminamos el registro
        await AccessControlModel.update(
            {
                is_active: 0,
                deleted_by,
                deleted_at,
            },
            { where: { guid } }
        )
        return handleHttpSuccess(
            res,
            'Access control deleted successfully',
            201
        )
    } catch (error) {
        console.error('Error: ', error) // Log the error
        return handleHttpError(res, '', 403, error)
    }
}

/**
 * @description Función para desactivar un registro de control de acceso por guid
 * @param {Object} req Request object
 * @param {Object} res Response object
 */
const DisableAccessControl = async (req, res) => {
    try {
        // Obtenemos el guid del control de acceso
        const { guid } = req.params
        // Obtenemos el guid del usuario que esta desactivando el rol
        const updated_by = req.user.guid
        // Obtenemos la fecha actual
        const updated_at = new Date()
        // Desactivamos el registro
        await AccessControlModel.update(
            {
                is_active: 0,
                updated_at,
                updated_by,
            },
            { where: { guid } }
        )
        return handleHttpSuccess(
            res,
            'Access control deactivated successfully',
            201
        )
    } catch (error) {
        console.error('Error: ', error) // Log the error
        return handleHttpError(res, '', 403, error)
    }
}

/**
 * @description Función para activar un registro de control de acceso por guid
 * @param {Object} req Request object
 * @param {Object} res Response object
 */
const EnableAccessControl = async (req, res) => {
    try {
        // Obtenemos el guid del control de acceso
        const { guid } = req.params
        // Obtenemos el guid del usuario que esta activando el rol
        const updated_by = req.user.guid
        // Obtenemos la fecha actual
        const updated_at = new Date()
        // Activamos el registro
        await AccessControlModel.update(
            {
                is_active: 1,
                updated_at,
                updated_by,
            },
            { where: { guid } }
        )
        return handleHttpSuccess(
            res,
            'Access control activated successfully',
            201
        )
    } catch (error) {
        console.error('Error: ', error) // Log the error
        return handleHttpError(res, '', 403, error)
    }
}

/**
 * @description Función para obtener todos los registros de control de acceso
 * @param {Object} req Request object
 * @param {Object} res Response object
 */
const AccessControlsAll = async (req, res) => {
    try {
        const accessControls = await AccessControlModel.findAll()
        return handleHttpSuccess(
            res,
            'Access controls retrieved',
            201,
            accessControls
        )
    } catch (error) {
        console.error('Error: ', error) // Log the error
        return handleHttpError(res, '', 403, error)
    }
}

/**
 * @description Función para contar todos los registros de control de acceso
 * @param {Object} req Request object
 * @param {Object} res Response object
 */
const CountAccessControls = async (req, res) => {
    try {
        const count = await AccessControlModel.count()
        return handleHttpCount(res, count)
    } catch (error) {
        console.error('Error: ', error) // Log the error
        return handleHttpError(res, '', 403, error)
    }
}

module.exports = {
    CreateAccessControl,
    AccessControls,
    AccessControl,
    UpdateAccessControl,
    DeleteAccessControl,
    DisableAccessControl,
    EnableAccessControl,
    AccessControlsAll,
    CountAccessControls,
}
