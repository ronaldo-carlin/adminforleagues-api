const { check, validationResult } = require('express-validator')

// Validación para crear un nuevo equipo
const validatorCreateTeam = () => {
    return [
        async (req, res, next) => {
            await check('name')
                .notEmpty()
                .withMessage('El nombre del equipo es requerido.')
                .run(req)
            await check('competition_guid')
                .optional()
                .isUUID()
                .withMessage('El competition_guid debe ser un UUID válido.')
                .run(req)
            await check('group')
                .optional()
                .isString()
                .withMessage('El grupo debe ser una cadena de texto.')
                .run(req)
            await check('image_path')
                .optional()
                .isString()
                .withMessage(
                    'El path de la imagen debe ser una cadena de texto.'
                )
                .run(req)
            await check('description')
                .optional()
                .isString()
                .withMessage('La descripción debe ser una cadena de texto.')
                .run(req)

            const errors = validationResult(req)
            if (!errors.isEmpty()) {
                return res.status(400).json({ errors: errors.array() })
            }

            next()
        },
    ]
}

// Validación para obtener un equipo por GUID
const validatorTeam = () => {
    return [
        async (req, res, next) => {
            await check('guid')
                .isUUID()
                .withMessage('El guid del equipo es requerido.')
                .run(req)

            const errors = validationResult(req)
            if (!errors.isEmpty()) {
                return res.status(400).json({ errors: errors.array() })
            }

            next()
        },
    ]
}

// Validación para actualizar un equipo
const validatorUpdateTeam = () => {
    return [
        async (req, res, next) => {
            await check('guid')
                .isUUID()
                .withMessage('El guid del equipo es requerido.')
                .run(req)
            await check('name')
                .optional()
                .notEmpty()
                .withMessage('El nombre del equipo es requerido.')
                .run(req)
            await check('competition_guid')
                .optional()
                .isUUID()
                .withMessage('El competition_guid debe ser un UUID válido.')
                .run(req)
            await check('group')
                .optional()
                .isString()
                .withMessage('El grupo debe ser una cadena de texto.')
                .run(req)
            await check('image_path')
                .optional()
                .isString()
                .withMessage(
                    'El path de la imagen debe ser una cadena de texto.'
                )
                .run(req)
            await check('description')
                .optional()
                .isString()
                .withMessage('La descripción debe ser una cadena de texto.')
                .run(req)

            const errors = validationResult(req)
            if (!errors.isEmpty()) {
                return res.status(400).json({ errors: errors.array() })
            }

            next()
        },
    ]
}

// Validación para eliminar un equipo
const validatorDeleteTeam = () => {
    return [
        async (req, res, next) => {
            await check('guid')
                .isUUID()
                .withMessage('El guid del equipo es requerido.')
                .run(req)

            const errors = validationResult(req)
            if (!errors.isEmpty()) {
                return res.status(400).json({ errors: errors.array() })
            }

            next()
        },
    ]
}

// Validación para deshabilitar un equipo
const validatorDisableTeam = () => {
    return [
        async (req, res, next) => {
            await check('guid')
                .isUUID()
                .withMessage('El guid del equipo es requerido.')
                .run(req)

            const errors = validationResult(req)
            if (!errors.isEmpty()) {
                return res.status(400).json({ errors: errors.array() })
            }

            next()
        },
    ]
}

// Validación para habilitar un equipo
const validatorEnableTeam = () => {
    return [
        async (req, res, next) => {
            await check('guid')
                .isUUID()
                .withMessage('El guid del equipo es requerido.')
                .run(req)

            const errors = validationResult(req)
            if (!errors.isEmpty()) {
                return res.status(400).json({ errors: errors.array() })
            }

            next()
        },
    ]
}

module.exports = {
    validatorCreateTeam,
    validatorTeam,
    validatorUpdateTeam,
    validatorDeleteTeam,
    validatorDisableTeam,
    validatorEnableTeam,
}
