const { handleHttpError } = require('../utils/handleError')
const { verifyAccessToken } = require('../utils/handleJwtTenant')
const AuthTenantModelFactory = require('../models/tenant/tenant_auth')

const authMiddleware = async (req, res, next) => {
    try {
        //Si no hay un Authorization en los headers retornamos error
        if (!req.headers.authorization) {
            handleHttpError(res, 'NEED_SESSION', 401)
            return
        }
        //Si existe un Authorization en los headers quitamos todo lo que este antes del espacio
        const token = req.headers.authorization.split(' ').pop()
        //Verificamos el token
        const dataToken = await verifyAccessToken(token)
        //Si no hay informacion en el token retornamos error
        if (!dataToken) {
            handleHttpError(res, 'Unauthorized', 401)
            return
        }
        //Obtenemos el id que viene en el token para consultar en la base de datos
        const query = {
            guid: dataToken.guid,
        }
        // obtenemos la instancia de sequelize del middleware
        const sequelize = getSequelize(req)
        // obtenemos el modelo de usuario
        const AuthTenantModel = getUserModel(sequelize)
        //Buscamos los datos del usuario en la base de datos en base al id del token
        const user = await AuthTenantModel.findOne({
            query,
            attributes: { exclude: ['password'] },
        })
        //Enviamos los datos
        req.user = await user
        next()
    } catch (e) {
        console.log('session', e)
        handleHttpError(res, 'NOT_SESSION', 401)
    }
}

// funcion para obtener el squelize del request y poder configurar los modelos
const getSequelize = (req) => {
    return req.sequelize
}

// funcion para obtener el modelo de usuario
const getUserModel = (sequelize) => {
    return AuthTenantModelFactory(sequelize)
}

module.exports = authMiddleware
