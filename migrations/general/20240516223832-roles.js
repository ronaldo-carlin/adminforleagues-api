module.exports = {
    async up(queryInterface, Sequelize) {
        // entidad para los roles de los usuarios
        await queryInterface.createTable('roles', {
            // identificador del registro
            id: {
                type: Sequelize.INTEGER,
                primaryKey: true,
                autoIncrement: true,
                allowNull: false,
            },
            // guid del registro
            guid: {
                type: Sequelize.UUID,
                defaultValue: Sequelize.UUIDV4, // Genera un UUID por defecto
                allowNull: false,
                unique: true,
            },
            // nombre del rol
            name: {
                type: Sequelize.TEXT,
            },
            // descripcion del rol
            description: {
                type: Sequelize.TEXT,
            },
            // estado del registro
            is_active: {
                type: Sequelize.BOOLEAN,
                defaultValue: 1, // valor por defecto
            },
            // usuario que creo el registro
            created_by: {
                type: Sequelize.UUID,
            },
            // fecha de creacion
            created_at: {
                type: Sequelize.DATE,
                defaultValue: Sequelize.literal('CURRENT_TIMESTAMP'),
            },
            // usuario que actualizo el registro
            updated_by: {
                type: Sequelize.UUID,
            },
            // fecha de actualizacion
            updated_at: {
                type: Sequelize.DATE,
            },
            // usuario que elimino el registro
            deleted_by: {
                type: Sequelize.UUID,
            },
            // fecha de eliminacion
            deleted_at: {
                type: Sequelize.DATE,
            },
        })

        // Agregar índice único después de crear la tabla
        await queryInterface.addIndex('roles', ['guid'], {
            unique: true,
            name: 'uniq_roles_guid',
        })
    },

    async down(queryInterface, Sequelize) {
        await queryInterface.removeIndex('roles', 'uniq_roles_guid')
        await queryInterface.dropTable('roles')
    },
}
