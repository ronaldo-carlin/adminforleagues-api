const { check } = require('express-validator')
const validateResults = require('../../utils/handleValidators')
const ModulesModel = require('../../models/general/modules')

// Validaciones para crear un Module
const ValidatorCreateModule = [
    check('name').isString().withMessage('El nombre es requerido.'),
    check('description').isString().withMessage('La descripción es requerida.'),
    check('to').isString().withMessage('La ruta es requerida.'),
    check('icon').isString().withMessage('El icono es requerido.'),
    check('is_parent').isBoolean().withMessage('El is_parent es requerido.'),
    check('parent_guid').isUUID().withMessage('El parent es requerido.'),
    validateResults,
]

// Validaciones para actualizar un Module
const ValidatorUpdateModule = [
    check('guid').isUUID().withMessage('El guid es requerido.'),
    check('name').isString().withMessage('El nombre es requerido.'),
    check('description').isString().withMessage('La descripción es requerida.'),
    check('to').isString().withMessage('La ruta es requerida.'),
    check('icon').isString().withMessage('El icono es requerido.'),
    check('is_parent').isBoolean().withMessage('El is_parent es requerido.'),
    check('parent_guid').isUUID().withMessage('El parent es requerido.'),
    validateResults,
]

// Validaciones para eliminar un Module
const ValidatorDeleteModule = [
    check('guid').isUUID().withMessage('El guid es requerido.'),
    validateResults,
]

// Validaciones para deshabilitar un Module
const ValidatorDisableModule = [
    check('guid').isUUID().withMessage('El guid es requerido.'),
    validateResults,
]

// Validaciones para habilitar un Module
const ValidatorEnableModule = [
    check('guid').isUUID().withMessage('El guid es requerido.'),
    validateResults,
]

module.exports = {
    ValidatorCreateModule,
    ValidatorUpdateModule,
    ValidatorDeleteModule,
    ValidatorDisableModule,
    ValidatorEnableModule,
}
