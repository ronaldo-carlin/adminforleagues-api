const { check, validationResult } = require('express-validator')

const validatorCreateAccessControl = () => {
    return [
        async (req, res, next) => {
            await check('role_guid')
                .isUUID()
                .withMessage('El guid del rol es requerido.')
                .run(req)
            await check('module_guid')
                .isUUID()
                .withMessage('El guid del módulo es requerido.')
                .run(req)
            await check('permissions_guid')
                .isUUID()
                .withMessage('El guid del permiso es requerido.')
                .run(req)

            const errors = validationResult(req)
            if (!errors.isEmpty()) {
                return res.status(400).json({ errors: errors.array() })
            }

            next()
        },
    ]
}

const validatorAccessControl = () => {
    return [
        async (req, res, next) => {
            await check('guid')
                .isUUID()
                .withMessage('El guid es requerido.')
                .run(req)

            const errors = validationResult(req)
            if (!errors.isEmpty()) {
                return res.status(400).json({ errors: errors.array() })
            }

            next()
        },
    ]
}

const validatorUpdateAccessControl = () => {
    return [
        async (req, res, next) => {
            await check('guid')
                .isUUID()
                .withMessage('El guid es requerido.')
                .run(req)
            await check('role_guid')
                .isUUID()
                .withMessage('El guid del rol es requerido.')
                .run(req)
            await check('module_guid')
                .isUUID()
                .withMessage('El guid del módulo es requerido.')
                .run(req)
            await check('permissions_guid')
                .isUUID()
                .withMessage('El guid del permiso es requerido.')
                .run(req)

            const errors = validationResult(req)
            if (!errors.isEmpty()) {
                return res.status(400).json({ errors: errors.array() })
            }

            next()
        },
    ]
}

const validatorDeleteAccessControl = () => {
    return [
        async (req, res, next) => {
            await check('guid')
                .isUUID()
                .withMessage('El guid es requerido.')
                .run(req)

            const errors = validationResult(req)
            if (!errors.isEmpty()) {
                return res.status(400).json({ errors: errors.array() })
            }

            next()
        },
    ]
}

const validatorDisableAccessControl = () => {
    return [
        async (req, res, next) => {
            await check('guid')
                .isUUID()
                .withMessage('El guid es requerido.')
                .run(req)

            const errors = validationResult(req)
            if (!errors.isEmpty()) {
                return res.status(400).json({ errors: errors.array() })
            }

            next()
        },
    ]
}

const validatorEnableAccessControl = () => {
    return [
        async (req, res, next) => {
            await check('guid')
                .isUUID()
                .withMessage('El guid es requerido.')
                .run(req)

            const errors = validationResult(req)
            if (!errors.isEmpty()) {
                return res.status(400).json({ errors: errors.array() })
            }

            next()
        },
    ]
}

const validatorAccessControlsByRol = () => {
    return [
        async (req, res, next) => {
            console.log('entro')
            await check('guid')
                .isUUID()
                .withMessage('El guid del rol es requerido.')
                .run(req)

            const errors = validationResult(req)
            if (!errors.isEmpty()) {
                return res.status(400).json({ errors: errors.array() })
            }

            next()
        },
    ]
}

module.exports = {
    validatorCreateAccessControl,
    validatorAccessControl,
    validatorUpdateAccessControl,
    validatorDeleteAccessControl,
    validatorDisableAccessControl,
    validatorEnableAccessControl,
    validatorAccessControlsByRol,
}
