module.exports = {
    async up(queryInterface, Sequelize) {
        // entidad para almacenar las reglas de los diferentes tipos de torneos
        await queryInterface.createTable('competitions_rules', {
            // id del registro
            id: {
                type: Sequelize.INTEGER,
                primaryKey: true,
                autoIncrement: true,
                allowNull: false,
            },
            guid: {
                type: Sequelize.UUID,
                defaultValue: Sequelize.UUIDV4,
                unique: true,
                allowNull: false,
            },
            // identificador del tipo de torneo
            type_competition_guid: {
                type: Sequelize.UUID,
            },
            // titulo de la regla
            title: {
                type: Sequelize.TEXT,
            },
            // descripcion de la regla
            description: {
                type: Sequelize.TEXT,
            },
            // estado del registro
            is_active: {
                type: Sequelize.BOOLEAN,
                defaultValue: 1, // valor por defecto
            },
            // usuario que creo el registro
            created_by: {
                type: Sequelize.UUID,
            },
            // fecha de creacion
            created_at: {
                type: Sequelize.DATE,
                defaultValue: Sequelize.literal('CURRENT_TIMESTAMP'),
            },
            // usuario que actualizo el registro
            updated_by: {
                type: Sequelize.UUID,
            },
            // fecha de actualizacion
            updated_at: {
                type: Sequelize.DATE,
            },
            // usuario que elimino el registro
            deleted_by: {
                type: Sequelize.UUID,
            },
            // fecha de eliminacion
            deleted_at: {
                type: Sequelize.DATE,
            },
        })

        // indices
        await queryInterface.addIndex('competitions_rules', ['guid'], {
            unique: true,
            name: 'unique_competitions_rules_guid',
        })

        await queryInterface.addIndex(
            'competitions_rules',
            ['type_competition_guid'],
            {
                name: 'idx_type_competition_guid',
            }
        )

        // llaves foraneas
        await queryInterface.addConstraint('competitions_rules', {
            fields: ['type_competition_guid'],
            type: 'foreign key',
            name: 'fk_competitions_rules_type_competition_guid',
            references: {
                table: 'types_competitions',
                field: 'guid',
            },
            onDelete: 'restrict',
            onUpdate: 'cascade',
        })
    },

    async down(queryInterface, Sequelize) {
        await queryInterface.removeConstraint(
            'competitions_rules',
            'fk_competitions_rules_type_competition_guid'
        )
        await queryInterface.removeIndex(
            'competitions_rules',
            'unique_competitions_rules_guid'
        )
        await queryInterface.removeIndex(
            'competitions_rules',
            'idx_type_competition_guid'
        )
        await queryInterface.dropTable('competitions_rules')
    },
}
