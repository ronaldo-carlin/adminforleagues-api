// Importar Sequelize
const { DataTypes } = require('sequelize')

// Definir la función factory para el modelo MatchStatisticsTenantModel
const MatchStatisticsTenantModelFactory = (sequelize) => {
    const MatchStatisticsTenantModel = sequelize.define(
        'match_statistics',
        {
            id: {
                type: DataTypes.INTEGER,
                primaryKey: true,
                autoIncrement: true,
            },
            guid: {
                type: DataTypes.UUID,
                primaryKey: true,
                defaultValue: DataTypes.UUIDV4,
                unique: true,
            },
            winner_team_guid: {
                type: DataTypes.TEXT,
            },
            match_guid: {
                type: DataTypes.TEXT,
            },
            fouls_home_team: {
                type: DataTypes.INTEGER,
            },
            fouls_away_team: {
                type: DataTypes.INTEGER,
            },
            corners_home_team: {
                type: DataTypes.INTEGER,
            },
            corners_away_team: {
                type: DataTypes.INTEGER,
            },
            offsides_home_team: {
                type: DataTypes.INTEGER,
            },
            offsides_away_team: {
                type: DataTypes.INTEGER,
            },
            passes_home_team: {
                type: DataTypes.INTEGER,
            },
            passes_away_team: {
                type: DataTypes.INTEGER,
            },
            pass_accuracy_home_team: {
                type: DataTypes.INTEGER,
            },
            pass_accuracy_away_team: {
                type: DataTypes.INTEGER,
            },
            shots_home_team: {
                type: DataTypes.INTEGER,
            },
            shots_away_team: {
                type: DataTypes.INTEGER,
            },
            shots_on_target_home_team: {
                type: DataTypes.INTEGER,
            },
            shots_on_target_away_team: {
                type: DataTypes.INTEGER,
            },
            possesion_home_team: {
                type: DataTypes.INTEGER,
            },
            possesion_away_team: {
                type: DataTypes.INTEGER,
            },
            home_team_goals: {
                type: DataTypes.INTEGER,
            },
            away_team_goals: {
                type: DataTypes.INTEGER,
            },
            penalty_home_team: {
                type: DataTypes.INTEGER,
            },
            penalty_away_team: {
                type: DataTypes.INTEGER,
            },
            yellow_cards_home_team: {
                type: DataTypes.INTEGER,
            },
            yellow_cards_away_team: {
                type: DataTypes.INTEGER,
            },
            red_cards_home_team: {
                type: DataTypes.INTEGER,
            },
            red_cards_away_team: {
                type: DataTypes.INTEGER,
            },
            is_active: {
                type: DataTypes.INTEGER,
                defaultValue: 1,
            },
            created_by: {
                type: DataTypes.UUID,
            },
            updated_by: {
                type: DataTypes.UUID,
            },
            deleted_by: {
                type: DataTypes.UUID,
            },
            created_at: {
                type: DataTypes.DATE,
            },
            updated_at: {
                type: DataTypes.DATE,
            },
            deleted_at: {
                type: DataTypes.DATE,
            },
        },
        {
            tableName: 'match_statistics',
            timestamps: false,
        }
    )

    return MatchStatisticsTenantModel
}

module.exports = MatchStatisticsTenantModelFactory
