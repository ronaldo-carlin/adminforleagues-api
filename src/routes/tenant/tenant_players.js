const express = require('express')
const router = express.Router()

// Controladores y validadores para players
const {
    validatorCreatePlayer,
    validatorPlayer,
    validatorUpdatePlayer,
    validatorDeletePlayer,
    validatorDisablePlayer,
    validatorEnablePlayer,
} = require('../../validators/tenant/tenant_players')

const {
    CreatePlayer,
    Players,
    Player,
    UpdatePlayer,
    DeletePlayer,
    DisablePlayer,
    EnablePlayer,
    PlayersAll,
    PlayersCount,
} = require('../../controllers/tenant/tenant_players')

const authMiddleware = require('../../middleware/session_tenant')

/* Rutas protegidas que requieren autenticación */
/**
 * @swagger
 * tags:
 *   name: Tenant_Players
 *   description: API para la gestión de jugadores
 */

// Ruta para crear un nuevo jugador
/**
 * @swagger
 * /tenant_players/CreatePlayer:
 *   post:
 *     summary: Crear un nuevo jugador
 *     description: Esta ruta permite crear un nuevo jugador con los detalles especificados.
 *     tags: [Tenant_Players]
 *     requestBody:
 *       required: true
 *       content:
 *         application/json:
 *           schema:
 *             type: object
 *             properties:
 *               name:
 *                 type: string
 *                 example: Lionel Messi
 *               team_guid:
 *                 type: uuid
 *                 example: 495db854-849b-41cd-b6b9-9c84824c92ba
 *               position:
 *                 type: string
 *                 example: Forward
 *     responses:
 *       201:
 *         description: Jugador creado exitosamente
 *         content:
 *           application/json:
 *             schema:
 *               $ref: '#/components/responses/success'
 *       400:
 *         description: Error en los datos enviados
 *         content:
 *           application/json:
 *             schema:
 *               $ref: '#/components/responses/BadRequest'
 *       403:
 *         description: Error de autenticación
 *         content:
 *           application/json:
 *             schema:
 *               $ref: '#/components/responses/UnauthorizedError'
 *       409:
 *         description: El jugador ya existe
 *         content:
 *           application/json:
 *             schema:
 *               $ref: '#/components/responses/ConflictError'
 *       500:
 *         description: Error en el servidor
 *         content:
 *           application/json:
 *             schema:
 *               $ref: '#/components/responses/InternalServerError'
 *     security:
 *        - bearerAuth: []
 */
router.post(
    '/CreatePlayer',
    authMiddleware,
    validatorCreatePlayer(),
    CreatePlayer
)

// Ruta para obtener todos los jugadores activos
/**
 * @swagger
 * /tenant_players/Players:
 *   get:
 *     summary: Obtener todos los jugadores activos
 *     tags: [Tenant_Players]
 *     responses:
 *       200:
 *         description: Jugadores obtenidos exitosamente
 *         content:
 *           application/json:
 *             schema:
 *               $ref: '#/components/responses/success'
 *       403:
 *         description: Error de autenticación
 *         content:
 *           application/json:
 *             schema:
 *               $ref: '#/components/responses/UnauthorizedError'
 *       500:
 *         description: Error en el servidor
 *         content:
 *           application/json:
 *             schema:
 *               $ref: '#/components/responses/InternalServerError'
 *     security:
 *        - bearerAuth: []
 */
router.get('/Players', authMiddleware, Players)

// Ruta para obtener un jugador por GUID
/**
 * @swagger
 * /tenant_players/Player/{guid}:
 *   get:
 *     summary: Obtener un jugador por GUID
 *     tags: [Tenant_Players]
 *     parameters:
 *       - in: path
 *         name: guid
 *         schema:
 *           type: string
 *           format: uuid
 *         required: true
 *         description: El GUID del jugador
 *     responses:
 *       200:
 *         description: Jugador obtenido exitosamente
 *         content:
 *           application/json:
 *             schema:
 *               $ref: '#/components/responses/success'
 *       403:
 *         description: Error de autenticación
 *         content:
 *           application/json:
 *             schema:
 *               $ref: '#/components/responses/UnauthorizedError'
 *       404:
 *         description: Jugador no encontrado
 *         content:
 *           application/json:
 *             schema:
 *               $ref: '#/components/responses/NotFoundError'
 *       500:
 *         description: Error en el servidor
 *         content:
 *           application/json:
 *             schema:
 *               $ref: '#/components/responses/InternalServerError'
 *     security:
 *        - bearerAuth: []
 */
router.get('/Player/:guid', authMiddleware, validatorPlayer(), Player)

// Ruta para actualizar un jugador
/**
 * @swagger
 * /tenant_players/UpdatePlayer/{guid}:
 *   patch:
 *     summary: Actualizar un jugador
 *     tags: [Tenant_Players]
 *     requestBody:
 *       required: true
 *       content:
 *         application/json:
 *           schema:
 *             type: object
 *             properties:
 *               name:
 *                 type: string
 *                 example: Lionel Messi
 *               team_guid:
 *                 type: uuid
 *                 example: 495db854-849b-41cd-b6b9-9c84824c92ba
 *               position:
 *                 type: string
 *                 example: Forward
 *     responses:
 *       200:
 *         description: Jugador actualizado exitosamente
 *         content:
 *           application/json:
 *             schema:
 *               $ref: '#/components/responses/success'
 *       400:
 *         description: Error en los datos enviados
 *         content:
 *           application/json:
 *             schema:
 *               $ref: '#/components/responses/BadRequest'
 *       403:
 *         description: Error de autenticación
 *         content:
 *           application/json:
 *             schema:
 *               $ref: '#/components/responses/UnauthorizedError'
 *       404:
 *         description: Jugador no encontrado
 *         content:
 *           application/json:
 *             schema:
 *               $ref: '#/components/responses/NotFoundError'
 *       500:
 *         description: Error en el servidor
 *         content:
 *           application/json:
 *             schema:
 *               $ref: '#/components/responses/InternalServerError'
 *     security:
 *        - bearerAuth: []
 */
router.patch(
    '/UpdatePlayer/:guid',
    authMiddleware,
    validatorUpdatePlayer(),
    UpdatePlayer
)

// Ruta para eliminar un jugador
/**
 * @swagger
 * /tenant_players/DeletePlayer/{guid}:
 *   delete:
 *     summary: Eliminar un jugador
 *     tags: [Tenant_Players]
 *     parameters:
 *       - in: path
 *         name: guid
 *         schema:
 *           type: string
 *           format: uuid
 *         required: true
 *         description: El GUID del jugador
 *     responses:
 *       200:
 *         description: Jugador eliminado exitosamente
 *         content:
 *           application/json:
 *             schema:
 *               $ref: '#/components/responses/success'
 *       400:
 *         description: Error en los datos enviados
 *         content:
 *           application/json:
 *             schema:
 *               $ref: '#/components/responses/BadRequest'
 *       403:
 *         description: Error de autenticación
 *         content:
 *           application/json:
 *             schema:
 *               $ref: '#/components/responses/UnauthorizedError'
 *       404:
 *         description: Jugador no encontrado
 *         content:
 *           application/json:
 *             schema:
 *               $ref: '#/components/responses/NotFoundError'
 *       500:
 *         description: Error en el servidor
 *         content:
 *           application/json:
 *             schema:
 *               $ref: '#/components/responses/InternalServerError'
 *     security:
 *        - bearerAuth: []
 */
router.delete(
    '/DeletePlayer/:guid',
    authMiddleware,
    validatorDeletePlayer(),
    DeletePlayer
)

// Ruta para deshabilitar un jugador
/**
 * @swagger
 * /tenant_players/DisablePlayer/{guid}:
 *   patch:
 *     summary: Deshabilitar un jugador
 *     tags: [Tenant_Players]
 *     requestBody:
 *       required: true
 *       content:
 *         application/json:
 *           schema:
 *             type: object
 *             properties:
 *               guid:
 *                 type: uuid
 *                 example: 495db854-849b-41cd-b6b9-9c84824c92ba
 *     responses:
 *       200:
 *         description: Jugador deshabilitado exitosamente
 *         content:
 *           application/json:
 *             schema:
 *               $ref: '#/components/responses/success'
 *       400:
 *         description: Error en los datos enviados
 *         content:
 *           application/json:
 *             schema:
 *               $ref: '#/components/responses/BadRequest'
 *       403:
 *         description: Error de autenticación
 *         content:
 *           application/json:
 *             schema:
 *               $ref: '#/components/responses/UnauthorizedError'
 *       404:
 *         description: Jugador no encontrado
 *         content:
 *           application/json:
 *             schema:
 *               $ref: '#/components/responses/NotFoundError'
 *       500:
 *         description: Error en el servidor
 *         content:
 *           application/json:
 *             schema:
 *               $ref: '#/components/responses/InternalServerError'
 *     security:
 *        - bearerAuth: []
 */
router.patch(
    '/DisablePlayer/:guid',
    authMiddleware,
    validatorDisablePlayer(),
    DisablePlayer
)

// Ruta para habilitar un jugador
/**
 * @swagger
 * /tenant_players/EnablePlayer/{guid}:
 *   patch:
 *     summary: Habilitar un jugador
 *     tags: [Tenant_Players]
 *     requestBody:
 *       required: true
 *       content:
 *         application/json:
 *           schema:
 *             type: object
 *             properties:
 *               guid:
 *                 type: uuid
 *                 example: 495db854-849b-41cd-b6b9-9c84824c92ba
 *     responses:
 *       200:
 *         description: Jugador habilitado exitosamente
 *         content:
 *           application/json:
 *             schema:
 *               $ref: '#/components/responses/success'
 *       400:
 *         description: Error en los datos enviados
 *         content:
 *           application/json:
 *             schema:
 *               $ref: '#/components/responses/BadRequest'
 *       403:
 *         description: Error de autenticación
 *         content:
 *           application/json:
 *             schema:
 *               $ref: '#/components/responses/UnauthorizedError'
 *       404:
 *         description: Jugador no encontrado
 *         content:
 *           application/json:
 *             schema:
 *               $ref: '#/components/responses/NotFoundError'
 *       500:
 *         description: Error en el servidor
 *         content:
 *           application/json:
 *             schema:
 *               $ref: '#/components/responses/InternalServerError'
 *     security:
 *        - bearerAuth: []
 */
router.patch(
    '/EnablePlayer/:guid',
    authMiddleware,
    validatorEnablePlayer(),
    EnablePlayer
)

// Ruta para obtener todos los jugadores
/**
 * @swagger
 * /tenant_players/PlayersAll:
 *   get:
 *     summary: Obtener todos los jugadores
 *     tags: [Tenant_Players]
 *     responses:
 *       200:
 *         description: players obtenidos exitosamente
 *         content:
 *           application/json:
 *             schema:
 *               $ref: '#/components/responses/success'
 *       403:
 *         description: Error de autenticación
 *         content:
 *           application/json:
 *             schema:
 *               $ref: '#/components/responses/UnauthorizedError'
 *       500:
 *         description: Error en el servidor
 *         content:
 *           application/json:
 *             schema:
 *               $ref: '#/components/responses/InternalServerError'
 *     security:
 *        - bearerAuth: []
 */
router.get('/PlayersAll', authMiddleware, PlayersAll)

// Ruta para contar jugadores
/**
 * @swagger
 * /tenant_players/PlayersCount:
 *   get:
 *     summary: Contar jugadores
 *     tags: [Tenant_Players]
 *     responses:
 *       200:
 *         description: Contador de jugadores obtenido exitosamente
 *         content:
 *           application/json:
 *             schema:
 *               $ref: '#/components/responses/success'
 *       403:
 *         description: Error de autenticación
 *         content:
 *           application/json:
 *             schema:
 *               $ref: '#/components/responses/UnauthorizedError'
 *       500:
 *         description: Error en el servidor
 *         content:
 *           application/json:
 *             schema:
 *               $ref: '#/components/responses/InternalServerError'
 *     security:
 *        - bearerAuth: []
 */
router.get('/PlayersCount', authMiddleware, PlayersCount)

module.exports = router
