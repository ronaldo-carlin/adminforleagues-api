const { check } = require('express-validator')
const validateResults = require('../../utils/handleValidators')
const AccessControl = require('../../models/general/access_control')

// Validaciones para crear un control de acceso
const ValidatorCreateAccessControl = [
    check('role_guid').isUUID().withMessage('El guid del rol es requerido.'),
    check('module_guid')
        .isUUID()
        .withMessage('El guid del módulo es requerido.'),
    check('permissions_guid')
        .isUUID()
        .withMessage('El guid del permiso es requerido.'),
    check('plan_guid').isUUID().withMessage('El guid del plan es requerido.'),

    (req, res, next) => {
        return validateResults(req, res, next)
    },
]

// Validaciones para obtener un control de acceso
const ValidatorAccessControl = [
    // checamos el guid que sea tipo guid
    check('guid').isUUID().withMessage('El guid es requerido.'),

    (req, res, next) => {
        return validateResults(req, res, next)
    },
]

// Validaciones para actualizar un control de acceso
const ValidatorUpdateAccessControl = [
    check('guid').isUUID().withMessage('El guid es requerido.'),
    check('role_guid').isUUID().withMessage('El guid del rol es requerido.'),
    check('module_guid')
        .isUUID()
        .withMessage('El guid del módulo es requerido.'),
    check('permissions_guid')
        .isUUID()
        .withMessage('El guid del permiso es requerido.'),
    check('plan_guid').isUUID().withMessage('El guid del plan es requerido.'),

    (req, res, next) => {
        return validateResults(req, res, next)
    },
]

// Validaciones para eliminar un control de acceso
const ValidatorDeleteAccessControl = [
    check('guid').isUUID().withMessage('El guid es requerido.'),

    (req, res, next) => {
        return validateResults(req, res, next)
    },
]

// Validaciones para deshabilitar un control de acceso
const ValidatorDisableAccessControl = [
    check('guid').isUUID().withMessage('El guid es requerido.'),

    (req, res, next) => {
        return validateResults(req, res, next)
    },
]

// Validaciones para habilitar un control de acceso
const ValidatorEnableAccessControl = [
    check('guid').isUUID().withMessage('El guid es requerido.'),

    (req, res, next) => {
        return validateResults(req, res, next)
    },
]

module.exports = {
    ValidatorCreateAccessControl,
    ValidatorAccessControl,
    ValidatorUpdateAccessControl,
    ValidatorDeleteAccessControl,
    ValidatorDisableAccessControl,
    ValidatorEnableAccessControl,
}
