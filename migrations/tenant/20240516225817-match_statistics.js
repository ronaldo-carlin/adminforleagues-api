module.exports = {
    async up(queryInterface, Sequelize) {
        // entidad para las estadisticas de los partidos
        await queryInterface.createTable('match_statistics', {
            // identificador de la tabla
            id: {
                type: Sequelize.INTEGER,
                primaryKey: true,
                autoIncrement: true,
                allowNull: false,
            },
            // identificador unico de la estadistica del partido
            guid: {
                type: Sequelize.UUID,
                defaultValue: Sequelize.UUIDV4,
                unique: true,
                allowNull: false,
            },
            // identificador del partido
            match_guid: {
                type: Sequelize.UUID,
            },
            // identificador del equipo ganador
            winner_team_guid: {
                type: Sequelize.UUID,
            },
            // identificador del equipo local
            fouls_home_team: {
                type: Sequelize.INTEGER,
            },
            // identificador del equipo visitante
            fouls_away_team: {
                type: Sequelize.INTEGER,
            },
            // cantidad de tiros de esquina del equipo local
            corners_home_team: {
                type: Sequelize.INTEGER,
            },
            // cantidad de tiros de esquina del equipo visitante
            corners_away_team: {
                type: Sequelize.INTEGER,
            },
            // cantidad de fueras de lugar del equipo local
            offsides_home_team: {
                type: Sequelize.INTEGER,
            },
            // cantidad de fueras de lugar del equipo visitante
            offsides_away_team: {
                type: Sequelize.INTEGER,
            },
            // cantidad de pases del equipo local
            passes_home_team: {
                type: Sequelize.INTEGER,
            },
            // cantidad de pases del equipo visitante
            passes_away_team: {
                type: Sequelize.INTEGER,
            },
            // porcentaje de pases completados del equipo local
            pass_accuracy_home_team: {
                type: Sequelize.INTEGER,
            },
            // porcentaje de pases completados del equipo visitante
            pass_accuracy_away_team: {
                type: Sequelize.INTEGER,
            },
            // cantidad de tiros del equipo local
            shots_home_team: {
                type: Sequelize.INTEGER,
            },
            // cantidad de tiros del equipo visitante
            shots_away_team: {
                type: Sequelize.INTEGER,
            },
            // cantidad de tiros al arco del equipo local
            shots_on_target_home_team: {
                type: Sequelize.INTEGER,
            },
            // cantidad de tiros al arco del equipo visitante
            shots_on_target_away_team: {
                type: Sequelize.INTEGER,
            },
            // porcentaje de posesion del equipo local
            possession_home_team: {
                type: Sequelize.INTEGER,
            },
            // porcentaje de posesion del equipo visitante
            possession_away_team: {
                type: Sequelize.INTEGER,
            },
            // cantidad de goles del equipo local
            home_team_goals: {
                type: Sequelize.INTEGER,
            },
            // cantidad de goles del equipo visitante
            away_team_goals: {
                type: Sequelize.INTEGER,
            },
            // cantidad de goles en penales del equipo local
            penalty_home_team_score: {
                type: Sequelize.INTEGER,
            },
            // cantidad de goles en penales del equipo visitante
            penalty_away_team_score: {
                type: Sequelize.INTEGER,
            },
            // cantidad de tarjetas amarillas del equipo local
            yellow_cards_home_team: {
                type: Sequelize.INTEGER,
            },
            // cantidad de tarjetas amarillas del equipo visitante
            yellow_cards_away_team: {
                type: Sequelize.INTEGER,
            },
            // cantidad de tarjetas rojas del equipo local
            red_cards_home_team: {
                type: Sequelize.INTEGER,
            },
            // cantidad de tarjetas rojas del equipo visitante
            red_cards_away_team: {
                type: Sequelize.INTEGER,
            },
            // estado del registro
            is_active: {
                type: Sequelize.BOOLEAN,
                defaultValue: 1, // valor por defecto
            },
            // usuario que creo el registro
            created_by: {
                type: Sequelize.UUID,
            },
            // fecha de creacion
            created_at: {
                type: Sequelize.DATE,
                defaultValue: Sequelize.literal('CURRENT_TIMESTAMP'),
            },
            // usuario que actualizo el registro
            updated_by: {
                type: Sequelize.UUID,
            },
            // fecha de actualizacion
            updated_at: {
                type: Sequelize.DATE,
            },
            // usuario que elimino el registro
            deleted_by: {
                type: Sequelize.UUID,
            },
            // fecha de eliminacion
            deleted_at: {
                type: Sequelize.DATE,
            },
        })

        // indices
        await queryInterface.addIndex('match_statistics', ['guid'], {
            unique: true,
            name: 'unique_match_statistics_guid',
        })

        await queryInterface.addIndex('match_statistics', ['match_guid'], {
            name: 'idx_match_statistics_match_guid',
        })

        await queryInterface.addIndex(
            'match_statistics',
            ['winner_team_guid'],
            {
                name: 'idx_match_statistics_winner_team_guid',
            }
        )

        // llaves foranea
        await queryInterface.addConstraint('match_statistics', {
            fields: ['match_guid'],
            type: 'foreign key',
            name: 'fk_match_statistics_match_guid',
            references: {
                table: 'matches',
                field: 'guid',
            },
            onUpdate: 'CASCADE',
            onDelete: 'NO ACTION',
        })

        await queryInterface.addConstraint('match_statistics', {
            fields: ['winner_team_guid'],
            type: 'foreign key',
            name: 'fk_match_statistics_winner_team_guid',
            references: {
                table: 'teams',
                field: 'guid',
            },
            onUpdate: 'CASCADE',
            onDelete: 'NO ACTION',
        })
    },

    async down(queryInterface, Sequelize) {
        await queryInterface.removeConstraint(
            'match_statistics',
            'fk_match_statistics_winner_team_guid'
        )
        await queryInterface.removeConstraint(
            'match_statistics',
            'fk_match_statistics_match_guid'
        )
        await queryInterface.removeIndex(
            'match_statistics',
            'idx_match_statistics_winner_team_guid'
        )
        await queryInterface.removeIndex(
            'match_statistics',
            'idx_match_statistics_match_guid'
        )
        await queryInterface.removeIndex(
            'match_statistics',
            'unique_match_statistics_guid'
        )
        await queryInterface.dropTable('match_statistics')
    },
}
