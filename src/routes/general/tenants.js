const express = require('express')
const { matchedData } = require('express-validator')
const router = express.Router()

// validaciones
const {
    ValidatorCreateTenant,
    ValidatorUpdateTenant,
    ValidatorDeleteTenant,
    ValidatorDisableTenant,
    ValidatorEnableTenant,
} = require('../../validators/general/tenants')

// Importa los controladores
const {
    CreateTenant,
    Tenants,
    Tenant,
    UpdateTenant,
    DeleteTenant,
    DisableTenant,
    EnableTenant,
    TenantsAll,
    CountAllTenants,
} = require('../../controllers/general/tenants')

// Middleware de autenticación
const authMiddleware = require('../../middleware/session')

// Rutas protegidas que requieren autenticación
/**
 * @swagger
 * tags:
 *   name: Tenants
 *   description: API para la gestión de tenants
 */

// Ruta para crear un nuevo tenant
/**
 * @swagger
 * /tenants/CreateTenant:
 *   post:
 *     summary: Crear un nuevo tenant
 *     description: Esta ruta permite crear un nuevo tenant con nombre y descripción.
 *     tags: [Tenants]
 *     requestBody:
 *       required: true
 *       content:
 *         application/json:
 *           schema:
 *             type: object
 *             properties:
 *               company_name:
 *                 type: string
 *                 example: Empresa XYZ
 *               email:
 *                 type: string
 *                 example: user@example.com
 *               phone:
 *                 type: string
 *                 example: 1234567890
 *               address:
 *                 type: string
 *                 example: Calle 123
 *               city:
 *                 type: string
 *                 example: Veracruz
 *               state:
 *                 type: string
 *                 example: Veracruz
 *               country:
 *                 type: string
 *                 example: México
 *     responses:
 *       201:
 *         description: Tenant creado exitosamente
 *         content:
 *           application/json:
 *             schema:
 *               $ref: '#/components/responses/success'
 *       400:
 *         description: Error en los datos enviados
 *         content:
 *           application/json:
 *             schema:
 *               $ref: '#/components/responses/BadRequest'
 *       403:
 *         description: Error de autenticación
 *         content:
 *           application/json:
 *             schema:
 *               $ref: '#/components/responses/UnauthorizedError'
 *       409:
 *         description: Tenant ya existe
 *         content:
 *           application/json:
 *             schema:
 *               $ref: '#/components/responses/ConflictError'
 *       500:
 *         description: Error en el servidor
 *         content:
 *           application/json:
 *             schema:
 *               $ref: '#/components/responses/InternalServerError'
 *     security:
 *        - bearerAuth: []
 */
router.post(
    '/CreateTenant',
    authMiddleware,
    ValidatorCreateTenant,
    CreateTenant
)

// Ruta para obtener todos los tenants activos
/**
 * @swagger
 *  /tenants/Tenants:
 *   get:
 *     summary: Obtener todos los tenants activos
 *     tags: [Tenants]
 *     responses:
 *       201:
 *         description: Tenants obtenidos exitosamente
 *         content:
 *           application/json:
 *             schema:
 *               $ref: '#/components/responses/success'
 *       403:
 *         description: Error de autenticación
 *         content:
 *           application/json:
 *             schema:
 *               $ref: '#/components/responses/UnauthorizedError'
 *       500:
 *         description: Error en el servidor
 *         content:
 *           application/json:
 *             schema:
 *               $ref: '#/components/responses/InternalServerError'
 *     security:
 *        - bearerAuth: []
 */
router.get('/Tenants', authMiddleware, Tenants)

// Ruta para obtener un tenant
/**
 * @swagger
 * /tenants/Tenant:
 *   get:
 *     summary: Obtiene un tenant por ID
 *     tags: [Tenants]
 *     parameters:
 *       - in: path
 *         name: guid
 *         schema:
 *           type: uuid
 *           format: uuid
 *         required: true
 *         description: El GUID de la tenant
 *     responses:
 *       201:
 *         description: Tenant obtenido exitosamente
 *         content:
 *           application/json:
 *             schema:
 *               $ref: '#/components/responses/success'
 *       403:
 *         description: Error de autenticación
 *         content:
 *           application/json:
 *             schema:
 *               $ref: '#/components/responses/UnauthorizedError'
 *       404:
 *        description: Tenant no encontrado
 *        content:
 *          application/json:
 *            schema:
 *              $ref: '#/components/responses/NotFoundError'
 *       500:
 *         description: Error en el servidor
 *         content:
 *           application/json:
 *             schema:
 *               $ref: '#/components/responses/InternalServerError'
 *     security:
 *        - bearerAuth: []
 */
router.get('/Tenant/:guid', authMiddleware, Tenant)

// Ruta para actualizar un tenant
/**
 * @swagger
 * /tenants/UpdateTenant:
 *   patch:
 *     summary: Actualizar un tenant
 *     tags: [Tenants]
 *     parameters:
 *       - in: path
 *         name: guid
 *         schema:
 *           type: uuid
 *           format: uuid
 *         required: true
 *         description: El GUID de la tenant
 *     requestBody:
 *       required: true
 *       content:
 *         application/json:
 *           schema:
 *             type: object
 *             properties:
 *               company_name:
 *                 type: string
 *                 example: Empresa XYZ
 *               email:
 *                 type: string
 *                 example: user@example.com
 *               phone:
 *                 type: string
 *                 example: 1234567890
 *               address:
 *                 type: string
 *                 example: Calle 123
 *               city:
 *                 type: string
 *                 example: Veracruz
 *               state:
 *                 type: string
 *                 example: Veracruz
 *               country:
 *                 type: string
 *                 example: México
 *     responses:
 *       201:
 *         description: Tenant actualizado exitosamente
 *         content:
 *           application/json:
 *             schema:
 *               $ref: '#/components/responses/success'
 *       400:
 *         description: Error en los datos enviados
 *         content:
 *           application/json:
 *             schema:
 *               $ref: '#/components/responses/BadRequest'
 *       403:
 *         description: Error de autenticación
 *         content:
 *           application/json:
 *             schema:
 *               $ref: '#/components/responses/UnauthorizedError'
 *       404:
 *        description: Tenant no encontrado
 *        content:
 *          application/json:
 *            schema:
 *              $ref: '#/components/responses/NotFoundError'
 *       500:
 *         description: Error en el servidor
 *         content:
 *           application/json:
 *             schema:
 *               $ref: '#/components/responses/InternalServerError'
 *     security:
 *        - bearerAuth: []
 */
router.patch(
    '/UpdateTenant/:guid',
    authMiddleware,
    ValidatorUpdateTenant,
    UpdateTenant
)

// Ruta para eliminar un tenant
/**
 * @swagger
 * /tenants/DeleteTenant:
 *   delete:
 *     summary: Eliminar un tenant
 *     tags: [Tenants]
 *     parameters:
 *       - in: path
 *         name: guid
 *         schema:
 *           type: uuid
 *           format: uuid
 *         required: true
 *         description: El GUID de la tenant
 *     responses:
 *       201:
 *         description: Tenant eliminado exitosamente
 *         content:
 *           application/json:
 *             schema:
 *               $ref: '#/components/responses/success'
 *       400:
 *         description: Error en los datos enviados
 *         content:
 *           application/json:
 *             schema:
 *               $ref: '#/components/responses/BadRequest'
 *       403:
 *         description: Error de autenticación
 *         content:
 *           application/json:
 *             schema:
 *               $ref: '#/components/responses/UnauthorizedError'
 *       404:
 *        description: Tenant no encontrado
 *        content:
 *          application/json:
 *            schema:
 *              $ref: '#/components/responses/NotFoundError'
 *       500:
 *         description: Error en el servidor
 *         content:
 *           application/json:
 *             schema:
 *               $ref: '#/components/responses/InternalServerError'
 *     security:
 *        - bearerAuth: []
 */
router.delete(
    '/DeleteTenant/:guid',
    authMiddleware,
    ValidatorDeleteTenant,
    DeleteTenant
)

// Ruta para desactivar un tenant
/**
 * @swagger
 * /tenants/DisableTenant:
 *   patch:
 *     summary: Desactivar un tenant
 *     tags: [Tenants]
 *     parameters:
 *       - in: path
 *         name: guid
 *         schema:
 *           type: uuid
 *           format: uuid
 *         required: true
 *         description: El GUID de la tenant
 *     responses:
 *       201:
 *         description: Tenant desactivado exitosamente
 *         content:
 *           application/json:
 *             schema:
 *               $ref: '#/components/responses/success'
 *       400:
 *         description: Error en los datos enviados
 *         content:
 *           application/json:
 *             schema:
 *               $ref: '#/components/responses/BadRequest'
 *       403:
 *         description: Error de autenticación
 *         content:
 *           application/json:
 *             schema:
 *               $ref: '#/components/responses/UnauthorizedError'
 *       404:
 *        description: Tenant no encontrado
 *        content:
 *          application/json:
 *            schema:
 *              $ref: '#/components/responses/NotFoundError'
 *       500:
 *         description: Error en el servidor
 *         content:
 *           application/json:
 *             schema:
 *               $ref: '#/components/responses/InternalServerError'
 *     security:
 *        - bearerAuth: []
 */
router.patch(
    '/DisableTenant/:guid',
    authMiddleware,
    ValidatorDisableTenant,
    DisableTenant
)

// Ruta para activar un tenant
/**
 * @swagger
 * /tenants/EnableTenant:
 *   patch:
 *     summary: Activar un tenant
 *     tags: [Tenants]
 *     parameters:
 *       - in: path
 *         name: guid
 *         schema:
 *           type: uuid
 *           format: uuid
 *         required: true
 *         description: El GUID de la tenant
 *     responses:
 *       201:
 *         description: Tenant activado exitosamente
 *         content:
 *           application/json:
 *             schema:
 *               $ref: '#/components/responses/success'
 *       400:
 *         description: Error en los datos enviados
 *         content:
 *           application/json:
 *             schema:
 *               $ref: '#/components/responses/BadRequest'
 *       403:
 *         description: Error de autenticación
 *         content:
 *           application/json:
 *             schema:
 *               $ref: '#/components/responses/UnauthorizedError'
 *       404:
 *        description: Tenant no encontrado
 *        content:
 *          application/json:
 *            schema:
 *              $ref: '#/components/responses/NotFoundError'
 *       500:
 *         description: Error en el servidor
 *         content:
 *           application/json:
 *             schema:
 *               $ref: '#/components/responses/InternalServerError'
 *     security:
 *        - bearerAuth: []
 */
router.patch(
    '/EnableTenant/:guid',
    authMiddleware,
    ValidatorEnableTenant,
    EnableTenant
)

// Ruta para obtener todos los tenants
/**
 * @swagger
 * /tenants/TenantsAll:
 *   get:
 *     summary: Obtener todos los tenants
 *     tags: [Tenants]
 *     responses:
 *       201:
 *         description: Tenants obtenidos exitosamente
 *         content:
 *           application/json:
 *             schema:
 *               $ref: '#/components/responses/success'
 *       400:
 *         description: Error en los datos enviados
 *         content:
 *           application/json:
 *             schema:
 *               $ref: '#/components/responses/BadRequest'
 *       403:
 *         description: Error de autenticación
 *         content:
 *           application/json:
 *             schema:
 *               $ref: '#/components/responses/UnauthorizedError'
 *       500:
 *         description: Error en el servidor
 *         content:
 *           application/json:
 *             schema:
 *               $ref: '#/components/responses/InternalServerError'
 *     security:
 *        - bearerAuth: []
 */
router.get('/TenantsAll', authMiddleware, TenantsAll)

// Ruta para contar todos los tenants
/**
 * @swagger
 * /tenants/TenantsCount:
 *   get:
 *     summary: Contar todos los tenants
 *     tags: [Tenants]
 *     responses:
 *       201:
 *         description: Tenants contados exitosamente
 *         content:
 *           application/json:
 *             schema:
 *               $ref: '#/components/responses/success'
 *       400:
 *         description: Error en los datos enviados
 *         content:
 *           application/json:
 *             schema:
 *               $ref: '#/components/responses/BadRequest'
 *       403:
 *         description: Error de autenticación
 *         content:
 *           application/json:
 *             schema:
 *               $ref: '#/components/responses/UnauthorizedError'
 *       500:
 *         description: Error en el servidor
 *         content:
 *           application/json:
 *             schema:
 *               $ref: '#/components/responses/InternalServerError'
 *     security:
 *        - bearerAuth: []
 */
router.get('/TenantsCount', authMiddleware, CountAllTenants)

module.exports = router
