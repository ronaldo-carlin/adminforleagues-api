const { Op } = require('sequelize')
const { v4: uuidv4 } = require('uuid')
const { matchedData } = require('express-validator')
const { handleHttpSuccess } = require('../../utils/handleSuccess')
const { handleHttpError } = require('../../utils/handleError')
const { handleHttpCount } = require('../../utils/handleCount')
const {
    getSequelize,
    getModulesModel,
} = require('../../helpers/sequelizeHelper')

/**
 * Este controlador es el encargado de registrar un módulo
 * @param {*} req
 * @param {*} res
 */
const CreateModule = async (req, res) => {
    try {
        const sequelize = getSequelize(req)
        const ModuleModel = getModulesModel(sequelize)
        const data = matchedData(req)
        const created_by = req.user.guid
        const created_at = new Date()
        const guid = await uuidv4()
        data.created_by = created_by
        data.created_at = created_at
        data.guid = guid

        const module = await ModuleModel.create(data)
        return handleHttpSuccess(res, 'Módulo creado.', 201, module)
    } catch (error) {
        console.log('Error al crear el módulo:', error)
        handleHttpError(res, 'Error al crear el módulo.', 403, error)
    }
}

/**
 * Este controlador es el encargado de obtener los módulos activos
 * @param {*} req
 * @param {*} res
 */
const Modules = async (req, res) => {
    try {
        const sequelize = getSequelize(req)
        const ModuleModel = getModulesModel(sequelize)

        const modules = await ModuleModel.findAll({
            where: { is_active: 1 },
        })

        if (!modules.length) {
            handleHttpError(res, 'No se encontraron módulos.', 404)
            return
        }

        handleHttpSuccess(res, 'Módulos encontrados.', 201, modules)
    } catch (error) {
        console.log('Error obteniendo módulos:', error)
        handleHttpError(res, 'Error obteniendo módulos.', 403, error)
    }
}

/**
 * Este controlador es el encargado de obtener un módulo por GUID
 * @param {*} req
 * @param {*} res
 */
const Module = async (req, res) => {
    try {
        const sequelize = getSequelize(req)
        const ModuleModel = getModulesModel(sequelize)
        const { guid } = req.params

        const module = await ModuleModel.findOne({
            where: { guid },
        })

        if (!module) {
            handleHttpError(res, 'El módulo no existe.', 404)
            return
        }

        if (module.is_active == 0) {
            handleHttpError(res, 'El módulo ha sido deshabilitado.', 401)
            return
        }

        handleHttpSuccess(res, 'Módulo encontrado.', 201, module)
    } catch (error) {
        console.log('Error obteniendo módulo:', error)
        handleHttpError(res, 'Error obteniendo módulo.', 403, error)
    }
}

/**
 * Este controlador es el encargado de actualizar un módulo
 * @param {*} req
 * @param {*} res
 */
const UpdateModule = async (req, res) => {
    try {
        const sequelize = getSequelize(req)
        const ModuleModel = getModulesModel(sequelize)
        const { guid } = req.params
        const data = matchedData(req)
        const updated_by = req.user.guid
        const updated_at = new Date()
        data.updated_by = updated_by
        data.updated_at = updated_at

        const [updateCount] = await ModuleModel.update(data, {
            where: { guid },
        })

        if (updateCount === 0) {
            return handleHttpError(res, 'El módulo no existe.', 404)
        }

        const module = await ModuleModel.findOne({
            where: { guid },
        })

        if (!module) {
            handleHttpError(res, 'El módulo no existe.', 404)
            return
        }

        if (module.is_active == 0) {
            handleHttpError(res, 'El módulo ha sido deshabilitado.', 401)
            return
        }

        handleHttpSuccess(res, 'Módulo actualizado.', 201, module)
    } catch (error) {
        console.log('Error actualizando módulo:', error)
        handleHttpError(res, 'Error actualizando módulo.', 403, error)
    }
}

/**
 * Este controlador es el encargado de eliminar un módulo
 * @param {*} req
 * @param {*} res
 */
const DeleteModule = async (req, res) => {
    try {
        const sequelize = getSequelize(req)
        const ModuleModel = getModulesModel(sequelize)
        const { guid } = req.params
        const deleted_by = req.user.guid
        const deleted_at = new Date()
        const [updateCount] = await ModuleModel.update(
            {
                is_active: 0,
                deleted_by,
                deleted_at,
            },
            {
                where: { guid },
            }
        )

        if (updateCount === 0) {
            return handleHttpError(res, 'El módulo no existe.', 404)
        }

        handleHttpSuccess(res, 'Módulo eliminado.', 201)
    } catch (error) {
        console.log('Error eliminando módulo:', error)
        handleHttpError(res, 'Error eliminando módulo.', 403, error)
    }
}

/**
 * Este controlador es el encargado de desactivar un módulo
 * @param {*} req
 * @param {*} res
 */
const DisableModule = async (req, res) => {
    try {
        const sequelize = getSequelize(req)
        const ModuleModel = getModulesModel(sequelize)
        const { guid } = req.params
        const updated_by = req.user.guid
        const updated_at = new Date()
        const [updateCount] = await ModuleModel.update(
            {
                is_active: 0,
                updated_by,
                updated_at,
            },
            {
                where: { guid },
            }
        )

        if (updateCount === 0) {
            return handleHttpError(res, 'El módulo no existe.', 404)
        }

        handleHttpSuccess(res, 'Módulo deshabilitado.', 201)
    } catch (error) {
        console.log('Error deshabilitando módulo:', error)
        handleHttpError(res, 'Error deshabilitando módulo.', 403, error)
    }
}

/**
 * Este controlador es el encargado de activar un módulo
 * @param {*} req
 * @param {*} res
 */
const EnableModule = async (req, res) => {
    try {
        const sequelize = getSequelize(req)
        const ModuleModel = getModulesModel(sequelize)
        const { guid } = req.params
        const updated_by = req.user.guid
        const updated_at = new Date()
        const [updateCount] = await ModuleModel.update(
            {
                is_active: 1,
                updated_by,
                updated_at,
            },
            {
                where: { guid },
            }
        )

        if (updateCount === 0) {
            return handleHttpError(res, 'El módulo no existe.', 404)
        }

        handleHttpSuccess(res, 'Módulo habilitado.', 201)
    } catch (error) {
        console.log('Error habilitando módulo:', error)
        handleHttpError(res, 'Error habilitando módulo.', 403, error)
    }
}

/**
 * Este controlador es el encargado de obtener todos los módulos
 * @param {*} req
 * @param {*} res
 */
const ModulesAll = async (req, res) => {
    try {
        const sequelize = getSequelize(req)
        const ModuleModel = getModulesModel(sequelize)

        const modules = await ModuleModel.findAll()

        if (!modules.length) {
            handleHttpError(res, 'No se encontraron módulos.', 404)
            return
        }

        handleHttpSuccess(res, 'Módulos encontrados.', 201, modules)
    } catch (error) {
        console.log('Error obteniendo módulos:', error)
        handleHttpError(res, 'Error obteniendo módulos.', 403, error)
    }
}

/**
 * Este controlador es el encargado de obtener todos los módulos activos
 * @param {*} req
 * @param {*} res
 */
const ModulesCount = async (req, res) => {
    try {
        const sequelize = getSequelize(req)
        const ModuleModel = getModulesModel(sequelize)

        const modules = await ModuleModel.count()

        if (!modules.length) {
            handleHttpError(res, 'No se encontraron módulos.', 404)
            return
        }

        handleHttpCount(res, 'Módulos encontrados.', 201, modules)
    } catch (error) {
        console.log('Error obteniendo módulos:', error)
        handleHttpError(res, 'Error obteniendo módulos.', 403, error)
    }
}

module.exports = {
    CreateModule,
    Modules,
    Module,
    UpdateModule,
    DeleteModule,
    DisableModule,
    EnableModule,
    ModulesAll,
    ModulesCount,
}
