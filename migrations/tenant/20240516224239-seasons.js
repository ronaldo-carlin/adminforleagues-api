module.exports = {
    async up(queryInterface, Sequelize) {
        // entidad para temporadas de los torneos
        await queryInterface.createTable('seasons', {
            // identificador de la temporada
            id: {
                type: Sequelize.INTEGER,
                primaryKey: true,
                autoIncrement: true,
                allowNull: false,
            },
            guid: {
                type: Sequelize.UUID,
                defaultValue: Sequelize.UUIDV4,
                unique: true,
                allowNull: false,
            },
            // nombre de la temporada
            name: {
                type: Sequelize.STRING,
            },
            // descripcion de la temporada
            description: {
                type: Sequelize.TEXT,
            },
            // fecha de inicio de la temporada
            start_date: {
                type: Sequelize.DATE,
            },
            // fecha de finalizacion de la temporada
            end_date: {
                type: Sequelize.DATE,
            },
            // estado del registro
            is_active: {
                type: Sequelize.BOOLEAN,
                defaultValue: 1, // valor por defecto
            },
            // usuario que creo el registro
            created_by: {
                type: Sequelize.Sequelize.UUID,
            },
            // fecha de creacion
            created_at: {
                type: Sequelize.DATE,
                defaultValue: Sequelize.literal('CURRENT_TIMESTAMP'),
            },
            // usuario que actualizo el registro
            updated_by: {
                type: Sequelize.Sequelize.UUID,
            },
            // fecha de actualizacion
            updated_at: {
                type: Sequelize.DATE,
            },
            // usuario que elimino el registro
            deleted_by: {
                type: Sequelize.Sequelize.UUID,
            },
            // fecha de eliminacion
            deleted_at: {
                type: Sequelize.DATE,
            },
        })

        await queryInterface.addIndex('seasons', ['guid'], {
            unique: true,
            name: 'uniq_seasons_guid',
        })
    },

    async down(queryInterface, Sequelize) {
        await queryInterface.removeIndex('seasons', 'uniq_seasons_guid')
        await queryInterface.dropTable('seasons')
    },
}
