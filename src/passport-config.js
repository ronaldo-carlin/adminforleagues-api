const passport = require('passport')
const GoogleStrategy = require('passport-google-oauth20').Strategy

// Configura la estrategia de Google
passport.use(
    new GoogleStrategy(
        {
            clientID: process.env.GOOGLE_CLIENT_ID,
            clientSecret: process.env.GOOGLE_CLIENT_SECRET,
            callbackURL: process.env.GOOGLE_CALLBACK_URL,
        },
        function (accessToken, refreshToken, profile, cb) {
            // Aquí, busca o crea un usuario basado en la información de `profile`
            User.findOrCreate({ googleId: profile.id }, function (err, user) {
                return cb(err, user)
            })
        }
    )
)

module.exports = passport
