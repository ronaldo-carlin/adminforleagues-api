module.exports = {
    async up(queryInterface, Sequelize) {
        // entidad para los usuarios que utilizaran la plataforma
        await queryInterface.createTable('users', {
            // id
            id: {
                type: Sequelize.INTEGER,
                primaryKey: true,
                autoIncrement: true,
                allowNull: false,
            },
            // guid
            guid: {
                type: Sequelize.UUID,
                defaultValue: Sequelize.UUIDV4, // Genera un UUID por defecto
                unique: true,
                allowNull: false,
            },
            // id del tenant
            tenant_guid: {
                type: Sequelize.UUID,
                allowNull: true,
            },
            // id del rol
            role_guid: {
                type: Sequelize.UUID,
            },
            // nombre de usuario
            username: {
                type: Sequelize.STRING(256),
            },
            // nombre
            first_name: {
                type: Sequelize.STRING(256),
            },
            // apellido
            last_name: {
                type: Sequelize.STRING(256),
            },
            // correo electronico
            email: {
                type: Sequelize.TEXT,
                allowNull: false,
                unique: true,
            },
            // contraseña
            password: {
                type: Sequelize.TEXT,
                allowNull: false,
            },
            // telefono
            phone: {
                type: Sequelize.STRING(15),
            },
            // verificacion del correo
            isVerified: {
                type: Sequelize.BOOLEAN,
                defaultValue: 0, // valor por defecto
            },
            // imagen que sube el usuario
            image_path: {
                type: Sequelize.TEXT,
            },
            // imagen de google
            picture: {
                type: Sequelize.TEXT,
            },
            // pais
            country: {
                type: Sequelize.STRING(256),
            },
            // estado
            state: {
                type: Sequelize.STRING(256),
            },
            // ciudad
            city: {
                type: Sequelize.STRING(256),
            },
            // id de google
            google_id: {
                type: Sequelize.TEXT,
            },
            // id de facebook
            facebook_id: {
                type: Sequelize.TEXT,
            },
            // verificacion de dos factores
            two_factor_auth: {
                type: Sequelize.BOOLEAN,
                defaultValue: 0, // valor por defecto
            },
            // boletín semanal
            weeklyNewsletter: {
                type: Sequelize.BOOLEAN,
                defaultValue: 0, // valor por defecto
            },
            // correos electronicos del ciclo de vida
            lifecycleEmails: {
                type: Sequelize.BOOLEAN,
                defaultValue: 0, // valor por defecto
            },
            // correos electronicos promocionales
            promotionalEmails: {
                type: Sequelize.BOOLEAN,
                defaultValue: 0, // valor por defecto
            },
            // actualizaciones de productos
            productUpdates: {
                type: Sequelize.BOOLEAN,
                defaultValue: 0, // valor por defecto
            },
            // ultima vez que inicio sesion
            last_login: {
                type: Sequelize.DATE,
            },
            // estado del usuario
            is_active: {
                type: Sequelize.BOOLEAN,
                defaultValue: 1, // valor por defecto
            },
            // usuario que creo el registro
            created_by: {
                type: Sequelize.UUID,
            },
            // fecha de creacion
            created_at: {
                type: Sequelize.DATE,
                defaultValue: Sequelize.literal('CURRENT_TIMESTAMP'),
            },
            // usuario que actualizo el registro
            updated_by: {
                type: Sequelize.UUID,
            },
            // fecha de actualizacion
            updated_at: {
                type: Sequelize.DATE,
            },
            // usuario que elimino el registro
            deleted_by: {
                type: Sequelize.UUID,
            },
            // fecha de eliminacion
            deleted_at: {
                type: Sequelize.DATE,
            },
        })

        // Agregar índice único en 'guid'
        await queryInterface.addIndex('users', ['guid'], {
            unique: true,
            name: 'uniq_users_guid',
        })

        // Agregar índice único en 'email'
        await queryInterface.addIndex('users', ['email'], {
            unique: true,
            using: 'HASH',
            name: 'uniq_users_email',
        })

        // Agregar otros índices necesarios
        await queryInterface.addIndex('users', ['tenant_guid'], {
            name: 'idx_users_tenant_guid',
        })

        await queryInterface.addIndex('users', ['role_guid'], {
            name: 'idx_users_role_guid',
        })

        // Agregar constraints después de crear la tabla
        await queryInterface.addConstraint('users', {
            fields: ['tenant_guid'],
            type: 'foreign key',
            name: 'fk_user_tenant',
            references: {
                table: 'tenants',
                field: 'guid',
            },
            onDelete: 'NO ACTION',
            onUpdate: 'NO ACTION',
        })

        await queryInterface.addConstraint('users', {
            fields: ['role_guid'],
            type: 'foreign key',
            name: 'fk_user_role',
            references: {
                table: 'roles',
                field: 'guid',
            },
            onDelete: 'NO ACTION',
            onUpdate: 'NO ACTION',
        })
    },

    async down(queryInterface, Sequelize) {
        await queryInterface.removeConstraint('users', 'fk_user_tenant')
        await queryInterface.removeConstraint('users', 'fk_user_role')
        await queryInterface.removeIndex('users', 'uniq_users_guid')
        await queryInterface.removeIndex('users', 'uniq_users_email')
        await queryInterface.removeIndex('users', 'idx_users_tenant_guid')
        await queryInterface.removeIndex('users', 'idx_users_role_guid')
        await queryInterface.dropTable('users')
    },
}
