const multer = require('multer')

const fs = require('fs')

const storage = multer.diskStorage({
    destination: async function (req, file, cb) {
        var dir = '${__dirname}/../storage/users'

        if (!fs.existsSync(dir)) {
            await fs.mkdirSync(dir)
        }

        const pathStorage = `${__dirname}/../storage/users`

        cb(null, pathStorage)
    },

    filename: function (req, file, cb) {
        const ext = file.originalname.split('.').pop()

        const filename = `file-${Date.now()}.${ext}`

        cb(null, filename)
    },
})

const uploadMiddleware = multer({ storage })

module.exports = uploadMiddleware
