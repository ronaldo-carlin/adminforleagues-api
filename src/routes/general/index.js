const express = require('express')
const fs = require('fs')
const path = require('path')

const router = express.Router()
const PATH_ROUTES = __dirname

const removeExtension = (filename) => {
    return filename.split('.').shift()
}

fs.readdirSync(PATH_ROUTES).forEach((file) => {
    const fullPath = path.join(PATH_ROUTES, file)
    const fileStat = fs.lstatSync(fullPath)

    if (!fileStat.isDirectory()) {
        const name = removeExtension(file)
        if (name !== 'index') {
            router.use(`/${name}`, require(fullPath))
        }
    }
})

module.exports = router
