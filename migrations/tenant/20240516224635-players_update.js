module.exports = {
    async up(queryInterface, Sequelize) {
        // entidad para que los jugadores puedan actualizar sus datos y el administrador pueda aprobarlos
        await queryInterface.createTable('players_update', {
            // id del registro
            id: {
                type: Sequelize.INTEGER,
                primaryKey: true,
                autoIncrement: true,
                allowNull: false,
            },
            // guid del registro
            guid: {
                type: Sequelize.UUID,
                defaultValue: Sequelize.UUIDV4,
                unique: true,
                allowNull: false,
            },
            // guid del jugador
            player_guid: {
                type: Sequelize.UUID,
            },
            // guid del equipo
            team_guid: {
                type: Sequelize.UUID,
            },
            // link unico del registro
            single_link: {
                type: Sequelize.TEXT,
            },
            // nombre del jugador
            first_name: {
                type: Sequelize.TEXT,
            },
            // apellido del jugador
            last_name: {
                type: Sequelize.TEXT,
            },
            // edad del jugador
            age: {
                type: Sequelize.INTEGER,
            },
            // fecha de nacimiento del jugador
            birthdate: {
                type: Sequelize.DATE,
            },
            // numero de camiseta del jugador
            dorsal_number: {
                type: Sequelize.INTEGER,
            },
            // posicion del jugador
            position: {
                type: Sequelize.TEXT,
            },
            // altura del jugador
            height: {
                type: Sequelize.INTEGER,
            },
            // peso del jugador
            weight: {
                type: Sequelize.INTEGER,
            },
            // pie habil del jugador
            skilled_foot: {
                type: Sequelize.TEXT,
            },
            // link de facebook del jugador
            facebook: {
                type: Sequelize.TEXT,
            },
            // link de instagram del jugador
            instagram: {
                type: Sequelize.TEXT,
            },
            // rasgos de personalidad del jugador
            personality_traits: {
                type: Sequelize.TEXT,
            },
            // imagen del jugador
            image_path: {
                type: Sequelize.TEXT,
            },
            // numero de telefono del jugador
            phone_number: {
                type: Sequelize.BIGINT,
            },
            // pais del jugador
            country: {
                type: Sequelize.TEXT,
            },
            // ciudad del jugador
            city: {
                type: Sequelize.TEXT,
            },
            // estado del jugador
            state: {
                type: Sequelize.TEXT,
            },
            // estado del registro
            is_active: {
                type: Sequelize.BOOLEAN,
                defaultValue: 1, // valor por defecto
            },
            // usuario que creo el registro
            created_by: {
                type: Sequelize.UUID,
            },
            // fecha de creacion
            created_at: {
                type: Sequelize.DATE,
                defaultValue: Sequelize.literal('CURRENT_TIMESTAMP'),
            },
            // usuario que actualizo el registro
            updated_by: {
                type: Sequelize.UUID,
            },
            // fecha de actualizacion
            updated_at: {
                type: Sequelize.DATE,
            },
            // usuario que elimino el registro
            deleted_by: {
                type: Sequelize.UUID,
            },
            // fecha de eliminacion
            deleted_at: {
                type: Sequelize.DATE,
            },
        })

        // indices
        await queryInterface.addIndex('players_update', ['guid'], {
            unique: true,
            name: 'unique_players_update_guid',
        })

        await queryInterface.addIndex('players_update', ['player_guid'], {
            name: 'idx_player_guid',
        })

        await queryInterface.addIndex('players_update', ['team_guid'], {
            name: 'idx_team_guid',
        })

        // llaves foraneas
        await queryInterface.addConstraint('players_update', {
            fields: ['player_guid'],
            type: 'foreign key',
            name: 'fk_players_update_player',
            references: {
                table: 'players',
                field: 'guid',
            },
        })

        await queryInterface.addConstraint('players_update', {
            fields: ['team_guid'],
            type: 'foreign key',
            name: 'fk_players_update_team',
            references: {
                table: 'teams',
                field: 'guid',
            },
        })
    },

    async down(queryInterface, Sequelize) {
        await queryInterface.removeConstraint(
            'players_update',
            'fk_players_update_team'
        )
        await queryInterface.removeConstraint(
            'players_update',
            'fk_players_update_player'
        )
        await queryInterface.removeIndex(
            'players_update',
            'unique_players_update_guid'
        )
        await queryInterface.removeIndex('players_update', 'idx_player_guid')
        await queryInterface.removeIndex('players_update', 'idx_team_guid')
        await queryInterface.dropTable('players_update')
    },
}
