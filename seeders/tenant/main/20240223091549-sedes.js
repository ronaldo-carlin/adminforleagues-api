const GUIDsTenant = require('../../guids_tenant')

module.exports = {
    async up(queryInterface, Sequelize) {
        await queryInterface.bulkInsert(
            'sedes',
            [
                {
                    guid: GUIDsTenant.sede1Guid,
                    name: 'Sede 1',
                    description: 'Sede 1',
                    is_active: 1,
                    created_at: new Date(),
                },
            ],
            {}
        )
    },

    async down(queryInterface, Sequelize) {
        await queryInterface.bulkDelete('sedes', null, {})
    },
}
