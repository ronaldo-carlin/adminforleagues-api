// app.js
const express = require('express')
const cors = require('cors')
const app = express()
const swaggerUI = require('swagger-ui-express')
const openApiConfiguration = require('./docs/swagger')
const morganBody = require('morgan-body')
const { dbConnectMySQL } = require('./database/mysql')
const path = require('path')
const session = require('express-session')
const passport = require('./passport-config')
const { SwaggerTheme } = require('swagger-themes')
const theme = new SwaggerTheme()
const swaggerThemes = require('swagger-theme-changer')
const themechanger = swaggerThemes.getTheme('universal-dark')

if (process.env.NODE_ENV === 'production') {
    require('dotenv').config({ path: 'production.env' })
} else {
    require('dotenv').config({ path: '.env.dev' })
}

app.use(
    '/api/storage/users',
    express.static(path.join(__dirname, 'storage/users'))
)
app.use(
    '/api/storage/logos',
    express.static(path.join(__dirname, 'storage/logos'))
)
app.use(cors())
app.use(express.json())
dbConnectMySQL()
app.use(
    session({
        // Configura el middleware express-session
        secret: process.env.APIKEY, // Cambia esto por una cadena secreta segura
        resave: false,
        saveUninitialized: true,
    })
)
app.use(passport.initialize()) // Inicializa passport
morganBody(app, {
    noColors: true,
    logIP: true,
    skip: function (req, res) {
        return res.statusCode < 400
    },
})

const swaggerOptions = {
    explorer: true, // Muestra el botón de "Explorar" en la esquina superior derecha
    customCss: [
        //theme.getBuffer("dark"), // Aplica el tema oscuro de SwaggerTheme
        //path.resolve(__dirname, "docs/custom-swagger.css"), // Ruta al nuevo archivo CSS personalizado
        themechanger, // Aplica el tema universal oscuro de swagger-theme-changer
    ],

    customSiteTitle: 'Documentación de API Admin For Leagues',
    customfavIcon: path.join(__dirname, 'storage/img/logo.png'), // Opcional, si quieres personalizar el favicon
    swaggerOptions: {
        docExpansion: 'none', // Muestra los endpoints en una lista
        filter: true, // Muestra un campo de búsqueda
        theme: 'dark', // Selecciona un tema
    },
}
app.use(
    '/docs',
    swaggerUI.serve,
    swaggerUI.setup(openApiConfiguration, swaggerOptions)
)
app.get('/', (req, res) => {
    res.json({ message: 'Server Up.' })
})
// Configura Express y middleware
app.use(express.urlencoded({ extended: false }))
app.use(express.json())
app.use(passport.initialize())
// ruta para acceder a las imagenes de fotos de perfil de competencias
app.use(
    '/tenant_competitions/images',
    express.static(path.join(__dirname, 'storage/profile_picture_competition'))
)
app.use('/', require('./routes'))

module.exports = app
