const { check, validationResult } = require('express-validator')
const {
    getSequelize,
    getPlayersModel,
    getTeamsModel,
} = require('../../helpers/sequelizeHelper')

// Validación para crear una actualización de jugador (players_update)
const validatorCreatePlayerUpdate = () => {
    return [
        async (req, res, next) => {
            const sequelize = getSequelize(req)
            const PlayersModel = getPlayersModel(sequelize)
            const TeamsModel = getTeamsModel(sequelize)

            // validamos que el GUID del jugador exista
            await check('player_guid')
                .isUUID()
                .withMessage('El GUID del jugador debe ser un UUID válido.')
                .custom(async (value) => {
                    const player = await PlayersModel.findOne({
                        where: { guid: value },
                    })
                    if (!player) {
                        return Promise.reject('El jugador no existe.')
                    }
                })
                .run(req)

            await check('team_guid')
                .isUUID()
                .withMessage('El GUID del equipo debe ser un UUID válido.')
                .custom(async (value) => {
                    const team = await TeamsModel.findOne({
                        where: { guid: value },
                    })
                    if (!team) {
                        return Promise.reject('El equipo no existe.')
                    }
                })
                .run(req)

            await check('single_link')
                .optional()
                .isURL()
                .withMessage('El enlace debe ser una URL válida.')
                .run(req)

            await check('first_name')
                .isString()
                .withMessage('El nombre debe ser una cadena de texto.')
                .run(req)

            await check('last_name')
                .isString()
                .withMessage('El apellido debe ser una cadena de texto.')
                .run(req)

            await check('age')
                .isInt({ min: 0 })
                .withMessage('La edad debe ser un número entero positivo.')
                .run(req)

            await check('birthdate')
                .optional()
                .isISO8601()
                .withMessage(
                    'La fecha de nacimiento debe ser una fecha válida en formato ISO8601.'
                )
                .run(req)

            await check('dorsal_number')
                .isInt({ min: 0 })
                .withMessage(
                    'El número dorsal debe ser un número entero positivo.'
                )
                .run(req)

            await check('position')
                .optional()
                .isString()
                .withMessage('La posición debe ser una cadena de texto.')
                .run(req)

            await check('height')
                .optional()
                .isInt({ min: 0 })
                .withMessage('La altura debe ser una cadena de texto.')
                .run(req)

            await check('weight')
                .optional()
                .isInt({ min: 0 })
                .withMessage('El peso debe ser una cadena de texto.')
                .run(req)

            await check('skilled_foot')
                .optional()
                .isString()
                .withMessage('El pie hábil debe ser una cadena de texto.')
                .run(req)

            await check('facebook')
                .optional()
                .isURL()
                .withMessage('El enlace de Facebook debe ser una URL válida.')
                .run(req)

            await check('instagram')
                .optional()
                .isURL()
                .withMessage('El enlace de Instagram debe ser una URL válida.')
                .run(req)

            await check('personality_traits')
                .optional()
                .isString()
                .withMessage(
                    'Los rasgos de personalidad deben ser una cadena de texto.'
                )
                .run(req)

            await check('image_path')
                .optional()
                .isString()
                .withMessage(
                    'El path de la imagen debe ser una cadena de texto.'
                )
                .run(req)

            await check('phone_number')
                .optional()
                .isMobilePhone()
                .withMessage('El número de teléfono debe ser válido.')
                .run(req)

            await check('country')
                .optional()
                .isString()
                .withMessage('El país debe ser una cadena de texto.')
                .run(req)

            await check('city')
                .optional()
                .isString()
                .withMessage('La ciudad debe ser una cadena de texto.')
                .run(req)

            await check('state')
                .optional()
                .isString()
                .withMessage('El estado debe ser una cadena de texto.')
                .run(req)

            const errors = validationResult(req)
            if (!errors.isEmpty()) {
                return res.status(400).json({ errors: errors.array() })
            }

            next()
        },
    ]
}

// Validación para obtener una actualización de jugador (players_update) por GUID
const validatorPlayerUpdate = () => {
    return [
        async (req, res, next) => {
            await check('guid')
                .isUUID()
                .withMessage(
                    'El GUID de la actualización es requerido y debe ser un UUID válido.'
                )
                .run(req)

            const errors = validationResult(req)
            if (!errors.isEmpty()) {
                return res.status(400).json({ errors: errors.array() })
            }

            next()
        },
    ]
}

// Validación para actualizar una actualización de jugador (players_update)
const validatorUpdatePlayerUpdate = () => {
    return [
        async (req, res, next) => {
            const sequelize = getSequelize(req)
            const PlayersModel = getPlayersModel(sequelize)
            const TeamsModel = getTeamsModel(sequelize)

            await check('guid')
                .isUUID()
                .withMessage(
                    'El GUID de la actualización es requerido y debe ser un UUID válido.'
                )
                .run(req)

            await check('player_guid')
                .isUUID()
                .withMessage('El GUID del jugador debe ser un UUID válido.')
                .custom(async (value) => {
                    const player = await PlayersModel.findOne({
                        where: { guid: value },
                    })
                    if (!player) {
                        return Promise.reject('El jugador no existe.')
                    }
                })
                .run(req)

            await check('team_guid')
                .isUUID()
                .withMessage('El GUID del equipo debe ser un UUID válido.')
                .custom(async (value) => {
                    const team = await TeamsModel.findOne({
                        where: { guid: value },
                    })
                    if (!team) {
                        return Promise.reject('El equipo no existe.')
                    }
                })
                .run(req)

            await check('single_link')
                .optional()
                .isURL()
                .withMessage('El enlace debe ser una URL válida.')
                .run(req)

            await check('first_name')
                .optional()
                .isString()
                .withMessage('El nombre debe ser una cadena de texto.')
                .run(req)

            await check('last_name')
                .optional()
                .isString()
                .withMessage('El apellido debe ser una cadena de texto.')
                .run(req)

            await check('age')
                .optional()
                .isInt({ min: 0 })
                .withMessage('La edad debe ser un número entero positivo.')
                .run(req)

            await check('birthdate')
                .optional()
                .isISO8601()
                .withMessage(
                    'La fecha de nacimiento debe ser una fecha válida en formato ISO8601.'
                )
                .run(req)

            await check('dorsal_number')
                .optional()
                .isInt({ min: 0 })
                .withMessage(
                    'El número dorsal debe ser un número entero positivo.'
                )
                .run(req)

            await check('position')
                .optional()
                .isString()
                .withMessage('La posición debe ser una cadena de texto.')
                .run(req)

            await check('height')
                .optional()
                .isInt({ min: 0 })
                .withMessage('La altura debe ser una cadena de texto.')
                .run(req)

            await check('weight')
                .optional()
                .isInt({ min: 0 })
                .withMessage('El peso debe ser una cadena de texto.')
                .run(req)

            await check('skilled_foot')
                .optional()
                .isString()
                .withMessage('El pie hábil debe ser una cadena de texto.')
                .run(req)

            await check('facebook')
                .optional()
                .isURL()
                .withMessage('El enlace de Facebook debe ser una URL válida.')
                .run(req)

            await check('instagram')
                .optional()
                .isURL()
                .withMessage('El enlace de Instagram debe ser una URL válida.')
                .run(req)

            await check('personality_traits')
                .optional()
                .isString()
                .withMessage(
                    'Los rasgos de personalidad deben ser una cadena de texto.'
                )
                .run(req)

            await check('image_path')
                .optional()
                .isString()
                .withMessage(
                    'El path de la imagen debe ser una cadena de texto.'
                )
                .run(req)

            await check('phone_number')
                .optional()
                .isMobilePhone()
                .withMessage('El número de teléfono debe ser válido.')
                .run(req)

            await check('country')
                .optional()
                .isString()
                .withMessage('El país debe ser una cadena de texto.')
                .run(req)

            await check('city')
                .optional()
                .isString()
                .withMessage('La ciudad debe ser una cadena de texto.')
                .run(req)

            await check('state')
                .optional()
                .isString()
                .withMessage('El estado debe ser una cadena de texto.')
                .run(req)

            await check('is_active')
                .optional()
                .isInt({ min: 0, max: 1 })
                .withMessage(
                    'El estado activo debe ser 0 (inactivo) o 1 (activo).'
                )
                .run(req)

            const errors = validationResult(req)
            if (!errors.isEmpty()) {
                return res.status(400).json({ errors: errors.array() })
            }

            next()
        },
    ]
}

// Validación para eliminar una actualización de jugador (players_update)
const validatorDeletePlayerUpdate = () => {
    return [
        async (req, res, next) => {
            await check('guid')
                .isUUID()
                .withMessage(
                    'El GUID de la actualización es requerido y debe ser un UUID válido.'
                )
                .run(req)

            const errors = validationResult(req)
            if (!errors.isEmpty()) {
                return res.status(400).json({ errors: errors.array() })
            }

            next()
        },
    ]
}

// validación para deshabilitar un jugador (players_update)
const validatorDisablePlayerUpdate = () => {
    return [
        async (req, res, next) => {
            await check('guid')
                .isUUID()
                .withMessage(
                    'El GUID del jugador es requerido y debe ser un UUID válido.'
                )
                .run(req)

            const errors = validationResult(req)
            if (!errors.isEmpty()) {
                return res.status(400).json({ errors: errors.array() })
            }

            next()
        },
    ]
}

// validación para habilitar un jugador (players_update)
const validatorEnablePlayerUpdate = () => {
    return [
        async (req, res, next) => {
            await check('guid')
                .isUUID()
                .withMessage(
                    'El GUID del jugador es requerido y debe ser un UUID válido.'
                )
                .run(req)

            const errors = validationResult(req)
            if (!errors.isEmpty()) {
                return res.status(400).json({ errors: errors.array() })
            }

            next()
        },
    ]
}

module.exports = {
    validatorCreatePlayerUpdate,
    validatorPlayerUpdate,
    validatorUpdatePlayerUpdate,
    validatorDeletePlayerUpdate,
    validatorDisablePlayerUpdate,
    validatorEnablePlayerUpdate,
}
