const { matchedData } = require('express-validator')
const OrdersModel = require('../../models/general/orders')
const { handleHttpError } = require('../../utils/handleError')
const { handleHttpSuccess } = require('../../utils/handleSuccess')
const { handleHttpCount } = require('../../utils/handleCount')

/**
 * Este controlador es el encargado de crear órdenes
 * @param {*} req
 * @param {*} res
 */
const CreateOrder = async (req, res) => {
    try {
        const data = matchedData(req)
        const created_by = req.user.guid
        const created_at = new Date()
        data.created_by = created_by
        data.created_at = created_at
        const order = await OrdersModel.create(data)
        return handleHttpSuccess(res, 'Se ha creado la orden.', 201, order)
    } catch (error) {
        console.log(error)
        return handleHttpError(res, '', 403, error)
    }
}

/**
 * Este controlador es el encargado de obtener todas las órdenes activas
 * @param {*} req
 * @param {*} res
 */
const Orders = async (req, res) => {
    try {
        const orders = await OrdersModel.findAll({
            where: { is_active: 1 },
        })
        if (!orders) {
            return handleHttpError(res, 'La orden no existe.', 404)
        }
        const message = orders.length
            ? 'Se han encontrado los datos.'
            : 'No se encontraron datos.'
        return handleHttpSuccess(res, message, 201, orders)
    } catch (error) {
        console.log(error)
        return handleHttpError(res, '', 403, error)
    }
}

/**
 * Este controlador es el encargado de obtener órdenes por guid
 * @param {*} req
 * @param {*} res
 */
const Order = async (req, res) => {
    try {
        const { guid } = req.params
        const DataOrder = await OrdersModel.findOne({
            where: { guid },
        })
        if (!DataOrder) {
            return handleHttpError(res, 'La orden no existe.', 404)
        }
        return handleHttpSuccess(
            res,
            'Se han encontrado los datos.',
            201,
            DataOrder
        )
    } catch (error) {
        console.log(error)
        return handleHttpError(res, '', 403, error)
    }
}

/**
 * Este controlador es el encargado de actualizar órdenes
 * @param {*} req
 * @param {*} res
 */
const UpdateOrder = async (req, res) => {
    try {
        const { guid } = req.params
        const updated_by = req.user.guid
        const updated_at = new Date()
        const data = matchedData(req)
        data.updated_by = updated_by
        data.updated_at = updated_at
        const [updateCount] = await OrdersModel.update(data, {
            where: { guid },
        })
        if (updateCount === 0) {
            return handleHttpError(res, 'La orden no existe.', 404)
        }
        const updatedOrder = await OrdersModel.findOne({
            where: { guid },
        })
        if (!updatedOrder) {
            return handleHttpError(
                res,
                'Error al obtener la orden actualizada.',
                500
            )
        }
        return handleHttpSuccess(
            res,
            'Se ha actualizado la orden.',
            201,
            updatedOrder
        )
    } catch (error) {
        console.log(error)
        return handleHttpError(res, '', 403, error)
    }
}

/**
 * Este controlador es el encargado de eliminar órdenes
 * @param {*} req
 * @param {*} res
 */
const DeleteOrder = async (req, res) => {
    try {
        const { guid } = req.params
        const deleted_by = req.user.guid
        const deleted_at = new Date()
        const [updateCount] = await OrdersModel.update(
            {
                is_active: 0,
                deleted_by,
                deleted_at,
            },
            {
                where: { guid },
            }
        )
        if (updateCount === 0) {
            return handleHttpError(res, 'La orden no existe.', 404)
        }
        handleHttpSuccess(res, 'Se ha eliminado la orden.', 201)
    } catch (error) {
        console.log(error)
        handleHttpError(res, '', 403, error)
    }
}

/**
 * Este controlador es el encargado de desactivar órdenes
 * @param {*} req
 * @param {*} res
 */
const DisableOrder = async (req, res) => {
    try {
        const { guid } = req.params
        const updated_by = req.user.guid
        const updated_at = new Date()
        const [updateCount] = await OrdersModel.update(
            {
                is_active: 0,
                updated_by,
                updated_at,
            },
            {
                where: { guid },
            }
        )
        if (updateCount === 0) {
            return handleHttpError(res, 'La orden no existe.', 404)
        }
        handleHttpSuccess(res, 'Se ha desactivado la orden.', 201)
    } catch (error) {
        console.log(error)
        handleHttpError(res, '', 403, error)
    }
}

/**
 * Este controlador es el encargado de activar órdenes
 * @param {*} req
 * @param {*} res
 */
const EnableOrder = async (req, res) => {
    try {
        const { guid } = req.params
        const updated_by = req.user.guid
        const updated_at = new Date()
        const [updateCount] = await OrdersModel.update(
            {
                is_active: 1,
                updated_by,
                updated_at,
            },
            {
                where: { guid },
            }
        )
        if (updateCount === 0) {
            return handleHttpError(res, 'La orden no existe.', 404)
        }
        handleHttpSuccess(res, 'Se ha activado la orden.', 201)
    } catch (error) {
        console.log(error)
        handleHttpError(res, '', 403, error)
    }
}

/**
 * Este controlador es el encargado de obtener todas las órdenes
 * @param {*} req
 * @param {*} res
 */
const OrdersAll = async (req, res) => {
    try {
        const DataOrders = await OrdersModel.findAll()
        if (!DataOrders) {
            handleHttpError(res, 'No se encontraron datos.', 404)
            return
        }
        const message = DataOrders.length
            ? 'Se han encontrado los datos.'
            : 'No se encontraron datos.'
        handleHttpSuccess(res, message, 201, DataOrders)
    } catch (error) {
        console.log(error)
        handleHttpError(res, '', 403, error)
    }
}

/**
 * Este controlador es el encargado de contar todas las órdenes
 * @param {*} req
 * @param {*} res
 */
const CountAllOrders = async (req, res) => {
    try {
        const DataOrders = await OrdersModel.count()
        if (!DataOrders) {
            handleHttpError(res, 'No se encontraron datos.', 404)
            return
        }
        handleHttpCount(res, 'Se han encontrado los datos.', 201, DataOrders)
    } catch (error) {
        console.log(error)
        handleHttpError(res, '', 403, error)
    }
}

module.exports = {
    CreateOrder,
    Orders,
    Order,
    UpdateOrder,
    DeleteOrder,
    DisableOrder,
    EnableOrder,
    OrdersAll,
    CountAllOrders,
}
