const express = require('express')
const router = express.Router()

// controladores de autenticación y middleware
const {
    validatorCreateUser,
    validatorUpdateUser,
    ValidatorDeleteUser,
    ValidatorDisableUser,
    ValidatorEnableUser,
    ValidatorPasswordResetUser,
} = require('../../validators/general/users')

const {
    CreateUser,
    Users,
    User,
    UpdateUser,
    DeleteUser,
    DisableUser,
    EnableUser,
    UploadProfilePicture,
    EncryptPasswordsReset,
    UsersAll,
    UsersCount,
} = require('../../controllers/general/users')

const uploadMiddleware = require('../../utils/handleStorage')
const authMiddleware = require('../../middleware/session')

/* Rutas protegidas que requieren autenticación */
/**
 * @swagger
 * tags:
 *   name: Users
 *   description: API para la gestión de usuarios
 */

// Ruta para crear un nuevo usuario
/**
 * @swagger
 * /users/CreateUser:
 *   post:
 *     summary: Crear un nuevo usuario
 *     description: Esta ruta permite crear un nuevo usuario con los detalles especificados.
 *     tags: [Users]
 *     requestBody:
 *       required: true
 *       content:
 *         application/json:
 *           schema:
 *             type: object
 *             properties:
 *               first_name:
 *                type: string
 *                example: prueba
 *               last_name:
 *                type: string
 *                example: prueba
 *               tenant_guid:
 *                type: uuid
 *                example: 2554128f-924f-4c26-a0f6-712526a50ba0
 *               role_guid:
 *                type: uuid
 *                example: 1881e61d-4be8-4e3b-9482-9643d5c05327
 *               username:
 *                type: string
 *                example: prueba
 *               email:
 *                type: string
 *                example: user@example.com
 *               password:
 *                type: Prueba321+.
 *                example:
 *               phone:
 *                type: string
 *                example: 1234567890
 *               isVerified:
 *                type: boolean
 *                example: 1
 *               image_path:
 *                type: string
 *                example: https://encrypted-tbn0.gstatic.com/images?q=tbn:ANd9GcTSrsozCMwpPp8yaK-zyiLe0XU-_chgZdCoPBA_BrZUXg&s
 *               picture:
 *                type: string
 *                example:
 *               country:
 *                type: string
 *                example:  mexico
 *               state:
 *                type: string
 *                example: veracruz
 *               city:
 *                type: string
 *                example: veracruz
 *               google_id:
 *                type: string
 *                example:
 *               facebook_id:
 *                type: string
 *                example:
 *               two_factor_auth:
 *                type: integer
 *                example: 1
 *               weeklyNewsletter:
 *                type: integer
 *                example: 1
 *               lifecycleEmails:
 *                type: integer
 *                example: 1
 *               promotionalEmails:
 *                type: integer
 *                example: 1
 *               productUpdates:
 *                type: integer
 *                example: 1
 *     responses:
 *       201:
 *         description: Usuario creado exitosamente
 *         content:
 *           application/json:
 *             schema:
 *               $ref: '#/components/responses/success'
 *       400:
 *         description: Error en los datos enviados
 *         content:
 *           application/json:
 *             schema:
 *               $ref: '#/components/responses/BadRequest'
 *       403:
 *         description: Error de autenticación
 *         content:
 *           application/json:
 *             schema:
 *               $ref: '#/components/responses/UnauthorizedError'
 *       409:
 *         description: El usuario ya existe
 *         content:
 *           application/json:
 *             schema:
 *               $ref: '#/components/responses/ConflictError'
 *       500:
 *         description: Error en el servidor
 *         content:
 *           application/json:
 *             schema:
 *               $ref: '#/components/responses/InternalServerError'
 *     security:
 *        - bearerAuth: []
 */
router.post('/CreateUser', authMiddleware, validatorCreateUser, CreateUser)

// Ruta para obtener todos los usuarios activos
/**
 * @swagger
 * /users/Users:
 *   get:
 *     summary: Obtener todos los usuarios activos
 *     tags: [Users]
 *     responses:
 *       200:
 *         description: Usuarios obtenidos exitosamente
 *         content:
 *           application/json:
 *             schema:
 *               $ref: '#/components/responses/success'
 *       403:
 *         description: Error de autenticación
 *         content:
 *           application/json:
 *             schema:
 *               $ref: '#/components/responses/UnauthorizedError'
 *       500:
 *         description: Error en el servidor
 *         content:
 *           application/json:
 *             schema:
 *               $ref: '#/components/responses/InternalServerError'
 *     security:
 *        - bearerAuth: []
 */
router.get('/Users', authMiddleware, Users)

// Ruta para obtener un usuario por Guid
/**
 * @swagger
 * /users/User:
 *   get:
 *     summary: Obtener un usuario por ID
 *     tags: [Users]
 *     parameters:
 *       - in: path
 *         name: guid
 *         schema:
 *           type: string
 *           format: uuid
 *         required: true
 *         description: El GUID del usuario
 *     responses:
 *       200:
 *         description: Usuario obtenido exitosamente
 *         content:
 *           application/json:
 *             schema:
 *               $ref: '#/components/responses/success'
 *       403:
 *         description: Error de autenticación
 *         content:
 *           application/json:
 *             schema:
 *               $ref: '#/components/responses/UnauthorizedError'
 *       404:
 *        description: Usuario no encontrado
 *        content:
 *          application/json:
 *            schema:
 *              $ref: '#/components/responses/NotFoundError'
 *       500:
 *         description: Error en el servidor
 *         content:
 *           application/json:
 *             schema:
 *               $ref: '#/components/responses/InternalServerError'
 *     security:
 *        - bearerAuth: []
 */
router.get('/User/:guid', authMiddleware, User)

// Ruta para actualizar un usuario
/**
 * @swagger
 * /users/UpdateUser:
 *   patch:
 *     summary: Actualizar un usuario
 *     tags: [Users]
 *     requestBody:
 *       required: true
 *       content:
 *         application/json:
 *           schema:
 *             type: object
 *             properties:
 *               guid:
 *                type: uuid
 *                example: 495db854-849b-41cd-b6b9-9c84824c92ba
 *               first_name:
 *                type: string
 *                example: prueba
 *               last_name:
 *                type: string
 *                example: prueba
 *               tenant_guid:
 *                type: uuid
 *                example: 2554128f-924f-4c26-a0f6-712526a50ba0
 *               role_guid:
 *                type: uuid
 *                example: 1881e61d-4be8-4e3b-9482-9643d5c05327
 *               username:
 *                type: string
 *                example: prueba
 *               email:
 *                type: string
 *                example: user@example.com
 *               password:
 *                type: Prueba321+.
 *                example:
 *               phone:
 *                type: string
 *                example: 1234567890
 *               isVerified:
 *                type: boolean
 *                example: 1
 *               image_path:
 *                type: string
 *                example: https://encrypted-tbn0.gstatic.com/images?q=tbn:ANd9GcTSrsozCMwpPp8yaK-zyiLe0XU-_chgZdCoPBA_BrZUXg&s
 *               picture:
 *                type: string
 *                example:
 *               country:
 *                type: string
 *                example:  mexico
 *               state:
 *                type: string
 *                example: veracruz
 *               city:
 *                type: string
 *                example: veracruz
 *               google_id:
 *                type: string
 *                example:
 *               facebook_id:
 *                type: string
 *                example:
 *               two_factor_auth:
 *                type: integer
 *                example: 1
 *               weeklyNewsletter:
 *                type: integer
 *                example: 1
 *               lifecycleEmails:
 *                type: integer
 *                example: 1
 *               promotionalEmails:
 *                type: integer
 *                example: 1
 *               productUpdates:
 *                type: integer
 *                example: 1
 *     responses:
 *       200:
 *         description: Usuario actualizado exitosamente
 *         content:
 *           application/json:
 *             schema:
 *               $ref: '#/components/responses/success'
 *       400:
 *         description: Error en los datos enviados
 *         content:
 *           application/json:
 *             schema:
 *               $ref: '#/components/responses/BadRequest'
 *       403:
 *         description: Error de autenticación
 *         content:
 *           application/json:
 *             schema:
 *               $ref: '#/components/responses/UnauthorizedError'
 *       404:
 *        description: Usuario no encontrado
 *        content:
 *          application/json:
 *            schema:
 *              $ref: '#/components/responses/NotFoundError'
 *       500:
 *         description: Error en el servidor
 *         content:
 *           application/json:
 *             schema:
 *               $ref: '#/components/responses/InternalServerError'
 *     security:
 *        - bearerAuth: []
 */
router.patch(
    '/UpdateUser/:guid',
    authMiddleware,
    validatorUpdateUser,
    UpdateUser
)

// Ruta para eliminar un usuario
/**
 * @swagger
 * /users/DeleteUser:
 *   delete:
 *     summary: Eliminar un usuario
 *     tags: [Users]
 *     parameters:
 *       - in: path
 *         name: guid
 *         schema:
 *           type: string
 *           format: uuid
 *         required: true
 *         description: El GUID del usuario
 *     responses:
 *       200:
 *         description: Usuario eliminado exitosamente
 *         content:
 *           application/json:
 *             schema:
 *               $ref: '#/components/responses/success'
 *       400:
 *         description: Error en los datos enviados
 *         content:
 *           application/json:
 *             schema:
 *               $ref: '#/components/responses/BadRequest'
 *       403:
 *         description: Error de autenticación
 *         content:
 *           application/json:
 *             schema:
 *               $ref: '#/components/responses/UnauthorizedError'
 *       404:
 *        description: Usuario no encontrado
 *        content:
 *          application/json:
 *            schema:
 *              $ref: '#/components/responses/NotFoundError'
 *       500:
 *         description: Error en el servidor
 *         content:
 *           application/json:
 *             schema:
 *               $ref: '#/components/responses/InternalServerError'
 *     security:
 *        - bearerAuth: []
 */
router.delete(
    '/DeleteUser/:guid',
    authMiddleware,
    ValidatorDeleteUser,
    DeleteUser
)

// Ruta para deshabilitar un usuario
/**
 * @swagger
 * /users/DisableUser:
 *   patch:
 *     summary: Deshabilita un usuario
 *     tags: [Users]
 *     requestBody:
 *       required: true
 *       content:
 *         application/json:
 *           schema:
 *             type: object
 *             properties:
 *               guid:
 *                 type: uuid
 *                 example: 495db854-849b-41cd-b6b9-9c84824c92ba
 *     responses:
 *       200:
 *         description: Usuario deshabilitado exitosamente
 *         content:
 *           application/json:
 *             schema:
 *               $ref: '#/components/responses/success'
 *       400:
 *         description: Error en los datos enviados
 *         content:
 *           application/json:
 *             schema:
 *               $ref: '#/components/responses/BadRequest'
 *       403:
 *         description: Error de autenticación
 *         content:
 *           application/json:
 *             schema:
 *               $ref: '#/components/responses/UnauthorizedError'
 *       404:
 *        description: Usuario no encontrado
 *        content:
 *          application/json:
 *            schema:
 *              $ref: '#/components/responses/NotFoundError'
 *       500:
 *         description: Error en el servidor
 *         content:
 *           application/json:
 *             schema:
 *               $ref: '#/components/responses/InternalServerError'
 *     security:
 *        - bearerAuth: []
 */
router.patch(
    '/DisableUser/:guid',
    authMiddleware,
    ValidatorDisableUser,
    DisableUser
)

// Ruta para habilitar un usuario
/**
 * @swagger
 * /users/EnableUser:
 *   patch:
 *     summary: Habilita un usuario
 *     tags: [Users]
 *     parameters:
 *       - in: path
 *         name: guid
 *         schema:
 *           type: string
 *           format: uuid
 *         required: true
 *         description: El GUID del usuario
 *     responses:
 *       200:
 *         description: Usuario habilitado exitosamente
 *         content:
 *           application/json:
 *             schema:
 *               $ref: '#/components/responses/success'
 *       400:
 *         description: Error en los datos enviados
 *         content:
 *           application/json:
 *             schema:
 *               $ref: '#/components/responses/BadRequest'
 *       403:
 *         description: Error de autenticación
 *         content:
 *           application/json:
 *             schema:
 *               $ref: '#/components/responses/UnauthorizedError'
 *       404:
 *        description: Usuario no encontrado
 *        content:
 *          application/json:
 *            schema:
 *              $ref: '#/components/responses/NotFoundError'
 *       500:
 *         description: Error en el servidor
 *         content:
 *           application/json:
 *             schema:
 *               $ref: '#/components/responses/InternalServerError'
 *     security:
 *        - bearerAuth: []
 */
router.patch(
    '/EnableUser/:guid',
    authMiddleware,
    ValidatorEnableUser,
    EnableUser
)

// Ruta para restablecer la contraseña
/**
 * @swagger
 * /users/RestorePassword:
 *   patch:
 *     summary: restablecimiento de contraseña
 *     tags: [Users]
 *     requestBody:
 *       required: true
 *       content:
 *         application/json:
 *           schema:
 *             type: object
 *             properties:
 *               guid:
 *                 type: string
 *                 example: 495db854-849b-41cd-b6b9-9c84824c92ba
 *               new_password:
 *                 type: string
 *                 example: Prueba321+.
 *     responses:
 *       200:
 *         description: Usuario actualizado exitosamente
 *         content:
 *           application/json:
 *             schema:
 *               $ref: '#/components/responses/success'
 *       400:
 *         description: Error en los datos enviados
 *         content:
 *           application/json:
 *             schema:
 *               $ref: '#/components/responses/BadRequest'
 *       403:
 *         description: Error de autenticación
 *         content:
 *           application/json:
 *             schema:
 *               $ref: '#/components/responses/UnauthorizedError'
 *       404:
 *         description: Usuario no encontrado
 *         content:
 *           application/json:
 *             schema:
 *               $ref: '#/components/responses/NotFoundError'
 *       500:
 *         description: Error en el servidor
 *         content:
 *           application/json:
 *             schema:
 *               $ref: '#/components/responses/InternalServerError'
 *     security:
 *        - bearerAuth: []
 */
router.patch(
    '/RestorePassword/:guid',
    authMiddleware,
    ValidatorPasswordResetUser,
    EncryptPasswordsReset
)

// Ruta para agregar foto de perfil
/**
 * @swagger
 * /users/UploadProfilePicture:
 *   post:
 *     summary: Agrega una foto de perfil al usuario
 *     description: Esta ruta permite agregar una foto de perfil para un usuario.
 *     tags: [Users]
 *     requestBody:
 *       required: true
 *       content:
 *         application/json:
 *           schema:
 *             type: object
 *             properties:
 *               user_guid:
 *                 type: string
 *                 example: 495db854-849b-41cd-b6b9-9c84824c92ba
 *     responses:
 *       200:
 *         description: Usuario habilitado exitosamente
 *         content:
 *           application/json:
 *             schema:
 *               $ref: '#/components/responses/success'
 *       400:
 *         description: Error en los datos enviados
 *         content:
 *           application/json:
 *             schema:
 *               $ref: '#/components/responses/BadRequest'
 *       403:
 *         description: Error de autenticación
 *         content:
 *           application/json:
 *             schema:
 *               $ref: '#/components/responses/UnauthorizedError'
 *       404:
 *        description: Usuario no encontrado
 *        content:
 *          application/json:
 *            schema:
 *              $ref: '#/components/responses/NotFoundError'
 *       500:
 *         description: Error en el servidor
 *         content:
 *           application/json:
 *             schema:
 *               $ref: '#/components/responses/InternalServerError'
 *     security:
 *        - bearerAuth: []
 */
router.post(
    '/UploadProfilePicture',
    authMiddleware,
    uploadMiddleware('users').single('myfile'),
    UploadProfilePicture
)

// Ruta para obtener todos los usuarios
/**
 * @swagger
 * /Users/UsersAll:
 *   get:
 *     summary: Obtener todos los usuarios
 *     tags: [Users]
 *     responses:
 *       200:
 *         description: Usuarios obtenidos exitosamente
 *         content:
 *           application/json:
 *             schema:
 *               $ref: '#/components/responses/success'
 *       403:
 *         description: Error de autenticación
 *         content:
 *           application/json:
 *             schema:
 *               $ref: '#/components/responses/UnauthorizedError'
 *       500:
 *         description: Error en el servidor
 *         content:
 *           application/json:
 *             schema:
 *               $ref: '#/components/responses/InternalServerError'
 *     security:
 *        - bearerAuth: []
 */
router.get('/UsersAll', authMiddleware, UsersAll)

// Ruta para obtener la cantidad de usuarios
/**
 * @swagger
 * /Users/UsersCount:
 *   get:
 *     summary: Obtener la cantidad de usuarios
 *     tags: [Users]
 *     responses:
 *       200:
 *         description: Cantidad de usuarios obtenida exitosamente
 *         content:
 *           application/json:
 *             schema:
 *               $ref: '#/components/responses/success'
 *       403:
 *         description: Error de autenticación
 *         content:
 *           application/json:
 *             schema:
 *               $ref: '#/components/responses/UnauthorizedError'
 *       500:
 *         description: Error en el servidor
 *         content:
 *           application/json:
 *             schema:
 *               $ref: '#/components/responses/InternalServerError'
 *     security:
 *        - bearerAuth: []
 */
router.get('/UsersCount', authMiddleware, UsersCount)

module.exports = router
