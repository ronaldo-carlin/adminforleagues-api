const { v4: uuidv4 } = require('uuid')
const GUIDsTenant = require('../../guids_tenant')
module.exports = {
    async up(queryInterface, Sequelize) {
        // Insertar los permisos que tiene en el modulo cada rol
        await queryInterface.bulkInsert(
            'access_control',
            [
                {
                    guid: uuidv4(),
                    role_guid: GUIDsTenant.roleSuperAdminGuid,
                    module_guid: GUIDsTenant.dashboardGuid,
                    permissions_guid: GUIDsTenant.PermissionReadGuid,
                    created_at: new Date(),
                },
            ],
            {}
        )
    },

    async down(queryInterface, Sequelize) {},
}
