const { matchedData } = require('express-validator')
const LicensesModel = require('../../models/general/licenses')
const { handleHttpError } = require('../../utils/handleError')
const { handleHttpSuccess } = require('../../utils/handleSuccess')
const { handleHttpCount } = require('../../utils/handleCount')

/**
 * Este controlador es el encargado de crear licencias
 * @param {*} req
 * @param {*} res
 */
const CreateLicense = async (req, res) => {
    try {
        const data = matchedData(req)
        const created_by = req.user.guid
        const created_at = new Date()
        data.created_by = created_by
        data.created_at = created_at
        const license = await LicensesModel.create(data)
        return handleHttpSuccess(res, 'Se ha creado la licencia.', 201, license)
    } catch (error) {
        console.log(error)
        return handleHttpError(res, '', 403, error)
    }
}

/**
 * Este controlador es el encargado de obtener todas las licencias activas
 * @param {*} req
 * @param {*} res
 */
const Licenses = async (req, res) => {
    try {
        const licenses = await LicensesModel.findAll({
            where: { is_active: 1 },
        })
        if (!licenses) {
            return handleHttpError(res, 'La licencia no existe.', 404)
        }
        const message = licenses.length
            ? 'Se han encontrado los datos.'
            : 'No se encontraron datos.'
        return handleHttpSuccess(res, message, 201, licenses)
    } catch (error) {
        console.log(error)
        return handleHttpError(res, '', 403, error)
    }
}

/**
 * Este controlador es el encargado de obtener licencias por guid
 * @param {*} req
 * @param {*} res
 */
const License = async (req, res) => {
    try {
        const { guid } = req.params
        const DataLicense = await LicensesModel.findOne({
            where: { guid },
        })
        if (!DataLicense) {
            return handleHttpError(res, 'La licencia no existe.', 404)
        }
        return handleHttpSuccess(
            res,
            'Se han encontrado los datos.',
            201,
            DataLicense
        )
    } catch (error) {
        console.log(error)
        return handleHttpError(res, '', 403, error)
    }
}

/**
 * Este controlador es el encargado de actualizar licencias
 * @param {*} req
 * @param {*} res
 */
const UpdateLicense = async (req, res) => {
    try {
        const { guid } = req.params
        const updated_by = req.user.guid
        const updated_at = new Date()
        const data = matchedData(req)
        data.updated_by = updated_by
        data.updated_at = updated_at
        const [updateCount] = await LicensesModel.update(data, {
            where: { guid },
        })
        if (updateCount === 0) {
            return handleHttpError(res, 'La licencia no existe.', 404)
        }
        const updatedLicense = await LicensesModel.findOne({
            where: { guid },
        })
        if (!updatedLicense) {
            return handleHttpError(
                res,
                'Error al obtener la licencia actualizada.',
                500
            )
        }
        return handleHttpSuccess(
            res,
            'Se ha actualizado la licencia.',
            201,
            updatedLicense
        )
    } catch (error) {
        console.log(error)
        return handleHttpError(res, '', 403, error)
    }
}

/**
 * Este controlador es el encargado de eliminar licencias
 * @param {*} req
 * @param {*} res
 */
const DeleteLicense = async (req, res) => {
    try {
        const { guid } = req.params
        const deleted_by = req.user.guid
        const deleted_at = new Date()
        const [updateCount] = await LicensesModel.update(
            {
                is_active: 0,
                deleted_by,
                deleted_at,
            },
            {
                where: { guid },
            }
        )
        if (updateCount === 0) {
            return handleHttpError(res, 'La licencia no existe.', 404)
        }
        handleHttpSuccess(res, 'Se ha eliminado la licencia.', 201)
    } catch (error) {
        console.log(error)
        handleHttpError(res, '', 403, error)
    }
}

/**
 * Este controlador es el encargado de desactivar licencias
 * @param {*} req
 * @param {*} res
 */
const DisableLicense = async (req, res) => {
    try {
        const { guid } = req.params
        const updated_by = req.user.guid
        const updated_at = new Date()
        const [updateCount] = await LicensesModel.update(
            {
                is_active: 0,
                updated_by,
                updated_at,
            },
            {
                where: { guid },
            }
        )
        if (updateCount === 0) {
            return handleHttpError(res, 'La licencia no existe.', 404)
        }
        handleHttpSuccess(res, 'Se ha desactivado la licencia.', 201)
    } catch (error) {
        console.log(error)
        handleHttpError(res, '', 403, error)
    }
}

/**
 * Este controlador es el encargado de activar licencias
 * @param {*} req
 * @param {*} res
 */
const EnableLicense = async (req, res) => {
    try {
        const { guid } = req.params
        const updated_by = req.user.guid
        const updated_at = new Date()
        const [updateCount] = await LicensesModel.update(
            {
                is_active: 1,
                updated_by,
                updated_at,
            },
            {
                where: { guid },
            }
        )
        if (updateCount === 0) {
            return handleHttpError(res, 'La licencia no existe.', 404)
        }
        handleHttpSuccess(res, 'Se ha activado la licencia.', 201)
    } catch (error) {
        console.log(error)
        handleHttpError(res, '', 403, error)
    }
}

/**
 * Este controlador es el encargado de obtener todas las licencias
 * @param {*} req
 * @param {*} res
 */
const LicensesAll = async (req, res) => {
    try {
        const DataLicenses = await LicensesModel.findAll()
        if (!DataLicenses) {
            handleHttpError(res, 'No se encontraron datos.', 404)
            return
        }
        const message = DataLicenses.length
            ? 'Se han encontrado los datos.'
            : 'No se encontraron datos.'
        handleHttpSuccess(res, message, 201, DataLicenses)
    } catch (error) {
        console.log(error)
        handleHttpError(res, '', 403, error)
    }
}

/**
 * Este controlador es el encargado de contar todas las licencias
 * @param {*} req
 * @param {*} res
 */
const CountAllLicenses = async (req, res) => {
    try {
        const DataLicenses = await LicensesModel.count()
        if (!DataLicenses) {
            handleHttpError(res, 'No se encontraron datos.', 404)
            return
        }
        handleHttpCount(res, 'Se han encontrado los datos.', 201, DataLicenses)
    } catch (error) {
        console.log(error)
        handleHttpError(res, '', 403, error)
    }
}

module.exports = {
    CreateLicense,
    Licenses,
    License,
    UpdateLicense,
    DeleteLicense,
    DisableLicense,
    EnableLicense,
    LicensesAll,
    CountAllLicenses,
}
