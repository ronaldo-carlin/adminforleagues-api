module.exports = {
    async up(queryInterface, Sequelize) {
        // Tabla de control de acceso
        await queryInterface.createTable('access_control', {
            // Identificador del registro
            id: {
                type: Sequelize.INTEGER,
                primaryKey: true,
                autoIncrement: true,
                allowNull: false,
            },
            // Identificador guia del registro
            guid: {
                type: Sequelize.UUID,
                defaultValue: Sequelize.UUIDV4,
                unique: true,
                allowNull: false,
            },
            // Identificador del rol
            role_guid: {
                type: Sequelize.UUID,
            },
            // Identificador del modulo
            module_guid: {
                type: Sequelize.UUID,
            },
            // Identificador del permiso
            permissions_guid: {
                type: Sequelize.UUID,
            },
            // estado del registro
            is_active: {
                type: Sequelize.BOOLEAN,
                defaultValue: 1, // valor por defecto
            },
            // usuario que creo el registro
            created_by: {
                type: Sequelize.UUID,
            },
            // fecha de creacion
            created_at: {
                type: Sequelize.DATE,
                defaultValue: Sequelize.literal('CURRENT_TIMESTAMP'),
            },
            // usuario que actualizo el registro
            updated_by: {
                type: Sequelize.UUID,
            },
            // fecha de actualizacion
            updated_at: {
                type: Sequelize.DATE,
            },
            // usuario que elimino el registro
            deleted_by: {
                type: Sequelize.UUID,
            },
            // fecha de eliminacion
            deleted_at: {
                type: Sequelize.DATE,
            },
        })

        // índices
        await queryInterface.addIndex('access_control', ['guid'], {
            unique: true,
            name: 'unique_access_control_guid',
        })

        await queryInterface.addIndex('access_control', ['role_guid'], {
            name: 'idx_access_control_role_guid',
        })

        await queryInterface.addIndex('access_control', ['module_guid'], {
            name: 'idx_access_control_module_guid',
        })

        await queryInterface.addIndex('access_control', ['permissions_guid'], {
            name: 'idx_access_control_permissions_guid',
        })

        // llaves foraneas
        await queryInterface.addConstraint('access_control', {
            fields: ['role_guid'],
            type: 'foreign key',
            name: 'fk_access_control_role_guid',
            references: {
                table: 'roles',
                field: 'guid',
            },
            onUpdate: 'CASCADE',
            onDelete: 'CASCADE',
        })

        await queryInterface.addConstraint('access_control', {
            fields: ['module_guid'],
            type: 'foreign key',
            name: 'fk_access_control_module_guid',
            references: {
                table: 'modules',
                field: 'guid',
            },
            onUpdate: 'CASCADE',
            onDelete: 'CASCADE',
        })

        await queryInterface.addConstraint('access_control', {
            fields: ['permissions_guid'],
            type: 'foreign key',
            name: 'fk_access_control_permissions_guid',
            references: {
                table: 'permissions',
                field: 'guid',
            },
            onUpdate: 'CASCADE',
            onDelete: 'CASCADE',
        })
    },

    async down(queryInterface, Sequelize) {
        await queryInterface.removeConstraint(
            'access_control',
            'fk_access_control_role_guid'
        )
        await queryInterface.removeConstraint(
            'access_control',
            'fk_access_control_module_guid'
        )
        await queryInterface.removeConstraint(
            'access_control',
            'fk_access_control_permissions_guid'
        )
        await queryInterface.removeIndex(
            'access_control',
            'idx_access_control_role_guid'
        )
        await queryInterface.removeIndex(
            'access_control',
            'idx_access_control_module_guid'
        )
        await queryInterface.removeIndex(
            'access_control',
            'idx_access_control_permissions_guid'
        )
        await queryInterface.removeIndex(
            'access_control',
            'unique_access_control_guid'
        )
        await queryInterface.dropTable('access_control')
    },
}
