const { check } = require('express-validator')
const validateResults = require('../../utils/handleValidators')

// Validaciones para crear un Plan
const ValidatorCreatePlan = [
    check('name').isString().withMessage('El nombre es requerido.'),
    check('description').isString().withMessage('La descripción es requerida.'),
    check('price').isNumeric().withMessage('El precio es requerido.'),
    check('trial_days')
        .isNumeric()
        .withMessage('Los días de prueba son requeridos.'),
    check('duration_days')
        .isNumeric()
        .withMessage('Los días de duración son requeridos.'),
    check('url').isString().withMessage('La URL es requerida.'),
    validateResults,
]

// Validaciones para actualizar un Plan
const ValidatorUpdatePlan = [
    check('guid').isUUID().withMessage('El guid es requerido.'),
    check('name').isString().withMessage('El nombre es requerido.'),
    check('description').isString().withMessage('La descripción es requerida.'),
    check('price').isNumeric().withMessage('El precio es requerido.'),
    check('trial_days')
        .isNumeric()
        .withMessage('Los días de prueba son requeridos.'),
    check('duration_days')
        .isNumeric()
        .withMessage('Los días de duración son requeridos.'),
    check('url').isString().withMessage('La URL es requerida.'),
    validateResults,
]

// Validaciones para eliminar un Plan
const ValidatorDeletePlan = [
    check('guid').isUUID().withMessage('El guid es requerido.'),
    validateResults,
]

// Validaciones para deshabilitar un Plan
const ValidatorDisablePlan = [
    check('guid').isUUID().withMessage('El guid es requerido.'),
    validateResults,
]

// Validaciones para habilitar un Plan
const ValidatorEnablePlan = [
    check('guid').isUUID().withMessage('El guid es requerido.'),
    validateResults,
]

module.exports = {
    ValidatorCreatePlan,
    ValidatorUpdatePlan,
    ValidatorDeletePlan,
    ValidatorDisablePlan,
    ValidatorEnablePlan,
}
