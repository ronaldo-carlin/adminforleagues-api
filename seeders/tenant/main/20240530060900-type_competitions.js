const GUIDsTenant = require('../../guids_tenant')

module.exports = {
    async up(queryInterface, Sequelize) {
        await queryInterface.bulkInsert('types_competitions', [
            {
                guid: GUIDsTenant.typesCompetitionsLigaGuid,
                sport_guid: GUIDsTenant.futbolGuid,
                category_guid: GUIDsTenant.categoriesLibreGuid,
                sede_guid: GUIDsTenant.sede1Guid,
                name: 'Liga',
                description: 'Liga',
                is_active: 1,
                created_at: new Date(),
            },
            {
                guid: GUIDsTenant.typesCompetitionsCopaGuid,
                sport_guid: GUIDsTenant.futbolGuid,
                category_guid: GUIDsTenant.categoriesLibreGuid,
                sede_guid: GUIDsTenant.sede1Guid,
                name: 'Copa',
                description: 'Copa',
                is_active: 1,
                created_at: new Date(),
            },
            {
                guid: GUIDsTenant.typesCompetitionsTorneoGuid,
                sport_guid: GUIDsTenant.futbolGuid,
                category_guid: GUIDsTenant.categoriesLibreGuid,
                sede_guid: GUIDsTenant.sede1Guid,
                name: 'Torneo',
                description: 'Torneo',
                is_active: 1,
                created_at: new Date(),
            },
            {
                guid: GUIDsTenant.typesCompetitionsEliminacionDirectaGuid,
                sport_guid: GUIDsTenant.futbolGuid,
                category_guid: GUIDsTenant.categoriesLibreGuid,
                sede_guid: GUIDsTenant.sede1Guid,
                name: 'eliminacion directa',
                description: 'eliminacion directa',
                is_active: 1,
                created_at: new Date(),
            },
            {
                guid: GUIDsTenant.typesCompetitionsEntrenamientoGuid,
                sport_guid: GUIDsTenant.futbolGuid,
                category_guid: GUIDsTenant.categoriesLibreGuid,
                sede_guid: GUIDsTenant.sede1Guid,
                name: 'Entrenamiento',
                description: 'Entrenamiento',
                is_active: 1,
                created_at: new Date(),
            },
            {
                guid: GUIDsTenant.typesCompetitionsAmistosoGuid,
                sport_guid: GUIDsTenant.futbolGuid,
                category_guid: GUIDsTenant.categoriesLibreGuid,
                sede_guid: GUIDsTenant.sede1Guid,
                name: 'Amistoso',
                description: 'Amistoso',
                is_active: 1,
                created_at: new Date(),
            },
        ])
    },

    async down(queryInterface, Sequelize) {
        await queryInterface.bulkDelete('types_competitions', null, {})
    },
}
