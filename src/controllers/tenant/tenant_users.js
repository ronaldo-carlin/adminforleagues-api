const { Op } = require('sequelize')
const { v4: uuidv4 } = require('uuid')
const { matchedData } = require('express-validator')
const PUBLIC_URL = process.env.PUBLIC_URL + '/users'
const { handleHttpSuccess } = require('../../utils/handleSuccess')
const { handleHttpError } = require('../../utils/handleError')
const { handleHttpCount } = require('../../utils/handleCount')
const { encrypt } = require('../../utils/handlePassword')
const { getSequelize, getUserModel } = require('../../helpers/sequelizeHelper')

/**
 * Este controlador es el encargado de registrar un cliente
 * @param {*} req
 * @param {*} res
 */
const CreateUser = async (req, res) => {
    try {
        // obtenemos la instancia de sequelize del middleware
        const sequelize = getSequelize(req)
        // obtenemos el modelo de usuario
        const AuthTenantModel = getUserModel(sequelize)
        //Obtenemos y limpiamos el body del request
        const body = matchedData(req)
        //Obtenemos el usuario que solicita la peticion
        const created_by = req.user.guid
        //Verificamos si el cliente ya existe en la base de datos
        const user = await AuthTenantModel.findOne({
            where: {
                [Op.or]: [
                    {
                        email: body.email,
                    },
                    {
                        phone: body.phone,
                    },
                ],
            },
        })
        //Si el cliente ya existe enviamos una respuesta de error
        if (user) {
            handleHttpError(res, 'El cliente ya existe.', 403)
            return
        }
        //Encriptamos la contraseña
        if (!body.password) {
            // si no encriptamos su numero de telefono como contraseña
            body.password = body.phone
        }
        const encryptedPassword = await encrypt(body.password)
        //Creamos un guid para el cliente
        const guid = await uuidv4()
        // Prepara el objeto para la creación del usuario
        const newUser = {
            guid,
            role_guid: body.role_guid,
            first_name: body.first_name,
            last_name: body.last_name,
            email: body.email,
            phone: body.phone,
            country: 'México',
            state: body.state,
            city: body.city,
            password: encryptedPassword,
            isVerified: 0,
            is_active: 1,
            created_at: new Date(),
            created_by: created_by,
        }

        // Agrega tenant_guid solo si está presente en el cuerpo
        if (body.tenant_guid) {
            newUser.tenant_guid = body.tenant_guid
        }

        await AuthTenantModel.create(newUser)
        //Enviamos una respuesta de éxito
        handleHttpSuccess(res, 'Cliente creado.', 201)
    } catch (error) {
        console.log('error register:', error)
        //Obtener y devolver el error
        handleHttpError(res, 'Error al crear el cliente.', 403, error)
    }
}

/**
 * Este controlador es el encargado de obtener los usuarios activos
 * @param {*} req
 * @param {*} res
 */
const Users = async (req, res) => {
    try {
        // obtenemos la instancia de sequelize del middleware
        const sequelize = getSequelize(req)
        // obtenemos el modelo de usuario
        const AuthTenantModel = getUserModel(sequelize)
        //Buscamos todos los clientes
        const user = await AuthTenantModel.findAll({
            where: { is_active: 1 },
            attributes: {
                //Incluimos los atributos que queremos mostrar
                include: ['updated_at'],
                //Excluimos las contraseñas
                exclude: ['password'],
            },
            order: [['id', 'DESC']],
        })
        //Si no existen clientes enviamos error
        if (!user) {
            handleHttpError(res, 'El cliente no existe.', 404)
            return
        }
        const message = user.length
            ? 'Se han encontrado los datos.'
            : 'No se encontraron datos.'
        handleHttpSuccess(res, message, 201, user)
    } catch (error) {
        console.log(error)
        //Obtener y devolver el error
        handleHttpError(res, 'Error obteniendo clientes.', 403, error)
    }
}

/**
 * Este controlador es el encargado de obtener el cliente
 * @param {*} req
 * @param {*} res
 */
const User = async (req, res) => {
    try {
        // obtenemos la instancia de sequelize del middleware
        const sequelize = getSequelize(req)
        // obtenemos el modelo de usuario
        const AuthTenantModel = getUserModel(sequelize)

        const { guid } = req.params
        // Buscamos el referenciado en la base de datos
        const user = await AuthTenantModel.findOne({
            where: { guid },
        })

        //Si el cliente no existe enviamos una respuesta de error
        if (!user) {
            handleHttpError(res, 'El cliente no existe.', 404)
            return
        }
        //Si el cliente esta inactivo enviamos una respuesta de error
        if (user.is_active == 0) {
            handleHttpError(res, 'El cliente ha sido deshabilitado.', 401)
            return
        }

        //Si existe el referenciado lo enviamos al front
        handleHttpSuccess(
            res,
            'Se ha encontrado los datos del cliente.',
            201,
            user
        )
    } catch (error) {
        console.log(error)
        //Obtener y devolver el error
        handleHttpError(res, 'Error obteniendo cliente.', 403, error)
    }
}

/**
 * Este controlador es el encargado de actualizar el cliente
 * @param {*} req
 * @param {*} res
 */
const UpdateUser = async (req, res) => {
    try {
        // obtenemos la instancia de sequelize del middleware
        const sequelize = getSequelize(req)
        // obtenemos el modelo de usuario
        const AuthTenantModel = getUserModel(sequelize)
        //Obtenemos el cliente enviado por el cuerpo
        const { guid } = req.params
        const updated_by = req.user.guid
        const updated_at = new Date()
        //Obtenemos el body del request que será actualizado
        const data = matchedData(req)
        data.updated_by = updated_by
        data.updated_at = updated_at

        const [updateCount] = await AuthTenantModel.update(data, {
            where: { guid },
        })
        if (updateCount === 0) {
            handleHttpError(res, 'El cliente no existe.', 404)
            return
        }
        const updatedUser = await AuthTenantModel.findOne({
            where: { guid },
            attributes: {
                exclude: ['password'],
            },
        })
        if (!updatedUser) {
            handleHttpError(
                res,
                'Error al obtener el cliente actualizado.',
                500
            )
            return
        }
        //Enviamos los datos actualizados
        handleHttpSuccess(res, 'Usuario actualizado.', 201, updatedUser)
    } catch (error) {
        //Obtener y devolver el error
        handleHttpError(res, 'Error actualizando el cliente.', 403, error)
    }
}

/**
 * Este controlador es el encargado de eliminar el cliente
 * @param {*} req
 * @param {*} res
 */
const DeleteUser = async (req, res) => {
    try {
        // obtenemos la instancia de sequelize del middleware
        const sequelize = getSequelize(req)
        // obtenemos el modelo de usuario
        const AuthTenantModel = getUserModel(sequelize)

        const { guid } = req.params
        const deleted_by = req.user.guid
        const deleted_at = new Date()
        const [updateCount] = await AuthTenantModel.update(
            {
                is_active: 0,
                deleted_by,
                deleted_at,
            },
            {
                where: { guid },
            }
        )
        if (updateCount === 0) {
            return handleHttpError(res, 'El usuario no existe.', 404)
        }
        handleHttpSuccess(res, 'Se ha eliminado el usuario.', 201)
    } catch (error) {
        console.log(error)
        //Obtener y devolver el error
        handleHttpError(res, 'Error al eliminar el cliente.', 403, error)
    }
}

/**
 * Este controlador es el encargado de desactivar el cliente
 * @param {*} req
 * @param {*} res
 */
const DisableUser = async (req, res) => {
    try {
        // obtenemos la instancia de sequelize del middleware
        const sequelize = getSequelize(req)
        // obtenemos el modelo de usuario
        const AuthTenantModel = getUserModel(sequelize)

        const { guid } = req.params
        const updated_by = req.user.guid
        const updated_at = new Date()

        const [updateCount] = await AuthTenantModel.update(
            {
                is_active: 0,
                updated_by,
                updated_at,
            },
            {
                where: { guid },
            }
        )

        if (updateCount === 0) {
            return handleHttpError(res, 'El pago no existe.', 404)
        }

        handleHttpSuccess(res, 'Usuario deshabilitado.', 201)
    } catch (error) {
        console.log(error)
        handleHttpError(res, 'Error deshabilitando el cliente.', 403, error)
    }
}

/**
 * Este controlador es el encargado de activar el cliente
 * @param {*} req
 * @param {*} res
 */
const EnableUser = async (req, res) => {
    try {
        // obtenemos la instancia de sequelize del middleware
        const sequelize = getSequelize(req)
        // obtenemos el modelo de usuario
        const AuthTenantModel = getUserModel(sequelize)

        const { guid } = req.params
        const updated_by = req.user.guid
        const updated_at = new Date()
        const [updateCount] = await AuthTenantModel.update(
            {
                is_active: 1,
                updated_by,
                updated_at,
            },
            {
                where: { guid },
            }
        )
        if (updateCount === 0) {
            return handleHttpError(res, 'El pago no existe.', 404)
        }
        handleHttpSuccess(res, 'Usuario habilidato.', 201)
    } catch (error) {
        console.log(error)
        //Obtener y devolver el error
        handleHttpError(res, 'Error habilitando el cliente.', 403, error)
    }
}

/**
 * Este controlador es el encargado de restablecer la contraseña
 * @param {*} req
 * @param {*} res
 */
const EncryptPasswordsReset = async (req, res) => {
    try {
        // obtenemos la instancia de sequelize del middleware
        const sequelize = getSequelize(req)
        // obtenemos el modelo de usuario
        const AuthTenantModel = getUserModel(sequelize)

        const { guid } = req.params
        const { new_password } = req.body
        const updated_by = req.user.guid
        const updated_at = new Date()

        if (!new_password) {
            return handleHttpError(
                res,
                'La nueva contraseña es requerida.',
                400
            )
        }

        const encryptedPassword = await encrypt(new_password)
        const [updateCount] = await AuthTenantModel.update(
            {
                password: encryptedPassword,
                updated_by,
                updated_at,
            },
            {
                where: { guid },
            }
        )
        if (updateCount === 0) {
            return handleHttpError(res, 'El usuario no existe.', 404)
        }
        handleHttpSuccess(res, 'Contraseña restablecida.', 201)
    } catch (error) {
        console.log(error)
        handleHttpError(res, 'Error restableciendo la contraseña.', 403, error)
    }
}

/**
 * Este controlador es el encargado de subir una imagen del cliente
 * @param {*} req
 * @param {*} res
 */
const UploadProfilePicture = async (req, res) => {
    try {
        // obtenemos la instancia de sequelize del middleware
        const sequelize = getSequelize(req)
        // obtenemos el modelo de usuario
        const AuthTenantModel = getUserModel(sequelize)

        console.log('🚀 ~ uploadProfilePicture ~ req.file:', req.file)
        // Verificar si se recibió la imagen en la solicitud
        if (!req.file) {
            return handleHttpError(
                res,
                'No se ha recibido ninguna imagen.',
                400
            )
        }

        // Ruta donde se guardará la imagen en el almacenamiento
        const logoPath = req.file.path

        // Obtener el ID del usuario del token de autenticación
        const guid = req.params.guid

        // Actualizar el campo logo_path del usuario en la base de datos
        await AuthTenantModel.update(
            {
                logo_path: `${PUBLIC_URL}/images/${req.file.filename}`,
            },
            {
                where: { guid: guid },
                returning: true,
            }
        )

        // Preparar la respuesta con los datos de la imagen
        const responseData = {
            filename: req.file.filename,
            logo_path: logoPath,
            url: `${PUBLIC_URL}/images/${req.file.filename}`,
        }
        console.log('🚀 ~ uploadProfilePicture ~ fileData:', responseData)

        handleHttpSuccess(res, true, 'Imagen subida.', 201, responseData)
    } catch (error) {
        console.log('🚀 ~ uploadProfilePicture ~ error:', error)
        //Obtener y devolver el error
        handleHttpError(res, 'Error al guardar la imagen.', 403)
    }
}

/**
 * Este controlador es el encargado de obtener todos los usuarios
 * @param {*} req
 * @param {*} res
 */
const UsersAll = async (req, res) => {
    try {
        // obtenemos la instancia de sequelize del middleware
        const sequelize = getSequelize(req)
        // obtenemos el modelo de usuario
        const AuthTenantModel = getUserModel(sequelize)

        const DataUsers = await AuthTenantModel.findAll()
        if (!DataUsers) {
            handleHttpError(res, 'No se encontraron datos.', 404)
            return
        }
        const message = DataUsers.length
            ? 'Se han encontrado los datos.'
            : 'No se encontraron datos.'
        handleHttpSuccess(res, message, 201, DataUsers)
    } catch (error) {
        console.log(error)
        handleHttpError(res, '', 403, error)
    }
}

/**
 * Este controlador es el encargado de contar todos los pagos
 * @param {*} req
 * @param {*} res
 */
const UsersCount = async (req, res) => {
    try {
        // obtenemos la instancia de sequelize del middleware
        const sequelize = getSequelize(req)
        // obtenemos el modelo de usuario
        const AuthTenantModel = getUserModel(sequelize)

        const DataUsers = await AuthTenantModel.count()
        if (!DataUsers) {
            handleHttpError(res, 'No se encontraron datos.', 404)
            return
        }
        handleHttpCount(res, 'Se han encontrado los datos.', 201, DataUsers)
    } catch (error) {
        console.log(error)
        handleHttpError(res, '', 403, error)
    }
}

module.exports = {
    CreateUser,
    Users,
    User,
    UpdateUser,
    DeleteUser,
    DisableUser,
    EnableUser,
    EncryptPasswordsReset,
    UploadProfilePicture,
    UsersAll,
    UsersCount,
}
