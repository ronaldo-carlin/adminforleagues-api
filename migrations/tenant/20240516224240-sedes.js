module.exports = {
    async up(queryInterface, Sequelize) {
        // entidad para las canchas
        await queryInterface.createTable('sedes', {
            // identificador de la tabla
            id: {
                type: Sequelize.INTEGER,
                primaryKey: true,
                autoIncrement: true,
                allowNull: false,
            },
            // identificador unico de la sede
            guid: {
                type: Sequelize.UUID,
                defaultValue: Sequelize.UUIDV4,
                unique: true,
                allowNull: false,
            },
            // nombre de la sede
            name: {
                type: Sequelize.STRING,
            },
            // descripcion de la sede
            description: {
                type: Sequelize.STRING,
            },
            // horario de la sede
            schedule: {
                type: Sequelize.STRING,
            },
            // direccion de la sede
            address: {
                type: Sequelize.STRING,
            },
            // ciudad de la sede
            city: {
                type: Sequelize.STRING,
            },
            // estado de la sede
            state: {
                type: Sequelize.STRING,
            },
            // pais de la sede
            country: {
                type: Sequelize.STRING,
            },
            // capacidad de la sede
            capacity: {
                type: Sequelize.INTEGER,
            },
            // latitud de la sede
            latitude: {
                type: Sequelize.DOUBLE,
            },
            // longitud de la sede
            longitude: {
                type: Sequelize.DOUBLE,
            },
            // imagen de la sede
            image_path: {
                type: Sequelize.STRING,
            },
            // estado del registro
            is_active: {
                type: Sequelize.BOOLEAN,
                defaultValue: 1, // valor por defecto
            },
            // usuario que creo el registro
            created_by: {
                type: Sequelize.UUID,
            },
            // fecha de creacion
            created_at: {
                type: Sequelize.DATE,
                defaultValue: Sequelize.literal('CURRENT_TIMESTAMP'),
            },
            // usuario que actualizo el registro
            updated_by: {
                type: Sequelize.UUID,
            },
            // fecha de actualizacion
            updated_at: {
                type: Sequelize.DATE,
            },
            // usuario que elimino el registro
            deleted_by: {
                type: Sequelize.UUID,
            },
            // fecha de eliminacion
            deleted_at: {
                type: Sequelize.DATE,
            },
        })

        await queryInterface.addIndex('sedes', ['guid'], {
            unique: true,
            name: 'uniq_sedes_guid',
        })
    },

    async down(queryInterface, Sequelize) {
        await queryInterface.removeIndex('sedes', 'uniq_sedes_guid')
        await queryInterface.dropTable('sedes')
    },
}
