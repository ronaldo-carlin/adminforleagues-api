const { matchedData } = require('express-validator')
const PaymentsModel = require('../../models/general/payments')
const { handleHttpError } = require('../../utils/handleError')
const { handleHttpSuccess } = require('../../utils/handleSuccess')
const { handleHttpCount } = require('../../utils/handleCount')

/**
 * Este controlador es el encargado de crear pagos
 * @param {*} req
 * @param {*} res
 */
const CreatePayment = async (req, res) => {
    try {
        const data = matchedData(req)
        const created_by = req.user.guid
        const created_at = new Date()
        data.created_by = created_by
        data.created_at = created_at
        const payment = await PaymentsModel.create(data)
        return handleHttpSuccess(res, 'Se ha creado el pago.', 201, payment)
    } catch (error) {
        console.log(error)
        return handleHttpError(res, '', 403, error)
    }
}

/**
 * Este controlador es el encargado de obtener todos los pagos activos
 * @param {*} req
 * @param {*} res
 */
const Payments = async (req, res) => {
    try {
        const payments = await PaymentsModel.findAll({
            where: { is_active: 1 },
        })
        if (!payments) {
            return handleHttpError(res, 'El pago no existe.', 404)
        }
        const message = payments.length
            ? 'Se han encontrado los datos.'
            : 'No se encontraron datos.'
        return handleHttpSuccess(res, message, 201, payments)
    } catch (error) {
        console.log(error)
        return handleHttpError(res, '', 403, error)
    }
}

/**
 * Este controlador es el encargado de obtener pagos por guid
 * @param {*} req
 * @param {*} res
 */
const Payment = async (req, res) => {
    try {
        const { guid } = req.params
        const DataPayment = await PaymentsModel.findOne({
            where: { guid },
        })
        if (!DataPayment) {
            return handleHttpError(res, 'El pago no existe.', 404)
        }
        return handleHttpSuccess(
            res,
            'Se han encontrado los datos.',
            201,
            DataPayment
        )
    } catch (error) {
        console.log(error)
        return handleHttpError(res, '', 403, error)
    }
}

/**
 * Este controlador es el encargado de actualizar pagos
 * @param {*} req
 * @param {*} res
 */
const UpdatePayment = async (req, res) => {
    try {
        const { guid } = req.params
        const updated_by = req.user.guid
        const updated_at = new Date()
        const data = matchedData(req)
        data.updated_by = updated_by
        data.updated_at = updated_at
        const [updateCount] = await PaymentsModel.update(data, {
            where: { guid },
        })
        if (updateCount === 0) {
            return handleHttpError(res, 'El pago no existe.', 404)
        }
        const updatedPayment = await PaymentsModel.findOne({
            where: { guid },
        })
        if (!updatedPayment) {
            return handleHttpError(
                res,
                'Error al obtener el pago actualizado.',
                500
            )
        }
        return handleHttpSuccess(
            res,
            'Se ha actualizado el pago.',
            201,
            updatedPayment
        )
    } catch (error) {
        console.log(error)
        return handleHttpError(res, '', 403, error)
    }
}

/**
 * Este controlador es el encargado de eliminar pagos
 * @param {*} req
 * @param {*} res
 */
const DeletePayment = async (req, res) => {
    try {
        const { guid } = req.params
        const deleted_by = req.user.guid
        const deleted_at = new Date()
        const [updateCount] = await PaymentsModel.update(
            {
                is_active: 0,
                deleted_by,
                deleted_at,
            },
            {
                where: { guid },
            }
        )
        if (updateCount === 0) {
            return handleHttpError(res, 'El pago no existe.', 404)
        }
        handleHttpSuccess(res, 'Se ha eliminado el pago.', 201)
    } catch (error) {
        console.log(error)
        handleHttpError(res, '', 403, error)
    }
}

/**
 * Este controlador es el encargado de desactivar pagos
 * @param {*} req
 * @param {*} res
 */
const DisablePayment = async (req, res) => {
    try {
        const { guid } = req.params
        const updated_by = req.user.guid
        const updated_at = new Date()
        const [updateCount] = await PaymentsModel.update(
            {
                is_active: 0,
                updated_by,
                updated_at,
            },
            {
                where: { guid },
            }
        )
        if (updateCount === 0) {
            return handleHttpError(res, 'El pago no existe.', 404)
        }
        handleHttpSuccess(res, 'Se ha desactivado el pago.', 201)
    } catch (error) {
        console.log(error)
        handleHttpError(res, '', 403, error)
    }
}

/**
 * Este controlador es el encargado de activar pagos
 * @param {*} req
 * @param {*} res
 */
const EnablePayment = async (req, res) => {
    try {
        const { guid } = req.params
        const updated_by = req.user.guid
        const updated_at = new Date()
        const [updateCount] = await PaymentsModel.update(
            {
                is_active: 1,
                updated_by,
                updated_at,
            },
            {
                where: { guid },
            }
        )
        if (updateCount === 0) {
            return handleHttpError(res, 'El pago no existe.', 404)
        }
        handleHttpSuccess(res, 'Se ha activado el pago.', 201)
    } catch (error) {
        console.log(error)
        handleHttpError(res, '', 403, error)
    }
}

/**
 * Este controlador es el encargado de obtener todos los pagos
 * @param {*} req
 * @param {*} res
 */
const PaymentsAll = async (req, res) => {
    try {
        const DataPayments = await PaymentsModel.findAll()
        if (!DataPayments) {
            handleHttpError(res, 'No se encontraron datos.', 404)
            return
        }
        const message = DataPayments.length
            ? 'Se han encontrado los datos.'
            : 'No se encontraron datos.'
        handleHttpSuccess(res, message, 201, DataPayments)
    } catch (error) {
        console.log(error)
        handleHttpError(res, '', 403, error)
    }
}

/**
 * Este controlador es el encargado de contar todos los pagos
 * @param {*} req
 * @param {*} res
 */
const PaymentsCount = async (req, res) => {
    try {
        const DataPayments = await PaymentsModel.count()
        if (!DataPayments) {
            handleHttpError(res, 'No se encontraron datos.', 404)
            return
        }
        handleHttpCount(res, 'Se han encontrado los datos.', 201, DataPayments)
    } catch (error) {
        console.log(error)
        handleHttpError(res, '', 403, error)
    }
}

module.exports = {
    CreatePayment,
    Payments,
    Payment,
    UpdatePayment,
    DeletePayment,
    DisablePayment,
    EnablePayment,
    PaymentsAll,
    PaymentsCount,
}
