module.exports = {
    async up(queryInterface, Sequelize) {
        // entidad para los cupones de descuento
        await queryInterface.createTable('payment_types', {
            // identificador del registro
            id: {
                type: Sequelize.INTEGER,
                primaryKey: true,
                autoIncrement: true,
                allowNull: false,
            },
            // guid del registro
            guid: {
                type: Sequelize.UUID,
                defaultValue: Sequelize.UUIDV4, // Genera un UUID por defecto
                unique: true,
                allowNull: false,
            },
            // tipo de pago
            name: {
                type: Sequelize.STRING(100),
                unique: true,
            },
            // descripcion del tipo de pago
            description: {
                type: Sequelize.TEXT,
            },
            // estado del registro
            is_active: {
                type: Sequelize.BOOLEAN,
                defaultValue: 1, // valor por defecto
            },
            // usuario que creo el registro
            created_by: {
                type: Sequelize.UUID,
            },
            // fecha de creacion
            created_at: {
                type: Sequelize.DATE,
                defaultValue: Sequelize.literal('CURRENT_TIMESTAMP'),
            },
            // usuario que actualizo el registro
            updated_by: {
                type: Sequelize.UUID,
            },
            // fecha de actualizacion
            updated_at: {
                type: Sequelize.DATE,
            },
            // usuario que elimino el registro
            deleted_by: {
                type: Sequelize.UUID,
            },
            // fecha de eliminacion
            deleted_at: {
                type: Sequelize.DATE,
            },
        })

        // Agregar índices después de crear la tabla
        await queryInterface.addIndex('payment_types', ['guid'], {
            unique: true,
            name: 'uniq_payment_types_guid',
        })
    },

    async down(queryInterface, Sequelize) {
        await queryInterface.removeIndex(
            'payment_types',
            'uniq_payment_types_guid'
        )
        await queryInterface.dropTable('payment_types')
    },
}
