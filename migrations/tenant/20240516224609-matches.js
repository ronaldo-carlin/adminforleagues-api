module.exports = {
    async up(queryInterface, Sequelize) {
        // entidad para los partidos
        await queryInterface.createTable('matches', {
            // identificador unico del partido
            id: {
                type: Sequelize.INTEGER,
                primaryKey: true,
                autoIncrement: true,
                allowNull: false,
            },
            // identificador unico del partido
            guid: {
                type: Sequelize.UUID,
                defaultValue: Sequelize.UUIDV4,
                unique: true,
                allowNull: false,
            },
            // equipo local
            home_team_guid: {
                type: Sequelize.UUID,
            },
            // equipo visitante
            away_team_guid: {
                type: Sequelize.UUID,
            },
            // competencia
            competition_guid: {
                type: Sequelize.UUID,
            },
            // sede
            sede_guid: {
                type: Sequelize.UUID,
            },
            // arbitro
            referee_guid: {
                type: Sequelize.UUID,
            },
            // fecha del partido
            date: {
                type: Sequelize.DATE,
            },
            // hora del partido
            time: {
                type: Sequelize.TIME,
            },
            // fase del partido
            group: {
                type: Sequelize.TEXT,
            },
            // estado del partido
            is_active: {
                type: Sequelize.BOOLEAN,
                defaultValue: 1, // valor por defecto
            },
            // usuario que creo el registro
            created_by: {
                type: Sequelize.UUID,
            },
            // fecha de creacion
            created_at: {
                type: Sequelize.DATE,
                defaultValue: Sequelize.literal('CURRENT_TIMESTAMP'),
            },
            // usuario que actualizo el registro
            updated_by: {
                type: Sequelize.UUID,
            },
            // fecha de actualizacion
            updated_at: {
                type: Sequelize.DATE,
            },
            // usuario que elimino el registro
            deleted_by: {
                type: Sequelize.UUID,
            },
            // fecha de eliminacion
            deleted_at: {
                type: Sequelize.DATE,
            },
        })

        await queryInterface.addIndex('matches', ['guid'], {
            unique: true,
            name: 'uniq_matches_guid',
        })

        await queryInterface.addIndex('matches', ['home_team_guid'], {
            name: 'idx_home_team_guid',
        })

        await queryInterface.addIndex('matches', ['away_team_guid'], {
            name: 'idx_away_team_guid',
        })

        await queryInterface.addIndex('matches', ['competition_guid'], {
            name: 'idx_competition_guid',
        })

        await queryInterface.addIndex('matches', ['sede_guid'], {
            name: 'idx_sede_guid',
        })

        await queryInterface.addIndex('matches', ['referee_guid'], {
            name: 'idx_referee_guid',
        })

        await queryInterface.addConstraint('matches', {
            fields: ['home_team_guid'],
            type: 'foreign key',
            name: 'fk_matche_home_team',
            references: {
                table: 'teams',
                field: 'guid',
            },
            onUpdate: 'CASCADE',
            onDelete: 'NO ACTION',
        })

        await queryInterface.addConstraint('matches', {
            fields: ['away_team_guid'],
            type: 'foreign key',
            name: 'fk_matche_away_team',
            references: {
                table: 'teams',
                field: 'guid',
            },
            onUpdate: 'CASCADE',
            onDelete: 'NO ACTION',
        })

        await queryInterface.addConstraint('matches', {
            fields: ['competition_guid'],
            type: 'foreign key',
            name: 'fk_matche_competition',
            references: {
                table: 'competitions',
                field: 'guid',
            },
            onUpdate: 'CASCADE',
            onDelete: 'NO ACTION',
        })

        await queryInterface.addConstraint('matches', {
            fields: ['sede_guid'],
            type: 'foreign key',
            name: 'fk_matche_sede',
            references: {
                table: 'sedes',
                field: 'guid',
            },
            onUpdate: 'CASCADE',
            onDelete: 'NO ACTION',
        })

        await queryInterface.addConstraint('matches', {
            fields: ['referee_guid'],
            type: 'foreign key',
            name: 'fk_matche_referee',
            references: {
                table: 'users',
                field: 'guid',
            },
            onUpdate: 'CASCADE',
            onDelete: 'NO ACTION',
        })
    },

    async down(queryInterface, Sequelize) {
        await queryInterface.removeConstraint('matches', 'fk_matche_home_team')
        await queryInterface.removeConstraint('matches', 'fk_matche_away_team')
        await queryInterface.removeConstraint(
            'matches',
            'fk_matche_competition'
        )
        await queryInterface.removeConstraint('matches', 'fk_matche_sede')
        await queryInterface.removeConstraint('matches', 'fk_matche_referee')
        await queryInterface.removeIndex('matches', 'uniq_matches_guid')
        await queryInterface.removeIndex('matches', 'idx_home_team_guid')
        await queryInterface.removeIndex('matches', 'idx_away_team_guid')
        await queryInterface.removeIndex('matches', 'idx_competition_guid')
        await queryInterface.removeIndex('matches', 'idx_sede_guid')
        await queryInterface.removeIndex('matches', 'idx_referee_guid')
        await queryInterface.dropTable('matches')
    },
}
