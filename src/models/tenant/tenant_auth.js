// Importar Sequelize
const { DataTypes } = require('sequelize')
const RolesTenantModelFactory = require('./tenant_roles')

// Definir la función factory para el modelo AuthTenantModel
const AuthTenantModelFactory = (sequelize) => {
    const RolesTenantModel = RolesTenantModelFactory(sequelize)

    const AuthTenantModel = sequelize.define(
        'users',
        {
            id: {
                type: DataTypes.INTEGER,
                primaryKey: true,
                autoIncrement: true,
            },
            guid: {
                type: DataTypes.UUID,
                primaryKey: true,
                defaultValue: DataTypes.UUIDV4,
                unique: true,
            },
            username: {
                type: DataTypes.TEXT,
            },
            first_name: {
                type: DataTypes.TEXT,
            },
            last_name: {
                type: DataTypes.TEXT,
            },
            email: {
                type: DataTypes.TEXT,
            },
            password: {
                type: DataTypes.TEXT,
            },
            role_guid: {
                type: DataTypes.TEXT,
            },
            phone: {
                type: DataTypes.INTEGER,
            },
            biography: {
                type: DataTypes.TEXT,
            },
            isVerified: {
                type: DataTypes.BOOLEAN,
            },
            image_path: {
                type: DataTypes.TEXT,
            },
            picture: {
                type: DataTypes.TEXT,
            },
            country: {
                type: DataTypes.TEXT,
            },
            state: {
                type: DataTypes.TEXT,
            },
            city: {
                type: DataTypes.TEXT,
            },
            google_id: {
                type: DataTypes.TEXT,
            },
            facebook_id: {
                type: DataTypes.TEXT,
            },
            two_factor_auth: {
                type: DataTypes.INTEGER,
            },
            weeklyNewsletter: {
                type: DataTypes.INTEGER,
            },
            lifecycleEmails: {
                type: DataTypes.INTEGER,
            },
            promotionalEmails: {
                type: DataTypes.INTEGER,
            },
            productUpdates: {
                type: DataTypes.INTEGER,
            },
            last_login: {
                type: DataTypes.DATE,
            },
            facebook: {
                type: DataTypes.TEXT,
            },
            instagram: {
                type: DataTypes.TEXT,
            },
            is_active: {
                type: DataTypes.INTEGER,
            },
            created_by: {
                type: DataTypes.UUID,
            },
            updated_by: {
                type: DataTypes.UUID,
            },
            deleted_by: {
                type: DataTypes.UUID,
            },
            created_at: {
                type: DataTypes.DATE,
            },
            updated_at: {
                type: DataTypes.DATE,
            },
            deleted_at: {
                type: DataTypes.DATE,
            },
        },
        {
            tableName: 'users',
            timestamps: false,
            defaultScope: {
                attributes: {
                    exclude: [
                        'created_by',
                        'created_at',
                        'updated_by',
                        'updated_at',
                        'deleted_by',
                        'deleted_at',
                    ],
                },
                include: [
                    {
                        model: RolesTenantModel,
                        as: 'role',
                        where: { is_active: 1 },
                        attributes: ['guid', 'name'],
                    },
                ],
            },
        }
    )

    AuthTenantModel.belongsTo(RolesTenantModel, {
        foreignKey: 'role_guid',
        targetKey: 'guid',
        as: 'role',
    })

    return AuthTenantModel
}

module.exports = AuthTenantModelFactory
