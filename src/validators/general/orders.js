const { check } = require('express-validator')
const validateResults = require('../../utils/handleValidators')

// Validaciones para crear una Orden
const ValidatorCreateOrder = [
    check('user_guid')
        .isUUID()
        .withMessage('El GUID del usuario es requerido y debe ser un UUID.'),
    check('plan_guid')
        .isUUID()
        .withMessage('El GUID del plan es requerido y debe ser un UUID.'),
    check('coupon_guid')
        .optional()
        .isUUID()
        .withMessage('El GUID del cupon debe ser un UUID si se proporciona.'),
    check('transaction').isString().withMessage('La transacción es requerida.'),
    check('payment_method')
        .isString()
        .withMessage('El método de pago es requerido.'),
    check('amount').isNumeric().withMessage('El monto es requerido.'),
    check('currency').isString().withMessage('La moneda es requerida.'),
    check('currency_icon')
        .isString()
        .withMessage('El ícono de la moneda es requerido.'),
    validateResults,
]

// Validaciones para actualizar una Orden
const ValidatorUpdateOrder = [
    check('guid')
        .isUUID()
        .withMessage('El guid es requerido y debe ser un UUID.'),
    check('user_guid')
        .optional()
        .isString()
        .withMessage('El GUID del usuario debe ser un UUID si se proporciona.'),
    check('plan_guid')
        .optional()
        .isUUID()
        .withMessage('El GUID del plan debe ser un UUID si se proporciona.'),
    check('coupon_guid')
        .optional()
        .isUUID()
        .withMessage('El GUID del cupon debe ser un UUID si se proporciona.'),
    check('transaction')
        .optional()
        .isString()
        .withMessage('La transacción debe ser un string si se proporciona.'),
    check('payment_method')
        .optional()
        .isString()
        .withMessage('El método de pago debe ser un string si se proporciona.'),
    check('amount')
        .optional()
        .isNumeric()
        .withMessage('El monto debe ser un número si se proporciona.'),
    check('currency')
        .optional()
        .isString()
        .withMessage('La moneda debe ser un string si se proporciona.'),
    check('currency_icon')
        .optional()
        .isString()
        .withMessage(
            'El ícono de la moneda debe ser un string si se proporciona.'
        ),
    validateResults,
]

// Validaciones para eliminar una Orden
const ValidatorDeleteOrder = [
    check('guid')
        .isUUID()
        .withMessage('El guid es requerido y debe ser un UUID.'),
    validateResults,
]

// Validaciones para deshabilitar una Orden
const ValidatorDisableOrder = [
    check('guid')
        .isUUID()
        .withMessage('El guid es requerido y debe ser un UUID.'),
    validateResults,
]

// Validaciones para habilitar una Orden
const ValidatorEnableOrder = [
    check('guid')
        .isUUID()
        .withMessage('El guid es requerido y debe ser un UUID.'),
    validateResults,
]

module.exports = {
    ValidatorCreateOrder,
    ValidatorUpdateOrder,
    ValidatorDeleteOrder,
    ValidatorDisableOrder,
    ValidatorEnableOrder,
}
