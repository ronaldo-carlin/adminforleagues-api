const GUIDsGeneral = require('../guids_general')

module.exports = {
    async up(queryInterface, Sequelize) {
        // Inserta un usuario por defecto
        await queryInterface.bulkInsert(
            'roles',
            [
                {
                    guid: GUIDsGeneral.roleSuperAdminGuid,
                    name: 'Super Administrador',
                    is_active: 1, // 1: Activo, 0: Inactivo
                    created_at: new Date(),
                },
                {
                    guid: GUIDsGeneral.roleClienteGuid,
                    name: 'cliente',
                    is_active: 1, // 1: Activo, 0: Inactivo
                    created_at: new Date(),
                },
            ],
            {}
        )
    },

    async down(queryInterface, Sequelize) {
        await queryInterface.bulkDelete('roles', null, {})
    },
}
