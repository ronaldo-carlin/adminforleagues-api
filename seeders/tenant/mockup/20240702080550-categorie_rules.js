const { v4: uuidv4 } = require('uuid')
const GUIDsTenant = require('../../guids_tenant')
module.exports = {
    async up(queryInterface, Sequelize) {
        // Inserta las reglas de las categorias
        await queryInterface.bulkInsert(
            'categorie_rules',
            [
                {
                    guid: uuidv4(),
                    category_guid: GUIDsTenant.categoriesLibreGuid,
                    title: 'Regla 1',
                    description: 'Descripcion de la regla 1',
                    created_at: new Date(),
                },
                {
                    guid: uuidv4(),
                    category_guid: GUIDsTenant.categoriesLibreGuid,
                    title: 'Regla 2',
                    description: 'Descripcion de la regla 2',
                    created_at: new Date(),
                },
                {
                    guid: uuidv4(),
                    category_guid: GUIDsTenant.categoriesFemenilGuid,
                    title: 'Regla 3',
                    description: 'Descripcion de la regla 3',
                    created_at: new Date(),
                },
                {
                    guid: uuidv4(),
                    category_guid: GUIDsTenant.categoriesInfantilGuid,
                    title: 'Regla 4',
                    description: 'Descripcion de la regla 4',
                    created_at: new Date(),
                },
            ],
            {}
        )
    },

    async down(queryInterface, Sequelize) {
        /**
         * Add commands to revert seed here.
         *
         * Example:
         * await queryInterface.bulkDelete('People', null, {});
         */
    },
}
