const { Sequelize } = require('sequelize')

if (process.env.NODE_ENV === 'production') {
    require('dotenv').config({ path: 'production.env' })
} else {
    require('dotenv').config({ path: '.env.dev' })
}

const database = process.env.POSTGRES_DATABASE
const username = process.env.POSTGRES_USER
const password = process.env.POSTGRES_PASSWORD
const host = process.env.POSTGRES_HOST

const sequelize = new Sequelize(database, username, password, {
    host,
    dialect: 'postgres',
    logging: false,
})

const dbConnectPostgresSql = async () => {
    try {
        await sequelize.authenticate()
        console.log('<<< connection to POSTGRES succeed >>>')
    } catch (err) {
        console.log('<<< error POSTGRES connection >>>', err)
    }
}

module.exports = { sequelize, dbConnectPostgresSql }
