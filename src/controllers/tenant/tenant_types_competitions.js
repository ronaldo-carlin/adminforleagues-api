const { Op } = require('sequelize')
const { v4: uuidv4 } = require('uuid')
const { matchedData } = require('express-validator')
const { handleHttpSuccess } = require('../../utils/handleSuccess')
const { handleHttpError } = require('../../utils/handleError')
const { handleHttpCount } = require('../../utils/handleCount')
const {
    getSequelize,
    getTypesCompetitionsModel,
} = require('../../helpers/sequelizeHelper')

/**
 * Este controlador es el encargado de registrar un tipo de competencia
 * @param {*} req
 * @param {*} res
 */
const CreateTypesCompetition = async (req, res) => {
    try {
        const sequelize = getSequelize(req)
        const TypesCompetitionModel = getTypesCompetitionsModel(sequelize)
        const data = matchedData(req)
        const created_by = req.user.guid
        const created_at = new Date()
        const guid = await uuidv4()
        data.created_by = created_by
        data.created_at = created_at
        data.guid = guid

        const typeCompetition = await TypesCompetitionModel.create(data)
        return handleHttpSuccess(
            res,
            'Tipo de competencia creado.',
            201,
            typeCompetition
        )
    } catch (error) {
        console.log('Error al crear el tipo de competencia:', error)
        handleHttpError(
            res,
            'Error al crear el tipo de competencia.',
            403,
            error
        )
    }
}

/**
 * Este controlador es el encargado de obtener los tipos de competencia activos
 * @param {*} req
 * @param {*} res
 */
const TypesCompetitions = async (req, res) => {
    try {
        const sequelize = getSequelize(req)
        const TypesCompetitionModel = getTypesCompetitionsModel(sequelize)

        const typesCompetitions = await TypesCompetitionModel.findAll({
            where: { is_active: 1 },
        })

        if (!typesCompetitions.length) {
            handleHttpError(res, 'No se encontraron tipos de competencia.', 404)
            return
        }

        handleHttpSuccess(
            res,
            'Tipos de competencia encontrados.',
            201,
            typesCompetitions
        )
    } catch (error) {
        console.log('Error obteniendo tipos de competencia:', error)
        handleHttpError(
            res,
            'Error obteniendo tipos de competencia.',
            403,
            error
        )
    }
}

/**
 * Este controlador es el encargado de obtener los tipos de competencia dependiendo del deporte seleccionado
 * @param {*} req
 * @param {*} res
 */
const TypesCompetitionsSport = async (req, res) => {
    try {
        const sequelize = getSequelize(req)
        const TypesCompetitionModel = getTypesCompetitionsModel(sequelize)
        const { guid } = req.params

        const typesCompetitions = await TypesCompetitionModel.findAll({
            where: { is_active: 1, sport_guid: guid },
        })

        if (!typesCompetitions.length) {
            handleHttpError(res, 'No se encontraron tipos de competencia.', 404)
            return
        }

        handleHttpSuccess(
            res,
            'Tipos de competencia encontrados.',
            201,
            typesCompetitions
        )
    } catch (error) {
        console.log('Error obteniendo tipos de competencia:', error)
        handleHttpError(
            res,
            'Error obteniendo tipos de competencia.',
            403,
            error
        )
    }
}

/**
 * Este controlador es el encargado de obtener un tipo de competencia por GUID
 * @param {*} req
 * @param {*} res
 */
const TypesCompetition = async (req, res) => {
    try {
        const sequelize = getSequelize(req)
        const TypesCompetitionModel = getTypesCompetitionsModel(sequelize)
        const { guid } = req.params

        const typeCompetition = await TypesCompetitionModel.findOne({
            where: { guid },
        })

        if (!typeCompetition) {
            handleHttpError(res, 'El tipo de competencia no existe.', 404)
            return
        }

        if (typeCompetition.is_active == 0) {
            handleHttpError(
                res,
                'El tipo de competencia ha sido deshabilitado.',
                401
            )
            return
        }

        handleHttpSuccess(
            res,
            'Tipo de competencia encontrado.',
            201,
            typeCompetition
        )
    } catch (error) {
        console.log('Error obteniendo tipo de competencia:', error)
        handleHttpError(
            res,
            'Error obteniendo tipo de competencia.',
            403,
            error
        )
    }
}

/**
 * Este controlador es el encargado de actualizar un tipo de competencia
 * @param {*} req
 * @param {*} res
 */
const UpdateTypesCompetition = async (req, res) => {
    try {
        const sequelize = getSequelize(req)
        const TypesCompetitionModel = getTypesCompetitionsModel(sequelize)
        const { guid } = req.params
        const data = matchedData(req)
        const updated_by = req.user.guid
        const updated_at = new Date()
        data.updated_by = updated_by
        data.updated_at = updated_at

        const [updateCount] = await TypesCompetitionModel.update(data, {
            where: { guid },
        })

        if (updateCount === 0) {
            return handleHttpError(
                res,
                'El tipo de competencia no existe.',
                404
            )
        }

        const typeCompetition = await TypesCompetitionModel.findOne({
            where: { guid },
        })

        if (!typeCompetition) {
            handleHttpError(res, 'El tipo de competencia no existe.', 404)
            return
        }

        if (typeCompetition.is_active == 0) {
            handleHttpError(
                res,
                'El tipo de competencia ha sido deshabilitado.',
                401
            )
            return
        }

        handleHttpSuccess(
            res,
            'Tipo de competencia actualizado.',
            201,
            typeCompetition
        )
    } catch (error) {
        console.log('Error actualizando tipo de competencia:', error)
        handleHttpError(
            res,
            'Error actualizando tipo de competencia.',
            403,
            error
        )
    }
}

/**
 * Este controlador es el encargado de eliminar un tipo de competencia
 * @param {*} req
 * @param {*} res
 */
const DeleteTypesCompetition = async (req, res) => {
    try {
        const sequelize = getSequelize(req)
        const TypesCompetitionModel = getTypesCompetitionsModel(sequelize)
        const { guid } = req.params
        const deleted_by = req.user.guid
        const deleted_at = new Date()
        const [updateCount] = await TypesCompetitionModel.update(
            {
                is_active: 0,
                deleted_by,
                deleted_at,
            },
            {
                where: { guid },
            }
        )

        if (updateCount === 0) {
            return handleHttpError(
                res,
                'El tipo de competencia no existe.',
                404
            )
        }

        handleHttpSuccess(res, 'Tipo de competencia eliminado.', 201)
    } catch (error) {
        console.log('Error eliminando tipo de competencia:', error)
        handleHttpError(
            res,
            'Error eliminando tipo de competencia.',
            403,
            error
        )
    }
}

/**
 * Este controlador es el encargado de desactivar un tipo de competencia
 * @param {*} req
 * @param {*} res
 */
const DisableTypesCompetition = async (req, res) => {
    try {
        const sequelize = getSequelize(req)
        const TypesCompetitionModel = getTypesCompetitionsModel(sequelize)
        const { guid } = req.params
        const updated_by = req.user.guid
        const updated_at = new Date()
        const [updateCount] = await TypesCompetitionModel.update(
            {
                is_active: 0,
                updated_by,
                updated_at,
            },
            {
                where: { guid },
            }
        )

        if (updateCount === 0) {
            return handleHttpError(
                res,
                'El tipo de competencia no existe.',
                404
            )
        }

        handleHttpSuccess(res, 'Tipo de competencia deshabilitado.', 201)
    } catch (error) {
        console.log('Error deshabilitando tipo de competencia:', error)
        handleHttpError(
            res,
            'Error deshabilitando tipo de competencia.',
            403,
            error
        )
    }
}

/**
 * Este controlador es el encargado de activar un tipo de competencia
 * @param {*} req
 * @param {*} res
 */
const EnableTypesCompetition = async (req, res) => {
    try {
        const sequelize = getSequelize(req)
        const TypesCompetitionModel = getTypesCompetitionsModel(sequelize)
        const { guid } = req.params
        const updated_by = req.user.guid
        const updated_at = new Date()
        const [updateCount] = await TypesCompetitionModel.update(
            {
                is_active: 1,
                updated_by,
                updated_at,
            },
            {
                where: { guid },
            }
        )

        if (updateCount === 0) {
            return handleHttpError(
                res,
                'El tipo de competencia no existe.',
                404
            )
        }

        handleHttpSuccess(res, 'Tipo de competencia habilitado.', 201)
    } catch (error) {
        console.log('Error habilitando tipo de competencia:', error)
        handleHttpError(
            res,
            'Error habilitando tipo de competencia.',
            403,
            error
        )
    }
}

/**
 * Este controlador es el encargado de obtener todos los tipos de competencia
 * @param {*} req
 * @param {*} res
 */
const TypesCompetitionsAll = async (req, res) => {
    try {
        const sequelize = getSequelize(req)
        const TypesCompetitionModel = getTypesCompetitionsModel(sequelize)

        const typesCompetitions = await TypesCompetitionModel.findAll()

        if (!typesCompetitions.length) {
            handleHttpError(res, 'No se encontraron tipos de competencia.', 404)
            return
        }

        handleHttpSuccess(
            res,
            'Tipos de competencia encontrados.',
            201,
            typesCompetitions
        )
    } catch (error) {
        console.log('Error obteniendo tipos de competencia:', error)
        handleHttpError(
            res,
            'Error obteniendo tipos de competencia.',
            403,
            error
        )
    }
}

/**
 * Este controlador es el encargado de contar todos los tipos de competencia
 * @param {*} req
 * @param {*} res
 */
const TypesCompetitionsCount = async (req, res) => {
    try {
        const sequelize = getSequelize(req)
        const TypesCompetitionModel = getTypesCompetitionsModel(sequelize)

        const count = await TypesCompetitionModel.count()

        handleHttpCount(res, 'Tipos de competencia encontrados.', 201, count)
    } catch (error) {
        console.log('Error contando tipos de competencia:', error)
        handleHttpError(res, 'Error contando tipos de competencia.', 403, error)
    }
}

module.exports = {
    CreateTypesCompetition,
    TypesCompetitions,
    TypesCompetitionsSport,
    TypesCompetition,
    UpdateTypesCompetition,
    DeleteTypesCompetition,
    DisableTypesCompetition,
    EnableTypesCompetition,
    TypesCompetitionsAll,
    TypesCompetitionsCount,
}
