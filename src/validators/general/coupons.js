const { check } = require('express-validator')
const validateResults = require('../../utils/handleValidators')

// Validaciones para crear un Coupon
const ValidatorCreateCoupon = [
    check('plan_guid')
        .isString()
        .withMessage('El guid del plan es requerido y de tipo guid'),
    check('code').isString().withMessage('El nombre del cupon es requerido.'),
    check('description').isString().withMessage('La descripción es requerida.'),
    check('discount_type')
        .isString()
        .withMessage('El tipo de descuento es requerida.'),
    check('discount_value')
        .isString()
        .withMessage('El valor del descuento es requerida.'),
    check('start_date')
        .isString()
        .withMessage('La fecha de incio es requerida.'),
    check('end_date')
        .isString()
        .withMessage('La fecha de expiración es requerida.'),
    check('uses')
        .isString()
        .withMessage('La cantidad de usos maximos es requerida.'),
    check('min_amount')
        .isString()
        .withMessage('La cantidad minima de la compra es requerida.'),
    check('max_amount')
        .isString()
        .withMessage('La cantidad maxima de la compra es requerida.'),
    validateResults,
]

// Validaciones para actualizar un Coupon
const ValidatorUpdateCoupon = [
    check('guid').isUUID().withMessage('El guid es requerido.'),
    check('plan_guid')
        .isString()
        .withMessage('El guid del plan es requerido y de tipo guid'),
    check('code').isString().withMessage('El nombre del cupon es requerido.'),
    check('description').isString().withMessage('La descripción es requerida.'),
    check('discount_type')
        .isString()
        .withMessage('El tipo de descuento es requerida.'),
    check('discount_value')
        .isString()
        .withMessage('El valor del descuento es requerida.'),
    check('start_date')
        .isString()
        .withMessage('La fecha de incio es requerida.'),
    check('end_date')
        .isString()
        .withMessage('La fecha de expiración es requerida.'),
    check('uses')
        .isString()
        .withMessage('La cantidad de usos maximos es requerida.'),
    check('min_amount')
        .isString()
        .withMessage('La cantidad minima de la compra es requerida.'),
    check('max_amount')
        .isString()
        .withMessage('La cantidad maxima de la compra es requerida.'),
    validateResults,
]

// Validaciones para eliminar un Coupon
const ValidatorDeleteCoupon = [
    check('guid').isUUID().withMessage('El guid es requerido.'),
    validateResults,
]

// Validaciones para deshabilitar un Coupon
const ValidatorDisableCoupon = [
    check('guid').isUUID().withMessage('El guid es requerido.'),
    validateResults,
]

// Validaciones para habilitar un Coupon
const ValidatorEnableCoupon = [
    check('guid').isUUID().withMessage('El guid es requerido.'),
    validateResults,
]

module.exports = {
    ValidatorCreateCoupon,
    ValidatorUpdateCoupon,
    ValidatorDeleteCoupon,
    ValidatorDisableCoupon,
    ValidatorEnableCoupon,
}
