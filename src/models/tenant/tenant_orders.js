// Importar Sequelize
const { DataTypes, Transaction } = require('sequelize')

// Definir la función factory para el modelo OrdersTenantModel
const OrdersTenantModelFactory = (sequelize) => {
    const OrdersTenantModel = sequelize.define(
        'orders',
        {
            id: {
                type: DataTypes.INTEGER,
                primaryKey: true,
                autoIncrement: true,
            },
            guid: {
                type: DataTypes.UUID,
                primaryKey: true,
                defaultValue: DataTypes.UUIDV4,
                unique: true,
            },
            user_guid: {
                type: DataTypes.TEXT,
            },
            plan_guid: {
                type: DataTypes.TEXT,
            },
            Transaction_guid: {
                type: DataTypes.TEXT,
            },
            payment_method: {
                type: DataTypes.TEXT,
            },
            amount: {
                type: DataTypes.INTEGER,
            },
            coupon_guid: {
                type: DataTypes.TEXT,
            },
            currency: {
                type: DataTypes.TEXT,
            },
            currency_icon: {
                type: DataTypes.TEXT,
            },
            currency_code: {
                type: DataTypes.TEXT,
            },
            enroll_start: {
                type: DataTypes.DATE,
            },
            enroll_end: {
                type: DataTypes.DATE,
            },
            expiration_date: {
                type: DataTypes.DATE,
            },
            renewal_date: {
                type: DataTypes.DATE,
            },
            cancel_date: {
                type: DataTypes.DATE,
            },
            cancel_type: {
                type: DataTypes.TEXT,
            },
            status: {
                type: DataTypes.TEXT,
            },
            is_active: {
                type: DataTypes.INTEGER,
                defaultValue: 1,
            },
            created_by: {
                type: DataTypes.UUID,
            },
            updated_by: {
                type: DataTypes.UUID,
            },
            deleted_by: {
                type: DataTypes.UUID,
            },
            created_at: {
                type: DataTypes.DATE,
            },
            updated_at: {
                type: DataTypes.DATE,
            },
            deleted_at: {
                type: DataTypes.DATE,
            },
        },
        {
            tableName: 'orders',
            timestamps: false,
        }
    )

    return OrdersTenantModel
}

module.exports = OrdersTenantModelFactory
