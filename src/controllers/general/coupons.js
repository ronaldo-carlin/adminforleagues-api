const { matchedData } = require('express-validator')
const CouponsModel = require('../../models/general/coupons')
const { handleHttpError } = require('../../utils/handleError')
const { handleHttpSuccess } = require('../../utils/handleSuccess')
const { handleHttpCount } = require('../../utils/handleCount')

/**
 * Este controlador es el encargado de crear cupones
 * @param {*} req
 * @param {*} res
 */
const CreateCoupon = async (req, res) => {
    try {
        const data = matchedData(req)
        const created_by = req.user.guid
        const created_at = new Date()
        data.created_by = created_by
        data.created_at = created_at
        const coupon = await CouponsModel.create(data)
        return handleHttpSuccess(res, 'Se ha creado el cupon.', 201, coupon)
    } catch (error) {
        console.log(error)
        return handleHttpError(res, '', 403, error)
    }
}

/**
 * Este controlador es el encargado de obtener todas los cupones activos
 * @param {*} req
 * @param {*} res
 */
const Coupons = async (req, res) => {
    try {
        const coupons = await CouponsModel.findAll({
            where: { is_active: 1 },
        })
        if (!coupons) {
            return handleHttpError(res, 'El cupon no existe.', 404)
        }
        const message = coupons.length
            ? 'Se han encontrado los datos.'
            : 'No se encontraron datos.'
        return handleHttpSuccess(res, message, 201, coupons)
    } catch (error) {
        console.log(error)
        return handleHttpError(res, '', 403, error)
    }
}

/**
 * Este controlador es el encargado de obtener cupones por guid
 * @param {*} req
 * @param {*} res
 */
const Coupon = async (req, res) => {
    try {
        const { guid } = req.params
        const DataCoupon = await CouponsModel.findOne({
            where: { guid },
        })
        if (!DataCoupon) {
            return handleHttpError(res, 'El cupon no existe.', 404)
        }
        return handleHttpSuccess(
            res,
            'Se han encontrado los datos.',
            201,
            DataCoupon
        )
    } catch (error) {
        console.log(error)
        return handleHttpError(res, '', 403, error)
    }
}

/**
 * Este controlador es el encargado de actualizar cupones
 * @param {*} req
 * @param {*} res
 */
const UpdateCoupon = async (req, res) => {
    try {
        const { guid } = req.params
        const updated_by = req.user.guid
        const updated_at = new Date()
        const data = matchedData(req)
        data.updated_by = updated_by
        data.updated_at = updated_at
        const [updateCount] = await CouponsModel.update(data, {
            where: { guid },
        })
        if (updateCount === 0) {
            return handleHttpError(res, 'El cupon no existe.', 404)
        }
        const updatedCoupon = await CouponsModel.findOne({
            where: { guid },
        })
        if (!updatedCoupon) {
            return handleHttpError(
                res,
                'Error al obtener el cupon actualizada.',
                500
            )
        }
        return handleHttpSuccess(
            res,
            'Se ha actualizado el cupon.',
            201,
            updatedCoupon
        )
    } catch (error) {
        console.log(error)
        return handleHttpError(res, '', 403, error)
    }
}

/**
 * Este controlador es el encargado de eliminar cupones
 * @param {*} req
 * @param {*} res
 */
const DeleteCoupon = async (req, res) => {
    try {
        const { guid } = req.params
        const deleted_by = req.user.guid
        const deleted_at = new Date()
        const [updateCount] = await CouponsModel.update(
            {
                is_active: 0,
                deleted_by,
                deleted_at,
            },
            {
                where: { guid },
            }
        )
        if (updateCount === 0) {
            return handleHttpError(res, 'El cupon no existe.', 404)
        }
        handleHttpSuccess(res, 'Se ha eliminado el cupon.', 201)
    } catch (error) {
        console.log(error)
        handleHttpError(res, '', 403, error)
    }
}

/**
 * Este controlador es el encargado de desactivar cupones
 * @param {*} req
 * @param {*} res
 */
const DisableCoupon = async (req, res) => {
    try {
        const { guid } = req.params
        const updated_by = req.user.guid
        const updated_at = new Date()
        const [updateCount] = await CouponsModel.update(
            {
                is_active: 0,
                updated_by,
                updated_at,
            },
            {
                where: { guid },
            }
        )
        if (updateCount === 0) {
            return handleHttpError(res, 'El cupon no existe.', 404)
        }
        handleHttpSuccess(res, 'Se ha desactivado el cupon.', 201)
    } catch (error) {
        console.log(error)
        handleHttpError(res, '', 403, error)
    }
}

/**
 * Este controlador es el encargado de activar cupones
 * @param {*} req
 * @param {*} res
 */
const EnableCoupon = async (req, res) => {
    try {
        const { guid } = req.params
        const updated_by = req.user.guid
        const updated_at = new Date()
        const [updateCount] = await CouponsModel.update(
            {
                is_active: 1,
                updated_by,
                updated_at,
            },
            {
                where: { guid },
            }
        )
        if (updateCount === 0) {
            return handleHttpError(res, 'El cupon no existe.', 404)
        }
        handleHttpSuccess(res, 'Se ha activado el cupon.', 201)
    } catch (error) {
        console.log(error)
        handleHttpError(res, '', 403, error)
    }
}

/**
 * Este controlador es el encargado de obtener todas los cupones
 * @param {*} req
 * @param {*} res
 */
const CouponsAll = async (req, res) => {
    try {
        const DataCoupons = await CouponsModel.findAll()
        if (!DataCoupons) {
            handleHttpError(res, 'No se encontraron datos.', 404)
            return
        }
        const message = DataCoupons.length
            ? 'Se han encontrado los datos.'
            : 'No se encontraron datos.'
        handleHttpSuccess(res, message, 201, DataCoupons)
    } catch (error) {
        console.log(error)
        handleHttpError(res, '', 403, error)
    }
}

/**
 * Este controlador es el encargado de contar todas los cupones
 * @param {*} req
 * @param {*} res
 */
const CouponsCount = async (req, res) => {
    try {
        const DataCoupons = await CouponsModel.count()
        if (!DataCoupons) {
            handleHttpError(res, 'No se encontraron datos.', 404)
            return
        }
        handleHttpCount(res, 'Se han encontrado los datos.', 201, DataCoupons)
    } catch (error) {
        console.log(error)
        handleHttpError(res, '', 403, error)
    }
}

module.exports = {
    CreateCoupon,
    Coupons,
    Coupon,
    UpdateCoupon,
    DeleteCoupon,
    DisableCoupon,
    EnableCoupon,
    CouponsAll,
    CouponsCount,
}
