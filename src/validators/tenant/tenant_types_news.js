const { check, validationResult } = require('express-validator')

// Validación para crear un nuevo tipo de noticia
const validatorCreateTypeNew = () => {
    return [
        async (req, res, next) => {
            await check('name')
                .notEmpty()
                .withMessage('El nombre del tipo de noticia es requerido.')
                .run(req)
            await check('description')
                .optional()
                .isString()
                .withMessage('La descripción debe ser una cadena de texto.')
                .run(req)

            const errors = validationResult(req)
            if (!errors.isEmpty()) {
                return res.status(400).json({ errors: errors.array() })
            }

            next()
        },
    ]
}

// Validación para obtener un tipo de noticia por GUID
const validatorTypeNew = () => {
    return [
        async (req, res, next) => {
            await check('guid')
                .isUUID()
                .withMessage('El GUID del tipo de noticia es requerido.')
                .run(req)

            const errors = validationResult(req)
            if (!errors.isEmpty()) {
                return res.status(400).json({ errors: errors.array() })
            }

            next()
        },
    ]
}

// Validación para actualizar un tipo de noticia
const validatorUpdateTypeNew = () => {
    return [
        async (req, res, next) => {
            await check('guid')
                .isUUID()
                .withMessage('El GUID del tipo de noticia es requerido.')
                .run(req)
            await check('name')
                .optional()
                .notEmpty()
                .withMessage('El nombre del tipo de noticia es requerido.')
                .run(req)
            await check('description')
                .optional()
                .isString()
                .withMessage('La descripción debe ser una cadena de texto.')
                .run(req)

            const errors = validationResult(req)
            if (!errors.isEmpty()) {
                return res.status(400).json({ errors: errors.array() })
            }

            next()
        },
    ]
}

// Validación para eliminar un tipo de noticia
const validatorDeleteTypeNew = () => {
    return [
        async (req, res, next) => {
            await check('guid')
                .isUUID()
                .withMessage('El GUID del tipo de noticia es requerido.')
                .run(req)

            const errors = validationResult(req)
            if (!errors.isEmpty()) {
                return res.status(400).json({ errors: errors.array() })
            }

            next()
        },
    ]
}

// Validación para deshabilitar un tipo de noticia
const validatorDisableTypeNew = () => {
    return [
        async (req, res, next) => {
            await check('guid')
                .isUUID()
                .withMessage('El GUID del tipo de noticia es requerido.')
                .run(req)

            const errors = validationResult(req)
            if (!errors.isEmpty()) {
                return res.status(400).json({ errors: errors.array() })
            }

            next()
        },
    ]
}

// Validación para habilitar un tipo de noticia
const validatorEnableTypeNew = () => {
    return [
        async (req, res, next) => {
            await check('guid')
                .isUUID()
                .withMessage('El GUID del tipo de noticia es requerido.')
                .run(req)

            const errors = validationResult(req)
            if (!errors.isEmpty()) {
                return res.status(400).json({ errors: errors.array() })
            }

            next()
        },
    ]
}

module.exports = {
    validatorCreateTypeNew,
    validatorTypeNew,
    validatorUpdateTypeNew,
    validatorDeleteTypeNew,
    validatorDisableTypeNew,
    validatorEnableTypeNew,
}
