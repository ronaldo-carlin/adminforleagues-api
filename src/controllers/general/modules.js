// controlador de modulos
const { matchedData } = require('express-validator')
const ModulesModel = require('../../models/general/modules')
const { handleHttpError } = require('../../utils/handleError')
const { handleHttpSuccess } = require('../../utils/handleSuccess')
const { handleHttpCount } = require('../../utils/handleCount')

/**
 * Este controlador es el encargado de crear los modulos
 * @param {*} req
 * @param {*} res
 */
const createModule = async (req, res) => {
    try {
        //Obtenemos los datos que vienen en el body
        const data = matchedData(req)
        //Obtenemos el guid del usuario que esta creando el modulo
        const created_by = req.user.guid
        // Obtenemos la fecha actual
        const created_at = new Date()
        //Le agregamos el usuario y la fecha de creación
        data.created_by = created_by
        data.created_at = created_at
        //Creamos el modulo
        const module = await ModulesModel.create(data)
        //Enviamos los datos
        return handleHttpSuccess(res, 'Se ha creado el modulo.', 201, module)
    } catch (error) {
        // mostrar error en consola
        console.log(error)
        //Obtener y devolver el error
        return handleHttpError(res, '', 403, error)
    }
}

/**
 * Este controlador es el encargado de obtener todos los modulos activos
 * @param {*} req
 * @param {*} res
 */
const Modules = async (req, res) => {
    try {
        //Buscamos todos los modulos
        const module = await ModulesModel.findAll({
            where: { is_active: 1 },
        })
        //Si no existen modulos enviamos error
        if (!module) {
            return handleHttpError(res, 'El modulo no existe.', 404)
        }
        const message = module.length
            ? 'Se han encontrado los datos.'
            : 'No se encontraron datos.'
        //Enviamos los datos
        return handleHttpSuccess(res, message, 201, module)
    } catch (error) {
        // mostrar error en consola
        console.log(error)
        //Obtener y devolver el error
        return handleHttpError(res, '', 403, error)
    }
}

/**
 * Este controlador es el encargado de obtener los modulos por guid
 * @param {*} req
 * @param {*} res
 */
const Module = async (req, res) => {
    try {
        //Obtenemos el guid del modulo
        const { guid } = req.params
        //Buscamos el modulo
        const DataModule = await ModulesModel.findOne({
            where: { guid },
        })
        //Si no existe el modulo enviamos error
        if (!DataModule) {
            return handleHttpError(res, 'El modulo no existe.', 404)
        }
        //Enviamos los datos
        return handleHttpSuccess(
            res,
            'Se han encontrado los datos.',
            201,
            DataModule
        )
    } catch (error) {
        // mostrar error en consola
        console.log(error)
        //Obtener y devolver el error
        return handleHttpError(res, '', 403, error)
    }
}

/**
 * Este controlador es el encargado de actualizar los modulos
 * @param {*} req
 * @param {*} res
 */
const updateModule = async (req, res) => {
    try {
        //Obtenemos los datos que vienen en el body
        const { guid } = req.params
        //Obtenemos el guid del usuario que esta actualizando el modulo
        const updated_by = req.user.guid
        //Obtenemos la fecha actual
        const updated_at = new Date()
        //Obtenemos los datos a actualizar
        const data = matchedData(req)
        // le agregamos el usuario y la fecha de actualización
        data.updated_by = updated_by
        data.updated_at = updated_at
        //Actualizamos el modulo
        const [updateCount] = await ModulesModel.update(data, {
            where: { guid },
        })
        // Validamos si no se encontró el módulo
        if (updateCount === 0) {
            return handleHttpError(res, 'El módulo no existe.', 404)
        }
        // Obtenemos el módulo actualizado
        const updatedModule = await ModulesModel.findOne({
            where: { guid },
            include: [
                {
                    model: ModulesModel,
                    as: 'parent',
                    attributes: {
                        exclude: [
                            'is_active',
                            'created_at',
                            'created_by',
                            'updated_at',
                            'updated_by',
                            'deleted_at',
                            'deleted_by',
                        ],
                    },
                },
            ],
        })
        // Validamos si no se encontró el módulo actualizado (caso raro pero posible)
        if (!updatedModule) {
            return handleHttpError(
                res,
                'Error al obtener el módulo actualizado.',
                500
            )
        }
        // Enviamos los datos
        return handleHttpSuccess(
            res,
            'Se ha actualizado el módulo.',
            201,
            updatedModule
        )
    } catch (error) {
        // mostrar error en consola
        console.log(error)
        //Obtener y devolver el error
        return handleHttpError(res, '', 403, error)
    }
}

/**
 * Este controlador es el encargado de eliminar los modulos
 * @param {*} req
 * @param {*} res
 */
const deleteModule = async (req, res) => {
    try {
        //Obtenemos el guid del modulo
        const { guid } = req.params
        //Obtenemos el guid del usuario que esta eliminando el modulo
        const deleted_by = req.user.guid
        //Obtenemos la fecha actual
        const deleted_at = new Date()
        //Eliminamos el modulo
        const [updateCount] = await ModulesModel.update(
            {
                is_active: 0,
                deleted_by,
                deleted_at,
            },
            {
                where: { guid },
            }
        )
        // Validamos si no se encontró el módulo
        if (updateCount === 0) {
            return handleHttpError(res, 'El módulo no existe.', 404)
        }
        //Enviamos los datos
        handleHttpSuccess(res, 'Se ha eliminado el modulo.', 201)
    } catch (error) {
        // mostrar error en consola
        console.log(error)
        //Obtener y devolver el error
        handleHttpError(res, '', 403, error)
    }
}

/**
 * Este controlador es el encargado de desactivar el modulos
 * @param {*} req
 * @param {*} res
 */
const disableModule = async (req, res) => {
    try {
        //Obtenemos el guid del modulo
        const { guid } = req.params
        //Obtenemos el guid del usuario que esta desactivando el modulo
        const updated_by = req.user.guid
        //Obtenemos la fecha actual
        const updated_at = new Date()
        //Desactivamos el modulo
        const [updateCount] = await ModulesModel.update(
            {
                is_active: 0,
                updated_by,
                updated_at,
            },
            {
                where: { guid },
            }
        )
        // Validamos si no se encontró el módulo
        if (updateCount === 0) {
            return handleHttpError(res, 'El módulo no existe.', 404)
        }
        //Enviamos los datos
        handleHttpSuccess(res, 'Se ha desactivado el modulo.', 201)
    } catch (error) {
        // mostrar error en consola
        console.log(error)
        //Obtener y devolver el error
        handleHttpError(res, '', 403, error)
    }
}

/**
 * Este controlador es el encargado de activar el modulos
 * @param {*} req
 * @param {*} res
 */
const enableModule = async (req, res) => {
    try {
        //Obtenemos el guid del modulo
        const { guid } = req.params
        //Obtenemos el guid del usuario que esta activando el modulo
        const updated_by = req.user.guid
        //Obtenemos la fecha actual
        const updated_at = new Date()
        //Activamos el modulo
        const [updateCount] = await ModulesModel.update(
            {
                is_active: 1,
                updated_by,
                updated_at,
            },
            {
                where: { guid },
            }
        )
        // Validamos si no se encontró el módulo
        if (updateCount === 0) {
            return handleHttpError(res, 'El módulo no existe.', 404)
        }
        //Enviamos los datos
        handleHttpSuccess(res, 'Se ha activado el modulo.', 201)
    } catch (error) {
        // mostrar error en consola
        console.log(error)
        //Obtener y devolver el error
        handleHttpError(res, '', 403, error)
    }
}

/**
 * Este controlador es el encargado de obtener todos los modulos
 * @param {*} req
 * @param {*} res
 */
const modulesAll = async (req, res) => {
    try {
        //Buscamos todos los modulos
        const DataModules = await ModulesModel.findAll()
        //Si no existen modulos enviamos error
        if (!DataModules) {
            handleHttpError(res, 'No se encontraron datos.', 404)
            return
        }
        const message = DataModules.length
            ? 'Se han encontrado los datos.'
            : 'No se encontraron datos.'
        //Enviamos los datos
        handleHttpSuccess(res, message, 201, DataModules)
    } catch (error) {
        // mostrar error en consola
        console.log(error)
        //Obtener y devolver el error
        handleHttpError(res, '', 403, error)
    }
}

/**
 * Este controlador es el encargado de contar todos los modulos
 * @param {*} req
 * @param {*} res
 */
const countAllModules = async (req, res) => {
    try {
        //Contamos todos los modulos
        const Datamodules = await ModulesModel.count()
        //Si no existen modulos enviamos error
        if (!Datamodules) {
            handleHttpError(res, 'No se encontraron datos.', 404)
            return
        }
        //Enviamos los datos
        handleHttpCount(res, 'Se han encontrado los datos.', 201, Datamodules)
    } catch (error) {
        // mostrar error en consola
        console.log(error)
        //Obtener y devolver el error
        handleHttpError(res, '', 403, error)
    }
}

module.exports = {
    createModule,
    Modules,
    Module,
    updateModule,
    deleteModule,
    disableModule,
    enableModule,
    modulesAll,
    countAllModules,
}
