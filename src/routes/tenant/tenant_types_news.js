const express = require('express')
const router = express.Router()

// Controladores y validadores para Types News
const {
    validatorCreateTypeNew,
    validatorTypeNew,
    validatorUpdateTypeNew,
    validatorDeleteTypeNew,
    validatorDisableTypeNew,
    validatorEnableTypeNew,
} = require('../../validators/tenant/tenant_types_news')

const {
    CreateTypeNew,
    TypesNews,
    TypeNew,
    UpdateTypeNew,
    DeleteTypeNew,
    DisableTypeNew,
    EnableTypeNew,
    TypesNewsAll,
    TypesNewsCount,
} = require('../../controllers/tenant/tenant_types_news')

const authMiddleware = require('../../middleware/session_tenant')

/* Rutas protegidas que requieren autenticación */
/**
 * @swagger
 * tags:
 *   name: Tenant_Types_News
 *   description: API para la gestión de tipos de noticias
 */

// Ruta para crear un nuevo tipo de noticia
/**
 * @swagger
 * /tenant_type_news/CreateTypeNew:
 *   post:
 *     summary: Crear un nuevo tipo de noticia
 *     description: Esta ruta permite crear un nuevo tipo de noticia con los detalles especificados.
 *     tags: [Tenant_Types_News]
 *     requestBody:
 *       required: true
 *       content:
 *         application/json:
 *           schema:
 *             type: object
 *             properties:
 *               name:
 *                 type: string
 *                 example: "Breaking News"
 *               description:
 *                 type: string
 *                 example: "Noticias de última hora."
 *     responses:
 *       201:
 *         description: Tipo de noticia creado exitosamente
 *         content:
 *           application/json:
 *             schema:
 *               $ref: '#/components/responses/success'
 *       400:
 *         description: Error en los datos enviados
 *         content:
 *           application/json:
 *             schema:
 *               $ref: '#/components/responses/BadRequest'
 *       403:
 *         description: Error de autenticación
 *         content:
 *           application/json:
 *             schema:
 *               $ref: '#/components/responses/UnauthorizedError'
 *       409:
 *         description: El tipo de noticia ya existe
 *         content:
 *           application/json:
 *             schema:
 *               $ref: '#/components/responses/ConflictError'
 *       500:
 *         description: Error en el servidor
 *         content:
 *           application/json:
 *             schema:
 *               $ref: '#/components/responses/InternalServerError'
 *     security:
 *        - bearerAuth: []
 */
router.post(
    '/CreateTypeNew',
    authMiddleware,
    validatorCreateTypeNew(),
    CreateTypeNew
)

// Ruta para obtener todos los tipos de noticias activas
/**
 * @swagger
 * /tenant_type_news/TypesNews:
 *   get:
 *     summary: Obtener todos los tipos de noticias activas
 *     tags: [Tenant_Types_News]
 *     responses:
 *       200:
 *         description: Tipos de noticias obtenidas exitosamente
 *         content:
 *           application/json:
 *             schema:
 *               $ref: '#/components/responses/success'
 *       403:
 *         description: Error de autenticación
 *         content:
 *           application/json:
 *             schema:
 *               $ref: '#/components/responses/UnauthorizedError'
 *       500:
 *         description: Error en el servidor
 *         content:
 *           application/json:
 *             schema:
 *               $ref: '#/components/responses/InternalServerError'
 *     security:
 *        - bearerAuth: []
 */
router.get('/TypesNews', authMiddleware, TypesNews)

// Ruta para obtener un tipo de noticia por GUID
/**
 * @swagger
 * /tenant_type_news/TypeNew/{guid}:
 *   get:
 *     summary: Obtener un tipo de noticia por GUID
 *     tags: [Tenant_Types_News]
 *     parameters:
 *       - in: path
 *         name: guid
 *         schema:
 *           type: string
 *           format: uuid
 *         required: true
 *         description: El GUID del tipo de noticia
 *     responses:
 *       200:
 *         description: Tipo de noticia obtenido exitosamente
 *         content:
 *           application/json:
 *             schema:
 *               $ref: '#/components/responses/success'
 *       403:
 *         description: Error de autenticación
 *         content:
 *           application/json:
 *             schema:
 *               $ref: '#/components/responses/UnauthorizedError'
 *       404:
 *         description: Tipo de noticia no encontrado
 *         content:
 *           application/json:
 *             schema:
 *               $ref: '#/components/responses/NotFoundError'
 *       500:
 *         description: Error en el servidor
 *         content:
 *           application/json:
 *             schema:
 *               $ref: '#/components/responses/InternalServerError'
 *     security:
 *        - bearerAuth: []
 */
router.get('/TypeNew/:guid', authMiddleware, validatorTypeNew(), TypeNew)

// Ruta para actualizar un tipo de noticia
/**
 * @swagger
 * /tenant_type_news/UpdateTypeNew/{guid}:
 *   patch:
 *     summary: Actualizar un tipo de noticia
 *     tags: [Tenant_Types_News]
 *     requestBody:
 *       required: true
 *       content:
 *         application/json:
 *           schema:
 *             type: object
 *             properties:
 *               name:
 *                 type: string
 *                 example: "Breaking News"
 *               description:
 *                 type: string
 *                 example: "Noticias de última hora."
 *     responses:
 *       200:
 *         description: Tipo de noticia actualizado exitosamente
 *         content:
 *           application/json:
 *             schema:
 *               $ref: '#/components/responses/success'
 *       400:
 *         description: Error en los datos enviados
 *         content:
 *           application/json:
 *             schema:
 *               $ref: '#/components/responses/BadRequest'
 *       403:
 *         description: Error de autenticación
 *         content:
 *           application/json:
 *             schema:
 *               $ref: '#/components/responses/UnauthorizedError'
 *       404:
 *         description: Tipo de noticia no encontrado
 *         content:
 *           application/json:
 *             schema:
 *               $ref: '#/components/responses/NotFoundError'
 *       500:
 *         description: Error en el servidor
 *         content:
 *           application/json:
 *             schema:
 *               $ref: '#/components/responses/InternalServerError'
 *     security:
 *        - bearerAuth: []
 */
router.patch(
    '/UpdateTypeNew/:guid',
    authMiddleware,
    validatorUpdateTypeNew(),
    UpdateTypeNew
)

// Ruta para eliminar un tipo de noticia
/**
 * @swagger
 * /tenant_type_news/DeleteTypeNew/{guid}:
 *   delete:
 *     summary: Eliminar un tipo de noticia
 *     tags: [Tenant_Types_News]
 *     parameters:
 *       - in: path
 *         name: guid
 *         schema:
 *           type: string
 *           format: uuid
 *         required: true
 *         description: El GUID del tipo de noticia
 *     responses:
 *       200:
 *         description: Tipo de noticia eliminado exitosamente
 *         content:
 *           application/json:
 *             schema:
 *               $ref: '#/components/responses/success'
 *       400:
 *         description: Error en los datos enviados
 *         content:
 *           application/json:
 *             schema:
 *               $ref: '#/components/responses/BadRequest'
 *       403:
 *         description: Error de autenticación
 *         content:
 *           application/json:
 *             schema:
 *               $ref: '#/components/responses/UnauthorizedError'
 *       404:
 *         description: Tipo de noticia no encontrado
 *         content:
 *           application/json:
 *             schema:
 *               $ref: '#/components/responses/NotFoundError'
 *       500:
 *         description: Error en el servidor
 *         content:
 *           application/json:
 *             schema:
 *               $ref: '#/components/responses/InternalServerError'
 *     security:
 *        - bearerAuth: []
 */
router.delete(
    '/DeleteTypeNew/:guid',
    authMiddleware,
    validatorDeleteTypeNew(),
    DeleteTypeNew
)

// Ruta para deshabilitar un tipo de noticia
/**
 * @swagger
 * /tenant_type_news/DisableTypeNew/{guid}:
 *   patch:
 *     summary: Deshabilitar un tipo de noticia
 *     tags: [Tenant_Types_News]
 *     requestBody:
 *       required: true
 *       content:
 *         application/json:
 *           schema:
 *             type: object
 *             properties:
 *               guid:
 *                 type: uuid
 *                 example: 495db854-849b-41cd-b6b9-9c84824c92ba
 *     responses:
 *       200:
 *         description: Tipo de noticia deshabilitado exitosamente
 *         content:
 *           application/json:
 *             schema:
 *               $ref: '#/components/responses/success'
 *       400:
 *         description: Error en los datos enviados
 *         content:
 *           application/json:
 *             schema:
 *               $ref: '#/components/responses/BadRequest'
 *       403:
 *         description: Error de autenticación
 *         content:
 *           application/json:
 *             schema:
 *               $ref: '#/components/responses/UnauthorizedError'
 *       404:
 *         description: Tipo de noticia no encontrado
 *         content:
 *           application/json:
 *             schema:
 *               $ref: '#/components/responses/NotFoundError'
 *       500:
 *         description: Error en el servidor
 *         content:
 *           application/json:
 *             schema:
 *               $ref: '#/components/responses/InternalServerError'
 *     security:
 *        - bearerAuth: []
 */
router.patch(
    '/DisableTypeNew/:guid',
    authMiddleware,
    validatorDisableTypeNew(),
    DisableTypeNew
)

// Ruta para habilitar un tipo de noticia
/**
 * @swagger
 * /tenant_type_news/EnableTypeNew/{guid}:
 *   patch:
 *     summary: Habilitar un tipo de noticia
 *     tags: [Tenant_Types_News]
 *     requestBody:
 *       required: true
 *       content:
 *         application/json:
 *           schema:
 *             type: object
 *             properties:
 *               guid:
 *                 type: uuid
 *                 example: 495db854-849b-41cd-b6b9-9c84824c92ba
 *     responses:
 *       200:
 *         description: Tipo de noticia habilitado exitosamente
 *         content:
 *           application/json:
 *             schema:
 *               $ref: '#/components/responses/success'
 *       400:
 *         description: Error en los datos enviados
 *         content:
 *           application/json:
 *             schema:
 *               $ref: '#/components/responses/BadRequest'
 *       403:
 *         description: Error de autenticación
 *         content:
 *           application/json:
 *             schema:
 *               $ref: '#/components/responses/UnauthorizedError'
 *       404:
 *         description: Tipo de noticia no encontrado
 *         content:
 *           application/json:
 *             schema:
 *               $ref: '#/components/responses/NotFoundError'
 *       500:
 *         description: Error en el servidor
 *         content:
 *           application/json:
 *             schema:
 *               $ref: '#/components/responses/InternalServerError'
 *     security:
 *        - bearerAuth: []
 */
router.patch(
    '/EnableTypeNew/:guid',
    authMiddleware,
    validatorEnableTypeNew(),
    EnableTypeNew
)

// Ruta para obtener todos los tipos de noticias (incluyendo deshabilitadas)
/**
 * @swagger
 * /tenant_type_news/AllTypesNews:
 *   get:
 *     summary: Obtener todos los tipos de noticias (incluyendo deshabilitadas)
 *     tags: [Tenant_Types_News]
 *     responses:
 *       200:
 *         description: Tipos de noticias obtenidas exitosamente
 *         content:
 *           application/json:
 *             schema:
 *               $ref: '#/components/responses/success'
 *       403:
 *         description: Error de autenticación
 *         content:
 *           application/json:
 *             schema:
 *               $ref: '#/components/responses/UnauthorizedError'
 *       500:
 *         description: Error en el servidor
 *         content:
 *           application/json:
 *             schema:
 *               $ref: '#/components/responses/InternalServerError'
 *     security:
 *        - bearerAuth: []
 */
router.get('/TypesNewsAll', authMiddleware, TypesNewsAll)

// Ruta para obtener el conteo de tipos de noticias
/**
 * @swagger
 * /tenant_type_news/TypesNewsCount:
 *   get:
 *     summary: Obtener el conteo de tipos de noticias
 *     tags: [Tenant_Types_News]
 *     responses:
 *       200:
 *         description: Conteo de tipos de noticias obtenido exitosamente
 *         content:
 *           application/json:
 *             schema:
 *               $ref: '#/components/responses/success'
 *       403:
 *         description: Error de autenticación
 *         content:
 *           application/json:
 *             schema:
 *               $ref: '#/components/responses/UnauthorizedError'
 *       500:
 *         description: Error en el servidor
 *         content:
 *           application/json:
 *             schema:
 *               $ref: '#/components/responses/InternalServerError'
 *     security:
 *        - bearerAuth: []
 */
router.get('/TypesNewsCount', authMiddleware, TypesNewsCount)

module.exports = router
