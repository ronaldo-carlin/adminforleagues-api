const { Op } = require('sequelize')
const { v4: uuidv4 } = require('uuid')
const { matchedData } = require('express-validator')
const { handleHttpSuccess } = require('../../utils/handleSuccess')
const { handleHttpError } = require('../../utils/handleError')
const { handleHttpCount } = require('../../utils/handleCount')
const {
    getSequelize,
    getMatchesModel,
} = require('../../helpers/sequelizeHelper')

/**
 * Este controlador es el encargado de registrar un partido
 * @param {*} req
 * @param {*} res
 */
const CreateMatch = async (req, res) => {
    try {
        const sequelize = getSequelize(req)
        const MatchModel = getMatchesModel(sequelize)
        const data = matchedData(req)
        const created_by = req.user.guid
        const created_at = new Date()
        const guid = await uuidv4()
        data.created_by = created_by
        data.created_at = created_at
        data.guid = guid

        const match = await MatchModel.create(data)
        return handleHttpSuccess(res, 'Partido creado.', 201, match)
    } catch (error) {
        console.log('Error al crear el partido:', error)
        handleHttpError(res, 'Error al crear el partido.', 403, error)
    }
}

/**
 * Este controlador es el encargado de obtener los partidos activos
 * @param {*} req
 * @param {*} res
 */
const Matches = async (req, res) => {
    try {
        const sequelize = getSequelize(req)
        const MatchModel = getMatchesModel(sequelize)

        const matches = await MatchModel.findAll({
            where: { is_active: 1 },
        })

        if (!matches.length) {
            handleHttpError(res, 'No se encontraron partidos.', 404)
            return
        }

        handleHttpSuccess(res, 'Partidos encontrados.', 201, matches)
    } catch (error) {
        console.log('Error obteniendo partidos:', error)
        handleHttpError(res, 'Error obteniendo partidos.', 403, error)
    }
}

/**
 * Este controlador es el encargado de obtener un partido por GUID
 * @param {*} req
 * @param {*} res
 */
const Match = async (req, res) => {
    try {
        const sequelize = getSequelize(req)
        const MatchModel = getMatchesModel(sequelize)
        const { guid } = req.params

        const match = await MatchModel.findOne({
            where: { guid },
        })

        if (!match) {
            handleHttpError(res, 'El partido no existe.', 404)
            return
        }

        if (match.is_active == 0) {
            handleHttpError(res, 'El partido ha sido deshabilitado.', 401)
            return
        }

        handleHttpSuccess(res, 'Partido encontrado.', 201, match)
    } catch (error) {
        console.log('Error obteniendo partido:', error)
        handleHttpError(res, 'Error obteniendo partido.', 403, error)
    }
}

/**
 * Este controlador es el encargado de actualizar un partido
 * @param {*} req
 * @param {*} res
 */
const UpdateMatch = async (req, res) => {
    try {
        const sequelize = getSequelize(req)
        const MatchModel = getMatchesModel(sequelize)
        const { guid } = req.params
        const data = matchedData(req)
        const updated_by = req.user.guid
        const updated_at = new Date()
        data.updated_by = updated_by
        data.updated_at = updated_at

        const [updateCount] = await MatchModel.update(data, {
            where: { guid },
        })

        if (updateCount === 0) {
            return handleHttpError(res, 'El partido no existe.', 404)
        }

        const match = await MatchModel.findOne({
            where: { guid },
        })

        if (!match) {
            handleHttpError(res, 'El partido no existe.', 404)
            return
        }

        if (match.is_active == 0) {
            handleHttpError(res, 'El partido ha sido deshabilitado.', 401)
            return
        }

        handleHttpSuccess(res, 'Partido actualizado.', 201, match)
    } catch (error) {
        console.log('Error actualizando partido:', error)
        handleHttpError(res, 'Error actualizando partido.', 403, error)
    }
}

/**
 * Este controlador es el encargado de eliminar un partido
 * @param {*} req
 * @param {*} res
 */
const DeleteMatch = async (req, res) => {
    try {
        const sequelize = getSequelize(req)
        const MatchModel = getMatchesModel(sequelize)
        const { guid } = req.params
        const deleted_by = req.user.guid
        const deleted_at = new Date()
        const [updateCount] = await MatchModel.update(
            {
                is_active: 0,
                deleted_by,
                deleted_at,
            },
            {
                where: { guid },
            }
        )

        if (updateCount === 0) {
            return handleHttpError(res, 'El partido no existe.', 404)
        }

        handleHttpSuccess(res, 'Partido eliminado.', 201)
    } catch (error) {
        console.log('Error eliminando partido:', error)
        handleHttpError(res, 'Error eliminando partido.', 403, error)
    }
}

/**
 * Este controlador es el encargado de desactivar un partido
 * @param {*} req
 * @param {*} res
 */
const DisableMatch = async (req, res) => {
    try {
        const sequelize = getSequelize(req)
        const MatchModel = getMatchesModel(sequelize)
        const { guid } = req.params
        const updated_by = req.user.guid
        const updated_at = new Date()
        const [updateCount] = await MatchModel.update(
            {
                is_active: 0,
                updated_by,
                updated_at,
            },
            {
                where: { guid },
            }
        )

        if (updateCount === 0) {
            return handleHttpError(res, 'El partido no existe.', 404)
        }

        handleHttpSuccess(res, 'Partido deshabilitado.', 201)
    } catch (error) {
        console.log('Error deshabilitando partido:', error)
        handleHttpError(res, 'Error deshabilitando partido.', 403, error)
    }
}

/**
 * Este controlador es el encargado de activar un partido
 * @param {*} req
 * @param {*} res
 */
const EnableMatch = async (req, res) => {
    try {
        const sequelize = getSequelize(req)
        const MatchModel = getMatchesModel(sequelize)
        const { guid } = req.params
        const updated_by = req.user.guid
        const updated_at = new Date()
        const [updateCount] = await MatchModel.update(
            {
                is_active: 1,
                updated_by,
                updated_at,
            },
            {
                where: { guid },
            }
        )

        if (updateCount === 0) {
            return handleHttpError(res, 'El partido no existe.', 404)
        }

        handleHttpSuccess(res, 'Partido habilitado.', 201)
    } catch (error) {
        console.log('Error habilitando partido:', error)
        handleHttpError(res, 'Error habilitando partido.', 403, error)
    }
}

/**
 * Este controlador es el encargado de obtener todos los partidos
 * @param {*} req
 * @param {*} res
 */
const MatchesAll = async (req, res) => {
    try {
        const sequelize = getSequelize(req)
        const MatchModel = getMatchesModel(sequelize)

        const matches = await MatchModel.findAll()

        if (!matches.length) {
            handleHttpError(res, 'No se encontraron partidos.', 404)
            return
        }

        handleHttpSuccess(res, 'Partidos encontrados.', 201, matches)
    } catch (error) {
        console.log('Error obteniendo partidos:', error)
        handleHttpError(res, 'Error obteniendo partidos.', 403, error)
    }
}

/**
 * Este controlador es el encargado de contar todos los partidos
 * @param {*} req
 * @param {*} res
 */
const MatchesCount = async (req, res) => {
    try {
        const sequelize = getSequelize(req)
        const MatchModel = getMatchesModel(sequelize)

        const count = await MatchModel.count()

        handleHttpCount(res, 'Partidos encontrados.', 201, count)
    } catch (error) {
        console.log('Error contando partidos:', error)
        handleHttpError(res, 'Error contando partidos.', 403, error)
    }
}

module.exports = {
    CreateMatch,
    Matches,
    Match,
    UpdateMatch,
    DeleteMatch,
    DisableMatch,
    EnableMatch,
    MatchesAll,
    MatchesCount,
}
