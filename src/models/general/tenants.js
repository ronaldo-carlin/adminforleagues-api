// Importar Sequelize directamente
const { Sequelize } = require('sequelize')
// Importar tu configuración de sequelize
const { sequelize } = require('../../database/mysql')

const TenantsModel = sequelize.define(
    'tenants',
    {
        id: {
            type: Sequelize.INTEGER,
            primaryKey: true,
            autoIncrement: true,
        },
        guid: {
            type: Sequelize.UUID,
            defaultValue: Sequelize.UUIDV4,
            unique: true,
        },
        company_name: {
            type: Sequelize.TEXT,
        },
        email: {
            type: Sequelize.TEXT,
        },
        phone: {
            type: Sequelize.TEXT,
        },
        country: {
            type: Sequelize.TEXT,
        },
        state: {
            type: Sequelize.TEXT,
        },
        city: {
            type: Sequelize.TEXT,
        },
        address: {
            type: Sequelize.TEXT,
        },
        is_active: {
            type: Sequelize.INTEGER,
        },
        created_by: {
            type: Sequelize.UUID,
        },
        updated_by: {
            type: Sequelize.UUID,
        },
        deleted_by: {
            type: Sequelize.UUID,
        },
        created_at: {
            type: Sequelize.DATE,
        },
        updated_at: {
            type: Sequelize.DATE,
        },
        deleted_at: {
            type: Sequelize.DATE,
        },
    },
    {
        tableName: 'tenants',
        timestamps: false,
        defaultScope: {
            attributes: {
                exclude: [
                    'created_by',
                    'updated_by',
                    'deleted_by',
                    'created_at',
                    'updated_at',
                    'deleted_at',
                ],
            },
        },
    }
)

module.exports = TenantsModel
