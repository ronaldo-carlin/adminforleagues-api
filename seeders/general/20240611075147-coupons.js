const { v4: uuidv4 } = require('uuid')
const GUIDsGeneral = require('../guids_general')
module.exports = {
    async up(queryInterface, Sequelize) {
        // Inserta los cupones por defecto
        await queryInterface.bulkInsert(
            'coupons',
            [
                {
                    guid: uuidv4(),
                    code: 'coupon1',
                    description: 'Cupón 1 de la aplicación',
                    plan_guid: GUIDsGeneral.planBasicoGuid,
                    discount_type: 'percentage',
                    discount_value: 10,
                    start_date: new Date(),
                    end_date: new Date(),
                    uses: 100,
                    used: 0,
                    link_by: 'url',
                    min_amount: 0,
                    max_amount: 0,
                    is_active: 1, // 1: Activo, 0: Inactivo
                    created_at: new Date(),
                },
                {
                    guid: uuidv4(),
                    code: 'coupon2',
                    description: 'Cupón 2 de la aplicación',
                    plan_guid: GUIDsGeneral.planBasicoGuid,
                    discount_type: 'percentage',
                    discount_value: 10,
                    start_date: new Date(),
                    end_date: new Date(),
                    uses: 100,
                    used: 0,
                    link_by: 'url',
                    min_amount: 0,
                    max_amount: 0,
                    is_active: 1, // 1: Activo, 0: Inactivo
                    created_at: new Date(),
                },
                {
                    guid: uuidv4(),
                    code: 'coupon3',
                    description: 'Cupón 3 de la aplicación',
                    plan_guid: GUIDsGeneral.planBasicoGuid,
                    discount_type: 'percentage',
                    discount_value: 10,
                    start_date: new Date(),
                    end_date: new Date(),
                    uses: 100,
                    used: 0,
                    link_by: 'url',
                    min_amount: 0,
                    max_amount: 0,
                    is_active: 1, // 1: Activo, 0: Inactivo
                    created_at: new Date(),
                },
            ],
            {}
        )
    },

    async down(queryInterface, Sequelize) {
        await queryInterface.bulkDelete('coupons', null, {})
    },
}
