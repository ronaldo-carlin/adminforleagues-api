const nodemailer = require('nodemailer')

// Configuramos el transported de nodemailer
let transporter = nodemailer.createTransport({
    host: process.env.MAIL_HOST,
    port: process.env.MAIL_PORT,
    secure: process.env.MAIL_SECURE,
    auth: {
        user: process.env.MAIL_USER,
        pass: process.env.MAIL_PASSWORD,
    },
})

// Función para enviar el correo de inicio de sesión
const sendEmail = async (options) => {
    try {
        const mailOptions = {
            from: `${process.env.MAIL_NAME} ${process.env.MAIL_USER}`, // Cambia esto por tu dirección de correo
            ...options,
        }

        await transporter.sendMail(mailOptions)
        console.log('Correo se enviado correctamente')
    } catch (error) {
        console.error('Error al enviar el correo:', error)
    }
}

module.exports = {
    transporter,
    sendEmail,
}
