const { check } = require('express-validator')
const validateResults = require('../../utils/handleValidators')

// Validaciones para crear una Licencia
const ValidatorCreateLicense = [
    check('plan_guid').isUUID().withMessage('El guid del plan es requerido.'),
    check('name').isString().withMessage('El nombre es requerido.'),
    check('description').isString().withMessage('La descripción es requerida.'),
    check('price').isNumeric().withMessage('El precio es requerido.'),
    check('users')
        .isNumeric()
        .withMessage('El número de usuarios es requerido.'),
    check('teams')
        .isNumeric()
        .withMessage('El número de equipos es requerido.'),
    check('matches')
        .isNumeric()
        .withMessage('El número de partidos es requerido.'),
    check('competitions')
        .isNumeric()
        .withMessage('El número de competencias es requerido.'),
    check('sedes').isNumeric().withMessage('El número de sedes es requerido.'),
    check('sponsors')
        .isNumeric()
        .withMessage('El número de patrocinadores es requerido.'),
    check('publications')
        .isNumeric()
        .withMessage('El número de publicaciones es requerido.'),
    check('players')
        .isNumeric()
        .withMessage('El número de jugadores es requerido.'),
    check('coaches')
        .isNumeric()
        .withMessage('El número de entrenadores es requerido.'),
    check('referees')
        .isNumeric()
        .withMessage('El número de árbitros es requerido.'),
    check('reports')
        .isNumeric()
        .withMessage('El número de reportes es requerido.'),
    check('duration_days')
        .isNumeric()
        .withMessage('El número de días de duración es requerido.'),
    check('matches_summary')
        .isString()
        .withMessage('El resumen de partidos es requerido.'),
    check('matches_reports')
        .isString()
        .withMessage('Los reportes de partidos son requeridos.'),
    check('ads').isNumeric().withMessage('El número de anuncios es requerido.'),
    validateResults,
]

// Validaciones para actualizar una Licencia
const ValidatorUpdateLicense = [
    check('guid').isUUID().withMessage('El guid es requerido.'),
    check('plan_guid').isUUID().withMessage('El guid del plan es requerido.'),
    check('name').isString().withMessage('El nombre es requerido.'),
    check('description').isString().withMessage('La descripción es requerida.'),
    check('price').isNumeric().withMessage('El precio es requerido.'),
    check('users')
        .isNumeric()
        .withMessage('El número de usuarios es requerido.'),
    check('teams')
        .isNumeric()
        .withMessage('El número de equipos es requerido.'),
    check('matches')
        .isNumeric()
        .withMessage('El número de partidos es requerido.'),
    check('competitions')
        .isNumeric()
        .withMessage('El número de competencias es requerido.'),
    check('sedes').isNumeric().withMessage('El número de sedes es requerido.'),
    check('sponsors')
        .isNumeric()
        .withMessage('El número de patrocinadores es requerido.'),
    check('publications')
        .isNumeric()
        .withMessage('El número de publicaciones es requerido.'),
    check('players')
        .isNumeric()
        .withMessage('El número de jugadores es requerido.'),
    check('coaches')
        .isNumeric()
        .withMessage('El número de entrenadores es requerido.'),
    check('referees')
        .isNumeric()
        .withMessage('El número de árbitros es requerido.'),
    check('reports')
        .isNumeric()
        .withMessage('El número de reportes es requerido.'),
    check('duration_days')
        .isNumeric()
        .withMessage('El número de días de duración es requerido.'),
    check('matches_summary')
        .isString()
        .withMessage('El resumen de partidos es requerido.'),
    check('matches_reports')
        .isString()
        .withMessage('Los reportes de partidos son requeridos.'),
    check('ads').isNumeric().withMessage('El número de anuncios es requerido.'),
    validateResults,
]

// Validaciones para eliminar una Licencia
const ValidatorDeleteLicense = [
    check('guid').isUUID().withMessage('El guid es requerido.'),
    validateResults,
]

// Validaciones para deshabilitar una Licencia
const ValidatorDisableLicense = [
    check('guid').isUUID().withMessage('El guid es requerido.'),
    validateResults,
]

// Validaciones para habilitar una Licencia
const ValidatorEnableLicense = [
    check('guid').isUUID().withMessage('El guid es requerido.'),
    validateResults,
]

module.exports = {
    ValidatorCreateLicense,
    ValidatorUpdateLicense,
    ValidatorDeleteLicense,
    ValidatorDisableLicense,
    ValidatorEnableLicense,
}
