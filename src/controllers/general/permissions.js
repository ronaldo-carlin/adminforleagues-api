const { matchedData } = require('express-validator')
const PermissionsModel = require('../../models/general/permissions')
const { handleHttpError } = require('../../utils/handleError')
const { handleHttpSuccess } = require('../../utils/handleSuccess')
const { handleHttpCount } = require('../../utils/handleCount')

/**
 * Este controlador es el encargado de crear permisos
 * @param {*} req
 * @param {*} res
 */
const createPermission = async (req, res) => {
    try {
        const data = matchedData(req)
        const created_by = req.user.guid
        const created_at = new Date()
        data.created_by = created_by
        data.created_at = created_at
        const permission = await PermissionsModel.create(data)
        return handleHttpSuccess(
            res,
            'Se ha creado el permiso.',
            201,
            permission
        )
    } catch (error) {
        console.log(error)
        return handleHttpError(res, '', 403, error)
    }
}

/**
 * Este controlador es el encargado de obtener todos los permisos activos
 * @param {*} req
 * @param {*} res
 */
const Permissions = async (req, res) => {
    try {
        const permissions = await PermissionsModel.findAll({
            where: { is_active: 1 },
        })
        if (!permissions) {
            return handleHttpError(res, 'El permiso no existe.', 404)
        }
        const message = permissions.length
            ? 'Se han encontrado los datos.'
            : 'No se encontraron datos.'
        return handleHttpSuccess(res, message, 201, permissions)
    } catch (error) {
        console.log(error)
        return handleHttpError(res, '', 403, error)
    }
}

/**
 * Este controlador es el encargado de obtener permisos por guid
 * @param {*} req
 * @param {*} res
 */
const Permission = async (req, res) => {
    try {
        const { guid } = req.params
        const DataPermission = await PermissionsModel.findOne({
            where: { guid },
        })
        if (!DataPermission) {
            return handleHttpError(res, 'El permiso no existe.', 404)
        }
        return handleHttpSuccess(
            res,
            'Se han encontrado los datos.',
            201,
            DataPermission
        )
    } catch (error) {
        console.log(error)
        return handleHttpError(res, '', 403, error)
    }
}

/**
 * Este controlador es el encargado de actualizar permisos
 * @param {*} req
 * @param {*} res
 */
const updatePermission = async (req, res) => {
    try {
        const { guid } = req.params
        const updated_by = req.user.guid
        const updated_at = new Date()
        const data = matchedData(req)
        data.updated_by = updated_by
        data.updated_at = updated_at
        const [updateCount] = await PermissionsModel.update(data, {
            where: { guid },
        })
        if (updateCount === 0) {
            return handleHttpError(res, 'El permiso no existe.', 404)
        }
        const updatedPermission = await PermissionsModel.findOne({
            where: { guid },
        })
        if (!updatedPermission) {
            return handleHttpError(
                res,
                'Error al obtener el permiso actualizado.',
                500
            )
        }
        return handleHttpSuccess(
            res,
            'Se ha actualizado el permiso.',
            201,
            updatedPermission
        )
    } catch (error) {
        console.log(error)
        return handleHttpError(res, '', 403, error)
    }
}

/**
 * Este controlador es el encargado de eliminar permisos
 * @param {*} req
 * @param {*} res
 */
const deletePermission = async (req, res) => {
    try {
        const { guid } = req.params
        const deleted_by = req.user.guid
        const deleted_at = new Date()
        const [updateCount] = await PermissionsModel.update(
            {
                is_active: 0,
                deleted_by,
                deleted_at,
            },
            {
                where: { guid },
            }
        )
        if (updateCount === 0) {
            return handleHttpError(res, 'El permiso no existe.', 404)
        }
        handleHttpSuccess(res, 'Se ha eliminado el permiso.', 201)
    } catch (error) {
        console.log(error)
        handleHttpError(res, '', 403, error)
    }
}

/**
 * Este controlador es el encargado de desactivar permisos
 * @param {*} req
 * @param {*} res
 */
const disablePermission = async (req, res) => {
    try {
        const { guid } = req.params
        const updated_by = req.user.guid
        const updated_at = new Date()
        const [updateCount] = await PermissionsModel.update(
            {
                is_active: 0,
                updated_by,
                updated_at,
            },
            {
                where: { guid },
            }
        )
        if (updateCount === 0) {
            return handleHttpError(res, 'El permiso no existe.', 404)
        }
        handleHttpSuccess(res, 'Se ha desactivado el permiso.', 201)
    } catch (error) {
        console.log(error)
        handleHttpError(res, '', 403, error)
    }
}

/**
 * Este controlador es el encargado de activar permisos
 * @param {*} req
 * @param {*} res
 */
const enablePermission = async (req, res) => {
    try {
        const { guid } = req.params
        const updated_by = req.user.guid
        const updated_at = new Date()
        const [updateCount] = await PermissionsModel.update(
            {
                is_active: 1,
                updated_by,
                updated_at,
            },
            {
                where: { guid },
            }
        )
        if (updateCount === 0) {
            return handleHttpError(res, 'El permiso no existe.', 404)
        }
        handleHttpSuccess(res, 'Se ha activado el permiso.', 201)
    } catch (error) {
        console.log(error)
        handleHttpError(res, '', 403, error)
    }
}

/**
 * Este controlador es el encargado de obtener todos los permisos
 * @param {*} req
 * @param {*} res
 */
const permissionsAll = async (req, res) => {
    try {
        const DataPermissions = await PermissionsModel.findAll()
        if (!DataPermissions) {
            handleHttpError(res, 'No se encontraron datos.', 404)
            return
        }
        const message = DataPermissions.length
            ? 'Se han encontrado los datos.'
            : 'No se encontraron datos.'
        handleHttpSuccess(res, message, 201, DataPermissions)
    } catch (error) {
        console.log(error)
        handleHttpError(res, '', 403, error)
    }
}

/**
 * Este controlador es el encargado de contar todos los permisos
 * @param {*} req
 * @param {*} res
 */
const countAllPermissions = async (req, res) => {
    try {
        const DataPermissions = await PermissionsModel.count()
        if (!DataPermissions) {
            handleHttpError(res, 'No se encontraron datos.', 404)
            return
        }
        handleHttpCount(
            res,
            'Se han encontrado los datos.',
            201,
            DataPermissions
        )
    } catch (error) {
        console.log(error)
        handleHttpError(res, '', 403, error)
    }
}

module.exports = {
    createPermission,
    Permissions,
    Permission,
    updatePermission,
    deletePermission,
    disablePermission,
    enablePermission,
    permissionsAll,
    countAllPermissions,
}
