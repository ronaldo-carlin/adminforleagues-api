const { v4: uuidv4 } = require('uuid')
const GUIDsGeneral = require('../guids_general')
module.exports = {
    async up(queryInterface, Sequelize) {
        // Inserta un usuario por defecto
        await queryInterface.bulkInsert(
            'users',
            [
                {
                    guid: uuidv4(),
                    tenant_guid: GUIDsGeneral.tenantSuperAdminGuid,
                    role_guid: GUIDsGeneral.roleSuperAdminGuid, // 1: Administrador General, 2: Administrador de liga, 3: Entrenador/Dueño del equipo, 4: arbitro
                    username: 'supertenant',
                    first_name: 'Super',
                    last_name: 'Tenant',
                    email: 'tenant@adminforleagues.com',
                    password:
                        '$2a$10$8B2Uqcb/.EZoeUW53bVEsudJxAjXOFhaY6nGAsWfViQq0p.o8n4yu', // Admin+.
                    phone: 1234567890,
                    isVerified: true,
                    image_path:
                        'https://encrypted-tbn0.gstatic.com/images?q=tbn:ANd9GcTSrsozCMwpPp8yaK-zyiLe0XU-_chgZdCoPBA_BrZUXg&s',
                    picture: '',
                    country: '',
                    state: '',
                    city: '',
                    google_id: '',
                    facebook_id: '',
                    two_factor_auth: false,
                    weeklyNewsletter: false,
                    lifecycleEmails: false,
                    promotionalEmails: false,
                    productUpdates: false,
                    last_login: new Date(),
                    is_active: 1, // 1: Activo, 0: Inactivo
                    created_at: new Date(),
                    updated_at: new Date(),
                },
            ],
            {}
        )
    },

    async down(queryInterface, Sequelize) {
        await queryInterface.bulkDelete('Users', null, {})
    },
}
