const { Op } = require('sequelize')
const { v4: uuidv4 } = require('uuid')
const { matchedData } = require('express-validator')
const { handleHttpSuccess } = require('../../utils/handleSuccess')
const { handleHttpError } = require('../../utils/handleError')
const { handleHttpCount } = require('../../utils/handleCount')
const {
    getSequelize,
    getTeamsCompetitionsModel,
} = require('../../helpers/sequelizeHelper')

/**
 * Este controlador es el encargado de registrar una competencia de equipos
 * @param {*} req
 * @param {*} res
 */
const CreateTeamsCompetition = async (req, res) => {
    try {
        const sequelize = getSequelize(req)
        const TeamsCompetitionsModel = getTeamsCompetitionsModel(sequelize)
        const data = matchedData(req)
        const created_by = req.user.guid
        const created_at = new Date()
        const guid = await uuidv4()
        data.created_by = created_by
        data.created_at = created_at
        data.guid = guid

        const teamCompetition = await TeamsCompetitionsModel.create(data)
        return handleHttpSuccess(
            res,
            'Competencia de equipos creada.',
            201,
            teamCompetition
        )
    } catch (error) {
        console.log('Error al crear la competencia de equipos:', error)
        handleHttpError(
            res,
            'Error al crear la competencia de equipos.',
            403,
            error
        )
    }
}

/**
 * Este controlador es el encargado de obtener las competencias de equipos activas
 * @param {*} req
 * @param {*} res
 */
const TeamsCompetitions = async (req, res) => {
    try {
        const sequelize = getSequelize(req)
        const TeamsCompetitionsModel = getTeamsCompetitionsModel(sequelize)

        const competitions = await TeamsCompetitionsModel.findAll({
            where: { is_active: 1 },
        })

        if (!competitions.length) {
            handleHttpError(
                res,
                'No se encontraron competencias de equipos.',
                404
            )
            return
        }

        handleHttpSuccess(
            res,
            'Competencias de equipos encontradas.',
            201,
            competitions
        )
    } catch (error) {
        console.log('Error obteniendo competencias de equipos:', error)
        handleHttpError(
            res,
            'Error obteniendo competencias de equipos.',
            403,
            error
        )
    }
}

/**
 * Este controlador es el encargado de obtener una competencia de equipos por GUID
 * @param {*} req
 * @param {*} res
 */
const TeamsCompetition = async (req, res) => {
    try {
        const sequelize = getSequelize(req)
        const TeamsCompetitionsModel = getTeamsCompetitionsModel(sequelize)
        const { guid } = req.params

        const competition = await TeamsCompetitionsModel.findOne({
            where: { guid },
        })

        if (!competition) {
            handleHttpError(res, 'La competencia de equipos no existe.', 404)
            return
        }

        if (competition.is_active == 0) {
            handleHttpError(
                res,
                'La competencia de equipos ha sido deshabilitada.',
                401
            )
            return
        }

        handleHttpSuccess(
            res,
            'Competencia de equipos encontrada.',
            201,
            competition
        )
    } catch (error) {
        console.log('Error obteniendo competencia de equipos:', error)
        handleHttpError(
            res,
            'Error obteniendo competencia de equipos.',
            403,
            error
        )
    }
}

/**
 * Este controlador es el encargado de actualizar una competencia de equipos
 * @param {*} req
 * @param {*} res
 */
const UpdateTeamsCompetition = async (req, res) => {
    try {
        const sequelize = getSequelize(req)
        const TeamsCompetitionsModel = getTeamsCompetitionsModel(sequelize)
        const { guid } = req.params
        const data = matchedData(req)
        const updated_by = req.user.guid
        const updated_at = new Date()
        data.updated_by = updated_by
        data.updated_at = updated_at

        const [updateCount] = await TeamsCompetitionsModel.update(data, {
            where: { guid },
        })

        if (updateCount === 0) {
            return handleHttpError(
                res,
                'La competencia de equipos no existe.',
                404
            )
        }

        const competition = await TeamsCompetitionsModel.findOne({
            where: { guid },
        })

        if (!competition) {
            handleHttpError(res, 'La competencia de equipos no existe.', 404)
            return
        }

        if (competition.is_active == 0) {
            handleHttpError(
                res,
                'La competencia de equipos ha sido deshabilitada.',
                401
            )
            return
        }

        handleHttpSuccess(
            res,
            'Competencia de equipos actualizada.',
            201,
            competition
        )
    } catch (error) {
        console.log('Error actualizando competencia de equipos:', error)
        handleHttpError(
            res,
            'Error actualizando competencia de equipos.',
            403,
            error
        )
    }
}

/**
 * Este controlador es el encargado de eliminar una competencia de equipos
 * @param {*} req
 * @param {*} res
 */
const DeleteTeamsCompetition = async (req, res) => {
    try {
        const sequelize = getSequelize(req)
        const TeamsCompetitionsModel = getTeamsCompetitionsModel(sequelize)
        const { guid } = req.params
        const deleted_by = req.user.guid
        const deleted_at = new Date()
        const [updateCount] = await TeamsCompetitionsModel.update(
            {
                is_active: 0,
                deleted_by,
                deleted_at,
            },
            {
                where: { guid },
            }
        )

        if (updateCount === 0) {
            return handleHttpError(
                res,
                'La competencia de equipos no existe.',
                404
            )
        }

        handleHttpSuccess(res, 'Competencia de equipos eliminada.', 201)
    } catch (error) {
        console.log('Error eliminando competencia de equipos:', error)
        handleHttpError(
            res,
            'Error eliminando competencia de equipos.',
            403,
            error
        )
    }
}

/**
 * Este controlador es el encargado de desactivar una competencia de equipos
 * @param {*} req
 * @param {*} res
 */
const DisableTeamsCompetition = async (req, res) => {
    try {
        const sequelize = getSequelize(req)
        const TeamsCompetitionsModel = getTeamsCompetitionsModel(sequelize)
        const { guid } = req.params
        const updated_by = req.user.guid
        const updated_at = new Date()
        const [updateCount] = await TeamsCompetitionsModel.update(
            {
                is_active: 0,
                updated_by,
                updated_at,
            },
            {
                where: { guid },
            }
        )

        if (updateCount === 0) {
            return handleHttpError(
                res,
                'La competencia de equipos no existe.',
                404
            )
        }

        handleHttpSuccess(res, 'Competencia de equipos deshabilitada.', 201)
    } catch (error) {
        console.log('Error deshabilitando competencia de equipos:', error)
        handleHttpError(
            res,
            'Error deshabilitando competencia de equipos.',
            403,
            error
        )
    }
}

/**
 * Este controlador es el encargado de activar una competencia de equipos
 * @param {*} req
 * @param {*} res
 */
const EnableTeamsCompetition = async (req, res) => {
    try {
        const sequelize = getSequelize(req)
        const TeamsCompetitionsModel = getTeamsCompetitionsModel(sequelize)
        const { guid } = req.params
        const updated_by = req.user.guid
        const updated_at = new Date()
        const [updateCount] = await TeamsCompetitionsModel.update(
            {
                is_active: 1,
                updated_by,
                updated_at,
            },
            {
                where: { guid },
            }
        )

        if (updateCount === 0) {
            return handleHttpError(
                res,
                'La competencia de equipos no existe.',
                404
            )
        }

        handleHttpSuccess(res, 'Competencia de equipos habilitada.', 201)
    } catch (error) {
        console.log('Error habilitando competencia de equipos:', error)
        handleHttpError(
            res,
            'Error habilitando competencia de equipos.',
            403,
            error
        )
    }
}

/**
 * Este controlador es el encargado de obtener todas las competencias de equipos
 * @param {*} req
 * @param {*} res
 */
const TeamsCompetitionsAll = async (req, res) => {
    try {
        const sequelize = getSequelize(req)
        const TeamsCompetitionsModel = getTeamsCompetitionsModel(sequelize)

        const competitions = await TeamsCompetitionsModel.findAll()

        if (!competitions.length) {
            handleHttpError(
                res,
                'No se encontraron competencias de equipos.',
                404
            )
            return
        }

        handleHttpSuccess(
            res,
            'Competencias de equipos encontradas.',
            201,
            competitions
        )
    } catch (error) {
        console.log('Error obteniendo competencias de equipos:', error)
        handleHttpError(
            res,
            'Error obteniendo competencias de equipos.',
            403,
            error
        )
    }
}

/**
 * Este controlador es el encargado de contar todas las competencias de equipos
 * @param {*} req
 * @param {*} res
 */
const TeamsCompetitionsCount = async (req, res) => {
    try {
        const sequelize = getSequelize(req)
        const TeamsCompetitionsModel = getTeamsCompetitionsModel(sequelize)

        const count = await TeamsCompetitionsModel.count()

        handleHttpCount(res, 'Competencias de equipos encontradas.', 201, count)
    } catch (error) {
        console.log('Error contando competencias de equipos:', error)
        handleHttpError(
            res,
            'Error contando competencias de equipos.',
            403,
            error
        )
    }
}

module.exports = {
    CreateTeamsCompetition,
    TeamsCompetitions,
    TeamsCompetition,
    UpdateTeamsCompetition,
    DeleteTeamsCompetition,
    DisableTeamsCompetition,
    EnableTeamsCompetition,
    TeamsCompetitionsAll,
    TeamsCompetitionsCount,
}
