# Changelog

All notable changes to this project will be documented in this file. See [standard-version](https://github.com/conventional-changelog/standard-version) for commit guidelines.

### [0.0.4](https://gitlab.com/ronaldo-carlin/adminforleagues-api/compare/v0.0.3...v0.0.4) (2024-08-13)

### Features

-   **Controller:** ✨ CREATE TENANT CON NUEVA BASE DE DATOS ([8f59893](https://gitlab.com/ronaldo-carlin/adminforleagues-api/commit/8f5989366ec72b135b840b5eb5fa8f53e00ac51e))
-   **Controller:** ✨ CREATE TENANT CON SEEDERS ([65b7b40](https://gitlab.com/ronaldo-carlin/adminforleagues-api/commit/65b7b4056410b9dc203bf786af0c68f24602ea79))
-   **Controller:** ✨ CRUD para permissions ([b6216d5](https://gitlab.com/ronaldo-carlin/adminforleagues-api/commit/b6216d5d0b0adc165e512fa489f1901fb92d2ba2))
-   **CRUD:** ✨ CRUD DE MODULES TENANT ([00f2d45](https://gitlab.com/ronaldo-carlin/adminforleagues-api/commit/00f2d45307dcc83610feba9c69bf2a04e80b7002))
-   **CRUD:** ✨ CRUD DE PERMISSIONS TENANT ([dcf0e4d](https://gitlab.com/ronaldo-carlin/adminforleagues-api/commit/dcf0e4de33d6204798928cb073708c1d9b865b1e))
-   **CRUD:** ✨ CRUD DE ROLES TENANT ([8c910fe](https://gitlab.com/ronaldo-carlin/adminforleagues-api/commit/8c910fe19258817c1a1044df89128a980c4f3092))
-   **CRUD:** ✨ CRUD para coupons ([f9582c5](https://gitlab.com/ronaldo-carlin/adminforleagues-api/commit/f9582c55c3a3d6633f71cfb65f4c0be15a48101d))
-   **CRUD:** ✨ CRUD para Licenses ([ccdd56b](https://gitlab.com/ronaldo-carlin/adminforleagues-api/commit/ccdd56b9c0d16f10de30bee9d33cc14c5a083e0b))
-   **CRUD:** ✨ CRUD para orders ([351eee6](https://gitlab.com/ronaldo-carlin/adminforleagues-api/commit/351eee6d38f652421c114be13fccd889afd4bb20))
-   **CRUD:** ✨ CRUD para payments ([3ba3d8e](https://gitlab.com/ronaldo-carlin/adminforleagues-api/commit/3ba3d8e5fa69bd1b71d4f19c6ff9a05caabec8b7))
-   **CRUD:** ✨ CRUD para Plans ([6b3b417](https://gitlab.com/ronaldo-carlin/adminforleagues-api/commit/6b3b417b6f2cbadb427888569b363603f00aaaa0))
-   **CRUD:** ✨ CRUD para tenant_credentials ([8b5b10f](https://gitlab.com/ronaldo-carlin/adminforleagues-api/commit/8b5b10f6ff4240ef5bbb9209eb5695ce7ce804e3))
-   **CRUD:** ✨ CRUD para tenants ([41c5b73](https://gitlab.com/ronaldo-carlin/adminforleagues-api/commit/41c5b73e812ec3bb44fabb19ce9dacc1fcd0ea42))
-   **JWT:** ✨ NUEVAS CREDENCIALES PARA JWT TENANT ([a718408](https://gitlab.com/ronaldo-carlin/adminforleagues-api/commit/a718408d17df271870feece7b8344ae3ac15f86f))
-   **Routes:** ✨ AVANCE DE RUTAS TENANT ([7597b7e](https://gitlab.com/ronaldo-carlin/adminforleagues-api/commit/7597b7eebb4505c879f1deea37a68b96bf4abfda))

### Bug Fixes

-   **Controller:** 🐛 Auth organizado mejor ([a7a90d4](https://gitlab.com/ronaldo-carlin/adminforleagues-api/commit/a7a90d4394c2345bb5ba25b36b0e179b8bc76173))
-   **JWT:** 🐛 🔒️ JWT ([7b13d4b](https://gitlab.com/ronaldo-carlin/adminforleagues-api/commit/7b13d4bb42062911292649281867f0fbf12f8b61))
-   **Routes:** 🐛 Ajuste del patch ([02d8705](https://gitlab.com/ronaldo-carlin/adminforleagues-api/commit/02d8705819d332a8145d4ec92ccd610f634f308e))
-   **Routes:** 🐛 route guid ([5e78a1a](https://gitlab.com/ronaldo-carlin/adminforleagues-api/commit/5e78a1a04fd704e60cbb101caae8e1a97e88742d))
-   **Routes:** 🐛 Swagger Modules ([065d589](https://gitlab.com/ronaldo-carlin/adminforleagues-api/commit/065d5894b674ccc93b6f206e92c6ceb49efc56cc))
-   **seeders:** 🐛 GUIDS ([95267b3](https://gitlab.com/ronaldo-carlin/adminforleagues-api/commit/95267b3e900d0b518ea8929f30b76d8b2e015858))
-   **seeders:** 🐛 GUIDS MODIFICADOS ([8cc3eba](https://gitlab.com/ronaldo-carlin/adminforleagues-api/commit/8cc3eba1d8e771c0eb749eb0bf9fbb34d50be049))
-   **Swagger:** 🐛 ✏️ swagger routes ([eb26ad2](https://gitlab.com/ronaldo-carlin/adminforleagues-api/commit/eb26ad20bd2d74d715a1d944b42038e2ebe79bd4))
-   **Swagger:** 🐛 Corrección de post a get ([0f10f9d](https://gitlab.com/ronaldo-carlin/adminforleagues-api/commit/0f10f9df4aba148fdeefdf228b9feecbb1e47cb4))
-   **Swagger:** 🐛 swagger plans ([7277dbd](https://gitlab.com/ronaldo-carlin/adminforleagues-api/commit/7277dbd66e5b01385ebce4a69b3075554362f44a))

### [0.0.3](https://gitlab.com/ronaldo-carlin/adminforleagues-api/compare/v0.0.2...v0.0.3) (2024-07-06)

### Features

-   **Controller:** ✨ CRUD para modulos ([3312e24](https://gitlab.com/ronaldo-carlin/adminforleagues-api/commit/3312e24379e862686681fb73115c0074341a82ea))
-   **Controller:** ✨ nuevos endpoints de access controller ([5a2a997](https://gitlab.com/ronaldo-carlin/adminforleagues-api/commit/5a2a997237579ab710dde12903daddc33ee78a07))
-   **seeders:** ✨ migraciones y seeders ([32035e7](https://gitlab.com/ronaldo-carlin/adminforleagues-api/commit/32035e7084164eb302ce16aea2116e704aa85f75))

### Bug Fixes

-   **migrations:** 🐛 Corrección de migraciones ([9de8312](https://gitlab.com/ronaldo-carlin/adminforleagues-api/commit/9de8312307442db9dd1267c824643e5a4c0a190c))

### [0.0.2](https://gitlab.com/ronaldo-carlin/adminforleagues-api/compare/v0.0.1...v0.0.2) (2024-06-28)

### Features

-   **migrations:** ✨ migracion tenant ([4e585d9](https://gitlab.com/ronaldo-carlin/adminforleagues-api/commit/4e585d938fde40ad2c67619f6a3b36b844dae95b))
-   **migrations:** ✨ migraciones generales ([0a5ab08](https://gitlab.com/ronaldo-carlin/adminforleagues-api/commit/0a5ab080199a2604e0ee2550cd9230ee0a020ae5))
-   **seeders:** ✨ nuevos mockup ([94c38bf](https://gitlab.com/ronaldo-carlin/adminforleagues-api/commit/94c38bf987c4ec9164dcdccf3405b7da08b59157))

### [0.0.1](https://gitlab.com/ronaldo-carlin/adminforleagues-api/compare/v1.0.1...v0.0.1) (2024-06-21)

### Features

-   **Config:** ✨ 🔖 versionamiento automatico ([0cc4ac6](https://gitlab.com/ronaldo-carlin/adminforleagues-api/commit/0cc4ac6c5a3ee70dbc86638dbad588a361210de6))
-   **users:** ✨ primera prueba de conventional commit ([4c74f1f](https://gitlab.com/ronaldo-carlin/adminforleagues-api/commit/4c74f1f276bfa73badcfe8cfad1d032461516560))

### Bug Fixes

-   **Document:** 🐛 actualizacion de versiones ([5f703a3](https://gitlab.com/ronaldo-carlin/adminforleagues-api/commit/5f703a39c1deb105f39a262284de5142f7d7f7e9))

## (2024-06-20)

-   actualizacion de las migraciones! ([69a18ac](https://gitlab.com/ronaldo-carlin/adminforleagues-api/commit/69a18ac))
-   inicio de sesion con google y dependencias minimas ([c70304e](https://gitlab.com/ronaldo-carlin/adminforleagues-api/commit/c70304e))
-   inicio del proyecto! ([a7b8e20](https://gitlab.com/ronaldo-carlin/adminforleagues-api/commit/a7b8e20))
-   Initial commit ([483e24b](https://gitlab.com/ronaldo-carlin/adminforleagues-api/commit/483e24b))
-   se acomodador los archivos por tenant y genral para mayor organizacion, tambien ya se creo el primer ([8a97395](https://gitlab.com/ronaldo-carlin/adminforleagues-api/commit/8a97395))
-   se agregaron y acomodaron los modulos que hacian falta, aun quedaron pendientes algunos. ([22d10a3](https://gitlab.com/ronaldo-carlin/adminforleagues-api/commit/22d10a3))
-   se agregaron ya todas las migraciones y modelos de todas las entidades. ([1242b83](https://gitlab.com/ronaldo-carlin/adminforleagues-api/commit/1242b83))
-   se cambiaron de lugar las migraciones y seeders. ([7266f02](https://gitlab.com/ronaldo-carlin/adminforleagues-api/commit/7266f02))
-   se cambiaron las credenciales del envio de correo. ([1070eba](https://gitlab.com/ronaldo-carlin/adminforleagues-api/commit/1070eba))
-   se modifico api para que acepte bien el token cuando inician sesion con google ([61b5adf](https://gitlab.com/ronaldo-carlin/adminforleagues-api/commit/61b5adf))
-   se modifico varios archivos para que ahora las busquedas sean por guid y no por id ([0def776](https://gitlab.com/ronaldo-carlin/adminforleagues-api/commit/0def776))
-   se terminaron de acompletar las entidades faltantes y se comodaron las migraciones en dos carpetas p ([5e18708](https://gitlab.com/ronaldo-carlin/adminforleagues-api/commit/5e18708))
