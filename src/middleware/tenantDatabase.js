// middleware/tenantDatabase.js
const { getSequelizeInstance } = require('../database/database')
const { handleHttpError } = require('../utils/handleError')

const selectDatabase = async (req, res, next) => {
    const subdomain = req.subdomain

    try {
        req.sequelize = await getSequelizeInstance(subdomain)
        next()
    } catch (error) {
        console.error('Error setting up tenant database:', error)
        handleHttpError(res, error.message, 500, 'Internal server error')
    }
}

module.exports = selectDatabase
