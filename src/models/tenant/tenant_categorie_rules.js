// Importar Sequelize
const { DataTypes } = require('sequelize')

// Definir la función factory para el modelo CategorieRulesTenantModel
const CategorieRulesTenantModelFactory = (sequelize) => {
    const CategorieRulesTenantModel = sequelize.define(
        'categorie_rules',
        {
            id: {
                type: DataTypes.INTEGER,
                primaryKey: true,
                autoIncrement: true,
            },
            guid: {
                type: DataTypes.UUID,
                primaryKey: true,
                defaultValue: DataTypes.UUIDV4,
                unique: true,
            },
            title: {
                type: DataTypes.STRING,
            },
            description: {
                type: DataTypes.STRING,
            },
            is_active: {
                type: DataTypes.INTEGER,
                defaultValue: 1,
            },
            created_by: {
                type: DataTypes.UUID,
            },
            updated_by: {
                type: DataTypes.UUID,
            },
            deleted_by: {
                type: DataTypes.UUID,
            },
            created_at: {
                type: DataTypes.DATE,
            },
            updated_at: {
                type: DataTypes.DATE,
            },
            deleted_at: {
                type: DataTypes.DATE,
            },
        },
        {
            tableName: 'categorie_rules',
            timestamps: false,
        }
    )

    return CategorieRulesTenantModel
}

module.exports = CategorieRulesTenantModelFactory
