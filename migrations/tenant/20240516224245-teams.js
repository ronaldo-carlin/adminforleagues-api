module.exports = {
    async up(queryInterface, Sequelize) {
        // entidad para los equipos
        await queryInterface.createTable('teams', {
            // id del equipo
            id: {
                type: Sequelize.INTEGER,
                primaryKey: true,
                autoIncrement: true,
                allowNull: false,
            },
            // identificador unico del equipo
            guid: {
                type: Sequelize.UUID,
                defaultValue: Sequelize.UUIDV4,
                unique: true,
                allowNull: false,
            },
            // torneo al que pertenece el equipo
            competition_guid: {
                type: Sequelize.UUID,
            },
            // nombre del equipo
            name: {
                type: Sequelize.TEXT,
            },
            // grupo al que pertenece el equipo
            group: {
                type: Sequelize.TEXT,
            },
            // logo del equipo
            image_path: {
                type: Sequelize.TEXT,
            },
            // descripcion del equipo
            description: {
                type: Sequelize.TEXT,
            },
            // estado del equipo
            is_active: {
                type: Sequelize.BOOLEAN,
                defaultValue: 1, // valor por defecto
            },
            // usuario que creo el registro
            created_by: {
                type: Sequelize.UUID,
            },
            // fecha de creacion
            created_at: {
                type: Sequelize.DATE,
                defaultValue: Sequelize.literal('CURRENT_TIMESTAMP'),
            },
            // usuario que actualizo el registro
            updated_by: {
                type: Sequelize.UUID,
            },
            // fecha de actualizacion
            updated_at: {
                type: Sequelize.DATE,
            },
            // usuario que elimino el registro
            deleted_by: {
                type: Sequelize.UUID,
            },
            // fecha de eliminacion
            deleted_at: {
                type: Sequelize.DATE,
            },
        })

        // indeces
        await queryInterface.addIndex('teams', ['guid'], {
            unique: true,
            name: 'unique_teams_guid',
        })

        await queryInterface.addIndex('teams', ['competition_guid'], {
            name: 'idx_competition_guid',
        })

        // llave foranea
        await queryInterface.addConstraint('teams', {
            fields: ['competition_guid'],
            type: 'foreign key',
            name: 'fk_teams_competition',
            references: {
                table: 'competitions',
                field: 'guid',
            },
        })
    },

    async down(queryInterface, Sequelize) {
        await queryInterface.removeConstraint('teams', 'fk_teams_competition')
        await queryInterface.removeIndex('teams', 'unique_teams_guid')
        await queryInterface.removeIndex('teams', 'idx_competition_guid')
        await queryInterface.dropTable('teams')
    },
}
