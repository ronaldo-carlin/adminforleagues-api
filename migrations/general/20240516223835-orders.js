module.exports = {
    async up(queryInterface, Sequelize) {
        // entidad para almacenar las ordenes de compra
        await queryInterface.createTable('orders', {
            // id del registro
            id: {
                type: Sequelize.INTEGER,
                primaryKey: true,
                autoIncrement: true,
                allowNull: false,
            },
            // guid del registro
            guid: {
                type: Sequelize.UUID,
                defaultValue: Sequelize.UUIDV4, // Genera un UUID por defecto
                unique: true,
                allowNull: false,
            },
            // identificador del usuario que realizo la compra
            user_guid: {
                type: Sequelize.UUID,
            },
            // identificador del plan comprado
            plan_guid: {
                type: Sequelize.UUID,
            },
            // cupon de descuento
            coupon_guid: {
                type: Sequelize.UUID,
            },
            // transaccion de pago
            transaction: {
                type: Sequelize.TEXT,
            },
            // metodo de pago
            payment_method: {
                type: Sequelize.TEXT,
            },
            // monto de la transaccion
            amount: {
                type: Sequelize.DECIMAL,
            },
            // moneda de la transaccion
            currency: {
                type: Sequelize.TEXT,
            },
            // icono de la moneda
            currency_icon: {
                type: Sequelize.TEXT,
            },
            // fecha de inicio de la orden
            enroll_start: {
                type: Sequelize.DATE,
            },
            // fecha de finalizacion de la orden
            enroll_end: {
                type: Sequelize.DATE,
            },
            // fecha de expiracion de la orden
            expiration_date: {
                type: Sequelize.DATE,
            },
            // fecha de renovacion de la orden
            renewal_date: {
                type: Sequelize.DATE,
            },
            // fecha de cancelacion de la orden
            cancel_date: {
                type: Sequelize.DATE,
            },
            // tipo de cancelacion
            cancel_type: {
                type: Sequelize.TEXT,
            },
            // estado del registro
            is_active: {
                type: Sequelize.BOOLEAN,
                defaultValue: 1, // valor por defecto
            },
            // usuario que creo el registro
            created_by: {
                type: Sequelize.UUID,
            },
            // fecha de creacion
            created_at: {
                type: Sequelize.DATE,
                defaultValue: Sequelize.literal('CURRENT_TIMESTAMP'),
            },
            // usuario que actualizo el registro
            updated_by: {
                type: Sequelize.UUID,
            },
            // fecha de actualizacion
            updated_at: {
                type: Sequelize.DATE,
            },
            // usuario que elimino el registro
            deleted_by: {
                type: Sequelize.UUID,
            },
            // fecha de eliminacion
            deleted_at: {
                type: Sequelize.DATE,
            },
        })

        // Agregar índices después de crear la tabla
        await queryInterface.addIndex('orders', ['guid'], {
            unique: true,
            name: 'uniq_orders_guid',
        })

        await queryInterface.addIndex('orders', ['user_guid'], {
            name: 'id_user_guid',
        })

        await queryInterface.addIndex('orders', ['plan_guid'], {
            name: 'id_plan_guid',
        })

        await queryInterface.addIndex('orders', ['coupon_guid'], {
            name: 'id_coupon_guid',
        })

        // Agregar constraints después de crear la tabla
        await queryInterface.addConstraint('orders', {
            fields: ['user_guid'],
            type: 'foreign key',
            name: 'fk_order_user',
            references: {
                table: 'users',
                field: 'guid',
            },
            onDelete: 'NO ACTION',
            onUpdate: 'NO ACTION',
        })

        await queryInterface.addConstraint('orders', {
            fields: ['plan_guid'],
            type: 'foreign key',
            name: 'fk_order_plan',
            references: {
                table: 'plans',
                field: 'guid',
            },
            onDelete: 'NO ACTION',
            onUpdate: 'NO ACTION',
        })

        await queryInterface.addConstraint('orders', {
            fields: ['coupon_guid'],
            type: 'foreign key',
            name: 'fk_order_coupon',
            references: {
                table: 'coupons',
                field: 'guid',
            },
            onDelete: 'NO ACTION',
            onUpdate: 'NO ACTION',
        })
    },

    async down(queryInterface, Sequelize) {
        await queryInterface.removeConstraint('orders', 'fk_order_user')
        await queryInterface.removeConstraint('orders', 'fk_order_plan')
        await queryInterface.removeConstraint('orders', 'fk_order_coupon')
        await queryInterface.removeIndex('orders', 'uniq_orders_guid')
        await queryInterface.removeIndex('orders', 'id_user_guid')
        await queryInterface.removeIndex('orders', 'id_plan_guid')
        await queryInterface.removeIndex('orders', 'id_coupon_guid')
        await queryInterface.dropTable('orders')
    },
}
