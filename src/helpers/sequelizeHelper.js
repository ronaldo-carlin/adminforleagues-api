// sequelizeHelper.js

const AuthTenantModelFactory = require('../models/tenant/tenant_auth')
const RolesTenantModelFactory = require('../models/tenant/tenant_roles')
const PermissionsTenantModelFactory = require('../models/tenant/tenant_Permissions')
const TokenTenantModelFactory = require('../models/tenant/tenant_token')
const ModulesTenantModelFactory = require('../models/tenant/tenant_modules')
const AccessControlTenantModelFactory = require('../models/tenant/tenant_access_control')
const SportsTenantModelFactory = require('../models/tenant/tenant_sports')
const SedesTenantModelFactory = require('../models/tenant/tenant_sedes')
const CategoriesTenantModelFactory = require('../models/tenant/tenant_categories')
const TypesCompetitionsTenantModelFactory = require('../models/tenant/tenant_types_competitions')
const SeasonsTenantModelFactory = require('../models/tenant/tenant_seasons')
const CompetitionsTenantModelFactory = require('../models/tenant/tenant_competitions')
const TeamsTenantModelFactory = require('../models/tenant/tenant_teams')
const PlayersTenantModelFactory = require('../models/tenant/tenant_players')
const MatchesTenantModelFactory = require('../models/tenant/tenant_matches')
const PlayersUpdateTenantModelFactory = require('../models/tenant/tenant_players_update')
const SponsorsTenantModelFactory = require('../models/tenant/tenant_sponsors')
const TeamsCompetitionsTenantModelFactory = require('../models/tenant/tenant_teams_competitions')
const TypesNewsTenantModelFactory = require('../models/tenant/tenant_types_news')
const { handleHttpError } = require('../utils/handleError')

// Función para obtener la instancia de Sequelize del request
const getSequelize = (req, res) => {
    if (!req.sequelize) {
        console.error('Sequelize instance is not defined')
        return handleHttpError(res, 'Database error', 500, '')
    }
    return req.sequelize
}

// Función para obtener el modelo de usuario
const getUserModel = (sequelize) => {
    const AuthTenantModel = AuthTenantModelFactory(sequelize)
    if (!AuthTenantModel) {
        console.error('AuthTenantModel is not defined')
        return handleHttpError(res, 'Model error', 500, '')
    }
    return AuthTenantModel
}

// Función para obtener el modelo de roles
const getRolesModel = (sequelize) => {
    const RolesTenantModel = RolesTenantModelFactory(sequelize)
    if (!RolesTenantModel) {
        console.error('RolesTenantModel is not defined')
        return handleHttpError(res, 'Model error', 500, '')
    }
    return RolesTenantModel
}

// Función para obtener el modelo de permisos
const getPermissionsModel = (sequelize) => {
    const PermissionsTenantModel = PermissionsTenantModelFactory(sequelize)
    if (!PermissionsTenantModel) {
        console.error('PermissionsTenantModel is not defined')
        return handleHttpError(res, 'Model error', 500, '')
    }
    return PermissionsTenantModel
}

// Función para obtener el modulo
const getModulesModel = (sequelize) => {
    const ModulesTenantModel = ModulesTenantModelFactory(sequelize)
    if (!ModulesTenantModel) {
        console.error('ModulesTenantModel is not defined')
        return handleHttpError(res, 'Model error', 500, '')
    }
    return ModulesTenantModel
}

// Función para obtener el control de acceso
const getAccessControlModel = (sequelize) => {
    const AccessControlTenantModel = AccessControlTenantModelFactory(sequelize)
    if (!AccessControlTenantModel) {
        console.error('AccessControlTenantModel is not defined')
        return handleHttpError(res, 'Model error', 500, '')
    }
    return AccessControlTenantModel
}

// Función para obtener el modelo de token
const getTokenModel = (sequelize) => {
    const TokenTenantModel = TokenTenantModelFactory(sequelize)
    if (!TokenTenantModel) {
        console.error('TokenTenantModel is not defined')
        return handleHttpError(res, 'Model error', 500, '')
    }
    return TokenTenantModel
}

// Función para obtener el deporte
const getSportsModel = (sequelize) => {
    const SportsTenantModel = SportsTenantModelFactory(sequelize)
    if (!SportsTenantModel) {
        console.error('SportsTenantModel is not defined')
        return handleHttpError(res, 'Model error', 500, '')
    }
    return SportsTenantModel
}

// Función para obtener las sedes
const getSedesModel = (sequelize) => {
    const SedesTenantModel = SedesTenantModelFactory(sequelize)
    if (!SedesTenantModel) {
        console.error('SedesTenantModel is not defined')
        return handleHttpError(res, 'Model error', 500, '')
    }
    return SedesTenantModel
}

// Función para obtener las categorias
const getCategoriesModel = (sequelize) => {
    const CategoriesTenantModel = CategoriesTenantModelFactory(sequelize)
    if (!CategoriesTenantModel) {
        console.error('CategoriesTenantModel is not defined')
        return handleHttpError(res, 'Model error', 500, '')
    }
    return CategoriesTenantModel
}

// Función para obtener los tipos de competencias
const getTypesCompetitionsModel = (sequelize) => {
    const TypesCompetitionsTenantModel =
        TypesCompetitionsTenantModelFactory(sequelize)
    if (!TypesCompetitionsTenantModel) {
        console.error('TypesCompetitionsTenantModel is not defined')
        return handleHttpError(res, 'Model error', 500, '')
    }
    return TypesCompetitionsTenantModel
}

// Función para obtener las temporadas
const getSeasonsModel = (sequelize) => {
    const SeasonsTenantModel = SeasonsTenantModelFactory(sequelize)
    if (!SeasonsTenantModel) {
        console.error('SeasonsTenantModel is not defined')
        return handleHttpError(res, 'Model error', 500, '')
    }
    return SeasonsTenantModel
}

// Función para obtener las competencias
const getCompetitionsModel = (sequelize) => {
    const CompetitionsTenantModel = CompetitionsTenantModelFactory(sequelize)
    if (!CompetitionsTenantModel) {
        console.error('CompetitionsTenantModel is not defined')
        return handleHttpError(res, 'Model error', 500, '')
    }
    return CompetitionsTenantModel
}

// Función para obtener los equipos
const getTeamsModel = (sequelize) => {
    const TeamsTenantModel = TeamsTenantModelFactory(sequelize)
    if (!TeamsTenantModel) {
        console.error('TeamsTenantModel is not defined')
        return handleHttpError(res, 'Model error', 500, '')
    }
    return TeamsTenantModel
}

// Función para obtener los jugadores
const getPlayersModel = (sequelize) => {
    const PlayersTenantModel = PlayersTenantModelFactory(sequelize)
    if (!PlayersTenantModel) {
        console.error('PlayersTenantModel is not defined')
        return handleHttpError(res, 'Model error', 500, '')
    }
    return PlayersTenantModel
}

// Función para obtener los partidos
const getMatchesModel = (sequelize) => {
    const MatchesTenantModel = MatchesTenantModelFactory(sequelize)
    if (!MatchesTenantModel) {
        console.error('MatchesTenantModel is not defined')
        return handleHttpError(res, 'Model error', 500, '')
    }
    return MatchesTenantModel
}

// Función para obtener los jugadores actualizados
const getPlayersUpdateModel = (sequelize) => {
    const PlayersUpdateTenantModel = PlayersUpdateTenantModelFactory(sequelize)
    if (!PlayersUpdateTenantModel) {
        console.error('PlayersUpdateTenantModel is not defined')
        return handleHttpError(res, 'Model error', 500, '')
    }
    return PlayersUpdateTenantModel
}

// Función para obtener los patrocinadores
const getSponsorsModel = (sequelize) => {
    const SponsorsTenantModel = SponsorsTenantModelFactory(sequelize)
    if (!SponsorsTenantModel) {
        console.error('SponsorsTenantModel is not defined')
        return handleHttpError(res, 'Model error', 500, '')
    }
    return SponsorsTenantModel
}

// Función para obtener los equipos en competencia
const getTeamsCompetitionsModel = (sequelize) => {
    const TeamsCompetitionsTenantModel =
        TeamsCompetitionsTenantModelFactory(sequelize)
    if (!TeamsCompetitionsTenantModel) {
        console.error('TeamsCompetitionsTenantModel is not defined')
        return handleHttpError(res, 'Model error', 500, '')
    }
    return TeamsCompetitionsTenantModel
}

// Función para obtener los tipos de noticias
const getTypesNewsModel = (sequelize) => {
    const TypesNewsTenantModel = TypesNewsTenantModelFactory(sequelize)
    if (!TypesNewsTenantModel) {
        console.error('TypesNewsTenantModel is not defined')
        return handleHttpError(res, 'Model error', 500, '')
    }
    return TypesNewsTenantModel
}

module.exports = {
    getSequelize,
    getUserModel,
    getRolesModel,
    getTokenModel,
    getPermissionsModel,
    getAccessControlModel,
    getModulesModel,
    getSportsModel,
    getSedesModel,
    getCategoriesModel,
    getTypesCompetitionsModel,
    getSeasonsModel,
    getCompetitionsModel,
    getPlayersModel,
    getTeamsModel,
    getMatchesModel,
    getPlayersUpdateModel,
    getSponsorsModel,
    getTeamsCompetitionsModel,
    getTypesNewsModel,
}
